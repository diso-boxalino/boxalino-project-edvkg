package ch.objcons.ecom.proj.edvkg.mzv;

import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.XBoxValueManagementException;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.box.client.BoxClient;
import ch.objcons.ecom.query.IQueryHook;

public class MailMergeQueryHook extends EDVKGQueryHook {

	protected IQueryHook _hookTotal;
	
	protected MailMergeQueryHook() {
		
	}
	
	protected MailMergeQueryHook(int queryTypeMailMerge) throws Exception {
		super(queryTypeMailMerge);
		int queryTypeTotal;
		switch (queryTypeMailMerge) {
		case QH_BRIEF_AN_PERSON:
			queryTypeTotal = QH_BRIEF_AN_PERSON_ORG;
			break;
		case QH_BRIEF_AN_ELTERN:
			queryTypeTotal = QH_BRIEF_AN_ELTERN_ORG;
			break;
		case QH_BRIEF_AN_ELTERN_NO_DUPLS:
			queryTypeTotal = QH_BRIEF_AN_ELTERN_NO_DUPLS_ORG;
			break;
		case QH_BRIEF_AN_FAMILIE:
			queryTypeTotal = QH_BRIEF_AN_FAMILIE_ORG;
			break;
		default:
			queryTypeTotal = -1;
		}
		_hookTotal = getInstance(queryTypeTotal);
		
	}
	
	
	
	@Override
	public String hookMaxRowCount(String queryString, QueryInfo queryInfo,
			BOContext context) {
		hookGetResults(queryString, queryInfo, context);
		return _hookTotal.hookMaxRowCount(queryString, queryInfo, context);
	}

	@Override
	public String hookGetResults(String queryString, QueryInfo queryInfo, BOContext context) {
		String res = super.hookGetResults(queryString, queryInfo, context);
		_hookTotal.hookGetResults(queryString, queryInfo, context); // schreibt nach client_request_hookNumberOfRows
		return res;
	}
	
	protected void setResult(IRequest request, int rowCount) {
		String[] numberOfRows = { String.valueOf(rowCount) };
		// Analog zu view_search_suchePersonen, welche in der Session gespeichert wird (und
		// es gibt nur eine Suchbenamsung), wird auch die Anzahl der Mail Merge Datensätze
		// in der Session gespeichert.
		try {
			BoxClient.getInstance().handleParameter(request, "session_hookNumberOfMailMergeRows", numberOfRows);
		} catch (XBoxValueManagementException e) {
		} 
	}

}
