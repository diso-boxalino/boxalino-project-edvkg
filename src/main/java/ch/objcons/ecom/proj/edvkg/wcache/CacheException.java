/*
 * Created on 24.01.2005
 *
 * (c) by Boxalino
 */
package ch.objcons.ecom.proj.edvkg.wcache;

/**
 * @author Dominik Raymann
 */
public class CacheException extends Exception {
	public CacheException(String message) {
		super(message);
	}
}
