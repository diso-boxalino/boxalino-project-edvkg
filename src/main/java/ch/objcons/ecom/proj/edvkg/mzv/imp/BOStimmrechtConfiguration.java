package ch.objcons.ecom.proj.edvkg.mzv.imp;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOObject;

public class BOStimmrechtConfiguration extends BOObject {

	public static final String CLASS_NAME = "StimmrechtConfiguration";
	
	public BOStimmrechtConfiguration(BLTransaction arg0, long arg1) {
		super(arg0, arg1);
	}

}
