package ch.objcons.ecom.proj.edvkg;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.BOValidationException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.log.Log;

/*
 * just setz the history of the Person
 */
public class BOKalenderDatum extends BOObject {

	private static int _indexTag;
	private static int _indexMonat;
	private static int _indexJahr;

	public BOKalenderDatum(BLTransaction trans, long oid) {
		super(trans, oid);
		if (_indexTag == 0) {
			// we do that just the first time
			try {
				_indexTag = getIndex("Tag");
				_indexMonat = getIndex("Monat");
				_indexJahr = getIndex("Jahr");
			}
			catch (XMetaModelNotFoundException e) {
				Log.logSystemAlarm("Unable to initialize BOKalenderDatum", e);
			}
		}
	}

	/*
	* method called before commit, we just validate if the date is correct
	*/
	public void validate(BOContext context) throws BOValidationException, XMetaModelQueryException, XSecurityException {
		String tag  = this.getAttribute("Tag", false, context);
		String monat = this.getAttribute("Monat", false, context);
		String jahr = this.getAttribute("Jahr", false, context);
		int monatd, tagd, jahrd;

		SimpleDateFormat sdate = new SimpleDateFormat("yyyyMMdd");
		sdate.setLenient(false);

		if ((tag == null) || (tag.length() == 0) || (tag.length() > 2)) {
			throw new BOValidationException("Der Tag ist nicht korrekt");
		}
		if (tag.length() == 1) {
			tag = "0"+tag;
		}
		if ((monat == null) || (monat.length() == 0) || (monat.length() > 2)) {
			throw new BOValidationException("Der Monat ist nicht korrekt");
		}
		if (monat.length() == 1) {
			monat = "0"+monat;
		}
		if (jahr == null) {
			// it is a static date, so we just care that nowbody can enter the 29. Februar
			jahr = "2001";
		} else if (jahr.length() != 4) {
			throw new BOValidationException("Das Jahr ist nicht korrekt");
		}
		jahrd = Integer.parseInt(jahr);

		java.util.Date date = null;
		try {
			date = sdate.parse(jahr+monat+tag);
		} catch (ParseException e) {
			throw new BOValidationException("Datum ist nicht korrekt");
		}
		return;
	}
}
