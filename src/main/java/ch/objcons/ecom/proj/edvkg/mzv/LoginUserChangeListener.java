package ch.objcons.ecom.proj.edvkg.mzv;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bl.BOObjectChangedEvent;
import ch.objcons.ecom.bl.IBOObjectChangeListener;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

public class LoginUserChangeListener implements IBOObjectChangeListener {

	
	private static final ILogger LOGGER = Logger.getLogger(LoginUserChangeListener.class);
	private static final Collection<String> IGNORE_CHANGES_FOR_ATTRIBUTES = Arrays.asList("lastLoginDate", "lastLoginTime", "failedLoginCount");
	private static final String KEY_MODIFICATIONSOURCE = "modifyingChangeListenerForLoginUser";
	
	@Override
	public void notifyObjectsChanged(BOObjectChangedEvent event) {
		BLTransaction trx = event.getTransaction();
		if (trx.getValue(KEY_MODIFICATIONSOURCE) == this) return;
		
		BOContext context = trx.getContext();
		try {
			BOObject boAdminUser = BoxalinoUtilities.getObjectByKey("UserGroup", "name", "bx_administratorUser", trx);
			BOObject boCommonUser = BoxalinoUtilities.getObjectByKey("UserGroup", "name", "edvkgUser", trx);
		
			for (BOObject boLoginUser : event.getChangedObjects()) {
				if (hasChangesForAttributes(boLoginUser, IGNORE_CHANGES_FOR_ATTRIBUTES)) continue;
				
				boLoginUser.setValue("modificationTS", new Date(), context);
				if (!boLoginUser.hasRecordedChanges("Administrator")) continue;
				
				boolean isAdministrator = Boolean.TRUE.equals(boLoginUser.getValue("Administrator"));
				boLoginUser.setReferencedObject("userItem", isAdministrator ? boAdminUser : boCommonUser);
			}
			trx.setValue(KEY_MODIFICATIONSOURCE, this);
			trx.commit();
		} catch (XMetaException e) {
			LOGGER.error("XMetaException occurred!", e);
		} catch (XSecurityException e) {
			LOGGER.error("XSecurityException occurred!", e);
		} catch (XBOConcurrencyConflict e) {
			LOGGER.error("XBOConcurrencyConflict occurred!", e);
		}
	}

	private boolean hasChangesForAttributes(BOObject bo, Collection<String> attributes) {
		for (String attribute : attributes)
			if (bo.hasRecordedChanges(attribute)) return true;
		return false;
	}

}
