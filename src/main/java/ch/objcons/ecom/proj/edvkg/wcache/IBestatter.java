/*
 * Created on 27.01.2005
 *
 * (c) by Boxalino
 */
package ch.objcons.ecom.proj.edvkg.wcache;
/**
 * @author Dominik Raymann
 */
public interface IBestatter {
	public abstract String getName();
	public abstract String getVorname();
	public abstract String getKurz();
	public abstract String getGesperrt();
	public abstract boolean isGesperrt();
	public abstract String getOID();
}