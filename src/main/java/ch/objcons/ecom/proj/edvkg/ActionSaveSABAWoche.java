/*
 * Created on 26.01.2005
 *
 * (c) by Boxalino
 */
package ch.objcons.ecom.proj.edvkg;

import java.util.Enumeration;
import java.util.Hashtable;

import ch.objcons.ecom.api.IParameters;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.adapters.EActionAdapter2;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.proj.edvkg.wcache.AgendaWoche;
import ch.objcons.ecom.proj.edvkg.wcache.CacheException;
import ch.objcons.ecom.proj.edvkg.wcache.CacheSABAWochen;
import ch.objcons.ecom.proj.edvkg.wcache.IKirchgemeinde;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.ecom.utils.EParseUtilities;
import ch.objcons.log.Log;

/**
 * @author Dominik Raymann
 */
public class ActionSaveSABAWoche extends EActionAdapter2 {
	private BoxEdit _box = null;
	
	private Hashtable _values = new Hashtable();
	private String _jahr = null;
	private String _woche = null;
	private String _kg = null;
	
	public ActionSaveSABAWoche(BoxEdit box, IRequest request,
			String slotID, String nextPage) {
		
		super(box,request,slotID,nextPage);
		_box = box;
		
	}
	public void performX() throws XMetaException, XDataTypeException,
			XSecurityException {
		IRequest request = getRequest();
		BOContext boContext = (BOContext)request.getContext();
		MetaObject moSABAWoche = MetaObjectLookup.getInstance().getMetaObject("SABAWoche");
		
		BLTransaction trans = BLTransaction.startTransaction(boContext);
		
		try {
			long oid = BoxEdit.getSABAWeek(_jahr,_woche,_kg,boContext);
			BOObject boSABAWoche = trans.getObject(moSABAWoche,oid,false);
			
			if (boSABAWoche == null) {
				Log.logSystemAlarm("FATAL ERROR: Woche nicht gefunden in perform() von ActionSaveSABAWoche");
				return;
			}
			Enumeration keyEnum = _values.keys();
			while (keyEnum.hasMoreElements()) {
				String field = (String)keyEnum.nextElement();
				String value = (String)_values.get(field);
				if (EParseUtilities.getSubstring(field,1)!=null) {
					BOObject boRefObject = boSABAWoche.getReferencedObject(EParseUtilities.getSubstring(field,0),boContext);
					boRefObject.setAttribute(EParseUtilities.getSubstring(field,1),value,boContext,null);
				} else { 
					boSABAWoche.setAttribute(field,value,boContext,null);
				}
			}
			boSABAWoche.setAttribute("komplett","true",boContext,null);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		try {
			trans.commit();
		}
		catch (XBOConcurrencyConflict e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CacheSABAWochen.getInstance(boContext).clearCachedWeek(_woche,_jahr);
		
	}
	
	public boolean validate() {
		if (!super.validate()) return false;
		IRequest request = getRequest();
		IParameters params = request.getParameters();
		BOContext boContext = (BOContext)request.getContext();
		
		
		_jahr = params.getParameter(getInfoKey()+"_SABAWoche_Jahr");
		if (_jahr == null) addPublisherError("Das Jahr muss mit '"+getInfoKey()+"_SABAWoche_Jahr' gesetzt werden");
		_woche = params.getParameter(getInfoKey()+"_SABAWoche_Woche");
		if (_woche == null) addPublisherError("Die Woche muss mit '"+getInfoKey()+"_SABAWoche_Woche' gesetzt werden");
		_kg = params.getParameter(getInfoKey()+"_SABAWoche_KG");
		if (_kg == null) addPublisherError("Die Kirchgemeinde muss mit '"+getInfoKey()+"_SABAWoche_KG' gesetzt werden");
		
		boolean validation = true;
		String validationString = params.getParameter(getInfoKey()+"_saveSABAWoche_validation");
		if ((validationString!=null) && (validationString.equalsIgnoreCase("false"))) {
			validation = false;
		}
		int gesperrteVierteltage = 0;
		
		if (_kg==null || _woche==null || _jahr==null) return false;
		
		for (int tag=0;tag<5;tag++) {
			for (int vierteltag=0;vierteltag<4;vierteltag++) {
				String field = "OID"+AgendaWoche.TAGE[tag]+AgendaWoche.VIERTELTAGE[vierteltag]+"_OIDSabaPers";
				String paramValue = params.getParameter(getInfoKey()+"_SABAWoche_"+field);
				if (paramValue!=null && !paramValue.equals("")) {
					if (!paramValue.equalsIgnoreCase("-1")) {
						try {
							if (!CacheSABAWochen.getInstance(boContext).isBestatterVerfuegbar(_kg,tag,vierteltag,_woche,_jahr,paramValue)) {
								IKirchgemeinde kirch = CacheSABAWochen.getInstance(boContext).getBuchendeKirchgemeinde(paramValue,tag,vierteltag,_woche,_jahr);
								
								addUserError("Der Pfarrer mit OID = "+paramValue+" ist am "+AgendaWoche.TAGE[tag]+" "+AgendaWoche.VIERTELTAGE[vierteltag]+" nicht verf�gbar. Bereits gebucht in KG '"+kirch.getName()+"'");
							} else {
								try {
									
									MetaObject moSABAPerson = MetaObjectLookup.getInstance().getMetaObject("SABAPerson");
									BLTransaction trans = BLTransaction.startTransaction(boContext);
									BOObject boSABAPerson = trans.getObject(moSABAPerson,Long.parseLong(paramValue), false);
									if (boSABAPerson == null) {
										addPublisherError("Person mit OID = "+paramValue+" nicht gefunden");
										return false;
									}
									if (boSABAPerson.getAttribute("gesperrt",boContext).equalsIgnoreCase("true")) {
										gesperrteVierteltage++;
									}
									
									
									_values.put(field,paramValue);
								} catch (Exception e) {
									addPublisherError("Exception during validation");
									return false;
								}
							}
						} catch (CacheException e) {
							Log.logSystemAlarm("ERROR while accessing cache: "+e.getMessage());
							return false;
						}
						
					} else {
						addUserError("Der Vierteltag "+field+" ist nicht ausgef�llt");
					}
				}
				
				field = "OID"+AgendaWoche.TAGE[tag]+AgendaWoche.VIERTELTAGE[vierteltag]+"_OIDBemerkung";
				paramValue = params.getParameter(getInfoKey()+"_SABAWoche_"+field);
				if (paramValue!=null) _values.put(field,paramValue);
				
				field = "OID"+AgendaWoche.TAGE[tag]+AgendaWoche.VIERTELTAGE[vierteltag]+"_Bemerkung";
				paramValue = params.getParameter(getInfoKey()+"_SABAWoche_"+field);
				if (paramValue!=null) _values.put(field,paramValue);
				
				
				
			}
		}
		
		if (validation && (gesperrteVierteltage>4)) {
			addUserError("Es sind mehr als 4 Vierteltage gesperrt (n�mlich "+gesperrteVierteltage+")");
			request.getParameters().setParameter(getInfoKey()+"_saveSABAWoche_valGesperrt",""+gesperrteVierteltage);
		}
		
		if (!this.hasErrorMessages()) return true;
		this.setResultPagePath(params.getParameter("url"));
		
		return false;
	}
	
	
	
}
