package ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.BOValidationException;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.system.security.XSecurityException;

public class BOSchuelerInfo extends BOObject {

	public static final String CLASS_NAME = "SchuelerInfo";
	
	public BOSchuelerInfo(BLTransaction arg0, long arg1) {
		super(arg0, arg1);
	}
	
	@Override
	public void validate(BOContext context) throws BOValidationException, XMetaModelQueryException, XSecurityException {
		// create history object as necessary
		if (!isNew() && !isDeleted() && isLastEntry(context) && isChanged()) {
			try {
				BOSchuelerInfo boHistory = asHistoryObject(this);
				boHistory.setValue("lastEntry", false, context);
			} catch (XMetaException e) {
				throw new RuntimeException(e);
			}
		}
		BOSchulKlasse schulKlasse = getSchulKlasse(context);
		if (schulKlasse != null) {
			BOSchulStufe schulStufe = schulKlasse.getSchulStufe(context);
			if (schulStufe != null) {
				updateRPGStufe(schulStufe, context);
			}
		}
		super.validate(context);
	}
	
	public void updateRPGStufe(BOSchulStufe schulStufe, BOContext context) throws XMetaModelNotFoundException, XSecurityException {
		Integer offset = (Integer) getValue("rpgSchuleOffset", context);
		if (offset == null) {
			offset = 0;
			setValue("rpgSchuleOffset", 0, context);
		}
		int stufe = (Integer) schulStufe.getValue("stufe", context);
		setValue("rpgStufe", stufe + offset, context);
	}
	
	private boolean isLastEntry(BOContext context) throws XMetaModelNotFoundException, XSecurityException {
		return Boolean.TRUE.equals(getValue("lastEntry", context));
	}
	
	public BOSchulKlasse getSchulKlasse(BOContext context) throws XMetaModelQueryException, XSecurityException {
		return (BOSchulKlasse) getReferencedObject("aktSchulklasse", context);
	}
	
	/**
	 * Acquires a new OID for the specified bo and loads the BO with previous OID into
	 * the trx of the bo.  
	 * @param bo 
	 * @return the bo matching the oid of the specified bo with it's current state in the database
	 */
	private static <T extends BOObject> T asHistoryObject(T bo) {
		long prevOID = bo.getOIDValue();
		try {
			// set oid of specified object using as-if-new mechanics
			bo.setOID(0);
			bo.setNew(true);
			bo.setOID();
			
			@SuppressWarnings("unchecked")
			T boHistory = (T) bo.getTransaction().getObject(bo.getMetaObject(), prevOID);
			return boHistory;
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
