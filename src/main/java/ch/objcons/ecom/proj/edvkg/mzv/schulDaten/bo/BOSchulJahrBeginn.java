package ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.server.Request;

public class BOSchulJahrBeginn extends BOObject {

	public static final String CLASS_NAME = "SchulJahrBeginn";
	
	public BOSchulJahrBeginn(BLTransaction arg0, long arg1) {
		super(arg0, arg1);
	}

	public String getJahr() throws XMetaModelNotFoundException, XSecurityException {
		BOContext context = (BOContext) Request.getCurrentOrDummy().getContext();
		Date per = (Date) getValue("per", context);
		Calendar cal = Calendar.getInstance();
		cal.setTime(per);
		return "" + cal.get(Calendar.YEAR);
	}
	
	public static BOSchulJahrBeginn getAktSchulJahr(IRequest request) {
		try {
			@SuppressWarnings("unchecked")
			List<BOSchulJahrBeginn> boSchulJahrBeginn = BoxalinoUtilities.executeQuery(CLASS_NAME, "", CLASS_NAME + "_per_desc", null, request);
			if (boSchulJahrBeginn.isEmpty()) return null;
			
			return boSchulJahrBeginn.get(0);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
