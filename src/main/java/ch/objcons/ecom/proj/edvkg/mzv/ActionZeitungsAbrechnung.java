package ch.objcons.ecom.proj.edvkg.mzv;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.ecom.api.EContent;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.adapters.EActionAdapter2;
import ch.objcons.ecom.bom.BOFileUpload;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.XMetaException;

/*
 * makes the export for the Zeitung
 *
 */
public class ActionZeitungsAbrechnung extends EActionAdapter2 {

	private long _aid;
	private long _fileOID;
	// private IFileStorageEntry _file;
	private FileInputStream outFile;
	boolean _iskath = false;
	private Hashtable _duplicateFinder = new Hashtable();
	private int _numOfDuplicates = 0;
	private static final boolean _showPostsperre = true;
	
	public ActionZeitungsAbrechnung(ch.objcons.ecom.proj.edvkg.mzv.BoxMZV box, IRequest request,
		String slotID, String nextPage, boolean kath) {
		super (box, request, slotID, nextPage);
	    _iskath = kath;
	}
	private void fillKath(OutputStream outStream)
	{
		Connection conn = null ;
		ResultSet queryResult = null;
		// my Strings
		String query;
		StringBuffer oneLine;
		String zip, geschlecht, anrede, titel, kgnr, vorname, name, strasse, hausnr;
		String plz, ort, postzustell, adrZusatz, postfach, anz, codeZeitung, betrag;
		String plzPostfach, zeikg, postsperre;
		Object myObj;

		try {
			String kg = getRequest().getParameters().getParameter("client_request_kg");
			
			conn = DBConnectionPool.instance().getConnection();

			// mysql query :
			query = " select "+
					"pers.oid, pers.bx_CodeGeschlecht, pers.bx_ZeitungAnrede, pers.bx_zusAnrede, titel.bx_name, zei.bx_OIDKirchgemeinde, "+
					"pers.bx_ZeitungVorname, pers.bx_ZusVorname, pers.bx_rufname, "+
					"pers.bx_ZeitungNachname, pers.bx_ZusName, pers.bx_name, "+
					"pers.bx_ZusStrassenName, zusstr.bx_name, pers.bx_Strassekg, str.bx_name, "+
					"pers.bx_ZusHausNr, pers.bx_HausNr, "+
					"pers.bx_ZusPLZOrt, pers.bx_PLZOrt, "+
					"pers.bx_ZusOrt,  pers.bx_Ort, "+
					"zushaus.bx_Postzustellbezirk, haus.bx_Postzustellbezirk, "+
					"pers.bx_zusAdrZusatz, bx_AdrZusatz, "+
					"pers.bx_zusPostfach, bx_Postfach, "+
		            "pers.bx_zusPLZPostfach, bx_PLZPostfach, " +
					"zei.bx_Anzahl, zei.bx_Betrag, zei.bx_oidkirchgemeinde, "+
					"pers.bx_Postsperre "+
				"FROM (bx_person as pers, bx_zeitung as zei, bx_kirchgemeinde as kg )"+
				"LEFT JOIN bx_codehaus as haus on "+
					 "pers.bx_OIDCodeStrasse = haus.bx_OIDStrasse AND "+
				  "pers.bx_HausNr = haus.bx_HausNr "+
				"LEFT JOIN bx_codehaus as zushaus on "+
					"pers.bx_ZusOIDStrasse = zushaus.bx_OIDStrasse AND "+
					"pers.bx_ZusHausNr = zushaus.bx_HausNr "+
				"LEFT JOIN bx_codestrasse as str on "+
					"pers.bx_OIDCodeStrasse = str.oid "+
				"LEFT JOIN bx_codestrasse as zusstr on "+
					"pers.bx_ZusOIDStrasse = zusstr.oid "+
		        "LEFT JOIN bx_codetitel as titel ON "+
			        "pers.bx_OIDCodeTitel = titel.OID " +
				"WHERE ";
			if ((kg != null) && (kg.trim().length() > 0)) {
				try {
					int kgi = Integer.parseInt(kg);
					if (kgi < 1000) {
						query += " zei.bx_oidkirchgemeinde = " + kg + " AND ";
					}
				}
				catch (NumberFormatException x) {
				}
			}
					query += "zei.bx_PersonListZeitungOID = pers.oid AND "+
					"zei.bx_oidkirchgemeinde = kg.oid AND "+
					"kg.bx_konfession = 'RK' AND "+
		            "( pers.bx_zeitungverzicht = 'n' OR "+
		            "  pers.bx_zeitungverzicht is null ) AND "+
		            "pers.bx_statusperson = 'j' AND " + 
		            "zei.bx_Betrag > 0.0 " +
			" ORDER BY zei.bx_oidkirchgemeinde, zei.bx_Betrag";

			long logID = 0;
		    if (ch.objcons.ecom.query.Config.getInstance().showQueryString()) {
				logID = ch.objcons.log.Log.logDesignerInfo("Query: ActionExport.fillKath: " + query);
				ch.objcons.log.Log.flushDesigner();
			}

			Statement querySTM = conn.createStatement();
			queryResult =  querySTM.executeQuery(query);

			if (ch.objcons.ecom.query.Config.getInstance().getSQLTraceLevel() > 0) {
				ch.objcons.log.Log.logDesignerInfoPS(logID, "");
			}
			
			char sepChar = ';';

			String header = "ZIP;Titel;Vorname;Name;Zusatz;Strasse;HausNr;Postfach;PLZ;Ort;Zustell;Anzahl;Betrag;KG";
			if(_showPostsperre){
				header += ";Postsperre"; 
			}
			header += "\r\n";

			outStream.write(header.getBytes());
			
			while (queryResult.next()) {
				myObj = queryResult.getObject(1);
				zip = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(2);
				geschlecht = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(3);
				if (myObj == null) myObj = queryResult.getObject(4);
				if (myObj == null) myObj = queryResult.getObject(5);
				if (myObj == null) {
					if (geschlecht.trim().equals("M")) {
						myObj = "Herr";
					}
					else if (geschlecht.trim().equals("W")) {
						myObj = "Frau";
					}
					else {
					  myObj = "An";
					}
				}
				titel = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(6);
				kgnr = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(7);
				if (myObj == null) myObj = queryResult.getObject(8);
				if (myObj == null) myObj = queryResult.getObject(9);
				vorname = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(10);
				if (myObj == null) myObj = queryResult.getObject(11);
				if (myObj == null) myObj = queryResult.getObject(12);
				name = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(13);
				if (myObj == null) myObj = queryResult.getObject(14);
				if (myObj == null) myObj = queryResult.getObject(15);
				if (myObj == null) myObj = queryResult.getObject(16);
				strasse = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(17);
				if (myObj == null) myObj = queryResult.getObject(18);
				hausnr = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(19);
				if (myObj == null) myObj = queryResult.getObject(20);
				plz = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(21);
				if (myObj == null) myObj = queryResult.getObject(22);
				ort = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(23);
				if (myObj == null) myObj = queryResult.getObject(24);
				postzustell = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(25);
				if (myObj == null) myObj = queryResult.getObject(26);
				adrZusatz = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(27);
				if (myObj == null) myObj = queryResult.getObject(28);
				postfach = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(29);
				if (myObj == null) myObj = queryResult.getObject(30);
				plzPostfach = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(31);
				anz = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(32);
				betrag = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(33);
				zeikg = (myObj == null)?"":myObj.toString();
				if(_showPostsperre){
					myObj = queryResult.getObject(34);
					postsperre = (myObj == null)?"":myObj.toString();
					postsperre = postsperre.equals("j") ? "ja" : "nein";
				}
//				myObj = queryResult.getObject(33);
//				codeZeitung = (myObj == null)?" ":myObj.toString();

				oneLine = new StringBuffer(500);
				oneLine.append(zip).append(sepChar);
				oneLine.append(titel).append(sepChar);
				oneLine.append(vorname).append(sepChar);
				oneLine.append(name).append(sepChar);
				oneLine.append(adrZusatz).append(sepChar);
				oneLine.append(strasse).append(sepChar);
				oneLine.append(hausnr).append(sepChar);
				oneLine.append(postfach).append(sepChar);
				if ( (postfach.trim().length() > 0) && (plzPostfach.trim().length() > 0) ) {
					plz=plzPostfach;
				}
				oneLine.append(plz).append(sepChar);
				oneLine.append(ort).append(sepChar);
				oneLine.append(postzustell).append(sepChar); // postzustell
//				oneLine.append(codeZeitung.substring(1, 3));// objekt
//				oneLine.append(codeZeitung.substring(3));// subobjekt
				oneLine.append(anz).append(sepChar);
				oneLine.append(betrag).append(sepChar);
				oneLine.append(zeikg);
//				oneLine.append(codeZeitung).append(sepChar);
				if(_showPostsperre){
					oneLine.append(sepChar).append(postsperre);
				}
				oneLine.append("\r\n");
				if (_duplicateFinder.get(oneLine.toString()) == null) {
					_duplicateFinder.put(oneLine.toString(), new Long(0));
					outStream.write(oneLine.toString().getBytes());
				}
				else {
					_numOfDuplicates++;
				}
			}
			outStream.close();

		}
		catch (SQLException e) {
				ch.objcons.log.Log.logSystemAlarm("SQL Exception beim generieren der Zeitung", e);
		}
		catch (IOException e) {
				ch.objcons.log.Log.logSystemAlarm("IO Exception beim generieren der Zeitung", e);
		}
		finally {
			try {
				if (queryResult != null) {
					queryResult.close();
				}
				if (conn != null) {
					DBConnectionPool.instance().ungetConnection(conn);
				}
			}
			catch (Exception e) {
				ch.objcons.log.Log.logSystemAlarm("Unexpected Exception", e);
			}
		}
	}
	private String fixWide(String text, int length) throws UnsupportedEncodingException
	{
		if (text == null) {
			text = "";
		}
		text = new String(text.getBytes(), "iso-8859-1");
		if (text.length() != length) {
	        if (text.length() > length) {
			    text = text.substring(0,length);
		    }
		    else {
			    char [] myArr = new char[length-text.length()];
				for (int i=0;i<myArr.length;i++) {
				    myArr[i] = ' ';
				}
				text = (new StringBuffer(text)).append(myArr).toString();
		    }
		}
		return text;
	}
	private String fixWideNull(String text, int length) throws UnsupportedEncodingException
		{
			if (text == null) {
				text = "";
			}
			text = new String(text.getBytes(), "iso-8859-1");
			if (text.length() != length) {
				if (text.length() > length) {
					text = text.substring(0,length);
				}
				else {
					char [] myArr = new char[length-text.length()];
					for (int i=0;i<myArr.length;i++) {
						myArr[i] = '0';
					}
					text = (new StringBuffer(new String(myArr))).append(text).toString();
				}
			}
			return text;
	}
	public void performX () throws XMetaException, XDataTypeException {
		EContent result;
		File outFile = null;
		FileInputStream inStream;
		OutputStream outStream;

		try {
			outFile = new File(BOFileUpload.Config.getInstance().getTempBaseDir(), "zeitungsrechnung.txt");
			if (outFile.exists()) {
				outFile.delete();
			}
			outFile.createNewFile();
			outStream = new BufferedOutputStream(new FileOutputStream(outFile));

			this.fillKath(outStream);

			// open the stream for reading
	    	inStream = new FileInputStream(outFile);

			// content type zur�ckgeben

			if (_iskath) {
				result = new EContent("kathZeitungen.csv", "csv" , inStream );
			}
			else {
				result = new EContent("refZeitungen.txt", "txt" , inStream );
			}

			this.setContent(result);
		}
		catch (FileNotFoundException e) {
			    ch.objcons.log.Log.logSystemAlarm("File not Found Exception beim generieren der Zeitung", e);
		}
		catch (IOException e) {
			    ch.objcons.log.Log.logSystemAlarm("IO Exception beim generieren der Zeitung", e);
		}
	}
}
