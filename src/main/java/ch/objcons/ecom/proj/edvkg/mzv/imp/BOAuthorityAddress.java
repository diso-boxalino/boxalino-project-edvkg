package ch.objcons.ecom.proj.edvkg.mzv.imp;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOObject;

/**
 * Created by Lutz Bliska on 30.06.15.
 */
public class BOAuthorityAddress extends BOObject {

    public static final String CLASS_NAME = "AuthorityAddress";

    public BOAuthorityAddress(BLTransaction trans, long oid){
        super(trans, oid);
        //getRecordedAttributesInclude().add("OIDCODESTRASSE");
        //getRecordedAttributesInclude().add("OIDCODEHAUS");
        //getRecordedAttributesInclude().add("StreetNo");
        //getRecordedAttributesInclude().add("ValidFrom");
        //getRecordedAttributesInclude().add("RoomNumber");
        //getRecordedAttributesInclude().add("Status");
    }


}
