package ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Contains information concerning the SchulDatenImport CSV-Format, such as which
 * column maps to which index.
 */
public class CSVFormatInfo {

	public static final String 
	// Schueler Basis
	ZIP_NR = "ZIP-Nr", 
	S_VORNAME = "S_Vorname",
	S_NAME = "S_Name", 
	S_GESCHLECHT = "S_geschl.",
	S_GEBDAT = "S_gebdat", 
	S_STRNR = "S_Str-Nr",
	S_STRASSE = "S_Strasse", 
	S_HAUSNR = "S_Hausnr",
	S_HAUSNRZUS = "S_Hausnrzus", 
	S_ADRESSZUSATZ = "S_Adresszus.",
	S_PLZ = "S_PLZ", 
	S_ORT = "S_Ort",
	S_REL = "S_Rel.", 
	ERSTSPRACHE = "Erstsprache",
	// Klasse
	KLASSENSTUFE = "Klassenstufe", 
	KLASSEN_ID = "Klassen-ID",
	ANREDE_LP = "Anrede LP", 
	VORNAME_LP = "Vorname LP",
	NACHNAME_LP = "Nachname LP", 
	SK_ID = "SK-ID",
	// Schulhaus
	Schulkreis = "Schulkreis",
	SH_ID = "SH-ID",
	ADRESSTYP = "Adresstyp",
	SH_NAME = "Name SH", 
	SH_TELEFON = "Telefon SH",
	SH_STR_NR = "SH_Str-Nr",
	SH_STRASSE = "SH_Strasse", 
	SH_HAUSNUMMER = "SH_Hausnummer",
	SH_HAUSNUMMER_ZUSATZ = "SH_Hausnummer Zusatz",
	SH_ADRESSZUSATZ = "SH_Adresszusatz", 
	SH_PLZ = "SH_Plz",
	SH_ORT = "SH_Ort",
	// Mutter
	ZIP_MUTTER = "ZIP Mutter",
	M_VORNAME = "M_Vorname", 
	M_NAME = "M_Name",
	M_GESCHLECHT = "M_geschl.", 
	M_STRNR = "M_Str-Nr",
	M_STRASSE = "M_Strasse", 
	M_HAUSNR = "M_Hausnr",
	M_HAUSNRZUS = "M_Hausnrzus", 
	M_ADRESSZUSATZ = "M_Adresszusatz",
	M_PLZ = "M_PLZ",
	M_ORT = "M_Ort",
	M_REL = "M_Rel.", 
	// Vater
	ZIP_VATER = "ZIP Vater", 
	V_VORNAME = "V_Vorname", 
	V_NAME = "V_Name", 
	V_GESCHLECHT = "V_geschl.", 
	V_STRNR = "V_Str-Nr", 
	V_STRASSE = "V_Strasse",
	V_HAUSNR = "V_Hausnr", 
	V_HAUSNRZUS = "V_Hausnrzus",
	V_ADRESSZUSATZ = "V_Adresszusatz", 
	V_PLZ = "V_PLZ",
	V_ORT = "V_Ort",
	V_REL = "V_Rel.";
	
	/**
	 * The order of the elements determines which column is which attribute.
	 */
	private static final String[] FIELDS = { ZIP_NR, S_VORNAME, S_NAME,
			S_GESCHLECHT, S_GEBDAT, S_STRNR, S_STRASSE, S_HAUSNR, S_HAUSNRZUS,
			S_ADRESSZUSATZ, S_PLZ, S_ORT, S_REL, ERSTSPRACHE, KLASSENSTUFE,
			KLASSEN_ID, ANREDE_LP, VORNAME_LP, NACHNAME_LP, SK_ID, Schulkreis,
			SH_ID, ADRESSTYP, SH_NAME, SH_TELEFON, SH_STR_NR, SH_STRASSE,
			SH_HAUSNUMMER, SH_HAUSNUMMER_ZUSATZ, SH_ADRESSZUSATZ, SH_PLZ,
			SH_ORT, ZIP_MUTTER, M_VORNAME, M_NAME, M_GESCHLECHT, M_STRNR,
			M_STRASSE, M_HAUSNR, M_HAUSNRZUS, M_ADRESSZUSATZ, M_PLZ, M_ORT,
			M_REL, ZIP_VATER, V_VORNAME, V_NAME, V_GESCHLECHT, V_STRNR,
			V_STRASSE, V_HAUSNR, V_HAUSNRZUS, V_ADRESSZUSATZ, V_PLZ, V_ORT,
			V_REL };
	
	/**
	 * Base 0 index mapping of fields.
	 */
	public static final Map<String, Integer> DEFAULT_MAPPING;
	
	static {
		Map<String, Integer> map = new LinkedHashMap<String, Integer>();
		for (int i = 0; i < FIELDS.length; i++) {
			map.put(FIELDS[i], i);
		}
		if (FIELDS.length != map.size()) {
			throw new RuntimeException("fields, map size mismatch (duplicate key?): " + Arrays.toString(FIELDS) + ", " + map);
		}
		DEFAULT_MAPPING = Collections.unmodifiableMap(map);
	}
	
}
