package ch.objcons.ecom.proj.edvkg.mzv.imp;

import ch.objcons.db.dbimport.XMappingException;
import ch.objcons.log.Log;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;


public class MapIndexEDVKG {

	static ILogger LOGGER = Logger.getLogger("ch.objcons.ecom.proj.edvkg.import");
	
	// allgemeinde Daten
	// public int _OID;
	public int _Heimatort;
	public int _Geburtsdatum;
	public int _Beruf;
	public int _AktivPassivStatus;
	public int _Passivdatum;
	public int _Sterbedatum;
	public int _KonfessionsDatum;
	public int _AufenthaltsartDatum;
	public int _MeldeadresseDatum;
//	public int _AdressSperreSicherheit;
	public int _CodeAdressSperre;
	public int _CodeAufenthaltsart;
	public int _CodeGeschlecht;

	// Adresse
	public int _Familienname;
	public int _Vorname;
	public int _Rufname;
	public int _Allianzname;
	public int _CodeVoranstellung;
	public int _CodeAnrede;
	public int _CodeTitel;
	public int _Postlagernd;
	public int _Postfach;
	// public int _PLZPostfach;
	public int _PLZPostfach;
	public int _Strassennummer;
	public int _Hausnummer;
	// public int _Ort;
	// public int _PLZOrt;
	// public int _PLZOrtEind;
	public int _Zusatzzeile;

	// Adresse Zustell
	public int _ZUS_Postlagernd;
	public int _ZUS_Postfach;
	// public int _ZusPLZPostfach;
	public int _ZUS_PLZPostfach;
	public int _ZUS_Strassennummer;
	// public int _ZusStrassenName;
	public int _ZUS_Hausnummer;
	// public int _ZusOrt;
	// public int _ZusPLZOrt;
	// public int _ZusPLZOrtEind;
	// public int _ZusOIDCodeOrtLand;
	public int _ZUS_Zusatzzeile;

	// Adresse Zuzug
	public int _ZuzDatum;
	public int _AAD_Postlagernd;
	public int _AAD_Postfach;
	// public int _ZuzPLZPostfach;
	public int _AAD_PLZPostfach;
	public int _AAD_Strassenname;
	public int _AAD_Hausnummer;
	public int _AAD_Ortsbezeichnung;
	// public int _ZuzPLZOrt;
	public int _AAD_PLZOrt;
	public int _AAD_CodeOrtLand;
	public int _AAD_Zusatzzeile;
	// public int _ZuzOIDKG;

	// Adresse Wegzug
	public int _WegDatum;
	// public int _WegPostlagernd;
	// public int _WegPostfach;
	// public int _WegPLZPostfach;
	// public int _WegPLZPostfachEind;
	// public int _WegStrassenName;
	// public int _WegHausNr;
	// public int _WegOrt;
	// public int _WegPLZOrt;
	// public int _WegPLZOrtEind;
	// public int _WegCodeOrtLand;
	// public int _WegAdrZusatz;
	// public int _WegOIDKG;

	// Verwandschaft
	public int _OIDCodeZivilstand;
	public int _ZivilstandsDatum;
	public int _OIDHaushalt;
	public int _OIDFamilie;
	public int _OIDVorstand;
	public int _OIDCodePartner;
	public int _OIDEhepartner;
	public int _OIDVater;
	public int _OIDMutter;
	public int _OIDVormund;

	// KGSpez
	public int _OIDKG;
	public int _OIDPfarrkreis;
	// public int _OIDMission;
	// public int _OIDKGVorstand;
	// public int _OIDKGPartner;
	// public int _OIDKGKind;
	// public int _OIDKGVorstandMission;
	// public int _OIDKGPartnerMission;
	// public int _OIDKGKindMission;
	public int _OIDKonfession;
	// public int _TelPrivat;
	// public int _TelGesch;
	// public int _TelMobile;
	// public int _TelFax;
	// public int _Email;
	// public int _ListeZeitung;
	// public int _OIDListOrgP;
	public int _CodeStimmrecht;
	// public int _AdresseAmt;

	// History
	public int _OIDPerson;
	// public int _Date;
	public int _Vorfallart;
	public int _Vorfalltyp;
	public int _DatVorfall;
	public int _DatVerarbeitung;
	// public int _OIDBenutzer;
	// public int _RecordStatus;

	// Stop Flag (nur Personenimport)
	public int _StopFlag;

	// Anforderungen
	public int _AnfLaufNummer;
	public int _LWMutiert;

	// Nur in Import 'Ohne Anrecht'
	public int _ohneAnrechtWeilID;
	
	// additional variables
	public String[] _nameList = new String[ImportRoulesEDVKG.MAX_COLUMS];
	// public MetaAttribute[] _metaType = new MetaAttribute[ImportRoulesEDVKG.MAX_COLUMS];

	private TableMappingEDVKG _tabMap;
	private boolean _hauptImport;

	// counter for the indexes that is not in the import file
	private int myIndex = ImportRoulesEDVKG.MAX_COLUMS / 2;


	public MapIndexEDVKG(TableMappingEDVKG tabMap, boolean hauptImport) throws XMappingException{
		_tabMap = tabMap;
		_hauptImport = hauptImport;

		if (hauptImport) {
		// allgemeinde Daten
			//_OID = load("OID");
			_Heimatort = load("OIDCodeOrtHeimat");
			_Geburtsdatum = load("Geburtsdatum");
			_Beruf = load("Beruf");
			_AktivPassivStatus = load("StatusPerson");
			_Passivdatum = load("PassivDatum");
			_Sterbedatum = load("Sterbedatum");
			_KonfessionsDatum = load("KonfessionsDatum");
			_AufenthaltsartDatum = load("AufenthaltsartDatum");
			_MeldeadresseDatum = load("MeldeadresseDatum");
	//		_AdressSperreSicherheit = load("AdressSperreSicherheit");
			_CodeAdressSperre = load("_CodeAdressSperre");
			_CodeAufenthaltsart = load("OIDCodeAufenthalt");
			_CodeGeschlecht = load("CodeGeschlecht");
	
		// Adresse
			_Familienname = load("Name");
			_Vorname = load("VornameLang");
			_Rufname = load("Vorname");
			_Allianzname = load("Allianzname");
			_CodeVoranstellung = load("OIDCodeVoranstellung");
			_CodeAnrede = load("OIDCodeAnrede");
			_CodeTitel = load("OIDCodeTitel");
			_Postlagernd = load("Postlagernd");
			_Postfach = load("Postfach");
			//_PLZPostfach = load("OIDPLZPostfach");
			_PLZPostfach = load("PLZPostfachEind");
			_Strassennummer = load("OIDCodeStrasse");
			_Hausnummer = load("HausNr");
			//_Ort = load("Ort");
			//_PLZOrt = load("PLZOrt");
			//_PLZOrtEind = load("PLZOrtEind");
			_Zusatzzeile = load("AdrZusatz");
	
		// Adresse Zustell
			_ZUS_Postlagernd = load("ZusPostlagernd");
			_ZUS_Postfach = load("ZusPostfach");
			//_ZusPLZPostfach = load("ZusPLZPostfach");
			_ZUS_PLZPostfach = load("ZusPLZPostfachEind");
			_ZUS_Strassennummer = load("ZusOIDStrasse");
			// _ZusStrassenName = load("ZusStrassenName");
			_ZUS_Hausnummer = load("ZusHausNr");
			// _ZusOrt = load("ZusOrt");
			//_ZusPLZOrt = load("ZusPLZOrt");
			//_ZusPLZOrtEind = load("ZusPLZOrtEind");
			//_ZusOIDCodeOrtLand = load("ZusOIDCodeOrtLand");
			_ZUS_Zusatzzeile = load("ZusAdrZusatz");
	
		// Adresse Zuzug
			_ZuzDatum = load("ZuzDatum");
			_AAD_Postlagernd = load("ZuzPostlagernd");
			_AAD_Postfach = load("ZuzPostfach");
			//_ZuzPLZPostfach = load("ZuzPLZPostfach");
			_AAD_PLZPostfach = load("ZuzPLZPostfachEind");
			_AAD_Strassenname = load("ZuzStrassenName");
			_AAD_Hausnummer = load("ZuzHausNr");
			_AAD_Ortsbezeichnung = load("ZuzOrt");
			//_ZuzPLZOrt = load("ZuzPLZOrt");
			_AAD_PLZOrt = load("ZuzPLZOrtEind");
			_AAD_CodeOrtLand = load("ZuzCodeOrtLand");
			_AAD_Zusatzzeile = load("ZuzAdrZusatz");
			//_ZuzOIDKG = load("ZuzOIDKG");
	
		// Adresse Wegzug
			_WegDatum = load("WegDatum");
			//_WegPostlagernd = load("WegPostlagernd");
			//_WegPostfach = load("WegPostfach");
			//_WegPLZPostfach = load("WegPLZPostfach");
			//_WegPLZPostfachEind = load("WegPLZPostfachEind");
			//_WegStrassenName = load("WegStrassenName");
			//_WegHausNr = load("WegHausNr");
			//_WegOrt = load("WegOrt");
			//_WegPLZOrt = load("WegPLZOrt");
			//_WegPLZOrtEind = load("WegPLZOrtEind");
			//_WegCodeOrtLand = load("WegCodeOrtLand");
			//_WegAdrZusatz = load("WegAdrZusatz");
			//_WegOIDKG = load("WegOIDKG");
	
		// Verwandschaft
			_OIDCodeZivilstand = load("OIDCodeZivilstand");
			_ZivilstandsDatum = load("ZivilstandsDatum");
			_OIDHaushalt = load("OIDHaushalt");
			//_OIDFamilie = load("OIDFamilie"); // wird nirgends verwendet
			_OIDVorstand = load("OIDVorstand");
			_OIDCodePartner = load("OIDCodePartner");
			_OIDEhepartner = load("OIDEhepartner");
			_OIDVater = load("OIDVater");
			_OIDMutter = load("OIDMutter");
			_OIDVormund = load("OIDVormund");
	
		// KGSpez
			_OIDKG = load("OIDKG");
			//_OIDMission = load("OIDMission");
			//_OIDKGVorstand = load("OIDKGVorstand");
			//_OIDKGPartner = load("OIDKGPartner");
			//_OIDKGKind = load("OIDKGKind");
			//_OIDKGVorstandMission = load("OIDKGVorstandMission");
			//_OIDKGPartnerMission = load("OIDKGPartnerMission");
			//_OIDKGKindMission = load("OIDKGKindMission");
			_OIDKonfession = load("OIDKonfession");
			_OIDPfarrkreis = load("OIDPfarrkreis");
			//_TelPrivat = load("TelPrivat");
			//_TelGesch = load("TelGesch");
			//_TelMobile = load("TelMobile");
			//_TelFax = load("TelFax");
			//_Email = load("Email");
			//_ListeZeitung = load("ListeZeitung");
			//_OIDListOrgP = load("OIDListOrgP");
			_CodeStimmrecht = load("CodeStimmrecht");
			//_AdresseAmt = load("AdresseAmt");
	
			// History
			// _Date = load("Date");
//			_Vorfallart = load("Vorfallart");
//			_Vorfalltyp = load("Vorfalltyp");
//			_DatVorfall = load("DatVorfall");
			// _OIDBenutzer = load("OIDBenutzer");
			//_RecordStatus = load("RecordStatus");
			_StopFlag = load("_StopFlag");
		} else {
			_ohneAnrechtWeilID = load("_CodeNichtLieferung");
		}
		_OIDPerson = load("OIDPerson");
		_AnfLaufNummer = load("_AnfLaufnummer");
		_LWMutiert = load("_LWMutiert");
		_DatVerarbeitung = load("DatVerarbeitung");
	}

	private int load (String name) throws XMappingException{
		int val = _tabMap.getAttrIndex(name);
		if (val < 0 || val >= ImportPersonen.MAX_COLUMS) {
			String msg = "Der Index fuer ["+name+"] konnte nicht gefunden werden, verwende eine interne";
			Log.logSystemAlarm(msg);
			LOGGER.error(msg);
			val = myIndex;
			_nameList[val] = name;
			// _metaType[val] = null;
			myIndex++;
		} else {
		    _nameList[val] = name;
		    // _metaType[val] = _tabMap.getAttrMapping(val).getMetaAttr();
		}
		return val;
	}


}

