/*
 * Created on 24.01.2005
 *
 * (c) by Boxalino
 */
package ch.objcons.ecom.proj.edvkg.wcache;

import java.util.Calendar;

import ch.objcons.ecom.bom.BOContext;

/**
 * @author Dominik Raymann
 */
public class AgendaWoche {
	protected String _woche = null;
	protected String _jahr = null;
	
	protected BOContext _boContext = null;
	
	public final static String TAGE[] = {"Mo","Di","Mi","Do","Fr"};
	public final static String VIERTELTAGE[] = {"Vor","Vor2","Nach","Nach2"};
	public final static int CALENDARDAYS[] = {Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY, Calendar.THURSDAY, Calendar.FRIDAY};
	
	protected AgendaWoche(String woche, String jahr, BOContext boContext) {
		_woche = woche;
		_jahr = jahr;
		_boContext = boContext;
	}
	
	public String getWoche() {
		return _woche;
	}
	
	public String getJahr() {
		return _jahr;
	}
}
