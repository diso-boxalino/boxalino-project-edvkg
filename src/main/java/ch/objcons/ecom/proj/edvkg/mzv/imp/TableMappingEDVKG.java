package ch.objcons.ecom.proj.edvkg.mzv.imp;

import static ch.objcons.ecom.proj.edvkg.mzv.Consts.Hist_AdressSperreSicherheitIndex;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;
import java.util.Vector;

import ch.objcons.db.dbimport.AttrMapping;
import ch.objcons.db.dbimport.IReader;
import ch.objcons.db.dbimport.LogFileWriter;
import ch.objcons.db.dbimport.RefAttrMapping;
import ch.objcons.db.dbimport.XImportException;
import ch.objcons.db.dbimport.XMappingException;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.bl.BLFactory;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.DataType;
import ch.objcons.ecom.meta.FeatureAttribute;
import ch.objcons.ecom.meta.MetaAttribute;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.MetaServiceManager;
import ch.objcons.ecom.meta.ReferenceAttribute;
import ch.objcons.ecom.meta.SimpleAttribute;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelChangeFailedException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.BOPersonHistory;
import ch.objcons.ecom.proj.edvkg.mzv.BoxMZV;
import ch.objcons.ecom.proj.edvkg.mzv.bva.BVASupport;
import ch.objcons.ecom.proj.edvkg.systeminfo.SystemInfo;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.log.Log;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

public class TableMappingEDVKG extends ch.objcons.db.dbimport.IndexedTableMapping {

	static ILogger LOGGER = Logger.getLogger("ch.objcons.ecom.proj.edvkg.import");

	/**
     * Performs the mapping between the records of the imported table and
     * the corresponding database objects.
     *
     * @param table the name of the table to import
     * @param service the name of the service the mapped table belongs to
     * @param object the name of the object the table is mapped to
     */
    public TableMappingEDVKG(String table, String service, String object, int numAttrs) throws XMetaException {
        super(table, service, object, numAttrs);
    }


    /**
    * mapps the imported tables records to the corresponding database objects
    *
    * @param reader the reader which provides for the tables records to import
    * @param context the context object
    */
	public void processRecords(IReader reader, BOContext context, Connection con, LogFileWriter logWriter)
			throws XImportException, XMappingException, XMetaException, XDataTypeException,
			IOException, XSecurityException, SQLException, XBOConcurrencyConflict
	{
		MetaObject metaObject = getMetaObject();
		String className = metaObject.getName();

		try {
			if (BoxMZV.TestMode && "ImportLog,PersonHistory".indexOf(className) == -1) {
				LOGGER.warn("DEBUGGING MODE ACTIVE: JUST IMPORT ImportLog,PersonHistory");
				return;
			}
			
			if (className.equals("ImportLog")) {
				this.processSTRIMP(reader, context, con);
			}
			else if (className.equals("PersonHistory")) {
				synchronized(PostImport.class) {
					// nicht hier hineinkommen wenn Postimport aktiv
					this.processPersonen(reader, context, con);
				}
			}
			else if (className.equals("CodeImport")) {
				this.processCodes(reader, context, con);
			}
			else if (className.equals("Kirchgemeinde")) {
				this.processKirchgemeinde(reader, context, con);
			}
			else if (className.equals("Pfarrkreis")) {
				this.processPfarrkreis(reader, context, con);
			}
			else if (className.equals("CodeHaus")) {
				this.processCodeHaus(reader, context, con);
			}
			else if (className.equals("CodeHausKG")) {
				this.processCodeHausKG(reader, context, con);
			}
			else {
				this.processDefault(reader, context, con);
			}
		} catch (Throwable t) {
			String msg = "Import failed while processing records (class " + className + ", hint: " + getTable() + ")!";
			ch.objcons.log.Log.logSystemAlarm(msg, t);
			LOGGER.fatal(msg, t);
			SystemInfo.handleInconsitency();
		}
	}

	/**
	 * starts all other imports
	 */
	public void processSTRIMP(IReader reader, BOContext context, Connection con)
			throws XImportException, XMappingException, XMetaException, XDataTypeException, IOException, XSecurityException, XBOConcurrencyConflict
	{
		Vector myImports;
		BOObject myObj;
		BLTransaction trans = BLTransaction.startTransaction(context);
		String query;
		String fileName;
		int oldNumber, newNumber;
		boolean toWait;
		DecimalFormat decForm = new DecimalFormat("000000");
		String lockFileName = null;
		File lockFile = null;
		FileWriter lockFileout = null;
		FileReader lockFilein = null;
		String titel;
		java.sql.Timestamp nextDate;

		/* set the own data format */
		DateFormat oldDateFormat = context.getDateFormat();
		context.setDateFormat(new SimpleDateFormat("yyyyMMdd"));

		IRequest request = context.getRequest();
		fileName = (String) request.getValue("import_importFileName");
		titel = (String) request.getValue("import_title");

		try {
		    // here we teh the fileName of the locking File
		    query = "select * from bx_importedvkg where bx_title = 'STRIMPend'";
		    myImports = BLManager.getResult (trans, query, true, context);
			if (myImports.size() != 1) {
				// here he have a problem
				throw new XImportException(0, "Der name STRIMPend darf nur einmal im Import verwendet werden");
			}
			myObj = (BOObject)myImports.elementAt(0);
			lockFileName = myObj.getAttribute("autoImportFile", context);

			if ("STRIMP".equals(titel)) {
				LOGGER.info("Starting STRIMP with titel: "+titel);
				// we are in the import of STRIMP
				// check if the lockFile is there
				lockFile = new File(lockFileName);
				if (lockFile.exists()) {
		    		// we habe an error
			        String msg = "Warning im Import: Das Lockfile "+lockFileName+" ist noch da!! Ist noch ein anderer Import am laufen??. Das Lockfile wird gel�scht";
					Log.logSystemAlarm(msg);
					LOGGER.error(msg);
					// throw new XImportException(0, "Das Lockfile "+lockFileName+" ist noch da!! Ist noch ein anderer Import am ");
					lockFile.delete();
				}

	    		query = "select max(OID) from bx_importlog";
		    	oldNumber = BLManager.getCount (query);

			    // now we import what he sent us
				this.processIt(reader, context, con, false /*clearTable*/);

	    		newNumber = BLManager.getCount (query);

	    		if (BoxImportEDVKG.mustIncrementLogNumberByOne()) {
		    		if (newNumber != oldNumber+1) {
			    		// we habe an error
				        String msg = "Fehler im Import: Laufnummer inkorrekt, alteLaufnummer="+oldNumber+", neueLaufnummer="+newNumber;
		    			Log.logSystemAlarm(msg);
		    			LOGGER.error(msg);
						throw new XImportException(0, "Ungueltige Laufnummer");
		    		}
	    		} else {
	    			// Kirchen-Web Import-Nummer muss lediglich gr�sser als letzte sein
	    			if (newNumber <= oldNumber) {
	    				String msg = "Fehler im Import: Laufnummer inkorrekt, alteLaufnummer="+oldNumber+", neueLaufnummer="+newNumber;
	    				Log.logSystemAlarm(msg);
	    				LOGGER.error(msg);
						throw new XImportException(0, "Ungueltige Laufnummer: Alte ist kleiner oder gleich der neuen");
	    			}
	    		}

				// now we write our File for the Import
				lockFile.createNewFile();
				lockFileout = new FileWriter(lockFile);
				lockFileout.write(decForm.format(newNumber)); // now we write the number
				// lockFileout.write(nextDate.toString());
				lockFileout.close();

				// now we lock for all that import files
	    		query = "select * from bx_importedvkg where bx_autoImport='j' and bx_title != 'STRIMP' and bx_title != 'STRIMPend'";
		        myImports = BLManager.getResult (trans, query, true, context);

		        Calendar cal = Calendar.getInstance();
		        cal.add(Calendar.MINUTE, 5);
		        nextDate = new java.sql.Timestamp(cal.getTimeInMillis());

		        // i just check all my nice imports and set the state to inactive
				for (int count=0; count<myImports.size(); count++) {
	    	        myObj = (BOObject)myImports.elementAt(count);
		    	    myObj.setAttribute("state", "inactive", context, null);
			    	fileName = myObj.getAttribute("autoImportFile", context);
				    // just use the first 6 char
					fileName = fileName.substring(0, fileName.length()-6)+decForm.format(newNumber);
					myObj.setAttribute("autoImportFile", fileName, context, null);
					myObj.setValue("startDate", nextDate, context);
		        }

				// now we commit this stuff
	    		trans.commit(con, true);

		        // activate my imports, new transaction so we read the objects again
		        myImports = BLManager.getResult (trans, query, true, context);
				for (int count=0; count<myImports.size(); count++) {
	    	           myObj = (BOObject)myImports.elementAt(count);
			        myObj.setAttribute("state", "active", context, null);
		        }

				// and now we activate the end
			    query = "select * from bx_importedvkg where bx_title = 'STRIMPend'";
	    	    myImports = BLManager.getResult (trans, query, true, context);
		    	if (myImports.size() != 1) {
			    	// here he have a problem
				    throw new XImportException(0, "Der name STRIMPend darf nur einmal im Import verwendet werden");
				}
	    		myObj = (BOObject)myImports.elementAt(0);
				myObj.setValue("startDate", nextDate, context);
				myObj.setAttribute("state", "active", context, null);

			    // now we commit this stuff
				trans.commit(con, true);

			}
			else {
				// check if the lockFile is there
				LOGGER.info("Starting STRIMPend with titel: "+titel);

				/* lockFile = new File("lockFileName");
				if (!lockFile.exists()) {
		    		// we habe an error
			        ch.objcons.log.Log.logSystemAlarm(
				    	"Fehler im Import: Das Lockfile "+lockFileName+" ist nicht da!! Ist noch ein anderer Import am laufen??");
					throw new XImportException(0, "Ungueltige Laufnummer");
				}

				// read the file
				lockFilein = new FileReader(lockFile);
				char[] cbuf = new char[6];
				lockFilein.read(cbuf);
				String oid = new String(cbuf);
				// check it the stuff is identical
				*/
		        Calendar cal = Calendar.getInstance();
		        cal.add(Calendar.MINUTE, 5);
		        nextDate = new java.sql.Timestamp(cal.getTimeInMillis());
//				nextDate = new java.sql.Timestamp(System.currentTimeMillis()+10000);
				// XXXX wait for DB update myObj.setValue("endDate", nextDate, context);

				// now we just delete the file
				// lockFile.delete();

				// we are in the import of the STRIMPend
			    query = "select * from bx_importedvkg where bx_title = 'STRIMP'";
	    	    myImports = BLManager.getResult (trans, query, true, context);
		    	if (myImports.size() != 1) {
			    	// here he have a problem
				    throw new XImportException(0, "Der name STRIMP darf nur einmal im Import verwendet werden");
				}
	    		myObj = (BOObject)myImports.elementAt(0);
				myObj.setValue("startDate", nextDate, context);
				myObj.setAttribute("state", "active", context, null);
				if (BoxMZV.TestMode) {
//					myObj.setAttribute("state", "done", context, null);
				}

			}

		    trans.commit(con, true);
		}
		catch (SQLException e) {
		    new XImportException(0, "SQLError in processSTRIM: "+e.getMessage());
		}
		finally {
			context.setDateFormat(oldDateFormat);
		}
	}

    /**
	    * mapps the imported tables records to the corresponding database objects
	    *
	    * @param reader the reader which provides for the tables records to import
	    * @param context the context object
	    */
		public void processPersonen(IReader reader, BOContext context, Connection con)
				throws XImportException, XMappingException, XMetaException, XDataTypeException, IOException, XSecurityException, XBOConcurrencyConflict
		{
			boolean hauptImport;
			if ("PersonenOhneAnrecht".equals(getTable())) {
				hauptImport = false;
			} else if ("PersonenMutationen".equals(getTable())) {
				hauptImport = true;
			} else {
				throw new XImportException("undefined data");
			}
			// handle every person entry induvidual
			// -> we get an history entry for every single change!!!!
			int transactionSize = 1;
		 	long lastTime = (new java.util.Date()).getTime();
			Vector<BOPersonHistory> newObjects = new Vector();
			BOPersonHistory actObj = null;
			java.sql.Timestamp actDate = new java.sql.Timestamp(System.currentTimeMillis());

			IRequest request = context.getRequest();

			Hashtable metaKeyAttrs = null;
			MetaObject metaObject = getMetaObject();
			BLTransaction trans = BLTransaction.startTransaction(context);

			Vector<String> record = null; // just one line of the inputfile

			/* read all the Mapping Variables consts */
			MapIndexEDVKG mapIndex = new MapIndexEDVKG(this, hauptImport);

			/* get the attribute mappings (key: attribute) */
			Hashtable attrMappings = getAttrMappings(false /* keysOnly */);

			/* get the default values */
			Hashtable defaultValues = getDefaultValues();

			long importId = BVASupport.currentImportId();
			/* create the Import Roules */
			ImportPersonen persImporter = new ImportPersonen(mapIndex, defaultValues, trans, context, _metaService, metaObject, this, !hauptImport, importId);

			/* get the key attributes meta attributes (key: attribute) */
			metaKeyAttrs = getMetaKeyAttrs();
			if (metaKeyAttrs == null || metaKeyAttrs.size() == 0) {
				throw new XMappingException("Key fields not found");
			}


			/* count line numbers for error and transaction handling */
			_lineNumber = 0;

			/* set the own data format */
			DateFormat oldDateFormat = context.getDateFormat();
			context.setDateFormat(new SimpleDateFormat("yyyyMMdd"));

			int totalImportiert = 0;
			try {
			
				int recordErrorCount = 0;
				/* read all the records */
				while (reader.hasMoreRecords()) {
	
					/* count line numbers for error handling */
					_lineNumber++;
	
					if ((_lineNumber % 1000) == 0) {
						long time = (new java.util.Date()).getTime();
						LOGGER.info("ProcessLine " + _lineNumber + " " + (time - lastTime));
						lastTime = time;
					}
	
					/* check if we have to commit the transaction */
					this.commitIfNeedet(_lineNumber, transactionSize, trans, attrMappings, context, con);
					try {
						/* get the next record */
						record = (Vector)(reader.nextRecord());
			
						/* sometimes there are some empty lines */ /* really ? */
						if (record.size() > 1) {
				
							actObj = persImporter.importPersonen(record, con);
							totalImportiert++;
						} 
					} catch (Exception e) {
						recordErrorCount++;
						if (actObj != null) {
							String eMessage = e.getMessage();
							String message = (eMessage == null ? "" : eMessage)  + " R: " + record;
							if (message.length() > 254) {
								message = message.substring(0, 254);
							}
							actObj.addMeldung(Meldungsvorlage.Imp_Error, message);
						}
						if (recordErrorCount < 100) {
							String msg = "Konnte Personen-Record nicht importieren (Zeile " + _lineNumber + "). Fahre mit n�chstem Record weiter.";
							Log.logSystemAlarm(msg, e);
							LOGGER.error(msg, e);
						} else {
							// jetzt ist das Fass voll
							throw new XMetaException("", "", "Personenimport abgebrochen da zu viele Fehler.");
						}
					}
				} // end while over all records

			} 
			finally {
				context.setDateFormat(oldDateFormat);
	        	/* commit whole table */
				this.setAllOID(trans, attrMappings, context, con);
//				this.setAllDupp(newObjects, trans, context);
				try {
					trans.commit(con, true);
				}
				catch (SQLException e) {
					String err = "Import error in the commit in final";
					ch.objcons.log.Log.logSystemAlarm(err, e);
					LOGGER.error(err, e);
				}
				catch (XBOConcurrencyConflict e) {
					String err = "Import error in the commit in final";
					ch.objcons.log.Log.logSystemAlarm(err, e);
					LOGGER.error(err, e);
				}
			}
			String msg = "Import (Personen " + (hauptImport ? "mit" : "ohne") + " Anrecht): the import is finished, " + totalImportiert + " persons imported, " + persImporter.getDatengleicheRecords() + " persons have no modifications";
			ch.objcons.log.Log.logSystemInfoWithMail(msg);
			LOGGER.info(msg);
		}
		
	    /**
		    * mapps the imported tables records to the corresponding database objects
		    *
		    * @param reader the reader which provides for the tables records to import
		    * @param context the context object
		    */
			public void processPersonBak(IReader reader, BOContext context, Connection con)
					throws XImportException, XMappingException, XMetaException, XDataTypeException, IOException, XSecurityException, XBOConcurrencyConflict
			{
				boolean update = false;
				String updatePolicy = null;
				String delQuery = null;
				Enumeration enumer = null;
				// handle every person entry induvidual
				// -> we get an history entry for every single change!!!!
				int transactionSize = 1;
			 	long lastTime = (new java.util.Date()).getTime();
				Vector newObjects = new Vector();
				BOObject actObj = null;
				java.sql.Timestamp actDate = new java.sql.Timestamp(System.currentTimeMillis());

				IRequest request = context.getRequest();

				Hashtable metaKeyAttrs = null;
				MetaObject metaObject = getMetaObject();
				BLTransaction trans = BLTransaction.startTransaction(context);

				Vector record = null; // just one line of the inputfile
				String vorfallArt;
				String vorfallTyp;

				/* read all the Mapping Variables consts */
				MapIndexEDVKG mapIndex = new MapIndexEDVKG(this, false);

				/* get the attribute mappings (key: attribute) */
				Hashtable attrMappings = getAttrMappings(false /* keysOnly */);

				/* get the default values */
				Hashtable defaultValues = getDefaultValues();

				/* create the Import Roules */
				ImportRoulesEDVKG impRoules = new ImportRoulesEDVKG(mapIndex, defaultValues, trans, context, _metaService, metaObject, this);

				/* get the key attributes meta attributes (key: attribute) */
				metaKeyAttrs = getMetaKeyAttrs();
				if (metaKeyAttrs == null || metaKeyAttrs.size() == 0) {
					throw new XMappingException("Key fields not found");
				}


				/* count line numbers for error and transaction handling */
				_lineNumber = 0;

				/* set the own data format */
				DateFormat oldDateFormat = context.getDateFormat();
				context.setDateFormat(new SimpleDateFormat("yyyyMMdd"));

				/* just that we don't forget it */
				// ch.objcons.log.Log.logSystemAlarm("Import: remove the Struth HACK !!!");

				try {
				
				int recordErrorCount = 0;
				/* read all the records */
				while (reader.hasMoreRecords()) {

					/* count line numbers for error handling */
					_lineNumber++;

					enumer = null;

					if ((_lineNumber % 1000) == 0) {
						long time = (new java.util.Date()).getTime();
						LOGGER.info("ProcessLine " + _lineNumber + " " + (time - lastTime));
						lastTime = time;
					}

					/* check if we have to commit the transaction */
					this.commitIfNeedet(_lineNumber, transactionSize, trans, attrMappings, context, con);
					try {
						/* get the next record */
						record = (Vector)(reader.nextRecord());
			
						/* sometimes there are some empty lines */
						if (record.size() > 1) {
			
						/* check the reason that we have to compute that junk */
						vorfallArt = ((String)(record.elementAt(mapIndex._Vorfallart))).trim();
						vorfallTyp = ((String)(record.elementAt(mapIndex._Vorfalltyp))).trim();
						/*
						String debug = ((String)record.elementAt(mapIndex._OIDPerson)).trim();
						if (debug.equals("100512141") || debug.equals("100734903") || debug.equals("100140510") || debug.equals("100127898") || debug.equals("100231579") || debug.equals("100418047") || debug.equals("100476488") || debug.equals("100721006")) {
							System.out.println("stop");
						}
						*/
						
						/* Struth HACK }}} */
						if ((vorfallArt.length() == 0) && (vorfallTyp.length() == 0)) {
							vorfallArt = "ERST";
							vorfallTyp = "E";
						}
			
						if (vorfallTyp.equalsIgnoreCase("E")) {
							/* erstanlieferung */
							if (vorfallArt.equalsIgnoreCase("ERST")) {
								/* starten der Erstanlieferung */
								actObj = impRoules.erstanlieferung(record);
							}
							else {
								this.errorEntry(vorfallTyp, vorfallArt, "Auf den Vorfalltyp 'E' muss die Vorfallart 'ERST' folgen.");
							}
						} else if (vorfallTyp.equalsIgnoreCase("M") || vorfallTyp.equalsIgnoreCase("K") ||
								vorfallTyp.equalsIgnoreCase("U") || vorfallTyp.equalsIgnoreCase("L")) {
							/* Mutation ore Korrektur */
							if (vorfallArt.equalsIgnoreCase("EERF") ||
								vorfallArt.equalsIgnoreCase("ERST")) {
								actObj = impRoules.erstanlieferung(record);
								newObjects.add(actObj);
							} else if (vorfallArt.equalsIgnoreCase("GEB")) {
								/* vorfall Geburt */
								actObj = impRoules.geburt(record);
								newObjects.add(actObj);
							} else if (vorfallArt.equalsIgnoreCase("KONF") ||
									  vorfallArt.equalsIgnoreCase("ZIV") ||
									  vorfallArt.equalsIgnoreCase("AUFA")) {
								/* vorfall Konfessionswechsel, Zivistandswechsel oder Aufenthaltsort */
								actObj = impRoules.konfZiviAuf(record, con);
								newObjects.add(actObj);
							} else if (vorfallArt.equalsIgnoreCase("NTYP") ||
									  vorfallArt.equalsIgnoreCase("VTYP") ||
									  vorfallArt.equalsIgnoreCase("PTYP") ||
									  vorfallArt.equalsIgnoreCase("GMUT") ||
									  vorfallArt.equalsIgnoreCase("HMUT") ||
									  vorfallArt.equalsIgnoreCase("SMUT") ||
									  vorfallArt.equalsIgnoreCase("EMUT") ||
									  vorfallArt.equalsIgnoreCase("ADMU") ||
									  vorfallArt.equalsIgnoreCase("AAMU") ||
									  vorfallArt.equalsIgnoreCase("PARM") ||
									  vorfallArt.equalsIgnoreCase("PHHZ") ||
									  vorfallArt.equalsIgnoreCase("NABE") ||
									  vorfallArt.equalsIgnoreCase("STPE") ||
									  vorfallArt.equalsIgnoreCase("SPER") ||
									  vorfallArt.equalsIgnoreCase("RGPE") ||
									  vorfallArt.equalsIgnoreCase("RGPL") ||
									  vorfallArt.equalsIgnoreCase("RCKR")) {
								/* standard Vorfall */
								actObj = impRoules.stdVorfall(record, con);
								newObjects.add(actObj);
							} else if (vorfallArt.equalsIgnoreCase("WEGZ") ||
									  vorfallArt.equalsIgnoreCase("TOD")) {
								/* vorfall Wegzug, Tod oder Rueckrapport */
								actObj = impRoules.wegTodRueck(record, con);
								newObjects.add(actObj);
							} else if (vorfallArt.equalsIgnoreCase("ZUZ")) {
								/* vorfall Zuzug */
								actObj = impRoules.zuzug(record);
								newObjects.add(actObj);
							} else if (vorfallArt.equalsIgnoreCase("UMZ")) {
								actObj = impRoules.umzug(record, con);
								newObjects.add(actObj);
							} else if (vorfallArt.equalsIgnoreCase("WZUZ")){
								/* vorfall Wiederzuzug */
								actObj = impRoules.wiederZuzug(record, con);
								newObjects.add(actObj);
							} else {
								this.errorEntry(vorfallTyp, vorfallArt, "Unbekannte Vorfallart");
							}
			
						} else {
							this.errorEntry(vorfallTyp, vorfallArt, "Unbekannter VorfallTyp");
						}
						
						// post process
						if (actObj != null) {
						    actObj.setValue("Date", actDate, context);
							actObj.setValue("User", "Stadt", context);
			//		        actObj.setValue("AdresseStadt", new Boolean(true), context);
			//				actObj.setValue("RecordStatus", "A", context);
							// check it the person is a vorstand
							Integer oid = (Integer)actObj.getValue("OIDPerson", context);
							// Achtung mh@20110304: zu diesem Zeitpunkt ist OIDFamilie noch nicht unbedingt gesetzt,
							// OIDVorstand ist aber bis zur Schni2 noch das Duplikat von OIDFamilie
							Integer vorstand = (Integer)actObj.getValue("OIDVorstand", context);
							if ( (vorstand != null) && (vorstand.equals(oid)) ) {
								actObj.setValue("Vorstand", new Boolean(true), context);
							}
							actObj.setValue("lastEntry", new Boolean(true), context);
							/* mh@20071008 The Sperre flag should already be set correctly in the history person */
							if ("S".equalsIgnoreCase((String)actObj.getValue("CodeAdressSperre", context))) {
								actObj.setValue(Hist_AdressSperreSicherheitIndex, new Boolean(true), context);
							}
						}
			
						} // end of if there are more then 1 elements
					} catch (Exception e) {
						recordErrorCount++;
						if (actObj != null) {
							ImportRoulesEDVKG.setRecordError('F', "Interner Fehler Personen-Record-Import", actObj, context);
						}
						if (recordErrorCount < 100) {
							String msg = "Konnte Personen-Record nicht importieren. Fahre mit n�chstem Record weiter.";
							Log.logSystemAlarm(msg, e);
							LOGGER.error(msg, e);
						} else {
							// jetzt ist das Fass voll
							throw new XMetaException("", "", "Personenimport abgebrochen da zu viele Fehler.");
						}
					}
				} // end while over all records

				} finally {
					String msg = "Import: the import is finished, the post processing will be the next";
					ch.objcons.log.Log.logSystemAlarm(msg);
					LOGGER.error(msg);
					context.setDateFormat(oldDateFormat);
		        	/* commit whole table */
					this.setAllOID(trans, attrMappings, context, con);
					this.setAllDupp(newObjects, trans, context);
					try {
						trans.commit(con, true);
					}
					catch (SQLException e) {
						String err = "Import error in the commit in final";
						ch.objcons.log.Log.logSystemAlarm(msg, e);
						LOGGER.error(msg, e);
					}
					catch (XBOConcurrencyConflict e) {
						String err = "Import error in the commit in final";
						ch.objcons.log.Log.logSystemAlarm(msg, e);
						LOGGER.error(msg, e);
					}
				}
			}

	/**
	 * just set the Objects to not valid if they are are duplicated
	 * 
	 * this idea is not bad, but it should be handled by the OIZ and it worked for the last few years. mh@20110913
	 */
	private void setAllDupp(Vector newObjects, BLTransaction trans, BOContext context)
		throws XMetaModelNotFoundException, XSecurityException, XMetaModelQueryException, XMetaException, XDataTypeException
	{
		BOObject actObj;
		HashMap myOIDs = new HashMap();
		Object oidPerson;
		long oid;
		Long oidObj;
		Long oldOID;
		BOObject oldMut;

		for (int count=0; count<newObjects.size(); count++) {
			actObj = (BOObject)newObjects.elementAt(count);
			if (actObj != null) {
			    oid = actObj.getOID();
			    oidPerson = actObj.getValue("OIDPerson", context);
			    oldOID = (Long)myOIDs.get(oidPerson);
			    if (oldOID != null) {
			        // get the Object of that oid
				    oldMut = trans.getObject(actObj.getMetaObject(), oldOID.longValue(), false/*isPartOf*/);
				    oldMut.setAttribute("RecordStatus", "D", context, null);
			    }
			    myOIDs.put(oidPerson, new Long(oid));
			}
		}

	}

	/**
	 * add the error to the error List in the database
	 */
	private void errorEntry(String vorfallTyp, String vorfallArt, String message) {
		// xxxx just add the error to the list
		String msg = "Fehler im Import: "+message+"  Vorfalltyp=<"+vorfallTyp+">, Vorfallart=<"+vorfallArt+"> ";
		Log.logSystemAlarm(msg);
		LOGGER.error(msg);
	}

	/**
    * mapps the imported tables records to the corresponding database objects
    *
    * @param reader the reader which provides for the tables records to import
    * @param context the context object
    */
	public void processCodes(IReader reader, BOContext context, Connection con)
			throws XImportException, XMappingException, XMetaException, XDataTypeException,
			IOException, XSecurityException, XBOConcurrencyConflict
	{

		BLTransaction trans =null;

		/* set the own data format */
		DateFormat oldDateFormat = context.getDateFormat();
		context.setDateFormat(new SimpleDateFormat("yyyyMMdd"));

		try {
			this.processIt(reader, context, con);

			trans = BLTransaction.startTransaction(context);
			this.activateCodes(trans, context);
		} finally {
			context.setDateFormat(oldDateFormat);
        		/* commit whole table */
			if (trans != null) {
				trans.commit();
			}
		}
		try {
			String query = "update bx_codezivilstand set bx_kurzname = 'Partnersch.aufg.(ger.)' where bx_code = 'AUGPG'";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, false);
			query = "update bx_codezivilstand set bx_kurzname = 'Partnerschaft (eingetr.)' where bx_code = 'EIP'";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, false);
			query = "update bx_codezivilstand set bx_kurzname = 'Partner.aufg.(v.scholl.)' where bx_code = 'AUGPU'";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, false);
			query = "update bx_codezivilstand set bx_kurzname = 'Partnersch.aufg.(Tod)' where bx_code = 'AUGPT'";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);
		} catch (XMetaModelQueryException e) {
			LOGGER.error("Error during update of Zivilstandcodes (Partnerschaften)", e);
		}
	
	}

    /**
    * mapps the imported tables records to the corresponding database objects
    *
    * @param reader the reader which provides for the tables records to import
    * @param context the context object
    */
	public void processKirchgemeinde(IReader reader, BOContext context, Connection con)
			throws XImportException, XMappingException, XMetaException, XDataTypeException,
			IOException, XSecurityException, XBOConcurrencyConflict
	{
		//CodeTableEDVKG konfCode = null;
		//new CodeTableEDVKG(_trans, _context, _metaService, "Konfession");
		String query = null;
		BLTransaction trans = null;

		/* set the own data format */
		DateFormat oldDateFormat = context.getDateFormat();
		context.setDateFormat(new SimpleDateFormat("yyyyMMdd"));

		try {
			this.processIt(reader, context, con);

			trans = BLTransaction.startTransaction(context);

			query = "update bx_kirchgemeinde kg set kg.bx_OIDKonfession = ("+
				" select konf.OID from bx_konfession konf where konf.bx_code=kg.bx_Konfession)";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);

			// just add the KG for the allg. Adressen
			query = "INSERT INTO bx_kirchgemeinde "+
				    " (OID, ArtOID, bx_Name, bx_OIDKonfession, bx_OIDKGTyp, bx_ZeitungsNr, "+
					" bx_Status, bx_Kurzname, bx_ID, bx_Konfession) "+
					" VALUES (1000,60,'allg Adressen',1,NULL,NULL,'j','ADR',NULL,'')";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);

			// just add the KG for the EDVKG
			query = "INSERT INTO bx_kirchgemeinde "+
				    " (OID, ArtOID, bx_Name, bx_OIDKonfession, bx_OIDKGTyp, bx_ZeitungsNr, "+
					" bx_Status, bx_Kurzname, bx_ID, bx_Konfession) "+
					" VALUES (1001,60,'EDVKG',1,NULL,NULL,'j','EDV',NULL,'')";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);

			// insert the KG for the MissionaItalia
			query = "INSERT INTO bx_kirchgemeinde "+
					" (OID, ArtOID, bx_Name, bx_OIDKonfession, bx_OIDKGTyp, bx_ZeitungsNr, "+
					" bx_Status, bx_Kurzname, bx_ID, bx_Konfession) "+
					" VALUES ("+PostImport.MISSION_ITALIA+",60,'Missione Cattolica Italiana',1,NULL,NULL,'j','MCI',NULL,'RK')";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);

			// insert the KG for the MissionaEspanol
			query = "INSERT INTO bx_kirchgemeinde "+
					" (OID, ArtOID, bx_Name, bx_OIDKonfession, bx_OIDKGTyp, bx_ZeitungsNr, "+
					" bx_Status, bx_Kurzname, bx_ID, bx_Konfession) "+
					" VALUES ("+PostImport.MISSION_ESPANOL+",60,'Missione Cattolica Espanol',1,NULL,NULL,'j','MCE',NULL,'RK')";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);

			// insert the Kath. Stadtverband mh@20060822
			query = "INSERT INTO bx_kirchgemeinde " +
					" (OID, ArtOID, bx_Name, bx_OIDKonfession, bx_OIDKGTyp, bx_ZeitungsNr, "+
					" bx_Status, bx_Kurzname, bx_ID, bx_Konfession) "+
					" VALUES (" + PostImport.STADTVERBAND_KAT+ ", 60, 'Kath. Stadtverband',1,NULL,NULL,'j','StK',NULL,'RK')";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);

			// insert the Ref. Stadtverband mh@20060822
			query = "INSERT INTO bx_kirchgemeinde " +
					" (OID, ArtOID, bx_Name, bx_OIDKonfession, bx_OIDKGTyp, bx_ZeitungsNr, "+
					" bx_Status, bx_Kurzname, bx_ID, bx_Konfession) "+
					" VALUES (" + PostImport.STADTVERBAND_REF+ ", 60, 'Ref. Stadtverband',2,NULL,NULL,'j','StR',NULL,'EV-REF')";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);
			
		} finally {
			context.setDateFormat(oldDateFormat);
			if (trans != null) {
				trans.commit();
			}
		}
	}

    /**
    * mapps the imported tables records to the corresponding database objects
    *
    * @param reader the reader which provides for the tables records to import
    * @param context the context object
    */
	public void processPfarrkreis(IReader reader, BOContext context, Connection con)
			throws XImportException, XMappingException, XMetaException, XDataTypeException,
			IOException, XSecurityException, SQLException, XBOConcurrencyConflict
	{
		/* set the own data format */
		DateFormat oldDateFormat = context.getDateFormat();
		context.setDateFormat(new SimpleDateFormat("yyyyMMdd"));

		try {
			// we use the uper process it, because here we have an update of the tables and not a set
			super.processRecords(reader, context, con, null);
		} finally {
			context.setDateFormat(oldDateFormat);
		}
	}

	public void processCodeHaus(IReader reader, BOContext context, Connection con)
			throws XImportException, XMappingException, XMetaException, XDataTypeException,
			IOException, XSecurityException, XBOConcurrencyConflict
	{
		String query = null;

		/* set the own data format */
		DateFormat oldDateFormat = context.getDateFormat();
		context.setDateFormat(new SimpleDateFormat("yyyyMMdd"));

		try {
			this.processIt(reader, context, con);
			/* Zeilen l�schen welche pro Strasse und Hausnummer mehrere Eintr�ge haben */
			query = "delete from tmpadr";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);
			query = "insert into tmpadr (persoid, oidcodestrasse, hausnr) select min(oid), bx_oidstrasse, bx_hausnr from bx_codehaus group by bx_oidstrasse, bx_hausnr having count(*) > 1";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);
			query = "delete ch from bx_codehaus ch inner join tmpadr t on ch.bx_oidstrasse = t.oidcodestrasse and ch.bx_hausnr = t.hausnr where ch.oid != t.persoid";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);

		} finally {
			context.setDateFormat(oldDateFormat);
		}
	}

	public void processCodeHausKG(IReader reader, BOContext context, Connection con)
				throws XImportException, XMappingException, XMetaException, XDataTypeException,
						IOException, XSecurityException, XBOConcurrencyConflict
	{
		String query = null;

		/* set the own data format */
		DateFormat oldDateFormat = context.getDateFormat();
		context.setDateFormat(new SimpleDateFormat("yyyyMMdd"));

		try {
			this.processIt(reader, context, con);
			/* Zeilen l�schen welche pro Strasse, Hausnummer und Kirchgemeinde mehrere Eintr�ge haben */
			query = "delete from tmpadr";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);
			query = "insert into tmpadr (persoid, oidcodestrasse, hausnr, konfession) select min(oid), bx_oidstrasse, bx_hausnr, bx_konfession from bx_codehauskg group by bx_oidstrasse, bx_hausnr, bx_konfession having count(*) > 1";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);
			query = "delete ch from bx_codehauskg ch inner join tmpadr t on ch.bx_oidstrasse = t.oidcodestrasse and ch.bx_hausnr = t.hausnr and ch.bx_konfession = t.konfession where ch.oid != t.persoid";
			LOGGER.info("Query = <"+query+">");
			BLManager.executeUpdate(con, query, true);

		} finally {
			context.setDateFormat(oldDateFormat);
		}
	}

	/**
    * mapps the imported tables records to the corresponding database objects
    *
    * @param reader the reader which provides for the tables records to import
    * @param context the context object
    */
	public void processDefault(IReader reader, BOContext context, Connection con)
			throws XImportException, XMappingException, XMetaException, XDataTypeException,
			IOException, XSecurityException, XBOConcurrencyConflict
	{
		/* set the own data format */
		DateFormat oldDateFormat = context.getDateFormat();
		context.setDateFormat(new SimpleDateFormat("yyyyMMdd"));

		try {
			this.processIt(reader, context, con);
		} finally {
			context.setDateFormat(oldDateFormat);
		}
	}

	/**
    * mapps the imported tables records to the corresponding database objects
    *
    * @param reader the reader which provides for the tables records to import
    * @param context the context object
    */
    protected void processIt(IReader reader, BOContext context, Connection con)
        throws XImportException, XMappingException, XMetaException, XDataTypeException,
		IOException, XSecurityException, XBOConcurrencyConflict
	{
	    this.processIt(reader, context, con, true);
	}

	/**
    * mapps the imported tables records to the corresponding database objects
    *
    * @param reader the reader which provides for the tables records to import
    * @param context the context object
	* @param if we have to clear the table bevore we import it
    */
    protected void processIt(IReader reader, BOContext context, Connection con, boolean clearTable)
        throws XImportException, XMappingException, XMetaException, XDataTypeException,
		IOException, XSecurityException, XBOConcurrencyConflict
    {
        String delQuery = null;

        IRequest request = context.getRequest();

        Hashtable metaKeyAttrs = null;
        Vector modifiedObjects = null;
        MetaObject metaObject = getMetaObject();
        BLTransaction trans = BLTransaction.startTransaction(context);
		int transactionSize = BoxImportEDVKG.getTransactionSize();

        /* get the attribute mappings (key: attribute) */
		Hashtable attrMappings = getAttrMappings(false /* keysOnly */);

        /* get the default values */
        Hashtable defaultValues = getDefaultValues();

        /* count line numbers for error handling */
        _lineNumber = 0;

		long lastTime = (new java.util.Date()).getTime();

		/* delete the whole table if we have something to import */
		if (reader.hasMoreRecords() && clearTable) {
			this.clearTable(context);
		}

		while (reader.hasMoreRecords()) {
        /* count line numbers for error handling */
		    _lineNumber++;

		    if ((_lineNumber % 1000) == 0) {
		    	long time = (new java.util.Date()).getTime();
			    LOGGER.info("ProcessLine " + _lineNumber + " " + (time - lastTime));
			    lastTime = time;
		    }

		    this.commitIfNeedet(_lineNumber, transactionSize, trans, attrMappings, context, con);

            /* get the next record */
			Object record = reader.nextRecord();

            /* get the values of the next record (key: attribute) */
            Hashtable values = getValues(record, false /* keysOnly */);

            /* get the corresponding object for the current record from the database
             * the object will be identified by the key field values of the record
             */
            BOObject boObject = null;
		    boObject = trans.getObject(metaObject, 0, false /* isPartOf */);

            Enumeration enumer = null;
            // set default values
            if (defaultValues != null) {
                enumer = defaultValues.keys();
                while (enumer.hasMoreElements()) {
                    String key = (String) enumer.nextElement();
										boObject.setAttribute(key, (String)defaultValues.get(key), context, null);
                }
            }

            /* set attribute values */
            enumer = values.keys();
            while (enumer.hasMoreElements()) {
                String attribute = (String) enumer.nextElement();
                AttrMapping attrMapp = (AttrMapping) attrMappings.get(attribute);

                if (attrMapp != null) {
                    String[] value = (String[]) values.get(attribute);
                    MetaAttribute metaAttr = attrMapp.getMetaAttr();
                    if (metaAttr instanceof FeatureAttribute) {
                        boObject.setAttribute(attribute, value[0], context, null);
                    }
                    else if (metaAttr instanceof SimpleAttribute) {
                        value[0] = preprocessValue((SimpleAttribute)metaAttr, value[0]);
                        boObject.setAttribute(attribute, value[0], context, null);
                    }
                    else {
                        // todo: error message?
                    }
                }
            }

            /* set references */
            if (_refAttrMapps != null) {
                enumer = _refAttrMapps.keys();
                while (enumer.hasMoreElements()) {
                    String refAttr = (String) enumer.nextElement();
                    RefAttrMapping refAttrMapp = (RefAttrMapping) _refAttrMapps.get(refAttr);
                    MetaObject metaRefObject = refAttrMapp.getMetaRefObject();
                    Hashtable refKeyAttrs = refAttrMapp.getRefKeyAttrs();
                    Hashtable metaRefKeyAttrs = refAttrMapp.getMetaRefKeyAttrs();
                    Hashtable refKeyValues = getRefKeyValues(refKeyAttrs, values, defaultValues);
                    BOObject refObject = this.getObject(metaRefObject, metaRefKeyAttrs, refKeyValues, null /*trans*/, context, con);
                    boObject.setReferencedObject(refAttr, refObject);
                }
            }
        }

        // set OID's of all new objects
		this.setAllOID(trans, attrMappings, context, con);

        /* commit whole table */
        try {
			trans.commit(con, true);
        }
		catch (SQLException e) {
			String msg =  "Error in commit in import it";
			Log.logSystemAlarm(msg, e);
			LOGGER.error(msg, e);
		}
    }


	/**
	 * just correct some funny values
	 */
	/* used to do some preprocessing on a fields value */
	protected String preprocessValue (SimpleAttribute metaAttr, String value) {
		if (value == null) return null;
    	
		String newValue = value.trim();
		// map aliases of boolean values to 'true' or 'false'
		DataType dt = metaAttr.getDataType();
		if (dt.getType() == DataType.BOOLEAN) {
			if (_trueAliases != null && _trueAliases.length > 0) {
				if (!value.equalsIgnoreCase("true") && !value.equalsIgnoreCase("false")) {
					newValue = "false";
					for (int i = 0; i < _trueAliases.length; i++) {
						if (value.equalsIgnoreCase(_trueAliases[i])) {
							newValue = "true";
							break;
						}
					}
				}
			}
		} else if (dt.getType() == DataType.DATE) {
			if (value.equals("00000000")) {
				newValue = null;
			}
		} else if ( "HausNr".equals(metaAttr.getExternName()) ||
					"WegHausNr".equals(metaAttr.getExternName()) ||
					"ZuzHausNr".equals(metaAttr.getExternName()) ||
					"ZusHausNr".equals(metaAttr.getExternName()) ) {
			while (value.startsWith("0")) {
				value = value.substring(1);
			}
			newValue = value;
		} 
			
		if (metaAttr instanceof ReferenceAttribute) {
			try {
				long longVal = Long.valueOf(value).longValue();
				if (longVal == 0) {
					newValue = null;
				}
			} catch (NumberFormatException e) {
				newValue = null; // somwhere later we will get a better error
			}
		}
		
		return newValue;
	}

	/**
	 * commits the connection if needet
	 */
	protected void commitIfNeedet(int lineNr, int transactionSize, BLTransaction trans,
		    Hashtable attrMappings, BOContext context, Connection con)
			throws XMetaException, XMetaModelQueryException, XMetaModelChangeFailedException,
			XSecurityException, XBOConcurrencyConflict {
		if (lineNr != 0) {
			if ( (lineNr%transactionSize) == 0) {
				this.setAllOID(trans, attrMappings, context, con);
				try {
					trans.commit(con, true);
				}
				catch (SQLException e) {
					String msg = "Error in Commit in the import";
					Log.logSystemAlarm(msg, e);
					LOGGER.error(msg, e);
					trans.emptyBuffers(); // Transaktion leeren, damit nicht derselbe Fehler nochmals entsteht 
				}
			}
		}
	}

	private List<BOObject> _newPH = new ArrayList<BOObject>();
	private List<BOObject> _newMld = new ArrayList<BOObject>();
	private List<BOObject> _newAnf = new ArrayList<BOObject>();
	private List<BOObject>[] _objToCommit = new List[] { _newPH, _newMld, _newAnf }; // Die Reihenfolge der Eintr�ge ist wichtig, weil OIDs schon vorhanden sein m�ssen
	private Hashtable<Long, Hashtable<Long, BOObject>> _objects = new Hashtable<Long, Hashtable<Long, BOObject>>();
	private Hashtable<Long, Integer> _newObjectsOIDCount = new Hashtable<Long, Integer>();
	
	protected void setAllOID(BLTransaction trans, Hashtable attrMappings, BOContext context, Connection con)
		    throws XMetaModelQueryException, XMetaModelChangeFailedException, XMetaException,
			XSecurityException{
	    // set OID's of all new objects

		// check if one of the imported values is the oid?
		if (attrMappings.containsKey("OID")) {
			List<BOObject> newObjects = (List)trans.getNewObjects().clone();
			for (int i=0; i<newObjects.size(); i++) {
				BOObject obj = (BOObject) newObjects.get(i);
				obj.setOID(Long.parseLong(obj.getAttribute("OID", context)));
			}
		}
		else {
			/* Teil der Aufgaben eines normalen Commits manuell durchf�hren:
			 * 1. OIDs aller neuen Objekte setzen
			 * 2. OIDs aller Referenzen setzen
			 */
			
			_newObjectsOIDCount.clear();
			
			for (Hashtable<Long, BOObject> objSet : _objects.values()) {
				objSet.clear();
			}

			Hashtable<Long, Hashtable<Long, BOObject>> transObjects = trans.getObjects();
			for (Entry<Long, Hashtable<Long, BOObject>> transObjSetEntry : transObjects.entrySet()) {
				Hashtable<Long, BOObject> objSet = _objects.get(transObjSetEntry.getKey());
				if (objSet == null) {
					objSet = new Hashtable<Long, BOObject>();
					_objects.put(transObjSetEntry.getKey(), objSet);
				}
				for (Entry<Long, BOObject> objEntry : transObjSetEntry.getValue().entrySet()) {
					BOObject obj = objEntry.getValue();
					if (obj.isChanged()) {
						objSet.put(objEntry.getKey(), obj);
					}
				}
			}
			
			
			List<BOObject> newObjects = (List)trans.getNewObjects().clone();
			for (BOObject newObject : newObjects) {
				long metaOID = newObject.getMetaObject().getOID();
				if (newObject.getOID() == 0) {
					Integer oidCount = _newObjectsOIDCount.get(metaOID);
					if (oidCount == null) {
						oidCount = 0;
					}
					// zu generierende OIDs z�hlen
					oidCount = oidCount + 1;
					_newObjectsOIDCount.put(metaOID, oidCount);
				}
			}
			
			/* OIDs setzen */
			for (Entry<Long, Integer> oidCountEntry : _newObjectsOIDCount.entrySet()) {
				Integer oidCount = oidCountEntry.getValue();
				if (oidCount != null && oidCount > 0) {
					long curMetaOID = oidCountEntry.getKey();
					MetaObject curMetaObj = MetaServiceManager.getInstance().getClassForOID(curMetaOID);
					long curOID = BLManager.getInstance().getOID(curMetaObj, oidCount);
					int i = 0;
					for (BOObject obj : newObjects) {
						if (obj.getMetaObject().getOID() == curMetaOID && obj.getOID() == 0) {
							obj.setOID(curOID++);
							i++;
						}
					}
					if (i != oidCount) {
						throw new XMetaException("OID", String.valueOf((curOID - i)), "oidCount stimmt nicht mit effektiv ben�tigten Anzahl OIDs �berein");
					}
//					LOGGER.error("Erstellte Objekte vom Typ " + curMetaObj.getName() + ": " + i);
				}
			}
			// Die neuen Objekte in den Objektpuffer �bertragen
			for (BOObject newObject : newObjects) {
				long metaOID = newObject.getMetaObject().getOID();
				Hashtable<Long, BOObject> objSet = _objects.get(metaOID);
				if (objSet == null) {
					objSet = new Hashtable<Long, BOObject>();
					_objects.put(metaOID, objSet);
				}
				objSet.put(newObject.getOID(), newObject);
			}
			
			// OIDs von Objektreferenzen setzen
			for (Hashtable<Long, BOObject> objSet : _objects.values()) {
				for (BOObject obj : objSet.values()) {
					obj.getReferenceOIDs();
				}
			}
		}
	}

	/**
	 * just make it public for the TableMapEDVKG
	 */
	public int getAttrIndex(String name) {
		return super.getAttrIndex(name);
	}

	/**
	 * The codes has to be imported before into the tmpCodeTable
	 */
	 private void activateCodes(BLTransaction trans, BOContext context)
		throws XMetaModelNotFoundException, XMetaModelQueryException,
		XSecurityException {

		// activate the aktivPassivStatus EF001 not needet, only true / false
		this.activateCode("CodeAktivPassiv", "EF001", false /*codeIsOID*/, trans, context);

		// activate the AdressSperre EF005 not needet, only true / false
		this.activateCode("CodeAdressSperre", "EF005", false /*codeIsOID*/, trans, context);

		// aktivate the Geschlecht EF008 not needet, only 'M' or 'W'

		// aktivate the Zivilstand EF009
		this.activateCode("CodeZivilstand", "EF009", false /*codeIsOID*/, trans, context);

		// aktivate the Aufenthaltsart EF010
		this.activateCode("CodeAufenthalt", "EF010", false /* codeIsOID */, trans, context);

		// aktivate the Aufenthaltsart EF010
		this.activateCode("CodeStimmrecht", "EF016", false /* codeIsOID */, trans, context);

		// aktivate the Code Anrede EF031
		this.activateCode("CodeAnrede", "EF031", true /* codeIsOID */, trans, context);

		// aktivate the Code Titel EF032
		this.activateCode("CodeTitel", "EF032", true /* codeIsOID */, trans, context);
	}

	/**
	 * activates the defined Code
	 */
	private void activateCode(String className, String code, boolean codeIsOID, BLTransaction trans, BOContext context)
		    throws XMetaModelNotFoundException, XMetaModelQueryException, XSecurityException{
		String query;
		MetaObject metaObj = _metaService.getClassForName(className);
		String tableName = metaObj.getTableName();

		long artOID = metaObj.getOID();

			// load the code table;
		Vector toList = BLManager.getResult (trans, "select * from "+tableName, true, context);
		Vector fromList = BLManager.getResult (trans, "select * from bx_codeimport where bx_CodeArt = '"+code+"'", true, context);

		Enumeration fromListEnum = fromList.elements();
		Enumeration toListEnum ;
		BOObject toObj = null;
		BOObject fromObj = null;
		String fromCode;
		String toCode;
		boolean notFound = true;

		if (fromList.size() > 0) {  // we do only something if we have some new codes to import
			while (fromListEnum.hasMoreElements()) {
				fromObj = (BOObject)fromListEnum.nextElement();
				fromCode = fromObj.getAttribute("code", context);
				toListEnum = toList.elements();
				notFound = true;
				while (notFound && toListEnum.hasMoreElements()) {
					toObj = (BOObject)toListEnum.nextElement();
					toCode = toObj.getAttribute("code", context);
				    if (toCode != null && toCode.equals(fromCode)) {
						notFound = false;
						// we copy the values
						this.setCodeValues(fromObj, toObj, codeIsOID, metaObj, context);
						// we delete the toValue from the Vector
						toList.remove(toObj);
					}
				}
				if (notFound) {
					// create a new value
					toObj = BLFactory.instance().createNew(trans, metaObj, false /* is part of */);
					this.setCodeValues(fromObj, toObj, codeIsOID, metaObj, context);
				}
			} // while ofer all elements of the from

			// there are additional Values that was not in the original Table
			toListEnum = toList.elements();
			while (toListEnum.hasMoreElements()) {
				// delete the Value
				toObj = (BOObject)toListEnum.nextElement();
				toObj.setDeleted(true, context);
			}

		}
	}

	/**
	 * set the values for the codes
	 */
	 private void setCodeValues(BOObject fromObj, BOObject toObj, boolean codeIsOID, MetaObject toMeta, BOContext context)
		    throws XMetaModelNotFoundException, XSecurityException{
		toObj.setValue("ValidFrom", fromObj.getValue("ValidFrom", context), context);
		toObj.setValue("code", fromObj.getValue("code", context), context);
		toObj.setValue("Status", fromObj.getValue("Status", context), context);
		toObj.setValue("Kurzname", fromObj.getValue("Kurzname", context), context);
		toObj.setValue("Name", fromObj.getValue("Name", context), context);
		if (codeIsOID) {
			toObj.setValue("OID", fromObj.getValue("code", context), context);
		}
	}
}
