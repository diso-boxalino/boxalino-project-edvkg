package ch.objcons.ecom.proj.edvkg.mzv.imp;

import static ch.objcons.ecom.proj.edvkg.mzv.Consts.Vorfall.Tod;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.Vorfall.Wegzug;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import ch.objcons.db.dbimport.XMappingException;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.BOTypeConverter;
import ch.objcons.ecom.datatypes.EDataTypeValidation;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.FeatureAttribute;
import ch.objcons.ecom.meta.MetaAttribute;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.MetaService;
import ch.objcons.ecom.meta.ReferenceAttribute;
import ch.objcons.ecom.meta.SimpleAttribute;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.BOPersonHistory;
import ch.objcons.ecom.proj.edvkg.mzv.Consts;
import ch.objcons.ecom.proj.edvkg.mzv.Consts.Vorfall;
import ch.objcons.ecom.proj.edvkg.mzv.OhneAnrechtWeil;
import ch.objcons.ecom.proj.edvkg.mzv.UtilsEDVKG;
import ch.objcons.ecom.proj.edvkg.mzv.bva.BVASupport;
import ch.objcons.ecom.query.StandardQuery;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.log.Log;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;
import com.boxalino.log.Stacktrace;

/**
 * Achtung: Referenzen werden hier ausnahmsweise mit getValue und setValue behandelt. Dies funktioniert nur solange wie
 * auf den entsprechenden Attributen nicht auch mit getReferencedObject und setReferencedObject hantiert wird.
 * 
 * @author Matthias Humbert
 *
 */
public class ImportPersonen {
	static ILogger LOGGER = Logger.getLogger("ch.objcons.ecom.proj.edvkg.import");

	/** max number of possible columns */
	public static final int MAX_COLUMS = 1000;

	/** definition how the rows are in order */
	private final MapIndexEDVKG _mapIndex;
	private final Hashtable _defaultValues;
	private final BLTransaction _trans;
	private final BOContext _context;
	private final MetaObject _metaObject;
	private final MetaService _metaService;
	// private MetaObject _metaObjectZeitung;
	private final TableMappingEDVKG _tableMapping;
	private final boolean _ohneAnrecht;

	/** Mapping of the base datas */
	private Mapping _mapBase;
	private Mapping _mapBasePLZ;

	/** Mapping for tze zustelladresse */
	private Mapping _mapZustell;
	private Mapping _mapZustellPLZ;
	private Mapping _mapZustellAAMU;
	private Mapping _mapZustellPLZAAMU;

	/** spez codes tables */
	private HashMap<String, Integer> codeKG;

	/** variables to set the OIDs */
	private static final int MAX_NUMBER_OF_OID = 10;
	private final long _nextOID = 0;
	private final long _maxOID = 0;

	private final int _zusOIDStrasseIndex;
	private final int _zusStrassenNameIndex;
	private final int _zusHausNrIndex;
	private final int _zusAdrZusatzIndex;
	private final int _zusPLZOrtIndex;
	private final int _zusPLZOrtEindIndex;
	private final int _zusOrtIndex;
	private final int _zusOIDCodeOrtLandIndex;
	private final int _zusOrtLandNameIndex;
	private final int _oidStrasseIndex;
	private final int _zusPostfachIndex;
	private final int _zusPLZPostfachIndex;
	private final int _zusPLZPostfachEindIndex;
	private final int _zusArtIndex;

	private int _datengleicheRecords;
	private Anforderungen _anforderungsLogik;
	
	private final long _importId;

	/**
	 * Class for the Roules how to import the EDVKG rows into the PersonenHistory.
	 */
	public ImportPersonen (MapIndexEDVKG mapIndex,
			Hashtable defaultValues,
			BLTransaction trans,
			BOContext context,
			MetaService metaService,
			MetaObject metaObject,
			TableMappingEDVKG tableMapping,
			boolean ohneAnrecht, long importId) throws XMetaModelNotFoundException, XMetaModelQueryException, XSecurityException {
		_mapIndex = mapIndex;
		_defaultValues=defaultValues;
		_trans = trans;
		_context = context;
		_metaService = metaService;
		_metaObject = metaObject;
		_tableMapping = tableMapping;
		_ohneAnrecht = ohneAnrecht;
		// _metaObjectZeitung = metaService.getClassForName("Zeitung");

		this.setMapBase();
		this.setMapZustell();
		this.setMapZustellAAMU();
		this.setCodeKG();
		
		// Zus�tzliche Initialisierungen
		MetaAttribute ma = _metaObject.getAllAttributeForName("ZusOIDStrasse", _context);
		_zusOIDStrasseIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusStrassenName", _context);
		_zusStrassenNameIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusHausNr", _context);
		_zusHausNrIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusAdrZusatz", _context);
		_zusAdrZusatzIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusPLZOrt", _context);
		_zusPLZOrtIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusPLZOrtEind", _context);
		_zusPLZOrtEindIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusOrt", _context);
		_zusOrtIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusOIDCodeOrtLand", _context);
		_zusOIDCodeOrtLandIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusOrtLandName", _context);
		_zusOrtLandNameIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("OIDCodeStrasse", _context);
		_oidStrasseIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusPostfach", _context);
		_zusPostfachIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusPLZPostfach", _context);
		_zusPLZPostfachIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusPLZPostfachEind", _context);
		_zusPLZPostfachEindIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusArt", _context);
		_zusArtIndex = ma.getIndex();
		
		_importId = importId;
	}

	public int getDatengleicheRecords() {
		return _datengleicheRecords;
	}
	
	/**
	 * Sets the Zustelladresse.
	 * @param record
	 * @param boObj
	 * @throws XMetaException
	 * @throws XDataTypeException
	 * @throws XSecurityException
	 */
	public void setZustellValues(Vector record, BOObject boObj) throws XMetaException, XDataTypeException, XSecurityException {
		boolean zustellIsExtern = isZustellAAMU(record);
		boolean zustellIsStadt = isZustellStadt(record);
		if (zustellIsExtern && zustellIsStadt) LOGGER.info("Importierte Adressen (OID=" + boObj.getAttribute("OIDPerson", false, _context) + ") hat stadt-externe und stadt-interne Zustelladresse");
		if (zustellIsExtern) {
			/* The suggestion is that every field of the zustelladresse is
			 * given
			 */
			this.setMappedValues(_mapZustellAAMU, record, boObj);
			this.setPLZValues(_mapZustellPLZAAMU, record, boObj);
			boObj.setValue(_zusOIDStrasseIndex, null, _context);
			boObj.setValue(_zusOrtLandNameIndex, null, _context);
			boObj.setValue(_zusArtIndex, "e", _context);
			/* correct CodeOrtLand */
			Number oidCodeOrtLand = (Number)boObj.getValue(_zusOIDCodeOrtLandIndex, false, _context);
			if (oidCodeOrtLand.longValue() < UtilsEDVKG.COUNTRY_MIN_VALUE) { 
				boObj.setValue(_zusOIDCodeOrtLandIndex, null, _context);
			}
		} else {
			/* Zustelladresse ist stadt-intern oder gar nicht gesetzt */
			this.setMappedValues(_mapZustell, record, boObj); 
			this.setPLZValues(_mapZustellPLZ, record, boObj); 
			/* Set fields to null because they may contain (invalid) 
			 * values of an external address */
			boObj.setValue(_zusStrassenNameIndex, null, _context);
			boObj.setValue(_zusOIDCodeOrtLandIndex, null, _context);
			boObj.setValue(_zusOrtLandNameIndex, null, _context);
			if (zustellIsStadt) {
				/* ZusPLZOrt, ZusPLZOrtEind and ZusOrt are set in the Consistency Check */
				boObj.setValue(_zusArtIndex, "s", _context);
			} else {
				boObj.setValue(_zusPLZOrtIndex, null, _context);
				boObj.setValue(_zusPLZOrtEindIndex, null, _context);
				boObj.setValue(_zusOrtIndex, null, _context);
				boObj.setValue(_zusArtIndex, null, _context);
			}
		}
	}
	
	/**
	 * Is the Zustelladresse stadt-extern ?
	 */
	public boolean isZustellAAMU(Vector record) {
		// Beurteilungskriterium: Ist der ausw�rtige Zustellort gesetzt?
		String zOrt = (String)record.elementAt(_mapIndex._AAD_Ortsbezeichnung);
		return !EDataTypeValidation.isNullOrEmptyString(zOrt);
	}
	
	/**
	 * Is the imported Zustelladresse stadt-intern ?
	 */
	public boolean isZustellStadt(Vector record) {
		// Beurteilungskriterium: Ist die Orts-PLZ oder die Postfach-PLZ gesetzt?
		String zPLZPostfach = (String)record.elementAt(_mapIndex._ZUS_PLZPostfach);
		String zOIDStrasse = (String)record.elementAt(_mapIndex._ZUS_Strassennummer);
		return !EDataTypeValidation.isNullOrEmptyString(zPLZPostfach) ||
			!EDataTypeValidation.isNullOrEmptyString(zOIDStrasse);
	}

	/**
	 * Provides information about changing Zustelladresse for the user
	 */
	public void doZustellAlert(BOPersonHistory oldBo, BOPersonHistory newBo, boolean umzug) {
		try {
			String oldZustell = oldBo.getAttribute(_zusArtIndex, false, _context);
			boolean oldZustellIsEnteredManually = "m".equals(oldZustell); 
			if (oldZustellIsEnteredManually) {
				String newZustell = newBo.getAttribute(_zusArtIndex, false, _context);
				boolean newZustellIsExtern = "e".equals(newZustell);
				boolean newZustellIsStadt = "s".equals(newZustell);
				Meldungsvorlage alert;
				if (newZustellIsExtern) {
					alert = Meldungsvorlage.ZA_ExterneUeberschreibtManuelle;
				} else if (newZustellIsStadt) {
					alert = Meldungsvorlage.ZA_StaedtischeUeberschreibtManuelle;
				} else /* No new Zustell */ {
					if (umzug) return;
					UtilsEDVKG.copyExternalHistoryZustell(oldBo, newBo, _context);
					alert = Meldungsvorlage.ZA_LoeschenManuelleVerh;
				}
				newBo.addMeldung(alert);
			}
		} catch (Exception e) {
			Log.logAlarm(LOGGER, "Zustell-Alert: " + e.getMessage(), e);
		}
	}
	
	/**
	 * Diese Methode bzw. das Feld VorfallBits dient zu internen Auswertungen und beansprucht keine vollst�ndig korrekte
	 * Ermittlung der Vorf�lle.
	 * 
	 * Diese Methode dient f�r den Hauptimport sowie auch f�r den Personen-ohne-Anrecht-Import
	 * 
	 * @param justAdminBits Setzt nur die administrativen Bits der Vorfall-Bits
	 */
	private int setVorfallBits(List<String> record, BOPersonHistory boOld, BOPersonHistory boNew, boolean umzug, boolean justAdminBits) throws XSecurityException, XMetaModelQueryException {
		Date date1, date2;
		EnumSet<Vorfall> vorfaelle = EnumSet.noneOf(Vorfall.class);

		if (isLieferungWeilMutiert(record)) {
			vorfaelle.add(Vorfall.Mutationslieferung);
		}
//		String lwFamilie = (String)record.get(_mapIndex._LWFamilie);
//		if (lwFamilie.equals("J")) { 
//			vorfaelle.add(Vorfall.Familienlieferung);
//		}
		if (getAnforderungsLaufnummer(record) != 0) {
			vorfaelle.add(Vorfall.Anforderungslieferung);
		}
		if (!_ohneAnrecht) {
			if (!justAdminBits) {
				if (boOld != null) {
					if (wasFieldSetToNonNull(Consts.Hist_WegDatumIndex, boOld, boNew)) {
						vorfaelle.add(Vorfall.Wegzug);
					}
					if (hatFeldGeaendert(Consts.Hist_ZivilstandsDatumIndex, boOld, boNew)) {
						vorfaelle.add(Vorfall.Zivilstandsaenderung);
					}
					if (hatFeldGeaendert(Consts.Hist_AufenthaltsartDatumIndex, boOld, boNew)) {
						vorfaelle.add(Vorfall.Aufenthaltsartaenderung);
					}
					if (hatFeldGeaendert(Consts.Hist_SterbedatumIndex, boOld, boNew)) {
						vorfaelle.add(Vorfall.Tod);
					}
					if (hatFeldGeaendert(Consts.Hist_KonfessionsDatumIndex, boOld, boNew)) {
						vorfaelle.add(Vorfall.Konfessionsaenderung);
					}
					/*
					if (!hatFeldGeaendert(Consts.Hist_ZuzDatumIndex, boOld, boNew) &&
						hatFeldGeaendert(Consts.MeldeadresseDatumIndex, boOld, boNew)) {
							
						vorfaelle.add(Vorfall.Umzug);
					}*/
					
					if (umzug) {
						vorfaelle.add(Vorfall.Umzug);
					}
					if ((date1 = (Date) boOld.getValue(Consts.Hist_WegDatumIndex)) != null && istPersonPassiv(boOld) &&
						(date2 = (Date) boNew.getValue(Consts.Hist_ZuzDatumIndex)) != null && date2.after(date1)) {
						
						vorfaelle.add(Vorfall.Wiederzuzug);
					}
				} else {
					vorfaelle.add(Vorfall.Erstlieferung);
				}
			}
		} else {
			vorfaelle.add(Vorfall.OhneAnrecht);
		}
		int bits = 0;
		for (Vorfall v : vorfaelle) {
			bits = bits | v.getBitMask();
		}
		boNew.setValue(Consts.Hist_VorfallBitsIndex, bits);
		return bits;
	}
	
	private boolean isLieferungWeilMutiert(List<String> record) {
		String lwMutiert = record.get(_mapIndex._LWMutiert);
		return "J".equals(lwMutiert);
	}
	
	private int getAnforderungsLaufnummer(List<String> record) {
		String lwAngefordert = record.get(_mapIndex._AnfLaufNummer);
		if (!EDataTypeValidation.isNullOrEmptyString(lwAngefordert)) {
			return Integer.parseInt(lwAngefordert);
		} else {
			return 0;
		}
	}

	private boolean hatFeldGeaendert (int feldIndex, BOPersonHistory boOld, BOPersonHistory boNew) throws XMetaModelNotFoundException, XSecurityException {
		Object oldValue = boOld.getValue(feldIndex);
		Object newValue = boNew.getValue(feldIndex);
		return newValue != null && !newValue.equals(oldValue) || newValue == null && oldValue != null;
	}
	
	private boolean wasFieldSetToNonNull(int fieldIndex, BOPersonHistory boOld, BOPersonHistory boNew) throws XMetaModelNotFoundException, XSecurityException {
		Object newValue = boNew.getValue(fieldIndex);
		if (newValue == null) return false;
		
		Object oldValue = boOld.getValue(fieldIndex);
		return !newValue.equals(oldValue);
	}
	
	private boolean istPersonPassiv (BOPersonHistory bo) throws XMetaModelQueryException, XSecurityException {
		return !"true".equals(bo.getAttribute(Consts.Hist_StatusPersonIndex, _context));
	}
	
	private String getDatVerarbeitung(Vector<String> record) throws XMetaModelNotFoundException {
		String datVerarbeitung = record.get(_mapIndex._DatVerarbeitung);
		SimpleAttribute simpleAttr = (SimpleAttribute)(_metaObject.getAllAttributeForIndex(Consts.Hist_DatVerarbeitungIndex));
		datVerarbeitung = _tableMapping.preprocessValue(simpleAttr, datVerarbeitung);
		return datVerarbeitung;
	}

	private void handleAnforderungen(Vector<String> record, boolean hauptImport, BOPersonHistory boHist, Connection c) throws XMetaException, XDataTypeException, XSecurityException {
		String datVerarbeitung = getDatVerarbeitung(record);
		Date verarbDatum = (Date) BOTypeConverter.convertStringToObject(
			(SimpleAttribute) _metaObject.getAllAttributeForIndex(Consts.Hist_DatVerarbeitungIndex), 
			datVerarbeitung, _context, null, null);
		
		int fbLaufnummer = getAnforderungsLaufnummer(record);
		boolean geliefertWeilMutiert = isLieferungWeilMutiert(record);
		boolean anrecht = hauptImport == true;
		
		OhneAnrechtWeil oaw = null;
		if (!hauptImport) {
			// Ohne Anrecht-Import
			String oawS = record.get(_mapIndex._ohneAnrechtWeilID);
			int grundID = Integer.parseInt(oawS);
			oaw = OhneAnrechtWeil.getGemaessID(grundID);
		}
		
		if (_anforderungsLogik == null) {
			_anforderungsLogik = new Anforderungen(c);
		}
		
		final long oidPerson;
		final BLTransaction trx;
		if (boHist != null) {
			oidPerson = boHist.getReferencedOID(Consts.Hist_OIDPersonIndex);
			trx = boHist.getTransaction();
		} else {
			oidPerson = Long.parseLong(record.elementAt(_mapIndex._OIDPerson));
			trx = _trans;
		}
		_anforderungsLogik.importiert(anrecht, oidPerson, boHist, verarbDatum, fbLaufnummer, oaw, geliefertWeilMutiert, trx);
	}
		
	public BOPersonHistory importPersonen(Vector<String> record, Connection con)  throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		BOPersonHistory bo;
		if (_ohneAnrecht) {
			bo = importPersonOhneAnrecht(record, con);
		} else {
			bo = importPersonenMutationen(record, con);
		}
		return bo;
	}
	
	public BOPersonHistory importPersonenMutationen(Vector<String> record, Connection con)  throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		// Auf korrekte Importdatenstruktur achten:
		String stopFlag = record.elementAt(_mapIndex._StopFlag);
		if (!"Z".equals(stopFlag)) {
			throw new XMappingException("Unkorrekte Datensatzstruktur im Import File");
		}
		
		BOPersonHistory oldBoObject;
		BOPersonHistory newBoObject;
		// get the old Object
		oldBoObject = this.getBaseObject(record, con);
		boolean erstAnlieferung = oldBoObject == null;
		Date nowInThisRoutine = new Date();
		if (!erstAnlieferung) {
			/* Klone oldBoObject: ACHTUNG: Alle nachfolgenden Routinen, welche berechnete Felder setzen, m�ssen
			 * diese auf einem Klon auch wieder auf null zur�cksetzen, wenn n�tig! Ansonsten kopieren sich diese
			 * Felder auf alle nachfolgenden Klone. 
			 */
			newBoObject = clonePerson(oldBoObject);
			// wegkommentiert, somit sieht man auch bis zu welchem Zeitpunkt eine Person aktiv war: 
			// oldBoObject.setValue("StatusPerson", Boolean.FALSE, _context);
		} else {
			newBoObject = this.createPerson();
		}
		/*** Kernimport Start ***/
		/* set attribute values */
		this.setMappedValues(_mapBase, record, newBoObject);
		// set the short values of the PLZ
		this.setPLZValues(_mapBasePLZ, record, newBoObject);
		this.setZustellValues(record, newBoObject);
		/*** Kernimport Ende ***/
		// PLZOrt, PLZOrtEind and Ort are set in the consistency check
		setSperre(record, newBoObject);
		
		boolean meldeAdresseChanged;
		if (erstAnlieferung){
			meldeAdresseChanged = false;
		} else {
			meldeAdresseChanged = setUmzugAdr(oldBoObject, newBoObject, con);
			doZustellAlert(oldBoObject, newBoObject, meldeAdresseChanged);
		}
		
		setKGZuzug(oldBoObject, newBoObject);
		setKGWegzug(oldBoObject, newBoObject);
		setKGsIsLast(oldBoObject, newBoObject);
		
		setPfarrkreis (record, newBoObject);

		BOPersonHistory importedObject = null;
		if (!erstAnlieferung && !hasChangedData(oldBoObject, newBoObject)) {
			_datengleicheRecords++;
			// Personendatensatz hat gar nicht ge�ndert
			// nur timestamp auf altem Objekt �ndern, neues verwerfen
			oldBoObject.setValue("Date", nowInThisRoutine, _context);
			// Das DatVerarbeitung-Feld auch setzen, 
			// da es mehr Aufschluss �ber den letzten Verarbeitungszeitpunkt bei der OIZ liefert
			oldBoObject.setValue(Consts.Hist_DatVerarbeitungIndex, newBoObject.getValue(Consts.Hist_DatVerarbeitungIndex));
			// den User auch setzen, falls zuletzt eine Mutation von einer IKG-Person durchgef�hrt wurde, ist es wahrscheinlich wichtiger zu wissen, dass der letzte Update von der Stadt kam
			oldBoObject.setValue("User", "Stadt", _context);
			// isLast wieder setzen
			oldBoObject.setValue("isLast", "true", _context);

			// Erstellte Objekte aus Transaktion entfernen
			BLTransaction trans = newBoObject.getTransaction();
			List<BOObject> meldungenListe = newBoObject.getListReferencedObjects(Consts.Hist_meldungenIndex, _context);
			if (meldungenListe != null) {
				for (BOObject boMeldung : meldungenListe) {
					trans.removeBufferedObject(boMeldung);
				}
			}
			trans.removeBufferedObject(newBoObject);
			
			importedObject = oldBoObject;
		} else {
			if (oldBoObject != null) {
				oldBoObject.setValue("lastEntry", Boolean.FALSE, _context);
			}
			newBoObject.setValue("ErstesUpdateDatum", nowInThisRoutine, _context); 
			newBoObject.setValue("User", "Stadt", _context);
			newBoObject.setValue("lastEntry", Boolean.TRUE, _context);
			newBoObject.setValue("Date", nowInThisRoutine, _context);
			importedObject = newBoObject;
		}
		
		int vBits = setVorfallBits(record, oldBoObject, newBoObject, meldeAdresseChanged, importedObject == oldBoObject);
		if ((vBits & (Tod.getBitMask() | Wegzug.getBitMask())) != 0 || !newBoObject.isRef(_context)) {
			// setzen, falls tot, weggezogen oder nicht mehr reformiert
			newBoObject.setValue("rpgAktiv", false, _context);
		}
		handleAnforderungen(record, true, importedObject, con);
		Long oidPerson = Long.parseLong(record.elementAt(_mapIndex._OIDPerson));
		BVASupport.markEntriesDelivered(oidPerson, _importId, null);
		return importedObject;
	}

	public BOPersonHistory importPersonOhneAnrecht(Vector<String> record, Connection con) throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		BOPersonHistory oldBoObject;
		BOPersonHistory curBoObject = null;
		// get the old Object
		oldBoObject = this.getBaseObject(record, con);
		boolean erstAnlieferung = oldBoObject == null;
		
		if (!erstAnlieferung) {
			Date nowInThisRoutine = new Date();
			if (!istPersonPassiv(oldBoObject)) {
				// neuen History-Eintrag machen, damit Change Aktiv-Passiv aufgezeichnet wird
				// duplicate the Object
				curBoObject = clonePerson(oldBoObject);
				String datVerarbeitung = getDatVerarbeitung(record);

				curBoObject.setValue("StatusPerson", Boolean.FALSE, _context);
				curBoObject.setAttribute(Consts.Hist_DatVerarbeitungIndex, datVerarbeitung, _context);
				curBoObject.setValue("ErstesUpdateDatum", nowInThisRoutine, _context);
				curBoObject.setValue("User", "Stadt", _context);
				curBoObject.setAttribute(Consts.Hist_RecordStatusIndex, "A", _context); // Damit wird Person aktiviert (wichtig wegen dem neuen Passiv-Status)
				curBoObject.setValue("isLast", Boolean.TRUE, _context);
				curBoObject.setValue("lastEntry", Boolean.TRUE, _context);
				curBoObject.setAttribute(Consts.Hist_PassivDatumIndex, datVerarbeitung, _context);
				
				oldBoObject.setValue("isLast", Boolean.FALSE, _context);
				oldBoObject.setValue("lastEntry", Boolean.FALSE, _context);
				
			} else {
				// do nothing ausser Aktualisierung vom Feld date
				curBoObject = oldBoObject;
			}
			curBoObject.setValue("Date", nowInThisRoutine, _context);
			setVorfallBits(record, oldBoObject, curBoObject, false, true);
		} else {
			// keine neue Person erstellen
		}
		Long oidPerson = Long.parseLong(record.elementAt(_mapIndex._OIDPerson));
		BVASupport.markEntriesDelivered(oidPerson, _importId, null);
		handleAnforderungen(record, false, curBoObject, con);
		return curBoObject;
	}

	private boolean hatRelevanteKonfession(BOPersonHistory bo) throws XMetaModelNotFoundException, XSecurityException {
		Number oid = (Number)bo.getValue(Consts.Hist_OIDKonfessionIndex);
		return oid != null && (oid.intValue() == PostImport.REFORMIERT || oid.intValue() == PostImport.KATHOLISCH);
	}
	
	private static final Set<Integer> DEBUG_SET = new HashSet<Integer>(Arrays.asList(105070614, 101180063, 100151110, 100381308)); 
	
	/**
	 * Gibt an, ob das Objekt wegen Import-Daten �nderungen erfahren hat.
	 * @param newBoObject
	 * @return
	 * @throws XSecurityException 
	 * @throws XMetaModelQueryException 
	 * @throws XMetaModelNotFoundException 
	 */
	private boolean hasChangedData(BOPersonHistory oldBoObject, BOPersonHistory newBoObject) throws XMetaModelNotFoundException, XMetaModelQueryException, XSecurityException {
		for (SimpleAttribute metaAttr : _metaObject.getAllPersistentSimpleAttributesByPosition()) {
			int index = metaAttr.getIndex(); 
			if (!newBoObject.attributeChanged(index) || index == Consts.Hist_DatVerarbeitungIndex
					|| index == Consts.Hist_isLastIndex || index == Consts.Hist_RecordStatusIndex) {
				continue;
			}
			// SR: untenstehender Kommentar betrifft den code nicht mehr, 
			// da die Person TROZDEM importiert wird (siehe Kommentar weiter unten).
			/*
			 * Bei der OIZ ist eine Person nur passiv, wenn sie gestorben oder
			 * weggezogen ist. In der MZV ist sie auch passiv, wenn die ganze Familie nicht
			 * mehr in einer KG ist (OIDKG is null). Darum gibt es Personen,
			 * welche als aktiv und ohne KG importiert und im PostImport 
			 * wieder zu passiv werden
			 * (siehe zuerst PostImport.passiviereFamilienOhneEineEinzigeKGMehr()).
			 * 
			 * Darum ist es auch nicht wirklich eine �nderung, wenn von passiv auf 
			 * aktiv gewechselt wird und gleichzeitig OIDKG von 'is not null' auf 'is null'.
			 * Letztere Condition sollte man aber nicht bringen, da es auch Personen gibt, welche
			 * OIDKG schon im alten Datensatz auf null hatten. Darum wird als Sicherheit,
			 * damit nicht zu viele Personen ins IF statement kommen, beim alten
			 * Datensatz auf keine relevante Konfession gepr�ft.
			 */
			/* currently not needed, kept for reference
			boolean warPassivUndIrrelevant = (index == Consts.Hist_StatusPersonIndex || index == Consts.Hist_OIDKGIndex) &&
				Boolean.FALSE.equals(oldBoObject.getValue(Consts.Hist_StatusPersonIndex)) 
			  	&& newBoObject.getValue(Consts.Hist_OIDKGIndex) == null && !hatRelevanteKonfession(oldBoObject);
			 */
			if (DEBUG_SET.contains(((Number) newBoObject.getValue("OIDPerson")).intValue())) {
				Object oid = newBoObject.getValue("OIDPerson");
				LOGGER.error("PH in debugset has change: " + oid + " attrix: " + index + " attrname: " + metaAttr.getInternalAttrName());
			}
			boolean fraumuensterFlag = (index == Consts.Hist_OIDKGIndex) && oldBoObject.getValue(Consts.Hist_OIDKGIndex) != null 
				&& ((Number) oldBoObject.getValue(Consts.Hist_OIDKGIndex)).intValue() == 1001;
			/* SR: Allow person to be imported if it's not releveant and it's only been changed to active, because
			 * not the all persons are guaranteed to have been considered and interpreted by the new PostImport. 
			 * Furthermore, when changes are made to how the PostImport works, it should be possible to request
			 * (Anforderungsmechanik) the person and it must be imported, even if solely the active state has changed.
			 */
			/* Fraum�nsterflag im Postimport wird true sein oder aber dann
			 * hat sich auch die Meldeadresse ge�ndert
			 */
			if (/*warPassivUndIrrelevant || */fraumuensterFlag) {
				// do nothing: no real change
				LOGGER.debug("Person ohne wirklichen Change: " + newBoObject.getValue(Consts.Hist_OIDPersonIndex));
			} else {
				// Intervenieren wenn Long und Integer verglichen werden und Werte gleich sind
				Object newValue = newBoObject.getValue(metaAttr.getIndex());
				Object oldValue = oldBoObject.getValue(metaAttr.getIndex());
				if (newValue instanceof Number) {
					// Objekte sind Zahlen
					if (newValue != null && oldValue != null && ((Number)newValue).longValue() == ((Number)oldValue).longValue()) {
						// the objects are equal!
						continue;
					} 
				} else {
					// Objekte sind keine Zahlen: Sicherheitshalber machen wir hier auch den Test, weil es
					// Konstellationen gibt in denen gewisse Attribute zuerst ge�ndert werden und am Schluss wieder den Wert des alten Personobjekts annehmen 
					
					// zuerst auf Boolean testen
					if ((newValue instanceof Boolean || oldValue instanceof Boolean) && 
						(newValue == null ? false : ((Boolean)newValue).booleanValue()) == (oldValue == null ? false : ((Boolean)oldValue).booleanValue())) {
						continue;
					}
						  
					// alle anderen Objekte
					if (newValue == null && oldValue == null || newValue != null && newValue.equals(oldValue)) {
						continue;
					}
				}
				return true;
			}
		}
		return false;
	}

	private void setSperre(Vector<String> record, BOPersonHistory newBoObject) throws XMetaModelNotFoundException, XSecurityException {
		String codeAdressSperre = record.elementAt(_mapIndex._CodeAdressSperre);
		newBoObject.setValue(Consts.Hist_AdressSperreSicherheitIndex, Boolean.valueOf("S".equalsIgnoreCase(codeAdressSperre))); 
	}

	/**
	 * Sollte �ndern bzw. erstmals gesetzt sein bei Zuzug (Erstanlieferung), Wiederzuzug, Umzug, 
	 * Neulieferung aufgrund Familie (Erstanlieferung)
	 * 
	 * @param newBoObject
	 * @throws XMetaException
	 * @throws XDataTypeException
	 * @throws XSecurityException
	 */
	/*
	 * Die Frage ist, ob es das KG-Zuzugsdatum �berhaupt braucht, da es eh immer dem Zuzugsdatum oder dem 
	 * Meldeadressedatum entspricht (mh@20110603).
	 * 
	 * Das Zuzugsdatum, ZuzOIDKG und ZuzAdresse (siehe Umzugs-Routine) werden von History-Eintrag zu History-Eintrag weiterkopiert,
	 * solange sich die Kg nicht �ndert
	 */
	private void setKGZuzug(BOObject oldBoObject, BOObject newBoObject) throws XMetaException, XDataTypeException, XSecurityException {
		if (oldBoObject == null || kgUngleich(oldBoObject, newBoObject)) {
			Date newZuzugsdatum = (Date) newBoObject.getValue("ZuzDatum", _context);
			Date newMeldeadressedatum = (Date) newBoObject.getValue("MeldeadresseDatum", _context);
			Date kgZuzugsdatum = latestOne(newZuzugsdatum, newMeldeadressedatum);
			newBoObject.setValue("ZuzKGDatum", kgZuzugsdatum, _context);
			if (oldBoObject != null) {
				Number oldKG = (Number) oldBoObject.getValue("OIDKG", _context);
				newBoObject.setValue("ZuzOIDKG", oldKG, _context);
			}
		} else {
			// Kirchen gleich
			// Durch den Clone ist ja das ZuzKGDatum vom alten Objekt auch auf dem neuen: Das ist OK, damit ZuzKGDatum und
			// ZuzOIDKG in der Passiv-Suche angezeigt wird
		}
	}

	/**
	 * Sollte �ndern bzw. erstmals gesetzt sein bei Wegzug, Tod, Umzug (auf dem alten History-Eintrag)
	 * 
	 * @param newBoObject
	 * @throws XMetaException
	 * @throws XDataTypeException
	 * @throws XSecurityException
	 */
	private void setKGWegzug(BOObject oldBoObject, BOObject newBoObject) throws XMetaException, XDataTypeException, XSecurityException {
		Date newWegzugsdatum = (Date) newBoObject.getValue("WegDatum", _context);
		Date newSterbedatum = (Date) newBoObject.getValue("Sterbedatum", _context);
		Date kgWegDatum = earlierOne(newWegzugsdatum, newSterbedatum); 
		newBoObject.setValue("WegKGDatum", kgWegDatum, _context); // setzt eben auch null (was der Normalfall ist)
		
		if (oldBoObject != null) {
			Date newMeldeadressedatum = (Date)newBoObject.getValue("MeldeadresseDatum", _context);
			if (newMeldeadressedatum != null) {
				oldBoObject.setValue("WegKGDatum", kgWegDatum, _context);
			}
			// Im Prinzip bewirkt diese If-Konstruktion, dass WegKGDatum und WegOIDKG unabh�ngig voneinander gesetzt werden
			if (kgUngleich(oldBoObject, newBoObject)) {
				Number newKG = (Number) newBoObject.getValue("OIDKG", _context);
				oldBoObject.setValue("WegOIDKG", newKG, _context);

				// set origKG
				if (newKG == null) {
					Number oldKG = (Number) oldBoObject.getValue("OIDKG", _context);
					newBoObject.setAttribute("OIDKGOrig", "" + oldKG, _context);
				}
			} else {
				// do nothing
			}
		}
		assert newSterbedatum == null && newWegzugsdatum == null || Boolean.FALSE.equals(newBoObject.getValue(Consts.Hist_StatusPersonIndex));
	}
	
	private void setKGsIsLast(BOObject oldBoObject, BOObject newBoObject) throws XSecurityException, XMetaException, XDataTypeException {
		if (oldBoObject == null) {
			newBoObject.setValue("isLast", new Boolean(true), _context);
		} else {
			Number oldKG = (Number) oldBoObject.getValue("OIDKG", _context);
			Number newKG = (Number) newBoObject.getValue("OIDKG", _context);
	
			if (!kgUngleich(oldBoObject, newBoObject)) {
				// that was not a change in the kg, so we do nothing exept set the islast
				oldBoObject.setValue("isLast", new Boolean(false), _context);
				newBoObject.setValue("isLast", new Boolean(true), _context);
			}
			else {
				// the old an the new will be active
				newBoObject.setValue("isLast", new Boolean(true), _context);
	
				if (oldKG == null /* <- Sinn ist nicht klar */ || newKG == null) {
					oldBoObject.setValue("isLast", new Boolean(false), _context);
				}
				else {
					// check if the are some old one (KG1 -> KG2 -> KG1)
					String query = "select * from bx_personhistory where bx_islast='j' and bx_oidkg="+newKG+" and bx_oidperson="+newBoObject.getAttribute("OIDPerson", _trans.getContext())+" order by OID desc";
					Vector result = BLManager.getResult(_trans, query, true /* isAlreadyFiltered */, _context);
					if (result.size() > 0) {
						// it is the first Element
						BOObject boObject = (BOObject)result.elementAt(0);
						boObject.setValue("isLast", new Boolean(false), _context);
					}
				}
			}
		}			
	}

	private boolean kgUngleich(BOObject oldBo, BOObject newBo) throws XMetaModelNotFoundException, XSecurityException {
		Number oldKG = (Number) oldBo.getValue("OIDKG", _context);
		Number newKG = (Number) newBo.getValue("OIDKG", _context);
		if (oldKG != null && oldKG.intValue() == 1001 || newKG != null && newKG.intValue() == 1001) {
			// 1001 ist keine wirkliche Kirchgemeinde
			return false;  
		}
		return oldKG == null && newKG != null || oldKG != null && !oldKG.equals(newKG);
	}
	
	private Date earlierOne(Date ... dates) {
		Date earliest = null;
		for (Date date : dates) {
			if (date != null && (earliest == null || earliest != null && date.before(earliest))) {
				earliest = date;
			}
		}
		return earliest;
	}
	
	private Date latestOne(Date ... dates) {
		Date latest = null;
		for (Date date : dates) {
			if (date != null && (latest == null || latest != null && date.after(latest))) {
				latest = date;
			}
		}
		return latest;
	}
	
	/**
	 * sets the zuzug / Wegzug KG and Datum
	 */
	private boolean setUmzugAdr(BOPersonHistory oldBoObject, BOPersonHistory newBoObject, Connection con) throws XMetaException, XSecurityException, XDataTypeException  {
		BOObject tmpObj;
		boolean istUmzug = false;

		String oldStr = oldBoObject.getAttribute("OIDCodeStrasse", _context);
		String oldStrNr = oldBoObject.getAttribute("HausNr", _context);
		Date oldMeldeAdrDatum = (Date) oldBoObject.getValue("MeldeadresseDatum", _context);
		String newStr = newBoObject.getAttribute("OIDCodeStrasse", _context);
		String newStrNr = newBoObject.getAttribute("HausNr", _context);
		Date newMeldeAdrDatum = (Date) newBoObject.getValue("MeldeadresseDatum", _context);
		Date newZuzugDatum = (Date) newBoObject.getValue("ZuzDatum", _context);

		if (oldStr == null) oldStr = "";
		if (oldStrNr == null) oldStrNr = "";
		if (newStr == null) newStr = "";
		if (newStrNr == null) newStrNr = "";

		// why must oldStrNr be equal to newStrNr?
		if ( !(oldStr.equals(newStr) && (oldStrNr.equals(newStrNr)) ) ) {
			// we have to set the zuzug and Wegzug str
			// Set the zuzugAdresse
			istUmzug = true;
			newBoObject.setAttribute("ZuzAdrZusatz", oldBoObject.getAttribute("AdrZusatz", _context), _context, null);
			newBoObject.setAttribute("ZuzPLZOrtEind", oldBoObject.getAttribute("PLZOrtEind", _context), _context, null);
			newBoObject.setAttribute("ZuzPLZOrt", oldBoObject.getAttribute("PLZOrt", _context), _context, null);
			newBoObject.setAttribute("ZuzOrt", oldBoObject.getAttribute("Ort", _context), _context, null);
			newBoObject.setAttribute("ZuzHausNr", oldBoObject.getAttribute("HausNr", _context), _context, null);
			tmpObj = oldBoObject.getReferencedObject("OIDCodeStrasse", _context);
			newBoObject.setAttribute("ZuzStrassenName", tmpObj != null ? tmpObj.getAttribute("Name", _context) : null, _context, null);
			newBoObject.setAttribute("ZuzPLZPostfachEind", oldBoObject.getAttribute("PLZPostfachEind", _context), _context, null);
			newBoObject.setAttribute("ZuzPLZPostfach", oldBoObject.getAttribute("PLZPostfach", _context), _context, null);
			newBoObject.setAttribute("ZuzPostfach", oldBoObject.getAttribute("Postfach", _context), _context, null);
			newBoObject.setAttribute("ZuzPostlagernd", oldBoObject.getAttribute("Postlagernd", _context), _context, null);
			newBoObject.setAttribute("ZuzPostfach", oldBoObject.getAttribute("Postfach", _context), _context, null);
			// set the Wegzugsadress
			oldBoObject.setAttribute("WegAdrZusatz", newBoObject.getAttribute("AdrZusatz", _context), _context, null);
			/* Ort und PLZ werden erst im Konsistenz-Check ge-updatet */
			String hausNr = newBoObject.getAttribute("HausNr", _context);
			oldBoObject.setAttribute("WegHausNr", hausNr, _context, null);
			tmpObj = newBoObject.getReferencedObject(_oidStrasseIndex, false, _context);
			setPLZAndOrt(oldBoObject, "Weg", tmpObj != null ? tmpObj.getOID() : -1, hausNr, con);
			oldBoObject.setAttribute("WegStrassenName", tmpObj != null ? tmpObj.getAttribute("Name", _context) : null, _context, null);
			oldBoObject.setAttribute("WegPLZPostfachEind", newBoObject.getAttribute("PLZPostfachEind", _context), _context, null);
			oldBoObject.setAttribute("WegPLZPostfach", newBoObject.getAttribute("PLZPostfach", _context), _context, null);
			oldBoObject.setAttribute("WegPostfach", newBoObject.getAttribute("Postfach", _context), _context, null);
			oldBoObject.setAttribute("WegPostlagernd", newBoObject.getAttribute("Postlagernd", _context), _context, null);
			oldBoObject.setAttribute("WegPostfach", newBoObject.getAttribute("Postfach", _context), _context, null);
		}
		
		if (newMeldeAdrDatum != null && !newMeldeAdrDatum.equals(oldMeldeAdrDatum) 
				&& newZuzugDatum != null && newMeldeAdrDatum.after(newZuzugDatum)) {
				
			// In der folgenden Condition wird oldMeldeAdrDatum auf nicht-null getestet, weil alte PersonenHistory-Eintr�ge
			// von vor der Schnittst.2 -Zeit gar kein solches Meldeadressdatum hatten und darum f�lschlicherweise angenommen w�rde, 
			// dass die Person seit ihrer letzten Lieferung umgezogen ist.
			if (oldMeldeAdrDatum != null && !istUmzug) {
				newBoObject.addMeldung(Meldungsvorlage.Umz_OhneAdrAenderung);
			}
		} else if (istUmzug) {
			newBoObject.addMeldung(Meldungsvorlage.Umz_OhneMeldeAdrDatum);
		}
		
		return istUmzug;
	}

	/**
	 * get the corresponding object for the current record from the database
	 */
	private BOPersonHistory getBaseObject(Vector record, Connection con) throws XMetaModelQueryException{
		return getBaseObject(record, con, true);
	}
	
	private BOPersonHistory getBaseObject(Vector record, Connection con, boolean islast) throws XMetaModelQueryException{
		String persoid = (String)record.elementAt(_mapIndex._OIDPerson);
		String query = "select * from bx_personhistory where " +
				"bx_OIDPerson = " + persoid;
		if (islast) query += " and bx_islast='j'";
		query += " order by oid desc";
		// System.out.println("Query getBaseObject = <"+query+">");
		Vector<BOPersonHistory> result = BLManager.getResult(_trans, query, true /* isAlreadyFiltered */, _trans.getContext(), con);
		if (result.size() > 0) {
			// it is the first Element
			return result.elementAt(0);
		} else if (islast) {
			BOPersonHistory histObj = getBaseObject(record, con, false);
			if (histObj != null) Log.logAlarm(LOGGER, "Import: Person (ZIP: " + persoid + ") hat keinen letzten History-Eintrag", new Stacktrace());
			return histObj;
		}
		//Log.logSystemAlarm("Konnte keinen History-Eintrag der Person mit ZIP=" + persoid + " zur�ckgeben");
		return null;
	}

	private void setPLZAndOrt(BOObject bo, String prefix, long oidStrasse, String hausNr, Connection con) throws XSecurityException, XMetaException, XDataTypeException{
		if (hausNr == null || oidStrasse <= 0) return;
		String query = "select * from bx_codehaus where " +
				"bx_OIDStrasse = " + oidStrasse + " and bx_HausNr = '" + hausNr + "' order by oid desc";
		Vector result = BLManager.getResult(_trans, query, true /* isAlreadyFiltered */, _trans.getContext(), con);
		if (result.size() > 0) {
			// it is the first Element
			BOObject addressObj = (BOObject)result.elementAt(0);
			String plzEind = addressObj.getAttribute("Postleizahl", false, _context);
			bo.setAttribute(prefix + "PLZOrtEind", plzEind, _context, null);
			bo.setAttribute(prefix + "PLZOrt", plzEind.substring(0, 4), _context, null);
			bo.setAttribute(prefix + "Ort", "Z�rich", _context, null);
		}
	}

	private void setDefaultValues(BOObject pers) throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		// set default values
		if (_defaultValues != null) {
			Enumeration values = _defaultValues.keys();
			while (values.hasMoreElements()) {
				String key = (String) values.nextElement();
				String value = (String)_defaultValues.get(key);
				MetaAttribute ma = _metaObject.getAllAttributeForName(key, _context);
				if (ma instanceof ReferenceAttribute) {
					// setAttribute ginge auch, f�hrt aber schlussendlich setReferencedObject aus
					// was in diesem Import nicht erw�nscht ist (beim Commit w�rde der Wert von setReferencedObject
					// den Wert den setValue setzt, �berschreiben)
					Integer numberValue = (value == null ? null : new Integer(value)); 
					pers.setValue(key, numberValue, _context);
				} else {
					pers.setAttribute(key, value, _context, null);
				}
			}
		}
	}
	
	private BOPersonHistory clonePerson(BOObject pers) throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		/* Die Meldungen sollen beim Aufruf von setClonedMembers() nicht kopiert werden!
		 * Darum darauf achten, dass sie im Source Objekt gar nicht geladen sind
		 * --> sollte eigentlich der Fall sein, weil pers erst gerade geladen wurde
		 */
		assert pers.getListAttributeBuffer(Consts.Hist_meldungenIndex, true) == null;
		BOPersonHistory newBoObject = new BOPersonHistory(pers.getTransaction(), 0);
		pers.setClonedMembers(newBoObject);
		
		// set the default values of the fields which are not overwritten by the import
		setDefaultValues(newBoObject);
		return newBoObject;
	}

	/**
	 * get a default Object with the default values to put into the database
	 */
	private BOPersonHistory createPerson() throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		BOPersonHistory boObject = (BOPersonHistory) _trans.getObject(_metaObject, 0, false /* isPartOf */);
		setDefaultValues(boObject);
		return boObject;
	}


	/**
	 * set the values in the object acording to the mapping
	 */
	private void setMappedValues(Mapping mapping, Vector record, BOObject boObject) throws XMetaException, XDataTypeException, XSecurityException {
		int counter = 0;
		String value;
		Integer codeValue;
		CodeTableEDVKG codeTable;
		int from;
		int to;
		MetaAttribute metaAttr;

		/* get the default values */
		Hashtable defaultValues = _tableMapping.getDefaultValues();

		for (counter = 0; counter < mapping.size(); counter++) {
			from = mapping.getFrom(counter);
			to = mapping.getTo(counter);
			metaAttr = mapping.getMeta(counter);
			value = ((String)(record.elementAt(from))).trim();

			// do not overwrite default value with empty one
			if ((!EDataTypeValidation.isNullString(value)) || (defaultValues == null) || (defaultValues.get(metaAttr.getExternName()) == null)) {
				if (metaAttr instanceof FeatureAttribute) {
					boObject.setAttribute(to, value, _context, null);
				} else if (metaAttr instanceof SimpleAttribute) {
					value = _tableMapping.preprocessValue((SimpleAttribute)(metaAttr), value);
					boObject.setAttribute(to, value, _context, null);
				} else {
					Log.logAlarm(LOGGER, "Error beim Import, falscher Typ in Funktion ImportRoulesEDVKG.setMappedValues, "+
						"counter="+counter+", from="+from+", to="+to+", name="+_mapIndex._nameList[to]);
				}
			}
		}
		
		// set the code values
		String defaultValue = null;
		for (counter = 0; counter < mapping.sizeCode(); counter++) {
			from = mapping.getCodeFrom(counter);
			to = mapping.getCodeTo(counter);
			codeTable = mapping.getCodeTable(counter);
			value = ((String)(record.elementAt(from))).trim();
			
			if (value.length() > 0) {
				codeValue = codeTable.getOID(value);
				// OIDs are allways SimpleAttribute's
				boObject.setValue(to, codeValue, _context);
			}
			else {
				// do not overwrite default value with empty one
				if (defaultValues != null) {
					metaAttr = mapping.getCodeMeta(counter);
					defaultValue = (String) defaultValues.get(metaAttr.getExternName());
				}
				if (defaultValue == null) {
					boObject.setValue(to, null, _context);
				}	
			}
		}
	}

	/**
	 * set the values of the PLZ
	 */
	private void setPLZValues(Mapping mapping, Vector record, BOObject boObject) throws XMetaException, XDataTypeException, XSecurityException {
		int counter = 0;
		String value;
		int from;
		int to;
		MetaAttribute metaAttr;
		for (counter = 0; counter < mapping.size(); counter++) {
			from = mapping.getFrom(counter);
			to = mapping.getTo(counter);
			metaAttr = mapping.getMeta(counter);
			value = ((String)(record.elementAt(from))).trim();
			//	Zwei letzten Ziffern (meistens 00) abschneiden
			// Achtung: value kann auch null oder leerer String sein
			if (value != null && value.length() > 4) value = value.substring(0,4); 
		    if (metaAttr instanceof FeatureAttribute) {
					boObject.setAttribute(to, value, _context, null);
		    } else if (metaAttr instanceof SimpleAttribute) {
		    	value = _tableMapping.preprocessValue((SimpleAttribute)(metaAttr), value);
					boObject.setAttribute(to, value, _context, null);
		    } else {
			    Log.logAlarm(LOGGER, "Error beim Import, falscher Typ in Funktion ImportRoulesEDVKG.setPLZValues");
		    }
		}
	}

	/**
	 * set the values of the KG's in the records
	 * @throws XDataTypeException 
	 * @throws XMetaException 
	 */
	private void setPfarrkreis (Vector record, BOPersonHistory boObject)
		    throws XSecurityException, XMetaException, XDataTypeException {
	    String name, kg;
		Integer oid;
		name = (String)record.elementAt(_mapIndex._OIDPfarrkreis); // Das ist nicht die OID sondern '01', '02', '03', ...
		kg = (String)record.elementAt(_mapIndex._OIDKG);
		oid = codeKG.get(kg+name);
		if (oid == null && kg != null && !kg.equals("00")) {
			boObject.addMeldung(Meldungsvorlage.Pfk_Unbek, kg);
		}
		boObject.setValue("OIDPfarrkreis", oid, _context);
	}

	/**
	 * set the codes for the KG
	 */
	 private void setCodeKG()
			throws XMetaModelNotFoundException, XMetaModelQueryException, XSecurityException {
		codeKG = new HashMap();
		MetaObject metaObject = _metaService.getClassForName("Pfarrkreis");
		StandardQuery query = BLManager.instance().getExtent(metaObject);
		Vector result = query.getResult(_trans,false, true, _context);
		BOObject myObj;
		Integer oid;
		String name, kg;
		for (int counter = 0; counter < result.size(); counter++) {
			myObj = (BOObject)result.elementAt(counter);
			oid = new Integer((int) myObj.getOID());
			name = myObj.getAttribute("Name", _context); 
			/* mh@20060519 Achtung: "kg" muss ein zweistelliger String sein!!! */
			kg = myObj.getAttribute("OIDKG", _context);
			if (kg.length() == 1) kg = "0" + kg;
			codeKG.put(kg+name, oid);
		}
	}

	 /**
	  * create the Mapping for the base types
	  */
	private void setMapBase() throws XMetaModelQueryException, XMetaModelNotFoundException, XSecurityException {
		_mapBase = new Mapping(_context, _metaObject, _metaService);
		_mapBasePLZ = new Mapping(_context, _metaObject, _metaService);

	// allgemeinde Daten
		// OIDPerson wird verwendet _mapWegzug.add(_mapIndex._OID, _mapIndex._nameList[_mapIndex._OID]);
		_mapBase.add(_mapIndex._Heimatort, _mapIndex._nameList[_mapIndex._Heimatort]);
		_mapBase.add(_mapIndex._Geburtsdatum, _mapIndex._nameList[_mapIndex._Geburtsdatum]);
		_mapBase.add(_mapIndex._Beruf, _mapIndex._nameList[_mapIndex._Beruf]);
		_mapBase.add(_mapIndex._AktivPassivStatus, _mapIndex._nameList[_mapIndex._AktivPassivStatus]);
		_mapBase.add(_mapIndex._Passivdatum, _mapIndex._nameList[_mapIndex._Passivdatum]);
		_mapBase.add(_mapIndex._Sterbedatum, _mapIndex._nameList[_mapIndex._Sterbedatum]);
		_mapBase.add(_mapIndex._KonfessionsDatum, _mapIndex._nameList[_mapIndex._KonfessionsDatum]);
		_mapBase.add(_mapIndex._AufenthaltsartDatum, _mapIndex._nameList[_mapIndex._AufenthaltsartDatum]);
		_mapBase.add(_mapIndex._MeldeadresseDatum, _mapIndex._nameList[_mapIndex._MeldeadresseDatum]);
		_mapBase.add(_mapIndex._ZuzDatum, _mapIndex._nameList[_mapIndex._ZuzDatum]);
		_mapBase.add(_mapIndex._WegDatum, _mapIndex._nameList[_mapIndex._WegDatum]);
		_mapBase.addCode(_mapIndex._CodeAufenthaltsart, _mapIndex._nameList[_mapIndex._CodeAufenthaltsart], "CodeAufenthalt");
		_mapBase.add(_mapIndex._CodeGeschlecht, _mapIndex._nameList[_mapIndex._CodeGeschlecht]);

	// Adresse
		_mapBase.add(_mapIndex._Familienname, _mapIndex._nameList[_mapIndex._Familienname]);
		_mapBase.add(_mapIndex._Vorname, _mapIndex._nameList[_mapIndex._Vorname]);
		_mapBase.add(_mapIndex._Rufname, _mapIndex._nameList[_mapIndex._Rufname]);
		_mapBase.add(_mapIndex._Allianzname, _mapIndex._nameList[_mapIndex._Allianzname]);
		_mapBase.addCode(_mapIndex._CodeVoranstellung, _mapIndex._nameList[_mapIndex._CodeVoranstellung], "CodeVoranstellung");
		_mapBase.add(_mapIndex._CodeAnrede, _mapIndex._nameList[_mapIndex._CodeAnrede]);
		_mapBase.addCode(_mapIndex._CodeTitel, _mapIndex._nameList[_mapIndex._CodeTitel], "CodeTitel");
		_mapBase.add(_mapIndex._Postlagernd, _mapIndex._nameList[_mapIndex._Postlagernd]);
		_mapBase.add(_mapIndex._Postfach, _mapIndex._nameList[_mapIndex._Postfach]);
		_mapBasePLZ.add(_mapIndex._PLZPostfach, "PLZPostfach");
		_mapBase.add(_mapIndex._PLZPostfach, _mapIndex._nameList[_mapIndex._PLZPostfach]);
		_mapBase.add(_mapIndex._Strassennummer, _mapIndex._nameList[_mapIndex._Strassennummer]);
		_mapBase.add(_mapIndex._Hausnummer, _mapIndex._nameList[_mapIndex._Hausnummer]);
		// no _mapBase.add(_mapIndex._Ort, _mapIndex._nameList[_mapIndex._Ort]);
		// no _mapBase.add(_mapIndex._PLZOrt, _mapIndex._nameList[_mapIndex._PLZOrt]);
		// no _mapBase.add(_mapIndex._PLZOrtEind, _mapIndex._nameList[_mapIndex._PLZOrtEind]);
		_mapBase.add(_mapIndex._Zusatzzeile, _mapIndex._nameList[_mapIndex._Zusatzzeile]);

	// Verwandschaft
		_mapBase.addCode(_mapIndex._OIDCodeZivilstand, _mapIndex._nameList[_mapIndex._OIDCodeZivilstand], "CodeZivilstand");
		_mapBase.add(_mapIndex._ZivilstandsDatum, _mapIndex._nameList[_mapIndex._ZivilstandsDatum]);
		_mapBase.add(_mapIndex._OIDHaushalt, _mapIndex._nameList[_mapIndex._OIDHaushalt]);
		// _mapBase.add(_mapIndex._OIDFamilie, _mapIndex._nameList[_mapIndex._OIDFamilie]);
		_mapBase.add(_mapIndex._OIDVorstand, _mapIndex._nameList[_mapIndex._OIDVorstand]);
		_mapBase.addCode(_mapIndex._OIDCodePartner, _mapIndex._nameList[_mapIndex._OIDCodePartner], "CodePartner");
		_mapBase.add(_mapIndex._OIDEhepartner, _mapIndex._nameList[_mapIndex._OIDEhepartner]);
		_mapBase.add(_mapIndex._OIDVater, _mapIndex._nameList[_mapIndex._OIDVater]);
		_mapBase.add(_mapIndex._OIDMutter, _mapIndex._nameList[_mapIndex._OIDMutter]);
		_mapBase.add(_mapIndex._OIDVormund, _mapIndex._nameList[_mapIndex._OIDVormund]);

	// KGSpez
		_mapBase.add(_mapIndex._OIDKG, _mapIndex._nameList[_mapIndex._OIDKG]);
		_mapBase.addCode(_mapIndex._OIDKonfession, _mapIndex._nameList[_mapIndex._OIDKonfession], "Konfession");
		_mapBase.add(_mapIndex._OIDKonfession, "Konfession");
//		_mapBase.add(_mapIndex._OIDPfarrkreis, _mapIndex._nameList[_mapIndex._OIDPfarrkreis]); wird extra gehandelt
		_mapBase.add(_mapIndex._CodeStimmrecht, _mapIndex._nameList[_mapIndex._CodeStimmrecht]);

	// History
		_mapBase.add(_mapIndex._OIDPerson, _mapIndex._nameList[_mapIndex._OIDPerson]);
		/*
		_mapBase.add(_mapIndex._Vorfallart, _mapIndex._nameList[_mapIndex._Vorfallart]);
		_mapBase.add(_mapIndex._Vorfalltyp, _mapIndex._nameList[_mapIndex._Vorfalltyp]);
		_mapBase.add(_mapIndex._DatVorfall, _mapIndex._nameList[_mapIndex._DatVorfall]);
		*/
		_mapBase.add(_mapIndex._DatVerarbeitung, _mapIndex._nameList[_mapIndex._DatVerarbeitung]);
	}


	/**
	 * create the Mapping table for the Stadt-Zustelladresse
	 */
	private void setMapZustell() throws XMetaModelQueryException, XMetaModelNotFoundException, XSecurityException {
		_mapZustell = new Mapping(_context, _metaObject, _metaService);
		_mapZustellPLZ = new Mapping(_context, _metaObject, _metaService);

		// Adresse Zustell Stadt
		_mapZustell.add(_mapIndex._ZUS_Postlagernd, _mapIndex._nameList[_mapIndex._ZUS_Postlagernd]);
		_mapZustell.add(_mapIndex._ZUS_Postfach, _mapIndex._nameList[_mapIndex._ZUS_Postfach]);
		_mapZustellPLZ.add(_mapIndex._ZUS_PLZPostfach, "ZusPLZPostfach");
		_mapZustell.add(_mapIndex._ZUS_PLZPostfach, _mapIndex._nameList[_mapIndex._ZUS_PLZPostfach]);
		_mapZustell.add(_mapIndex._ZUS_Strassennummer, _mapIndex._nameList[_mapIndex._ZUS_Strassennummer]);
		// no _mapBase.add(_mapIndex._ZusStrassenName, _mapIndex._nameList[_mapIndex._ZusStrassenName]);
		_mapZustell.add(_mapIndex._ZUS_Hausnummer, _mapIndex._nameList[_mapIndex._ZUS_Hausnummer]);
		// _mapBase.add(_mapIndex._ZusOrt, _mapIndex._nameList[_mapIndex._ZusOrt]);
		// _mapBase.add(_mapIndex._ZusPLZOrt, _mapIndex._nameList[_mapIndex._ZusPLZOrt]);
		// _mapBase.add(_mapIndex._ZusPLZOrtEind, _mapIndex._nameList[_mapIndex._ZusPLZOrtEind]);
		// _mapBase.add(_mapIndex._ZusOIDCodeOrtLand, _mapIndex._nameList[_mapIndex._ZusOIDCodeOrtLand]);
		_mapZustell.add(_mapIndex._ZUS_Zusatzzeile, _mapIndex._nameList[_mapIndex._ZUS_Zusatzzeile]);
	}

	/**
	 * create the Mapping table for the Zustelladresse ausserhalb der Stadt
	 */
	private void setMapZustellAAMU() throws XMetaModelQueryException, XMetaModelNotFoundException, XSecurityException {
		_mapZustellAAMU = new Mapping(_context, _metaObject, _metaService);
		_mapZustellPLZAAMU = new Mapping(_context, _metaObject, _metaService);

		// Adresse Zustell ausserhalb Stadt, bestimmt zum Ueberschreiben der Adresse innerhalb der Stadt
		_mapZustellAAMU.add(_mapIndex._AAD_Postlagernd,  _mapIndex._nameList[_mapIndex._ZUS_Postlagernd]);
		_mapZustellAAMU.add(_mapIndex._AAD_Postfach, _mapIndex._nameList[_mapIndex._ZUS_Postfach]);
		_mapZustellPLZAAMU.add(_mapIndex._AAD_PLZPostfach, "ZusPLZPostfach");
		_mapZustellAAMU.add(_mapIndex._AAD_PLZPostfach, _mapIndex._nameList[_mapIndex._ZUS_PLZPostfach]);
		_mapZustellPLZAAMU.add(_mapIndex._AAD_PLZOrt, "ZusPLZOrt"); // nur AAMU-Zustell
		_mapZustellAAMU.add(_mapIndex._AAD_PLZOrt, "ZusPLZOrtEind"); // nur AAMU-Zustell
		_mapZustellAAMU.add(_mapIndex._AAD_Ortsbezeichnung, "ZusOrt"); // nur AAMU-Zustell
		_mapZustellAAMU.add(_mapIndex._AAD_Strassenname, "ZusStrassenName");
		_mapZustellAAMU.add(_mapIndex._AAD_Hausnummer, "ZusHausNr");
		_mapZustellAAMU.add(_mapIndex._AAD_CodeOrtLand, "ZusOIDCodeOrtLand"); // nur AAMU-Zustell
		_mapZustellAAMU.add(_mapIndex._AAD_Zusatzzeile, _mapIndex._nameList[_mapIndex._ZUS_Zusatzzeile]);
		// _mapWegzug.add(_mapIndex._ZuzOIDKG, "WegOIDKG");
	}

	private class Mapping {
		private final Vector fromList = new Vector();
		private final Vector toList = new Vector();
		private final Vector toMetaList = new Vector();
		private final Vector fromCodeList = new Vector();
		private final Vector toCodeList = new Vector();
		private final Vector codeList = new Vector();
		private final Vector toCodeMetaList = new Vector();

		private final BOContext _context;
		private final MetaObject _metaObject;
		private final MetaService _metaService;

		public Mapping (BOContext context, MetaObject metaObject, MetaService metaService) {
			_context = context;
			_metaObject = metaObject;
			_metaService = metaService;
		}

		public void add (int from, String to) throws XMetaModelNotFoundException, XMetaModelQueryException {
			if (from >= ImportRoulesEDVKG.MAX_COLUMS/2) {

			}
			fromList.add(new Integer(from));
			int toNr;
			MetaAttribute metaAttr;

			metaAttr = _metaObject.getAllAttributeForName(to, _context);
			toNr = metaAttr.getIndex();
			toList.add(new Integer(toNr));
			toMetaList.add(metaAttr);
		}


		public int getFrom(int index) {
			return ((Integer)(fromList.elementAt(index))).intValue();
		}

		public int getTo(int index) {
			return ((Integer)(toList.elementAt(index))).intValue();
		}

		public MetaAttribute getMeta(int index) {
			return (MetaAttribute)(toMetaList.elementAt(index));
		}

		public int size() {
			return toList.size();
		}

		public void addCode (int from, String to, String codeName) throws XMetaModelNotFoundException, XMetaModelQueryException, XSecurityException {
			fromCodeList.add(new Integer(from));
			int toNr;
			MetaAttribute metaAttr;

			metaAttr = _metaObject.getAllAttributeForName(to, _context);
			toNr = metaAttr.getIndex();
			toCodeList.add(new Integer(toNr));
			toCodeMetaList.add(metaAttr);
			codeList.add(new CodeTableEDVKG(_trans, _context, _metaService, codeName));
		}

		public int getCodeFrom(int index) {
			return ((Integer)(fromCodeList.elementAt(index))).intValue();
		}

		public int getCodeTo(int index) {
			return ((Integer)(toCodeList.elementAt(index))).intValue();
		}
		
		public MetaAttribute getCodeMeta(int index) {
			return (MetaAttribute)(toCodeMetaList.elementAt(index));
		}		

		public CodeTableEDVKG getCodeTable(int index) {
			return (CodeTableEDVKG)(codeList.elementAt(index));
		}

		public int sizeCode() {
			return toCodeList.size();
		}

	}


}
