/*
 * Created on 16.03.2005
 *
 * (c) by Boxalino
 */
package ch.objcons.ecom.proj.edvkg;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.proj.edvkg.wcache.CacheSABAWochen;
import ch.objcons.ecom.system.security.XSecurityException;

/**
 * @author Dominik Raymann
 */
public class BOSABABestatterWoche extends BOObject {
	
	
	
	
	public void writeToDB(boolean isInsert, Connection con) throws SQLException,
			XMetaException, XSecurityException, XBOConcurrencyConflict {
		
		BOContext boContext = new BOContext(Locale.getDefault());
		
		String jahr = this.getAttribute("Jahr", boContext);
		String woche = this.getAttribute("Woche", boContext);
		
		CacheSABAWochen.getInstance(boContext).clearCachedWeek(woche,jahr);
		
		
		super.writeToDB(isInsert, con);
		
		
		
		
	}
	public BOSABABestatterWoche(BLTransaction arg0, long arg1) {
		super(arg0, arg1);
		
	}
}
