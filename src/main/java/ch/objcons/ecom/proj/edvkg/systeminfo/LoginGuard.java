package ch.objcons.ecom.proj.edvkg.systeminfo;

import java.beans.PropertyChangeEvent;

import ch.objcons.ecom.api.IAction;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.adapters.EPropertyChangeEvent;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.box.login.BoxLogin;
import ch.objcons.ecom.box.login.ModelAndViewLogin;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;
import com.boxalino.server.Manager;


public class LoginGuard implements java.beans.PropertyChangeListener {

	public static ILogger LOGGER = Logger.getLogger(LoginGuard.class);

	private boolean _loginEnabled = false;
	
	/**
	 * BXSingleton. Use {@link #getInstance()} instead
	 */
	@Deprecated
	public LoginGuard() {
		BoxLogin.getInstance().addPropertyChangeListener(this);
	}
	
	/**
	 * propertyChange method comment.
	 */
	public void propertyChange(PropertyChangeEvent event) {
		if (_loginEnabled) return;
		
		if (event instanceof EPropertyChangeEvent) {
			String propName = event.getPropertyName();
			if (!propName.equals("loggedon")) return;
			if (!Boolean.TRUE.equals(event.getNewValue())) return;
			
			IRequest request = ((EPropertyChangeEvent) event).getRequest();
			ModelAndViewLogin mav = ModelAndViewLogin.getROInfo(request, BoxLogin.getLoginInfoKey("login"), "edvkg");
			BOObject boLoginUser = mav.getObject();
			try {
				if (Boolean.TRUE.equals(boLoginUser.getValue("Administrator"))) return;
			} catch (XMetaModelNotFoundException e) {
				LOGGER.error("XMetaModelNotFoundException occurred!", e);
			} catch (XSecurityException e) {
				LOGGER.error("XSecurityException occurred!", e);
			}
			logout(request);
		}
	}
	
	private void logout(IRequest request) {
		try {
			IAction action = BoxLogin.getInstance().executeCommand(request, "cmd_logout", null, 0, 0);
			if (action != null && action.doValidate(request)) {
				action.doPerform(request);
			}
			request.addUserError("Login nicht erlaubt");
		} catch (Exception e) {
			LOGGER.fatal("Failed to logout user", e);
		}
	}
	
	public void setUserLoginEnabled(boolean enabled) {
		_loginEnabled = enabled;
	}
	
	public boolean isUserLoginEnabled() {
		return _loginEnabled;
	}
	
	public static LoginGuard getInstance() {
		return Manager.getService(LoginGuard.class);
	}
	
}
