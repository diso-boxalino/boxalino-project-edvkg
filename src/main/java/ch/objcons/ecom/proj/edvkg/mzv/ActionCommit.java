package ch.objcons.ecom.proj.edvkg.mzv;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.XBoxValueManagementException;
import ch.objcons.ecom.api.adapters.EActionAdapter2;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.box.util.AttributeInformation;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.ListAttribute;
import ch.objcons.ecom.meta.MetaService;
import ch.objcons.ecom.meta.ReferenceAttribute;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.system.security.XSecurityException;

/*
 * stores all object in a transaction to the database
 *
 */
public class ActionCommit extends EActionAdapter2 {

	private BLTransaction _trx = null;
	private BoxMZV _editBox = null;
	private boolean _isCommit = false;
	private MetaService _service = null;
	private String _command = null;

	public ActionCommit(BoxMZV editBox, IRequest request, String slotID, MetaService service, String nextPage, BLTransaction trx, boolean isCommit) {
		super (editBox, request, slotID, nextPage);
		_trx = trx;
		_editBox = editBox;
		_isCommit = isCommit;
		_service = service;
	}

	public ActionCommit(BoxMZV editBox, IRequest request, String slotID, MetaService service, String nextPage, BLTransaction trx, boolean isCommit, String command) {
		super (editBox, request, slotID, nextPage);
		_trx = trx;
		_editBox = editBox;
		_isCommit = isCommit;
		_service = service;
		_command = command;
	}

	private void handleValues(Hashtable values, BOContext boContext) throws XBoxValueManagementException, XDataTypeException, XMetaException, XSecurityException {
		String key = "Organisation_OIDListOrgP";
		String[] oids = (String[]) values.get(key);

		// search object and set attribute value
		AttributeInformation attrInfo = new AttributeInformation (key, getInfoKey(),_service, AttributeInformation.FILTER_ALL, getRequest(), _trx, AttributeInformation.MODE_READ);
		ListAttribute attr = (ListAttribute) attrInfo.getMetaAttribute(boContext);
		ReferenceAttribute refAttr = (ReferenceAttribute) attr.getListAttribute();
		BOObject obj = attrInfo.getTargetObject();

		// may happen if the request contains a old/wrong oid!!!
		if (obj == null) {
			this.addPublisherError("object with oid '" + getRequest().getParameters().getParameter(attrInfo.getOIDParam()) + "' does not exist (" + attrInfo.getOIDParam() + ")");
			return;
		}

		// order both lists
		TreeMap existMap = new TreeMap();
		Vector listObjects = (Vector) obj.getListReferencedObjects(attr.getIndex(), true, boContext).clone();
		for (int i=0; i<listObjects.size(); i++) {
			BOObject temp = (BOObject) listObjects.elementAt(i);
			Long oid = new Long(temp.getAttribute("PersonOIDListOrgPOID", boContext));

			existMap.put(oid, temp);
		}
		TreeSet newSet = new TreeSet();
		for (int j=0; j<oids.length; j++) {
			newSet.add(new Long(oids[j]));
		}

		// iterate ober both lists and match them
		Iterator existIt = existMap.keySet().iterator();
		Iterator newIt = newSet.iterator();
		Long existOID = null;
		Long newOID = null;
		boolean wasSmaller = false;
		while (existIt.hasNext() && newIt.hasNext()) {
			existOID = (Long) existIt.next();
			newOID = (Long) newIt.next();

			if (existOID.compareTo(newOID) == 0) {
				// nothing to do
			}
			else if (existOID.compareTo(newOID) < 0) {
				// delete old entries
				BOObject temp = (BOObject) existMap.get(existOID);
				obj.removeListReferencedObject(attr.getIndex(), temp, true);

				while (existIt.hasNext()) {
					existOID = (Long) existIt.next();
					if (existOID.compareTo(newOID) < 0) {
						temp = (BOObject) existMap.get(existOID);
						obj.removeListReferencedObject(attr.getIndex(), temp, true);
					}
					else {
						break;
					}
				}
				
				// last existOID was smaller then the actual newOID
				if ((!existIt.hasNext()) && (existOID.compareTo(newOID) < 0)) {
					wasSmaller = true;
				}
			}
			else {
				// add new entries
				// create new object and add to list
				BOObject temp = _trx.getObject(refAttr.getReferencedObject(), 0, false);
				temp.setAttribute("PersonOIDListOrgPOID", ""+newOID, boContext, null);
				temp.setValue("eintritt", new java.sql.Date(System.currentTimeMillis()), boContext);

				// add object to list
				obj.addListReferencedObject(attr.getIndex(), temp, true);

				while (newIt.hasNext()) {
					newOID = (Long) newIt.next();
					if (existOID.compareTo(newOID) > 0) {
						// create new object and add to list
						temp = _trx.getObject(refAttr.getReferencedObject(), 0, false);
						temp.setAttribute("PersonOIDListOrgPOID", ""+newOID, boContext, null);
						temp.setValue("eintritt", new java.sql.Date(System.currentTimeMillis()), boContext);

						// add object to list
						obj.addListReferencedObject(attr.getIndex(), temp, true);
					}
					else {
						break;
					}
				}
			}
		}

		// put the rest to the list
		// remove old values
		while (existIt.hasNext()) {
			existOID = (Long) existIt.next();
			// delete old entries
			BOObject temp = (BOObject) existMap.get(existOID);
			obj.removeListReferencedObject(attr.getIndex(), temp, true);
		}
		
		// add new values
		// test if we have to put the actual value too
		if (wasSmaller) {
			// create new object and add to list
			BOObject temp = _trx.getObject(refAttr.getReferencedObject(), 0, false);
			temp.setAttribute("AbstractPersonPersonenListeOID", ""+newOID, boContext, null); // "PersonOIDListOrgPOID"
//			temp.setValue("eintritt", new java.sql.Date(System.currentTimeMillis()), boContext);

			// add object to list
			obj.addListReferencedObject(attr.getIndex(), temp, true);			
		}
				
		// add new values
		while (newIt.hasNext()) {
			newOID = (Long) newIt.next();
			// create new object and add to list
			BOObject temp = _trx.getObject(refAttr.getReferencedObject(), 0, false);
			temp.setAttribute("PersonOIDListOrgPOID", ""+newOID, boContext, null);
			temp.setValue("eintritt", new java.sql.Date(System.currentTimeMillis()), boContext);

			// add object to list
			obj.addListReferencedObject(attr.getIndex(), temp, true);
		}

/*
		// remove objects not in OIDs
		Vector listObjects = (Vector) obj.getListReferencedObjects(attr.getIndex(), true useAccessControl, boContext).clone();
		for (int i=0; i<listObjects.size(); i++) {
			BOObject temp = (BOObject) listObjects.elementAt(i);

			int j=0;
			for (j=0; j<oids.length; j++) {
				if (oids[j].equals(temp.getAttribute("PersonOIDListOrgPOID", boContext))) {
					break;
				}
			}
			if (j == oids.length) {
				// remove object from list
				obj.removeListReferencedObject(attr.getIndex(), temp);
			}
		}

		// add objects in OIDs
		for (int i=0; i<oids.length; i++) {
			int j=0;

			for (j=0; j<listObjects.size(); j++) {
				BOObject temp = (BOObject) listObjects.elementAt(j);
				if (temp.getAttribute("PersonOIDListOrgPOID", boContext).equals(""+oids[i])) {
					break;
				}
			}
			if (j == listObjects.size()) {
				// create new object and add to list
				BOObject temp = _trx.getObject(refAttr.getReferencedObject(), 0, false);
				temp.setAttribute("PersonOIDListOrgPOID", ""+oids[i], boContext, null);
				temp.setValue("eintritt", new java.sql.Date(System.currentTimeMillis()), boContext);

				// add object to list
				obj.addListReferencedObject(attr.getIndex(), temp);
			}
		}
*/
	}

	public void performX () throws XMetaException, XDataTypeException, XSecurityException {
		BOContext boContext = (BOContext)getRequest().getContext();

		try {
/*
			if ("cmd_createSABAWochen".equals(_command)) {
				// attrInfo = new AttributeInformation(classPath, boxName, null, AttributeInformation.FILTER_ALL, request);
				EBoxWithMetaDataAdapter constsBox = (EBoxWithMetaDataAdapter)ch.objcons.ecom.engine.EEngine.getInstance().getBox("global");
				String anzWs = constsBox.getMDValue(this.getRequest(), new EValueID("Consts_SABAWochen"));
				int anzWochen = Integer.parseInt(anzWs);
				GregorianCalendar cal = new GregorianCalendar();
				String query = null; // XXX "select sp.*, sw.bx_woche from bx_wochenliste as wl, bx_SABAPerson as sp left join bx_sabawoche as sw on sw.bx_woche = wl.oid where wl.oid >= {=date_week_of_year} and wl.oid <= {=date_week_of_year({=global_Consts_SABAWochen})} and sw.bx_komplett != 'j' and sp.bx_oidsabaperstyp = 25 order by sw.bx_woche";
				MetaObject mobjw = MetaObjectLookup.getInstance().getMetaObject("SABAWoche");
				MetaObject mobjp = MetaObjectLookup.getInstance().getMetaObject("SABAPerson");

				BOObject bobjp, bobjw = null;
				int actWeek, actYear;
				StringQuery strqw = new StringQuery(_trx, 0);
				StringQuery strqp = new StringQuery(_trx, 0);
				strqw.init("SABAWocheQuery", "SABAWocheQuery", mobjw);
				strqp.init("SABAPersonQuery", "SABAPersonQuery", mobjp);
				Vector toDoList = null;

				for (int count = 0; count < anzWochen; count ++) {
					actWeek = cal.get(Calendar.WEEK_OF_YEAR);
					actYear = cal.get(Calendar.YEAR);

					// set all SABAWOchen the tocheck to true
					query = "select sw.* from bx_SABAPerson as sp, bx_sabawoche as sw "+
						  " where sw.bx_oidkg=sp.bx_oidkg and sw.bx_woche = "+actWeek+" and "+
						  " sp.bx_oidsabaperstyp = 25 order by sp.bx_oidkg";
					strqw.setSqlStatement(query);
					toDoList = strqw.getResult(_trx, boContext);
					for (int ic = 0; ic < toDoList.size(); ic++) {
						bobjw = (BOObject)toDoList.elementAt(ic);
						bobjw.setValue("toCheck", new Boolean(true), boContext);
					}

					// create all missing SABAWochen
					query = "select sp.* from bx_SABAPerson as sp "+
			              "left join bx_sabawoche as sw on sw.bx_oidkg=sp.bx_oidkg and sw.bx_woche = "+actWeek+
			              " where sp.bx_oidsabaperstyp = 25 and sw.oid is null order by sp.bx_oidkg";
					strqp.setSqlStatement(query);
					toDoList = strqp.getResult(_trx, boContext);
					for (int ic = 0; ic < toDoList.size(); ic++) {
						bobjp = (BOObject)toDoList.elementAt(ic);
						bobjw = _trx.getObject(mobjw, 0, false);
						bobjw.setAttribute("OIDKG", bobjp.getAttribute("OIDKG", boContext), boContext, null);
						bobjw.setAttribute("OIDSABAKG", bobjp.getAttribute("OID", boContext), boContext, null);
						bobjw.setAttribute("Woche", Integer.toString(actWeek), boContext, null);
						bobjw.setAttribute("Jahr", Integer.toString(actYear), boContext, null);
						bobjw.setValue("toCheck", new Boolean(true), boContext);
					}
					cal.add(Calendar.DATE, 7);
				}
				_trx.commit();
			}
			else {
*/
				Hashtable values =_editBox.getValues(getRequest());
				if (_isCommit) {
					// set values of session in different objects
					handleValues(values, boContext);
				}

				if (_isCommit) {
					try {
						_trx.commit();
					}
					catch (XBOConcurrencyConflict e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
//		}
		catch (XBoxValueManagementException x) {
			ch.objcons.log.Log.logDesignerAlarm (x.getMessage(), x);
		}
	}
}
