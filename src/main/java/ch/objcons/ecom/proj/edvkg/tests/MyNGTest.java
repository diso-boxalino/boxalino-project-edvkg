package ch.objcons.ecom.proj.edvkg.tests;

import java.util.Iterator;

import org.testng.AssertJUnit;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MyNGTest {
	@DataProvider(name = "importPersonen")
	public Iterator<Object[]> getInputFromImport() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final ImportDataIterator<Object[]> dataQueue = new ImportDataIterator<Object[]>();
		
		final Thread tTest = Thread.currentThread();
		
		Runnable r = new Runnable() {

			@Override
			public void run() {
				Object[][] data = new Object[][] {
					{ "Tempo", "Hallo" },
					{ "Velo", "Velo" },
					{ "Test3", "Test3" },
					{ "Test4", "Hallo" }
						
				};
				dataQueue.put(data[0]);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dataQueue.put(data[1]);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dataQueue.put(data[2]);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dataQueue.put(data[3]);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				dataQueue.stopWaiting();
				tTest.interrupt();
			}
			
		};
		Thread t = new Thread(r, "TestDataProducer");
		t.start();

		return dataQueue;
//		return new Object[][] {
//				{ "Tempo", "Hallo" },
//				{ "Velo", "Velo" }
//		};
	}
	
	@Test(dataProvider = "importPersonen")
	public void f(String a, String b) {
		AssertJUnit.assertEquals("Fehlgeschlagen", a, b);
  }
  
  

}
