package ch.objcons.ecom.proj.edvkg.systeminfo;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOObject;

public class BOSystemInfo extends BOObject {

	public static final String CLASS_NAME = "SystemInfo";
	
	public BOSystemInfo(BLTransaction arg0, long arg1) {
		super(arg0, arg1);
	}
	
}
