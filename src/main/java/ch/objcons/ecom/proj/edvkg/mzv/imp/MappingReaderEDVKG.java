package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.io.IOException;
import java.sql.Connection;

import ch.objcons.db.dbimport.IndexedTableMapping;
import ch.objcons.db.dbimport.MappingReaderPlus;
import ch.objcons.db.dbimport.XMappingException;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.meta.XMetaException;

public class MappingReaderEDVKG extends MappingReaderPlus {

	private Connection _dbConn;

    public MappingReaderEDVKG(IRequest request, String mappFile, Connection dbConn) throws IOException, XMappingException, XMetaException {
        super(mappFile, request);
		_dbConn = dbConn;
    }
    protected IndexedTableMapping getTableMapping() throws XMetaException{
		return new TableMappingEDVKG(_tableName, _boxName, _className, _numFields);
	}
}
