package ch.objcons.ecom.proj.edvkg.mzv.bva;

import java.util.ArrayList;
import java.util.List;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.system.security.XSecurityException;

public class BOBVA extends BOObject {

	public static final String CN = "BVA";
	
	public BOBVA(BLTransaction trans, long oid) {
		super(trans, oid);
	}
	
	@Override
	public String getAttribute(String attrName, BOContext context) throws XMetaModelQueryException, XSecurityException {
		if ("statusLabel".equals(attrName)) {
			return getStatus().getLabel();
		}
		return super.getAttribute(attrName, context);
	}
	
	public List<BOBVAEntry> getEntries() {
		return refList("entries");
	}

	public BVAStatus setStatus(BVAStatus newStatus) {
		return BVASupport.setStatus(newStatus, this);
	}
	
	public BVAStatus getStatus() {
		return BVASupport.getStatus(this);
	}
	
	public List<Long> getMembers() {
		List<BOBVAEntry> entries = getEntries();
		List<Long> members = new ArrayList<Long>();
		for (BOBVAEntry e : entries) {
			members.add((Long) e.val("OIDPerson"));
		}
		return members;
	}
	
	public boolean sameMembers(List<Long> members) {
		List<Long> selfMembers = getMembers();
		return selfMembers.size() == members.size() && selfMembers.containsAll(members);
	}
	
}
