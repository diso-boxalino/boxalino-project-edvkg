package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.util.Date;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.proj.edvkg.mzv.Consts;
import ch.objcons.ecom.proj.edvkg.mzv.OhneAnrechtWeil;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

public class BOPersonOA extends BOObject {
	public static final String CLASS_NAME = "PersonOA";
	static ILogger LOGGER = Logger.getLogger("boxalino.edvkg.import");

	public BOPersonOA(BLTransaction trans, long oid) {
		super(trans, oid);
	}
	
	public static final BOPersonOA createOrSetPersonOA(long personOID, Date datVerarbeitung, OhneAnrechtWeil oaw, 
			boolean lwMutiert, BLTransaction trans) throws XSecurityException, XMetaException, XDataTypeException {
		
		BOPersonOA bo = (BOPersonOA) trans.getObject(Consts.PersonOAMeta, personOID, false);
		if (bo == null) {
			bo = (BOPersonOA) trans.createNewObject("PersonOA");
			bo.setOID(personOID);
		}
		bo.setValue(Consts.PersonOA_datVerarbeit, datVerarbeitung);
		bo.setValue(Consts.PersonOA_lwMutiert, Boolean.valueOf(lwMutiert));
		bo.setAttribute(Consts.PersonOA_grundID, String.valueOf(oaw.getID()), trans.getContext());
		return bo;
	}

}
