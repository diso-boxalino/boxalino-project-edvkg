package ch.objcons.ecom.proj.edvkg.mzv.imp;

public enum Meldungsdomain {
	Import ('I'), PostImport ('P');

	private char _id;
	
	Meldungsdomain (char id) {
		_id = id;
	}
	
	public char getIdentifier() {
		return _id;
	}
}
