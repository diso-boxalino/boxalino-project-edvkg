package ch.objcons.ecom.proj.edvkg.mzv.imp;

public class Sizeof
{
    public static void main (String [] args) throws Exception
    {
        // Warm up all classes/methods we will use
        runGC ();
        usedMemory ();

        // Array to keep strong references to allocated objects
        final int count = 100000;
        Object [] objects = new Object [count];
        
        long heap1 = 0;

        // Allocate count+1 objects, discard the first one
        for (int i = -1; i < count; ++ i)
        {
            Object object = null;
            
            // Instantiate your data here and assign it to object
            
            //object = new Object ();
            object = new PersOIDs ();
            //object = new Boolean (false);
            //object = new Integer (i);
            //object = new Long (i);
            //object = new String ();
            //object = new byte [128][1];
            
            if (i >= 0)
                objects [i] = object;
            else
            {
                object = null; // Discard the warm up object
                runGC ();
                heap1 = usedMemory (); // Take a before heap snapshot
            }
        }

        runGC ();
        long heap2 = usedMemory (); // Take an after heap snapshot:
        
        final int size = Math.round (((float)(heap2 - heap1))/count);
        System.out.println ("'before' heap: " + heap1 +
                            ", 'after' heap: " + heap2);
        System.out.println ("heap delta: " + (heap2 - heap1) +
            ", {" + objects [0].getClass () + "} size = " + size + " bytes");

        for (int i = 0; i < count; ++ i) objects [i] = null;
        objects = null;
    }

    public static void runGC () throws Exception
    {
        // It helps to call Runtime.gc()
        // using several method calls:
        for (int r = 0; r < 4; ++ r) _runGC ();
    }

    private static void _runGC () throws Exception
    {
        long usedMem1 = usedMemory (), usedMem2 = Long.MAX_VALUE;
        for (int i = 0; (usedMem1 < usedMem2) && (i < 500); ++ i)
        {
            s_runtime.runFinalization ();
            s_runtime.gc ();
            Thread.currentThread ().yield ();
            usedMem2 = usedMem1;
            usedMem1 = usedMemory ();
        }
    }

    public static long usedMemory ()
    {
        return s_runtime.totalMemory () - s_runtime.freeMemory ();
    }
    
    private static final Runtime s_runtime = Runtime.getRuntime ();

} // End of class

class PersOIDs {
	//boolean _changed = false;
	
	boolean _isPassiv; //mh: ben�tzt
	boolean _isKind;
	
	private boolean _oidKGKindBestaetigt = false;
	boolean _oidKGVorstandChanged = false;
	boolean _oidVorstandMissionChanged = false;
	boolean _oidKGPartnerChanged = false;
	boolean _oidKGPartnerMissionChanged = false;
	boolean _passivChanged = false;
	
	long _oidperson = 0;
	long _oidKG = 0;

	long _oidVorstand = 0;
	long _oidEhepartner = 0;

	long _oidVater = 0;
	long _oidMutter = 0;

	long _oidFamilie = 0;
	
	long _oidMission = 0;

	long _oidKGPartner = 0;
	long _oidKGPartnerMission = 0;
	long _oidKGVorstand = 0;
	long _oidKGVorstandMission = 0;
	
//	long _oidKind = 0;	// OID irgendeines Kindes, aber nur, wenn in gleicher Familie
	long _oidKGKindOrig = 0;
	long _oidKGKind = 0;
	boolean _newOIDKGKindSet = false;
//	long _lastOIDKGKind = 0;
//	long _oidKGMission = 0;
	
	boolean istKind () {
		return _isKind;
	}
	
	boolean isOIDKGKindBestaetigt () {
		if (_oidKGKindBestaetigt) return true;
		if ((_oidKGKind == 0) && (_oidKGKindOrig == 0)) return true;
		return false;
	}
	
	long getOIDKGKind () {
		if (isOIDKGKindBestaetigt()) return _oidKGKind;
		return 0;
	}
	
	void setOIDKGKind (long oid) {
		if (_oidKGKindBestaetigt) {
			return;
		}
		if (oid != 0) {
			if (_oidKGKindOrig == oid) {
				_oidKGKind = _oidKGKindOrig;
				_oidKGKindBestaetigt = true;
			}
			else {
				_oidKGKind = oid;
				_newOIDKGKindSet = true;
				
			}
		}
		else {
			if (!_newOIDKGKindSet) {
				_oidKGKind = 0;
			}
		}
	}
	
	void setOIDKGPartner (long oid) {
		if (oid == _oidKGPartner) return;
		_oidKGPartnerChanged = true;
		_oidKGPartner = oid;
	}
	
	void setOIDPartnerMission (long oid) {
		if (oid == _oidKGPartnerMission) return;
		_oidKGPartnerMissionChanged = true;
		_oidKGPartnerMission = oid;
	}
	
	void setOIDKGVorstand (long oid) {
		if (oid == _oidKGVorstand) return;
		_oidKGVorstandChanged = true;
		_oidKGVorstand = oid;
	}
	
	void setOIDVorstandMission (long oid) {
		if (oid == _oidKGVorstandMission) return;
		_oidVorstandMissionChanged = true;
		_oidKGVorstandMission = oid;
	}
	
	void setPassiv (boolean passiv) {
		if (passiv == _isPassiv) return;
		_passivChanged = true;
		_isPassiv = passiv;
	}
};


