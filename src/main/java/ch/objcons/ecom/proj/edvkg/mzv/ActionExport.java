package ch.objcons.ecom.proj.edvkg.mzv;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.ecom.api.EContent;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.XBoxValueManagementException;
import ch.objcons.ecom.api.adapters.EActionAdapter2;
import ch.objcons.ecom.bom.BOFileUpload;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.datatypes.EDataTypeValidation;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.UtilsEDVKG.EDVKGFieldGetter;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.util.NotImplementedException;

/*
 * makes the export for the Zeitung
 *
 */
public class ActionExport extends EActionAdapter2 {

	private long _aid;
	private long _fileOID;
	// private IFileStorageEntry _file;
	private FileInputStream outFile;
	boolean _iskath = false;
	private final Set<String> _duplicateFinder = new HashSet<String>();
	private int _numOfDuplicates = 0;

	private static final int[] refMap = new int[Consts.Person2History.length];
	private static final int[] rkMap = new int[Consts.Person2History.length];
	/**
	 * wird anstatt 000000 gesetzt und indiziert, dass ein Postfach gesetzt ist (nur PLZ)
	 */
	private static final String RK_POSTFACH_DEFAULT = "999999";
	
	static {
		loadMaps();
	}
	
	private static void loadMaps() {
		refMap[Consts.ZusOIDStrasseIndex] = 36; 
		refMap[Consts.ZusStrassenNameIndex] = 13; 
		refMap[Consts.ZusHausNrIndex] = 17;
		refMap[Consts.ZusAdrZusatzIndex] = 25;
		refMap[Consts.ZusPLZOrtIndex] = 19;
		refMap[Consts.ZusOrtIndex] = 21;
		refMap[Consts.ZusPostfachIndex] = 29;
		refMap[Consts.ZusPLZPostfachIndex] = 31;
		refMap[Consts.ZusArtIndex] = 34;

		refMap[Consts.StrasseKGIndex] = 15;
		refMap[Consts.AdrZusatzIndex] = 26;
		refMap[Consts.HausNrIndex] = 18;
		
		refMap[Consts.PLZOrtIndex] = 20;
		refMap[Consts.OrtIndex] = 22;
		refMap[Consts.PostfachIndex] = 30;
		refMap[Consts.PLZPostfachIndex] = 32;
		
		refMap[Consts.ZusNameIndex] = 11;
		refMap[Consts.ZusVornameIndex] = 8;
		refMap[Consts.ZusAnredeIndex] = 4;

		refMap[Consts.ZeitungAnredeIndex] = 3;
		refMap[Consts.ZeitungNachnameIndex] = 10;
		refMap[Consts.ZeitungVornameIndex] = 7;
		refMap[Consts.CodeGeschlechtIndex] = 2;

		refMap[Consts.NameIndex] = 12;
		refMap[Consts.VornameIndex] = 9;	
			
//		refMap[Consts.AdresseStadtIndex] = 37;
			
		rkMap[Consts.ZusOIDStrasseIndex] = 34; 
		rkMap[Consts.ZusStrassenNameIndex] = 13; 
		rkMap[Consts.ZusHausNrIndex] = 15;
		rkMap[Consts.ZusAdrZusatzIndex] = 22;
		rkMap[Consts.ZusPLZOrtIndex] = 17;
		rkMap[Consts.ZusOrtIndex] = 19;
		rkMap[Consts.ZusPostfachIndex] = 24;
		rkMap[Consts.ZusPLZPostfachIndex] = 26;
		rkMap[Consts.ZusArtIndex] = 32;

		rkMap[Consts.StrasseKGIndex] = 14;
		rkMap[Consts.AdrZusatzIndex] = 23;

		
		rkMap[Consts.HausNrIndex] = 16;
		
		rkMap[Consts.PLZOrtIndex] = 18;
		rkMap[Consts.OrtIndex] = 20;
		rkMap[Consts.PostfachIndex] = 25;
		rkMap[Consts.PLZPostfachIndex] = 27;
		
		rkMap[Consts.ZusNameIndex] = 11;
		rkMap[Consts.ZusVornameIndex] = 8;
		rkMap[Consts.ZusAnredeIndex] = 4;

		rkMap[Consts.ZeitungAnredeIndex] = 3;
		rkMap[Consts.ZeitungNachnameIndex] = 10;
		rkMap[Consts.ZeitungVornameIndex] = 7;

		rkMap[Consts.CodeGeschlechtIndex] = 2;

		rkMap[Consts.NameIndex] = 12;
		rkMap[Consts.VornameIndex] = 9;
//		rkMap[Consts.AdresseStadtIndex] = 35;
	}
	
	public ActionExport(ch.objcons.ecom.proj.edvkg.mzv.BoxMZV box, IRequest request,
		String slotID, String nextPage, boolean kath) {
		super (box, request, slotID, nextPage);
	    _iskath = kath;
	}
		/**
	 * fills the output in the file for the ref
	 * Das Resultat ist eine Ausgabe die folgende Info enth�llt :
	*
	*
		KUNDENNUMMER                              10
		SPRACHCODE                                1
		ANREDECODE                                2
		TITEL                                     15
		VORNAME                                   15
		NACHNAME                                  30
		N�HERE BEZEICHNUNG 1                      30
		N�HERE BEZEICHNUNG 2                      30
		N�HERE BEZEICHNUNG 3                      30
		STRASSENBEZEICHNUNG                       30
		STRASSENNUMMER                            4      0
		STRASSENNUMMER-ZUSATZ                     3
		WOHNUNGSNUMMER                            4
		POSTFACHNUMMER                            6      0
		POSTLEITZAHL                              9
		POSTLEITZAHL-ZUSATZ                       2
		ORTSCHAFT                                 18
		LANDNUMMER                                3
		AUTOKENNZEICHEN                           3
		BOTENBEZIRK                               3      0
		OBJEKT                                    4
		SUBOBJEKT                                 4
		ABOART                                    2
		MENGE                                     5      0
		ZUSTELLART                                2
		UNTERZUSTELLART 1                         2
		UNTERZUSTELLART 2                         2
		UNTERZUSTELLART 3                         2
		UNTERZUSTELLART 4                         2
		ANZAHL PLAKATE                            3      0
		VERTR�GERTOUR                             4
		LAUFSEQUENZ                               5
		WERBECODE                                 8
		STATISTIKNUMMER                           5
		MEHR EXEMPLARE OHNE ADRES                 1
		CODE 1                                    2
		CODE 2                                    2
		CODE 3                                    2
		CODE 4                                    2
		CODE 5                                    2
		CODE 6                                    2
		RECORDNUMMER                              7      0
		** FILLER                                 13
		UPID       FIELD UPDATE / IDENTIFIER      7      0
	*
	*	F�r die Adresse sind folgende Felder als Resultat der Query gew�nscht :
	*	ZIP, Geschlecht, Titel, Kirchgemeinde, Vorname, Name, Strasse, HausNr, PLZOrt, Ort, Postbez
	*
	 */
	private void fillKath(OutputStream outStream)
	{
		Connection conn = null ;
		ResultSet queryResult = null;
		// my Strings
		String query;
		StringBuilder oneLine;
		String zip, geschlecht, anrede, titel, kgnr, vorname, name, strasse, hausnr;
		String plz, ort, postzustell, adrZusatz, postfach, zusPostfach, anz, codeZeitung, betragscode;
		String plzPostfach, zusPlzPostfach;
		Object myObj;
		int i;

		try {
			conn = DBConnectionPool.instance().getConnection();

			// mysql query :
			query = "select " +
					"pers.oid, " +							//  1
					"pers.bx_CodeGeschlecht, " + 			//  2
					"pers.bx_ZeitungAnrede, " + 			//  3
					"pers.bx_zusAnrede, " + 				//  4
					"titel.bx_name, " + 					//  5
					"zei.bx_OIDKirchgemeinde, " +			//  6
					"pers.bx_ZeitungVorname, " + 			//  7
					"pers.bx_ZusVorname, " + 				//  8
					"pers.bx_rufname, " +					//  9
					"pers.bx_ZeitungNachname, " + 			// 10
					"pers.bx_ZusName, " + 					// 11
					"pers.bx_name, " +						// 12
					"pers.bx_ZusStrassenName, " + 			// 13
					"pers.bx_Strassekg, " + 				// 14
					"pers.bx_ZusHausNr, " + 				// 15
					"pers.bx_HausNr, " +					// 16
					"pers.bx_ZusPLZOrt, " + 				// 17
					"pers.bx_PLZOrt, " +					// 18
					"pers.bx_ZusOrt, " +					// 19
					"pers.bx_Ort, " +						// 20
					"pers.bx_Bezirk, " +                    // 21
					"pers.bx_zusAdrZusatz, " + 				// 22
					"bx_AdrZusatz, " +						// 23
					"pers.bx_zusPostfach, " + 				// 24
					"bx_Postfach, " +						// 25
		            "pers.bx_zusPLZPostfach, " + 			// 26
					"bx_PLZPostfach, " +					// 27
					"zei.bx_Anzahl, " + 					// 28
					"czei.bx_ZeitungsCode, " +				// 29
					"pers.bx_adresssperresicherheit, " +	// 30
					"zei.bx_Betrag, " +                     // 31
					"pers.bx_zusart, " +    				// 32
					"codeOL.bx_name, " +					// 33
					"pers.bx_ZusOIDStrasse, " +				// 34
					"pers.bx_AdresseStadt " +				// 35
					
				"FROM (bx_person as pers, bx_ZeitungCode as czei, bx_kirchgemeinde as kg) "+
				"INNER JOIN bx_zeitung as zei ON " +
				"zei.bx_PersonListZeitungOID = pers.oid AND "+
	            "(coalesce(zei.bx_zeitungsnr,'') != '99999' or coalesce(pers.bx_zeitungverzicht, 'n') = 'n') " +
				"LEFT JOIN bx_codehaus as haus on "+
					 "pers.bx_OIDCodeStrasse = haus.bx_OIDStrasse AND "+
				  "pers.bx_HausNr = haus.bx_HausNr "+
				"LEFT JOIN bx_codehaus as zushaus on "+
					"pers.bx_ZusOIDStrasse = zushaus.bx_OIDStrasse AND "+
					"pers.bx_ZusHausNr = zushaus.bx_HausNr "+
				"LEFT JOIN bx_codestrasse as str on "+
					"pers.bx_OIDCodeStrasse = str.oid "+
				"LEFT JOIN bx_codestrasse as zusstr on "+
					"pers.bx_ZusOIDStrasse = zusstr.oid "+
		        "LEFT JOIN bx_codetitel as titel ON "+
			        "pers.bx_OIDCodeTitel = titel.OID " +
			        "LEFT JOIN bx_CodeOrtLand codeOL ON " +
					"pers.BX_ZUSOIDCODEORTLAND = codeOL.oid " +
				"WHERE "+
//				    "pers.OID = 100402577 AND " + 
					"zei.bx_oidkirchgemeinde = kg.oid AND "+
					"kg.bx_konfession = 'RK' AND "+
		            "zei.bx_oidkirchgemeinde = czei.bx_oidkg AND "+
		            "pers.bx_statusperson = 'j'";

			long logID = 0;
		    if (ch.objcons.ecom.query.Config.getInstance().showQueryString()) {
				logID = ch.objcons.log.Log.logDesignerInfo("Query: ActionExport.fillKath: " + query);
				ch.objcons.log.Log.flushDesigner();
			}

			Statement querySTM = conn.createStatement();
			queryResult =  querySTM.executeQuery(query);

			if (ch.objcons.ecom.query.Config.getInstance().getSQLTraceLevel() > 0) {
				ch.objcons.log.Log.logDesignerInfoPS(logID, "");
			}
			
			final ResultSet fgQueryResult = queryResult;
			
			EDVKGFieldGetter fieldGetter = new EDVKGFieldGetter () {
				
				@Override
				public Object getField(int index) throws SQLException {
					return fgQueryResult.getObject(rkMap[index]);
				}

				@Override
				public Object getField(String attrPath)
						throws XSecurityException, SQLException, XMetaException {
					
					if ("OIDCodeTitel_Name".equals(attrPath)) {
						return fgQueryResult.getObject(5);
					} else if ("ZusOIDStrasse_Name".equals(attrPath)) {
						return  fgQueryResult.getObject(13);
					} else if ("ZusOIDCodeOrtLand_Name".equals(attrPath)) {
						return  fgQueryResult.getObject(33);
					} else if ("getAnrede".equals(attrPath)) {
						return UtilsEDVKG.getAnrede(this);
				 	} else {
						return attrPath;
					}
				}

				private String _perQueryZusArt = null;
				
				@Override
				public void setZusart(String zusArt) {
					_perQueryZusArt = zusArt;
				}
				
				@Override
				public String getZusArt() {
					return _perQueryZusArt;
				}

				@Override
				public Object getField(int derefIndex, int targetIndex)
						throws XMetaModelQueryException, XSecurityException,
						SQLException {
					throw new NotImplementedException();
				}

				@Override
				public Object getObject(int index)
						throws XMetaModelQueryException, XSecurityException,
						SQLException {
					throw new NotImplementedException();
				}

				@Override
				public Object getList(int index)
						throws XMetaModelQueryException, XSecurityException,
						SQLException {
					throw new NotImplementedException();
				}
				
				@Override
				public BOObject getBO() {
					throw new UnsupportedOperationException();
				}
				
				@Override
				public void setBO(BOPersonHistory bo) {
					throw new UnsupportedOperationException();
				}
			};
			
			while (queryResult.next()) {
				String zusArt = (String)queryResult.getObject(32);
				fieldGetter.setZusart(zusArt);
				
				myObj = queryResult.getObject(1);
				zip = (myObj == null)?"":myObj.toString();
				{//Anrede
					titel = getZeitungsFeld(UtilsEDVKG.ix_ZeitungsAnrede, rkMap, fieldGetter, queryResult);
				}
				myObj = queryResult.getObject(6);
				kgnr = (myObj == null)?"":myObj.toString();
				{//Vorname
					vorname = getZeitungsFeld(UtilsEDVKG.ix_ZeitungsVorname, rkMap, fieldGetter, queryResult);
				}
				{//Name
					name = getZeitungsFeld(UtilsEDVKG.ix_ZeitungsNachname, rkMap, fieldGetter, queryResult);
				}
				{//Strasse
					strasse = getZeitungsFeld(UtilsEDVKG.ix_Strasse, rkMap, fieldGetter, queryResult);					
				}
				{//Hausnr 
					hausnr = getZeitungsFeld(UtilsEDVKG.ix_HausNr, rkMap, fieldGetter, queryResult);
				}
				{//PLZ
					plz = getZeitungsFeld(UtilsEDVKG.ix_PLZPostfach, rkMap, fieldGetter, queryResult);
				}
				{//Ort
					ort = getZeitungsFeld(UtilsEDVKG.ix_Ort, rkMap, fieldGetter, queryResult);
				}
				myObj = queryResult.getObject(21);
				postzustell = (myObj == null)?"":myObj.toString();
				
				postfach = getZeitungsFeld(UtilsEDVKG.ix_Postfach, rkMap, fieldGetter, queryResult);
				if (postfach != null) {
					/*
					 * Bei dem Katholischen Zeitungsfile ist folgende Zuweisung nicht gew�nscht
					 * 
						strasse = "Postfach ";
						hausnr  = "";
					*/
					if (isTrimpty(postfach)) {
						postfach = RK_POSTFACH_DEFAULT;
					} else {
						postfach = postfach.trim(); //Das Postfach entspricht manchmal einem White-Space. Dieser wird dadurch entfernt
					}
				}
				adrZusatz = getZeitungsFeld(UtilsEDVKG.ix_AdrZusatzConsiderPostfach, rkMap, fieldGetter, queryResult);
				
				myObj = queryResult.getObject(28);
				anz = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(29);
				codeZeitung = (myObj == null)?" ":myObj.toString();

				myObj = queryResult.getObject(31);
				betragscode = (myObj == null) ? "" : (((i = ((Float)myObj).intValue()) < 10) ? "0" + i : "" + i);
				
				oneLine = new StringBuilder(345);
				//offset 0
				oneLine.append(fixWide(zip, 10));    // Adresscode
				oneLine.append(fixWide("1", 1));   // Sprachcode, ist immer '1'
				oneLine.append(fixWide(" ", 2));    // Anredecode, ist immer ' '
				oneLine.append(fixWide(titel, 15));   // titel
				oneLine.append(fixWide(vorname, 15));
				oneLine.append(fixWide(name, 30));
				oneLine.append(fixWide(adrZusatz, 30));
				oneLine.append(fixWide("", 30));     // RSNB2
				oneLine.append(fixWide("", 30));     // RSNB3
				oneLine.append(fixWide(strasse, 30));
				oneLine.append(fixWideNull(hausnr, 4));
				oneLine.append(fixWide("   ", 3));// hausnr Zusatz
				oneLine.append(fixWide("", 4));// Wohnungsnr
				oneLine.append(fixWideNull(postfach, 6));
/*
*/
				//offset 210
				oneLine.append(fixWide(plz, 9));
				oneLine.append(fixWideNull("00", 2)); // PLZ Zusatz
				oneLine.append(fixWide(ort, 18));
				oneLine.append(fixWide("   ", 3)); // LandNr
				oneLine.append(fixWide("   ", 3)); // Autokennzeichen
				oneLine.append(fixWideNull(postzustell, 3)); // postzustell
				oneLine.append(fixWide(codeZeitung.substring(1, 3), 4));// objekt
				oneLine.append(fixWide(codeZeitung.substring(3), 4));// subobjekt
				oneLine.append(fixWide("NO", 2));// aboart
				oneLine.append(fixWideNull(anz, 5));// menge
				oneLine.append(fixWide("NP", 2)); // zustellart
				oneLine.append(fixWide("        ", 8)); // unter zustellart 1, 2, 3, 4
				oneLine.append(fixWideNull("000", 3)); // anz Plakate
				oneLine.append(fixWide("    ", 4)); // Vertr�gertour
				oneLine.append(fixWide("     ", 5)); // laufsequenz
				oneLine.append(fixWide(codeZeitung, 8)); // webecode
				oneLine.append(fixWide("     ", 5)); // Statistiknr
				oneLine.append(fixWide(" ", 1)); // exempl. ohne Adresse
				//299
				oneLine.append(fixWide(betragscode, 2)); // Neu 300-301: Zeitungs-Betragscode
				oneLine.append(fixWide("          ", 10)); // Neu code 2-6 anstatt 1-6
				oneLine.append(fixWideNull("0000000", 7)); // Recordnr
				oneLine.append(fixWide("             ", 13)); // filler
				oneLine.append(fixWide("       ", 7)); // update / identifier
				oneLine.append(fixWide("0", 1)); // somthing tha is also in the old one
				oneLine.append("\r\n");
				if (!_duplicateFinder.contains(oneLine.toString())) {
					_duplicateFinder.add(oneLine.toString());
					outStream.write(oneLine.toString().getBytes());
				}
				else {
					_numOfDuplicates++;
				}
			}
//			System.out.println("katholisch: " + _numOfDuplicates + " Dupletten in Abfrage eliminiert");
			ch.objcons.log.Log.logDesignerInfo("katholisch: " + _numOfDuplicates + " Dupletten in Abfrage eliminiert");
			outStream.close();

		}
		catch (SQLException e) {
				ch.objcons.log.Log.logSystemAlarm("SQL Exception beim generieren der Zeitung", e);
		}
		catch (IOException e) {
				ch.objcons.log.Log.logSystemAlarm("IO Exception beim generieren der Zeitung", e);
		} catch (XBoxValueManagementException e) {
			ch.objcons.log.Log.logSystemAlarm("XBoxValueManagementException occured!", e);
		} catch (XSecurityException e) {
			ch.objcons.log.Log.logSystemAlarm("XSecurityException occured!", e);
		} catch (XMetaException e) {
			ch.objcons.log.Log.logSystemAlarm("XMetaException occured!", e);
		}
		finally {
			try {
				if (queryResult != null) {
					queryResult.close();
				}
				if (conn != null) {
					DBConnectionPool.instance().ungetConnection(conn);
				}
			}
			catch (Exception e) {
				ch.objcons.log.Log.logSystemAlarm("Unexpected Exception", e);
			}
		}
	}
		
	private String getZeitungsFeld(int labelIndex, int[] map, EDVKGFieldGetter fieldGetter, ResultSet rs) throws SQLException, XBoxValueManagementException, XSecurityException, XMetaException {
		Object field = UtilsEDVKG.getEDVKGField(labelIndex, map, fieldGetter);
		if (field instanceof String) return (String)field;
		if (field instanceof List<?>) {
			List<Object> attrList = (List<Object>) field;
			String res = null;
			for (Object item : attrList) {
				if (item instanceof Integer) {
					Integer index = (Integer)item;
					// index: Hole Wert aus Result Set
					Object object = rs.getObject(index);
					if (object != null) res = String.valueOf(object);
				} else if (item instanceof String) {
					// string: Resultat schon da
					Object object = fieldGetter.getField(String.valueOf(item));
					if (object != null) res = String.valueOf(object); 
				}
				
				
				if (res != null) {
					//leere Anrede-Felder �berspringen
					if (labelIndex == UtilsEDVKG.ix_ZeitungsAnrede) {
						if (res.trim().isEmpty()) {
							continue;
						}
					}
					return res;
				}
			}
			return res;
		}
		return null;
	}
	
/**
 * fills the output in the file for the ref
 * Das Resultat ist eine Ausgabe die folgende Info enth�llt :
*	Adresscode    	1
*	KIRCHGEMEINDE	2      0
*	PLZ Haus             4      0
*	PLZ-NAME             25
*	POSTZUSTELLBEZ HAUS  3      0
*	STRASSENNAME         30
*	HAUSNUMMER ZIS       5
*	IDENT-NUMMER Abo Nr. 6      0
*	Anrede               12
*	Titel/Fname/Rufname  30
*	Adresszeile          30
*	Strasse/Hausnummer   27
*	PLZ/Ort              30
*	ANZ. EXEMPLARE       2      0
*
*	F�r die Adresse sind folgende Felder als Resultat der Query gew�nscht :
*	ZIP, Geschlecht, Titel, Kirchgemeinde, Vorname, Name, Strasse, HausNr, PLZOrt, Ort, Postbez
*
 */
private void fillRef(OutputStream outStream) {
	Connection conn = null;
	ResultSet queryResult = null;
	// my Strings
	String query;
	StringBuilder oneLine;
	String zip, geschlecht, anrede, titel, kgnr, vorname, name, strasse, hausnr;
	String ort, postzustell, postfach, plzPostfach, adrZusatz, anz;
	String adrStadt, adrCode;
	Object myObj;

	try {
		conn = DBConnectionPool.instance().getConnection();

		final int	ZIP = 1;

		final int	CodeGeschlecht = 2;

		final int	Titel = 5;
		final int	KG = 6;
		
		final int	ZusStrassenNameStadt = 14;

		final int	ZusHausPostZustellBezirk = 23;
		final int	HausPostZustellBezirk = 24;

		final int	Anzahl = 27;

		final int	AdresseStadt = 28;
		
		final int   ZUSART = 34;
		final int 	OrtLandName = 35;
		// the query for the Person that have the Strasse
		query =
			" select "
				+ "pers.oid, "							//  1
				+ "pers.bx_CodeGeschlecht, "			//  2
				+ "pers.bx_ZeitungAnrede, "				//  3
				+ "pers.bx_zusAnrede, "					//  4
				+ "titel.bx_name, "						//  5
				+ "zei.bx_OIDKirchgemeinde, "			//  6
				+ "pers.bx_ZeitungVorname, "			//  7
				+ "pers.bx_ZusVorname, "				//  8
				+ "pers.bx_rufname, "					//  9
				+ "pers.bx_ZeitungNachname, "			// 10
				+ "pers.bx_ZusName, "					// 11
				+ "pers.bx_name, "						// 12
				+ "pers.bx_ZusStrassenName, "			// 13
				+ "zusstr.bx_name, "					// 14
				+ "pers.bx_Strassekg, "					// 15
				+ "str.bx_name, "						// 16
				+ "pers.bx_ZusHausNr, "					// 17
				+ "pers.bx_HausNr, "					// 18
				+ "pers.bx_ZusPLZOrt, "					// 19
				+ "pers.bx_PLZOrt, "					// 20
				+ "pers.bx_ZusOrt, "					// 21
				+ "pers.bx_Ort, "						// 22
				+ "zushaus.bx_Postzustellbezirk, "		// 23
				+ "haus.bx_Postzustellbezirk, "			// 24
				+ "pers.bx_zusAdrZusatz, "				// 25
				+ "bx_AdrZusatz, "						// 26
				+ "zei.bx_Anzahl, "						// 27
				+ "pers.bx_AdresseStadt, "				// 28
				+ "pers.bx_zusPostfach, "				// 29
				+ "pers.bx_Postfach, "					// 30
				+ "pers.bx_zusPLZPostfach, "			// 31
				+ "pers.bx_PLZPostfach, "				// 32
				+ "pers.bx_adresssperresicherheit, "	// 33
				+ "pers.bx_zusart, "					// 34
				+ "codeOL.bx_name, "					// 35
				+ "pers.bx_ZusOIDStrasse, "				// 36
				+ "pers.bx_AdresseStadt "				// 37
				+ "FROM (bx_person as pers, "
				+ "bx_kirchgemeinde as kg) "
				+ "INNER JOIN bx_zeitung as zei on " 
				+ "zei.bx_PersonListZeitungOID = pers.oid AND "
				+ "(coalesce(zei.bx_zeitungsnr, '') != 99999 OR coalesce(pers.bx_zeitungverzicht, 'n') = 'n') "
				+ "LEFT JOIN bx_codehaus as haus on "
				+ "pers.bx_OIDCodeStrasse = haus.bx_OIDStrasse AND "
				+ "pers.bx_HausNr = haus.bx_HausNr "
				+ "LEFT JOIN bx_codehaus as zushaus on "
				+ "pers.bx_ZusOIDStrasse = zushaus.bx_OIDStrasse AND "
				+ "pers.bx_ZusHausNr = zushaus.bx_HausNr "
				+ "LEFT JOIN bx_codestrasse as str on "
				+ "pers.bx_OIDCodeStrasse = str.oid "
				+ "LEFT JOIN bx_codestrasse as zusstr on "
				+ "pers.bx_ZusOIDStrasse = zusstr.oid "
				+ "LEFT JOIN bx_codetitel as titel ON "
				+ "pers.bx_OIDCodeTitel = titel.OID "
				+ "LEFT JOIN bx_CodeOrtLand codeOL ON "
				+ "pers.BX_ZUSOIDCODEORTLAND = codeOL.oid "
				+ "WHERE "
//				+ "pers.OID = 100756455 AND "
				+ "zei.bx_oidkirchgemeinde = kg.oid AND "
				+ "kg.bx_konfession = 'EV-REF' AND "
				+ "pers.bx_statusperson = 'j'";

		long logID = 0;
		if (ch.objcons.ecom.query.Config.getInstance().showQueryString()) {
			logID = ch.objcons.log.Log.logDesignerInfo("Query: ActionExport.fillRef: " + query);
			ch.objcons.log.Log.flushDesigner();
		}
		Statement querySTM = conn.createStatement();
		queryResult = querySTM.executeQuery(query);

		if (ch.objcons.ecom.query.Config.getInstance().getSQLTraceLevel() > 0) {
			ch.objcons.log.Log.logDesignerInfoPS(logID, "");
		}

		final ResultSet fgQueryResult = queryResult;
		
		EDVKGFieldGetter fieldGetter = new EDVKGFieldGetter () {
			
			@Override
			public Object getField(int index) throws SQLException {
				return fgQueryResult.getObject(refMap[index]);
			}

			@Override
			public Object getField(String attrPath)
					throws XSecurityException, SQLException, XMetaException {
				
				if ("OIDCodeTitel_Name".equals(attrPath)) {
					return fgQueryResult.getObject(Titel);
				} else if ("ZusOIDStrasse_Name".equals(attrPath)) {
					return  fgQueryResult.getObject(ZusStrassenNameStadt);
				} else if ("ZusOIDCodeOrtLand_Name".equals(attrPath)) {
					return  fgQueryResult.getObject(OrtLandName);
				} else if ("getAnrede".equals(attrPath)) {
					return UtilsEDVKG.getAnrede(this);
			 	} else {
					return attrPath;
				}
			}

			private String _perQueryZusArt;
			
			@Override
			public void setZusart(String zusArt) {
				_perQueryZusArt = zusArt;
			}
			
			@Override
			public String getZusArt() {
				return _perQueryZusArt;
			}

			@Override
			public Object getField(int derefIndex, int targetIndex)
					throws XMetaModelQueryException, XSecurityException,
					SQLException {
				throw new NotImplementedException();
			}

			@Override
			public Object getObject(int index) throws XMetaModelQueryException,
					XSecurityException, SQLException {
				throw new NotImplementedException();
			}

			@Override
			public Object getList(int index) throws XMetaModelQueryException,
					XSecurityException, SQLException {
				throw new NotImplementedException();
			}
			
			@Override
			public BOObject getBO() {
				throw new UnsupportedOperationException();
			}
			
			@Override
			public void setBO(BOPersonHistory bo) {
				throw new UnsupportedOperationException();
			}
			
		};
		
		
		while (queryResult.next()) {
			fieldGetter.setZusart((String)queryResult.getObject(ZUSART));
			// ZIP
			myObj = queryResult.getObject(ZIP);
			zip = (myObj == null) ? "" : myObj.toString();
			zip = zip.trim();

			// Geschlecht
			myObj = queryResult.getObject(CodeGeschlecht);
			geschlecht = (myObj == null) ? "" : myObj.toString();
			geschlecht = geschlecht.trim();

			
			anrede = emptyOrTrim(getZeitungsFeld(UtilsEDVKG.ix_ZeitungsAnredeWithoutTitle, refMap, fieldGetter, queryResult));
			titel = emptyOrTrim(getZeitungsFeld(UtilsEDVKG.ix_ZeitungsNamenzeileTitle, refMap, fieldGetter, queryResult));

			// Kirchgemeinde
			myObj = queryResult.getObject(KG);
			kgnr = (myObj == null) ? "" : myObj.toString();
			kgnr = kgnr.trim();
			
			vorname = emptyOrTrim(getZeitungsFeld(UtilsEDVKG.ix_ZeitungsVorname, refMap, fieldGetter, queryResult));
			name = emptyOrTrim(getZeitungsFeld(UtilsEDVKG.ix_ZeitungsNachname, refMap, fieldGetter, queryResult));
			strasse = emptyOrTrim(getZeitungsFeld(UtilsEDVKG.ix_Strasse, refMap, fieldGetter, queryResult));
			hausnr = emptyOrTrim(getZeitungsFeld(UtilsEDVKG.ix_HausNr, refMap, fieldGetter, queryResult));
			ort = emptyOrTrim(getZeitungsFeld(UtilsEDVKG.ix_Ort, refMap, fieldGetter, queryResult));
			
			{// Postzustell
				myObj = queryResult.getObject(ZusHausPostZustellBezirk);
				if (myObj == null) {
					myObj = queryResult.getObject(HausPostZustellBezirk);
				}
				postzustell = (myObj == null) ? "" : myObj.toString();
				postzustell = postzustell.trim();
			}
			
			adrZusatz = emptyOrTrim(getZeitungsFeld(UtilsEDVKG.ix_AdrZusatzConsiderPostfach, refMap, fieldGetter, queryResult));
			
			{// Anzahl
				myObj = queryResult.getObject(Anzahl);
				anz = (myObj == null) ? "" : myObj.toString().trim();
			}
			
			{// Adresse Stadt
				myObj = queryResult.getObject(AdresseStadt);
				adrStadt = (myObj == null) ? "" : myObj.toString().trim();
			}
			
			postfach = getZeitungsFeld(UtilsEDVKG.ix_Postfach, refMap, fieldGetter, queryResult);
			
			plzPostfach = emptyOrTrim(getZeitungsFeld(UtilsEDVKG.ix_PLZPostfach, refMap, fieldGetter, queryResult));
			
			// calculate the Adresscode
			if (postfach != null) {
				adrCode = "4";
			}
			else if ("j".equals(adrStadt)) {
				adrCode = "3";
			}
			else if (plzPostfach.isEmpty()) {
				adrCode = "1";
			}
			else {
				adrCode = "2";
			}
						
			oneLine = new StringBuilder(208);
			oneLine.append(fixWide(adrCode, 1)); // Adresscode
			oneLine.append(fixWide(kgnr, 2)); // kg
			oneLine.append(fixWide(plzPostfach, 4)); // postleizahl
			oneLine.append(fixWide(ort, 25)); // ort
			oneLine.append(fixWide(postzustell, 3));
			oneLine.append(fixWide(strasse, 30));
			oneLine.append(fixWide(hausnr, 5));
			oneLine.append(fixWide(zip, 9));
			oneLine.append(fixWide(anrede, 12));

			String nameColumn = concat(" ", titel, vorname, name);
			if (nameColumn.length() > 30) {
				int desiredLength = 30 - (nameColumn.length() - vorname.length()); // np if negative
				String firstVorname = shortenBySplit(vorname, desiredLength);
				String snc = concat(" ", titel, firstVorname, name);
				if (snc.length() > 30) {
					snc = concat(" ", vorname, name);
					if (snc.length() > 30) {
						snc = concat(" ", firstVorname, name);
					}
				}
				nameColumn = snc;
			}
			oneLine.append(fixWide(nameColumn, 30));
			
			oneLine.append(fixWide(adrZusatz, 30));
			
			if (postfach == null) {
				oneLine.append(fixWide(strasse + " " + hausnr, 27));
			} else {
				oneLine.append(fixWide("Postfach " + postfach, 27));
			}
			
			oneLine.append(fixWide(plzPostfach + " " + ort, 30));
			oneLine.append(fixWide(anz, 2));
			oneLine.append("\r\n");
			if (!_duplicateFinder.contains(oneLine.toString())) {
				_duplicateFinder.add(oneLine.toString());
				outStream.write(oneLine.toString().getBytes());
			} else {
				_numOfDuplicates++;
			}
		}
		outStream.close();
		ch.objcons.log.Log.logDesignerInfo(
			"reformiert: " + _numOfDuplicates + " Dupletten in Abfrage eliminiert");
	}
	catch (SQLException e) {
		ch.objcons.log.Log.logSystemAlarm("SQL Exception beim generieren der Zeitung", e);
	}
	catch (IOException e) {
		ch.objcons.log.Log.logSystemAlarm("IO Exception beim generieren der Zeitung", e);
	} catch (XMetaModelQueryException e) {
		ch.objcons.log.Log.logSystemAlarm("XMetaModelQueryException beim generieren der Zeitung!", e);
	} catch (XBoxValueManagementException e) {
		ch.objcons.log.Log.logSystemAlarm("XBoxValueManagementException  beim generieren der Zeitung!", e);
	} catch (XSecurityException e) {
		ch.objcons.log.Log.logSystemAlarm("XSecurityException  beim generieren der Zeitung!", e);
	} catch (XMetaException e) {
		ch.objcons.log.Log.logSystemAlarm("XMetaException  beim generieren der Zeitung!", e);
	}
	finally {
		try {
			if (queryResult != null) {
				queryResult.close();
			}
			if (conn != null) {
				DBConnectionPool.instance().ungetConnection(conn);
			}
		}
		catch (Exception e) {
			ch.objcons.log.Log.logSystemAlarm("Unexpected Exception", e);
		}
	}

}

	private static String concat(String sep, String... strings) {
		StringBuilder sb = new StringBuilder();
		for (String s : strings) {
			if (s == null || s.isEmpty()) continue;
			
			if (sb.length() != 0) sb.append(sep);
			
			sb.append(s);
		}
		return sb.toString();
	}
	
	/**
	 * Splits the specified string by " " and returns as many segments
	 * as possible without exceeding desiredLength, guaranteed to 
	 * return at least one segment, regardless of desiredLength.
	 * <ul>
	 * <li>"a b", 2 > "a"</li>
	 * <li>"abc", 2 > "abc"</li>
	 * <li>"a b cd", 5 > "a b"</li>
	 * </ul>
	 */
	private static String shortenBySplit(String s, int desiredLength) {
		String[] spl = s.split(" ");
		if (spl.length == 1) return s;
		
		StringBuilder sb = new StringBuilder(spl[0]);
		for (String seg : spl) {
			int nextLength = sb.length() + " ".length() + seg.length();
			if (nextLength > desiredLength) break;
			
			sb.append(" ").append(seg);
		}
		return sb.toString();
	}

	private boolean isTrimpty(String postfach) {
		return postfach.trim().isEmpty();
	}
	
	private String emptyOrTrim(String s) {
		return s  == null ? "" : s.trim();
	}
	
	private String fixWide(String text, int length) throws UnsupportedEncodingException
	{
		if (text == null) {
			text = "";
		}
		text = new String(text.getBytes(), "iso-8859-1");
		if (text.length() != length) {
	        if (text.length() > length) {
			    text = text.substring(0,length);
		    }
		    else {
			    char [] myArr = new char[length-text.length()];
				for (int i=0;i<myArr.length;i++) {
				    myArr[i] = ' ';
				}
				text = (new StringBuffer(text)).append(myArr).toString();
		    }
		}
		if (length != text.length()) {
			System.out.println("hello");
		}
		return text;
	}
	
	private String fixWideNull(String text, int length) throws UnsupportedEncodingException
		{
			if (text == null) {
				text = "";
			}
			text = new String(text.getBytes(), "iso-8859-1");
			if (text.length() != length) {
				if (text.length() > length) {
					text = text.substring(0,length);
				}
				else {
					char [] myArr = new char[length-text.length()];
					for (int i=0;i<myArr.length;i++) {
						myArr[i] = '0';
					}
					text = (new StringBuffer(new String(myArr))).append(text).toString();
				}
			}
			if (length != text.length()) {
				System.out.println("hello");
			}
			return text;
	}
	@Override
	public void performX () throws XMetaException, XDataTypeException {
		EContent result;
		File outFile = null;
		FileInputStream inStream;
		OutputStream outStream;

		try {
			if (_iskath) {
				// we create the stream
				outFile = new File(BOFileUpload.Config.getInstance().getTempBaseDir(), "zeikath.txt");
				if (outFile.exists()) {
					outFile.delete();
				}
				outFile.createNewFile();
				outStream = new BufferedOutputStream(new FileOutputStream(outFile));

				this.fillKath(outStream);
			}
			else {
				// we create the stream
				outFile = new File(BOFileUpload.Config.getInstance().getTempBaseDir(), "zeiref.txt");
				if (outFile.exists()) {
					outFile.delete();
				}
				outFile.createNewFile();
				outStream = new BufferedOutputStream(new FileOutputStream(outFile));

				this.fillRef(outStream);
			}

			// open the stream for reading
	    	inStream = new FileInputStream(outFile);

			// content type zur�ckgeben

			if (_iskath) {
				result = new EContent("kathZeitungen.txt", "txt" , inStream );
			}
			else {
				result = new EContent("refZeitungen.txt", "txt" , inStream );
			}

			this.setContent(result);
		}
		catch (FileNotFoundException e) {
			    ch.objcons.log.Log.logSystemAlarm("File not Found Exception beim generieren der Zeitung", e);
		}
		catch (IOException e) {
			    ch.objcons.log.Log.logSystemAlarm("IO Exception beim generieren der Zeitung", e);
		}
	}
	
	public static boolean isValidAddress(String anrede, String vorname, String name, String ort, String strasse, String hausNr, String postfach, String plzPostfach) {
		boolean valid = true;
		if (EDataTypeValidation.isNullOrEmptyString(anrede)
				|| (EDataTypeValidation.isNullOrEmptyString(vorname) && EDataTypeValidation.isNullOrEmptyString(name))) {
			valid = false;
		} else if (EDataTypeValidation.isNullOrEmptyString(ort) ||
				EDataTypeValidation.isNullOrEmptyString(strasse) || EDataTypeValidation.isNullOrEmptyString(hausNr)) {
			if (EDataTypeValidation.isNullOrEmptyString(postfach) && EDataTypeValidation.isNullOrEmptyString(plzPostfach)) {
				valid = false;
			}
		}
		return valid;
	}
}
