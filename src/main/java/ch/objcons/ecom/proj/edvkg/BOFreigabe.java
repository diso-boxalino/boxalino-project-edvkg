/*
 * Created on 16.03.2005
 *
 * (c) by Boxalino
 */
package ch.objcons.ecom.proj.edvkg;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.BOValidationException;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.wcache.CacheSABAWochen;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.log.Log;

/**
 * @author Dominik Raymann
 */
public class BOFreigabe extends BOObject {
	
	
	
	public void writeToDB(boolean isInsert, Connection con) throws SQLException,
			XMetaException, XSecurityException, XBOConcurrencyConflict {
		
		BOContext boContext = new BOContext(Locale.getDefault());
		CacheSABAWochen.getInstance(boContext).clearCache();
		
		
		super.writeToDB(isInsert, con);
	}
	public BOFreigabe(BLTransaction arg0, long arg1) {
		super(arg0, arg1);
		
	}
	
	public void validate(BOContext context) throws BOValidationException,
			XMetaModelQueryException, XSecurityException {
		
		super.validate(context); 
		if (this.isNew()) return;
		System.out.print("validate...");
		MetaObject mobFreigabe = MetaObjectLookup.getInstance().getMetaObject("Freigabe");	
		String von = this.getAttribute("von", context);
		String bis = this.getAttribute("bis", context);
		System.out.println(von+" - "+ bis);
		
		BLTransaction trans = BLTransaction.startTransaction(context);
		
		BOFreigabe that = (BOFreigabe)trans.getObject(mobFreigabe, getOID(), false); 
		Vector v = null;
		
		if (this.isDeleted()) {
			v = usesFreigabe(this, null, context);
		} else {
			v = usesFreigabe(that, this, context);
		}
		if (v == null) throw new BOValidationException("INTERNAL ERROR: couldn't evaluate validity for the Freigabe object");
		if (v.size()>0)	{
			StringBuilder msg = new StringBuilder(v.size() * 90);
			for (Iterator it = v.iterator(); it.hasNext();) {
				BOObject zuord = (BOObject)it.next();
				BOObject boKg = zuord.getReferencedObject("OIDKG", context);
				String datum = zuord.getAttribute("Datum", context);
				String viertelS = zuord.getAttribute("Vierteltag", context);
				String viertel = "";
				if (viertelS.equals("1")) viertel = "08:00 - 10:00";
				if (viertelS.equals("2")) viertel = "10:00 - 12:00";
				if (viertelS.equals("3")) viertel = "13:00 - 15:00";
				if (viertelS.equals("4")) viertel = "15:00 - 17:00";
				msg.append("Ist am "+datum+" von der Kirchgemeinde "+boKg.getAttribute("Name", context)+" bereits gebucht ("+viertel+")<br>\n");
			}
			throw new BOValidationException(msg.toString());
		}
		
		
	}
	
	public static Vector usesFreigabe(BOFreigabe oldFreigabe, BOFreigabe newFreigabe, BOContext context) {
		try {
			String myOID = oldFreigabe.getAttribute("SABAPersonFreigabenOID", context);
			BLTransaction trans = BLTransaction.startTransaction(context);
			String queryFilter = "";
								
			String oldVonS = oldFreigabe.getAttribute("von", context);
			String oldBisS = oldFreigabe.getAttribute("bis", context);
			
			
			Vector oldKGs = oldFreigabe.getListReferencedObjects("Kirchgemeinden", context);
			HashSet delKGs = new HashSet();
			
			String oldInStatement = null;
			
			for (Iterator it = oldKGs.iterator(); it.hasNext();) {
				BOObject kg = (BOObject)it.next();
				delKGs.add(new Long(kg.getOID()));
				if (oldInStatement == null) {
					oldInStatement = ""+kg.getOID();
				} else {
					oldInStatement = oldInStatement +", "+kg.getOID();
				}
			}
			
			if (newFreigabe == null) {
				queryFilter = "(SABAZuordnung_OIDSabaPers == "+myOID+")";
				queryFilter = queryFilter + " AND (SABAZuordnung_Datum >= '"+oldVonS+"' AND SABAZuordnung_Datum <= '"+oldBisS+"' AND SABAZuordnung_OIDKG IN("+oldInStatement+"))";
				System.out.println(queryFilter);
				return BoxalinoUtilities.executeQuery("SABAZuordnung", queryFilter, trans, context);
			}
			
	
			String newVonS = newFreigabe.getAttribute("von", context);
			String newBisS = newFreigabe.getAttribute("bis", context);
			
			Vector newKGs = newFreigabe.getListReferencedObjects("Kirchgemeinden", context);
			
			
			for (Iterator it = newKGs.iterator(); it.hasNext();) {
				BOObject kg = (BOObject)it.next();
				delKGs.remove(new Long(kg.getOID()));
			}
			
			String inStatement = null;
			for (Iterator it = delKGs.iterator(); it.hasNext(); ) {
				Long kg = (Long)it.next();
				if (inStatement == null) {
					inStatement = ""+kg;
				} else {
					inStatement = inStatement +", "+kg;
				}
			}
			
			
			String newQueryFilter = "(SABAZuordnung_OIDSabaPers == "+myOID+") " +
					"AND (((SABAZuordnung_Datum >= '"+oldVonS+"'" +
							"AND SABAZuordnung_Datum < '"+newVonS+"') " +
						"OR (SABAZuordnung_Datum <= '" + oldBisS + "'" +
							"AND SABAZuordnung_Datum > '" + newBisS + "')) " +
						"AND SABAZuordnung_OIDKG IN("+(oldInStatement)+"))";
			Vector newVector = BoxalinoUtilities.executeQuery("SABAZuordnung", newQueryFilter, trans, context);
			return newVector;
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.logSystemAlarm("couldn't check Freigabe",e);
		}
		return null;
		
	}
	
}
