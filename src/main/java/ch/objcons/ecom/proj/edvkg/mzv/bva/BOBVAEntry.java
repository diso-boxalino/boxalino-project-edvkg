package ch.objcons.ecom.proj.edvkg.mzv.bva;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.system.security.XSecurityException;

public class BOBVAEntry extends BOObject {

	public static final String CN = "BVAEntry";
	
	public BOBVAEntry(BLTransaction trans, long oid) {
		super(trans, oid);
	}

	@Override
	public String getAttribute(String attrName, BOContext context) throws XMetaModelQueryException, XSecurityException {
		if ("statusLabel".equals(attrName)) {
			return getStatus().getLabel();
		}
		return super.getAttribute(attrName, context);
	}
	
	public BOBVA getBVA() {
		return ref(BOBVA.CN + "entriesOID");
	}
	
	public BVAStatus setStatus(BVAStatus newState) {
		return BVASupport.setStatus(newState, this);
	}
	
	public BVAStatus getStatus() {
		return BVASupport.getStatus(this);
	}

}
