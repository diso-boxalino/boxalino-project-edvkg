package ch.objcons.ecom.proj.edvkg.tests;

import junit.framework.TestCase;

/**
 * @author s_rikli
 * Dieser Test ist nun unn�tig, da die Stimmrecht-Bestimmung in ein SQL-Query verlegt wurde
 */
public class TestStimmrecht extends TestCase {


//	private static final int DEFAULT_OID_CODEAUFENTHALT = 13;
//	private static final int RK = 1;
//	private static final int EV_REF = 2;
//	
//	private static PostImport _cc = null;
//	private Calendar _cal = null;
//	
//	 static {
//		try {
//			ServiceLocator.init(new HostServiceLocator());
//			ShopProperties.getInstance().setHTMLRoot("file:///C:/Workspace/mzv");
//			BoxLoader.getInstance().initBoxes();
//			Connection con = DBConnectionPool.instance().getConnection();
//			_cc = new PostImport(con, new LogFileWriter("test.log"), new LogFileWriter("test2.log"), (BOContext) BoxalinoUtilities.getDummyRequest().getContext());
//			_cc._testInitialize();
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
//	}
//	
//	@Override
//	protected void setUp(){
//		resetCal();
//	}
//	 
//	@Test
//	public void testStimmrechtRK18() {
//		_cal.add(Calendar.YEAR, -18);
//		Date geburtsdatum = _cal.getTime();
//		assertTrue(testRKInSpan(geburtsdatum));
//	}
//	
//	@Test
//	public void testStimmrechtRK18_14DaySpan() {
//		_cal.add(Calendar.YEAR, -18);
//		_cal.add(Calendar.DAY_OF_YEAR, -14);
//		Date geburtsdatum = _cal.getTime();
//		assertTrue(testRKInSpan(geburtsdatum));
//	}
//	
//	@Test
//	public void testStimmrechtRK18_14DaySpan_all() {
//		_cal.add(Calendar.YEAR, -18);
//		for (int i = 0; i < 14; i++) {
//			_cal.add(Calendar.DAY_OF_YEAR, -1);
//			Date geburtsdatum = _cal.getTime();
//			assertTrue(testRKInSpan(geburtsdatum));
//		}
//	}
//	
//	@Test
//	public void testStimmrechtRK18_OutOfSpanTooYoung() {
//		_cal.add(Calendar.YEAR, -12);
//		Date geburtsdatum = _cal.getTime();
//		assertFalse(testRKInSpan(geburtsdatum));
//	}
//	
//	@Test
//	public void testStimmrechtRK18_OutOfSpanTooOld()  {
//		_cal.add(Calendar.YEAR, -20);
//		Date geburtsdatum = _cal.getTime();
//		assertFalse(testRKInSpan(geburtsdatum));
//	}
//	
//	@Test
//	public void testStimmrechtRK18_OutOfSpanBy1TooYoung()  {
//		_cal.add(Calendar.YEAR, -18);
//		_cal.add(Calendar.DAY_OF_YEAR, 1);
//		Date geburtsdatum = _cal.getTime();
//		assertFalse(testRKInSpan(geburtsdatum));
//	}
//	
//	@Test
//	public void testStimmrechtRK18_OutOfSpanBy1TooOld()  {
//		_cal.add(Calendar.YEAR, -18);
//		_cal.add(Calendar.DAY_OF_YEAR, -15);
//		Date geburtsdatum = _cal.getTime();
//		assertFalse(testRKInSpan(geburtsdatum));
//	}
//	
//	@Test
//	public void testStimmrechtRef16() {
//		_cal.add(Calendar.YEAR, -16);
//		Date geburtsdatum = _cal.getTime();
//		assertTrue(testEVREFInSpan(geburtsdatum));
//	}
//	
//	@Test
//	public void testStimmrechtRef16_14DaySpan() {
//		_cal.add(Calendar.YEAR, -16);
//		_cal.add(Calendar.DAY_OF_YEAR, -14);
//		Date geburtsdatum = _cal.getTime();
//		assertTrue(testEVREFInSpan(geburtsdatum));
//	}
//	
//	@Test
//	public void testStimmrechtRef16_14DaySpan_all() {
//		_cal.add(Calendar.YEAR, -16);
//		for (int i = 0; i < 14; i++) {
//			_cal.add(Calendar.DAY_OF_YEAR, -1);
//			Date geburtsdatum = _cal.getTime();
//			assertTrue(testEVREFInSpan(geburtsdatum));
//		}
//	}
//	
//	@Test
//	public void testStimmrechtRef16_OutOfSpanTooYoung() {
//		_cal.add(Calendar.YEAR, -4);
//		Date geburtsdatum = _cal.getTime();
//		assertFalse(testEVREFInSpan(geburtsdatum));
//	}
//	
//	@Test
//	public void testStimmrechtRef16_OutOfSpanTooOld()  {
//		_cal.add(Calendar.YEAR, -42);
//		Date geburtsdatum = _cal.getTime();
//		assertFalse(testEVREFInSpan(geburtsdatum));
//	}
//
//	@Test
//	public void testStimmrechtRef16_OutOfSpanBy1TooYoung()  {
//		_cal.add(Calendar.YEAR, -16);
//		_cal.add(Calendar.DAY_OF_YEAR, 1);
//		Date geburtsdatum = _cal.getTime();
//		assertFalse(testEVREFInSpan(geburtsdatum));
//	}
//	
//	@Test
//	public void testStimmrechtRef16_OutOfSpanBy1TooOld()  {
//		_cal.add(Calendar.YEAR, -16);
//		_cal.add(Calendar.DAY_OF_YEAR, -15);
//		Date geburtsdatum = _cal.getTime();
//		assertFalse(testEVREFInSpan(geburtsdatum));
//	}
//	
//	
//	private void resetCal() {
//		_cal = Calendar.getInstance();
//		clearHMSMS(_cal);
//	}
//	
//	private void clearHMSMS(Calendar cal) {
//		cal.set(Calendar.MILLISECOND,0);
//		cal.set(Calendar.SECOND,0);
//		cal.set(Calendar.MINUTE,0);
//		cal.set(Calendar.HOUR_OF_DAY,0);
//	}
//	
//	private boolean testRKInSpan(Date geburtsdatum) {
//		return (_cc._testIsStimmrechtPotenziell("RK", DEFAULT_OID_CODEAUFENTHALT, geburtsdatum))
//			&& (_cc._testDeservesStimmrecht(RK, geburtsdatum));
//	}
//	
//	private boolean testEVREFInSpan(Date geburtsdatum){
//		return (_cc._testIsStimmrechtPotenziell("EV-REF", DEFAULT_OID_CODEAUFENTHALT, geburtsdatum))
//			&& (_cc._testDeservesStimmrecht(EV_REF, geburtsdatum));
//	}
//	
//	
	
}
