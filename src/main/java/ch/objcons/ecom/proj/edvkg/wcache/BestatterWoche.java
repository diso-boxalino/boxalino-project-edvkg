/*
 * Created on 24.01.2005
 *
 * (c) by Boxalino
 */
package ch.objcons.ecom.proj.edvkg.wcache;

import java.util.Vector;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.meta.MetaObject;

/**
 * @author Dominik Raymann
 */
public class BestatterWoche extends AgendaWoche implements Comparable, IBestatter {
	private String _persOID = null;
	private String _name = null;
	private String _vorname = null;
	private String _kurz = null;
	private boolean _gesperrt = false;
	
	
	private KGWoche[][] _buchung = new KGWoche[5][4];
	private Boolean[][] _verfuegbar = new Boolean[5][4];
	
	
	
	
	
	public int compareTo(Object arg0) {
		BestatterWoche obj2 = (BestatterWoche)arg0;
		if (this._gesperrt!=obj2._gesperrt) {
			if (this._gesperrt==true) {
				return -1;
			} else {
				return 1;
			}	
		}
		
		if ((this._kurz == null) && (obj2._kurz != null)) return 1;
		if ((this._kurz != null) && (obj2._kurz == null)) return -1;
		if ((this._kurz == null) && (obj2._kurz == null)) return 0;
		return this._kurz.toLowerCase().compareTo(obj2._kurz.toLowerCase());
		
	}
	
	public BestatterWoche(String woche, String jahr, String persOID, BOContext boContext) throws CacheException {
		super(woche,jahr,boContext);
		_persOID = persOID;
		init();
	}
	
	public void setBuchung(int tag, int vierteltag, KGWoche kgWoche) {
		if (!_gesperrt) {
			_buchung[tag][vierteltag] = kgWoche;
		}
	}
	
	public IKirchgemeinde getBuchendeKirchgemeinde(int tag, int vierteltag) {
		return (IKirchgemeinde)_buchung[tag][vierteltag];
	}
	
	public KGWoche getBuchung(int tag, int vierteltag) {
		return _buchung[tag][vierteltag];
	}
	
	public boolean getVerfuegbar(int tag, int vierteltag) {
		return _verfuegbar[tag][vierteltag].booleanValue();
	}
	
	private void init() throws CacheException {
		try {
			BLTransaction trans = BLTransaction.startTransaction(_boContext);
			MetaObject moSABAPerson = MetaObjectLookup.getInstance().getMetaObject("SABAPerson");
			BOObject boBestatter = trans.getObject(moSABAPerson,Long.parseLong(_persOID),false);
			_name = boBestatter.getAttribute("Name",_boContext);
			_vorname = boBestatter.getAttribute("Vorname",_boContext);
			_kurz = boBestatter.getAttribute("Kurz",_boContext);
			String gesperrt = boBestatter.getAttribute("gesperrt",_boContext);
			if (gesperrt.equalsIgnoreCase("true")) {
				_gesperrt = true;
			}
			
			Vector v = BoxalinoUtilities.executeQuery("SABABestatterWoche","(SABABestatterWoche_Woche == "+_woche+") and (SABABestatterWoche_Jahr == "+_jahr+") and (SABABestatterWoche_SABAPersonSperrungenOID == "+_persOID+")",trans,_boContext);
			BOObject boSABABestatterWoche = null;
			MetaObject moSABABestatterWoche = MetaObjectLookup.getInstance().getMetaObject("SABABestatterWoche");
			if (v.size()>1) {
				//throw new CacheException("ERROR - mehr als eine SABABestatterWoche gefunden f�r Bestatter (OID = "+_persOID + ")");
				boSABABestatterWoche = (BOObject)v.firstElement();
			} if (v.size()==0) {
				boSABABestatterWoche = trans.getObject(moSABABestatterWoche,0,false);
			} else {
				boSABABestatterWoche = (BOObject)v.firstElement();
			}
			
			
			for (int tag = 0;tag<5;tag++) {
				for (int vierteltag = 0; vierteltag<4;vierteltag++) {
					
					String verfuegbar = boSABABestatterWoche.getAttribute(TAGE[tag]+""+VIERTELTAGE[vierteltag],_boContext);
					if (verfuegbar==null) {
						_verfuegbar[tag][vierteltag] = new Boolean(true);
					} else if (verfuegbar.equalsIgnoreCase("true")) {
						_verfuegbar[tag][vierteltag] = new Boolean(true);
					} else if (verfuegbar.equalsIgnoreCase("false")) {
						_verfuegbar[tag][vierteltag] = new Boolean(false);
					} 
					
				}
			}
			
			
			
		} catch (Exception e) {
			throw new CacheException("ERROR - konnte Woche der SABAPerson (OID = "+_persOID+")nicht initialisieren : "+e.getMessage());
		}
	}
	
	public String getName() {
		return _name;
	}
	
	public String getVorname() {
		return _vorname;
	}
	
	public String getKurz() {
		return _kurz;
	}
	public String getOID() {
		return _persOID;
	}
	
	public String getGesperrt() {
		if (_gesperrt) return "true";
		return "false";
	}
	public boolean isGesperrt() {
		return _gesperrt;
	}
	public boolean equals(Object arg0) {
		if (!(arg0 instanceof BestatterWoche)) {
			throw new ClassCastException();
		}
		BestatterWoche other = (BestatterWoche)arg0;
		return this.getOID().equals(other.getOID());
		
	}
}