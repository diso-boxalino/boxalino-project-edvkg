/*
 * Created on 28.01.2005
 *
 * (c) by Boxalino
 */
package ch.objcons.ecom.proj.edvkg.wcache;

/**
 * @author Dominik Raymann
 */
public interface IKirchgemeinde {
	public abstract String getName();
	public abstract String getOID();
	
}
