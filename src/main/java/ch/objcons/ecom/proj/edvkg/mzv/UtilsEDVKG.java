/*
    $RN $(RNUM), 27.07.2007, Matthias Humbert, BOXALINO AG
    $WHY Familienbildung (familyInfo) sortiert nach '
    Geburtsdatum. Briefempf�nger kann nicht mehr passiv '
    oder gesperrt sein.
*/


package ch.objcons.ecom.proj.edvkg.mzv;

import static ch.objcons.ecom.datatypes.EDataTypeValidation.isNullOrEmptyString;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.*;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import ch.objcons.ecom.api.EValueID;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.XBoxValueManagementException;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.BOValidationException;
import ch.objcons.ecom.bom.utils.BOMQuery;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.box.util.SelectExprIterator;
import ch.objcons.ecom.box.util.VectorResult;
import ch.objcons.ecom.box.view.BoxView;
import ch.objcons.ecom.datatypes.EDataTypeValidation;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.engine.EEngine;
import ch.objcons.ecom.expr.ExprParser;
import ch.objcons.ecom.expr.IExpression;
import ch.objcons.ecom.expr.XExpression;
import ch.objcons.ecom.meta.*;
import ch.objcons.ecom.proj.edvkg.mzv.Consts.FI_Familienrollen;
import ch.objcons.ecom.proj.edvkg.mzv.Consts.Vorfall;
import ch.objcons.ecom.proj.edvkg.mzv.imp.IPerson;
import ch.objcons.ecom.proj.edvkg.mzv.imp.PostImport.Person;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.RPGSupport;
import ch.objcons.ecom.query.Expression;
import ch.objcons.ecom.query.Predicate;
import ch.objcons.ecom.query.StandardQuery;
import ch.objcons.ecom.query.XMaxRowException;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.ecom.utils.EParseUtilities;
import ch.objcons.log.Log;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;
import com.boxalino.server.Request;

public class UtilsEDVKG {

	private static ILogger LOGGER = Logger.getLogger("boxalino.edvkg.person.validation");

	public final static int Family = 0;
	public final static int Anrede = 1;
	public final static int Externe = 2;
	private final static String[] Utils = new String[] {
		"Family",
		"Anrede",
		"Externe"
	};
	
	/**
	 * This is the minimum value for country OIDs. Values below this limit
	 * reference a location inside of Switzerland.
	 */
	public static final int COUNTRY_MIN_VALUE = 8000;
	
	public static final int ix_Name = 1;
	public static final int ix_Vorname = 2;
	public static final int ix_ZeitungsAnrede = 3;
	public static final int ix_ZeitungsNachname = 4;
	public static final int ix_ZeitungsVorname = 5;
	public static final int ix_AdrZusatz = 6;
	public static final int ix_Strasse = 7;
	public static final int ix_ZusStrasseOnly = 8;
	public static final int ix_HausNr = 9;
	public static final int ix_PLZ = 10;
	public static final int ix_Ort = 11;
	public static final int ix_PLZPostfach = 12;
	public static final int ix_PLZPostfachOnly = 13;
	public static final int ix_PLZPostfachWohn = 14;
	public static final int ix_Postfach = 15;
	public static final int ix_ZusLand = 16;
	public static final int ix_ZusArtDesc = 17;
	public static final int ix_ZusAnrede = 18;
	
	public static final int ix_ZeitungsAnredeWithoutTitle = 19;
	public static final int ix_ZeitungsNamenzeileTitle = 20;
	public static final int ix_AdrZusatzConsiderPostfach = 21;
	
	public static final int ix_OIDVaterFamilie = 22;
	public static final int ix_OIDMutterFamilie = 23;
	public static final int ix_FamBez = 24;
	public static final int ix_PassivesAusgewachsenesKind = 25;
	public static final int ix_OIDExternPartner = 26;
	
	public static final int ix_GrundPassiv = 27;
	public static final int ix_Vorfaelle = 28;
	public static final int ix_VorfaelleAdmin = 29;
	public static final int ix_HatMeldungen = 30;
	public static final int ix_LetztesUpdateDatum = 31;
	public static final int ix_VorfaelleDesc = 32;
	public static final int ix_VorfaelleAdminDesc = 33;

	public static final int ix_KonfessionsDatum = 34;
	public static final int ix_AufenthaltsartDatum = 35;
	public static final int ix_MeldeadresseDatum = 36;
	public static final int ix_PassivDatum = 37;
	public static final int ix_ZivilstandsDatum = 38;

	public static final Hashtable <String, Integer> _name2Index;
	
	static {
		_name2Index = new Hashtable();
		_name2Index.put("Name", new Integer(ix_Name));
		_name2Index.put("Vorname", new Integer(ix_Vorname));
		_name2Index.put("ZeitungsAnrede", new Integer(ix_ZeitungsAnrede));
		_name2Index.put("ZusAnrede", new Integer(ix_ZusAnrede));
		_name2Index.put("ZeitungsNachname", new Integer(ix_ZeitungsNachname));
		_name2Index.put("ZeitungsVorname", new Integer(ix_ZeitungsVorname));
		_name2Index.put("AdrZusatz", new Integer(ix_AdrZusatz));
		_name2Index.put("Strasse", new Integer(ix_Strasse));
		_name2Index.put("ZeitungsStrasse", new Integer(ix_Strasse));
		_name2Index.put("ZusStrasseOnly", new Integer(ix_ZusStrasseOnly));
		_name2Index.put("HausNr", new Integer(ix_HausNr));
		_name2Index.put("PLZ", new Integer(ix_PLZ));
		_name2Index.put("ZeitungsPLZ", new Integer(ix_PLZ));
		_name2Index.put("Ort", new Integer(ix_Ort));
		_name2Index.put("ZeitungsOrt", new Integer(ix_Ort));
		_name2Index.put("PLZPostfach", new Integer(ix_PLZPostfach));
		_name2Index.put("PLZPostfachOnly", new Integer(ix_PLZPostfachOnly));
		_name2Index.put("PLZPostfachWohn", new Integer(ix_PLZPostfachWohn));
		_name2Index.put("Postfach", new Integer(ix_Postfach));
		_name2Index.put("ZusLand", new Integer(ix_ZusLand));
		_name2Index.put("ZusArtDesc", new Integer(ix_ZusArtDesc));
		_name2Index.put("OIDVaterFamilie", new Integer(ix_OIDVaterFamilie));
		_name2Index.put("OIDMutterFamilie", new Integer(ix_OIDMutterFamilie));
		_name2Index.put("FamBez", new Integer(ix_FamBez));
		_name2Index.put("PassivesAusgewachsenesKind", new Integer(ix_PassivesAusgewachsenesKind));
		_name2Index.put("OIDExternPartner", new Integer(ix_OIDExternPartner));
		_name2Index.put("GrundPassiv", new Integer(ix_GrundPassiv));
		_name2Index.put("Vorfaelle", new Integer(ix_Vorfaelle));
		_name2Index.put("VorfaelleAdmin", new Integer(ix_VorfaelleAdmin));
		_name2Index.put("HatMeldungen", new Integer(ix_HatMeldungen));
		_name2Index.put("LetztesUpdateDatum", new Integer(ix_LetztesUpdateDatum));
		_name2Index.put("VorfaelleDesc", new Integer(ix_VorfaelleDesc));
		_name2Index.put("VorfaelleAdminDesc", new Integer(ix_VorfaelleAdminDesc));
		_name2Index.put("KonfessionsDatum", new Integer(ix_KonfessionsDatum));
		_name2Index.put("AufenthaltsartDatum", new Integer(ix_AufenthaltsartDatum));
		_name2Index.put("MeldeadresseDatum", new Integer(ix_MeldeadresseDatum));
		_name2Index.put("PassivDatum", new Integer(ix_PassivDatum));
		_name2Index.put("ZivilstandsDatum", new Integer(ix_ZivilstandsDatum));
	}
	
	
	private BLTransaction _trans = null;
	private IRequest _request = null;
	private BOContext _context = null;
	private String _slotID = null;
	private BoxMZV _boxEDVKG = null;
	private boolean _useAccessControl = false;
	/**
	 * Based of client_request_location. The value is always <b>FALSE</b> unless client_request_location equals org 
	 */
	private boolean _ignorePostSperre = false;
	/**
	 * Erstellt ein Hilfsobjekt, welches Funktoinen bereitstellt, die immer wieder
	 * verwendet werden. Der Konstruktor setzt die Variablen, welche st�ndig
	 * gebraucht werden.
	 * @param request
	 * @param trans
	 * @param boxEDVKG Box EDVKG falls null, wird sie ermittelt
	 */
	public UtilsEDVKG (IRequest request, BLTransaction trans, BoxMZV boxEDVKG) {
		_request = request;
		_context = (BOContext)request.getContext();
		_trans = trans;
		if (boxEDVKG == null) _boxEDVKG = (BoxMZV) EEngine.getInstance().getBox("edvkg");
		else _boxEDVKG = boxEDVKG;
		_slotID = _boxEDVKG.getSlotID();
		_useAccessControl = true;
		String location = _request.getParameters().getParameter("client_request_location");
		if(location != null){
			_ignorePostSperre = location.equals("org"); 
		}
	}
	
	/**
	 * Holt oder erzeugt eine Instanz f�r das angegebene Utility
	 * @param utilID siehe Array Utils
	 * @param request
	 * @param boxEDVKG die Box EDVKG
	 * @return
	 */
	public static UtilsEDVKG getInstance (int utilID, IRequest request, BoxMZV boxEDVKG) {
		UtilsEDVKG ue = (UtilsEDVKG)request.getValue(boxEDVKG.getSlotID() + "_UtilsObj" + Utils[utilID]);
		if (ue == null) {
			BLTransaction trans = (BLTransaction)request.getValue(boxEDVKG.getSlotID() + "_readOnlyTrans");
		    if (trans == null) {
		    	trans = BLTransaction.startTransaction((BOContext)request.getContext());
		    	request.setValue(boxEDVKG.getSlotID() + "_readOnlyTrans", trans);
		    }
			ue = new UtilsEDVKG(request, trans, boxEDVKG);
			request.setValue(boxEDVKG.getSlotID() + "_UtilsObj" + Utils[utilID], ue);
		}
		return ue;
	}
	public static int counter = 0;
	/**
	 * Ermittelt Familieninformationen. Diese Methode gibt pro Familie immer nur
	 * der selbe Postempf�nger zur�ck.
	 * @param familyMemberOID Die Personen-OID einer Person mit Familie
	 * @return Anzahl der Familienmitglieder oder 0 falls Person keine Familie hat.
	 */
	public int setupFamilyInfo (long familyMemberOID) {
		String familyMemberOIDStr = "" + familyMemberOID;
		String famoidStr = null;
		long familyOID = -1;
		try {
			// Zeit f�r Familien-OID abzufragen f�llt nicht ins Gewicht (0.06 ms)
			BoxView viewBox = (BoxView)EEngine.getInstance().getBox("view");
			famoidStr = viewBox.getMDValue(_request, new EValueID ("Person[" + familyMemberOIDStr + "]_OIDFamilie"));
			if (EDataTypeValidation.isNullOrEmptyString(famoidStr)) {
				famoidStr = "";
			} else {
				familyOID = Long.parseLong(famoidStr);
			}
		} catch (Exception e) {
			Log.logSystemError("Konnte Familien-OID nicht ermitteln: ", e);
			famoidStr = "";
		}

		Vector famList = null;
		String famListOID = (String)_request.getValue(_slotID + "_familyListOID");
		famList = (Vector)_request.getValue(_slotID + "_familyList");
		StandardQuery famQuery = (StandardQuery)_request.getValue(_slotID + "_familyQuery");
		Expression preliminaryFilterStatement = (Predicate)_request.getValue(_slotID + "_familyPredicate");

		if (!famoidStr.equals(famListOID)) {
			try {
				if (familyOID > 0) {
					if (famQuery == null) {
						famQuery = BLManager.getInstance().getExtent(Consts.PersonMeta);
						String filter = RPGSupport.isRPG() ? "Person_rpgAktiv == true" : "Person_StatusPerson == true";
						//String filter = "Person_StatusPerson == true && (Person_Postsperre == NULL || Person_Postsperre == false)";
						String order = "Person_Geburtsdatum";
						//Predicate.createPredicate(query, condition, master, newPredicate);
						IExpression expr = ExprParser.getExpression(filter, false);
						SelectExprIterator i = new SelectExprIterator(famQuery, false /* ignoreEmptyValues */, _context);
						expr.iterate(i);
						preliminaryFilterStatement = i.getWhereStatement();
						Vector orderItems = new Vector();
						VectorResult.interpretOrderBy(order, orderItems, _request, famQuery);
						famQuery.setOrderBy(orderItems);
						_request.setValue(_slotID + "_familyQuery", famQuery);
						_request.setValue(_slotID + "_familyPredicate", preliminaryFilterStatement);
					} 
					MetaAttribute attrOIDFamilie = Consts.PersonMeta.getAllAttributeForName("OIDFamilie", _context);
					Predicate familyCondition = Predicate.createPredicate(famQuery, Predicate.OPERATOR_EQ, attrOIDFamilie, famoidStr);
					Predicate definitivePred = Predicate.createPredicate(famQuery, Predicate.OPERATOR_AND, familyCondition, preliminaryFilterStatement);
					famQuery.setWhereStatement(definitivePred, null);
					famList = famQuery.getResult(null, _context);
					//famList = query.getResult(null, false, false, null, null, null, definitivePred, order, orderItems, null, null, false, false, null, _context, null);

					//famList = BoxalinoUtilities.executeQuery(_request, "Person", filter, order, _trans, _context);
				} else {
					famList = new Vector(0);
				}
			} catch (Exception e) {
				Log.logSystemError("Fehler beim Erzeugen von familyInfo [" + famoidStr + "]", e);
			}
			_request.setValue(_slotID + "_familyList", famList);
			_request.setValue(_slotID + "_familyListOID", famoidStr);

			String curPartner = null; int curPartnerInd = -1;
			String curVorstand = null; int curVorstandInd = -1;
			String noParentOid = null;
			//Date noParentOidGeburtsdatum = null;
			for (int i = 0; i < famList.size(); i++) {
				String oidStr = getListValue(famList, i, "OID");
				if (oidStr == null /* || !"true".equals(getListValue(famList, curVorstandInd, "StatusPerson")) 
					|| "true".equals(getListValue(famList, curVorstandInd, "AdressSperreSicherheit"))*/) {

					famList.remove(i--);
					continue;
				}
				if (oidStr.equals(getListValue(famList, i, "OIDFamilie"))) {
					curVorstand = oidStr;
					curVorstandInd = i;
					continue;
				}
				String tempEhepartner = getListValue(famList, i, "OIDEhepartner");
				String tempVorstand = getListValue(famList, i, "OIDFamilie");
				if (tempVorstand != null && tempVorstand.equals(tempEhepartner)) {
					curPartner = oidStr; curPartnerInd = i;
					continue;
				}
				//Date geburtsdatum = (Date)getListValueAsObject(famList, i, "Geburtsdatum");
				if (noParentOid == null /* && 
				  (noParentOidGeburtsdatum == null || noParentOidGeburtsdatum.after(geburtsdatum))*/) {
					// Falls Postsperre gesetzt, dann soll ein Kind schon gar nicht als Briefempf�nger in Betracht gezogen werden
					if(!"true".equals(getListValue(famList, i, "Postsperre"))){
						noParentOid = oidStr;
					}
				}
			}
			
			if (curVorstand == null) curVorstand = "";
			if (curPartner == null) curPartner = "";
			setResult(Family, "famVorstand", curVorstand);
			setResult(Family, "famVorstandInd", "" + curVorstandInd);
			setResult(Family, "famPartner", curPartner);
			setResult(Family, "famPartnerInd", "" + curPartnerInd);

			
			String mailRecepient = null;
			boolean useVorstand = false;
			boolean usePartner  = false;
			boolean useChild    = false;
			 
			if (curVorstandInd != -1 && ( !"true".equals(getListValue(famList, curVorstandInd, "Postsperre")) || _ignorePostSperre ) ){
				useVorstand = true;
			}
			if (curPartnerInd != -1  && ( !"true".equals(getListValue(famList, curPartnerInd,  "Postsperre")) || _ignorePostSperre ) ){
				usePartner = true;
			}
			if (noParentOid != null){
				useChild = true;
			}
			
			if (useVorstand) { 
				mailRecepient = curVorstand;
			} else if (usePartner) {
				mailRecepient = curPartner;
			} else if (useChild) {
				mailRecepient = noParentOid;
			}
			if (mailRecepient == null) mailRecepient = "";
			setResult(Family, "famMailRecepient", mailRecepient);
		}
		_request.setValue(_slotID + "_Util" + Utils[Family] + "OID", new Long(familyOID));
		return famList.size();
	}

	public int setupFamilienExterneInfo (long familyMemberOID) {
		List<Object[]> res = new ArrayList<Object[]>(3);
		try {
			BOObject boPerson = BoxalinoUtilities.getObjectReadOnly(PersonMeta, familyMemberOID, _context);
			Number famInfoBits = (Number) boPerson.getValue(Consts.FamInfoIndex);
			int famRollenValue = ((famInfoBits.intValue() & Consts.FI_FamilienrollenMask) >> Consts.FI_FamilienrollenBitPosition);
			FI_Familienrollen rolle = FI_Familienrollen.values()[famRollenValue];
			if (rolle == FI_Familienrollen.Alleine || rolle == FI_Familienrollen.PartnerImKonkubinat || 
				rolle == FI_Familienrollen.Undefined) {
				long oidPartner = boPerson.getReferencedOID(Consts.OIDOrigPartnerIndex);
				addEntry(oidPartner, "Partner", res);
			}
			if (rolle == FI_Familienrollen.KindMitVater || rolle == FI_Familienrollen.Undefined) {
				long oidMutter = boPerson.getReferencedOID(Consts.OIDMutterIndex);
				addEntry(oidMutter, "Mutter", res);
			}
			if (rolle == FI_Familienrollen.KindMitMutter || rolle == FI_Familienrollen.Undefined) {
				long oidVater = boPerson.getReferencedOID(Consts.OIDVaterIndex);
				addEntry(oidVater, "Vater", res);
			}
			if (rolle == FI_Familienrollen.Undefined) {
				List<BOObject> kinder = BoxalinoUtilities.executeQuery(_request, "Person", "Person_OIDMutter == " + boPerson.getOID() + " or Person_OIDVater == " + boPerson.getOID(), "", null, _context);
				if (kinder.size() > 0) {
					long famOID = boPerson.getReferencedOID(Consts.OIDFamilieIndex);
					for (BOObject boKind : kinder) {
						if (boKind.getReferencedOID(Consts.OIDFamilieIndex) == famOID) {
							addEntry(boKind.getOID(), "Kind", res);
						}
					}
				}
			}
			_request.setValue(_slotID + "_familienExtList", res);
		} catch (Exception e) {
			
		}
		return res.size();
	}
	
	/**
	 * Adds the specified entry to the list as new Object[] {oid, role}.
	 * If and only if a Person with the specified oid exists.
	 */
	private void addEntry(long oid, String role, Collection<Object[]> entries) {
		if (!personExists(oid)) return;
		
		entries.add(new Object[] {oid, role});
	}
	
	private boolean personExists(long oid) {
		if (oid <= 0) return false;
		
		return BOMQuery.of(BOPerson.CLASS_NAME).match("OID == " + oid).size() > 0;
	}
	
	private String getListValue(Vector list, int i, String attr) {
		try {
			return ((BOObject)list.elementAt(i)).getAttribute(attr, _useAccessControl, _context);
		} catch (Exception e) {
			Log.logSystemError("Listenelement konnte nicht ermittelt werden", e);
			return null;
		}
	}
	
	private Object getListValueAsObject(Vector list, int i, String attr) {
		try {
			return ((BOObject)list.elementAt(i)).getValue(attr, _useAccessControl, _context);
		} catch (Exception e) {
			Log.logSystemError("Listenelement konnte nicht ermittelt werden", e);
			return null;
		}
	}

	private long getPersonValueLong(long oid, String field) {
		long resOID = 0;
		try {
			BOObject obj = _trans.getObject(PersonMeta, oid, false);
			String res = obj.getAttribute(field, true, _context);
			if (res == null) return 0;
			resOID = Long.parseLong(res);
		} catch (Exception e) {
		}
		return resOID;
	}
	
	private String getPersonValue(long oid, String field) {
		String res = null;
		try {
			BOObject obj = _trans.getObject(PersonMeta, oid, false);
			res = obj.getAttribute(field, _useAccessControl, _context);
		} catch (Exception e) {
		}
		if (res == null) res = "";
		return res;
	}

	private String getPersonValueEDVKG(long oid, String field) {
		String res = null;
		try {
			res = _boxEDVKG.getMDValue(_request, new EValueID("Person[" + oid + "]_" + field));
		} catch (Exception e) {
		}
		if (res == null) res = "";
		return res;
	}

	public long filterOID (long oid) {
		if (oid == 0) return 0;
		
		boolean sperre = !_ignorePostSperre && "true".equals(getPersonValue(oid, "Postsperre"));
		if (sperre || !isActive(oid)) return 0;
		
		return oid;
	}
	

	public static boolean isActive(long oid) {
		try {
			MetaObject moPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			IRequest request = Request.getCurrentOrDummy();
			BOContext context = (BOContext) request.getContext();
			BOPerson boPerson = (BOPerson) BLManager.getReadOnlyObject(moPerson, oid, context);
			return RPGSupport.isRPG(request) ? boPerson.isActiveRPG(context) : boPerson.isActiveSP(context);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Eruiert die Anrede der Familie
	 * @param familyMemberOID OID eines Familienmitglieds
	 * @return Anrede als String
	 */
	public void setupFamilyAnrede (long familyMemberOID) {
		// --------------------------------------------------------------------
		// Entscheidung, wer angeschrieben wird
		//    Vorstand und Ehepartner
		//    Vostand oder Ehepartner wenn Vorstand nicht verf�gbar
		//    Adressat direkt
		// --------------------------------------------------------------------
		
		long familienOID = getPersonValueLong(familyMemberOID, "OIDFamilie");
		long vorstandOID = getPersonValueLong(familyMemberOID, "OIDFamilie");
		long ehepartnerOID = getPersonValueLong(vorstandOID, "OIDEhepartner");
		/* Ist der Vorstand des Ehepartner derjenige, der es sein m�sste ? */
		//if (ehepartnerOID != 0 && getPersonValueLong(ehepartnerOID, "OIDFamilie") != vorstandOID) ehepartnerOID = 0;
		/* Ist der Ehepartner in der gleichen Familie wie die gesuchte Person ? */
		if (ehepartnerOID != 0 && getPersonValueLong(ehepartnerOID, "OIDFamilie") != familienOID) ehepartnerOID = 0;
		/* Ist der Vorstand in der gleichen Familie wie die gesuchte Person ? */
		if (vorstandOID != 0 && getPersonValueLong(vorstandOID, "OIDFamilie") != familienOID) vorstandOID = 0;
		vorstandOID = filterOID(vorstandOID);
		ehepartnerOID = filterOID(ehepartnerOID);
		
		long effektivVorstandOID = vorstandOID;
		
		if (effektivVorstandOID == 0) {
			if (ehepartnerOID != 0) {
				effektivVorstandOID = ehepartnerOID;
				ehepartnerOID = 0;
			}
		}
		
		long vaterOID = 0;
		long mutterOID = 0;
		
		long anredeVonOID = familyMemberOID;
		
		if (effektivVorstandOID != 0) {
			String gv = getPersonValue(effektivVorstandOID, "CodeGeschlecht").toLowerCase().trim();
			
			if ("w".equals(gv)) {
				anredeVonOID = effektivVorstandOID;
				vaterOID = ehepartnerOID;
				mutterOID = effektivVorstandOID;
			} else if ("m".equals(gv)) {
				anredeVonOID = effektivVorstandOID;
				vaterOID = effektivVorstandOID;
				mutterOID = ehepartnerOID;
			}
		}
		
		String vaterVorname = "";
		String vaterName = "";
		String vaterGeschlecht = "";
		String mutterVorname = "";
		String mutterName = "";
		String mutterGeschlecht = "";
		//Konfessionen
		String vaterKonfessionOID = "";
		String mutterKonfessionOID = "";

		if (vaterOID != 0) {
			vaterVorname = getPersonValueEDVKG(vaterOID, "Vorname");
			vaterName = getPersonValueEDVKG(vaterOID, "Name");
			vaterGeschlecht = getPersonValue(vaterOID, "CodeGeschlecht");
			vaterKonfessionOID = getPersonValue(vaterOID, "OIDKonfession");
		}
		if (mutterOID != 0) {
			mutterVorname = getPersonValueEDVKG(mutterOID, "Vorname");
			mutterName = getPersonValueEDVKG(mutterOID, "Name");
			mutterGeschlecht = getPersonValue(mutterOID, "CodeGeschlecht");
			mutterKonfessionOID = getPersonValue(mutterOID, "OIDKonfession");
		}


		// --------------------------------------------------------------------
		// Anrede und Namenszeile V2 neue Anforderungen KUS 10.2.16
		// --------------------------------------------------------------------
		// Used later for output
		String namenszeileAnrede = "";
		String namenszeile1 = "";
		String namenszeile2 = "";

		// Persons
		String[] person1 = new String[7];
		person1[0] = (vaterOID != 0) ? String.valueOf(vaterOID) : "";
		//If empty = "" / If not empty = Herr oder Frau setzten
		person1[1] = vaterGeschlecht.isEmpty() ? "" : ("M".equals(vaterGeschlecht)) ? "Herr" : "Frau";
		person1[2] = vaterVorname;
		person1[3] = vaterName;
		person1[4] = vaterGeschlecht;
		person1[5] = vaterKonfessionOID;
		person1[6] = getPersonValue(vaterOID, "hatKind");

		String[] person2 = new String[7];
		person2[0] = (mutterOID != 0) ? String.valueOf(mutterOID): "";
		person2[1] = mutterGeschlecht.isEmpty() ? "" : ("W".equals(mutterGeschlecht)) ? "Frau" : "Herr";
		person2[2] = mutterVorname;
		person2[3] = mutterName;
		person2[4] = mutterGeschlecht;
		person2[5] = mutterKonfessionOID;
		person2[6] = getPersonValue(mutterOID, "hatKind");

		String[] person3 = new String[7];

		//Variables
		//
		String loggedInKG = null;
		String konfKGCode = null;
		String konfKGOID = null;

		boolean ohnePartnerAndererKonf = false;
		String briefAn = "";


		//Start
		//Get Session Values
		try {
			ohnePartnerAndererKonf = Boolean.parseBoolean(EEngine.getInstance().getValue(_request, "client", new EValueID("session_konfessionpartner")));
			briefAn = EEngine.getInstance().getValue(_request, "client", new EValueID("session_BriefAn"));
			loggedInKG = EEngine.getInstance().getValue(_request, "client", new EValueID("session_actualKG"));
			Vector kirchgemeinde = BoxalinoUtilities.executeQuery("Kirchgemeinde","Kirchgemeinde_OID == "+loggedInKG,null,_trans,_request);
			if (kirchgemeinde.size() >= 1) {
				BOObject bo = (BOObject)kirchgemeinde.firstElement();
				konfKGCode = bo.getAttribute("Konfession", _trans.getContext());
				konfKGOID = bo.getAttribute("OIDKonfession", _trans.getContext());
				//if it's empty, go and get it by the referenced object
				if (konfKGCode.isEmpty()) {
					//hole reference object
					BOObject bo1 = bo.getReferencedObject("OIDKonfession", _trans.getContext());
					konfKGCode = bo1.getAttribute("code", _trans.getContext());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}


		//p1 <=> p2 tauschen
		//da p1 andere konfession, 2te prio
		if ((!person1[5].equals(konfKGOID)) && (person2[5].equals(konfKGOID))) {
			person3 = person1;
			person1 = person2;
			person2 = person3;
		} else {
			//n�t, werden immer beide angeschrieben, ausser mit flag
		}

		//ohne partner anderer konf -> Setze person auf NULL
		if (ohnePartnerAndererKonf) {
			if (!person2[5].equals(konfKGOID)) {
				person2[0] = "";
				person2[1] = "";
				person2[2] = "";
				person2[3] = "";
				person2[4] = "";
				person2[5] = "";
				person2[6] = "";
			}
		}


		//Beide Mitglieder normal anschreiben

		//Falls mit Kind ist die Anschrift immer Familie
		if (Boolean.parseBoolean(person1[6]) || Boolean.parseBoolean(person2[6])) {
			namenszeileAnrede = "Familie";
		} else {
			//Gleichgeschlechtliche haben eine andere Anschrift
			if (person1[1].equals(person2[1]) && !ohnePartnerAndererKonf) {
				//Get the Limit Property in Metaserviceproperties
				MetaService ms = null;
				MetaServiceProperty familienanschriftGleichgeschlechtlichW = null;
				MetaServiceProperty familienanschriftGleichgeschlechtlichM = null;
				try {
					ms = MetaServiceManager.getInstance().getServiceForName("edvkg");
					if(ms != null) {
						familienanschriftGleichgeschlechtlichW = ms.getPropertyForName("FamilienanschriftGleichgeschlechtlichW");
						familienanschriftGleichgeschlechtlichM = ms.getPropertyForName("FamilienanschriftGleichgeschlechtlichM");
					}
				} catch (XMetaModelNotFoundException e) {
					e.printStackTrace();
				}
				namenszeileAnrede = (person1[1] == "Frau") ? familienanschriftGleichgeschlechtlichW.getValue() : familienanschriftGleichgeschlechtlichM.getValue();
			} else {
				namenszeileAnrede = person1[1] +" und "+person2[1];
			}
		}

		//gleiche namen
		if (person1[3].equals(person2[3])) {
			namenszeile1 = person1[2] +" und "+person2[2]+ " " +person1[3];
		//contains
		} else if(person1[3].contains(person2[3])){
			namenszeile1 = person1[2] +" und "+person2[2]+ " " +person1	[3];
			namenszeile2 = "";
		} else if (person2[3].contains(person1[3])) {
			namenszeile1 = person1[2] +" und "+person2[2]+ " " +person2[3];
			namenszeile2 = "";
		//unterschiedliche
		} else {
			//Wenn Familie, trotz unterschiedlicher Namen => ohne anrede
			if (Boolean.parseBoolean(person1[6]) || Boolean.parseBoolean(person2[6])) {
				namenszeile1 = person1[2]+ " " +person1[3] + " und";
				namenszeile2 = person2[2]+ " " +person2[3];
			} else {
				namenszeileAnrede = "";
				namenszeile1 = person1[1] +" "+person1[2]+ " " +person1[3] + " und";
				namenszeile2 = person2[1] +" "+person2[2]+ " " +person2[3];
			}
		}


		/*
		 * SPEZIALF�LLE
		 */

		//Einzelpersonen andressieren
		//falls beide leer => Organisation, �bernehmen aus alter L�sung -> siehe weiter unten/ Codeline ~800
		if (person1[3].isEmpty() && person2[3].isEmpty()) {
			//setzte alles auf leer
			namenszeileAnrede = "";
			namenszeile1 = "";
			namenszeile2 = "";
		} else if (person1[3].isEmpty() || person2[3].isEmpty()) {
			//Falls nur eine Person leer

			//Wenn Person1 leer = Setzte Person2 auf Person1
			if (person1[3].isEmpty()) {
				person3 = person1;
				person1 = person2;
				person2 = person3;
			}

			namenszeileAnrede = person1[1];
			namenszeile1 = person1[2] + " " + person1[3];
			namenszeile2 = "";
		}

		// --------------------------------------------------------------------
		// Anrede und Namenszeile V2 FERTIG
		// --------------------------------------------------------------------

		String anrede = null;
		String namensZeile = null;
		String vornamen = null;
		String anredeName = null;
		String gg = "false";

		if (!vaterVorname.equals("") && !mutterVorname.equals("")) {
			if (!vaterGeschlecht.equalsIgnoreCase(mutterGeschlecht)) {
				String hatKind1 = getPersonValue(vaterOID, "hatKind");
				String hatKind2 = getPersonValue(mutterOID, "hatKind");
				if ("true".equals(hatKind1) || "true".equals(hatKind2)) {
					anrede = "Familie";
				}
				else {
					anrede = "Herr und Frau";
				}

				namensZeile = vaterVorname + " und " + mutterVorname + " " + vaterName;
				vornamen = vaterVorname + " und " + mutterVorname;
				anredeName = vaterName;

			} else {
				// Gleichgeschlechtliche Paare
				namensZeile = vaterVorname + " " + vaterName + " und " + mutterVorname + " " + mutterName;
				vornamen = vaterVorname + " " + vaterName;
				anredeName = " und " + mutterVorname + " " + mutterName;
				gg = "true";
				anrede = "An";
			}
		}
		else if (!vaterVorname.equals("")) {
			String hatKind1 = getPersonValue(vaterOID, "hatKind");
			if ("true".equals(hatKind1)) {
				anrede = "Familie";
			}
			else {
				anrede = "Herr";
			}
			namensZeile = vaterVorname + " " + vaterName;

			vornamen = vaterVorname;
			anredeName = vaterName;
		}
		else if (!mutterVorname.equals("")) {
			String hatKind2 = getPersonValue(mutterOID, "hatKind");
			if ("true".equals(hatKind2)) {
				anrede = "Familie";
			} else {
				anrede = "Frau";
			}
			namensZeile = mutterVorname + " " + mutterName;
			vornamen = mutterVorname;
			anredeName = mutterName;
		} else {
			/* Familienmitglied das nicht Elternteil ist */
			String vorname = getPersonValueEDVKG(familyMemberOID, "Vorname");
			String name = getPersonValueEDVKG(familyMemberOID, "Name");
			String familyMemberGeschlecht = getPersonValue(familyMemberOID, "CodeGeschlecht").toLowerCase().trim();
			
			namensZeile = vorname + " " + name;
			vornamen = vorname;
			anredeName = name;
			if (vorname.equals("")) namensZeile = name;
			if ("w".equals(familyMemberGeschlecht)) {
				anrede = "Frau";
			} else if ("m".equals(familyMemberGeschlecht)) {
				anrede = "Herr";
			} else {
				anrede = "An";
			}
		}
		
		if (anredeVonOID > 500000000L) {
			String zusAnrede = getPersonValue(anredeVonOID, "ZusAnrede");
			if (!zusAnrede.trim().isEmpty()) {
				anrede = zusAnrede;
			}
		}


		/**
		 * FAMILIENANSCHRIFT bei ORGANISATIONEN
		 * KUS 11.02.16: Da nichts definiert wurde: Falls Familienanschriften leer sind �bernehmen aus alter L�sung
		 * Ausser bei Brief an Eltern
		 */
		//falls beide leer => Organisation, �bernehmen aus alter L�sung
		if (person1[3].isEmpty() && person2[3].isEmpty()) {
			namenszeileAnrede = (namenszeileAnrede.isEmpty()) ? anrede: namenszeileAnrede;
			namenszeile1 = (namenszeile1.isEmpty()) ? vornamen: namenszeile1;
			if (!briefAn.equals("eltern")) {
				namenszeile2 = (namenszeile2.isEmpty()) ? anredeName: namenszeile2;
			}
		}
		//DONE
		
		setResult(Anrede, "anrede", anrede);
		setResult(Anrede, "namenszeile", namensZeile);
		setResult(Anrede, "vornamen", vornamen);
		setResult(Anrede, "anredeVonOID", "" + anredeVonOID);
		setResult(Anrede, "vaterVorname", vaterVorname);
		setResult(Anrede, "vaterName", vaterName);
		setResult(Anrede, "mutterVorname", mutterVorname);
		setResult(Anrede, "mutterName", mutterName);
		setResult(Anrede, "anredeName", anredeName);
		setResult(Anrede, "gleichgeschlechtlich", gg);
		//setResult(Anrede, "familienAnschrift", familienAnschrift);
		setResult(Anrede, "familienAnschrift1", namenszeileAnrede); //familienAnschrift1);
		setResult(Anrede, "familienAnschrift2", namenszeile1); //familienAnschrift2);
		setResult(Anrede, "familienAnschrift3", namenszeile2); //familienAnschrift3);
	}
	
	private void setResult(int utilID, String functionName, Object value) {
		_request.setValue(_slotID + "_Util" + Utils[utilID] + "_" + functionName, value);
	}
	
	/**
	 * Retourniert je nach Utility und Funktion folgende Werte:
	 * @param oid Die ID des Objektes - z.B. Familien-OID oder Familienmitglied-OID
	 *        falls 0 -> es wird einfach das letzte berechnete Resultat genommen, egal
	 *        f�r welche OID das Resultat berechnet wurde
	 * @param utilID Anrede
	 * @param function z.B. "anrede": An, Herr, Herr und Frau usw.
	 * @param function namenszeile: Namenszeile welche direkt auf Anrede folgt
	 *
	 * @param utilID Family
	 * @param function 
	 * @return
	 * Utility: Anrede
	 * Funktion: anrede: An, Herr, Herr und Frau usw.
	 *           namenszeile: Namenszeile welche direkt auf Anrede folgt
	 *           vornamen: Vornamen des Adressaten (z.B. 'Fritz und K�thi')
	 *           
	 * Utility: Family
	 * Funktion:famVorstand: Vorstand
	 *          famVorstandInd: Index vom Vorstand in der Liste
	 *          famPartner: Ehepartner
	 *          famPartnerInd: Index vom Partner
	 *          famMailRecepient: Postadressat
	 */
	public String getResult(long oid, int utilID, String function) {
		/* Bemerkung: Cache funktioniert f�r famileyInfo nicht da in calculatedOID die 
		 * Familien-OID steht und nicht die Personen-OID
		 */
		Long calculatedOID = (Long)_request.getValue(_slotID + "_Util" + Utils[utilID] + "OID");
		if ((calculatedOID == null || calculatedOID.longValue() != oid) && oid > 0) {
			switch (utilID) {
				case Anrede:
					setupFamilyAnrede(oid);
					break;
				case Family:
					setupFamilyInfo(oid);
					break;
				default:
					return null;
			}
			_request.setValue(_slotID + "_Util" + Utils[utilID] + "OID", new Long(oid));
		}
		return (String)_request.getValue(_slotID + "_Util" + Utils[utilID] + "_" + function);
	}

	/**
	 * Ermittelt, ob eine History-Zustelladresse extern ist. 
	 * 
	 * @deprecated Feld zusArt verwenden
	 */
	@Deprecated
	public final static boolean isHistZustellExtern(BOObject boObj, BOContext boContext) throws XMetaException, XSecurityException {
		String plzOrtEind = boObj.getAttribute(Hist_zusPLZOrtEindIndex, false, boContext);
		String strasseExtern = boObj.getAttribute(Hist_zusStrassenNameIndex, false, boContext);
		String plzPostfachEind = boObj.getAttribute(Hist_zusPLZPostfachEindIndex, false, boContext);
		return isZustellExtern(plzOrtEind, strasseExtern, plzPostfachEind);
	}
	
	/**
	 * Ermittelt, ob die Zustelladresse ausserhalb der Stadt ist.<p>
	 *
	 * Falls die beiden Kriterien, dass die 6-stellige Postleitzahl 
	 * der Zustelladresse existiert und 
	 * die externe Zustellstrasse gesetzt ist oder das eine
	 * Kriterium, dass die 6-stellige
	 * Zustell-Postfach-PLZ gesetzt ist, erf�llt ist,
	 * dann gilt die vollst�ndige externe Zustell-Adresse als gegeben.
	 * 
	 * @deprecated Feld zusArt verwenden
	 */
	@Deprecated
	public final static boolean isZustellExtern(String zustellPLZOrtEind, String zustellStrasseExtern, String zustellPLZPostfachEind) {
		return !EDataTypeValidation.isNullOrEmptyString(zustellPLZOrtEind) && 
			     !EDataTypeValidation.isNullOrEmptyString(zustellStrasseExtern) ||
			   !EDataTypeValidation.isNullOrEmptyString(zustellPLZPostfachEind);		
	}
	
	/**
	 * Ermittelt, ob die Zustelladresse einer h�ndischen Mutation entspringt. Dies
	 * ist der Fall, wenn die Zustelladresse keine externe Adresse ist und die Zustellstrasse
	 * gesetzt ist (die ausgeschriebene). 
	 * 
	 * @deprecated Feld zusArt verwenden
	 */
	@Deprecated
	public final static boolean isZustellEnteredManually (String zustellStrasse, boolean isZustellExtern) {
		return !isZustellExtern && !EDataTypeValidation.isNullOrEmptyString(zustellStrasse);		
	}
	
	/**
	 * Ermittelt, ob die Zustelladresse vorhanden ist. Beachtet dazu
	 * indirekt auch die Postfachfelder. Die Zustelladresse ist vorhanden, 
	 * wenn entweder die Stadt-Zustellstrasse (OID) (f�r Stadt-interne
	 * Zustelladressen), die h�ndische Zustellstrasse (f�r manuelle, st�dtische)
	 * oder der Zustellort (f�r externe Zustelladressen)
	 * gegeben ist. Beachte, dass es nicht gen�gt, nur die zwei Zustellstrassen
	 * zu pr�fen, da auch das Zustellpostfach gesetzt sein kann ohne Zustellstrasse.
	 * Darum wird der Ort auch �berpr�ft.
	 * @param zustellStrasseStadt Die OID der Zustellstrasse
	 * @param zustellStrasseManuell Die h�ndische Zustellstrasse bzw. die externe Zustellstr.
	 * @param zustellOrt Der Zustellort
	 * 
	 * @deprecated Feld zusArt verwenden
	 */
	@Deprecated
	public final static boolean zustellExists(String zustellStrasseStadt, String zustellStrasseManuell, String zustellOrt) {
		
		return !EDataTypeValidation.isNullOrEmptyString(zustellStrasseStadt) ||
			   !EDataTypeValidation.isNullOrEmptyString(zustellStrasseManuell) ||
			   !EDataTypeValidation.isNullOrEmptyString(zustellOrt);
	}
	
	/**
	 * Kopiert die externe Zustelladresse
	 * @param fromBo Sourcen-BO
	 * @param toBo Ziel-BO
	 */
	public final static void copyExternalHistoryZustell (BOObject fromBo, BOObject toBo, BOContext ctx) throws XMetaException, XDataTypeException, XSecurityException {
		copyField(Hist_zusStrassenNameIndex, fromBo, toBo, ctx);
		copyField(Hist_zusHausNrIndex, fromBo, toBo, ctx);
		copyField(Hist_zusAdrZusatzIndex, fromBo, toBo, ctx);
		copyField(Hist_zusPostfachIndex, fromBo, toBo, ctx);
		copyField(Hist_zusPLZPostfachIndex, fromBo, toBo, ctx);
		copyField(Hist_zusPLZPostfachEindIndex, fromBo, toBo, ctx);
		copyField(Hist_zusOrtIndex, fromBo, toBo, ctx);
		copyField(Hist_zusPLZOrtIndex, fromBo, toBo, ctx);
		copyField(Hist_zusPLZOrtEindIndex, fromBo, toBo, ctx);
		copyField(Hist_zusPostlagerndIndex, fromBo, toBo, ctx);
		// copyField(_hist_zusOIDCodeOrtLandIndex, fromBo, toBo, ctx);
		copyField(Hist_zusOrtLandNameIndex, fromBo, toBo, ctx);
		// ausser _hist_zusOIDStrasseIndex alles kopieren
		copyField(Hist_zusArtIndex, fromBo, toBo, ctx);
	}
	
	public final static void copyField (int index, BOObject fromBo, BOObject toBo, BOContext ctx) throws XMetaException, XDataTypeException, XSecurityException {
		String value = fromBo.getAttribute(index, false, ctx);
		toBo.setAttribute(index, value, ctx, null);
	}
	
	private final static BOObject getEDVKGObject(IRequest request, String attrName) throws XBoxValueManagementException, XSecurityException, XMetaModelQueryException {
		String personClassName = EParseUtilities.getSubstring(attrName, 0);
		int indexBracket = attrName.indexOf('[');
		String oidStr = null;
		if (indexBracket != -1) {
			int indexBracketEnd = attrName.indexOf(']', indexBracket + 1);
			if (indexBracketEnd == -1) {
				throw new XBoxValueManagementException("Cannot parse expression: " + attrName);
			}
			oidStr = attrName.substring(indexBracket + 1, indexBracketEnd);
			personClassName = personClassName.substring(0, indexBracket);
		} else {
			String oidParam = "view_" + personClassName + "_OID";
			oidStr = request.getParameters().getParameter(oidParam);
		}
		int oid = 0;
		try {
			oid = Integer.parseInt(oidStr);
		}
		catch (NumberFormatException x) {
			throw new XBoxValueManagementException("OID not set or not valid: " + attrName);			
		}
		
		MetaObject mobj = null;
		if ("PersonHistory".equals(personClassName)) mobj = PersonHistoryMeta;
		else mobj = PersonMeta;
		BOObject personObj = BLManager.getObject(mobj, null, oid, false, (BOContext) request.getContext());
		if (personObj == null) throw new XBoxValueManagementException(personClassName + "[" + oidStr + "] not found");
		return personObj;
	}
	
	public interface EDVKGFieldGetter {
		public Object getField(int index) throws XMetaModelQueryException, XSecurityException, SQLException;
		public Object getField(String attrPath) throws XMetaModelQueryException, XSecurityException, SQLException, XMetaException;
		public Object getObject(int index) throws XMetaModelQueryException, XSecurityException, SQLException;
		public Object getList(int index) throws XMetaModelQueryException, XSecurityException, SQLException;
		/**
		 * Dereferenziert zuerst bevor Feld geholt wird.
		 * @param derefIndex Erster Index eines Attr. das eine Referenz ist
		 * @param targetIndex
		 */
		public Object getField(int derefIndex, int targetIndex) throws XMetaModelQueryException, XSecurityException, SQLException;
		public String getZusArt();
		public void setZusart(String zusArt);
		public BOObject getBO();
		public void setBO(BOPersonHistory bo);
	}
	
	/**
	 * 
	 * @param labelIndex
	 * @param map
	 * @param fieldGetter
	 * @return Folgende Objekte werden zur�ckgegeben: <ul>
	 * <li>String falls direkt Endresultat</li>
	 * <li>List&lt;Object&gt; falls auszuwertende Attributindeces bzw. Attributpfade</li>
	 * <li>der String "getAnrede" falls Anredefunktion noch aufgerufen werden muss</li>
	 * </ul>
	 * @throws XBoxValueManagementException
	 * @throws XMetaModelQueryException
	 * @throws XSecurityException
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	public final static Object getEDVKGField (Integer labelIndex, int[] map, EDVKGFieldGetter fieldGetter) throws XBoxValueManagementException, XMetaModelQueryException, XSecurityException, SQLException {
		String resString;
		List<Object> attrList = new ArrayList<Object>();
		switch (labelIndex.intValue()) {

		/* Geh�rt zur Zustellanrede */
		case ix_Name:
			attrList.add(new Integer(map[ZusNameIndex]));
			attrList.add(new Integer(map[NameIndex]));
			break;
		/* Geh�rt zur Zustellanrede */
		case ix_Vorname:
			attrList.add(new Integer(map[ZusVornameIndex]));
			attrList.add(new Integer(map[VornameIndex]));
			break;
		/* Geh�rt zur Zeitungsanrede */
		case ix_ZeitungsAnrede: // wird momentan nur von ActionExport verwendet
			attrList.add(new Integer(map[ZeitungAnredeIndex]));
			attrList.add(new Integer(map[ZusAnredeIndex]));
			attrList.add("OIDCodeTitel_Name");
			attrList.add("getAnrede");
			break;
		case ix_ZeitungsAnredeWithoutTitle: // wird momentan nur von ActionExport verwendet
			attrList.add(new Integer(map[ZeitungAnredeIndex]));
			attrList.add(new Integer(map[ZusAnredeIndex]));
			attrList.add("getAnrede");
			break;
		case ix_ZeitungsNamenzeileTitle:
			if (EDataTypeValidation.isNullOrEmptyString((String) fieldGetter.getField(ZeitungAnredeIndex)) 
				&& EDataTypeValidation.isNullOrEmptyString((String) fieldGetter.getField(ZusAnredeIndex)) ) {
				attrList.add("OIDCodeTitel_Name");
			}
			break;
		case ix_ZusAnrede: // weiss nicht warum dieser Eintrag existiert mh@20100224
			attrList.add("getAnrede");
			break;
		/* Geh�rt zur Zeitungsanrede */
		case ix_ZeitungsNachname:
			attrList.add(new Integer(map[ZeitungNachnameIndex]));
			attrList.add(new Integer(map[ZusNameIndex]));
			attrList.add(new Integer(map[NameIndex]));
			break;
		/* Geh�rt zur Zeitungsanrede */
		case ix_ZeitungsVorname:
			attrList.add(new Integer(map[ZeitungVornameIndex]));
			attrList.add(new Integer(map[ZusVornameIndex]));
			attrList.add(new Integer(map[VornameIndex]));
			break;
		case ix_AdrZusatz:
			if (!EDataTypeValidation.isNullOrEmptyString(fieldGetter.getZusArt())) {
				attrList.add(new Integer(map[ZusAdrZusatzIndex]));
			} else {
				attrList.add(new Integer(map[AdrZusatzIndex]));
			}
			break;
		case ix_AdrZusatzConsiderPostfach: // wird momentan nur von ActionExport verwendet
			if (getPostfach(!isNullOrEmptyString(fieldGetter.getZusArt()),fieldGetter) != null) {
				break;
			}
			
			if (!EDataTypeValidation.isNullOrEmptyString(fieldGetter.getZusArt())) {
				attrList.add(new Integer(map[ZusAdrZusatzIndex]));
			} else {
				attrList.add(new Integer(map[AdrZusatzIndex]));
			}
			
			break;
		case ix_Strasse:
			String zusArt = fieldGetter.getZusArt();
			if (!isNullOrEmptyString(zusArt)) {
				attrList.add(new Integer(map[ZusStrassenNameIndex]));
				if ("s".equals(zusArt)) {
					Object object = fieldGetter.getField(ZusOIDStrasseIndex);
					if (object != null && (Long)object > 0) {
						attrList.add(new Integer(map[StrasseKGIndex]));
					}
				}
			} else {
				attrList.add(new Integer(map[StrasseKGIndex]));
			}
			break;
		case ix_ZusStrasseOnly:
			if (!isNullOrEmptyString(fieldGetter.getZusArt())) {
				attrList.add(new Integer(map[ZusStrassenNameIndex]));
				attrList.add("ZusOIDStrasse_Name");
			}
			break;
		case ix_HausNr:
			if (!isNullOrEmptyString(fieldGetter.getZusArt())) {
				attrList.add(new Integer(map[ZusHausNrIndex]));
			} else {
				attrList.add(new Integer(map[HausNrIndex]));
			}
			break;
		case ix_PLZ:
			if (!isNullOrEmptyString(fieldGetter.getZusArt())) {
				attrList.add(new Integer(map[ZusPLZOrtIndex]));
			} else {
				attrList.add(new Integer(map[PLZOrtIndex]));
			}
			break;
		case ix_Ort:
			if (!isNullOrEmptyString(fieldGetter.getZusArt())) {
				attrList.add(new Integer(map[ZusOrtIndex]));
			} else {
				attrList.add(new Integer(map[OrtIndex]));
			}
			break;
		/*
		 * Gibt auf jeden Fall eine PLZ zur�ck. 
		 */
		case ix_PLZPostfach:
			if (!isNullOrEmptyString(fieldGetter.getZusArt())) {
				attrList.add(new Integer(map[ZusPLZPostfachIndex]));
				attrList.add(new Integer(map[ZusPLZOrtIndex]));
			} else {
				attrList.add(new Integer(map[PLZPostfachIndex]));
				attrList.add(new Integer(map[PLZOrtIndex]));
			}
			break;
		/*
		 * Im Unterschied zu PLZPostfach gibt diese Funktion null zur�ck,
		 * falls kein Postfach definiert ist.
		 */
		case ix_PLZPostfachOnly:
			if (!isNullOrEmptyString(fieldGetter.getZusArt())) {
				String zusPostfach = (String) fieldGetter.getField(ZusPostfachIndex);
				attrList.add(new Integer(map[ZusPLZPostfachIndex]));
				/* Zustellorts-PLZ nur in Betracht ziehen, wenn Zustellpostfach gesetzt */
				if (!isNullOrEmptyString(zusPostfach)) attrList.add(new Integer(map[ZusPLZOrtIndex]));  
			} else {
				/* Wohn-Postfach nur in Betracht ziehen wenn keine Zustelladresse existiert */
				attrList.add(new Integer(map[PLZPostfachIndex]));
				String wohnPostfach = (String) fieldGetter.getField(PostfachIndex);
				if (!isNullOrEmptyString(wohnPostfach)) attrList.add(new Integer(map[PLZOrtIndex]));
			}
			break;
		/*
		 * Ermittelt die PLZ f�r ein Postfach der Wohnadresse. Zusammen mit
		 * view_<BO>_Postfach ergibt das die Postfachzeile falls mindestens
		 * eines der beiden Felder gesetzt ist.
		 */
		case ix_PLZPostfachWohn:
			attrList.add(new Integer(map[PLZPostfachIndex]));
			attrList.add(new Integer(map[PLZOrtIndex]));
			break;
		/*
		 * Ergibt zusammen mit PLZPostfach die Postfachzeile. Sie ist dann definiert,
		 * wenn diese Funktion hier nicht null zur�ckgibt. 
		 */
		case ix_Postfach:
			/* Retourniert Postfach-Nr falls vorhanden, ein einziges Leerzeichen, wenn Postfach
			 * gesetzt, aber keine Nummer vorhanden und null falls kein Postfach vorhanden
			 */
			return getPostfach(!isNullOrEmptyString(fieldGetter.getZusArt()), fieldGetter);
		case ix_ZusLand:
			attrList.add(new Integer(map[ZusOrtLandNameIndex]));
			attrList.add("ZusOIDCodeOrtLand_Name");
			break;
		case ix_ZusArtDesc:
			if (isNullOrEmptyString(fieldGetter.getZusArt())) {
				return "Leere Zustelladresse";
			} else {
				char c = fieldGetter.getZusArt().charAt(0);
				switch (c) {
					case 'm': resString = "H�ndische Zustelladresse"; break;
					case 's': resString = "St�dtische Zustelladresse"; break;
					case 'e': resString = "Externe Zustelladresse";	break;	
					case 'M': resString = "Tempor�re, h�ndische Zustelladr."; break;
					default: resString = "Unbekannte Art von Zustelladr.";
				}
			}
			return resString;
		case ix_OIDVaterFamilie:
		case ix_OIDMutterFamilie:
			int ix = labelIndex.intValue();
			int ix_oidElternteil = (ix == ix_OIDVaterFamilie ? Consts.OIDVaterIndex : Consts.OIDMutterIndex);
			Object oid = fieldGetter.getField(ix_oidElternteil);
			Object famOID = fieldGetter.getField(Consts.OIDFamilieIndex);
			if (oid != null && famOID != null) {
				Object famOIDElternteil = fieldGetter.getField(ix_oidElternteil, Consts.OIDFamilieIndex);
				if (famOID.equals(famOIDElternteil)) {
					return oid;
				}
			}
			break;
		case ix_FamBez:
			Number famInfoBits = (Number)fieldGetter.getObject(Consts.FamInfoIndex);
			if (famInfoBits != null) {
				if ((famInfoBits.intValue() & Consts.FI_PassivesAusgewachsenesKindMask) > 0) {
					return "Passives vollj�hr. Kind";
				} else {
					int famRollenValue = ((famInfoBits.intValue() & Consts.FI_FamilienrollenMask) >> Consts.FI_FamilienrollenBitPosition);
					FI_Familienrollen rolle = FI_Familienrollen.values()[famRollenValue];
					if (rolle == FI_Familienrollen.Undefined) {
						return "";
					} else {
						return Consts.FI_Familienrollen2String.get(rolle);
					}
				}
			}
			break;
		case ix_PassivesAusgewachsenesKind:
			famInfoBits = (Number)fieldGetter.getObject(Consts.FamInfoIndex);
			if (famInfoBits != null) {
				int passivesAusgewachsenesKindValue = ((famInfoBits.intValue() & Consts.FI_PassivesAusgewachsenesKindMask) >> Consts.FI_PassivesAusgewachsenesKindBitPosition);
				return Boolean.toString(passivesAusgewachsenesKindValue == 1);
			}
			break;
		case ix_OIDExternPartner:
			Object oidOrigPartner = fieldGetter.getField(Consts.OIDOrigPartnerIndex);
			Object oidNFBPartner = fieldGetter.getField(Consts.OIDEhepartnerIndex);
			if (oidOrigPartner != null && !oidOrigPartner.equals(oidNFBPartner)) {
				return oidOrigPartner;
			}
			break;
		case ix_KonfessionsDatum:
		case ix_AufenthaltsartDatum:
		case ix_MeldeadresseDatum:
		case ix_PassivDatum:
		case ix_ZivilstandsDatum:
			ix = labelIndex.intValue();
			final int[] ixArray = { Consts.KonfessionsDatumIndex, Consts.AufenthaltsartDatumIndex, Consts.MeldeadresseDatumIndex, Consts.PassivDatumIndex, Consts.ZivilstandsDatumIndex };
			int metaIX = ixArray[ix - ix_KonfessionsDatum]; //Die Vorraussetzungen, damit dies funktioniert, sollten klar sein 
			Date vorfallsDatum = (Date) fieldGetter.getObject(metaIX);
			String dateS;
			if (vorfallsDatum == null) {
				dateS = "k.D."; // kein Datum
			} else {
				dateS = SimpleDateFormat.getDateInstance().format(vorfallsDatum);
			}
			return dateS;
		case ix_GrundPassiv:
			/* Wird nur auf History Objekt aufgerufen */
			vorfallsDatum = (Date) fieldGetter.getObject(Consts.SterbedatumIndex);
			String kommentar;
			if (vorfallsDatum == null) {
				vorfallsDatum = (Date) fieldGetter.getObject(Consts.WegDatumIndex);
				if (vorfallsDatum == null) {
					vorfallsDatum = (Date) fieldGetter.getObject(Consts.KonfessionsDatumIndex);
					if (vorfallsDatum == null) {
						vorfallsDatum = (Date) fieldGetter.getObject(Consts.PassivDatumIndex);
						kommentar = "Inaktiv, ";
					} else {
						kommentar = "Austritt, ";
					}
				} else {
					kommentar = "Wegzug, ";
				}
				if (vorfallsDatum == null) {
					vorfallsDatum = (Date) fieldGetter.getBO().getValue(Consts.Hist_WegKGDatumIndex);
					if (vorfallsDatum != null) {
						kommentar = "Wegzug, ";
					} else if (fieldGetter.getBO() instanceof BOPersonHistory) {
						BOPersonHistory next = BOPersonHistory.nextEntry((BOPersonHistory) fieldGetter.getBO());
						if (next != null) {
							Vorfall v = next.getVorfall(Vorfall.Wegzug, Vorfall.Umzug);
							if (v != null) {
								kommentar = v + ", ";
								vorfallsDatum = (Date) next.getValue(Consts.Hist_MeldeadresseDatumIndex);
								if (vorfallsDatum == null) vorfallsDatum = (Date) next.getValue(Consts.Hist_ZuzDatumIndex);
								
								if (vorfallsDatum == null) vorfallsDatum = (Date) next.getValue(Consts.Hist_WegKGDatumIndex);
							}
						}
					}
				}
			} else {
				kommentar = "Tod, ";
			}
			kommentar += vorfallsDatum != null ? SimpleDateFormat.getDateInstance().format(vorfallsDatum) : "Datum unbekannt";
			return kommentar;
		case ix_Vorfaelle:
		case ix_VorfaelleAdmin:
		case ix_VorfaelleDesc:
		case ix_VorfaelleAdminDesc:
			ix = labelIndex.intValue();
			boolean mustBeVorfall = ix == ix_Vorfaelle || ix == ix_VorfaelleDesc;
			Number vorfallBits = (Number) fieldGetter.getObject(Consts.Hist_VorfallBitsIndex);
			String vListe = null;
			if (vorfallBits != null) {
				int vorfaelle = vorfallBits.intValue();
				for (Vorfall v : Vorfall.values()) {
					if ((vorfaelle & v.getBitMask()) > 0 && v.istVorfall() == mustBeVorfall) {
						if (ix == ix_Vorfaelle || ix == ix_VorfaelleAdmin) {
							vListe = vListe == null ? v.getAbkuerzung() : vListe + ", " + v.getAbkuerzung();
						} else {
							vListe = vListe == null ? v.toString() : vListe + ", " + v.toString();
						}
					}
				}
			}
			/*
			if (vListe == null && mustBeVorfall) {
				BOPersonHistory next = BOPersonHistory.nextEntry((BOPersonHistory) fieldGetter.getBO());
				if (next != null) {
					boolean compact = ix == ix_Vorfaelle || ix == ix_VorfaelleAdmin;
					Vorfall v = next.getVorfall(Vorfall.Wegzug, Vorfall.Umzug);
					if (v != null) {
						String s = compact ? v.getAbkuerzung() : v.toString(); 
						vListe = vListe == null ? s : vListe + ", " + s;
					}
				}
			}
			*/
			return vListe;
		case ix_HatMeldungen:
			List list = (List) fieldGetter.getList(Consts.Hist_meldungenIndex);
			return (list != null && list.size() > 0) ? "ja" : "nein";
		default: 
			throw new XBoxValueManagementException("Interner Attribut-Fehler: " + labelIndex);
		}
		return attrList;
	}
	
	public final static String getEDVKGValue(IRequest request, String attrName) throws XMetaModelQueryException, XBoxValueManagementException, XSecurityException {
		String attrNameSuche = EParseUtilities.getSubstring(attrName, 1);
		String resString = null;
		List <Object> attrList = new Vector<Object>(5) ;
		String actName;
		final BOContext context = (BOContext)request.getContext();
		BOObject personObj = getEDVKGObject(request, attrName);
		final int[] map;
		if (personObj instanceof BOPerson) {
			map = Person2Person;
		} else {
			map = Person2History;
		}
		final String zusArt = personObj.getAttribute(map[ZusArtIndex], true, context);
		final EDVKGFieldGetter fieldGetter = new EDVKGFieldGetterImpl(personObj, map, zusArt, context);
		try {
			Integer labelIndex = _name2Index.get(attrNameSuche);
			if (labelIndex != null) {
				Object object = getEDVKGField(labelIndex, map, fieldGetter);
				if (object instanceof List) {
					attrList = (List<Object>) object;
				} else {
					return (String)object;
				}
			} else {
				attrList.add(attrNameSuche);
			}
	
			for (int count = 0; count < attrList.size(); count++) {
				Object attrId = attrList.get(count);
				if (attrId instanceof Integer) {
					resString = personObj.getAttribute((Integer)attrId, true, context);
				} else {
					String attrStr = (String)attrId;
					if ("getAnrede".equals(attrStr)) {
						resString = getAnrede(fieldGetter);
					} else if (attrStr.indexOf('_') == -1) {
						resString = personObj.getAttribute(attrStr, true, context);
					} else {
						String prefix = EParseUtilities.getSubstring(attrName, 0);
						BoxView viewBox = (BoxView)EEngine.getInstance().getBox("view");
						resString = viewBox.getValue(request, new EValueID(prefix + "_" + attrStr));
					}
				}
				if (!EDataTypeValidation.isNullString(resString)) {
					return resString;
				}
			}
		} catch (SQLException e) { //not gonna happen
			LOGGER.error("SQLException occured!", e);
		}

		return null;
	}
	
	public static String getVorname(String vorname) {
		if (vorname != null) {
			return vorname.split(" ")[0];
		}
		return null;
	}
	
	public static final String getAnrede(EDVKGFieldGetter fieldGetter) throws XMetaModelQueryException, XSecurityException, SQLException {
		String resString = (String) fieldGetter.getField(ZusAnredeIndex);
		if (!EDataTypeValidation.isNullString(resString)) {
			return resString;
		}
		resString = (String) fieldGetter.getField(CodeGeschlechtIndex);
		if (resString == null) {
			return "An";
		}
		resString.trim();
		if (resString.indexOf("M") != -1) {
			return "Herr";
		}
		if (resString.indexOf("W") != -1) {
			return "Frau";
		}
		return "An";
		
	}
	
	/**
	 * Gibt das Postfach zur�ck. Falls das Postfach nicht gesetzt ist, die
	 * Postfach-PLZ aber schon, dann wird ein Leerzeichen zur�ckgegeben.
	 * @param zustell <code>true</code> falls Zustellfelder
	 * @return Das Postfach, ein Leerzeichen f�r Postfach ohne Nummer oder <code>null</code> wenn
	 * kein Postfach vorhanden.
	 * @throws SQLException 
	 */
	public final static String getPostfach(boolean zustell, EDVKGFieldGetter fieldGetter) throws XMetaModelQueryException, XBoxValueManagementException, XSecurityException, SQLException {
		int plzPostfachInd, postfachInd;
		if (zustell) {
			plzPostfachInd = ZusPLZPostfachIndex;
			postfachInd = ZusPostfachIndex;
		} else {
			plzPostfachInd = PLZPostfachIndex;
			postfachInd = PostfachIndex;
		}
		
		/* Falls Zugeh�rige Postleitzahl existiert, dann muss das Postfach genommen werden auch
		 * wenn es leer ist. */
		String plzPostfach = (String) fieldGetter.getField(plzPostfachInd);
		String postfach = (String) fieldGetter.getField(postfachInd);
		if (!EDataTypeValidation.isNullString(plzPostfach)) {
			if (EDataTypeValidation.isNullString(postfach)) return " "; // indicates that there is a postfach, but its number is not set
		}
		return postfach;
	}

	/**
	 * Validiert eine Zustelladresse auf Korrektheit je nach Typ (ZusArt), wobei nur bei 
	 * ZusArt == 'M' eine Konsequenz resultiert. Falls ZusArt == 'M', werden die 
	 * Strassen-Codes und eindeutigen PLZ nicht �berpr�ft.
	 * @throws BOValidationException Falls f�r ZusArt == 'M' eine negative Validierungsmeldung 'geworfen' werden muss
	 * @see BOPerson#writeToDB(boolean, java.sql.Connection)
	 */
	public final static void validateZustellAdresse(BOPerson pers, BOContext context) throws BOValidationException, XSecurityException, XMetaModelNotFoundException {
		String zusArt = (String)pers.getValue(ZusArtIndex, false, context);
		if (isNullOrEmptyString(zusArt)) return;
		long zusOIDStrasse = pers.getReferencedOID(ZusOIDStrasseIndex);
		String zusHausNr = (String)pers.getValue(ZusHausNrIndex, false, context);
		String zusOrt = (String)pers.getValue(ZusOrtIndex, false, context);
		String zusPLZOrt = (String)pers.getValue(ZusPLZOrtIndex, false, context);
		String zusPLZOrtEind = (String)pers.getValue(ZusPLZOrtEindIndex, false, context);
		String zusPLZPostfach = (String)pers.getValue(ZusPLZPostfachIndex, false, context);
		String zusPLZPostfachEind = (String)pers.getValue(ZusPLZPostfachEindIndex, false, context);
		String zusPostfach = (String)pers.getValue(ZusPostfachIndex, false, context);
		long zusOIDCodeOrtLand = pers.getReferencedOID(ZusOIDCodeOrtLandIndex);
		String zusOrtLandName = (String)pers.getValue(ZusOrtLandNameIndex, false, context);
		String zusStrassenName = (String)pers.getValue(ZusStrassenNameIndex, false, context);
		String errorMsg = null;
		boolean validation = false;
		if ("s".equals(zusArt)) {
			/* Bei st�dtischen Adressen wird Existenz von Hausnummer �berpr�ft */
			validation = "Z�rich".equals(zusOrt)
				&& (zusOIDStrasse > 0 && !isNullOrEmptyString(zusHausNr)
					|| validatePLZPair(zusPLZPostfach, zusPLZPostfachEind)
					|| !isNullOrEmptyString(zusPostfach) && validatePLZPair(zusPLZOrt, zusPLZOrtEind))
				&& zusOIDCodeOrtLand <= 0 && isNullOrEmptyString(zusOrtLandName);
			if (!validation) errorMsg = "St�dtische Zustelladresse unvollst�ndig";
		} else if ("e".equals(zusArt)) {
			validation = !isNullOrEmptyString(zusOrt)
				&& (!isNullOrEmptyString(zusStrassenName) && validatePLZPair(zusPLZOrt, zusPLZOrtEind) 
					|| validatePLZPair(zusPLZPostfach, zusPLZPostfachEind)
					|| !isNullOrEmptyString(zusPostfach) && validatePLZPair(zusPLZOrt, zusPLZOrtEind));
			if (!validation) errorMsg = "Stadt-externe Zustelladresse unvollst�ndig";
		} else if ("m".equalsIgnoreCase(zusArt) /* or 'M'*/) {
			validation = !isNullOrEmptyString(zusOrt)
				&& (!isNullOrEmptyString(zusStrassenName) && !isNullOrEmptyString(zusPLZOrt) 
					|| !isNullOrEmptyString(zusPLZPostfach)
					|| !isNullOrEmptyString(zusPostfach) && !isNullOrEmptyString(zusPLZOrt));
			boolean validationOfCodeFields = true;
			if (zusArt.equals("m")) {	
				validationOfCodeFields = isNullOrEmptyString(zusPLZOrtEind) 
					&& isNullOrEmptyString(zusPLZPostfachEind)
					&& zusOIDStrasse <= 0
					&& zusOIDCodeOrtLand <= 0;
			} 
			String msg = null;
			if (!validation) {
				if (zusArt.equals("M") && isManualZustellEmpty(pers, context)) {
					return;
				}
				if (isNullOrEmptyString(zusOrt)) {
					msg = "Die Ortsangabe fehlt.";
				} else {
					msg = "Es fehlt entweder die Strasse und die Orts-PLZ oder es fehlt die Postfach-PLZ oder es fehlt das Postfach und die Orts-PLZ.";
				}
				if (zusArt.equals("M")) throw new BOValidationException("Zustelladresse nicht korrekt: " + msg);
			}
			if (!validationOfCodeFields) {
				if (!isNullOrEmptyString(zusPLZOrtEind)) {
					msg = "Der Strassen-Code darf nicht gesetzt sein.";
					LOGGER.error("Manuelle Zustelladresse: " + msg + " (Person-OID=" + pers.getOID() + ")");
				} else if (!isNullOrEmptyString(zusPLZOrtEind)) {
					msg = "Der L�nder-Code darf nicht gesetzt sein.";
					LOGGER.error("Manuelle Zustelladresse: " + msg + " (Person-OID=" + pers.getOID() + ")");
				} else if (!isNullOrEmptyString(zusPLZOrtEind)) {
					msg = "Die eindeutige Orts-PLZ darf nicht gesetzt sein.";
					LOGGER.error("Manuelle Zustelladresse: " + msg + " (Person-OID=" + pers.getOID() + ")");
				} else /*if (!isNullOrEmptyString(zusPLZPostfachEind) )*/ {
					msg = "Die eindeutige Postfach-PLZ darf nicht gesetzt sein.";
					LOGGER.error("Manuelle Zustelladresse: " + msg + " (Person-OID=" + pers.getOID() + ")");
				}
			}
		}
		if (errorMsg != null) {
			errorMsg += ", OID=" + pers.getOID() +
			"\n ZusArt: " + zusArt +
			"\n ZusOIDStrasse: " + zusOIDStrasse +
			"\n ZusStrassenName: " + zusStrassenName +
			"\n ZusHausNr: " + zusHausNr +
			"\n ZusPostfach: " + zusPostfach +
			"\n ZusPLZPostfach: " + zusPLZPostfach +
			"\n ZusPLZPostfachEind: " + zusPLZPostfachEind +
			"\n ZusPLZOrt: " + zusPLZOrt +
			"\n ZusPLZOrtEind: " + zusPLZOrtEind +
			"\n ZusOrt" + zusOrt +
			"\n ZusOIDCodeOrtLand: " + zusOIDCodeOrtLand +
			"\n ZusOrtLandName: " + zusOrtLandName;
		}
		
	}

	public final static boolean validatePLZPair(String plz, String plzEind) {
		return !isNullOrEmptyString(plz) && !isNullOrEmptyString(plzEind) 
			&& plzEind.substring(0, 4).equals(plz);
	}
	
	public final static boolean isManualZustellEmpty(BOObject bo, BOContext boContext) throws XMetaModelNotFoundException, XSecurityException {
		return  "m".equalsIgnoreCase((String)bo.getValue(ZusArtIndex, false, boContext)) &&
			isNullOrEmptyString((String)bo.getValue(ZusStrassenNameIndex, false, boContext)) &&
			//getValue(ZusOIDStrasseIndex, false, boContext);
			isNullOrEmptyString((String)bo.getValue(ZusHausNrIndex, false, boContext)) &&
			isNullOrEmptyString((String)bo.getValue(ZusAdrZusatzIndex, false, boContext)) &&
			isNullOrEmptyString((String)bo.getValue(ZusPostfachIndex, false, boContext)) &&
			isNullOrEmptyString((String)bo.getValue(ZusPLZPostfachIndex, false, boContext)) &&
			//getValue(ZusPLZPostfachEindIndex, false, boContext);
			isNullOrEmptyString((String)bo.getValue(ZusOrtIndex, false, boContext)) &&
			isNullOrEmptyString((String)bo.getValue(ZusPLZOrtIndex, false, boContext)) &&
			//getValue(ZusPLZOrtEindIndex, false, boContext);
			//getValue(ZusOIDCodeOrtLandIndex, false, boContext);
			isNullOrEmptyString((String)bo.getValue(ZusOrtLandNameIndex, false, boContext)) &&
			EDataTypeValidation.isNullOrFalse((Boolean)bo.getValue(ZusPostlagerndIndex, false, boContext));
	}
	
	/**
	 * Konkubinatsinfo setzen f�r jede Person f�r Count SQL Queries: Bit-Feld
	 * mit 4 Bits: dcba; Belegung wie folgt:
	 * - d = Konkubinat (der Eltern) vorhanden
	 * - c = Der Vater befindet sich innerhalb der Familie bzw. die Person ist der Vater
	 * - b = Die Mutter befindet sich innerhalb der Familie bzw. die Person ist die Mutter
	 * - a = Beide Elternteile befinden sich in der Familie (nur f�r Kinder)
	 * 
	 * Bemerkung: Es kann kein gleichgeschlechtliches Konkubinat geben in der MZV, da
	 * ein Konkubinat nur dann als solches erkannt wird, wenn mindestens ein Kind in der Familie
	 * existiert, bei welchem Vater und Mutter in der gleichen Familie sind.
	 * 
	 * Familienrolle setzen (3 Bits = 8 M�glichkeiten) ab Bitposition 4
	 * 
	 */
	public final static short getFamilienInfoBitField(IPerson p, short predefinedFields) {
		IPerson vorstand = p.getVorstand();
		// Wenn passiv, dann keine Infos setzen
		if (vorstand == null || !p.istAktivAny() || p.getOID() > Consts.AlleHaendischeGrenze) {
			return (short)(predefinedFields & Consts.FI_PredefinedMask); 
		}
		boolean istTechVorstand = p.equals(vorstand);
		// Vorstand ist nie passiv
		IPerson vorstandPartner = vorstand.getPartner();
		boolean istVater = p.getGeschlecht() == 'M' && (istTechVorstand || p.equals(vorstandPartner) && vorstandPartner.istAktivAny()); // Person ist Vater
		boolean istMutter = p.getGeschlecht() == 'W' && (istTechVorstand || p.equals(vorstandPartner) && vorstandPartner.istAktivAny()); // Person ist Mutter 
		boolean istKind = false;
		char partnerBez = p.getPartnerBeziehung();
		IPerson tmp;
		/* Start Berechnung der Konkubinatsinformation */
		int value = (partnerBez == 'k' ? 8 : 0);
		if (istVater || (tmp = p.getVater()) != null && (vorstand.equals(tmp) || tmp.equals(vorstandPartner))) {
			if (!istVater) istKind = true;
			value = value | 4;
		} 
		if (istMutter || (tmp = p.getMutter()) != null && (vorstand.equals(tmp) || tmp.equals(vorstandPartner))) {
			if (!istMutter) istKind = true;
			value = value | 2;
		}
		if ((value & 6) == 6) {
			// Vater und Mutter in derselben Familie
			value = value | 1;
		}
		/* Ende Berechnung der Konkubinatsinformation */
		/* Start Berechnung der Familienrolle */
		int rollenValue = Consts.FI_Familienrollen.Undefined.ordinal();
		if (istKind) {
			if ((value & 1) == 1) {
				rollenValue = Consts.FI_Familienrollen.Kind.ordinal();
			} else if ((value & 2) == 2) {
				rollenValue = Consts.FI_Familienrollen.KindMitMutter.ordinal();
			} else {
				rollenValue = Consts.FI_Familienrollen.KindMitVater.ordinal();
			}
		} else if (p.equals(vorstand) || p.equals(vorstandPartner)) {
			switch (partnerBez) {
			case 'e': 
				rollenValue = Consts.FI_Familienrollen.Ehepartner.ordinal();
				break;
			case 'k':
				rollenValue = Consts.FI_Familienrollen.PartnerImKonkubinat.ordinal();
				break;
			case 'g':
				rollenValue = Consts.FI_Familienrollen.Partner.ordinal();
				break;
			case 'a':
				rollenValue = Consts.FI_Familienrollen.Alleine.ordinal();
				break;
			default:
				rollenValue = Consts.FI_Familienrollen.Undefined.ordinal();
			}
		} else {
			rollenValue = Consts.FI_Familienrollen.Undefined.ordinal();
		}
		/* Ende Berechnung der Familienrolle */
		value = value | (rollenValue << Consts.FI_FamilienrollenBitPosition) | (predefinedFields & Consts.FI_PredefinedMask);
		return (short) value;
	}

	/**
	 * Erstellt den Ersatz f�r eine EGID (solange diese noch nicht erh�ltlich ist).
	 * Format: @see {@link Person#_egid}
	 * Alle Hausnummerzus�tze welche nicht das Format a..z bzw. aa..zz haben, erhalten den Wert 0.
	 * @param oidCodeStrasse
	 * @param hausNr
	 *
	 * @return
	 */
	public static long makeEGID(long oidCodeStrasse, String hausNr) {
		
		int reineHausNr = 0;
		int hausNrZusatz = 0;
		if (hausNr != null) {
			int i = 0;
			while (i < hausNr.length() && hausNr.charAt(i) >= '0' && hausNr.charAt(i) <= '9') i++;
			reineHausNr = i > 0 ? Integer.parseInt(hausNr.substring(0, i)) : 0;
			if (hausNr.length() > i) {
				char z1 =  Character.toLowerCase(hausNr.charAt(i));
				hausNrZusatz = z1 >= 'a' && z1 <= 'z' ? z1 - ('a' - '1') : 0;
				if (hausNr.length() > i + 1) {
					char z2 =  Character.toLowerCase(hausNr.charAt(i+1));
					hausNrZusatz += 10 * (z2 >= 'a' && z2 <= 'z' ? z2 - ('a' - '1') : 0);
				}
			}
		}
		return oidCodeStrasse * 1000000 + reineHausNr * 100 + hausNrZusatz;
	}

}
