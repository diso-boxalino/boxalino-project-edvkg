package ch.objcons.ecom.proj.edvkg.tests;

import static ch.objcons.ecom.proj.edvkg.tests.CompositionDifference.Difference;
import static ch.objcons.ecom.proj.edvkg.tests.CompositionDifference.S1Only;
import static ch.objcons.ecom.proj.edvkg.tests.CompositionDifference.S2Only;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;

public class CompFamilien {

	/* 
	 query used go generate csv:
	 select bx_oidfamilie, group_concat(oid) from bx_person where oid < 500*1000*1000 and bx_statusperson = 'j' 
	 && bx_oidfamilie is not null and bx_OIDKonfession in(1, 2) group by bx_oidfamilie;
	*/
	
//	private static final EnumSet<CompositionDifference> ENABLED_CD = EnumSet.of(S1Only, S2Only);
	
	private static final EnumSet<CompositionDifference> ENABLED_CD = EnumSet.allOf(CompositionDifference.class);
	
	private static final boolean SIMPLE_COMPARISON = true;
	
	public static void main(String[] args) {
		long then = System.currentTimeMillis();
		try {
			Reader r0 = new FileReader("C:/Users/Simon Rikli/Documents/work/mzv/compfamilien/s1familien.csv");
			Reader r1 = new FileReader("C:/Users/Simon Rikli/Documents/work/mzv/compfamilien/s2familien.csv");
			Writer w = new FileWriter("C:/Users/Simon Rikli/Documents/work/mzv/compfamilien/familienComp.csv");
			Appendable sb = w;
			Set<Family> d0 = readData(r0);
			Set<Family> d1 = readData(r1);
			long commonFamilies = 0;
			Iterator<Family> it = d0.iterator();
			while (it.hasNext()) {
				if (d1.remove(it.next())) {
					it.remove();
					commonFamilies++;
				}
			}
			System.out.println("Identische Familien S1, S2: " + commonFamilies);
			System.out.println("S1 Familien: " + d0.size());
			System.out.println("S2 Familien: " + d1.size());
			
			Map<CompositionDifference, Collection<ComparisonInfo>> diff = new HashMap<CompositionDifference, Collection<ComparisonInfo>>();
			for (CompositionDifference cd : CompositionDifference.values()) {
				diff.put(cd, new HashSet<ComparisonInfo>(1024));
			}
			writeln(sb, "Bezeichnung", "S1 Familie", "S2 Familie");
			if (SIMPLE_COMPARISON) {
				checkCompositionSimple(d0, d1, diff);
			} else {
				checkCompositionWithDiff(d0, d1, diff);
			}
			for (Entry<CompositionDifference, Collection<ComparisonInfo>> e : diff.entrySet()) {
				for (ComparisonInfo ci : e.getValue()) {
					writeln(sb, e.getKey(), ci);
				}
			}
			w.close();
			System.out.println("Done, took " + (System.currentTimeMillis() - then) + "ms");
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}


	private static void checkCompositionSimple(Set<Family> d0, Set<Family> d1, Map<CompositionDifference, Collection<ComparisonInfo>> diff) {
		Map<Long, Family> mm0 = memberMapping(d0);
		Map<Long, Family> mm1 = memberMapping(d1);
		System.out.println("S1 Mitglieder: " + mm0.size());
		System.out.println("S2 Mitglieder: " + mm1.size());
		for (Long m0 : mm0.keySet()) {
			if (mm1.remove(m0) == null) {
				addDiff(diff, S1Only, m0);
			}
		}
		for (Long m1 : mm1.keySet()) {
			addDiff(diff, S2Only, null, m1);
		}
	}
	
	private static void checkCompositionWithDiff(Set<Family> d0, Set<Family> d1, Map<CompositionDifference, Collection<ComparisonInfo>> diff) {
		Map<Long, Family> mm0 = memberMapping(d0);
		Map<Long, Family> mm1 = memberMapping(d1);
		System.out.println("S1 Mitglieder: " + mm0.size());
		System.out.println("S2 Mitglieder: " + mm1.size());
		Set<Family> seen = new HashSet<Family>(mm0.size());
		for (Entry<Long, Family> e0 : mm0.entrySet()) {
			long m0 = e0.getKey();
			Family f0 = e0.getValue();
			Family f1 = mm1.get(m0);
			if (f1 == null) {
				if (addDiff(diff, S1Only, m0)) {
					seen.add(f0);
				}
			} else {
				if (addDiff(diff, Difference, f0, f1)) {
					Collections.addAll(seen, f0, f1);
				}
			}
		}
		for (Family family : d1) {
			if (seen.contains(family)) continue;
			
			addDiff(diff, S2Only, null, family);
		}
	}
	
	@SuppressWarnings("unchecked")
	private static boolean addDiff(Map<CompositionDifference, Collection<ComparisonInfo>> diff, CompositionDifference type, Object... info) {
		if (!ENABLED_CD.contains(type)) return false;
		
		List<Set<Long>> listOfMembers = new ArrayList<Set<Long>>(2);
		for (Object o : info) {
			if (o instanceof Family) {
				listOfMembers.add(((Family) o).getMembers());
			} else if (o instanceof Set<?>) {
				listOfMembers.add((Set<Long>) o);
			} else if (o instanceof Long) {
				listOfMembers.add(Collections.singleton((Long) o));
			} else {
				listOfMembers.add(Collections.<Long>emptySet());
			}
		}
		return diff.get(type).add(new ComparisonInfo(listOfMembers));
	}

	private static void writeln(Appendable sb, Object... cols) throws IOException {
		String sep = "";
		for (Object col : cols) {
			writecol(sb, col, sep);
			sep = ",";
		}
		sb.append("\n");
	}
	
	private static void writeln(Appendable sb, CompositionDifference cd, ComparisonInfo ci) throws IOException {
		writecol(sb, cd.getDesc(), "");
		for (Set<Long> m : ci.getListOfMembers()) {
			String s = "" + m;
			s = s.substring(1, s.length() - 1);
			writecol(sb, s, ",");
		}
		sb.append('\n');
	}
	
	private static void writecol(Appendable sb, Object val, String sep) throws IOException {
		sb.append(sep).append('"').append("" + val).append('"');
	}
	
	private static Set<Family> readData(Reader r0) {
		Set<Family> data = new HashSet<Family>(200000);
		CSVReader csvReader = new CSVReader(r0, ',');
		try {
			csvReader.readNext(); // skip header
			for (String[] line : csvReader.readAll()) {
				Set<Long> members = new HashSet<Long>(line.length - 1);
				for (int i = line.length; i-- > 1;) {
					for (String member : line[i].replaceAll("\"", "").split(",")) {
						members.add(Long.parseLong(member));
					}
				}
				long familyOID = Long.parseLong(line[0]);
				data.add(new Family(familyOID, members));
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				r0.close();
			} catch (IOException e) {}
			try {
				csvReader.close();
			} catch (IOException e) {}
		}
		return data;
	}

	private static Map<Long, Family> memberMapping(Collection<Family> data) {
		Map<Long, Family> map = new HashMap<Long, Family>(data.size() * 2);
		for (Family family : data) {
			for (Long member : family.getMembers()) {
				map.put(member, family);
			} 
		}
		return map;
	}
	
	private static final class Family {
		
		private final int _h;
		
		private final long _familyOID;
		
		private final Set<Long> _members;
		
		private Family(long familyOID, Set<Long> members) {
			_familyOID = familyOID;
			_members = members;
			_h = members.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			Set<Long> oMembers = ((Family) obj)._members;
			return _members.size() == oMembers.size() && _members.containsAll(oMembers);
		}
		
		@Override
		public int hashCode() {
			return _h;
		}
		
		public Set<Long> getMembers() {
			return _members;
		}
		
		@Override
		public String toString() {
			return "Family[" + _familyOID + "]: " + _members;
		}
		
	}
	
	private static final class ComparisonInfo {
		
		private final List<Set<Long>> _listOfMembers;
		
		public ComparisonInfo(List<Set<Long>> listOfMembers) {
			_listOfMembers = listOfMembers;
		}
		
		public List<Set<Long>> getListOfMembers() {
			return _listOfMembers;
		}
		
		@Override
		public String toString() {
			return "" + _listOfMembers;
		}
		
		@Override
		public int hashCode() {
			return _listOfMembers.hashCode();
		}
		
		@Override
		public boolean equals(Object o) {
			if (o == this) return true;
			
			if (!(o instanceof ComparisonInfo))  return false;
			
			ComparisonInfo ci = (ComparisonInfo) o;
			return _listOfMembers.equals(ci._listOfMembers);
		}
		
	}
	
}
