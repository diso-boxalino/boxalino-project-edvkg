package ch.objcons.ecom.proj.edvkg.tests;

public enum CompositionDifference {

	S1Only("Mitglied nur in S1 aktiv"), 
	S2Only("Mitglied nur in S2 aktiv"), 
	Difference("Familie unterschiedlich");

	private final String _desc;

	private CompositionDifference(String desc) {
		_desc = desc;
	}

	public String getDesc() {
		return _desc;
	}
}