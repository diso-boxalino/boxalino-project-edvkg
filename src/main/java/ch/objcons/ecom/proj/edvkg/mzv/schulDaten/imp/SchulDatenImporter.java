package ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp;

import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.ADRESSTYP;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.ANREDE_LP;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.ERSTSPRACHE;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.KLASSENSTUFE;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.KLASSEN_ID;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.NACHNAME_LP;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.SH_HAUSNUMMER;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.SH_HAUSNUMMER_ZUSATZ;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.SH_ID;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.SH_NAME;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.SH_ORT;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.SH_PLZ;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.SH_STRASSE;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.SH_STR_NR;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.Schulkreis;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.VORNAME_LP;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.ZIP_MUTTER;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.ZIP_VATER;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.PersonType.CHILD;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.PersonType.PARENT;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.SchulDatenMapping.KONF_MAPPING;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.SchulDatenMapping.M_MAPPING;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.SchulDatenMapping.S_MAPPING;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.SchulDatenMapping.V_MAPPING;

import java.io.File;
import java.io.FilenameFilter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.expr.XExpression;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.BOPerson;
import ch.objcons.ecom.proj.edvkg.mzv.BOPersonHistory;
import ch.objcons.ecom.proj.edvkg.mzv.BoxMZV;
import ch.objcons.ecom.proj.edvkg.mzv.imp.PostImport;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchuelerInfo;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchulHaus;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchulJahrBeginn;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchulKlasse;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchulSprache;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchulStufe;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVSupport.CSVRows;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVSupport.EntryRow;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.PersonType;
import ch.objcons.ecom.query.XMaxRowException;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

/**
 * Reads the SchulDatenImport CSV-File (lexically highest *.csv file located at {@link BoxMZV#getSchulDatenImportFolder()}.  
 * Data is written primarily into PersonHistory and classes relevant to SchulDaten.
 * After the main import, a {@link PostImport} is executed to activate the newly created PersonHistory objects and 
 * finally the schuelerInfo reference on Person is set.
 * 
 * @see SchulDatenMapping
 * @see CSVFormatInfo
 */
public class SchulDatenImporter {

	/**
	 * Main import lock to prevent concurrent imports.
	 */
	private static final Lock _lock = new ReentrantLock();
	
	private static final ILogger LOGGER = Logger.getLogger(SchulDatenImporter.class);
	
	private static final Pattern 
		POSTFACH = Pattern.compile("^\\s*?(?i)Postfach\\s*?"), 
		IS_KINDERGARTEN = Pattern.compile("(?i)Kindergarten"), 
		IS_GYM_OR_SEK = Pattern.compile("((?i)gym)|((?i)sek)"),
		EXTRACT_SCHULSTUFE = Pattern.compile("\\d*"),
		EMPTY_PATTERN = Pattern.compile("((?i)NULL)|((?i)leer)")
	;

	private int _schuljahr;
	
	private final MetaObject _moPerson, _moPersonHistory, _moSchulHaus, _moSchulKlasse, _moSchuelerInfo, _moSchulStufe;
	
	private final Map<String, BOSchulHaus> _schulHaeuser = new HashMap<String, BOSchulHaus>();
	
	private final Map<String, BOSchulKlasse> _schulKlassen = new HashMap<String, BOSchulKlasse>();
	
	private final Map<String, BOSchulStufe> _schulStufen = new HashMap<String, BOSchulStufe>();
	
	private final Map<String, BOSchulSprache> _schulSprachen = new HashMap<String, BOSchulSprache>();

	private final Set<Long> _zipImported = new HashSet<Long>();
	
	private Map<String, BOObject> _konfCodeToBO = null;

	private Map<PersonType, BOObject> _typeToZivilStand;
	
	public SchulDatenImporter() {
		MetaObjectLookup moLookup = MetaObjectLookup.getInstance();
		try {
			_moPerson = moLookup.getMetaObject(BOPerson.CLASS_NAME);
			_moPersonHistory = moLookup.getMetaObject(BOPersonHistory.CLASS_NAME);
			_moSchulHaus = moLookup.getMetaObject(BOSchulHaus.CLASS_NAME);
			_moSchulKlasse = moLookup.getMetaObject(BOSchulKlasse.CLASS_NAME);
			_moSchuelerInfo = moLookup.getMetaObject(BOSchuelerInfo.CLASS_NAME);
			_moSchulStufe = moLookup.getMetaObject(BOSchulStufe.CLASS_NAME);
		} catch (XMetaModelNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * @return <code>true</code>, if a new import file was available and imported successfully, <code>false</code> if there 
	 * was no import file, or the import terminated unexpectedly.
	 */
	public static boolean doImport() {
		String path = BoxMZV.getInstance().getSchulDatenImportFolder();
		File[] files = new File(path).listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				return name.matches(".*(?i)\\.csv$");
			}
			
		});
		if (files == null) {
			LOGGER.warn("couldn't list files for path \"" + path + "\" (does the specified path exist?)");
			return false;
		}
		if (files.length == 0) {
			LOGGER.debug("no schuldaten import file found in folder: " + path);
			return false;
		}
		final Lock lock = _lock;
		if (!lock.tryLock()) {
			LOGGER.warn("schuldaten import already running, ignoring this attempt to run");
			return false;
		}
		try {
			TreeSet<File> sortedFiles = new TreeSet<File>(Arrays.asList(files));
			boolean didImport = new SchulDatenImporter().doImport(sortedFiles.pollLast());
			for (File fileToSkip : sortedFiles) { 
				if (descendIntoDir(fileToSkip, "skipped")) {
					LOGGER.info("skipped file " + fileToSkip);
				}
			}
			return didImport;
		} finally {
			lock.unlock();
		}
	}
	
	public boolean doImport(File importFile) {
		final Lock lock = _lock;
		if (!lock.tryLock()) {
			LOGGER.warn("schuldaten import already running, ignoring this attempt to run");
			return false;
		}
		try {
			long then = System.currentTimeMillis();
			LOGGER.info("executing schuldaten import for file: " + importFile);
			
			IRequest request = BoxalinoUtilities.getDummyRequest();
			BOContext context = (BOContext) request.getContext();
			context.setGUIClient(true); // hack to ignore security during import
			BLTransaction trx = BLTransaction.startTransaction(context);
			
			checkPersonHistoryLastEntryState();
			long prevMaxSchuelerOID = getMaxOID(BOSchuelerInfo.CLASS_NAME, context);
			CSVRows rows = CSVSupport.read(importFile.getAbsolutePath(), CSVFormatInfo.DEFAULT_MAPPING);
			try {
				processRows(rows, trx, context);
			} finally {
				rows.close();
			}
			checkPersonHistoryLastEntryState();
			
			// set bx_schulAktiv to 'n' of all persons (except for manually created) not imported by the current import 
			// the ones imported by the current import have their bx_schulAktiv set to 'j'
			execUpdate("update bx_schuelerinfo set bx_schulAktiv = 'n' where OID <= " + prevMaxSchuelerOID + " and bx_PersonschuelerInfosOID < 500000000");
			
			descendIntoDir(importFile, "done");
			LOGGER.info("schuldaten import part1 completed, took " + (System.currentTimeMillis() - then) + "ms");
			LOGGER.info("schuldaten import calling PostImport");
			
			BoxMZV.getInstance().directPostImport(request, "0", false);
			
			// assign bx_schulerInfo after the PostImport has completed, because only then there is a Person object for any PersonHistory object we created. 
			execUpdate("update bx_person p left join bx_schuelerinfo si on p.OID = si.bx_PersonschuelerInfosOID && bx_lastEntry = 'j' set bx_schuelerInfo = si.OID;");
			LOGGER.info("schuldaten import part2 completed");
			return true;
		} catch (Exception e) {
			LOGGER.fatal("schuldaten import terminated unexpectedly", e);
			return false;
		} finally {
			lock.unlock();
		}
	}
	
	/**
	 * Each row is written to the database independently, therefore the atomicity of the import
	 * as a whole is not guaranteed. If a {@link RecordException} occurs while processing a row, the current
	 * row is discarded and the import is continued, unless the exception threshold is hit.
	 */
	private void processRows(CSVRows rows, BLTransaction trx, BOContext context) {
		final int recordFaultThreshold = 100;
		int seqRecordFaultCount = 0;
		int fatalExceptionRecordFaultCount = 0;
		int currentRowsIndex = 0;
		for (EntryRow e : rows) {
			try {
				currentRowsIndex++;

				PersonValues schueler = new PersonValues(e, S_MAPPING, CHILD);
				PersonValues mutter = new PersonValues(e, M_MAPPING, PARENT);
				PersonValues vater = new PersonValues(e, V_MAPPING, PARENT);
				
				asPersonHistory(schueler, trx, context);
				asPersonHistory(mutter, trx, context);
				asPersonHistory(vater, trx, context);
				
				BOSchulHaus boSchulHaus = getSchulHaus(e, trx, context);
				BOSchulKlasse boSchulKlasse = getSchulKlasse(e, boSchulHaus, trx, context);
				getSchulStufe(e, boSchulKlasse, trx, context);
				getSchuelerInfo(e, schueler, boSchulKlasse, trx, context);
				
				trx.commit();
				seqRecordFaultCount = 0;
				fatalExceptionRecordFaultCount = 0;
			} catch (RecordException x) {
				if(e != null && e.size() > 0) {
					LOGGER.error("Fehler bei der Verarbeitung der Zeile '" + e.getLineNumber() + "', ZIP-Nr: '" + e.get(0) + "', Fehler: '" + x.getMessage() + "'");
				}else{
					LOGGER.error("Fehler bei der Verarbeitung der Zeile '" + currentRowsIndex + "', ZIP-Nr: 'Unbekannt NULL', Fehler: '" + x.getMessage() + "'");
				}
					if (++seqRecordFaultCount > recordFaultThreshold) {
					throw new RuntimeException("maximum sequential record fault count exceeded!", x);
				}
			} catch (Exception x) {
				if(e != null && e.size() > 0) {
					LOGGER.fatal("Fehler bei der Verarbeitung der Zeile '" + e.getLineNumber() + "', ZIP-Nr: '" + e.get(0) + "', Fehler: '" + x.getMessage() + "' \n\n" + e);
				}else{
					LOGGER.fatal("Fehler bei der Verarbeitung der Zeile '" + currentRowsIndex + "', ZIP-Nr: 'Unbekannt NULL', Fehler: '" + x.getMessage() + "' \n\n" + e);
				}
				if (++fatalExceptionRecordFaultCount > recordFaultThreshold) {
					throw new RuntimeException("maximum fatal exception count exceeded!", x);
				}
			} 
			// discard unnecessary cached objects and invalid objects
			trx.emptyBuffers();
		}
	}
	
	private void asPersonHistory(PersonValues pv, BLTransaction trx, BOContext context) throws XSecurityException, XMetaException, XDataTypeException, XExpression, XMaxRowException {
		String zip = pv.zip();
		if (zip == null || zip.isEmpty()) return;
		
		if (!_zipImported.add(Long.parseLong(zip))) {
			// it is expected to have duplicate entries for parents, since parents exist per child entry
			if (pv.getType() == PARENT) return;
			
			recordFault("multiple records for Schueler-ZIP");
		}
		
		long oid = Long.parseLong(zip);
		BOPerson checkPerson = (BOPerson) BoxalinoUtilities.getObjectReadOnly(_moPerson, oid, context);
		BOPersonHistory checkPH = getLastPHEntry(zip, trx, context); 
		BOPersonHistory ph = null;
		boolean isBeingImported = checkPH != null && "A".equals(checkPH.getAttribute("RecordStatus", context));
		boolean isPersonActive = checkPerson != null && checkPerson.isActiveSP(context);
		if (isBeingImported || isPersonActive) {
			// don't update the person in the case that the person is activeSP
			// because it will be imported regularly (as data changes) by the OIZ import
			ph = checkPH;
			if (ph == null) {
				LOGGER.warn("expected personhistory not to be null for person with oid " + oid);
			}
		}
		if (ph == null) {
			if (checkPH != null) {
				checkPH.setValue("lastEntry", false, context);
			}
			ph = (BOPersonHistory) trx.createNewObject(_moPersonHistory);
			// set the reference to a possibly inexistent object which is OK; resolved 
			// by {@link PostImportHelper#activateMutationen}
			ph.setValue("OIDPerson", oid, context);
			if (pv.getType() == CHILD) {
				EntryRow e = pv.getEntryRow();
				String zipMutter = e.get(ZIP_MUTTER), zipVater = e.get(ZIP_VATER);
				if (zipMutter != null && !zipMutter.isEmpty()) {
					ph.setValue("OIDMutter", Long.parseLong(zipMutter), context);
				}
				if (zipVater != null && !zipVater.isEmpty()) {
					ph.setValue("OIDVater", Long.parseLong(zipVater), context);
				}
			}
			ph.setValue("lastEntry", true, context);
			
			ph.setAttribute("Name", pv.name(), context);
			ph.setAttribute("Vorname", pv.vorname(), context);
			String geschlecht = unmaskEmpty(pv.geschlecht());
			ph.setAttribute("CodeGeschlecht", geschlecht);
			ph.setAttribute("ZusAnrede", getAnrede(geschlecht), context);
			String gebdat = pv.gebdat();
			if (gebdat != null) {
				try {
					ph.setValue("Geburtsdatum", context.getDateFormat().parse(gebdat), context);
				} catch (ParseException x) {
					LOGGER.warn("failed to parse gebdat. ZIP: '" + zip + "'", x);
				}
			}
			setAddress(pv, ph, trx, context);
			BOObject konfession = mapToKonfession(pv.rel(), context);
			ph.setReferencedObject("OIDKonfession", konfession);
			if (konfession != null) {
				ph.setAttribute("Konfession", konfession.getAttribute("code", context), context);
			}
			ph.setAttribute("RecordStatus", "A", context);
			ph.setValue("StatusPerson", false, context);
			ph.setAttribute("User", "Schule", context);
			ph.setReferencedObject("OIDCodeZivilstand", getZivilstand(pv, context));
		}
		if (ph.getValue("rpgAktiv") == null) { // assign initially
			ph.setValue("rpgAktiv", true, context);
			// set rpgAktiv to true on checkPerson implicitly {@link PostImportHelper#activateMutationen}
			ph.setAttribute("RecordStatus", "A", context);
		}
	}
	
	private String getAnrede(String geschlecht) {
		if (geschlecht == null || geschlecht.isEmpty()) return null;
		
		String g = geschlecht.substring(0, 1);
		if ("w".equalsIgnoreCase(g)) return "Frau";
		
		if ("m".equalsIgnoreCase(g)) return "Herr";
		
		return null;
	}

	private String unmaskEmpty(String value) {
		if (value == null || EMPTY_PATTERN.matcher(value).matches()) {
			return null;
		}
		return value;
	}
	
	private BOObject getZivilstand(PersonValues pv, BOContext context) {
		if (_typeToZivilStand == null) {
			Map<PersonType, BOObject> m = new EnumMap<PersonType, BOObject>(PersonType.class);
			m.put(CHILD, getFirst("CodeZivilstand", "CodeZivilstand_code == 'LEDIG'", null, context));
			m.put(PARENT, getFirst("CodeZivilstand", "CodeZivilstand_code == 'UNBEK'", null, context));
			_typeToZivilStand = Collections.unmodifiableMap(m);
		}
		return _typeToZivilStand.get(pv.getType());
	}

	private void setAddress(PersonValues pv, BOPersonHistory ph, BLTransaction trx, BOContext context) throws XMetaException, XDataTypeException, XSecurityException {
		if (pv.hausnr().length() > 4) recordFault("max len for HausNr exceeded");
		
		if (pv.hausnrzus().length() > 4) recordFault("max len for ZusHausNr exceeded");
		
		BOObject boCodeStrasse = getStreet(pv.strnr(), pv.strasse(), trx, context);
		ph.setReferencedObject("OIDCodeStrasse", boCodeStrasse);
		
		ph.setAttribute("HausNr", pv.hausnr(), context);
		ph.setAttribute("ZusHausNr", pv.hausnrzus(), context);
		
		String postfach = null, plzPostfach = null, plzOrt = null, adrZus = null;
		String[] pfSpl = POSTFACH.split(pv.adresszus(), 2);
		if (pfSpl.length == 2) {
			postfach = pfSpl[1];
			plzPostfach = pv.plz();
		} else {
			plzOrt = pv.plz();
			adrZus = pv.adresszus();
		}
		ph.setAttribute("Postfach", postfach, context);
		ph.setAttribute("PLZPostfach", plzPostfach, context);
		ph.setAttribute("PLZOrt", plzOrt, context);
		ph.setAttribute("AdrZusatz", adrZus, context);
		ph.setAttribute("Ort", pv.ort(), context);
	}
	
	private BOObject getStreet(String streetNumber, String streetName, BLTransaction trx, BOContext context) throws XSecurityException, XMetaException, XDataTypeException {
		streetNumber = unmaskEmpty(streetNumber);
		if (streetNumber == null || streetNumber.isEmpty()) return null;
		
		long oid = Long.parseLong(streetNumber);
		BOObject boCodeStrasse = trx.loadObject("CodeStrasse", oid);
		if (boCodeStrasse.isNew()) {
			boCodeStrasse.setAttribute("Name", streetName);
			boCodeStrasse.setValue("validFrom", new Date(), context);
			boCodeStrasse.setAttribute("KurzName", streetName);
			boCodeStrasse.setValue("Status", Boolean.TRUE, context);
		}
		return boCodeStrasse;
	}
	
	private BOObject mapToKonfession(String konfession, BOContext context) {
		String code = KONF_MAPPING.get(konfession);
		if (_konfCodeToBO == null) {
			Map<String, BOObject> konfCode2BO = new HashMap<String, BOObject>();
			for (String key : KONF_MAPPING.values()) {
				BOObject bo = BoxalinoUtilities.getObjectByKey("Konfession", "code", key, null, context);
				konfCode2BO.put(key, bo);
			}
			_konfCodeToBO = Collections.unmodifiableMap(konfCode2BO);
		}
		return _konfCodeToBO.get(code);
	}
	
	private BOSchulHaus getSchulHaus(EntryRow e, BLTransaction trx, BOContext context) throws XMetaException, XDataTypeException, XSecurityException {
		String shId = e.get(SH_ID);
		if (shId == null || shId.isEmpty()) recordFault("cannot determine or create Schulhaus because SH_ID is not defined");
		
		BOSchulHaus sh = _schulHaeuser.get(shId);
		if (sh != null) return sh;
		
		String cn = BOSchulHaus.CLASS_NAME; 
		sh = getFirst(cn, cn + "_externalId == '" + shId + "'", trx, context);
		if (sh == null) {
			sh = (BOSchulHaus) trx.createNewObject(_moSchulHaus);
		}
		sh.setAttribute("adressTyp", e.get(ADRESSTYP), context);
		sh.setAttribute("name", e.get(SH_NAME), context);
		sh.setReferencedObject("OIDStrasse", getStreet(e.get(SH_STR_NR), e.get(SH_STRASSE), trx, context));
		sh.setAttribute("strassenName", e.get(SH_STRASSE), context);
		sh.setAttribute("hausNr", e.get(SH_HAUSNUMMER), context);
		sh.setAttribute("hausNrZusatz", e.get(SH_HAUSNUMMER_ZUSATZ), context);
		sh.setAttribute("plz", e.get(SH_PLZ), context);
		sh.setAttribute("ort", e.get(SH_ORT), context);
		sh.setAttribute("schulKreis", e.get(Schulkreis), context);
		sh.setAttribute("externalId", shId);
		
		_schulHaeuser.put(shId, sh);
		return sh;
	}
	
	private BOSchulKlasse getSchulKlasse(EntryRow e, BOSchulHaus boSchulHaus, BLTransaction trx, BOContext context) throws XSecurityException, XMetaException, XDataTypeException {
		String id = e.get(KLASSEN_ID);
		BOSchulKlasse sk = _schulKlassen.get(id);
		if (sk != null) return sk; 
			
		String cn = BOSchulKlasse.CLASS_NAME;
		String filter = cn + "_id" + " == " + id;
		sk = getFirst(cn, filter, trx, context);
		if (sk == null) {
			sk = (BOSchulKlasse) trx.createNewObject(_moSchulKlasse);
		}
		sk.setAttribute("id", id, context);
		sk.setValue("schuljahr", getSchuljahr(trx, context), context);
		sk.setAttribute("lpAnrede", e.get(ANREDE_LP), context);
		sk.setAttribute("lpVorname", e.get(VORNAME_LP), context);
		sk.setAttribute("lpName", e.get(NACHNAME_LP), context);
		sk.setReferencedObject("SchulHausschulKlassenOID", boSchulHaus);
		
		_schulKlassen.put(id, sk);
		return sk;
	}
	
	private int getSchuljahr(BLTransaction trx, BOContext context) throws XMetaException, XDataTypeException, XSecurityException {
		if (_schuljahr != 0) return _schuljahr;
		
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		String cn = BOSchulJahrBeginn.CLASS_NAME;
		BOSchulJahrBeginn boSchuljahr = getFirst(cn, "", cn + "_per_desc", trx, context);
		if (boSchuljahr != null) {
			Date per = (Date) boSchuljahr.getValue("per", context);
			Calendar perCal = Calendar.getInstance();
			perCal.setTime(per);
			if (year != perCal.get(Calendar.YEAR)) {
				boSchuljahr = null; // must create new entry
			}
		}
		if (boSchuljahr == null) {
			boSchuljahr = (BOSchulJahrBeginn) trx.createNewObject(cn);
			boSchuljahr.setValue("per", cal.getTime(), context);
		}
		return _schuljahr = year;
	}

	private BOSchulStufe getSchulStufe(EntryRow e, BOSchulKlasse boSchulKlasse, BLTransaction trx, BOContext context) throws XSecurityException, XMetaException, XDataTypeException {
		String name = e.get(KLASSENSTUFE);
		String cacheKey = name.toLowerCase().trim();
		BOSchulStufe boSchulStufe = _schulStufen.get(cacheKey);
		if (boSchulStufe == null) {
			String cn = BOSchulStufe.CLASS_NAME;
			String filter = cn + "_name like '" + name + "'";
			boSchulStufe = getFirst(cn, filter, trx, context);
			if (boSchulStufe == null) {
				boSchulStufe = (BOSchulStufe) trx.createNewObject(_moSchulStufe);
			}
			boSchulStufe.setValue("stufe", parseStufe(name), context);
			boSchulStufe.setAttribute("name", name, context);
			_schulStufen.put(cacheKey, boSchulStufe);
		}
		boSchulStufe.addSchulKlasse(boSchulKlasse);
		return boSchulStufe;
	}
	
	private int parseStufe(String name) {
		if (IS_KINDERGARTEN.matcher(name).find()) return 0;
		
		Matcher m = EXTRACT_SCHULSTUFE.matcher(name);
		if (!m.find()) recordFault("stufe has illegal format, expected a number " + name);
		
		int offset = IS_GYM_OR_SEK.matcher(name).find() ? 6 : 0;
		return offset + Integer.parseInt(m.group());
	}

	private BOSchuelerInfo getSchuelerInfo(EntryRow e, PersonValues schueler, BOSchulKlasse boSchulKlasse, BLTransaction trx, BOContext context) throws XSecurityException, XMetaException, XDataTypeException {
		String zip = schueler.zip();
		String cn = BOSchuelerInfo.CLASS_NAME;
		String filter = cn + "_PersonschuelerInfosOID" + " == " + zip + " && " + cn + "_lastEntry == true";
		BOSchuelerInfo prevSi = getFirst(cn, filter, trx, context);
		// avoid creation of new si if present has identical data?
		// (fields): rpgKG, erstSprache, aktSchulklasse
		// update in any case: importDate
		// update if necessary: lastEntry, schulAktiv, rpgSchuleOffset, rpgSperre
		
		if (prevSi != null) {
			prevSi.setValue("lastEntry", false, context);
		}
		
		BOSchuelerInfo si = (BOSchuelerInfo) trx.createNewObject(_moSchuelerInfo);
		si.setReferencedObject("rpgKG", getRPGKG(schueler, trx, context));
		si.setReferencedObject("erstSprache", getErstSprache(e.get(ERSTSPRACHE), trx, context));
		si.setReferencedObject("aktSchulklasse", boSchulKlasse);
		si.setValue("PersonschuelerInfosOID", Long.parseLong(zip), context);
		si.setValue("importDate", new Date(), context);
		si.setValue("lastEntry", true, context);
		si.setValue("schulAktiv", true, context);
		if (prevSi != null) {
			si.setValue("rpgSchuleOffset", prevSi.getValue("rpgSchuleOffset", context), context);
			si.setValue("rpgSperre", prevSi.getValue("rpgSperre", context), context);
		}
		si.updateRPGStufe(boSchulKlasse.getSchulStufe(context), context);
		return si;
	}
	
	private BOObject getRPGKG(PersonValues pv, BLTransaction trx, BOContext context) throws XMetaModelQueryException, XSecurityException {
		String konf = KONF_MAPPING.get(pv.rel());
		if (konf == null) return null;
		
		String cn = "CodeHausKG";
		String fStrNr = cn + "_OIDStrasse == " + pv.strnr(); 
		String fHausNr = cn + "_HausNr like '" + pv.hausnr() + pv.hausnrzus() + "'";
		String fKonf = cn + "_Konfession == '" + konf + "'";
		BOObject boCodeHausKG = getFirst(cn, fStrNr + " && " + fHausNr + " && " + fKonf, trx, context);
		if (boCodeHausKG == null) {
			boCodeHausKG = getFirst(cn, fStrNr + " && " + fKonf, trx, context); // try weak match (w/o hausNr)
			String matchParams = pv.strasse() + " " + (pv.hausnr() + pv.hausnrzus()) + " (strnr: " + pv.strnr() + ", konf: " + konf + ")";
			if (boCodeHausKG == null) {
				LOGGER.warn("failed to find matching KG for " + matchParams);
				return null;
			} else {
				LOGGER.warn("couldn't find strong matching KG for " + matchParams);
			}
		}
		return boCodeHausKG.getReferencedObject("OIDKG", context);
	}

	private BOSchulSprache getErstSprache(String name, BLTransaction trx, BOContext context) throws XSecurityException, XMetaException, XDataTypeException {
		BOSchulSprache bo = _schulSprachen.get(name);
		if (bo != null) return bo;
		
		bo = (BOSchulSprache) BoxalinoUtilities.getObjectByKey(BOSchulSprache.CLASS_NAME, "bezeichnung", name, null, context);
		if (bo == null) {
			bo = (BOSchulSprache) trx.createNewObject(BOSchulSprache.CLASS_NAME);
			bo.setAttribute("bezeichnung", name, context);
		}
		_schulSprachen.put(name, bo);
		return bo;
	}
	
	// Utility methods used within the import
	
	private static boolean descendIntoDir(File file, String dirName) {
		File dir = new File(file.getParent() + "/" + dirName);
		dir.mkdirs();
		File dest = new File(dir + "/" + file.getName());
		boolean success = file.renameTo(dest);
		if (!success) {
			LOGGER.warn("failed to move file (" + file + " > " + dest + ")");
		}
		return success;
	}
	
	private static BOPersonHistory getLastPHEntry(String zip, BLTransaction trx, BOContext context) {
		String cn = BOPersonHistory.CLASS_NAME;
		String filter = cn + "_lastEntry == true && " + cn + "_OIDPerson == " + zip;
		try {
			@SuppressWarnings("unchecked")
			List<BOObject> entries = BoxalinoUtilities.executeQuery(cn, filter, "", 1, trx, context);
			int size = entries.size();
			if (size == 0) return null;
			
			if (size > 1) {
				LOGGER.warn("Found " + size + " entries with lastEntry == 'j' for OID " + zip);
			}
			return (BOPersonHistory) entries.get(0);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private static void checkPersonHistoryLastEntryState() {
		Connection c = null;
		Statement stmt = null;
		try {
			c = DBConnectionPool.instance().getConnection();
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("select count(1) from bx_personhistory where bx_lastentry = 'j' group by bx_oidperson having count(1) > 1");
			long count = rs.next() ? rs.getLong(1) : -1;
			if (count > 0) {
				LOGGER.warn("PersonHistory lastEntry invariant, count: " + count);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			DBConnectionPool.instance().ungetConnection(c);
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					LOGGER.error("SQLException occurred!", e);
				}
			}
		}
	}
	
	private static long getMaxOID(String className, BOContext context) {
		try {
			@SuppressWarnings("unchecked")
			List<BOObject> bos = BoxalinoUtilities.executeQuery(className, className + "_OID_desc", null, 1, null, context);
			return !bos.isEmpty() ? bos.get(0).getOID() : -1;
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private static <T extends BOObject> T getFirst(String className, String filter, BLTransaction trx, BOContext context) {
		return (T) getFirst0(className, filter, "", trx, context);
	}

	@SuppressWarnings("unchecked")
	private static <T extends BOObject> T getFirst(String className, String filter, String order, BLTransaction trx, BOContext context) {
		return (T) getFirst0(className, filter, order, trx, context);
	}
	
	// must do like so because of "no unique maximal instance exists for type variable [...]" issue
	private static BOObject getFirst0(String className, String filter, String order, BLTransaction trx, BOContext context) {
		try {
			@SuppressWarnings("unchecked")
			List<BOObject> bos = BoxalinoUtilities.executeQuery(className, filter, order, 1, trx, context);
			if (bos.isEmpty()) return null;
			
			return bos.get(0);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private static void execUpdate(String sql) {
		Connection c = null;
		Statement stmt = null;
		try {
			c = DBConnectionPool.instance().getConnection();
			stmt = c.createStatement();
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			DBConnectionPool.instance().ungetConnection(c);
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					LOGGER.error("SQLException occurred!", e);
				}
			}
		}
	}
	
	/**
	 * Stops processing the current row by throwing an Exception.
	 * @param message what's wrong with the record
	 * @throws RecordException using the specified message
	 */
	private void recordFault(String message) {
		throw new RecordException(message);
	}
	
	/**
	 * RuntimeException used to indicate that the Record has unexpected or invalid data.
	 * The RecordException is logged without stack-trace. 
	 */
	private static class RecordException extends RuntimeException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3853217533795878620L;
		
		public RecordException(String message) {
			super(message);
		}

	}
	
}
