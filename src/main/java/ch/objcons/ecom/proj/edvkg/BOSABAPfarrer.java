/*
 * Created on 28.01.2005
 *
 * (c) by Boxalino
 */
package ch.objcons.ecom.proj.edvkg;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.proj.edvkg.wcache.CacheSABAWochen;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.log.Log;

/**
 * @author Dominik Raymann
 */
public class BOSABAPfarrer extends BOObject {
	
	public static String CLASS_NAME= "SABAPfarrer";	
	
	public void writeToDB(boolean isInsert, Connection con) throws SQLException,
			XMetaException, XSecurityException, XBOConcurrencyConflict {
		
		this.setOID();
		BOContext boContext = new BOContext(Locale.getDefault());
		
		BOObject boKG = this.getReferencedObject("OIDKG", boContext);
		String kgID = boKG.getAttribute("ID",boContext);
		while (kgID.length()<3) {
			kgID = "0".concat(kgID);
		}
		
		String pID = ""+this.getOID();
		while (pID.length()<5) {
			pID = "0".concat(pID);
		}
		String id = pID.concat(kgID);
		try {
			this.setAttribute("ID",id,boContext,null);
		} catch (XDataTypeException e) {
			Log.logSystemAlarm("Error in BOSABAPfarrer, writeToDB: "+e.getMessage());
			return;
		}
		
		CacheSABAWochen.getInstance(boContext).clearCache();
		
		super.writeToDB(isInsert, con);
	}
	
	
	public BOSABAPfarrer(BLTransaction trans, long oid) {
		super(trans, oid);
		_watchChanges = true;
		_recordChanges = true;
		getRecordedAttributesInclude().add("Name");
		getRecordedAttributesInclude().add("Vorname");
		getRecordedAttributesInclude().add("Kurz");
		getRecordedAttributesInclude().add("Zusatz1");
		getRecordedAttributesInclude().add("Zusatz2");
		getRecordedAttributesInclude().add("StrassePostfach");
		getRecordedAttributesInclude().add("PLZOrt");
		getRecordedAttributesInclude().add("Tel1");
		getRecordedAttributesInclude().add("Tel2");
		getRecordedAttributesInclude().add("Fax1");
		getRecordedAttributesInclude().add("Fax2");
		getRecordedAttributesInclude().add("Bemerkung");
		getRecordedAttributesInclude().add("eMail");
		getRecordedAttributesInclude().add("Geschlecht");
		getRecordedAttributesInclude().add("Pfarrer");
		getRecordedAttributesInclude().add("Verwalter");
		getRecordedAttributesInclude().add("aktiv");
		getRecordedAttributesInclude().add("ID");
		getRecordedAttributesInclude().add("StrasseNr");
		getRecordedAttributesInclude().add("Anrede");
		getRecordedAttributesInclude().add("communicationMethod");
		
	}
}
