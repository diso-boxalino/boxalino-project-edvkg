package ch.objcons.ecom.proj.edvkg.mzv.schulDaten;

import ch.objcons.ecom.api.IRequest;

import com.boxalino.server.Request;

public class RPGSupport {

	private RPGSupport() {}
	
	public static boolean isRPG() {
		return isRPG(Request.getCurrentOrDummy());
	}
	
	public static boolean isRPG(IRequest request) {
		return Boolean.parseBoolean(request.getParameters().getParameter("param_isRPG"));
	}
	
}
