package ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOObject;

public class BOSchulSprache extends BOObject {

	public static final String CLASS_NAME = "SchulSprachen";
	
	public BOSchulSprache(BLTransaction arg0, long arg1) {
		super(arg0, arg1);
	}
	
}
