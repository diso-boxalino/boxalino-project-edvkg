package ch.objcons.ecom.proj.edvkg.mzv.imp;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOObject;

public class BOStimmrechtAufenthaltsarten extends BOObject {

	public static final String CLASS_NAME = "StimmrechtAufenthaltsarten";
	
	public BOStimmrechtAufenthaltsarten(BLTransaction arg0, long arg1) {
		super(arg0, arg1);
	}

}
