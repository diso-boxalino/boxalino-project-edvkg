package ch.objcons.ecom.proj.edvkg;

/**
 * exception for the security system.
 */

public class DummyException extends RuntimeException {


	public DummyException() {
		super();
	}
	public DummyException(String s) {
		super(s);
	}
}
