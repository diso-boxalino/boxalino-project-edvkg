package ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.system.security.XSecurityException;

public class BOSchulStufe extends BOObject {

	public static final String CLASS_NAME = "SchulStufe";
	
	public BOSchulStufe(BLTransaction arg0, long arg1) {
		super(arg0, arg1);
	}
	
	public void addSchulKlasse(BOSchulKlasse boSchulKlasse) throws XMetaException, XSecurityException {
		addListReferencedObject("schulKlassen", boSchulKlasse);
		boSchulKlasse.setReferencedObject("SchulStufeschulKlassenOID", this);
	}
	
}
