package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.util.HashMap;
import java.util.Vector;

import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.MetaService;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.query.StandardQuery;
import ch.objcons.ecom.system.security.XSecurityException;


public class CodeTableEDVKG {

	HashMap<String, Integer> codeList;

	/**
	 * Class to load the Code Tables for the conversion
	 */
	public CodeTableEDVKG (BLTransaction trans, BOContext context, MetaService metaService, String className)
			throws XMetaModelNotFoundException, XMetaModelQueryException, XSecurityException {

		codeList = new HashMap();
		MetaObject metaObject = metaService.getClassForName(className);
		StandardQuery query = BLManager.instance().getExtent(metaObject);
		Vector result = query.getResult(trans,false /* includingSubclasses */, true /* useAccessControl */, context);
		BOObject myObj;
		Integer oid;
		String code;
		for (int counter = 0; counter < result.size(); counter++) {
			myObj = (BOObject)result.elementAt(counter);
			oid = new Integer((int) myObj.getOID());
			code = myObj.getAttribute("code", context);
			codeList.put(code, oid);
		}
	}

	public Integer getOID(String code) {
		Integer oid = codeList.get(code);
		return oid;

	}

}
