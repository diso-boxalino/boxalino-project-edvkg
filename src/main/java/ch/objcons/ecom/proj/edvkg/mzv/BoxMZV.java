package ch.objcons.ecom.proj.edvkg.mzv;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.db.DBUtils;
import ch.objcons.db.dbimport.LogFileWriter;
import ch.objcons.ecom.api.ECursorContext;
import ch.objcons.ecom.api.EListID;
import ch.objcons.ecom.api.EValueID;
import ch.objcons.ecom.api.IAction;
import ch.objcons.ecom.api.IBoxContainer;
import ch.objcons.ecom.api.IBoxUpgrader;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.XBoxValueManagementException;
import ch.objcons.ecom.api.adapters.EBoxWithMetaDataAdapter;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bl.BOObjectChangedEvent;
import ch.objcons.ecom.bl.IBOObjectChangeListener;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.box.util.AttributeInformation;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.box.util.SelectExprIterator;
import ch.objcons.ecom.box.util.VectorResult;
import ch.objcons.ecom.box.view.BoxView;
import ch.objcons.ecom.datatypes.EDataTypeValidation;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.engine.EEngine;
import ch.objcons.ecom.expr.ExprParser;
import ch.objcons.ecom.expr.IExpression;
import ch.objcons.ecom.expr.XExpression;
import ch.objcons.ecom.meta.MetaAttribute;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.MetaService;
import ch.objcons.ecom.meta.MetaServiceManager;
import ch.objcons.ecom.meta.MetaServiceProperty;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.imp.*;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchulJahrBeginn;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.SchulDatenImporter;
import ch.objcons.ecom.proj.edvkg.systeminfo.SystemInfo;
import ch.objcons.ecom.proj.edvkg.systeminfo.SystemInfo.ReadOnlyMode;
import ch.objcons.ecom.query.Predicate;
import ch.objcons.ecom.query.StandardQuery;
import ch.objcons.ecom.query.StringQuery;
import ch.objcons.ecom.query.XMaxRowException;
import ch.objcons.ecom.system.JobScheduler;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.ecom.utils.EParseUtilities;
import ch.objcons.log.Log;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;
import com.boxalino.server.Manager;

/*
 * ATTENTION !! THIS IS THE BOX FOR -- MZV -- !! ATTENTNION (and not SABA)

 * generic editing of objects
 *
 * to do:
 * - synchronization of parallel request in same session
 * - versioning of objects for optimistic locking
 * - handle lists of simple attributes
 */
public class BoxMZV extends EBoxWithMetaDataAdapter implements IBOObjectChangeListener {

	
	private static final ILogger LOGGER = Logger.getLogger(BoxMZV.class);
	public static boolean TestMode = false;
	/**
	 * LOGGER f�r das Stimmrecht
	 */
	private static final ILogger LOGGER_SR = Logger.getLogger("Stimmrecht");
	private static final ILogger LOGGER_PostImportForSubset = Logger.getLogger("PostImportForSubset");
	static LogFileWriter LogFileWriter2Logger = new LogFileWriter(LOGGER_PostImportForSubset);

	public static final int DEFAULT_AGE_REF = 18;
	public static final int DEFAULT_AGE_RK = 18;
	
	private static final String PROP_SRA_REF_CURRENT = "Stimmrecht Alter REF";
	private static final String PROP_SRA_REF_HISTORY = "Stimmrecht Alter REF History";
	
	private static final String PROP_SRA_RK_CURRENT = "Stimmrecht Alter RK";
	private static final String PROP_SRA_RK_HISTORY = "Stimmrecht Alter RK History";
	
	private static final String PROP_ANF_EXPORT_FILE = "Anforderungen Export File";
	private static final String PROP_ANF_EXPORT_FILE_DEFAULT = "mzv2oiz";
	private static final String PROP_TEST_MODUS = "test mode";

	public static final int KONF_RK = 1;
	public static final int KONF_EVREF = 2;
	
	private static final String[] KONF = {"","Katholisch","Reformiert"};
	private static final List<String> ExterneAttrList = Arrays.asList("OID", "Rolle");
	
	// hack because of problems with query performance!!!
	private static boolean _useSubstringInQuery = false;

	public static boolean _useImportWindow = true;
	public static int _importWindowHourStart = 22;
	public static int _importWindowHourEnd = 4;
	public static String _anforderungsExportFile;

	private int _stimmrechtsAlterRK = DEFAULT_AGE_RK;
	private int _stimmrechtsAlterRef = DEFAULT_AGE_REF;
	private Long[] _aartenOIDsRef = null;
	private Long[] _aartenOIDsRK = null;
	
	//Stimmrechts Properties
	private int _stimmrechtsAlterRK_Current;
	private int _stimmrechtsAlterRef_Current;
	private String _stimmrechtsAlterRK_History;
	private String _stimmrechtsAlterRef_History;
	
	private boolean _anforderungCheckLaufnummer;
	
	private boolean _isAnforderungSlave;
	
	private String _schulDatenImportFolder;
	
	public BoxMZV (String slotID) {
		super(slotID, "box_search", "AA7");
	}

	public IBoxUpgrader getBoxUpgrader() {
		return new BoxUpgrader(this);
	}

	public static void createZeitungsOrganisation(IRequest request) {
		SimpleDateFormat sdf = new SimpleDateFormat();
		String date = sdf.format(new java.util.Date());
		try {
			BOContext context = (BOContext) request.getContext();

			MetaService ms = MetaServiceManager.getInstance().getServiceForName("edit");
			MetaObject metaKirchgemeinde = ms.getClassForName("Kirchgemeinde");
			MetaObject metaOrganisation = ms.getClassForName("Organisation");
			MetaObject metaOrgPersonen = ms.getClassForName("OrgPersonen");
			MetaObject metaPerson = ms.getClassForName("Person");

			StandardQuery ext = BLManager.instance().getExtent(metaKirchgemeinde);
			Vector v = ext.getResult(null, context);
			Enumeration e = v.elements();
			while (e.hasMoreElements()) {
				BOObject kg = (BOObject) e.nextElement();
				long kgoid = kg.getOID();
				//			if (kgoid != 33) continue;
				// Alle Personen f�r die zei.bx_personenlistzeitungoid die aktuelle KG referenziert
				// die keinen Zeitungsverzicht haben, aktiv sind, aber keiner KG zugeordnet sind
				String querySQL =
					"select pers.* from bx_person as pers, bx_zeitung as zei "
					+ "where zei.bx_personlistzeitungoid = pers.oid and "
					+ " zei.bx_OIDKirchgemeinde="
					+ kgoid
					+ " and "
					+ " (pers.bx_oidkg is null or pers.bx_oidkg != "
					+ kgoid
					+ ")"
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " (pers.oid >= 519000000 and pers.oid < 600000000) and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";

				//			result = BLManager.getFirstString(query);

				BLTransaction trx = BLTransaction.startTransaction(context);
				BOObject newOrg = trx.getObject(metaOrganisation, 0, false);
				newOrg.setAttribute("Name", "Zeitung (autom.)", context, null);
				newOrg.setAttribute("Aktiv", "true", context, null);
				newOrg.setAttribute("createDate", date, context, null);
				newOrg.setReferencedObject("OIDKirchgemeinde", kg);
				StringQuery query =
					(StringQuery) BLManager.instance().createQuery("name", "string", "desc", metaPerson, null);
				query.setSqlStatement(querySQL);
				Vector result = query.getResult(null, context);

				Enumeration ep = result.elements();
				while (ep.hasMoreElements()) {
					BOObject pers = (BOObject) ep.nextElement();
					BOObject newOrgPerson = trx.getObject(metaOrgPersonen, 0, false);
					newOrgPerson.setReferencedObject("PersonOIDListOrgPOID", pers);
					newOrg.addListReferencedObject("OIDListOrgP", newOrgPerson);
				}
				trx.commit();
			}
		}
		catch (Exception x) {
			x.printStackTrace();
		}
	}
	public static void deleteExistingZeitungsOrganisation(IRequest request) {
		try {
			MetaService ms = MetaServiceManager.getInstance().getServiceForName("edit");
			MetaObject metaOrganisation = ms.getClassForName("Organisation");

			BOContext context = (BOContext) request.getContext();
			StandardQuery query =
				(StandardQuery) BLManager.instance().createQuery("name", "standard", "desc", metaOrganisation, null);

			MetaAttribute maKey = metaOrganisation.getAllAttributeForName("Name", context);

			Predicate condition = Predicate.createPredicate(query, Predicate.OPERATOR_EQ, maKey, "Zeitung (autom.)");

			query.setWhereStatement(condition, null);

			ECursorContext cc = new ECursorContext(new EListID("edvkg", "zeitorg"));
			BLTransaction trx = BLTransaction.startTransaction(context);
			Vector result =
				VectorResult.getQueryResult(
						request,
						true,
						false,
						query,
						cc,
						"edit",
						ms,
						AttributeInformation.FILTER_SLOT,
						trx);
			if (result.size() == 0) return;
			Enumeration e = result.elements();
			while (e.hasMoreElements()) {
				BOObject bo = (BOObject) e.nextElement();
				bo.setDeleted(true, context);
			}
			trx.commit();
		}
		catch (Exception x) {
			x.printStackTrace();
		}
	}
	private void doActivation (IRequest request) {
	}

	public Hashtable getParameterList(String functionString) {
		Hashtable ht = new Hashtable();
		StringTokenizer st = new StringTokenizer(functionString,",");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			int sep = token.indexOf("=");
			ht.put(token.substring(0,sep),token.substring(sep+1));

		}
		return ht;
	}

	public int getMDListSize (IRequest request, ECursorContext cursorContext) {
		String listID = cursorContext.getListID().getListID();
		if (listID.equals("familyInfo")) {
			/**
			 * macht ein listrows �ber die Familien-OID, welche der Person geh�rt die im Filter steht
			 * und ermittelt Vorstand, Ehepartner und der Postadressat
			 * innerhalb der Familie
			 */
			UtilsEDVKG ue = UtilsEDVKG.getInstance(UtilsEDVKG.Family, request, this);
			String famMemberOIDStr = cursorContext.getFilter();
			long famMemberOID = -1;
			if (famMemberOIDStr != null && famMemberOIDStr.length() > 0) 
				famMemberOID = Long.parseLong(famMemberOIDStr);
			return ue.setupFamilyInfo(famMemberOID);
		} else if (listID.equals("familienExterne")) {
			/**
			 * macht ein listrows �ber alle externen Beziehungen, welche nicht in der Familie vorkommen
			 */
			UtilsEDVKG ue = UtilsEDVKG.getInstance(UtilsEDVKG.Externe, request, this);
			String famMemberOIDStr = cursorContext.getFilter();
			long famMemberOID = -1;
			if (famMemberOIDStr != null && famMemberOIDStr.length() > 0) 
				famMemberOID = Long.parseLong(famMemberOIDStr);
			return ue.setupFamilienExterneInfo(famMemberOID);
		} else if (listID.equals("meldungsCodes")) {
			return Meldungscode.values().length;
		} else {
			return 0;
		}

	}

	public String getMDListValue(IRequest request,
			ECursorContext cursorContext, int rowIndex, EValueID id)
	throws XMetaModelQueryException, XBoxValueManagementException,
	XSecurityException {

		String listID = cursorContext.getListID().getListID();

		if (listID.equals("familyInfo")) {
			BOContext context = (BOContext)request.getContext();
			String attr = id.getAttributeName();
			Vector famList = (Vector)request.getValue(getSlotID() + "_familyList");
			if (famList == null) throw new XBoxValueManagementException(id.toString() + ": Liste nicht definiert");
			return ((BOObject)famList.elementAt(rowIndex)).getAttribute(attr, cursorContext.getUseAccessControl(), context);
		}
		else if (listID.equals("familienExterne")) {
			BOContext context = (BOContext)request.getContext();
			String attr = id.getAttributeName();
			List<Object[]> list = (List<Object[]>)request.getValue(getSlotID() + "_familienExtList");
			if (list == null) throw new XBoxValueManagementException(id.toString() + ": Liste nicht definiert");
			Object[] item = list.get(rowIndex);
			int ix = ExterneAttrList.indexOf(attr);
			return item[ix].toString();
		} else if (listID.equals("meldungsCodes")) {
			String res = null;
			Meldungscode mc = Meldungscode.values()[rowIndex];
			if (id.getAttributeName().equals("name")) {
				res = mc.toString();
			} else if (id.getAttributeName().equals("ID")) {
				res = mc.getIdentifier();
			}
			return res;
		}
		return null;
	}


	// special handling of address display
	public String getMDValue(IRequest request, EValueID id) throws XMetaModelQueryException, XBoxValueManagementException, XSecurityException {
		String attrName = id.getAttributeName();
		String attrNameSuche = EParseUtilities.getSubstring(attrName, 1);
		String attr = EParseUtilities.getSubstring(attrName, 2);

		BOContext boContext = (BOContext)request.getContext();

		if (attrName.startsWith("Person")) {
			/* This is the function most used. So it should come at the beginning */ 
			return UtilsEDVKG.getEDVKGValue(request, attrName);
		}
		else if (attrName.startsWith("StatZeitung")) {
			return getZeitungsStatistik(attrName, attrNameSuche);
		}
		else if (attrName.startsWith("familyInfo")) {
			/**
			 * Resultate der Liste familyInfo abfragen 
			 */
			//long oid = Long.parseLong(attrNameSuche);
			UtilsEDVKG ue = UtilsEDVKG.getInstance(UtilsEDVKG.Family, request, this);
			return ue.getResult(0, UtilsEDVKG.Family, attrNameSuche);
		}
		else if (attrName.startsWith("anrede")) {
			long oid = Long.parseLong(attr);
			UtilsEDVKG ue = UtilsEDVKG.getInstance(UtilsEDVKG.Anrede, request, this);
			return ue.getResult(oid, UtilsEDVKG.Anrede, attrNameSuche);
		}
		else if (attrName.startsWith("sortparam_deforder")) {
			String actKG = EEngine.getInstance().getValue(request, "login", new EValueID("OID"));
			String subvar = attrName.substring(18);
			boolean dir = false;
			if (subvar.endsWith("DIR")) { subvar = subvar.substring(0, subvar.length() - 3); dir = true; }
			String res = EEngine.getInstance().getValue(request, "global", new EValueID("Consts_defsortorder" + subvar + "kg" + actKG));
			if (EDataTypeValidation.isNullString(res)) return null;
			if (dir) return res.substring(res.lastIndexOf('_') + 1);
			else return res.substring(0, res.lastIndexOf('_'));
		}
		else if (attrName.equals("sortorder_mitactive") || attrName.equals("sortorder_mitpassive")) {
			try {
				String actKG = EEngine.getInstance().getValue(request, "login", new EValueID("OID"));
				String passiv = "";
				String temp = "";
				if (attrName.equals("sortorder_mitpassive")) passiv = "passiv";
				for (int i = 1; i <= 3; i++) {
					String o = EEngine.getInstance().getValue(request, "global", new EValueID("Consts_defsortorder" + passiv + i + "kg" + actKG));
					if (!EDataTypeValidation.isNullString(o)) {
						if (temp.length() > 0) temp = temp + ",";
						temp += o;
					}
				}
				return temp;
			}
			catch (Exception x) {
				return "";
			}
		}
		/* Erstellt die SQL-Sortierstrings f�r die Suchen von aktiven bzw.
		 * passiven Personen aufgrund der gespeicherten Sortiereinstellungen.
		 */
		else if (attrName.startsWith("sortorder_metaactive") || attrName.startsWith("sortorder_metapassive")) {
			String argumentStr = EParseUtilities.getSubstring(attrName, 2);
			int argument = 0;
			String[] customFunctions = new String[3];
			if (!EDataTypeValidation.isNullOrEmptyString(argumentStr)) {
				try {
					argument = Integer.parseInt(argumentStr);
				} catch (NumberFormatException e) {
					throw new XBoxValueManagementException("Falsches Argument am Ende: " + attrName);
				}
				if (argument > 0 && argument <= 2) {
					customFunctions[0] = "case when ?1 is not null then ?2 else ?3 end";
					customFunctions[1] = "case when ?1 = 'm' or ?1 = 'e' then ?2 else ?3 end";
					return customFunctions[argument - 1];
				} else {
					throw new XBoxValueManagementException("Falsches Argument am Ende: " + attrName);
				}
			}
			String orderDef = EEngine.getInstance().getValue(request, "client", new EValueID("session_order"));
			if (orderDef == null) return "";
			String briefAnStr = EEngine.getInstance().getValue(request, "client", new EValueID("session_BriefAn"));
			boolean briefAnZeitung = "zeitung".equalsIgnoreCase(briefAnStr);
			String classString = "Person_";
			if (attrName.startsWith("sortorder_metapassive")) classString = "PersonHistory_";
			String orderToken;
			String temp = "";
			StringTokenizer st1 = new StringTokenizer(orderDef, ",", true);
			while (st1.hasMoreTokens()) {
				String o = st1.nextToken().trim();
				String sortDir = "";
				String coalesce = "coalesce";
				String zustellFun1 = "$ZustellFunc1";
				String zustellFun2 = "$ZustellFunc2";
				if (o.endsWith("_desc")) {
					sortDir = "_desc";
					o = o.substring(0, o.length() - 5);
				} else if (o.endsWith("_asc")) {
					o = o.substring(0, o.length() - 4);
				}
				coalesce += sortDir;
				zustellFun1 += sortDir;
				zustellFun2 += sortDir;
				orderToken = null;
				if (o.equals(",")) {
					orderToken = ",";
				} else if (o.equalsIgnoreCase("Name")) {
					if (briefAnZeitung)	orderToken = coalesce + "(ZeitungNachname,ZusName,Name)";
					else orderToken = coalesce + "(ZusName,Name)";
				} else if (o.equalsIgnoreCase("Vorname")) {
					if (briefAnZeitung) orderToken = coalesce + "(ZeitungVorname, ZusVorname,Vorname)";
					else orderToken = coalesce + "(ZusVorname,Vorname)";
				} else if (o.equalsIgnoreCase("Strasse_Nr")) {
					orderToken = zustellFun2 + "(ZusArt,ZusStrassenName,StrasseKG), " + zustellFun1 + "(ZusArt,ZusSortHausNr,SortHausNr), " + zustellFun1 + "(ZusArt,ZusHausNr,HausNr)";
				} else if (o.equalsIgnoreCase("Strasse")) {
					orderToken = zustellFun2 + "(ZusArt,ZusStrassenName,StrasseKG)";
				} else if (o.equalsIgnoreCase("HausNr")) {
					orderToken = zustellFun1 + "(ZusArt,ZusSortHausNr,SortHausNr), " + zustellFun1 + "(ZusArt,ZusHausNr,HausNr)";
				} else if (o.equalsIgnoreCase("PLZPostfach_PLZ")) {
					orderToken = zustellFun1 + "(ZusArt,coalesce(ZusPLZPostfach,ZusPLZOrt),coalesce(PLZPostfach,PLZOrt))";
				} else if (o.equalsIgnoreCase("PLZ")) {
					orderToken = zustellFun1 + "(ZusArt,ZusPLZOrt,PLZOrt)";
				} else if (o.equalsIgnoreCase("Ort")) {
					orderToken = zustellFun1 + "(ZusArt,ZusOrt,Ort)"; // the ZusOrt must not be null anyway if a zustell is set but person history may cause problems 
				} else if (o.equalsIgnoreCase("Postfach")) {
					orderToken = zustellFun1 + "(ZusArt,ZusPostfach,Postfach)";
				} else if (o.equalsIgnoreCase("PLZPostfach")) {
					orderToken = zustellFun1 + "(ZusArt,ZusPLZPostfach,PLZPostfach)";
				} else if (o.equalsIgnoreCase("Bezirk")) {
					orderToken = "Bezirk" + sortDir;
				} else if (o.equalsIgnoreCase("Konfession")) {
					orderToken = "OIDKonfession_Name" + sortDir;
				} else if (o.equalsIgnoreCase("Geburtstag")) {
					orderToken = "Geburtsdatum" + sortDir;
				} else if (o.equalsIgnoreCase("Zivilstand")) {
					orderToken = "OIDCodeZivilstand_Kurzname" + sortDir;
				} else if (o.equalsIgnoreCase("Zivilstandsdatum")) {
					orderToken = "ZivilstandsDatum" + sortDir;
				} else if (o.equalsIgnoreCase("Aufenthaltsart")) {
					orderToken = "OIDCodeAufenthalt_Name" + sortDir;
				} else if (o.equalsIgnoreCase("ZuzugsdatumKG")) {
					orderToken = "ZuzKGDatum" + sortDir;
				} else if (o.equalsIgnoreCase("Pfarrkreis")) {
					orderToken = "OIDPfarrkreis_Name" + sortDir;
				} else if (o.equalsIgnoreCase("ZIP")) {
					orderToken = "OID" + sortDir;
				} else if (o.equalsIgnoreCase("ZIPPassiv")) {
					orderToken = "OIDPerson" + sortDir + ",OID" + sortDir;
				} else {
					throw new XBoxValueManagementException("Unbekannte Sortierordnungsbezeichnung: " + o);
				}
				temp += orderToken;
			}
			temp = "AdresseStadt," + temp;
			StringTokenizer st2 = new StringTokenizer(temp, ",()", true);
			String res = "";
			while (st2.hasMoreTokens()) {
				String token = st2.nextToken().trim();
				if (token.startsWith("coalesce") || 
						token.startsWith("$ZustellFunc") ||
						",()".indexOf(token) != -1) {
					res += token;
					continue;
				}
				res += classString + token;
			}
			return res;
		} else if (attrName.startsWith("qhTransform")){
			int qhId = 0;
			String sqhId = EParseUtilities.getSubstring(attrName, 1);
			if(!sqhId.equals("")){
				qhId = Integer.parseInt(sqhId);
			}
			if(qhId == 0){
				return "6";
			}else if(qhId == 1){
				return "7";
			}else if(qhId == 2){
				return "8";
			}else if(qhId == 4){
				return "9";
			}
			return sqhId;
		} else if (attrName.startsWith("meldung_code(")) {
			List<String> params = new ArrayList<String>(1);
			EParseUtilities.parseMethod(attrName, params);
			for (Meldungscode m : Meldungscode.values()) {
				if (m.getIdentifier().equals(params.get(0))) {
					return m.toString();
				} else if (m.name().equals(params.get(0))) {
					return m.getIdentifier();
				}

			}
			return null;
		} else if (attrName.startsWith("meldung_type(")) {
			List<String> params = new ArrayList<String>(1);
			EParseUtilities.parseMethod(attrName, params);
			for (Meldungstyp t : Meldungstyp.values()) {
				if (t.getTypeIdentifier().equals(params.get(0))) {
					return t.toString();
				}
				if (t.name().equals(params.get(0))) {
					return t.getTypeIdentifier();
				}
			}
			return null;
		} else if (attrName.startsWith("ohneAnrechtWeil_code(")) {
			List<String> params = new ArrayList<String>(1);
			EParseUtilities.parseMethod(attrName, params);
			try {
				int oawID = Integer.parseInt(params.get(0));
				return OhneAnrechtWeil.getGemaessID(oawID).getCode();
			} catch (Exception e) {
				LOGGER.error("exception occurred", e);
			}
			return "";
		} else if ("systemInfo_readonly".equals(attrName)) {
			return SystemInfo.getReadOnlyMode().toString();
		} else if ("systemInfo_disableImport".equals(attrName)) {
			return "" + SystemInfo.getImportsDisabled();
		} else if ("systemInfo_disableUserLogin".equals(attrName)) {
			return "" + SystemInfo.getUserLoginDisabled();
		} else if ("rpgAktSchulJahr".equals(attrName)) {
			BOSchulJahrBeginn boSchuljahr = BOSchulJahrBeginn.getAktSchulJahr(request);
			return boSchuljahr != null ? boSchuljahr.getJahr() : null;
		} else {
			BoxView viewBox = (BoxView) EEngine.getInstance().getBox("view");
			return viewBox.getMDValue(request, new EValueID(attrName));
		}
	}

	public Vector getMDValueVector(IRequest request, EValueID id) throws XMetaModelQueryException, XBoxValueManagementException {
		return null;
	}
	private static int getOID (String attrName) {
		// get OID out of className:
		// syntax: className[OID]
		String oidString = "";
		int oid = 0;
		int index = attrName.indexOf('[');
		if (index != -1) {
			oidString = attrName.substring(index+1, attrName.length()-1);
			index = oidString.indexOf(']');
			if (index != -1) {
				oidString = oidString.substring(0, index);
			} else {
				oidString="";
			}
		}

		if (EDataTypeValidation.isNullString(oidString)) return -1;

		try {
			oid = Integer.parseInt(oidString);
		}
		catch (NumberFormatException x) {
			return -1;
		}

		return oid;
	}

	Hashtable getValues(IRequest request) {
		Hashtable values = (Hashtable) request.getValue(getSlotID() + "_values");
		if (values == null) {
			values = new Hashtable();
			request.setValue(getSlotID() + "_values", values);
		}
		return values;
	}

	private String getZeitungsStatistik(String attrName, String attrNameSuche)
	throws XMetaModelQueryException {
		Statement stmt = null;
		String result = null;
		String query = null;
		int oid = BoxMZV.getOID(attrName);
		//	if ((oid != 29) && (oid != 2) && (oid != 1)) return "?";
		//	if (oid != 29) return "?";
		if (oid > 0) {
			if ("Summe".equals(attrNameSuche)) {
				// we summarize over the Konfession
				query =
					"select sum(bx_anzahl) from bx_zeitung as zei, bx_kirchgemeinde as kg, bx_person as pers "
					+ "where zei.bx_OIDKirchgemeinde = kg.oid and kg.bx_OIDkonfession="
					+ oid
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";
			}
			else if ("SummeKosten".equals(attrNameSuche)) {
				// we summarize over the Konfession
				query =
					"select sum(zei.bx_anzahl) from bx_zeitung as zei, bx_kirchgemeinde as kg, bx_person as pers "
					+ "where bx_Betrag is not null and bx_Betrag != 0 and zei.bx_OIDKirchgemeinde = kg.oid "
					+ "and kg.bx_OIDkonfession="
					+ oid
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";
				;
			}
			else if ("SummeGratis".equals(attrNameSuche)) {
				// we summarize over the Konfession
				query =
					"select sum(zei.bx_anzahl) from bx_zeitung as zei, bx_kirchgemeinde as kg, bx_person as pers "
					+ "where (bx_Betrag is null or bx_Betrag = 0) and zei.bx_OIDKirchgemeinde = kg.oid "
					+ "and kg.bx_OIDkonfession="
					+ oid
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";
				;
			}
			else if ("SummeAusl".equals(attrNameSuche)) {
				// we summarize over the Konfession
				/* query = "select sum(bx_anzahl) from bx_zeitung as zei, bx_kirchgemeinde as kg, bx_person as pers "+
				  "where zei.bx_personlistzeitungoid = pers.oid and pers.bx_plzort is not null and "+
				  "pers.bx_zusplzort is not null and zei.bx_OIDKirchgemeinde = kg.oid and kg.bx_OIDkonfession="+oid; */
				/*query = "select sum(bx_anzahl) from bx_zeitung as zei, bx_kirchgemeinde as kg, bx_person as pers "+
			      "where zei.bx_personlistzeitungoid = pers.oid and "+
			      "( pers.bx_zusplzort not like ('80%') or ( pers.bx_zusplzort is null and pers.bx_plzort is null ) or "+
			      " ( pers.bx_zusplzort is null and pers.bx_zusplzort not like ('80%') ) ) and "+
			      "zei.bx_OIDKirchgemeinde = kg.oid and kg.bx_OIDkonfession="+oid;*/
				query =
					"select sum(bx_anzahl) from bx_zeitung as zei, bx_person as pers, bx_kirchgemeinde as kg "
					+ "where zei.bx_personlistzeitungoid = pers.oid and "
					+ " zei.bx_OIDKirchgemeinde = kg.oid and kg.bx_OIDkonfession="
					+ oid
					+ " and "
					+ " (pers.bx_oidkg is null or pers.bx_oidkg != kg.oid)"
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";
				/* mja
			query =
				"select sum(bx_anzahl) from bx_zeitung as zei, bx_person as pers, bx_kirchgemeinde as kg "
					+ "where zei.bx_personlistzeitungoid = pers.oid and "
					+ "( pers.bx_zusplzort not like ('80%')  or "
					+ "( pers.bx_zusplzort is null and pers.bx_plzort is null ) or "
					+ "( pers.bx_zusplzort is null and pers.bx_plzort not like ('80%')) "
					+ ") and zei.bx_OIDKirchgemeinde = kg.oid and kg.bx_OIDkonfession="
					+ oid
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";
				 */
			}
			else if ("SummeInl".equals(attrNameSuche)) {
				query =
					"select sum(bx_anzahl) from bx_zeitung as zei, bx_person as pers, bx_kirchgemeinde as kg "
					+ "where zei.bx_personlistzeitungoid = pers.oid and "
					+ " zei.bx_OIDKirchgemeinde = kg.oid and kg.bx_OIDkonfession="
					+ oid
					+ " and "
					+ " pers.bx_oidkg = kg.oid"
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";
			}
			else if ("SummeVerzicht".equals(attrNameSuche)) {
				query =
					"select sum(bx_anzahl) from bx_zeitung as zei, bx_person as pers, bx_kirchgemeinde as kg "
					+ "where zei.bx_personlistzeitungoid = pers.oid and "
					+ " zei.bx_OIDKirchgemeinde = kg.oid and kg.bx_OIDkonfession="
					+ oid
					+ " and "
					+ " pers.bx_oidkg = kg.oid"
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " pers.bx_zeitungverzicht = 'j' and pers.bx_statusperson = 'j'";
			}
			else if ("Anzahl".equals(attrNameSuche)) {
				// we just count the Kirchgemeinde
				query =
					"select sum(zei.bx_anzahl) from bx_zeitung as zei, bx_person as pers where bx_OIDKirchgemeinde="
					+ oid
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";
			}
			else if ("AnzahlKosten".equals(attrNameSuche)) {
				query =
					"select sum(zei.bx_anzahl) from bx_zeitung as zei, bx_person as pers "
					+ "where bx_Betrag is not null and bx_Betrag != 0 and bx_OIDKirchgemeinde="
					+ oid
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";
			}
			else if ("AnzahlGratis".equals(attrNameSuche)) {
				query =
					"select sum(zei.bx_anzahl) from bx_zeitung as zei, bx_person as pers "
					+ "where (bx_Betrag is null or bx_Betrag = 0) and bx_OIDKirchgemeinde="
					+ oid
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";
			}
			else if ("AnzahlAusl".equals(attrNameSuche)) {
				/* query = "select sum(bx_anzahl) from bx_zeitung as zei, bx_person as pers "+
			      "where zei.bx_personlistzeitungoid = pers.oid and pers.bx_plzort not likeis not null and "+
			      "pers.bx_zusplzort is not null and bx_OIDKirchgemeinde="+oid; */
				/* query = "select sum(bx_anzahl) from bx_zeitung as zei, bx_person as pers "+
			      "where zei.bx_personlistzeitungoid = pers.oid and "+
			      "( pers.bx_zusplzort not like ('80%') or ( pers.bx_zusplzort is null and pers.bx_plzort is null ) or "+
				  " ( pers.bx_zusplzort is null and pers.bx_zusplzort not like ('80%') ) ) and "+
			      "pers.bx_zusplzort is not null and bx_OIDKirchgemeinde="+oid; */
				/* mja
			query =
				"select sum(zei.bx_anzahl) from bx_zeitung as zei, bx_person as pers "
					+ "where zei.bx_personlistzeitungoid = pers.oid and "
					+ "( pers.bx_zusplzort not like ('80%')  or "
					+ "( pers.bx_zusplzort is null and pers.bx_plzort is null ) or "
					+ "( pers.bx_zusplzort is null and pers.bx_plzort not like ('80%')) "
					+ ") and zei.bx_OIDKirchgemeinde="
					+ oid
					+ " and "
					+ "  zei.bx_PersonListZeitungOID = pers.oid and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";
				 */
				query =
					"select sum(zei.bx_anzahl) from bx_zeitung as zei, bx_person as pers "
					+ "where zei.bx_personlistzeitungoid = pers.oid and "
					+ " zei.bx_OIDKirchgemeinde=" + oid
					+ " and "
					+ " (pers.bx_oidkg is null or pers.bx_oidkg != " + oid + ")"
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";
			}
			else if ("AnzahlInl".equals(attrNameSuche)) {
				query =
					"select sum(zei.bx_anzahl) from bx_zeitung as zei, bx_person as pers "
					+ "where zei.bx_personlistzeitungoid = pers.oid and "
					+ " zei.bx_OIDKirchgemeinde=" + oid
					+ " and "
					+ " pers.bx_oidkg = " + oid
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " ( pers.bx_zeitungverzicht is null or pers.bx_zeitungverzicht = 'n') and pers.bx_statusperson = 'j'";
			}
			else if ("AnzahlVerzicht".equals(attrNameSuche)) {
				query =
					"select sum(zei.bx_anzahl) from bx_zeitung as zei, bx_person as pers "
					+ "where zei.bx_personlistzeitungoid = pers.oid and "
					+ " zei.bx_OIDKirchgemeinde=" + oid
					+ " and "
					+ " pers.bx_oidkg = " + oid
					+ " and "
					+ " zei.bx_PersonListZeitungOID = pers.oid and "
					+ " pers.bx_zeitungverzicht = 'j' and pers.bx_statusperson = 'j'";
			}
			// System.out.println(query);
			result = BLManager.getFirstString(query);
			return result;
		}
		return null;
	}
	public IAction handleCommand(IRequest request, String id, String path, int xValue, int yValue) {
		BLTransaction trans = BLTransaction.startTransaction((BOContext) request.getContext());
		if (id.equals("cmd_commit")) {
			return new ActionCommit(this, request, this.getSlotID(), getMetaService(), path, trans, true);
		}
		else if (id.equals("cmd_commitDatenbereinigung")) {
			return new ActionCommitDatenbereinigung(this, request, this.getSlotID(), getMetaService(), path, trans, true);
		}
		else if (id.equals("cmd_setValues")) {
			return new ActionCommit(this, request, this.getSlotID(), getMetaService(), path, trans, false);
		}
		else if (id.equals("cmd_zeitungKath")) {
			// In Zeitungsadmin referenziert (Admin-Bereich)
			return new ActionExport(this, request, this.getSlotID(), path, true /*kath*/);
		}
		else if (id.equals("cmd_zeitungRechnung")) {
			// In beiden Zeitungsadmin referenziert
			return new ActionZeitungsAbrechnung(this, request, this.getSlotID(), path, true /*kath*/);
		}
		else if (id.equals("cmd_zeitungRef")) {
			// In Zeitungsadmin referenziert (Admin-Bereich)
			return new ActionExport(this, request, this.getSlotID(), path, false /*ref*/);
		}
		else if (id.equals("cmd_zeitungKathCSV")) {
			// In beiden Zeitungsadmin referenziert
			return new ActionExportCSV(this, request, this.getSlotID(), path, true /*kath*/);
		}
		else if (id.equals("cmd_zeitungRefCSV")) {
			// In beiden Zeitungsadmin referenziert
			return new ActionExportCSV(this, request, this.getSlotID(), path, false /*ref*/);
		}
		else if (id.equals("cmd_inspectScheduler")) {
			JobScheduler js = JobScheduler.getInstance();
			return null;
		} else if(id.equals("cmd_reloadStimmrechtXML") || id.equals("cmd_reloadStimmrechtProperties")) {
			reloadStimmrechtProperties();
		} else if (id.equals("cmd_deletePerson")) {
			return new ActionDeletePerson(this, request, getSlotID(), path);
		}
		return null;
	}
	public IAction handleParameter (IRequest request, String id, String[] value) throws XBoxValueManagementException {
		if (id.endsWith("_OID")) {
			// add object identifiers as loop through value
			request.addLoopThroughValue(getSlotID() + "_" + id, value[0]);

			// test if the path to the OID is correct
			try {
				AttributeInformation attrInfo = new AttributeInformation(id, getSlotID(), this.getMetaService(), AttributeInformation.FILTER_ALL, request, null);
			}
			catch (XMetaModelQueryException e) {
				this.addPublisherError(request, "incorrect path to OID: " + e.getMessage());
			}
			catch (XSecurityException e) {
				this.addPublisherError(request, "incorrect path to OID: " + e.getMessage());
			}
		}
		else if (id.equals("cmd_createZeitungsOrganisation")) {
			deleteExistingZeitungsOrganisation(request);
			createZeitungsOrganisation(request);
		}
		else if (id.equals("cmd_doActivation")) {
			boolean suppressRefCheck = request.getContext().isReferenceCheckSuppressed();
			try {
				request.getContext().setReferenceCheckSuppressed(true);
				doActivation(request);
			}
			finally {
				request.getContext().setReferenceCheckSuppressed(suppressRefCheck);
			}
		}
		else if (id.equals("cmd_setDefaultSortOrder") || id.equals("cmd_setDefaultSortOrderPassiv")) {
			String actKG = EEngine.getInstance().getValue(request, "login", new EValueID("OID"));
			String passiv = "";
			if (id.endsWith("Passiv")) passiv = "passiv";
			System.out.print("set default-sort-order " + passiv + " to ");
			try {
				BOContext boContext = (BOContext)request.getContext();
				BLTransaction trans = BLTransaction.startTransaction(boContext);
				MetaObject constsClass = MetaObjectLookup.getInstance().getMetaObject("Consts");
				for (int i = 1; i <= 3; i++) {
					String o = request.getParameters().getParameter("edvkg_sortparam_deforder" + passiv + i);
					String d = request.getParameters().getParameter("edvkg_sortparam_deforder" + passiv + i + "DIR");
					if (!(d != null && ("asc".equalsIgnoreCase(d)) || "desc".equalsIgnoreCase(d))) d = "asc"; 
					if (i > 1) System.out.print(",");
					System.out.print(o);
					if (o != null && o.length() == 0) o = null; else o = o + "_" + d;
					Vector constsObjects = executeQuery (constsClass, "Consts_name == 'defsortorder" + passiv + i + "kg" + actKG + "'", trans, boContext);
					if ((constsObjects == null) || (constsObjects.size() == 0)) {
						BOObject co = trans.getObject(constsClass, 0, false);
						co.setAttribute("name", "defsortorder" + passiv + i + "kg" + actKG, boContext, null);
						co.setAttribute("value", o, boContext, null);
					}
					else {
						BOObject co = (BOObject)constsObjects.elementAt(0);
						co.setAttribute("name", "defsortorder" + passiv + i + "kg" + actKG, boContext, null);
						co.setAttribute("value", o, boContext, null);
					}
				}
				System.out.println();
				trans.commit();		
			}
			catch (Exception x) {
				x.printStackTrace();
			}		
		}
		else if (id.equals("cmd_fixR")) {
			Log.logSystemError("cmd_fixR darf nicht mehr aufgerufen werden seit dem neuen Konsistenz-Check. Ausf�hrung abgebrochen.");
			//string "cmd_fixR" can't be found in the whole project
			//fixRedundancies();
		}
		else if (id.equals("cmd_javaCC")) {
			this.directPostImport(request, value[0], true);
		} else if (id.equals("cmd_schulDatenImport")) {
			SchulDatenImporter.doImport();
		} else if (id.equals("cmd_anfExport")) {
			try {
				Anforderungen.createExportFile(_anforderungsExportFile, (BOContext)request.getContext(), null, new LogFileWriter(LOGGER));
			} catch (Exception e) {
				LOGGER.error("Fehler beim Aufruf des OIZ-Exports", e);
			}
		} else if (id.startsWith("sortparam")) {

		} else if (id.equals("systemInfo_readonly")) {
			SystemInfo.processReadOnlyMode(ReadOnlyMode.valueOf(value[0]));
		} else if ("systemInfo_disableImport".equals(id)) {
			SystemInfo.setImportsDisabled(Boolean.valueOf(value[0]));
		} else if ("systemInfo_disableUserLogin".equals(id)) {
			SystemInfo.setUserLoginDisabled(Boolean.valueOf(value[0]));
		} else if ("context_suppressReferenceCheck".equals(id)) {
			BOContext context = (BOContext) request.getContext();
			context.setReferenceCheckSuppressed(Boolean.parseBoolean(value[0]));
		} else {
			// put all input values to the buffer
			getValues(request).put(id, value);
		}

		return null;
	}
	
	public static Vector executeQuery (MetaObject aMetaobject, String aFilter, BLTransaction aTransaction, BOContext aBoContext) throws XMetaModelNotFoundException, XExpression, XMetaException, XDataTypeException, XMetaModelQueryException, XSecurityException, XMaxRowException {
		StandardQuery standardQuery = BLManager.instance().getExtent(aMetaobject);
		if (aFilter == null || aFilter.equals("")) {
			return standardQuery.getResult(aTransaction, aBoContext);
		}
		else {
			IExpression expressions = ExprParser.getExpression(aFilter, false);
			SelectExprIterator expressionCollector = new SelectExprIterator(standardQuery, false, aBoContext);
			expressions.iterate(expressionCollector); 
			return standardQuery.getResult(aTransaction, true, true, null, null /* limit-row-count */, null, expressionCollector.getWhereStatement(), "ASC", null, aBoContext);
		}
	}


	public void init (IRequest request, IBoxContainer theContainer) throws Exception {
		super.init(request, theContainer);
		initProperties();

		// set commands
		theContainer.addCommand(this, "cmd_commit", false);
		theContainer.addCommand(this, "cmd_commitDatenbereinigung", false);
		theContainer.addCommand(this, "cmd_setValues", false);
		theContainer.addCommand(this, "cmd_zeitungRechnung", false);
		theContainer.addCommand(this, "cmd_zeitungKath", false);
		theContainer.addCommand(this, "cmd_zeitungRef", false);
		theContainer.addCommand(this, "cmd_zeitungKathCSV", false);
		theContainer.addCommand(this, "cmd_zeitungRefCSV", false);
		theContainer.addCommand(this, "cmd_wordlistperson", false);
		theContainer.addCommand(this, "cmd_fixRedu", false);
		theContainer.addCommand(this, "cmd_inspectScheduler", false);
		theContainer.addCommand(this, "cmd_reloadStimmrechtXML", false);
		theContainer.addCommand(this, "cmd_reloadStimmrechtProperties", false);
		theContainer.addCommand(this, "cmd_deletePerson", false);
	}

	public void initPhase2(IRequest request) throws Exception {
		super.initPhase2(request);
		ch.objcons.log.Log.logDesignerInfo("initPhase2 started");
		LOGGER.info("Box EDVKG is starting");

		try {
			EDVKGQueryHook[] hooks = new EDVKGQueryHook[10];
			hooks[0] = EDVKGQueryHook.getInstance(EDVKGQueryHook.QH_BRIEF_AN_FAMILIE);
			hooks[1] = EDVKGQueryHook.getInstance(EDVKGQueryHook.QH_BRIEF_AN_PERSON);
			hooks[2] = EDVKGQueryHook.getInstance(EDVKGQueryHook.QH_BRIEF_AN_ELTERN);
			hooks[4] = EDVKGQueryHook.getInstance(EDVKGQueryHook.QH_BRIEF_AN_ELTERN_NO_DUPLS);
			hooks[3] = EDVKGQueryHook.getInstance(EDVKGQueryHook.QH_BRIEF_AN_ZEITUNGSADRESSE);
			hooks[5] = EDVKGQueryHook.getInstance(EDVKGQueryHook.QH_ZEITUNGSEMPFAENGER);
			//hooks f�r org
			hooks[6] = EDVKGQueryHook.getInstance(EDVKGQueryHook.QH_BRIEF_AN_FAMILIE_ORG);
			hooks[7] = EDVKGQueryHook.getInstance(EDVKGQueryHook.QH_BRIEF_AN_PERSON_ORG);
			hooks[8] = EDVKGQueryHook.getInstance(EDVKGQueryHook.QH_BRIEF_AN_ELTERN_ORG);
			hooks[9] = EDVKGQueryHook.getInstance(EDVKGQueryHook.QH_BRIEF_AN_ELTERN_NO_DUPLS_ORG);

			//hooks[1] = EDVKGQueryHook.getInstance(EDVKGQueryHook.QH_ZEITUNGSEMPFAENGER);
			ECursorContext.QueryHooks.getInstance().setQueryHooks(hooks);
		}
		catch (Exception e) {
			// if any parsing or other things failes we just mack a error output
			ch.objcons.log.Log.logDesignerError("Exception initphase2", e);
		}
		MetaObject moPersonHistory = MetaObjectLookup.getInstance().getMetaObject("Person");
		BLManager.getInstance().addBOObjectChangeListener(moPersonHistory, this);
		BLManager.getInstance().addBOObjectChangeListener(MetaObjectLookup.getInstance().getMetaObject("LoginUser"), new LoginUserChangeListener());
		SystemInfo.setUserLoginDisabled(SystemInfo.getUserLoginDisabled());
	}
	
	protected void initProperties () throws Exception {
		//  get the default values
		_useSubstringInQuery = new Boolean(getProperty("use substring in query" , "true")).booleanValue();
		_useImportWindow = getProperty_boolean("use import time window" , "true");
		_importWindowHourStart = getProperty_int("import time window start hour", 22);
		_importWindowHourEnd = getProperty_int("import time window end hour", 4);
		
		_stimmrechtsAlterRK_Current = getProperty_int(PROP_SRA_RK_CURRENT, DEFAULT_AGE_RK);
		_stimmrechtsAlterRK_History  = getProperty(PROP_SRA_RK_HISTORY, ""+DEFAULT_AGE_RK);
		
		_stimmrechtsAlterRef_Current = getProperty_int(PROP_SRA_REF_CURRENT, DEFAULT_AGE_REF);
		_stimmrechtsAlterRef_History  = getProperty(PROP_SRA_REF_HISTORY, ""+DEFAULT_AGE_REF);
		
		String exportDir = ".";
		_anforderungsExportFile = getProperty(PROP_ANF_EXPORT_FILE, exportDir + File.separator + PROP_ANF_EXPORT_FILE_DEFAULT);
		
		boolean testmode = Boolean.parseBoolean(getProperty(PROP_TEST_MODUS, "false"));
		TestMode = testmode;
		int anforderungenExportStartHour = getProperty_int("anforderungen export start hour", 20);
		{
			Date anforderungenExportStart = nextDateForHour(anforderungenExportStartHour);
			JobScheduler.getInstance().executeAtAndRepeat(AnforderungExporter.JOB_KEY, new AnforderungExporter(), anforderungenExportStart, JobScheduler.DAILY);
		}
		
		_anforderungCheckLaufnummer = getProperty_boolean("anforderungen check laufnummer", "true");
		
		_schulDatenImportFolder = getProperty("schulDatenImportFolder");
		_isAnforderungSlave = getProperty_boolean("is anforderungen slave", "false");
	}
	
	private static Date nextDateForHour(int hour) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, hour);
		for (int field : new int[] { Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND }) {
			cal.set(field, 0);
		}
		if (cal.compareTo(Calendar.getInstance()) < 0) {
			cal.add(Calendar.DAY_OF_YEAR, 1);
		}
		return cal.getTime();
	}
	
	public boolean getAnforderungCheckLaufnummer() {
		return _anforderungCheckLaufnummer;
	}
	
	public boolean isAnforderungSlave() {
		return _isAnforderungSlave;
	}
	
	public static boolean useSubstringInQuery() {
		return _useSubstringInQuery;
	}

	/**
	 * Is called if the consistency check should be started explicitely
	 * @param request
	 */
	public void directPostImport (IRequest request, String value, boolean async) {
		BOContext boContext = (BOContext)request.getContext();
		Connection dbConn = null;
		try {
			//boContext.setGUIClient(true);
			dbConn = DBConnectionPool.instance().getConnection();

			IRequest newRequest = BoxalinoUtilities.getDummyRequest(boContext.getLocale(), request.getSession());
			BOContext newContext = ((BOContext)newRequest.getContext());
			newContext.setGUIClient(true);
			int runCode = 0;
			try {
				runCode = Integer.parseInt(value);
			} catch (NumberFormatException e) {
			}
			PostImport cc = new PostImport(dbConn, null, null, newContext, runCode);
			if (async) {
				JobScheduler sched = ch.objcons.ecom.system.JobScheduler.getInstance();
				Date now = new Date();
				Calendar cal = new GregorianCalendar();
				cal.setTime(now);
				cal.add(Calendar.SECOND, 3);
				now = cal.getTime();
				//Util util = new Util(dbConn, logWriter);
				sched.executeAt("Post Import MZV", cc, now);
			} else {
				cc.run();
			}
		}
		catch (SQLException e) {
			ch.objcons.log.Log.logSystemAlarm("SQLException", e);
		}
	}

	public void notifyObjectsChanged(BOObjectChangedEvent event) {
		Set<BOObject> changedObjects = event.getChangedObjects();
		Connection conn = null;
		PostImport check = null;
		try {

			for (BOObject bo : changedObjects) {
				BOPerson boPerson = (BOPerson) bo;
				
				/* Automatische Zeitungen werden nur an Stadtpersonen verteilt,
				 * deswegen ist die Pr�fung einer h�ndischen Person nicht n�tig */
				if ((boPerson.hasZeitungsVerzichtChanged() || boPerson.hasAdressZusatzIndexChanged())
						&& boPerson._updatedInMask 
						&& boPerson.getOID() < 500000000) {
					if (check == null) {
						conn = DBConnectionPool.instance().getConnection();
						check = new PostImport(conn, LogFileWriter2Logger, LogFileWriter2Logger, event.getTransaction().getContext());
					}
					//FIXME why synchronize here instead of within PostImport?
					synchronized(PostImport.class) {
						check.callPostImportForSubset((int)bo.getOID());
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error(null, e);
			if (conn != null) {
				try {
					conn.rollback();
				} catch (SQLException e1) {
					LOGGER.fatal("SQLException occured!", e1);
				}
			}
		} finally {
			if (conn != null) {
				DBConnectionPool.instance().ungetConnection(conn);
			}
		}
		
		
	}
	
	
	public void reloadStimmrechtProperties() {
		IRequest request = BoxalinoUtilities.getDummyRequest();
		BOContext context = (BOContext) request.getContext();
		BLTransaction trx  = BLTransaction.startTransaction(context);

			_aartenOIDsRK = getAufenthaltsartenForKonfession(trx, request, KONF_RK);
			_aartenOIDsRef = getAufenthaltsartenForKonfession(trx, request, KONF_EVREF);
			

			if (_aartenOIDsRef.length == 0) {
				LOGGER_SR.error("Reformiert: es sind keine Aufenthaltsarten definiert!");
			} else {
				Map<String,String> config = mapSRConfig(trx, request, KONF_EVREF);
				String rawAlter = config.get("alter");
				try {
					if (!EDataTypeValidation.isNullOrEmptyString(rawAlter)) {
						_stimmrechtsAlterRef = Integer.parseInt(rawAlter);
						
						String konfession = "EV-REF";
						int previousAlter = _stimmrechtsAlterRef_Current;
						_stimmrechtsAlterRef_Current = _stimmrechtsAlterRef;
						boolean alterIsModified = _stimmrechtsAlterRef != previousAlter; 
						if (alterIsModified) {
							LOGGER_SR.info("Reformiert: Alter wird von \"" + previousAlter + "\" auf \"" + _stimmrechtsAlterRef + "\" ge�ndert.");
						}
						
						try {
							if (alterIsModified) {
							 	MetaService ms = MetaServiceManager.getInstance().getServiceForName("edvkg");
				                MetaServiceProperty propCurrent = ms.getPropertyForName(PROP_SRA_REF_CURRENT);
				                MetaServiceProperty propHistory = ms.getPropertyForName(PROP_SRA_REF_HISTORY);
				                
			                    BLTransaction trans = BLTransaction.startTransaction(context);
			                    propCurrent.setValue(String.valueOf(_stimmrechtsAlterRef));
			                    String newProp = null;
			                    if (_stimmrechtsAlterRef_History.isEmpty()) {
			                    	newProp = String.valueOf(_stimmrechtsAlterRef);
			                    } else {
			                    	newProp = _stimmrechtsAlterRef_History + " " + _stimmrechtsAlterRef;
			                    }
			                    if (newProp.length() > 255) {
			                    	newProp = newProp.substring(newProp.length()-255, newProp.length());
			                    	newProp.trim();
			                    }
		                        propHistory.setValue(newProp);
		                        trans.putObject(propCurrent);
		                        trans.putObject(propHistory);
		                        trans.commit();
							}
							
	                        executeStimmrechtsQuery(previousAlter, _stimmrechtsAlterRef, konfession, KONF_EVREF, _aartenOIDsRef);
							if (alterIsModified) {
								LOGGER_SR.info("Reformiert: Alter wurde erfolgreich von \"" + previousAlter + "\" auf \"" + _stimmrechtsAlterRef + "\" ge�ndert.");
							}
						} catch (XMetaException e) {
							LOGGER_SR.error("Reformiert: XMetaException occured!", e);
						} catch (XSecurityException e) {
							LOGGER_SR.error("Reformiert: XSecurityException occured!", e);
						} catch (XBOConcurrencyConflict e) {
							LOGGER_SR.error("Reformiert: XBOConcurrencyConflict occured!", e);
						} catch (SQLException e) {
							LOGGER_SR.fatal("Reformiert: Update fehlgeschlagen!", e);
						}
					}
				} catch (NumberFormatException e) {
					LOGGER_SR.error("Reformiert: Ung�ltiges Alter \"" + rawAlter + "\" verwende vorheriges Alter (=" + _stimmrechtsAlterRef_Current + ")");
					_stimmrechtsAlterRef = _stimmrechtsAlterRef_Current;
				}
				
			}
			
			if (_aartenOIDsRK.length == 0) {
				LOGGER_SR.error("Katholisch: es sind keine Aufenthaltsarten definiert!");
			} else {
				Map<String,String> config = mapSRConfig(trx, request, KONF_RK);
				String rawAlter = config.get("alter");
				try {
					if (!EDataTypeValidation.isNullOrEmptyString(rawAlter)) {
						_stimmrechtsAlterRK = Integer.parseInt(rawAlter);
						String konfession = "RK";
						int previousAlter = _stimmrechtsAlterRK_Current;
						_stimmrechtsAlterRK_Current = _stimmrechtsAlterRK;
						boolean alterIsModified = _stimmrechtsAlterRK_Current != previousAlter;
						if (alterIsModified) {
							LOGGER_SR.info("Katholisch: Alter wird von \"" + previousAlter + "\" auf \"" + _stimmrechtsAlterRK + "\" ge�ndert.");
						}
						try {
							if (alterIsModified) {
							 	MetaService ms = MetaServiceManager.getInstance().getServiceForName("edvkg");
				                MetaServiceProperty propCurrent = ms.getPropertyForName(PROP_SRA_RK_CURRENT);
				                MetaServiceProperty propHistory = ms.getPropertyForName(PROP_SRA_RK_HISTORY);
				                
			                    BLTransaction trans = BLTransaction.startTransaction(context);
			                    propCurrent.setValue(String.valueOf(_stimmrechtsAlterRK));		                    
			                    String newProp = null;
			                    if (_stimmrechtsAlterRK_History.isEmpty()) {
			                    	newProp = String.valueOf(_stimmrechtsAlterRK);
			                    } else {
			                    	newProp = _stimmrechtsAlterRK_History + " " + _stimmrechtsAlterRK;
			                    }
			                    if (newProp.length() > 255) {
			                    	newProp = newProp.substring(newProp.length()-255, newProp.length());
			                    	newProp.trim();
			                    }
		                        propHistory.setValue(newProp);
		                        trans.putObject(propCurrent);
		                        trans.putObject(propHistory);
		                        trans.commit();
							}
	                        executeStimmrechtsQuery(previousAlter, _stimmrechtsAlterRK, konfession, KONF_RK, _aartenOIDsRK);
	                        if (alterIsModified) {
	                        	LOGGER_SR.info("Katholisch: Alter wurde erfolgreich von \"" + previousAlter + "\" auf \"" + _stimmrechtsAlterRK + "\" ge�ndert.");
	                        }
						} catch (XMetaException e) {
							LOGGER_SR.error("Katholisch: XMetaException occured!", e);
						} catch (XSecurityException e) {
							LOGGER_SR.error("Katholisch: XSecurityException occured!", e);
						} catch (XBOConcurrencyConflict e) {
							LOGGER_SR.error("Katholisch: XBOConcurrencyConflict occured!", e);
						} catch (SQLException e) {
							LOGGER_SR.fatal("Katholisch: Update fehlgeschlagen!", e);
						}

					}
				} catch (NumberFormatException e) {
					LOGGER_SR.error("Katholisch: Ung�ltiges Alter \"" + rawAlter + "\" verwende vorheriges Alter (=" + _stimmrechtsAlterRK_Current + ")");
					_stimmrechtsAlterRK = _stimmrechtsAlterRK_Current;
				}
			}
			
			try {
				executeStimmrechtsClearingQuery();
			} catch (SQLException e) {
				LOGGER_SR.error("Ein Fehler ist aufgetreten beim Ausf�hren des clearing-queries!", e);
			} catch (Exception e) {
				LOGGER_SR.fatal("Ein schwerwiegender Fehler ist aufgetreten beim Ausf�hren des clearing-queries!", e);
			}
	}
	
	private Map<String, String> mapSRConfig(BLTransaction trx, IRequest request, int konfessionOID) {
		Map<String, String> config = new Hashtable<String,String>();
		String filter = BOStimmrechtConfiguration.CLASS_NAME + "_konfession  == " + konfessionOID;
		try {
			List<BOStimmrechtConfiguration> rs = BoxalinoUtilities.executeQuery(BOStimmrechtConfiguration.CLASS_NAME, filter, "", trx, request);
			for (BOStimmrechtConfiguration bo : rs) {
				String type = bo.getAttribute("propertyType", trx.getContext());
				String value = bo.getAttribute("propertyValue", trx.getContext());
				config.put(type, value);
			}
		} catch (Exception e) {
			LOGGER_SR.error(KONF[konfessionOID] + ": Fehler beim laden der Aufenthaltsarten!", e);
		}
		return config;
	}

	private Long[] getAufenthaltsartenForKonfession(BLTransaction trx, IRequest request, int konfessionOID) {
		List<Long> aartOIDs = new ArrayList<Long>();
		String filter = BOStimmrechtAufenthaltsarten.CLASS_NAME + "_konfession  == " + konfessionOID;
		try {
			List<BOStimmrechtAufenthaltsarten> rs = BoxalinoUtilities.executeQuery(BOStimmrechtAufenthaltsarten.CLASS_NAME, filter, "", trx, request);
			for (BOStimmrechtAufenthaltsarten bo : rs) {
				String code = bo.getAttribute("aufenthaltsart", trx.getContext());
				BOObject boCodeAArt = BoxalinoUtilities.getObjectByKey("CodeAufenthalt", "code", code, trx);
				if (boCodeAArt == null) {
					LOGGER_SR.error("Konnte Aufenthaltsart mit Code \"" + code + "\" nicht finden!");
				} else {
					aartOIDs.add(boCodeAArt.getOID());
				}
			}
		} catch (Exception e) {
			LOGGER_SR.error(KONF[konfessionOID] + ": Fehler beim laden der Aufenthaltsarten!", e);
		}
		return aartOIDs.toArray(new Long[aartOIDs.size()]);
	}

	public static List<BOAuthorityAddress> getAuthorityAddresses(BOContext context) {

		BLTransaction trx  = BLTransaction.startTransaction(context);
		try {
			return BoxalinoUtilities.executeQuery(BOAuthorityAddress.CLASS_NAME, "", trx, context);
		} catch (Exception e) {
			LOGGER_SR.error("Fehler beim laden der Amtsadressen!", e);
		}
		return null;
	}

	private void executeStimmrechtsQuery(int previousAlter, int newAlter, String konfessionLabel, int konfession, Long[] codes) throws SQLException {
		StringBuilder sb = new StringBuilder();
		for (Long code : codes) {
			if (sb.length() > 0) {
				sb.append("," + code);
			} else {
				sb.append(code);
			}
		}
		
		// Folgende Query Updated nur Personen innerhalb der Zeitspanne (previousAlter - newAlter)
		// was konkret heisst, eine Anpassung an den Aufenthaltsarten im XML wird NUR f�r diese Zeitspanne ber�cksichtigt.
		// (>> potentiell inkonsistente �ltere Personen, es muss ein manuelles Query ausgef�hrt werden)
		// NOTE: Personen, die j�nger als newAlter sind werden nat�rlich bei entsprechendem Alter durch den CC ber�cksichtigt.
		/*
		String query = "update bx_person p set p.bx_stimmrecht = case when (" +
					   " p.bx_oidcodeaufenthalt in (" + sb.toString() + ")" +
					   " ) then 'j' else 'n' end" +
					   " where p.bx_oidkonfession = '" + konfession + "'"
					   + " and p.bx_geburtsdatum <= DATE_SUB(CURDATE(), INTERVAL " + newAlter + " YEAR)"
					   + " and p.bx_geburtsdatum >= DATE_SUB(CURDATE(), INTERVAL " + previousAlter + " YEAR)";
		*/
		
		// Folgende Query Updated ALLE Personen einer bestimmten Konfession
		// d.h. man kann mit Hilfe folgender query auch das Alter von Personen hochsetzten und Codes �ndern
		/**/
		String query = "update bx_person p set p.bx_stimmrecht = case when (" +
			   " p.bx_geburtsdatum <= DATE_SUB(CURDATE(), INTERVAL " + newAlter + " YEAR) and" +
			   " p.bx_oidcodeaufenthalt in (" + sb.toString() + ")" +
			   " ) then 'j' else 'n' end" +
			   " where p.bx_oidkonfession = '" + konfession + "'";
		/**/
		
		Connection conn = null;
		try {
			conn = DBConnectionPool.instance().getConnection();
			Statement stmt = conn.createStatement();
			int updateCount = stmt.executeUpdate(query);
			LOGGER_SR.info("Update f�r " + konfessionLabel + " wurde ausgef�hrt, " + updateCount + " Personen betroffen. \n" + query + "");
		} finally {
			if (conn != null) {
				DBConnectionPool.instance().ungetConnection(conn);
			}
		}
	}
	
	/**
	 * @throws SQLException
	 * Diese Funktion setzt das stimmrecht f�r Personen, die nicht reformiert oder katholisch sind auf 'n'
	 */
	private void executeStimmrechtsClearingQuery() throws SQLException {
		String konfessionList = KONF_RK + "," + KONF_EVREF;
		String query = "update bx_person p set p.bx_stimmrecht = 'n'" +
					" where  (( !(p.bx_oidkonfession in (" + konfessionList + ")) or p.bx_oidkonfession is null ) && coalesce(p.bx_stimmrecht,'j') != 'n') or OID > 500000000";
		Connection conn = null;
		try {
			conn = DBConnectionPool.instance().getConnection();
			Statement stmt = conn.createStatement();
			int updateCount = stmt.executeUpdate(query);
			LOGGER_SR.info("Update f�r nicht-reformierte und nicht-katholische wurde ausgef�hrt, " + updateCount + " Personen wurden angepasst. \n" + query + "");
		} finally {
			if (conn != null) {
				DBConnectionPool.instance().ungetConnection(conn);
			}
		}
	}
	
	public static void deletePerson(IRequest request, long OID) {
		BOContext context = (BOContext)request.getContext();
		BLTransaction trx = BLTransaction.startTransaction(context);
		Connection conn = null;
		Statement stmt = null;
		try {
			trx.begin();
			conn = trx.getConnection();
			stmt = conn.createStatement();
			
			DBUtils.executeUpdate(stmt, "delete from bx_person where oid = " + OID);
			DBUtils.executeUpdate(stmt, "delete from bx_zeitung where bx_personlistzeitungoid = " + OID);
			DBUtils.executeUpdate(stmt, "delete from bx_personhistory where BX_OIDPERSON = " + OID);
			DBUtils.executeUpdate(stmt, "delete from bx_orgpersonen where BX_PERSONOIDLISTORGPOID = " + OID);
			
			stmt.close();
			stmt = null;
			trx.commit();
			BLManager.getInstance().emptyBuffersWithReferrers(MetaObjectLookup.getInstance().getMetaObject("Person"));
		} catch (Exception e) {
			try {
				trx.rollback();
				LOGGER.error("Exception occured while trying to delete Person with oid \"" + OID + "\"!", e);
			} catch (XMetaException xme) {
				LOGGER.error("Rollback failed!", xme);
			}
		} finally {
			if (stmt != null) {
				DBUtils.closeStatement(stmt);
			}
		}
	}

	public static BoxMZV getInstance() {
		return Manager.getService(BoxMZV.class);
	}

	public String getSchulDatenImportFolder() {
		return _schulDatenImportFolder;
	}
	
}
