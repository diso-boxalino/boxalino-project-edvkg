package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.Consts;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;
import com.boxalino.log.Stacktrace;

public class BOMeldung extends BOObject {
	private static final char ArgSeparator = '|';
	static ILogger LOGGER = Logger.getLogger("boxalino.edvkg");
	public static final String CLASS_NAME = "Meldung";
	
	public BOMeldung(BLTransaction trans, long oid) {
		super(trans, oid);
	}

	@Override
	public String getAttribute(int index, boolean arg1, BOContext context) throws XMetaModelQueryException, XSecurityException {
		if (index == Consts.Meldung_mld) {
			return getMld(context);
		} else {
			return super.getAttribute(index, arg1, context);
		}
	}
	
	public String getMld(BOContext context) {
		try {
			BOObject boMldVorlage = getReferencedObject(Consts.Meldung_MeldungsvorlagemeldungenOID, context);
			if (boMldVorlage == null) {
				LOGGER.error("Meldungsvorlage zu Meldung " + getOID() + " nicht gefunden", new Stacktrace());
				return null;
			} else {
				String text = boMldVorlage.getAttribute(Consts.Meldungsvorlage_text, context);
				String args = getAttribute(Consts.Meldung_arguments, context);
				return makeMeldung(text, args);
			}
		} catch (XMetaModelQueryException e) {
			throw new RuntimeException(e);
		} catch (XSecurityException e) {
			throw new RuntimeException(e);
		}
	}

	public static String makeMeldung(String meldungsgeruest, String arguments) {
		String[] args = null;
		if (arguments != null) {
			StringTokenizer st = new StringTokenizer(arguments, String.valueOf(ArgSeparator));
			args = new String[st.countTokens()];
			int i = 0;
			while (st.hasMoreTokens()) {
				args[i] = st.nextToken();
				if (args[i] == null) args[i] = "";
			}
		}
		return String.format(meldungsgeruest, (Object[]) args);
	}
	
	public static BOMeldung createMeldung(BLTransaction trans, Meldungsvorlage mv, Object... arguments) throws XMetaException, XDataTypeException, XSecurityException {
		String argString = getArgString(arguments);
		BOMeldung boMeldung = (BOMeldung) trans.getObject(Consts.MeldungMeta, 0);
		boMeldung.setAttribute(Consts.Meldung_MeldungsvorlagemeldungenOID, String.valueOf(mv.getOID()), trans.getContext());
		boMeldung.setAttribute(Consts.Meldung_arguments, argString, trans.getContext());
		return boMeldung;
	}
	
	
	
	static final class Meldung {
		
		Meldungsvorlage _mv;
		
		Object[] _arguments;
		
		public Meldung(Meldungsvorlage mv, Object[] args) {
			_mv = mv;
			_arguments = args;
		}
		
		String getArgString() {
			return BOMeldung.getArgString(_arguments);
		}
		
		public boolean istAnforderungNoetig() {
			return _mv.istAnforderungNoetig();
		}
		
		
		public Long getZIPForAnforderung() {
			int ix = ixZIPForAnforderung();
			return ix != -1 ? (Long)_arguments[ix] : null;
		}
		
		private int ixZIPForAnforderung() {
			for (int i = 0; i < _arguments.length; i++) {
				Object arg = _arguments[i];
				if (arg instanceof Long) return i;
			}
			return -1;
		}
		
		public Object[] argsWithOID(Long origin, long oid) {
			return argsWithOID(origin, oid, new Object[0]);
		}
		
		public Object[] argsWithOID(Long origin, long oid, Object... newArgs) {
			int ix = ixZIPForAnforderung();
			List<Object> args = new ArrayList<Object>(Arrays.asList(_arguments));
			if (ix != -1) {
				args.set(ix, oid);
			} else {
				args.add(oid);
			}
			args.addAll(Arrays.asList(newArgs));
			return args.toArray();
		}

		public Meldungsvorlage getVorlage() {
			return _mv;
		}
		
	}
	
	public static String getArgString(Object[] arguments) {
		if (arguments == null || arguments.length == 0) {
			return null;
		}
		StringBuilder sb = new StringBuilder(100);
		for (Object arg : arguments) {
			sb.append(arg);
			sb.append(ArgSeparator);
		}
		sb.setLength(sb.length() - 1);
		return sb.toString();
	}
	
}
