package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Vector;

import ch.objcons.db.dbimport.LogFileWriter;
import ch.objcons.db.dbimport.MappingReader;
import ch.objcons.db.dbimport.XMappingException;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.box.dbimport.BOImportPlus;
import ch.objcons.ecom.engine.ERequestImpl;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.BoxMZV;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.SchulDatenImporter;
import ch.objcons.ecom.proj.edvkg.systeminfo.SystemInfo;
import ch.objcons.ecom.system.JobScheduler;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.log.Log;
import ch.objcons.util.properties.ShopProperties;

import com.boxalino.fileaccess.FileAccessManager;
import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

/*
 * handles the database import task
 * supports different states of an import task:
 * 'inactive', 'active', 'started', 'aborted', 'done'
 */
public class BOImportEDVKG extends BOImportPlus {
 
    static ILogger LOGGER = Logger.getLogger("ch.objcons.ecom.proj.edvkg.import");
    
	private static final String _jobPrefix = "ImportEDVKG_";


    public BOImportEDVKG(ch.objcons.ecom.bl.BLTransaction trans, long oid) {
        super(trans, oid);
    }

    @Override
	protected String getJobPrefix() {
        return _jobPrefix;
    }


    @Override
	protected void setImportParams (BOContext context) throws XMetaException, XSecurityException {
		super.setImportParams(context);
		String fileName;

		IRequest request = context.getRequest();

		// set it the postImport shoud make a consistence Check
		Boolean consistCheck = (Boolean) this.getValue("consistenceCheck", context);
		if (consistCheck == null) consistCheck = false;
		request.setValue("import_consistenceCheck", consistCheck);

		Boolean isErstanlieferung = (Boolean) this.getValue("IsErstanlieferung", context);
		if (isErstanlieferung == null) isErstanlieferung = false;
		request.setValue("import_isErstanlieferung", isErstanlieferung);

		String titel = this.getAttribute("title", context);
		request.setValue("import_title", titel);

		fileName = (String) request.getValue("import_importFileName");

		if (fileName.contains("zmpkbk")) {
			// that is our special guest: it triggers the import of the other files
			try {
			    String query = "select max(OID) from bx_importlog";
			    int number = BLManager.getCount (query);

			    DecimalFormat decForm = new DecimalFormat("000000");
			    if (BoxImportEDVKG.mustIncrementLogNumberByOne()) {
			    	fileName = fileName.substring(0, fileName.length()-6)+decForm.format(number+1);
			    } else {
			    	fileName = fileName.substring(0, fileName.length()-6)+decForm.format(number);
			    }
				request.setValue("import_importFileName", fileName);
			} catch (XMetaModelQueryException e) {
				LOGGER.error("Fehler beim Bestimmen des Importfiles kbk", e);
			}
		}
		File mappingFile = FileAccessManager.getInstance().getPathTranslatedFile("/import/" + getAttribute("mappingFile", context));
		request.setValue("import_mappFileName", mappingFile.getAbsolutePath());
        // mja 2005-03-30
        //		context.setDateFormat(new SimpleDateFormat("yyyyMMdd"));
    }

    private String getNextKbkFile(String fileName) throws XMetaModelQueryException {
	    String query = "select max(OID) from bx_importlog";
	    final int curNumber = BLManager.getCount (query);
	    DecimalFormat decForm = new DecimalFormat("000000");
	    final String curNumberFormatted = decForm.format(curNumber);
	    File importFile = new File(fileName);
    	File dir = importFile.getParentFile();
 
    	FilenameFilter kbkFilter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (!dir.isDirectory()) return false;
				int pos = name.indexOf("zmpkbk");
				if (pos >= 0) {
					if (curNumberFormatted.compareTo(name.substring(pos+"zmpkbk".length(),name.length())) < 0){
						return true;
					}
				}
				return false;
			}
		};
	    File[] kbkFiles = dir.listFiles(kbkFilter);
	    Arrays.sort(kbkFiles);
	    if (kbkFiles.length > 0) {
	    	return kbkFiles[0].getAbsolutePath();
	    } else {
	    	return null;
	    }
    }
    
	/**
	 * user for the EDVKG import ot wait for an import file
	 * waits for the import file and returns false if the import file ist not there in time
	 */
	@Override
	protected boolean waitForImportFile(String importFile, BOContext context) {
		Boolean checkFile;

		try {
		    checkFile = (Boolean)(this.getValue("WaitForImportfile", context));

		    if ( !( (checkFile != null) && (checkFile) ) ) {
			    // nothing to do !!
			    return true;
		    }
/*
			if (lastDate.getTime() < System.currentTimeMillis()) {
				_logWriter.writeText("It is to late for the import, stop it");
				return false;
			}
*/
		    /* mja-2005-03-03 */
		    Calendar cal = Calendar.getInstance();
		    int hour = cal.get(Calendar.HOUR_OF_DAY);
/*
		    if ((hour > 4) && (hour < 22)) {
				_logWriter.writeText("import only starts from 22:00 to 4:00");
				return false;
		    }
*/			
			File impFile;
			String importTempFile = null;
			if(BoxImportEDVKG.mustIncrementLogNumberByOne()){
				_logWriter.writeText("start waiting for the File "+importFile);
			}else{
				_logWriter.writeText("start waiting for the File "+importFile+"*");
			}
			boolean found = false;
			while (!found) {
				if (BoxMZV._useImportWindow) {
				    cal = Calendar.getInstance();
				    hour = cal.get(Calendar.HOUR_OF_DAY);
			    	try {
					    if (BoxMZV._importWindowHourStart < BoxMZV._importWindowHourEnd) {
					    	// z.b. 2 bis 4 uhr
						    if ((hour < BoxMZV._importWindowHourStart) || (hour > BoxMZV._importWindowHourEnd)) {
								Thread.sleep(60000);
						    	continue;
						    }
					    }
					    else {
					    	// z.b. 22 bis 4 uhr
						    if ((hour < BoxMZV._importWindowHourStart) && (hour > BoxMZV._importWindowHourEnd)) {
								Thread.sleep(60000);
						    	continue;
						    }
					    }
					} catch (InterruptedException e) {
						return false;
					}
				}
			    if (BoxImportEDVKG.mustIncrementLogNumberByOne()) {
			    	impFile = new File(importFile);
			        found = impFile.exists();
			    } else {
				    importTempFile = getNextKbkFile(importFile);
				    found = importTempFile != null;
				    if(importTempFile != null){
				    	context.getRequest().setValue("import_importFileName", importTempFile);
				    	_logWriter.writeText("start waiting for the File "+importTempFile);
				    }
			    }
		    	try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					return false;
				}
				found = found && !SystemInfo.getImportsDisabled();
		    }
			return found;
		}
		catch (XMetaModelNotFoundException e) {
			Log.logAlarm(LOGGER, "Datenimport: error in waitForFile", e);
			return false;
		}
		catch (XSecurityException e) {
			Log.logAlarm(LOGGER, "Datenimport: error in waitForFile", e);
			return false;
		}
		catch (XMetaModelQueryException e) {
			Log.logAlarm(LOGGER, "Fehler in 'waitForImportFile'", e);
			return false;
		}
	}

	/**
	 * returns if the import can be startet direct
	 */
	@Override
	public boolean canStartDirect() {
		return false;
	}


	@Override
	public void writeToDB(boolean isInsert, Connection con) throws SQLException, XMetaException, XBOConcurrencyConflict  {
		try {
            BOContext boContext = _trans.getContext();
			String state = this.getAttribute("state", boContext);

			super.writeToDB(isInsert, con);

			if (state.equals("done")) {
			    // look fo an object that was depending from us
				BOObject boObject;

				String query = "select * from bx_importedvkg where " +
			        "bx_DependsOn = " + this.getAttribute("OID", boContext);
				Vector result = BLManager.getResult(_trans, query, true /* isAlreadyFiltered */, _trans.getContext());
				if (result.size() > 0) {
			        // there is just one
					boObject = (BOObject)result.elementAt(0);
					if (boObject.getAttribute("state", boContext).equalsIgnoreCase("active")) {
						((BOImportEDVKG)(boObject)).putToJobScheduler(boContext);
					}
				}
			}

		}
		catch (XSecurityException e) {
			Log.logAlarm(LOGGER, "Datenimport: unexpected error write to DB", e);
		}
	}


	@Override
	public void putToJobScheduler(BOContext boContext) throws XMetaException, XSecurityException {
		java.sql.Timestamp time = (java.sql.Timestamp) this.getValue("startDate", boContext);
//		long now = System.currentTimeMillis();
		Calendar cal = Calendar.getInstance();
		long now = cal.getTimeInMillis();
		boolean delay = false;
		if (time != null) {
			if (time.getTime() == now) {
				LOGGER.info("time == now (" + time.getTime() + " == " + now + ")");
			}
			else if (time.getTime() < now) {
				LOGGER.info("time < now (" + time.getTime() + " < " + now + ")");
			}
			else {
				LOGGER.info("time > now (" + time.getTime() + " > " + now + ")");
				delay = true;
			}
		}
		if (BoxMZV.TestMode) {
			delay = false;
		}
		LOGGER.info("H:"+time.getHours()+" M:"+time.getMinutes());
		BOObject dependingObj = this.getReferencedObject("DependsOn", boContext);
		String depState;
		boolean doIt = false;
		this._context = boContext;
		_context.getRequest().endHTTPRequest(false);

		if (dependingObj != null) {
			depState = dependingObj.getAttribute("state", boContext);
			if (depState.equalsIgnoreCase("done")) {
			    doIt = true;
			}
		}
		else {
			doIt = true;
		}

		if (doIt) {
			if (delay) {
				JobScheduler.getInstance().executeAt(getJobPrefix() + getOID(), this, time);
			}
			else {
				cal.add(Calendar.SECOND, 3);
				JobScheduler.getInstance().executeAt(getJobPrefix() + getOID(), this, cal.getTime());
			}
/* mja 2005-03-30
			if ((time == null) || (time.getTime() < now)) {
				JobScheduler.getInstance().execute(getJobPrefix() + getOID(), this);
		    }
		    else {
				JobScheduler.getInstance().executeAt(getJobPrefix() + getOID(), this, time);
		    }
*/
		}
	}


	@Override
	protected void postImport (IRequest request, Connection dbConn, BOContext boContext) {
		boolean autoImport = (Boolean) request.getValue("import_autoImport");
		boolean consistCheck = (Boolean) request.getValue("import_consistenceCheck");

		String importFileName = getImportFileName(request);
		if (autoImport) {
			boolean autoRemove = ((Boolean) request.getValue("import_autoRemove")).booleanValue();
			String errMsg;
			if (autoRemove) {
				File importFile = new File(importFileName);
				if (!importFile.delete()) {
					errMsg = "Die Importdatei '"+importFileName+"' konnte nicht gel�scht werden.";
					if (_logWriter != null) {
						_logWriter.writeWarning(errMsg);
					}
					else {
						Log.logAlarm(LOGGER, "Datenimport: "+errMsg);
					}
				}
			}
		}
		LogFileWriter inkonsistenzenLog = null;
		try {
			// check the consistency of the db
			String logFileName2 = ShopProperties.getInstance().getPath("ecom.log.dir") + "inconsistencies.txt";
			try {
				inkonsistenzenLog = new LogFileWriter(logFileName2);
			} catch (IOException e) {
				Log.logAlarm(LOGGER, "IOException", e);					
				LOGGER.info("Fehler beim Initialisieren der Inkonsistenzen-Logdatei; verwende normalen Import-Log");
				inkonsistenzenLog = getLogWriter();
			}
			
			// try SchulDatenImport if we would execute a PostImport. Since a successful SchulDatenImport  
			// includes a PostImport, we don't need to do another afterwards.
			if (consistCheck && !SchulDatenImporter.doImport()) {
				IRequest newRequest = BoxalinoUtilities.getDummyRequest(boContext.getLocale(), request.getSession());
				BOContext newContext = (BOContext) newRequest.getContext();
				newContext.setGUIClient(true);
				PostImport newCheck = new PostImport(dbConn, getLogWriter(), inkonsistenzenLog, newContext);
				synchronized(PostImport.class) {
					newCheck.startPostImport(null);
				}
			}
		}
		finally {
			if (inkonsistenzenLog != null) inkonsistenzenLog.closeLogFile();
		}
	}

	/**
	 * returns the Filename of the Import File
	 */
	protected String getImportFileName(IRequest iRequest) {
	    return (String) iRequest.getValue("import_importFileName");
	}

	@Override
	protected MappingReader getMappingReader (String mappFileName, Connection dbConn)
		throws IOException, XMappingException, XMetaException
	{
		ERequestImpl iRequest = (ERequestImpl) _context.getRequest();
		return new MappingReaderEDVKG(iRequest, mappFileName, dbConn);
	}
}
