package ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp;

import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.M_ADRESSZUSATZ;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.M_GESCHLECHT;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.M_HAUSNR;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.M_HAUSNRZUS;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.M_NAME;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.M_PLZ;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.M_REL;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.M_STRASSE;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.M_STRNR;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.M_VORNAME;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.S_ADRESSZUSATZ;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.S_GEBDAT;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.S_GESCHLECHT;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.S_HAUSNR;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.S_HAUSNRZUS;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.S_NAME;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.S_PLZ;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.S_REL;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.S_STRASSE;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.S_STRNR;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.S_VORNAME;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.V_ADRESSZUSATZ;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.V_GESCHLECHT;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.V_HAUSNR;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.V_HAUSNRZUS;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.V_NAME;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.V_PLZ;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.V_REL;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.V_STRASSE;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.V_STRNR;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.V_VORNAME;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.ZIP_MUTTER;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.ZIP_NR;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVFormatInfo.ZIP_VATER;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType;

public class SchulDatenMapping {

	private SchulDatenMapping() {}
	
	static final Map<String, String> KONF_MAPPING;
	
	static {
		Map<String, String> konfMapping = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
		konfMapping.put("evangelisch-reformiert", "EV-REF");
		konfMapping.put("r�misch-katholisch", "RK");
		KONF_MAPPING = Collections.unmodifiableMap(konfMapping);
	}
	
	static final Map<ValueType, String> S_MAPPING, M_MAPPING, V_MAPPING;
	
	static {
		S_MAPPING = PersonValues.mappingBuilder()
			.zip(ZIP_NR).name(S_NAME).vorname(S_VORNAME).geschlecht(S_GESCHLECHT).gebdat(S_GEBDAT).strnr(S_STRNR).strasse(S_STRASSE)
			.hausnr(S_HAUSNR).hausnrzus(S_HAUSNRZUS).adresszus(S_ADRESSZUSATZ).plz(S_PLZ).rel(S_REL)
		.build();
		
		M_MAPPING = PersonValues.mappingBuilder()
			.zip(ZIP_MUTTER).name(M_NAME).vorname(M_VORNAME).geschlecht(M_GESCHLECHT).strnr(M_STRNR).strasse(M_STRASSE)
			.hausnr(M_HAUSNR).hausnrzus(M_HAUSNRZUS).adresszus(M_ADRESSZUSATZ).plz(M_PLZ).rel(M_REL)
		.build();
		
		V_MAPPING = PersonValues.mappingBuilder()
			.zip(ZIP_VATER).name(V_NAME).vorname(V_VORNAME).geschlecht(V_GESCHLECHT).strnr(V_STRNR).strasse(V_STRASSE)
			.hausnr(V_HAUSNR).hausnrzus(V_HAUSNRZUS).adresszus(V_ADRESSZUSATZ).plz(V_PLZ).rel(V_REL)
		.build();
	}
}
