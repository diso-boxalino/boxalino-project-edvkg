package ch.objcons.ecom.proj.edvkg.mzv;

import ch.objcons.ecom.api.IAction;
import ch.objcons.ecom.api.IBox;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.adapters.EActionAdapter2;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.system.security.XSecurityException;

public class ActionDeletePerson extends EActionAdapter2 implements IAction {

	long _OID;
	
	public ActionDeletePerson(IBox box, IRequest request, String infoKey,
			String resultPagePath) {
		super(box, request, infoKey, resultPagePath);
	}

	@Override
	public void performX() throws XMetaException, XDataTypeException,
			XSecurityException, XBOConcurrencyConflict {
		BoxMZV.deletePerson(getRequest(), _OID);
	}
	
	@Override
	public boolean validate() {
		_OID = Long.parseLong(getRequest().getParameters().getParameter("edvkg_personOID"));
		return super.validate();
	}

}
