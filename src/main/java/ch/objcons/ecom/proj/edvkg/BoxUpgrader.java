package ch.objcons.ecom.proj.edvkg;

import java.util.Hashtable;

import ch.objcons.ecom.api.IBox;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.adapters.EBoxUpgraderAdapter;
import ch.objcons.ecom.api.adapters.SimpleBoxUpgraderAdapter;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.bom.utils.UpgradeUtil;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

public class BoxUpgrader extends SimpleBoxUpgraderAdapter {
		private static ILogger LOGGER = Logger.getLogger("ch.ikg.upgrader");
		
		BoxUpgrader (IBox box) {
	        super(box);
	    }

		public void initialSetup (IRequest request, IBox box) throws Exception {
		}
		
		@SuppressWarnings("serial")
		public void upgradeToVersion_01_44(IRequest request, IBox box) throws XDataTypeException, XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = getTrx();		
			MetaObject mobjSabaPfarrer = uu.getClassForName(BOSABAPfarrer.CLASS_NAME);			
			uu.epoIntegerAttribute(trx, context, mobjSabaPfarrer, "communicationMethod", "Kommunikationsart", false, false /* mandantory */, false);	
				
			uu.epoData(trx, context, "Labels", "key", "phone", new Hashtable<String, String>(){{
				put("txtShort", "Tel");
				put("txtLong", "Telefon");
				put("keyOrig", "phone");
			}});
			
			uu.epoData(trx, context, "Labels", "key", "fax", new Hashtable<String, String>(){{
				put("txtShort", "Fax");
				put("txtLong", "Fax");
				put("keyOrig", "fax");
			}});
			
			uu.epoData(trx, context, "Labels", "key", "undefined", new Hashtable<String, String>(){{
				put("txtShort", "Undefiniert");
				put("txtLong", "Undefiniert");
				put("keyOrig", "undefined");	
			}});
			
		}
}