package ch.objcons.ecom.proj.edvkg;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.db.SQLFormat;
import ch.objcons.ecom.api.ECursorContext;
import ch.objcons.ecom.api.EValueID;
import ch.objcons.ecom.api.IAction;
import ch.objcons.ecom.api.IBox;
import ch.objcons.ecom.api.IBoxContainer;
import ch.objcons.ecom.api.IBoxUpgrader;
import ch.objcons.ecom.api.IContext;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.XBoxValueManagementException;
import ch.objcons.ecom.api.adapters.EBoxWithMetaDataAdapter;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.box.util.AttributeInformation;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.box.util.SelectExprIterator;
import ch.objcons.ecom.box.view.BoxView;
import ch.objcons.ecom.datatypes.EDataTypeValidation;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.engine.EEngine;
import ch.objcons.ecom.expr.ExprParser;
import ch.objcons.ecom.expr.IExpression;
import ch.objcons.ecom.expr.XExpression;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.LoginUserChangeListener;
import ch.objcons.ecom.proj.edvkg.systeminfo.SystemInfo;
import ch.objcons.ecom.proj.edvkg.systeminfo.SystemInfo.ReadOnlyMode;
import ch.objcons.ecom.proj.edvkg.wcache.BestatterWoche;
import ch.objcons.ecom.proj.edvkg.wcache.CacheException;
import ch.objcons.ecom.proj.edvkg.wcache.CacheSABAWochen;
import ch.objcons.ecom.proj.edvkg.wcache.IBestatter;
import ch.objcons.ecom.proj.edvkg.wcache.IKirchgemeinde;
import ch.objcons.ecom.query.StandardQuery;
import ch.objcons.ecom.query.StringQuery;
import ch.objcons.ecom.query.XMaxRowException;
import ch.objcons.ecom.system.JobScheduler;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.ecom.utils.EParseUtilities;
import ch.objcons.log.Log;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;
import com.boxalino.server.Manager;

/*
 * ATTENTION !! THIS IS THE BOX FOR -- SABA -- !! ATTENTNION (and not MZV)
 * 
 * generic editing of objects
 *
 * to do:
 * - synchronization of parallel request in same session
 * - versioning of objects for optimistic locking
 * - handle lists of simple attributes
 */
public class BoxEdit extends EBoxWithMetaDataAdapter {

	
	private static final ILogger LOGGER = Logger.getLogger(BoxEdit.class);
	static final boolean TestMode = false;
	/**
	 * LOGGER f�r das Stimmrecht
	 */
	// hack because of problems with query performance!!!
	private static boolean _useSubstringInQuery = false;
	private static String _SABAAuslieferung = "";
	static String _SABAAnzahlFreiTage = "";
	static int _SABAAnzahlWochen = 5;

	static boolean _useImportWindow = true;
	static int _importWindowHourStart = 22;
	static int _importWindowHourEnd = 4;

	public BoxEdit (String slotID) {
		super(slotID, "box_search", "AA7");
	}

	public IBoxUpgrader getBoxUpgrader() {
		return new BoxUpgrader(this);
	}

	synchronized public void checkSABACompletion (IRequest request) {
		Connection c = null;
		try {
			c = DBConnectionPool.instance().getConnection();
			Statement s = c.createStatement();

			IContext context = request.getContext();
			Calendar cal = Calendar.getInstance(context.getLocale());
			cal.add(Calendar.WEEK_OF_YEAR,-2);
			int year = cal.get(Calendar.YEAR);
			int week = (cal.get(Calendar.WEEK_OF_YEAR));
			if (week >= 52 && cal.get(Calendar.MONTH) == Calendar.JANUARY) {
				year = year - 1; // week belongs to last year 
				System.out.println("Java: week >= 52 and month = january");
			}
			String sql = "delete from bx_sabawoche where (bx_jahr < " + year + ") or (bx_jahr = " + year + " and bx_woche < " + week + ")";
			s.executeUpdate(sql);
			cal.add(Calendar.WEEK_OF_YEAR,-1);			
			sql ="delete from bx_sabazuordnung where (bx_datum <= " + SQLFormat.dateToSQLvalue(cal.getTime()) + ")";
			s.executeUpdate(sql);
		}
		catch (SQLException e) {
			ch.objcons.log.Log.logSystemError("Error checkSABACompletion (SQL)", e);
		}
		finally {
			DBConnectionPool.instance().ungetConnection(c);
		}
		try {
			GregorianCalendar cal = new GregorianCalendar();

			BOContext context = (BOContext)request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			MetaObject mobjkg = MetaObjectLookup.getInstance().getMetaObject("Kirchgemeinde");
			MetaObject mobjweek = MetaObjectLookup.getInstance().getMetaObject("SABAWoche");
			StandardQuery sabaKGExtent = BLManager.instance().getExtent(mobjkg);
			Vector kgs = sabaKGExtent.getResult(trx, context);

			for (int i=0; i<_SABAAnzahlWochen; i++) {
				int actWeek = cal.get(Calendar.WEEK_OF_YEAR);
				//				int actYear = cal.get(Calendar.YEAR);
				if ((cal.get(Calendar.MONTH) == Calendar.DECEMBER) && (cal.get(Calendar.WEEK_OF_YEAR) == 1)) {
					cal.set(Calendar.DAY_OF_YEAR, cal.getActualMaximum(Calendar.DAY_OF_YEAR));
					cal.add(Calendar.DAY_OF_YEAR, 1);
				}
				int actYear = cal.get(Calendar.YEAR);
				Enumeration kgsEn = kgs.elements();
				while (kgsEn.hasMoreElements()) {
					BOObject kg = (BOObject)kgsEn.nextElement();
					BOSABAWoche week = null;
					if (kg.getOID() > 999) continue;

					StringQuery strqw = new StringQuery(trx, 0);
					strqw.init("SABAWocheQuery", "SABAWocheQuery", mobjweek);
					strqw.setSqlStatement("select * from bx_sabawoche where bx_oidkg=" + kg.getOID() + " and bx_woche = " + actWeek+" and bx_jahr = "+actYear);
					Vector resWeek = strqw.getResult(trx, context);
					if ((resWeek != null) && (resWeek.size() == 1)) {
						week = ((BOSABAWoche)resWeek.elementAt(0));
					} else {
						week = (BOSABAWoche)trx.getObject(mobjweek, 0, false);
						week.setReferencedObject("OIDKG", kg);
						week.setAttribute("Woche", ""+actWeek, context, null);
						week.setAttribute("Jahr", ""+actYear, context, null);
						week.setValue("toCheck", new Boolean(false), context);

						//week.updateObject(context);
					}



					//long weekOID = getSABAWeek("" + actYear, "" + actWeek, "" + kg.getOID(), context);
					if (week == null) {
						continue;
					}
					//BOSABAWoche week = (BOSABAWoche)BLManager.getObject(mobjweek, trx, weekOID, false, context);

					week.setValue("toCheck", new Boolean(true), context);
					week.updateObject(context);
				}
				cal.add(Calendar.DATE, 7);
			}
			trx.commit();
		}
		catch (Exception x) {
			ch.objcons.log.Log.logDesignerError("Exception: checkSABACompletion",x);
			x.printStackTrace();
		}
	}
	public Hashtable getParameterList(String functionString) {
		Hashtable ht = new Hashtable();
		StringTokenizer st = new StringTokenizer(functionString,",");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			int sep = token.indexOf("=");
			ht.put(token.substring(0,sep),token.substring(sep+1));

		}
		return ht;
	}

	public int getMDListSize (IRequest request, ECursorContext cursorContext) {
		String listID = cursorContext.getListID().getListID();
		if (listID.equalsIgnoreCase("verfBestatter")) {

			String filter = cursorContext.getFilter();
			Hashtable ht = getParameterList(filter);
			String kg =(String)ht.get("kg");
			String woche = (String)ht.get("woche");
			String jahr = (String)ht.get("jahr");
			String field = (String)ht.get("tag");
			int tag = getTag(field); int vierteltag= getVierteltag(field);

			if (tag==-1 || vierteltag==-1) return 0;

			Vector result;
			try {
				result = CacheSABAWochen.getInstance((BOContext)request.getContext()).getVerfuegbareBestatter(kg,tag,vierteltag,woche,jahr);
			} catch (CacheException e) {
				Log.logSystemAlarm("ERROR while accessing cache: "+e.getMessage());
				return 0;

			}
			request.setValue("verfBestatter", result);
			return result.size();
		} else if (listID.equalsIgnoreCase("verfBestatterWoche")) {
			String woche = request.getParameters().getParameter(getSlotID()+"_SABAWoche_Woche");
			if (woche==null) {
				addPublisherError(request,"Die Woche muss mit '"+getSlotID()+"_SABAWoche_Woche' spezifiziert werden");
				return 0;
			}
			String jahr = request.getParameters().getParameter(getSlotID()+"_SABAWoche_Jahr");
			if (jahr==null) {
				addPublisherError(request,"Das Jahr muss mit '"+getSlotID()+"_SABAWoche_Jahr' spezifiziert werden");
				return 0;
			}
			String kg = request.getParameters().getParameter(getSlotID()+"_SABAWoche_KG");
			if (kg==null) {
				addPublisherError(request,"Die KG muss mit '"+getSlotID()+"_SABAWoche_KG' spezifiziert werden");
				return 0;
			}
			Vector result = null;
			try {
				result = CacheSABAWochen.getInstance((BOContext)request.getContext()).getVerfuegbareBestatterGanzeWoche(kg,woche,jahr);
			} catch (CacheException e) {
				Log.logSystemAlarm("ERROR while accessing cache: "+e.getMessage());
				return 0;
			}
			if (result!=null) {
				request.setValue("verfBestatterWoche", result);
				return result.size();
			}
			return 0;

		} else {
			return 0;
		}

	}

	public int getTag(String field) {
		int tag = -1;
		if (field.startsWith("OIDMo")) tag = 0;
		if (field.startsWith("OIDDi")) tag = 1;
		if (field.startsWith("OIDMi")) tag = 2;
		if (field.startsWith("OIDDo")) tag = 3;
		if (field.startsWith("OIDFr")) tag = 4;

		if (tag == -1) {
			if (field.startsWith("Mo")) tag = 0;
			if (field.startsWith("Di")) tag = 1;
			if (field.startsWith("Mi")) tag = 2;
			if (field.startsWith("Do")) tag = 3;
			if (field.startsWith("Fr")) tag = 4;
		}
		return tag;
	}

	public int getVierteltag(String field) {
		int vierteltag = -1;
		if (field.endsWith("Vor")) vierteltag = 0;
		if (field.endsWith("Vor2")) vierteltag = 1;
		if (field.endsWith("Nach")) vierteltag = 2;
		if (field.endsWith("Nach2")) vierteltag = 3;
		return vierteltag;
	}

	public String getMDListValue(IRequest request,
			ECursorContext cursorContext, int rowIndex, EValueID id)
	throws XMetaModelQueryException, XBoxValueManagementException,
	XSecurityException {

		String listID = cursorContext.getListID().getListID();

		if (listID.equalsIgnoreCase("verfBestatter")) {
			Vector bestatter = (Vector)request.getValue("verfBestatter");
			if (bestatter == null) return null;
			BestatterWoche bwoche = (BestatterWoche)bestatter.elementAt(rowIndex);
			String attribute = id.getAttributeName();
			if (attribute.equalsIgnoreCase("kurz")) {
				return bwoche.getKurz();
			} else if (attribute.equalsIgnoreCase("OID")) {
				return bwoche.getOID();
			} else if (attribute.equalsIgnoreCase("gesperrt")) {
				return bwoche.getGesperrt();
			} 
			return null;

		} else if (listID.equalsIgnoreCase("verfBestatterWoche")) {
			Vector bestatter = (Vector)request.getValue("verfBestatterWoche");
			if (bestatter == null) return null;
			BestatterWoche bwoche = (BestatterWoche)bestatter.elementAt(rowIndex);
			String attribute = id.getAttributeName();
			if (attribute.equalsIgnoreCase("kurz")) {
				return bwoche.getKurz();
			} else if (attribute.equalsIgnoreCase("OID")) {
				return bwoche.getOID();
			} else if (attribute.equalsIgnoreCase("gesperrt")) {
				return bwoche.getGesperrt();
			} 
			return null;
		}
		return null;
	}


	// special handling of address display
	public String getMDValue(IRequest request, EValueID id) throws XMetaModelQueryException, XBoxValueManagementException, XSecurityException {
		String attrName = id.getAttributeName();
		String attrNameSuche = EParseUtilities.getSubstring(attrName, 1);
		String attr = EParseUtilities.getSubstring(attrName, 2);

		BOContext boContext = (BOContext)request.getContext();

		if (attrName.equals("SABAWeek")) {
			String year = request.getParameters().getParameter("client_request_sabayear");
			String week = request.getParameters().getParameter("client_request_sabaweek");
			String kg = request.getParameters().getParameter("client_request_sabakg");
			return "" + getSABAWeek(year, week, kg, (BOContext)request.getContext());

		} else if (attrName.startsWith("SABAWoche_")) {
			IBox errorBox = EEngine.getInstance().getBox("error");
			String errorCount = errorBox.getValue(request, new EValueID("count"));
			if ((errorCount!=null) && !errorCount.equalsIgnoreCase("0")) {
				return request.getParameters().getParameter(getSlotID()+"_"+attrName);
			}

			String woche = "";
			String jahr = "";
			String kg = "";

			if ((request.getParameters().getParameter(getSlotID()+"_copyWeek")!=null) && request.getParameters().getParameter(getSlotID()+"_copyWeek").equalsIgnoreCase("true")){

				woche = request.getParameters().getParameter(getSlotID()+"_copyWeek_weekFrom");
				jahr = request.getParameters().getParameter(getSlotID()+"_copyWeek_yearFrom");
				kg = request.getParameters().getParameter(getSlotID()+"_copyWeek_kgFrom");
				System.out.println("Copy week: "+woche+"/"+jahr+" in KG "+kg);
			} else {
				woche = request.getParameters().getParameter(getSlotID()+"_SABAWoche_Woche");
				if (woche==null) {
					throw new XBoxValueManagementException("Die Woche muss mit '"+getSlotID()+"_SABAWoche_Woche' spezifiziert werden");
				}
				jahr = request.getParameters().getParameter(getSlotID()+"_SABAWoche_Jahr");
				if (jahr==null) {
					throw new XBoxValueManagementException("Das Jahr muss mit '"+getSlotID()+"_SABAWoche_Jahr' spezifiziert werden");
				}
				kg = request.getParameters().getParameter(getSlotID()+"_SABAWoche_KG");
			}

			String field = EParseUtilities.getSubstring(attrName,1);
			int tag = getTag(field); int vierteltag = getVierteltag(field);
			if (tag==-1 || vierteltag == -1) throw new XBoxValueManagementException("ung�ltiger Vierteltag "+field+" in "+getSlotID()+"_"+attrName);

			if (EParseUtilities.getSubstring(attrName,1).startsWith("OID") && EParseUtilities.getSubstring(attrName,2).equalsIgnoreCase("OIDSabaPers")) {

				if (kg==null) {
					throw new XBoxValueManagementException("Die Kirchgemeinde muss mit '"+getSlotID()+"_SABAWoche_KG' spezifiziert werden");
				}

				try {
					IBestatter bestatter = CacheSABAWochen.getInstance(boContext).getGebuchterBestatter(kg,tag,vierteltag,woche,jahr);
					if (bestatter!=null) return bestatter.getOID(); else return null;

				} catch (CacheException e) {
					Log.logSystemAlarm("ERROR while accessing cache: "+e.getMessage());
					return null;
				}

			} else if (EParseUtilities.getSubstring(attrName,1).startsWith("OID") && EParseUtilities.getSubstring(attrName,2).equalsIgnoreCase("OIDBemerkung")) {

				if (kg==null) {
					throw new XBoxValueManagementException("Die Kirchgemeinde muss mit '"+getSlotID()+"_SABAWoche_KG' spezifiziert werden");
				}				
				try {
					return CacheSABAWochen.getInstance(boContext).getStandardBemerkung(kg,tag,vierteltag,woche,jahr);
				} catch (CacheException e) {
					Log.logSystemAlarm("ERROR while accessing cache: "+e.getMessage());
					return null;
				}

			}  else if (EParseUtilities.getSubstring(attrName,1).startsWith("OID") && EParseUtilities.getSubstring(attrName,2).equalsIgnoreCase("Bemerkung")) {

				if (kg==null) {
					throw new XBoxValueManagementException("Die Kirchgemeinde muss mit '"+getSlotID()+"_SABAWoche_KG' spezifiziert werden");
				}				
				try {
					return CacheSABAWochen.getInstance(boContext).getBemerkung(kg,tag,vierteltag,woche,jahr);
				} catch (CacheException e) {
					Log.logSystemAlarm("ERROR while accessing cache: "+e.getMessage());
					return null;
				}
			} else if (EParseUtilities.getSubstring(attrName,1).startsWith("OID") && EParseUtilities.getSubstring(attrName,2).equalsIgnoreCase("isEditierbar")) {

				if (kg==null) {
					throw new XBoxValueManagementException("Die Kirchgemeinde muss mit '"+getSlotID()+"_SABAWoche_KG' spezifiziert werden");
				}				
				try {

					if(CacheSABAWochen.getInstance(boContext).isEditierbar(request, kg,tag,vierteltag,woche,jahr)) return "true"; else return "false";
				} catch (CacheException e) {
					Log.logSystemAlarm("ERROR while accessing cache: "+e.getMessage());
					return null;
				}
			} else if (EParseUtilities.getSubstring(attrName,2).startsWith("isBestatterGebucht")) {
				Vector methodP = new Vector();
				String method = EParseUtilities.parseMethod(EParseUtilities.getSubstring(attrName,2),methodP);
				if (!method.equals("isBestatterGebucht")) return null;
				try {
					if (CacheSABAWochen.getInstance(boContext).isBestatterGebucht((String)methodP.firstElement(),tag,vierteltag,woche,jahr)) return "true"; else return "false";
				} catch (CacheException e) {
					Log.logSystemAlarm("ERROR while accessing cache: "+e.getMessage());
					return null;
				}
			} else if (EParseUtilities.getSubstring(attrName,2).startsWith("getBuchendeKirchgemeinde")) {
				Vector methodP = new Vector();
				String method = EParseUtilities.parseMethod(EParseUtilities.getSubstring(attrName,2),methodP);
				if (!method.equals("getBuchendeKirchgemeinde")) return null;
				try {
					IKirchgemeinde kirchgemeinde = CacheSABAWochen.getInstance(boContext).getBuchendeKirchgemeinde((String)methodP.firstElement(),tag,vierteltag,woche,jahr);
					if (kirchgemeinde!=null) return kirchgemeinde.getName(); else return null;
				} catch (CacheException e) {
					Log.logSystemAlarm("ERROR while accessing cache: "+e.getMessage());
					return null;
				}
			} else {
				return null;
			}


		}
		else if (attrName.equalsIgnoreCase("saveSABAWoche_valGesperrt")) {
			String gesperrte = request.getParameters().getParameter(getSlotID()+"_saveSABAWoche_valGesperrt");
			return gesperrte;
		} else if (attrName.equalsIgnoreCase("saveSABAWoche_validation")) {
			return "false";
		} else if ("systemInfo_readonly".equals(attrName)) {
			return SystemInfo.getReadOnlyMode().toString();
		} else if ("systemInfo_disableImport".equals(attrName)) {
			return "" + SystemInfo.getImportsDisabled();
		} else if ("systemInfo_disableUserLogin".equals(attrName)) {
			return "" + SystemInfo.getUserLoginDisabled();
		} else {
			BoxView viewBox = (BoxView) EEngine.getInstance().getBox("view");
			return viewBox.getMDValue(request, new EValueID(attrName));
		}
	}

	public Vector getMDValueVector(IRequest request, EValueID id) throws XMetaModelQueryException, XBoxValueManagementException {
		return null;
	}
	private static int getOID (String attrName) {
		// get OID out of className:
		// syntax: className[OID]
		String oidString = "";
		int oid = 0;
		int index = attrName.indexOf('[');
		if (index != -1) {
			oidString = attrName.substring(index+1, attrName.length()-1);
			index = oidString.indexOf(']');
			if (index != -1) {
				oidString = oidString.substring(0, index);
			} else {
				oidString="";
			}
		}

		if (EDataTypeValidation.isNullString(oidString)) return -1;

		try {
			oid = Integer.parseInt(oidString);
		}
		catch (NumberFormatException x) {
			return -1;
		}

		return oid;
	}

	public static long getSABAWeek(String year, String week, String kg, BOContext context) {

		BLTransaction trx;
		try {
			MetaObject mobjw = MetaObjectLookup.getInstance().getMetaObject("SABAWoche");
			MetaObject mobjp = MetaObjectLookup.getInstance().getMetaObject("Kirchgemeinde");

			trx = BLTransaction.startTransaction(context);
			BOSABAWoche boWeek = null;

			BOObject boPerson = null;
			StringQuery strqp = new StringQuery(trx, 0);
			strqp.init("SABAPersonQuery", "SABAPersonQuery", mobjp);
			strqp.setSqlStatement("select * from bx_kirchgemeinde where OID=" + kg);
			Vector resPerson = strqp.getResult(trx, context);
			Enumeration e = resPerson.elements();
			while (e.hasMoreElements()) {
				BOObject o = (BOObject)e.nextElement();
				if (o.getAttribute("ArtOID", context).equals("Kirchgemeinde")) boPerson = o;
			}
			if (boPerson == null) {
				ch.objcons.log.Log.logDesignerAlarm ("kg " + kg + " not found");
				return 0;
			}

			synchronized (BoxEdit.class) {
				StringQuery strqw = new StringQuery(trx, 0);
				strqw.init("SABAWocheQuery", "SABAWocheQuery", mobjw);
				strqw.setSqlStatement("select * from bx_sabawoche where bx_oidkg=" + kg + " and bx_woche = " + week+" and bx_jahr = "+year);
				Vector resWeek = strqw.getResult(trx, context);
				if ((resWeek != null) && (resWeek.size() == 1)) {
					return ((BOObject)resWeek.elementAt(0)).getOID();
				}
	
				boWeek = (BOSABAWoche)trx.getObject(mobjw, 0, false);
				boWeek.setAttribute("OIDKG", "" + boPerson.getOID(), context, null);
				boWeek.setAttribute("Woche", week, context, null);
				boWeek.setAttribute("Jahr", year, context, null);
				boWeek.setValue("toCheck", new Boolean(false), context);
	
				boWeek.setAllZuordParam(context);
	
				trx.commit();
			}
			return boWeek.getOID();
		}
		/*
		catch (XBoxValueManagementException x) {
			ch.objcons.log.Log.logDesignerAlarm (x.getMessage(), x);
		}
		 */
		catch (Exception x) {
			ch.objcons.log.Log.logDesignerAlarm (x.getMessage(), x);
		}
		return 0;
	}
	Hashtable getValues(IRequest request) {
		Hashtable values = (Hashtable) request.getValue(getSlotID() + "_values");
		if (values == null) {
			values = new Hashtable();
			request.setValue(getSlotID() + "_values", values);
		}
		return values;
	}

	public IAction handleCommand(IRequest request, String id, String path, int xValue, int yValue) {
		BLTransaction trans = BLTransaction.startTransaction((BOContext) request.getContext());
		if (id.equals("cmd_createSABAWochen")) {
			checkSABACompletion(request);
			//			return new ActionCommit(this, request, this.getSlotID(), getMetaService(), path, trans, false, "cmd_createSABAWochen");
		}
		else if (id.equals("cmd_SABAFile")) {
			return new ActionSABAExport(this, request, this.getSlotID(), path, "cmd_SABAFile");
		} else if (id.equals("cmd_saveSABAWoche")) {
			return new ActionSaveSABAWoche(this,request,getSlotID(),path);
		}
		else if (id.equals("cmd_inspectScheduler")) {
			JobScheduler js = JobScheduler.getInstance();
			return null;
		}
		return null;
	}
	public IAction handleParameter (IRequest request, String id, String[] value) throws XBoxValueManagementException {
		if (id.endsWith("_OID")) {
			// add object identifiers as loop through value
			request.addLoopThroughValue(getSlotID() + "_" + id, value[0]);

			// test if the path to the OID is correct
			try {
				AttributeInformation attrInfo = new AttributeInformation(id, getSlotID(), this.getMetaService(), AttributeInformation.FILTER_ALL, request, null);
			}
			catch (XMetaModelQueryException e) {
				this.addPublisherError(request, "incorrect path to OID: " + e.getMessage());
			}
			catch (XSecurityException e) {
				this.addPublisherError(request, "incorrect path to OID: " + e.getMessage());
			}
		}
		else if (id.equals("cmd_SABAWocheImport")) {
			SABAImport.importSABAWoche(this,(BOContext)request.getContext());
		}
		else if (id.startsWith("sortparam")) {

		} 
		else if (id.equals("cmd_SABAverfuegbar")) {
			/*
			Vector v = CacheSABAWochen.getInstance((BOContext)request.getContext()).getVerfuegbareBestatter("1",0,0,"7","2005");
			System.out.println("SABA verfuegbar: ");
			for (int i = 0;i<v.size();i++) {
				System.out.println(((BestatterWoche)v.elementAt(i)).getKurz());
			}
			CacheSABAWochen.getInstance((BOContext)request.getContext()).clearCache();*/
		} else if (id.equals("systemInfo_readonly")) {
			SystemInfo.processReadOnlyMode(ReadOnlyMode.valueOf(value[0]));
		} else if ("systemInfo_disableImport".equals(id)) {
			SystemInfo.setImportsDisabled(Boolean.valueOf(value[0]));
		} else if ("systemInfo_disableUserLogin".equals(id)) {
			SystemInfo.setUserLoginDisabled(Boolean.valueOf(value[0]));
		} else {
			// put all input values to the buffer
			getValues(request).put(id, value);
		}

		return null;
	}
	
	public static Vector executeQuery (MetaObject aMetaobject, String aFilter, BLTransaction aTransaction, BOContext aBoContext) throws XMetaModelNotFoundException, XExpression, XMetaException, XDataTypeException, XMetaModelQueryException, XSecurityException, XMaxRowException {
		StandardQuery standardQuery = BLManager.instance().getExtent(aMetaobject);
		if (aFilter == null || aFilter.equals("")) {
			return standardQuery.getResult(aTransaction, aBoContext);
		}
		else {
			IExpression expressions = ExprParser.getExpression(aFilter, false);
			SelectExprIterator expressionCollector = new SelectExprIterator(standardQuery, false, aBoContext);
			expressions.iterate(expressionCollector); 
			return standardQuery.getResult(aTransaction, true, true, null, null /* limit-row-count */, null, expressionCollector.getWhereStatement(), "ASC", null, aBoContext);
		}
	}


	public void init (IRequest request, IBoxContainer theContainer) throws Exception {
		super.init(request, theContainer);
		initProperties();

		// set commands
		theContainer.addCommand(this, "cmd_createSABAWochen", false);
		theContainer.addCommand(this, "cmd_SABAFile", false);
		theContainer.addCommand(this, "cmd_wordlistperson", false);
		theContainer.addCommand(this, "cmd_saveSABAWoche", false);
		theContainer.addCommand(this, "cmd_fixRedu", false);
		theContainer.addCommand(this, "cmd_inspectScheduler", false);
	}

	public void initPhase2(IRequest request) throws Exception {
		super.initPhase2(request);
		ch.objcons.log.Log.logDesignerInfo("initPhase2 started");
		LOGGER.info("Box EDVKG is starting");
		// insert the SABAAuslieferAction into the Scheduler

		try {
			ActionSABAExport saba = new ActionSABAExport(this, request, "edvkg", "", "automated");
			ch.objcons.ecom.system.JobScheduler sched = ch.objcons.ecom.system.JobScheduler.getInstance();
			int hour = Integer.parseInt(_SABAAuslieferung.substring(0, 2));
			int min = Integer.parseInt(_SABAAuslieferung.substring(3, 5));
			GregorianCalendar cal = new GregorianCalendar();
			GregorianCalendar actCal = new GregorianCalendar();
			cal.set(Calendar.MINUTE, min);
			cal.set(Calendar.HOUR_OF_DAY, hour);
			// if we set a date in the past, the scheduler will do it immediately and we don't like that
			if (actCal.getTime().getTime() > cal.getTime().getTime()) {
				cal.add(Calendar.HOUR, 24); // so we will be active in the next slot
			}
			ch.objcons.log.Log.logDesignerInfo("schedule SABAExport for "+cal.getTime());

			sched.executeAtAndRepeat("SABAAuslieferung", saba, cal.getTime(), JobScheduler.DAILY);
			ch.objcons.log.Log.logDesignerInfo("SABAExport scheduled");

			GregorianCalendar cal2 = new GregorianCalendar();
			cal2.set(Calendar.MINUTE, 00);
			cal2.set(Calendar.HOUR_OF_DAY, 2);
			if (actCal.getTime().getTime()>cal2.getTime().getTime()) {
				cal2.add(Calendar.HOUR,24);
			}
			sched.executeAtAndRepeat("SABACacheRefresh", CacheSABAWochen.getInstance((BOContext)request.getContext()),cal2.getTime(),JobScheduler.DAILY);
			ch.objcons.log.Log.logDesignerInfo("SABACacheRefresh scheduled for "+cal2.getTime());
		}
		catch (Exception e) {
			// if any parsing or other things failes we just mack a error output
			ch.objcons.log.Log.logDesignerError("Exception: Couldn't schedule SABAExport", e);
		}
		BLManager.getInstance().addBOObjectChangeListener(MetaObjectLookup.getInstance().getMetaObject("LoginUser"), new LoginUserChangeListener());
		SystemInfo.setUserLoginDisabled(SystemInfo.getUserLoginDisabled());
	}
	
	protected void initProperties () throws Exception {
		//  get the default values
		_useSubstringInQuery = new Boolean(getProperty("use substring in query" , "true")).booleanValue();
		_SABAAuslieferung = getProperty("SABA-Auslieferung" , "05:00");

		_SABAAnzahlFreiTage = getProperty("SABA-Anzahl Freitage" , "2");
		String v = getProperty("SABA-Anzahl Wochen" , "5");
		try {
			_SABAAnzahlWochen = Integer.parseInt(v);
		}
		catch (Exception x) {
		}
		_useImportWindow = getProperty_boolean("use import time window" , "true");
		_importWindowHourStart = getProperty_int("import time window start hour", 22);
		_importWindowHourEnd = getProperty_int("import time window end hour", 4);
	}
	
	public static boolean useSubstringInQuery() {
		return _useSubstringInQuery;
	}


	public static BoxEdit getInstance() {
		return Manager.getService(BoxEdit.class);
	}
	
}
