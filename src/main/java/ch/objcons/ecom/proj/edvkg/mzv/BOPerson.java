package ch.objcons.ecom.proj.edvkg.mzv;

import static ch.objcons.ecom.datatypes.EDataTypeValidation.isNullOrEmptyString;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.BezirkIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.CodeHaus_Postzustellbezirk;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.CodeStrasse_Name;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.HausNrIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.KonfessionIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.Konfession_code;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.OIDCodeStrasseIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.OIDEhepartnerIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.OIDFamilieIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.OIDKGIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.OIDKonfessionIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.OIDMutterIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.OIDVaterIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.OIDVorstandIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.PLZPostfachIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.PostfachIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.SortHausNrIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.StrasseKGIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.VorstandIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusAdrZusatzIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusArtIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusHausNrIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusOIDCodeOrtLandIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusOIDStrasseIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusOrtIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusOrtLandNameIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusPLZOrtEindIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusPLZOrtIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusPLZPostfachEindIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusPLZPostfachIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusPostfachIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusPostlagerndIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusSortHausNrIndex;
import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusStrassenNameIndex;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

import ch.objcons.db.dbimport.LogFileWriter;
import ch.objcons.ecom.api.IBox;
import ch.objcons.ecom.api.ISession;
import ch.objcons.ecom.api.XBoxValueManagementException;
import ch.objcons.ecom.bl.BLFactory;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.BOValidationException;
import ch.objcons.ecom.bom.IBOChangeSourceObserver;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.datatypes.EDataTypeValidation;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.engine.EEngine;
import ch.objcons.ecom.meta.ListAttribute;
import ch.objcons.ecom.meta.MetaAttribute;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.log.Log;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

public class BOPerson extends BOObject implements IBOChangeSourceObserver {

	public static final String CLASS_NAME = "Person";
	
	static ILogger LOGGER = Logger.getLogger("boxalino.edvkg.person.bo");
	static LogFileWriter LogFileWriter2Logger = new LogFileWriter(LOGGER);

	private static final int _KonfessionChanged = 0x1;
	private static final int _HausNrChanged = 0x2;
	private static final int _OIDCodeStrasseChanged = 0x4;
	private static final int _ZusOIDCodeStrasseChanged = 0x80;
	private static final int _ZusHausNrChanged = 0x8;
	private static final int _ZusStrassenNameChanged = 0x10;
	private static final int _PostfachChanged = 0x20;
	private static final int _ZusPostfachChanged = 0x40;
	private static final int _PLZPostfachChanged = 0x100;
	private static final int _ZusPLZPostfachChanged = 0x200;
	private static final int _ZusPLZOrtChanged = 0x400;
	private static final int _ZusArtChanged = 0x800;
	private static final int _ZeitungVerzichtChanged = 0x1000;
	private static final int _EWIDChanged = 0x2000;
	private static final int _ZustellBezirkDirty = _ZusArtChanged | _HausNrChanged 
	| _OIDCodeStrasseChanged | _ZusOIDCodeStrasseChanged |_ZusHausNrChanged 
	| _ZusStrassenNameChanged | _PostfachChanged | _ZusPostfachChanged
	| _PLZPostfachChanged | _ZusPLZPostfachChanged;
	private static final int _StrasseKGDirty = _ZusArtChanged | _OIDCodeStrasseChanged 
	| _ZusOIDCodeStrasseChanged;

	private static final int[] attrsNotToHistorize = new int[] { 
		Consts.OIDIndex,
		Consts.ArtOIDIndex,
		Consts.BezirkIndex,
		Consts.PersonensperreStadtIndex,
		Consts.autoZeitungIndex,
		Consts.FamInfoIndex,
		Consts.PartnerBezFamIndex,
		Consts.OIDOrigPartnerIndex,
		Consts.OIDEhepartnerIndex,
		Consts.SchuelerInfoIndex,
		Consts.CodeHausKGIndex
	};
	
	static {
		Arrays.sort(attrsNotToHistorize);
	}
	
	// tells if we changed some information regarding the KG's of the familiy
	private int _changedFields = 0;
	private char _origZusArt;
	boolean _changedKGData = false;
	boolean _updatedInMask = false;
	String _origKG = null;


	public BOPerson(BLTransaction trans, long oid) {
		super(trans, oid);
		//Wird auf false gesetzt, weil BOChangelistener existiert.
		//_watchChanges = false;
	}

	public void setZustellEmpty(BOContext boContext) throws XMetaException, XSecurityException {
		setReferencedObject(ZusOIDStrasseIndex, null);
		setValue(ZusStrassenNameIndex, null, boContext);
		setValue(ZusHausNrIndex, null, boContext);
		setValue(ZusAdrZusatzIndex, null, boContext);
		setValue(ZusPostfachIndex, null, boContext);
		setValue(ZusPLZPostfachIndex, null, boContext);
		setValue(ZusPLZPostfachEindIndex, null, boContext);
		setValue(ZusOrtIndex, null, boContext);
		setValue(ZusPLZOrtIndex, null, boContext);
		setValue(ZusPLZOrtEindIndex, null, boContext);
		setValue(ZusOIDCodeOrtLandIndex, null, boContext);
		setValue(ZusOrtLandNameIndex, null, boContext);
		setValue(ZusPostlagerndIndex, null, boContext);
		setValue(ZusArtIndex, null, boContext);
	}

	/*
	 * write objects to database
	 * propagate action to depending objects and list objects (list values)
	 */
	public void writeToDB(boolean isInsert, Connection con) throws SQLException, XMetaException, XSecurityException, XBOConcurrencyConflict {
		BOContext boContext = _trans.getContext();
		boolean origRefCheckFlag = boContext.isReferenceCheckSuppressed();
		try {
			/*
			 * 09.01.2004 dhe/mja setReferenceCheckSuppressed
			 *
			 * wir tun dies, da Personen in der DB sein k�nnen, welche z.B. in OIDHaushalt
			 * eine OID haben, f�r welche es auf der DB keine Person gibt
			 * dies liegt daran, dass der Import bei der Aktivierung auch fehlerhafte Personen
			 * aktiviert und somit m�gliche Inkonsistenzen auftreten k�nnen, welche einen Update
			 * verhindern t�ten.
			 *
			 */
			boContext.setReferenceCheckSuppressed(true);

			try {
				String zusArt = (String)getValue(ZusArtIndex, false, boContext);
				boolean stadtAdresse = "s".equals(zusArt);
				boolean zusArtEmpty = isNullOrEmptyString(zusArt);

				/* *** ZusArt *** */
				if ((_changedFields & _ZusArtChanged) != 0 && zusArtEmpty) {
					setZustellEmpty(boContext);
				} else if ("M".equals(zusArt)) {
					zusArt = "m";
					super.setAttribute(ZusArtIndex, zusArt, boContext, null);
					setValue(ZusOIDStrasseIndex, null, boContext);
					setValue(ZusPLZOrtEindIndex, null, boContext);
					setValue(ZusPLZPostfachEindIndex, null, boContext);
				}
				if ("m".equalsIgnoreCase(zusArt)) {
					if (UtilsEDVKG.isManualZustellEmpty(this, boContext)) {
						setZustellEmpty(boContext);
					}
				}

				/* *** StrasseKG *** */
				BOObject strObj = null;
				BOObject oidCodeStrasse = null;
				BOObject zusOIDStrasse = null;
				if ((_changedFields & _StrasseKGDirty ) != 0) {
					if (stadtAdresse) {
						zusOIDStrasse = getReferencedObject(ZusOIDStrasseIndex, false, boContext);
						strObj = zusOIDStrasse;
					} else {
						oidCodeStrasse = getReferencedObject(OIDCodeStrasseIndex, false, boContext);
						strObj = oidCodeStrasse;
					}
					String strasse = null;
					if (strObj != null) strasse = strObj.getAttribute(CodeStrasse_Name, false, boContext);
					if (strasse != null) super.setAttribute(StrasseKGIndex, strasse, boContext, null);
				}
				/* *** HausNr *** */
				if ((_changedFields & _HausNrChanged) != 0) {
					String hausNr = getAttribute(HausNrIndex, false, boContext);
					String sortHausNr = null;
					// Hausnummer in separates Feld kopieren
					if (hausNr != null) {
						int i = 0;
						while (i < hausNr.length() && hausNr.charAt(i) >= '0' && hausNr.charAt(i) <= '9') i++;
						if (i > 0) sortHausNr = hausNr.substring(0, i); else sortHausNr = null;
					}
					if (sortHausNr != null) super.setAttribute(SortHausNrIndex, sortHausNr, boContext, null);
				}
				/* *** ZusHausNr *** */
				if ((_changedFields & _ZusHausNrChanged) != 0) {
					String wahlHausNr = getAttribute(ZusHausNrIndex, false, boContext);
					String wahlSortHausNr = null;
					// Wahl-Hausnummer in separates Feld kopieren
					if (wahlHausNr != null) {
						int i = 0;
						while (i < wahlHausNr.length() && wahlHausNr.charAt(i) >= '0' && wahlHausNr.charAt(i) <= '9') i++;
						if (i > 0) wahlSortHausNr = wahlHausNr.substring(0, i); else wahlSortHausNr = null;
					}
					if (wahlSortHausNr != null) super.setAttribute(ZusSortHausNrIndex, wahlSortHausNr, boContext, null);
				}

				/* *** Bezirk *** 
				 * Falls an Adressdaten, welche f�r die Ermittlung des Zustellbezirks wichtig 
				 * sind, etwas ge�ndert wurde, dann Zustellbezirk aktualisieren oder l�schen 
				 */
				if ((_ZustellBezirkDirty & _changedFields) != 0) {
					String postfach = getAttribute(PostfachIndex, false, boContext);
					String plzpostfach = getAttribute(PLZPostfachIndex, false, boContext);
					String zusPostfach = getAttribute(ZusPostfachIndex, false, boContext);
					String zusPlzpostfach = getAttribute(ZusPLZPostfachIndex, false, boContext);
					long tmpOIDStrasse = 0;
					String tmpHausNr = null;
					if (zusArtEmpty && isNullOrEmptyString(postfach) && isNullOrEmptyString(plzpostfach)) {
						tmpOIDStrasse = getReferencedOID(OIDCodeStrasseIndex);
						tmpHausNr = getAttribute(HausNrIndex, false, boContext);
					} else if (stadtAdresse && isNullOrEmptyString(zusPostfach) && isNullOrEmptyString(zusPlzpostfach)) {
						tmpOIDStrasse = getReferencedOID(ZusOIDStrasseIndex);
						tmpHausNr = getAttribute(ZusHausNrIndex, false, boContext);
					} 
					boolean deleteBezirk = true;
					if (tmpOIDStrasse > 0 && tmpHausNr != null) {
						String query = "select * from bx_codehaus where bx_OIDStrasse="+ tmpOIDStrasse +" and bx_hausnr='" + tmpHausNr + "'";
						Vector histObjs = BLManager.getResult(_trans, query, true /* isAlreadyFiltered */, boContext, con);
						if (histObjs != null) {
							Enumeration e = histObjs.elements();
							if (e.hasMoreElements()) {
								BOObject bo = (BOObject)e.nextElement();
								String bezirk = bo.getAttribute(CodeHaus_Postzustellbezirk, false, boContext);
								super.setAttribute(BezirkIndex, bezirk, boContext, null);
								deleteBezirk = false;
							}
						}
					}
					if (deleteBezirk) {
						/* Zustellbezirk l�schen */
						super.setAttribute(BezirkIndex, null, boContext, null);
					}
				}

				/* *** Konfession setzen *** */
				if ((_KonfessionChanged & _changedFields) != 0 
						&& isNullOrEmptyString(getAttribute(KonfessionIndex, false, boContext))) {
					BOObject konfObj = getReferencedObject(OIDKonfessionIndex, false, boContext);
					String konfession;
					if (konfObj != null) konfession = konfObj.getAttribute(Konfession_code, false, boContext); else konfession = null; 
					super.setAttribute(KonfessionIndex, konfession, boContext, null);
				}
				if (isNew()) {
					long oid = getOID();
					if (oid == 0) {
						setOID();
						oid = getOID();
					}
					if (oid  >= 600 * 1000 * 1000) {
						setAttribute(OIDFamilieIndex, "" + oid, boContext);
					}
				}
			} catch (XDataTypeException e) {
				throw new XMetaException (e.getName(), e.getValue(), e.getMessage());
			}
			boolean hasChanged = hasChangedAtributes();
			super.writeToDB(isInsert, con);
			/* reset changed field mask */
			_changedFields = _changedFields & (_ZeitungVerzichtChanged | _EWIDChanged);

			/* set the sesstion to switch the history off */
			ISession mySession = boContext.getRequest().getSession();
			String noHistory = (String)mySession.getValue("import_noHistory");

//			if (hasChangedAtributes()) {
			if (hasChanged) {
				/* Write History Item */
				try {
					if ( ! "true".equals(noHistory) ) {

						BOObject lastHistObj = getActiveHistObj(boContext, con);

						// set the historyObject the status to inactive:
						// - change only the entries with the same KG, there may be others for other KG's (if there was an Umzug)
						// - if the KG changed, we change the 'isLast' of the new KG only!
						String oid = this.getAttribute("OID", false, boContext);
						String query = "select * from bx_personhistory where bx_OIDPerson="+oid+" and bx_isLast='j'";
						String oidKG = this.getAttribute("OIDKG", false, boContext);
						if (oidKG == null) {
							query += " and bx_oidkg is null";
						}
						else {
							query += " and bx_oidkg = " + oidKG;	
						}
						query += " order by oid desc";

						// now we inactivate the old one
						Vector histObjs = BLManager.getResult(_trans, query, true /* isAlreadyFiltered */, boContext, con);
						if (histObjs.size() > 0) {
							Enumeration e = histObjs.elements();
							while (e.hasMoreElements()) {
								BOObject bo = (BOObject)e.nextElement();
								bo.setValue("isLast", new Boolean(false), boContext);
								bo.writeBack(con);
							}
						} else {
							//Log.logSystemAlarm("Personen-Historisierung: Person (OID: " + oid + " und KG: " + oidKG + ") hatte kein als letzten markierten History Eintrag f�r die entsprechende KG");
							/* KG der Person hat ge�ndert */
						}
						// Falls History BOs im Memory sind, stimmen die Attribute lastEntry nicht mehr unbedingt �berein
						// Dies macht aber nichts, da lastEntry nur f�r direkte SQL Queries gebraucht wird.
						adjustLastEntry(oid, con);

						MetaObject histMetaObject = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
						BOObject histObj = _trans.getObject(histMetaObject, 0 /* OID */, false /* isPartOf */);
						String attrValue;

						// set all the attributes in the History
						MetaAttribute [] attrs = this.getMetaObject().getAllAttributes();

						// copy all except not OID, ArtOID and List Attributes (Zeitungen, OrgPerson)
						for (int count=0; count < attrs.length; count++) {
							if ( !(attrs[count] instanceof ListAttribute) ) {
								if (Arrays.binarySearch(attrsNotToHistorize, attrs[count].getIndex()) < 0 &&
									attrs[count].getLifecycleType() == MetaAttribute.LIFECYCLE_PERSISTENT) {
									
									attrValue = this.getAttribute(attrs[count].getIndex(), false /*useAccessControl*/, boContext);
									histObj.setAttribute( attrs[count].getExternName(), attrValue, boContext, null /* Warnings */);
									//							}
								} else if (attrs[count].getIndex() == Consts.PersonensperreStadtIndex){
									attrValue = this.getAttribute(attrs[count].getIndex(), false /*useAccessControl*/, boContext);
									histObj.setAttribute(Consts.Hist_AdressSperreSicherheitIndex, attrValue, boContext);
								} else if (attrs[count].getIndex() == Consts.OIDOrigPartnerIndex) {
									long poid = getReferencedOID(Consts.OIDOrigPartnerIndex);
									if (poid > 0) {
										histObj.setAttribute(Consts.Hist_OIDEhepartnerIndex, "" + poid, boContext);
									}
								}
							}
						}
						// copy the origKG from the last history object
						if (lastHistObj != null) {
							attrValue = lastHistObj.getAttribute(Consts.Hist_OIDKGOrigIndex, false /*useAccessControl*/, boContext);
							histObj.setAttribute(Consts.Hist_OIDKGOrigIndex, attrValue, boContext);
						}

						// set the origKG
						if (_origKG != null) {
							histObj.setAttribute(Consts.Hist_OIDKGOrigIndex, _origKG, boContext);
						}

						// set the date
						Date curDate = new Date();
						histObj.setValue(Consts.Hist_DatVerarbeitungIndex, curDate);
//						histObj.setValue("DatVorfall", new java.sql.Date(System.currentTimeMillis()), boContext);
						histObj.setValue(Consts.Hist_ErstesUpdateDatumIndex, curDate);
						histObj.setValue(Consts.Hist_DateIndex, curDate);
						histObj.setValue(Consts.Hist_isLastIndex, new Boolean(true));
						histObj.setValue(Consts.Hist_lastEntryIndex, new Boolean(true));
						histObj.setAttribute(Consts.Hist_OIDPersonIndex, ""+this.getOID(), boContext);
						// alarm mail mh@20061113
						if (this.getOID() == 0) Log.logSystemAlarm("Es wurde eine Historyzeile geschrieben mit OidPerson == 0. OID des Personen-Objekts: " + this.getAttribute("OID", boContext));
						// set the user mh@20061012
						IBox loginBox = EEngine.getInstance().getBox("login");
						String userStr = null;
						String userOID = null;
						try {
							userStr = loginBox.getValue(boContext.getRequest(), loginBox.getValueID("loginName"));
							if (userStr == null) userStr = "(unbekannt)"; else {
								userOID = loginBox.getValue(boContext.getRequest(), loginBox.getValueID("OID"));
								userStr = userStr + " [" + userOID + "]"; 
							}
						} catch (XBoxValueManagementException e) {
							System.err.println("Couldn't update user string");
						}
						histObj.setAttribute("User", userStr, boContext, null);
						histObj.writeBack(con);
					}
				}
				catch (XDataTypeException e) {
					// just throw an exception that we can throw
					throw new XMetaException (e.getName(), e.getValue(), e.getMessage());
				}
			}
		}
		finally {
			boContext.setReferenceCheckSuppressed(origRefCheckFlag);
		}
	}

	private void adjustLastEntry(String oid, Connection con) throws XMetaModelQueryException {
		String sql = "update bx_personhistory set bx_lastEntry = 'n' where bx_oidperson = " + oid; 
		BLManager.executeUpdate(con, sql, false);
		BLFactory.instance().emptyBuffers(Consts.PersonHistoryMeta);
	}

	@Override
	public String getAttribute(int id, boolean useAccessControl, BOContext context) 
		throws XMetaModelQueryException, XSecurityException {

		if (id == Consts.VorstandIndex) {
			Number oidFam = (Number) getValue(Consts.OIDFamilieIndex);
			return Boolean.toString(oidFam != null && oidFam.longValue() == getOID());
		} 
		return super.getAttribute(id, useAccessControl, context);
	}

	/*
	 * deletes an object from the database;
	 */
	public void deleteFromDB(Connection con) throws SQLException, XMetaException, XSecurityException, XBOConcurrencyConflict {
		super.deleteFromDB(con);

		BOObject histObj;
		BOContext boContext = _trans.getContext();
		ISession mySession = boContext.getRequest().getSession();
		String noHistory = (String)mySession.getValue("import_noHistory");

		if ( ! "true".equals(noHistory) ) {
			// set the historyObject the status to inactive
			histObj = this.getActiveHistObj(boContext, con);
			if (histObj != null) {
				histObj.setValue("StatusPerson", new Boolean(false), boContext);
				histObj.setValue("PassivDatum", new java.sql.Date(System.currentTimeMillis()), boContext);
				histObj.setValue("DatVerarbeitung", new java.sql.Date(System.currentTimeMillis()), boContext);
				histObj.setValue("DatVorfall", new java.sql.Date(System.currentTimeMillis()), boContext);
				histObj.writeBack(con);
			}
		}
	}

	public void validate(BOContext context) throws BOValidationException, XMetaModelQueryException, XSecurityException {
		super.validate(context);

		if (isDeleted()) {
			// test if person is part of organisation
			if (getListReferencedObjects("OIDListOrgP", context).size() > 0) {
				throw new BOValidationException("Person ist noch Mitglied einer Organisation - l�schen nicht erlaubt");
			}		
		}
		else {
			// to do:
			// Test, ob Konfession mit KG �bereinstimmt, bzw. bei Konfessionslosen oder Andersgl�ubigen auf
			// leer gesetzt ist!!!

			/* Verifiy Zustelladresse */
			UtilsEDVKG.validateZustellAdresse(this, context);
		}
	}

	/**
	 * returns the active History Object
	 */
	private BOObject getActiveHistObj(BOContext boContext, Connection con) throws XMetaException, XSecurityException{
		// set the historyObject the status to inactive
		String persoid = this.getAttribute("OID", boContext);
		String query = "select * from bx_personhistory where bx_OIDPerson="+persoid + " and bx_lastentry='j' " + 
		" order by oid desc";

		Vector histObjs = BLManager.getResult(_trans, query, true /* isAlreadyFiltered */, boContext, con);
		if (histObjs.size() > 0) {
			// we have a object
			return (BOObject)histObjs.elementAt(0);
		} else {
			/* should never happen */
			try {
				/* Korrektur vornehmen */
				query = "select * from bx_personhistory where bx_OIDPerson="+ persoid + " order by oid desc";
				BLTransaction extraTrans = BLTransaction.startTransaction(boContext);
				histObjs = BLManager.getResult(extraTrans, query, true /* isAlreadyFiltered */, boContext, con);
				if (histObjs.size() > 0) {
					BOObject histObj = (BOObject)histObjs.elementAt(0);
					long histOID = histObj.getOID();
					histObj.setValue("isLast", new Boolean(true), boContext);
					histObj.setValue("lastEntry", new Boolean(true), boContext);
					extraTrans.commit();
					histObj = _trans.loadObject("PersonHistory", histOID);
					if (((Boolean)histObj.getValue("lastEntry", false, null)).booleanValue() != true) Log.logSystemInfo("Inconsistent bom behaviour");
					return histObj;
				} else if (Long.parseLong(persoid) < 500000000L) {
					Log.logSystemError("Personen-Historisierung: Person (ZIP: " + persoid + ") hat keine History!");
				}
			} catch (Exception e) {
				Log.logSystemAlarm("Exception beim Korrigieren der letzen History-Objekts", e);
			}
		}
		return null;
	}


	/**
	 * @see ch.objcons.ecom.bom.BOObject#setAttribute(String, String, BOContext, Vector)
	 */
	public void setAttribute(int attrId, String value, BOContext context, Vector warnings)
	throws XMetaException, XDataTypeException, XSecurityException {
		boolean zeitungsVerzichtWasNull = getValue(Consts.ZeitungVerzichtIndex) == null;

		boolean kgChanged = false;
		// compare relevant data and set _changedKGData
		if (attrId == OIDKGIndex ||
				attrId == OIDVaterIndex ||
				attrId == OIDMutterIndex ||
				attrId == OIDEhepartnerIndex ||
				attrId == VorstandIndex ||
				attrId == OIDVorstandIndex ||
				attrId == OIDFamilieIndex) {

			kgChanged = true;
			if (value != null) value = value.trim();

			// Achtung: bei Vorstand ist 'false' und NULL identisch!!!
			String oldValue = getAttribute(attrId, false, context);
			if (((oldValue == null) && (EDataTypeValidation.isNullString(value) || ("false".equals(value)))) || 
					((oldValue != null) && oldValue.equals(value))) {
				// falls sich nichts ge�ndert hat:
				// nothing to do ...
			}
			else {
				_changedKGData = true;

				// store origKG
				String noHistory = (String)context.getRequest().getSession().getValue("import_noHistory");
				if ( ! "true".equals(noHistory) ) {
					if (attrId == OIDKGIndex && EDataTypeValidation.isNullString(value)) {
						_origKG = oldValue;
					}
				}
			}
		}		
		super.setAttribute(attrId, value, context, warnings);

		if (!kgChanged) {
			if (attrId == OIDKonfessionIndex && attributeChanged(attrId)) {
				_changedFields = _changedFields | _KonfessionChanged;
			} else if (attrId == HausNrIndex && attributeChanged(attrId)) {
				_changedFields = _changedFields | _HausNrChanged;
			} else if (attrId == OIDCodeStrasseIndex && attributeChanged(attrId)) {
				_changedFields = _changedFields | _OIDCodeStrasseChanged;
			} else if (attrId == ZusOIDStrasseIndex && attributeChanged(attrId)) {
				_changedFields = _changedFields | _ZusOIDCodeStrasseChanged;
			} else if (attrId == ZusHausNrIndex && attributeChanged(attrId)) {
				_changedFields = _changedFields | _ZusHausNrChanged;
			} else if (attrId == ZusStrassenNameIndex && attributeChanged(attrId)) {
				_changedFields = _changedFields | _ZusStrassenNameChanged;
			} else if (attrId == PostfachIndex && attributeChanged(attrId)) {
				_changedFields = _changedFields | _PostfachChanged;
			} else if (attrId == PLZPostfachIndex && attributeChanged(attrId)) {
				_changedFields = _changedFields | _PLZPostfachChanged;
			} else if (attrId == ZusPostfachIndex && attributeChanged(attrId)) {
				_changedFields = _changedFields | _ZusPostfachChanged;
			} else if (attrId == ZusPLZPostfachIndex && attributeChanged(attrId)) {
				_changedFields = _changedFields | _ZusPLZPostfachChanged;
			} else if (attrId == ZusArtIndex && attributeChanged(attrId)) {
				_changedFields = _changedFields | _ZusArtChanged;
			} else if (attrId == Consts.EWIDIndex && attributeChanged(attrId)) {
				_changedFields = _changedFields | _EWIDChanged;
			} 
		}
		if (attrId == Consts.ZeitungVerzichtIndex && attributeChanged(attrId)
				&& !("false".equals(value) && zeitungsVerzichtWasNull)) {
			_changedFields = _changedFields | _ZeitungVerzichtChanged;
		}
	}

	public void notify(Object changeSource, Object boContextInfo) {
		if ("edit".equals(changeSource)) {
			_updatedInMask = true;
		}		
	}
	
	public boolean hasZeitungsVerzichtChanged(){
		return (_changedFields & _ZeitungVerzichtChanged) != 0;
	}

	public boolean hasAdressZusatzIndexChanged(){
		return (_changedFields & _EWIDChanged) != 0;
	}

	/**
	 * Checks, whether this person is active based on statusPerson.
	 * @return true, if statusPerson is true
	 */
	public boolean isActiveSP(BOContext context) {
		try {
			return Boolean.TRUE.equals(getValue("StatusPerson", context));
		} catch (XMetaModelNotFoundException e) {
			throw new RuntimeException(e);
		} catch (XSecurityException e) {
			throw new RuntimeException(e);
		}
	}
	
	public boolean isActiveRPG(BOContext context) {
		try {
			return Boolean.TRUE.equals(getValue("rpgAktiv", context));
		} catch (XMetaModelNotFoundException e) {
			throw new RuntimeException(e);
		} catch (XSecurityException e) {
			throw new RuntimeException(e);
		}
	}
	
}