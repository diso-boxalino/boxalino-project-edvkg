package ch.objcons.ecom.proj.edvkg.mzv.bva;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum BVAStatus {

	DELIVERED("geliefert"), 
	INCONSISTENT("inkonsistent"),
	SUPPRESSED("inkonsistent ohne Anrecht");
	
	private static final Map<String, BVAStatus> LABEL_TO_STATE;
	
	static {
		Map<String, BVAStatus> m = new HashMap<String, BVAStatus>();
		for (BVAStatus v : values()) {
			m.put(v.getLabel(), v);
		}
		LABEL_TO_STATE = Collections.unmodifiableMap(m);
	}
	
	private final String _label;

	private BVAStatus(String label) {
		_label = label;
	}
	
	public String getLabel() {
		return _label;
	}
	
	public static BVAStatus byLabel(String label) {
		return LABEL_TO_STATE.get(label);
	}
	
}
