package ch.objcons.ecom.proj.edvkg.tests;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.annotations.Test;

public class TestNGProto {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { MyNGTest.class });
		testng.addListener(tla);
		testng.run(); 
	}

	@Test
	public void g() {
		
	}
	
}
