package ch.objcons.ecom.proj.edvkg;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.db.SQLFormat;
import ch.objcons.ecom.api.EContent;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.adapters.EActionAdapter2;
import ch.objcons.ecom.api.adapters.EBoxWithMetaDataAdapter;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOFileUpload;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.system.security.XSecurityException;

/*
 * makes the export for the Zeitung
 *
 */
public class ActionSABAExport extends EActionAdapter2 implements Runnable {// implements com.ibm.as400.access.ActionCompletedListener {

	// private IFileStorageEntry _file;
	private FileInputStream outFile;
	String _command = null;
	boolean _completed = false;
	private BoxEdit _box = null;

	public ActionSABAExport(BoxEdit boxEdit, IRequest request,
		String slotID, String nextPage, String command) {
		super (boxEdit, request, slotID, nextPage);
		_box = boxEdit;
		_command = command;
	}
	
	private String fixWide(String text, int length) throws UnsupportedEncodingException
	{
		if (text == null) {
			text = "";
		}
		text = new String(text.getBytes(), "iso-8859-1");
		if (text.length() != length) {
	        if (text.length() > length) {
			    text = text.substring(0,length);
		    }
		    else {
			    char [] myArr = new char[length-text.length()];
				for (int i=0;i<myArr.length;i++) {
				    myArr[i] = ' ';
				}
				text = (new StringBuffer(text)).append(myArr).toString();
		    }
		}
		if (length != text.length()) {
			System.out.println("hello");
		}
		return text;
	}
	

	/**
	 * implementation of the Runnable Interface
	 * That will be called from the Job Scheduler
	 */
	public void run() {
		ch.objcons.log.Log.logDesignerInfo("SABAExport running");
		try {
			this.performX();
		}
		catch (Exception e) {
			ch.objcons.log.Log.logSystemError("Error Processing SABA", e);
		}
	}


	public void performX () throws XMetaException, XDataTypeException, XSecurityException {
		EContent result;
		EContent result2;
		File outFile = null;
		File outFile2 = null;
		OutputStream outStream;
		OutputStream outStream2;
		InputStream inStream;
		InputStream inStream2;
		
		String command;
		EBoxWithMetaDataAdapter constsBox = (EBoxWithMetaDataAdapter)ch.objcons.ecom.engine.EEngine.getInstance().getBox("global");

		

			// call the ftp program
			if (true) { //("automated".equals(_command)) {
				//System.out.println("Automatische Auslieferung startet...");
				ch.objcons.log.Log.logDesignerInfo("automated export (performX) started");
				
				
				BOContext boContext = (BOContext)getRequest().getContext();
				MetaObject moProt = MetaObjectLookup.getInstance().getMetaObject("SABAUeb");
				BLTransaction trans = BLTransaction.startTransaction(boContext);
				BOObject boProt = trans.getObject(moProt,0,false);
				
				try {
					_box.checkSABACompletion(getRequest());
					ch.objcons.log.Log.logDesignerInfo("SABAExport SABA weeks completed");
				
					// we are called from the scheduler
				
					//command = constsBox.getMDValue(this.getRequest(), new EValueID("Consts_SABAAuslieferungAuto"));
					
						// we create the output for SABA
						outFile = new File(BOFileUpload.Config.getInstance().getTempBaseDir(), "bestatter.txt");
						if (outFile.exists()) {
							outFile.delete();
						}
						boProt.setAttribute("Ziel","SABA",boContext,null);
						
						ch.objcons.log.Log.logDesignerInfo("SABAExport open file :"+outFile);
						outFile.createNewFile();
						outStream = new BufferedOutputStream(new FileOutputStream(outFile));
						ch.objcons.log.Log.logDesignerInfo("fill table");
						this.fillSABATable(outStream);
						outStream.flush();
						outStream.close();
						//zweite file
						
						outFile2 = new File(BOFileUpload.Config.getInstance().getTempBaseDir(), "agenda.txt");
						if(outFile2.exists()){
							outFile2.delete();
						}
						boProt.setAttribute("Ziel", "SABA", boContext, null);
						
						ch.objcons.log.Log.logDesignerInfo("SABAExport open file :"+outFile2);
						outFile2.createNewFile();
						outStream2 = new BufferedOutputStream(new FileOutputStream(outFile2));
						ch.objcons.log.Log.logDesignerInfo("fill table");
						this.fillSABATable2(outStream2);
						outStream2.flush();
						outStream2.close();
						
						
						boProt.setAttribute("Return","OK",boContext,null);
						boProt.setAttribute("Meldung","Erstellung erfolgreich",boContext,null);
						ch.objcons.log.Log.logDesignerInfo("SABAExport successful");
					} catch (IOException ioe) {
						ch.objcons.log.Log.logDesignerError("SABAExport I/O Exception",ioe);
						boProt.setAttribute("Return","ER",boContext,null);
						boProt.setAttribute("Meldung","I/O Exception: "+ioe.getMessage(),boContext,null);
					} catch (SQLException sqle) {
						ch.objcons.log.Log.logDesignerError("SABAExport SQL Exception",sqle);
						boProt.setAttribute("Return","ER",boContext,null);
						boProt.setAttribute("Meldung","SQL Exception: "+sqle.getMessage(),boContext,null);
					} catch (Exception e) {
						ch.objcons.log.Log.logDesignerError("SABAExport unknown Exception",e);
						boProt.setAttribute("Return","ER",boContext,null);
						boProt.setAttribute("Meldung","Unexpected Exception: "+e.getMessage(),boContext,null);
					} finally {
						try {
							trans.commit();
						}
						catch (XBOConcurrencyConflict e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
			}
			else {
				// we are called direct, so we don't fill the table new
				//command = constsBox.getMDValue(this.getRequest(), new EValueID("Consts_SABAAuslieferungHand"));
				ch.objcons.log.Log.logDesignerInfo("SABAExport manually");
				outFile = new File(BOFileUpload.Config.getInstance().getTempBaseDir(), "personen.txt");
				outFile2 = new File(BOFileUpload.Config.getInstance().getTempBaseDir(), "zuordnung.txt");
				if (!outFile.exists()) {
					return;
				}else {
					try {
						inStream = new FileInputStream(outFile);
		
						// content type zur�ckgeben
		
						result = new EContent("sap0sa.txt", "txt" , inStream );
						this.setContent(result);
					} catch(IOException e) {
						System.out.println("Can't read SABA file "+outFile.getName());
					}
				}
				
				if(!outFile2.exists()){
					return;
				}else{
					try{
						inStream2 = new FileInputStream(outFile2);
						result2 = new EContent("file2test.txt", "txt", inStream2);
						this.setContent(result2);
					}catch (IOException e) {
						System.out.println("Can't read SABA file "+outFile2.getName());
					}
				}
			}
	}

	/** Bestatter
	 * fills out the Table for SABA
	SAGEMECDSA TEXT IS 'Kirchgemeinde' ,
	SAPFKRCDSA TEXT IS 'Pfarrkreis' ,
	SALAUTAGSA TEXT IS 'Datum JJJJMMTT' ,
	SATAGBEZSA TEXT IS 'Tagesbezeichnung' ,
	SAHALDEFSA TEXT IS 'HalbtSA Definition' ,
	SAHALDEFSA TEXT IS 'Halbtag Disposition' ,
	SAANREDESA TEXT IS 'Anrede' ,
	SAADRES1SA TEXT IS 'Adresszeile 1' ,
	SAADRES2SA TEXT IS 'Adresszeile 2' ,
	SAADRES3SA TEXT IS 'Adresszeile 3' ,
	SAADRES4SA TEXT IS 'Adresszeile 4' ,
	SAADRES5SA TEXT IS 'Adresszeile 5' ,
	SATEFON1SA TEXT IS 'Telefon 1' ,
	SATEFON2SA TEXT IS 'Telefon 2' ,
	SATEFAX1SA TEXT IS 'Telefax 1' ,
	SATEFAX2SA TEXT IS 'Telefax 2' ,
	SABEMTEXSA TEXT IS 'Bemerkungs-Text'
	 */
	private void fillSABATable(OutputStream outStream) throws XMetaModelQueryException, SQLException, IOException {
		Connection conn = null ;
		ResultSet queryResult = null;
		// my Strings
		String query;
		StringBuffer oneLine;
		String id, kgid, aktiv, anrede, name, zusatz1;
		String adr1_str, adr1_strnr, adr2_fach, adr3_plz, adr4_ort, land;
		String tel1, tel2, fax1, fax2;
		String eMail1, eMail2, eMail3, eMail4, eMail5, eMail6, bem;
		Object myObj;
		Integer communicationMethod = -1;

		try {
			conn = DBConnectionPool.instance().getConnection();

			// mysql query :
			query="select pers.OID, pers.bx_id, pers.bx_oidkg, pers.bx_aktiv, pers.bx_anrede, " +
				  "concat_ws(' ', pers.bx_name, pers.bx_vorname), pers.bx_zusatz1, pers.bx_strassepostfach, " +
				  "pers.bx_plzort, pers.bx_tel1, pers.bx_tel2, pers.bx_fax1, " +
				  "pers.bx_fax2, pers.bx_eMail, pers.bx_eMailCC1, pers.bx_eMailCC2, " +
				  "pers.bx_eMailCC3, pers.bx_Bemerkung, pers.bx_strassenr, pers.SABAPfarrer_communicationMethod "+
				  "from bx_sabaperson as pers "+
				  "where pers.bx_aktiv = 'j' "+
				  "and pers.bx_pfarrer = 'j' " +
				  "and pers.bx_oidkg <> 1001 " +
				  "order by pers.oid asc, pers.bx_oidkg asc";
			
			long logID = 0;
		    if (ch.objcons.ecom.query.Config.getInstance().showQueryString()) {
				logID = ch.objcons.log.Log.logDesignerInfo("Query: ActionSABAExport.fillSABA: " + query);
				ch.objcons.log.Log.flushDesigner();
			}

			Statement querySTM = conn.createStatement();
			queryResult =  querySTM.executeQuery(query);

			if (ch.objcons.ecom.query.Config.getInstance().getSQLTraceLevel() > 0) {
				ch.objcons.log.Log.logDesignerInfoPS(logID, "");
			}
			
			while (queryResult.next()) {
				//System.out.println("Result available");
				boolean gesperrt = false;
				
				myObj = queryResult.getObject(1);
				String oid = (myObj == null) ? "" : myObj.toString();

				myObj = queryResult.getObject(2);
				id = (myObj == null)?"":myObj.toString();

				myObj = queryResult.getObject(3);
				kgid = (myObj == null)?"":myObj.toString();

				int idLength = id.length();
				int kgidLength = kgid.length();

				if (idLength != 8) {
					if (idLength == 0) {
						id = oid;
						idLength = id.length();
						if (idLength > 3) continue;
					}
					if (idLength == 1) {
						id = "0000" + id;
					}
					else if (idLength == 2) {
						id = "000" + id;
					}
					else if (idLength == 3) {
						id = "00" + id;
					}
					else if (idLength == 4) {
						id = "0" + id;
					}					
					else {
						id = "00000";
					}
					if (kgidLength == 1) {
						kgid = "00" + kgid;
					}
					else if (kgidLength == 2) {
						kgid = "0" + kgid;
					}
					else {
						kgid = "000";
					}
					id = id + kgid;
				}

				myObj = queryResult.getObject(4);
				aktiv = (myObj == null)?"":myObj.toString();
				if(aktiv.equals("n")){
					aktiv="N";
				}else if(aktiv.equals("j")){
					aktiv="J";
				}
				
				myObj = queryResult.getObject(5);
				anrede = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(6);
				name = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(7);
				zusatz1 = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(8);
				adr1_str = (myObj == null)?"":myObj.toString();
				
//				adr2_fach ="aaa";
				
				myObj = queryResult.getObject(9);
				String plzort = (myObj == null)?"":myObj.toString();
				adr3_plz ="";
				adr4_ort ="";
				if(!plzort.equals("")){
					int index = plzort.indexOf(' ');
					if (index!=-1) {
						adr3_plz=plzort.substring(0,index);
						adr4_ort = plzort.substring(index+1);
					} else {
						adr4_ort = plzort;
					}
					
				}
				
				land = "Schweiz";
				
				myObj = queryResult.getObject(10);
				tel1 = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(11);
				tel2 = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(12);
				fax1 = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(13);
				fax2 = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(14);
				eMail1 = (myObj == null)?"":myObj.toString();
				
				eMail2 = ""; // "kirchgemeinde@kg.ch";
				
				eMail3 = ""; // "edv_kg@edv.ch";
				
				myObj = queryResult.getObject(15);
				eMail4 = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(16);
				eMail5 = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(17);
				eMail6 = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(18);
				bem = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(19);
				adr1_strnr = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(20);
				communicationMethod = (myObj == null) ? 0 :Integer.parseInt(myObj.toString());
				
				if(communicationMethod != 2){ //wenn nicht email als Kommunikationsart
					eMail1 = ""; eMail2 = ""; eMail3 = ""; eMail4 = ""; eMail5 = ""; eMail6 = "";  
				}
				
				oneLine = new StringBuffer(1165);
//				oneLine.append(fixWide(id+kgid,8));
				oneLine.append(fixWide(id,8));
				oneLine.append(fixWide(aktiv,1));
				oneLine.append(fixWide(anrede,64));
				oneLine.append(fixWide(name,64));
				oneLine.append(fixWide(zusatz1,30));
				oneLine.append(fixWide(adr1_str,64));
				oneLine.append(fixWide(adr1_strnr,5));
				oneLine.append(fixWide(adr3_plz,6));
				oneLine.append(fixWide(adr4_ort,30));
				oneLine.append(fixWide(land,30));
				oneLine.append(fixWide(tel1,32));
				oneLine.append(fixWide(tel2,32));
				oneLine.append(fixWide(fax1,32));
				oneLine.append(fixWide(fax2,32));
				oneLine.append(fixWide(eMail1,80));
				oneLine.append(fixWide(eMail2,80));
				oneLine.append(fixWide(eMail3,80));
				oneLine.append(fixWide(eMail4,80));
				oneLine.append(fixWide(eMail5,80));
				oneLine.append(fixWide(eMail6,80));
				oneLine.append(fixWide(bem,255));
								
				if (!queryResult.isLast()) oneLine.append("\r\n");
				outStream.write(oneLine.toString().getBytes());
				
			}
//			System.out.println("katholisch: " + _numOfDuplicates + " Dupletten in Abfrage eliminiert");
			
		} catch (UnsupportedEncodingException use) {
			ch.objcons.log.Log.logDesignerError("SABAExport fillSABATable UnsupportedEncoding Exception",use);
		} catch (Exception e) {
			ch.objcons.log.Log.logDesignerError("SABAExport fillSABATable Exception",e);
		}
		
		finally {
			try {
				if (queryResult != null) {
					queryResult.close();
				}
				if (conn != null) {
					DBConnectionPool.instance().ungetConnection(conn);
				}
			}
			catch (Exception e) {
				ch.objcons.log.Log.logSystemAlarm("Unexpected Exception", e);
			}
		}
	}	
	/**
	 * Agenda
	 */
	private void fillSABATable2(OutputStream outStream2) {
		Connection conn = null ;
		ResultSet queryResult = null;
		// my Strings
		String query;
		StringBuffer oneLine;
		StringBuffer twoLine;
		String kg, datum, vtag, vtag1, id, kgid, htdisp;
		String bem;
		Object myObj;

		try {
			conn = DBConnectionPool.instance().getConnection();

			// mysql query :
			query = " select pers.bx_gesperrt, zu.bx_oidkg, zu.bx_datum, zu.bx_vierteltag, " +
					"pers.oid, pers.bx_oidkg, "+
					"bem.bx_name, pers.bx_id from bx_sabazuordnung as zu " +
					"left join bx_sabaperson as pers " +
					"on zu.bx_oidsabapers = pers.oid " +
					"left join bx_bemerkung as bem " +
					"on zu.bx_oidbemerkung = bem.oid " +
					"left join bx_kirchgemeinde as kg " +
					"on zu.bx_oidkg = kg.oid " +
					"where zu.bx_datum >=" + SQLFormat.dateToSQLvalue(new java.sql.Date(System.currentTimeMillis())) + " "+
					"and zu.bx_datum <="+ SQLFormat.dateToSQLvalue(new java.sql.Date(System.currentTimeMillis()+6L*7*24*60*60*1000)) +" "+
					"and kg.oid < 1001 " +
					"order by zu.bx_oidkg asc, zu.bx_datum asc, zu.bx_vierteltag asc";
			
			long logID = 0;
		    if (ch.objcons.ecom.query.Config.getInstance().showQueryString()) {
				logID = ch.objcons.log.Log.logDesignerInfo("Query: ActionSABAExport.fillSABA: " + query);
				ch.objcons.log.Log.flushDesigner();
			}

			Statement querySTM = conn.createStatement();
			queryResult =  querySTM.executeQuery(query);

			if (ch.objcons.ecom.query.Config.getInstance().getSQLTraceLevel() > 0) {
				ch.objcons.log.Log.logDesignerInfoPS(logID, "");
			}
			//TreeSet s = new TreeSet();
			while (queryResult.next()) {
				//System.out.println("Result available");
				boolean gesperrt = false;
				
				myObj = queryResult.getObject(1);
				htdisp="G";
				if (myObj!=null) {
					if (myObj.toString().equals("j")) {
						gesperrt = true;
						htdisp = "G";
					} else {
						gesperrt = false;
						htdisp = "P";
					}
				}
				else {
					gesperrt = true;
				}
				
				myObj = queryResult.getObject(2);
				kg = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(3);
				String tdatum = (myObj == null)?"":myObj.toString();
				datum="";
				if (!tdatum.equals("")) {
					StringTokenizer stt = new StringTokenizer(tdatum,"-");
					while (stt.hasMoreTokens()) {
						datum = datum.concat(stt.nextToken());
					}
				}
				
				myObj = queryResult.getObject(4);
				String viertel = (myObj == null)?"":myObj.toString();
				vtag=viertel.trim();
				/*vtag1 = "";
				if(viertel.equals("V")){
					vtag = "1"; 
					vtag1 = "2";
				}else if(viertel.equals("N")){
					vtag = "3";
					vtag1 = "4";
				}
				*/
				myObj = queryResult.getObject(5);
				String oid = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(6);
				kgid = (myObj == null)?"":myObj.toString();
				String origKGID = kgid;
/*
				if ((origKGID == null) || (origKGID.length() == 0)) {
					origKGID = "000";
				}
*/				
				myObj = queryResult.getObject(7);
				bem = (myObj == null)?"":myObj.toString();
				
				myObj = queryResult.getObject(8);
				id = (myObj == null)?"":myObj.toString();
				
				int idLength = id.length();
				int kgidLength = kgid.length();

				if (idLength != 8) {
					if (idLength == 0) {
						id = oid;
						idLength = id.length();
						if (idLength > 3) continue;
					}
					if (idLength == 1) {
						id = "0000" + id;
					}
					else if (idLength == 2) {
						id = "000" + id;
					}
					else if (idLength == 3) {
						id = "00" + id;
					}
					else if (idLength == 4) {
						id = "0" + id;
					}					
					else {
						id = "00000";
					}
					if (kgidLength == 1) {
						kgid = "00" + kgid;
					}
					else if (kgidLength == 2) {
						kgid = "0" + kgid;
					}
					else {
						kgid = "000";
					}
					id = id + kgid;
				}
				if (gesperrt) {
					String myKG = kg;
					if (myKG == null) continue;
					switch (myKG.length()) {
					case 0:
						continue;
					case 1:
						myKG = "00" + myKG;
						break;
					case 2:
						myKG = "0" + myKG;
						break;
					case 3:
						break;
					default:
						continue;
					}
					id = "00000" + "000"; // + myKG;   2005-06-28
				}
				/*
				String linekey = kg + "�" + datum + "�" + vtag;
				if (s.contains(linekey)) {
					Log.logDesignerError("saba agenda: " + linekey + " kommt doppelt vor");
					continue;
				}
				s.add(linekey);
				*/
				oneLine = new StringBuffer(275);
				oneLine.append(fixWide(kg,3));
				oneLine.append(fixWide(datum,8));
				oneLine.append(fixWide(vtag,1));
//				oneLine.append(fixWide(persid+kgid,8));
				oneLine.append(fixWide(id,8));
				oneLine.append(fixWide(bem,255));
				
				/*
				twoLine = new StringBuffer(275);
				twoLine.append(fixWide(kg,3));
				twoLine.append(fixWide(datum,8));
				twoLine.append(fixWide(vtag1,1));
				twoLine.append(fixWide(id,8));
				twoLine.append(fixWide(bem,255));
				*/
				if (!queryResult.isLast()) oneLine.append("\r\n");
				//if (!queryResult.isLast()) twoLine.append("\r\n");
				outStream2.write(oneLine.toString().getBytes());
				//outStream2.write(twoLine.toString().getBytes());
				
			}
//			System.out.println("katholisch: " + _numOfDuplicates + " Dupletten in Abfrage eliminiert");
		
		} catch (UnsupportedEncodingException use) {
			ch.objcons.log.Log.logDesignerError("SABAExport fillSABATable UnsupportedEncoding Exception",use);
		} catch (Exception e) {
			ch.objcons.log.Log.logDesignerError("SABAExport fillSABATable Exception",e);
		}
		finally {
			try {
				if (queryResult != null) {
					queryResult.close();
				}
				if (conn != null) {
					DBConnectionPool.instance().ungetConnection(conn);
				}
			}
			catch (Exception e) {
				ch.objcons.log.Log.logSystemAlarm("Unexpected Exception", e);
			}
		}
	}
}
