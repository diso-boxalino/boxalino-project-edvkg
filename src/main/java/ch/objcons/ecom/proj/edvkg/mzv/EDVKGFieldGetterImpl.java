package ch.objcons.ecom.proj.edvkg.mzv;

import static ch.objcons.ecom.proj.edvkg.mzv.Consts.ZusOIDStrasseIndex;

import java.sql.SQLException;

import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.UtilsEDVKG.EDVKGFieldGetter;
import ch.objcons.ecom.system.security.XSecurityException;

public class EDVKGFieldGetterImpl implements EDVKGFieldGetter {

	
	private BOObject personObj;
	
	private int[] map;
	
	private String zusArt;
	
	private BOContext context;
	
	public EDVKGFieldGetterImpl(BOObject personObj, int[] map, String zusArt, BOContext context) {
		this.personObj = personObj;
		this.map = map;
		this.zusArt = zusArt;
		this.context = context;
	}
	
	@Override
	public Object getField(int index) throws XMetaModelQueryException, XSecurityException {
		int finalIndex = map[index];
		if (index == ZusOIDStrasseIndex) {
			return personObj.getReferencedOID(finalIndex);
		} else {
			if (personObj instanceof BOPersonHistory && index == Consts.Hist_VorfallBitsIndex) {
				finalIndex = index; // Feld, das nur auf PersonHistory vorkommt
			}
			return personObj.getAttribute(finalIndex, true, context);
		}
	}

	@Override
	public Object getField(int derefIndex, int targetIndex) throws XMetaModelQueryException, XSecurityException {
		int finalIndex = map[derefIndex];
		BOObject boAPersonObj = personObj.getReferencedObject(finalIndex, false, context);
		if (boAPersonObj == null) {
			return null;
		}
		finalIndex = map[targetIndex];
		if (targetIndex == ZusOIDStrasseIndex) {
			return boAPersonObj.getReferencedOID(finalIndex);
		} else {
			return boAPersonObj.getAttribute(finalIndex, true, context);						
		}
	}

	@Override
	public Object getField(String attrPath)
			throws XMetaModelQueryException, XSecurityException {

		return attrPath;
	}

	@Override
	public String getZusArt() {
		return zusArt;
	}

	@Override
	public void setZusart(String zusArt) {
		// not in use
	}

	@Override
	public Object getObject(int index) throws XMetaModelQueryException,
			XSecurityException, SQLException {
		int finalIndex = map[index];
		if (personObj instanceof BOPersonHistory && 
				index == Consts.Hist_DateIndex ||
				index == Consts.Hist_VorfallBitsIndex) {
			finalIndex = index; // Feld, das nur auf PersonHistory vorkommt
		}
		return personObj.getValue(finalIndex);
	}

	@Override
	public Object getList(int index) throws XMetaModelQueryException,
			XSecurityException, SQLException {
		int finalIndex = index == Consts.Hist_meldungenIndex /* Feld, das nur auf PersonHistory vorkommt */ ? index : map[index];
		return personObj.getListReferencedObjects(finalIndex, context);
	}
	
	@Override
	public BOObject getBO() {
		return personObj;
	}

	@Override
	public void setBO(BOPersonHistory bo) {
		personObj = bo;
	}
	
}
