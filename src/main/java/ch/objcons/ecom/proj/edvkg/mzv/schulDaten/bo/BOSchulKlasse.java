package ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.datatypes.EDataTypeValidation;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.server.Request;

public class BOSchulKlasse extends BOObject {

	public static final String CLASS_NAME = "SchulKlasse";
	
	public BOSchulKlasse(BLTransaction arg0, long arg1) {
		super(arg0, arg1);
	}
	
	@Override
	public String getAttribute(String attrName, boolean useAccessControl, BOContext context) throws XMetaModelQueryException, XSecurityException {
		if ("name".equals(attrName)) {
			String name = attr("name");
			if (!EDataTypeValidation.isNullOrEmptyString(name)) return name;
			
			return getSchulStufenName() + " " + attr("id");
		} else if ("lp".equals(attrName)) {
			return attr("lpAnrede") + " " + attr("lpVorname") + " " + attr("lpName");
		}
		return super.getAttribute(attrName, useAccessControl, context);
	}

	private String getSchulStufenName() throws XMetaModelQueryException, XSecurityException {
		BOContext context = getContext();
		BOSchulStufe boSchulStufe = getSchulStufe(context);
		if (boSchulStufe == null) return "Stufe unbek.";
		
		return boSchulStufe.getAttribute("name", context);
	}
	
	public BOSchulStufe getSchulStufe(BOContext context) throws XMetaModelQueryException, XSecurityException {
		return (BOSchulStufe) getReferencedObject("SchulStufeschulKlassenOID", context);
	}
	
	public String attr(String name) {
		try {
			return super.getAttribute(name, true, getContext());
		} catch (XMetaModelQueryException e) {
			throw new RuntimeException(e);
		} catch (XSecurityException e) {
			throw new RuntimeException(e);
		}
	}
	
	private BOContext getContext() {
		BLTransaction trx = getTransaction();
		if (trx != null) return trx.getContext();
		
		return (BOContext) Request.getCurrentOrDummy().getContext();
	}
	
}
