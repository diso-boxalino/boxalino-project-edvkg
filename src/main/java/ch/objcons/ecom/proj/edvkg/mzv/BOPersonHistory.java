package ch.objcons.ecom.proj.edvkg.mzv;

import java.util.EnumSet;
import java.util.Set;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.utils.BOMQuery;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.Consts.Vorfall;
import ch.objcons.ecom.proj.edvkg.mzv.imp.BOMeldung;
import ch.objcons.ecom.proj.edvkg.mzv.imp.Meldungsvorlage;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;
import com.boxalino.server.Request;

public class BOPersonHistory extends BOObject {

	public static final String CLASS_NAME = "PersonHistory";
	
	static ILogger LOGGER = Logger.getLogger("boxalino.edvkg.person.bo");
	
	public BOPersonHistory(BLTransaction trans, long oid) {
		super(trans, oid);
	}

	@Override
	public String getAttribute(int id, boolean useAccessControl, BOContext context) 
		throws XMetaModelQueryException, XSecurityException {

		if (id == Consts.Hist_VorstandIndex) {
			Number oidFam = (Number) getValue(Consts.Hist_OIDFamilieIndex);
			return Boolean.toString(oidFam != null && oidFam.longValue() == getOID());
		} else {
			return super.getAttribute(id, useAccessControl, context);
		}
	}
	
	public void addMeldung(Meldungsvorlage mv, String... arguments) throws XSecurityException, XMetaException, XDataTypeException {
		String argString = BOMeldung.getArgString(arguments);
		BOObject boMeldung = _trans.getObject(Consts.MeldungMeta, 0);
		boMeldung.setAttribute(Consts.Meldung_MeldungsvorlagemeldungenOID, String.valueOf(mv.getOID()), _trans.getContext());
		boMeldung.setAttribute(Consts.Meldung_arguments, argString, _trans.getContext());
		addListReferencedObject(Consts.Hist_meldungenIndex, boMeldung);
		boMeldung.setReferencedObject(Consts.Meldung_PersonHistorymeldungenOID, this);
	}
	
	/**
	 * Checks, whether this PersonHistory object is active based on statusPerson.
	 * @return true, if statusPerson is true
	 */
	public boolean isActiveSP(BOContext context) {
		try {
			return Boolean.TRUE.equals(getValue("StatusPerson", context));
		} catch (XMetaModelNotFoundException e) {
			throw new RuntimeException(e);
		} catch (XSecurityException e) {
			throw new RuntimeException(e);
		}
	}
	
	public boolean isRef/*OrKath*/(BOContext context) {
		try {
			long konfession = getReferencedOID("OIDKonfession", context);
			return konfession == 1/* || konfession == 2*/;
		} catch (XMetaModelNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public static BOPersonHistory nextEntry(BOPersonHistory bo) {
		try {
			BOContext context = (BOContext) Request.getCurrentOrDummy().getContext();
			long oidPerson = bo.getReferencedOID("OIDPerson", context);
			String[] match = { "OIDPerson == " + oidPerson, "OID > " + bo.getOID() }; 
			return BOMQuery.of(CLASS_NAME).match(match).order("OID_asc").first();
		} catch (XMetaModelNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	public int getVorfallBits() {
		try {
			Integer bits = (Integer) getValue(Consts.Hist_VorfallBitsIndex);
			return bits != null ? bits : 0;
		} catch (XMetaModelNotFoundException e) {
			throw new RuntimeException(e);
		} catch (XSecurityException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Set<Vorfall> getVorfaelle() {
		int bits = getVorfallBits();
		EnumSet<Vorfall> vorfaelle = EnumSet.noneOf(Vorfall.class);
		for (Vorfall v : Vorfall.values()) {
			if ((v.getBitMask() & bits) != 0) vorfaelle.add(v);
		}
		return vorfaelle;
	}
	
	public boolean hasVorfall(Vorfall v) {
		return (getVorfallBits() & v.getBitMask()) != 0;
	}
	
	public Vorfall getVorfall(Vorfall... vorfaelle) {
		for (Vorfall v : vorfaelle) {
			if (hasVorfall(v)) return v;
		}
		return null;
	}
	
}
