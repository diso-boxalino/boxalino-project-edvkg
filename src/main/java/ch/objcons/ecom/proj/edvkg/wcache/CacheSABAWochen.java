/*
 * Created on 24.01.2005
 *
 * (c) by Boxalino
 */
package ch.objcons.ecom.proj.edvkg.wcache;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import ch.objcons.ecom.api.EValueID;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.engine.EEngine;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.shop.date.DateEService;
import ch.objcons.log.Log;

/**
 * @author Dominik Raymann
 */
public class CacheSABAWochen implements Runnable {
	
	private static CacheSABAWochen _instance = null;
	
	private BOContext _boContext = null;
	//private BOContext _boContext = new BOContext(Locale.getDefault());
	
	final private int _cacheSize = 20;
	private Hashtable _cache = new Hashtable(_cacheSize);
	
	
	synchronized private Hashtable getWoche(String woche, String jahr) throws CacheException{
		Hashtable dieseWoche = (Hashtable)_cache.get(jahr+"/"+woche);
		//dieseWoche = null;
		if (dieseWoche==null) {
			if (_cache.size()>= _cacheSize) {
				_cache.remove(_cache.keys().nextElement());
			}
			
			dieseWoche = new Hashtable(2);
			dieseWoche.put("bestatter", new Hashtable());
			_cache.put(jahr+"/"+woche,dieseWoche);
			dieseWoche.put("kg", getAllKGWochen(woche,jahr));
			
			
		}
		return dieseWoche;
	}
	
	
	
	public BestatterWoche getBestatterWoche(String woche, String jahr, String oid) throws CacheException{
		Hashtable dieseWoche = getWoche(woche,jahr);
		Hashtable bestatterWochen = (Hashtable)dieseWoche.get("bestatter");
		BestatterWoche bestatterWoche = (BestatterWoche)(bestatterWochen.get(oid));
		if (bestatterWoche == null) {
			bestatterWoche = new BestatterWoche(woche,jahr,oid,_boContext);
			bestatterWochen.put(oid,bestatterWoche);
		}
		return bestatterWoche;
	}
	
	public KGWoche getKGWoche(String woche, String jahr, String oid)  throws CacheException{
		Hashtable dieseWoche = getWoche(woche,jahr);
		Hashtable kgWochen = (Hashtable)dieseWoche.get("kg");
		KGWoche kgWoche = (KGWoche)(kgWochen.get(oid));
		if (kgWoche == null) {
			kgWoche = new KGWoche(woche,jahr,oid,_boContext);
			// wirklich initPhase2 durchlaufen?
			kgWoche.initPhase2();
			kgWochen.put(oid,kgWoche);
		}
		return kgWoche;
		
	}
	
	
	private CacheSABAWochen(BOContext boContext) {
		super();
		_boContext = boContext;
	}
	
	public static CacheSABAWochen getInstance(BOContext boContext) {
		
		if (_instance == null) {
			_instance = new CacheSABAWochen(boContext);
		}
		
		return _instance;
	}
	
	synchronized private Hashtable getAllKGWochen(String woche, String jahr) throws CacheException{
		Hashtable kgWochen = new Hashtable();
		try {
			BLTransaction trans = BLTransaction.startTransaction(_boContext);
			MetaObject moKG = MetaObjectLookup.getInstance().getMetaObject("Kirchgemeinde");
			Vector v = BLManager.instance().getExtent(moKG).getResult(trans,_boContext);
			//Vector v = BoxalinoUtilities.executeQuery("Kirchgemeinde",null,trans,_boContext);
			long start = System.currentTimeMillis();
			for (int i = 0; i<v.size();i++) {
				BOObject bokg =(BOObject)v.elementAt(i);
				String oid = ""+bokg.getOID();
				kgWochen.put(oid,new KGWoche(woche,jahr,oid,_boContext));
			}
			long split = System.currentTimeMillis();
			for (Iterator it = kgWochen.values().iterator();it.hasNext();) {
				KGWoche kgWoche =(KGWoche)it.next();
				kgWoche.initPhase2();
				
			}
			long end = System.currentTimeMillis();
			/*
			System.out.println("init Phase 1 ("+woche+", "+jahr+") = "+(split-start)+"ms");
			System.out.println("init Phase 2 ("+woche+", "+jahr+") = "+(end-split)+"ms");
			*/
		} catch (Exception e) {
			throw new CacheException("ERROR - konnte Wochen der Kirchgemeinden nicht einlesen: "+e.getMessage());
		}
		return kgWochen;
		
	}
	
	public Vector getVerfuegbareBestatter(String kgoid, int tag, int vierteltag, String woche, String jahr) throws CacheException{
		KGWoche kgWoche = getKGWoche(woche,jahr,kgoid);
		return kgWoche.getVerfuegbareBestatter(tag, vierteltag);
	}
	
	public Vector getVerfuegbareBestatterGanzeWoche(String kgoid, String woche, String jahr) throws CacheException {
		KGWoche kgWoche = getKGWoche(woche,jahr,kgoid);
		return kgWoche.getVerfuegbareBestatterGanzeWoche();
	}
	
	public boolean isBestatterVerfuegbar(String kgoid, int tag, int vierteltag, String woche, String jahr, String persoid) throws CacheException{
		Vector v = getVerfuegbareBestatter(kgoid, tag, vierteltag,woche,jahr);
		for (Iterator it = v.iterator();it.hasNext();) {
			BestatterWoche bestatter = (BestatterWoche)it.next();
			if (bestatter.getOID().equalsIgnoreCase(persoid)) return true;
		}
		return false;
		
	}
	
	public IBestatter getGebuchterBestatter(String kgoid, int tag, int vierteltag, String woche, String jahr) throws CacheException{
		KGWoche kgWoche = getKGWoche(woche,jahr,kgoid);
		return kgWoche.getGebuchterBestatter(tag, vierteltag);
	}
	
	public String getStandardBemerkung(String kgoid, int tag, int vierteltag, String woche, String jahr) throws CacheException{
		KGWoche kgWoche = getKGWoche(woche,jahr,kgoid);
		return kgWoche.getStandardBemerkung(tag, vierteltag);
	}
	public String getBemerkung(String kgoid, int tag, int vierteltag, String woche, String jahr) throws CacheException {
		KGWoche kgWoche = getKGWoche(woche,jahr,kgoid);
		return kgWoche.getBemerkung(tag, vierteltag);
	}
	
	public void clearCache() {
		_cache.clear();
	}
	
	public void clearCachedWeek(String woche, String jahr) {
		_cache.remove(jahr+"/"+woche);
		
	}
	
	public boolean isEditierbar(IRequest request, String kgoid, int tag, int vierteltag, String woche, String jahr) throws CacheException{
		GregorianCalendar cal = new GregorianCalendar();
		cal.set(Calendar.DAY_OF_WEEK, AgendaWoche.CALENDARDAYS[tag]);
		cal.set(Calendar.WEEK_OF_YEAR, Integer.parseInt(woche));
		cal.set(Calendar.YEAR, Integer.parseInt(jahr));
		
		Date heute = new Date();
		if ((cal.getTime().getTime()-heute.getTime()) > 2L*7*24*60*60*1000) {
			return true;
		}
		
		String val = "";
		try {
			val = EEngine.getInstance().getValue(request, "login", new EValueID("Administrator"));
		}
		catch (Exception x) {
			Log.logSystemError("konnte nicht herausfinden, ob User ein Admin ist", x);
			return false;
		}
		if ((val != null) && (val.equals("true"))) return true;
		return false;
		
		
	}
	
	public boolean isBestatterGebucht(String persoid, int tag, int vierteltag, String woche, String jahr) throws CacheException {
		BestatterWoche bwoche  = getBestatterWoche(woche,jahr,persoid);
		KGWoche kwoche = bwoche.getBuchung(tag,vierteltag);
		if (kwoche == null) return false;
		return true;
	}
	
	public IKirchgemeinde getBuchendeKirchgemeinde(String persoid, int tag, int vierteltag, String woche, String jahr) throws CacheException {
		BestatterWoche bwoche  = getBestatterWoche(woche,jahr,persoid);
		return bwoche.getBuchendeKirchgemeinde(tag,vierteltag);
	}
	
	public void run() {
		Log.logDesignerInfo("refreshing cache");
		clearCache();
		GregorianCalendar cal = new GregorianCalendar();
		try {
			for (int i=0; i<12; i++) {
				int actweek = cal.get(Calendar.WEEK_OF_YEAR);
				int actyear = DateEService.getYearOfWeek(cal);
				getWoche(""+actweek,""+actyear);
				cal.add(Calendar.WEEK_OF_YEAR,1);
			}
			Log.logDesignerInfo("cache refreshed");
		} catch (CacheException cex) {
			Log.logDesignerError("couldn't refresh cache",cex);
		}
	}
}
