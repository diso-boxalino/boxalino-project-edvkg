package ch.objcons.ecom.proj.edvkg.tests;

import java.sql.Connection;

import junit.framework.TestCase;
import ch.objcons.db.DBConnectionPool;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.engine.EEngine;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.proj.edvkg.mzv.ActionExport;
import ch.objcons.ecom.proj.edvkg.mzv.BoxMZV;
import ch.objcons.ecom.shop.servlets.BoxLoader;
import ch.objcons.util.properties.ShopProperties;

import com.boxalino.server.HostServiceLocator;
import com.boxalino.services.ServiceLocator;


public class TestActionExport extends TestCase {

	Connection con = null;
	static BoxMZV boxEdit = null;
	
	static {
		 try {
			 ServiceLocator.init(new HostServiceLocator());
			 ShopProperties.getInstance().setHTMLRoot("file:///C:/Workspace/mzv");
			 BoxLoader.getInstance().initBoxes();
			 boxEdit = (BoxMZV) EEngine.getInstance().getBox("edvkg"); 
		 } catch (Exception e) {
			 throw new RuntimeException(e);
		 }	
	}
	 
	@Override
	protected void setUp() throws Exception {
		con = DBConnectionPool.instance().getConnection();
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		DBConnectionPool.instance().ungetConnection(con);
		con = null;
		super.tearDown();
	}
	
	public void testPostcode() throws XMetaException, XDataTypeException {
		ActionExport action = new ActionExport(boxEdit, BoxalinoUtilities.getDummyRequest(), "edvkg", "", true);
		action.performX();
	}
	
}
