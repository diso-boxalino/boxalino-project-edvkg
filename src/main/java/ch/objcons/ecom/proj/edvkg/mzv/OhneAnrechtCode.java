package ch.objcons.ecom.proj.edvkg.mzv;

public enum OhneAnrechtCode {

	/* Erwartete F�lle */
	KeineRelevanteKonfession (1, "K"),
	Passiv (2, "P"),
	
	/* Fehlerf�lle */
	UnbekannteZIP (99, "U");
	
	private OhneAnrechtWeil _oaw;
	
	private OhneAnrechtCode (int id, String code) {
		_oaw = new OhneAnrechtWeil(id, code);
	}
	
	public OhneAnrechtWeil getOAW() {
		return _oaw;
	}
	
}
