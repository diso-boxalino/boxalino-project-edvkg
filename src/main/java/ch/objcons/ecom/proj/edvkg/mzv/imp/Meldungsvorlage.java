package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.db.DBUtils;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;

public enum Meldungsvorlage {
	// Achtung: Nummer der Meldung nicht �ndern, da sonst Verweise in DB verf�lscht werden
	RefVaterError (1, Meldungsdomain.PostImport, Meldungscode.ZIPRef, Meldungstyp.Fehler, "Vater Record fehlt: %s", true),
	RefMutterError (2, Meldungsdomain.PostImport, Meldungscode.ZIPRef, Meldungstyp.Fehler, "Mutter Record fehlt: %s", true),
	RefPartnerError (3, Meldungsdomain.PostImport, Meldungscode.ZIPRef, Meldungstyp.Fehler, "Partner Record fehlt: %s", true),
	RefOIZFamilieError (4, Meldungsdomain.PostImport, Meldungscode.ZIPRef, Meldungstyp.Fehler, "OIZ Familie Record fehlt: %s", true),
	RefVormundError (21, Meldungsdomain.PostImport, Meldungscode.ZIPRef, Meldungstyp.Fehler, "Vormund Record fehlt: %s", true),
	Alt_ErrorOidHaushalt (5, Meldungsdomain.PostImport, Meldungscode.Alter_Import, Meldungstyp.Fehler, "Error ZIPHaushalt", false),
	Alt_ErrorOidHaushaltNull (6, Meldungsdomain.PostImport, Meldungscode.Alter_Import, Meldungstyp.Fehler, "Error kein ZIPHaushalt", false),
	ZA_ExterneUeberschreibtManuelle (7, Meldungsdomain.Import, Meldungscode.Zustelladresse, Meldungstyp.Warnung, "Ausw�rtige Zustelladr. �berschreibt manuelle", false),
	ZA_StaedtischeUeberschreibtManuelle (8, Meldungsdomain.Import, Meldungscode.Zustelladresse, Meldungstyp.Warnung, "St�dtische Zustelladr. �berschreibt manuelle", false),
	ZA_LoeschenManuelleVerh (9, Meldungsdomain.Import, Meldungscode.Zustelladresse, Meldungstyp.Info, "L�schen manueller Zustelladr. verhindert", false),
	ZA_SQLNormalisierung (10, Meldungsdomain.Import, Meldungscode.Zustelladresse, Meldungstyp.Info, "SQL Normalisierung Zustelladr.", false),
	Umz_OhneAdrAenderung (11, Meldungsdomain.Import, Meldungscode.Umzug, Meldungstyp.Warnung, "Umzug gem�ss Meldeadresse-G�ltig von, aber ohne Adress�nderung", false),
	Umz_OhneMeldeAdrDatum (12, Meldungsdomain.Import, Meldungscode.Umzug, Meldungstyp.Warnung, "Umzug gem�ss Adress�nderung, aber ohne neuem Meldeadresse-G�ltig von", false),
	Pfk_Unbek (13, Meldungsdomain.Import, Meldungscode.Pfarrkreis, Meldungstyp.Fehler, "Pfarrkreis konnte nicht ermittelt werden: '%s'", false),
	Imp_Error (14, Meldungsdomain.Import, Meldungscode.Import, Meldungstyp.Fehler, "Interner Fehler Personen-Record-Import: '%s'", false),
	Fam_AlleinstehendeKinder (15, Meldungsdomain.PostImport, Meldungscode.Familienbildung, Meldungstyp.Warnung, "Alleinstehendes Kind: %s", true),
	Fam_AlleinstehendMitFamilie (16, Meldungsdomain.PostImport, Meldungscode.Familienbildung, Meldungstyp.Warnung, "Obwohl Person alleinstehend ist und keine Kinder vorhanden sind, ist Familie gr�sser als 1 (%s).", false),
	Fam_UnidirektPartnerVerbindung (17, Meldungsdomain.PostImport, Meldungscode.Familienbildung, Meldungstyp.Warnung, "Es ist nur eine unidirektionale Partnerverbindung vorhanden (%s,%s)", true),
	Fam_UngleichePartner (22, Meldungsdomain.PostImport, Meldungscode.Familienbildung, Meldungstyp.Warnung, "Die Partner zeigen nicht aufeinander (%s,%s)", true),
	Fam_AziKindGemaessMutter (18, Meldungsdomain.PostImport, Meldungscode.Familienbildung, Meldungstyp.Info, "Der Adresszusatzindex vom Kind wird nach demjenigen der Mutter gesetzt (%s)", false), 
	Anf_ManOhneAnrecht (19, Meldungsdomain.Import, Meldungscode.Anforderungen, Meldungstyp.Info, "Kein Anrecht f�r manuelle Anforderung (%s)", false),
	Anf_KeineLieferung (20, Meldungsdomain.Import, Meldungscode.Anforderungen, Meldungstyp.Fehler, "Keine Lieferung f�r diese ZIP ", false),
	PsvVaterError (23, Meldungsdomain.PostImport, Meldungscode.ZIPRef, Meldungstyp.Fehler, "Vater Record passiv (Kind allein, aktiv): %s", true),
	PsvMutterError (24, Meldungsdomain.PostImport, Meldungscode.ZIPRef, Meldungstyp.Fehler, "Mutter Record passiv (Kind allein, aktiv): %s", true),
	;

	private Meldungsdomain _domain;
	private Meldungscode _code;
	private Meldungstyp _type;
	private String _text;
	private int _nr;
	private boolean _anforderungNoetig;
	
	private Meldungsvorlage(int nr, Meldungsdomain domain, Meldungscode code, Meldungstyp type, String text, boolean anforderungNoetig) {
		_nr = nr;
		_code = code;
		_type = type;
		_domain = domain;
		_text = text;
		_anforderungNoetig = anforderungNoetig;
	}

	public long getOID() {
		return _nr;
	}
	
	public static void writeVorlagenToDB (BOContext context) throws SQLException, XMetaModelNotFoundException {
		Set<Integer> usedOIDs = new HashSet<Integer>();
		for (Meldungsvorlage mv : values()) {
			if (!usedOIDs.add(mv._nr)) {
				throw new RuntimeException("MV mit doppelten OIDs");
			}
		}
		
		BLTransaction trans = BLTransaction.startTransaction(context);
		Connection c = null;
		long artoid = MetaObjectLookup.getInstance().getMetaObject("Meldungsvorlage").getOID();
		try {
			c = DBConnectionPool.instance().getConnection();
			Statement stmt = c.createStatement();
			stmt.executeUpdate("delete from bx_meldungsvorlage");
			String sql = "insert into bx_meldungsvorlage " +
				"(oid, artoid, bx_text, bx_type, bx_code, bx_domain) values (?, " + artoid + ", ?, ?, ?, ?)";
			PreparedStatement ps = c.prepareStatement(sql);
			for (Meldungsvorlage mv : values()) {
				ps.setLong(1, mv.getOID());
				ps.setString(2, mv.getText());
				ps.setString(3, String.valueOf(mv.getTyp().getTypeIdentifier().charAt(0)));
				ps.setString(4, mv.getCode().getIdentifier());
				ps.setString(5, String.valueOf(mv.getDomain().getIdentifier()));
				DBUtils.executeUpdate(ps, sql);
			}
			if (!c.getAutoCommit()) {
				c.commit();
			}
		} finally {
			if (c != null) {
				DBConnectionPool.instance().ungetConnection(c);
			}
		}
	}

	public String getText() {
		return _text;
	}
	
	private Meldungscode getCode() {
		return _code;
	}
	
	private Meldungstyp getTyp() {
		return _type;
	}
	
	private Meldungsdomain getDomain() {
		return _domain;
	}
	
	boolean istAnforderungNoetig() {
		return _anforderungNoetig;
	}
}
