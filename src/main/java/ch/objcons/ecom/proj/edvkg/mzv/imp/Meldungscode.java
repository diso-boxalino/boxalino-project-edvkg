package ch.objcons.ecom.proj.edvkg.mzv.imp;

public enum Meldungscode {
	Zustelladresse("Z"), 
	Pfarrkreis("P"), 
	Umzug("U"), 
	Import ("I"), 
	ZIPRef ("R"),
	Familienbildung ("F"),
	Alter_Import("O"), 
	Anforderungen ("A");

	private String _typeIdent;
	
	private Meldungscode(String typeIdent) {
		_typeIdent = typeIdent;
	}
	
	public String getIdentifier() {
		return _typeIdent;
	}
	
}
