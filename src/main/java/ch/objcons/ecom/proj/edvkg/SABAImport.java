/*
 * Created on 21.10.2004
 * 
 */
package ch.objcons.ecom.proj.edvkg;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import ch.objcons.db.dbimport.ImportStructText;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.query.StringQuery;
import ch.objcons.ecom.shop.date.DateEService;




/**
 * @author Dominik Raymann
 *  
 */
public class SABAImport
{
	

	public static void importSABAWoche(BoxEdit box, BOContext context)
	
	{
		ImportStructText parser = null;
        parser = new ImportStructText(',');
        
        parser.setSkipReturnChar(true);
        parser.setSkipFirstLine(false);
        Date today = new Date();
        try {
        	parser.parse("c:\\temp\\sap0ag.csv");
        	
        	int laufnr = 1;
        	int nullPfarrer = 0;
        	int counter = 0;
        	int imported=0;
	        while (parser.hasMoreRecords()) {
	        	counter++;
	        	//if (counter==21) break;
	        	System.out.println("Datensatz "+counter);
	        	BLTransaction trx = BLTransaction.startTransaction(context);
	        	
	        	
	        	Vector sabazuord = (Vector)parser.nextRecord();
	        	String kg_oid = (String)sabazuord.elementAt(0);
	        	String montag = (String)sabazuord.elementAt(2);
	        	String datum = (String)sabazuord.elementAt(3);
	        	String wochentag = (String)sabazuord.elementAt(4);
	        	String halbtag = (String)sabazuord.elementAt(5);
	        	String disposition = (String)sabazuord.elementAt(6);
	        	String person = (String)sabazuord.elementAt(7);
	        	String bemerkung = (String)sabazuord.elementAt(8);
	        	if (datum.equalsIgnoreCase("20041207") && kg_oid.equalsIgnoreCase("27")) {
	        		System.out.println("HALLO");
	        	}
	        	
	        	SimpleDateFormat sdfImport = new SimpleDateFormat("yyyyMMdd");
	        	SimpleDateFormat sdfDB = new SimpleDateFormat("dd.MM.yyyy");
	        	Date d_datum = sdfImport.parse(datum);
	        	GregorianCalendar cal = new GregorianCalendar();
	        	cal.setTime(d_datum);
	        	
	        	if ((today.getTime()-d_datum.getTime())>(4L*7*24*60*60*1000)) {
	        		System.out.println("record too old ("+d_datum+")--> skip");
	        		System.out.println("today = "+today.getTime()+", d_datum = "+d_datum.getTime()+", delta = "+(today.getTime()-d_datum.getTime()));
	        		continue;
	        	}
	        	
	        	
	        	int actDay = cal.get(Calendar.DAY_OF_MONTH);
	        	int actMonth = cal.get(Calendar.MONTH)+1;	        	
	        	int actWeek = cal.get(Calendar.WEEK_OF_YEAR);
	        	int really_actYear = cal.get(Calendar.YEAR);
	        	
	        	int actYear = DateEService.getYearOfWeek(cal);
	        	
	        	
	        	BOSABAWoche week = null;
				
				MetaObject mobjweek = MetaObjectLookup.getInstance().getMetaObject("SABAWoche");
				MetaObject mobjkg = MetaObjectLookup.getInstance().getMetaObject("Kirchgemeinde");
				MetaObject mobjpfarrer = MetaObjectLookup.getInstance().getMetaObject("SABAPfarrer");
				MetaObject mobjkaldatum = MetaObjectLookup.getInstance().getMetaObject("KalenderDatum");
				MetaObject mobjkalender = MetaObjectLookup.getInstance().getMetaObject("Kalender");
				MetaObject mobjbemerkung = MetaObjectLookup.getInstance().getMetaObject("Kalender");
				MetaObject mobjibem = MetaObjectLookup.getInstance().getMetaObject("SABAIBemerkungenTemp");
				
				BOObject bo_kg = (BOObject)trx.getObject(mobjkg,Long.parseLong(kg_oid),false);
				
				StringQuery strqw = new StringQuery(trx, 0);
				strqw.init("SABAWocheQuery", "SABAWocheQuery", mobjweek);
				strqw.setSqlStatement("select * from bx_sabawoche where bx_oidkg=" + kg_oid + " and bx_woche = " + actWeek+" and bx_jahr = "+actYear);
				//System.out.println("select * from bx_sabawoche where bx_oidkg=" + kg_oid + " and bx_woche = " + actWeek+" and bx_jahr = "+actYear);
				Vector resWeek = strqw.getResult(trx, context);
				if ((resWeek != null) && (resWeek.size() >= 1)) {
					System.out.println("week found");
					week = ((BOSABAWoche)resWeek.elementAt(0));
				} else {
					System.out.println("week not found, create new one");
					week = (BOSABAWoche)trx.getObject(mobjweek, 0, false);
					week.setReferencedObject("OIDKG", bo_kg);
					week.setAttribute("Woche", ""+actWeek, context, null);
					week.setAttribute("Jahr", ""+actYear, context, null);
					week.setValue("toCheck", new Boolean(false), context);
					
					
				}
				week.updateObject(context);
				String attribute = "OID"+wochentag+((halbtag.equalsIgnoreCase("V")?"Vor":"Nach"));
				BOObject zuordnung = week.getReferencedObject(attribute,context);
				String vergleichsDatum = zuordnung.getAttribute("Datum",context);
				Date v_datum = sdfDB.parse(vergleichsDatum);
				
				if (!v_datum.equals(d_datum)) {
					System.out.println(actWeek+" "+actYear);
					System.out.println("!!! ERROR while importing");
					
					System.out.println("!!! "+d_datum+" <> "+v_datum);
					System.out.println("Import aborted.");
					
					return;
				}
				
				BOObject bo_pfarrer = null;
				if ((disposition.equalsIgnoreCase("P") || disposition.equalsIgnoreCase("F")) && (!person.equalsIgnoreCase("1"))) {
					StringQuery pfarrerQuery = new StringQuery(trx,0);
					pfarrerQuery.init("SABAPersonQuery","SABAPersonQuery",mobjpfarrer);
					pfarrerQuery.setSqlStatement("select * from bx_sabaperson where bx_oidkg="+kg_oid+" and bx_kurz="+person);
					//System.out.println("select * from bx_sabaperson where bx_oidkg="+kg_oid+" and bx_kurz="+person);
					Vector resPfarrer = pfarrerQuery.getResult(trx,context);
					if (resPfarrer.size()>1) {
						
						System.out.println("Too many Pfarrer found. Size of result is "+resPfarrer.size());
						System.out.println("Take first one.");
						bo_pfarrer =(BOObject)resPfarrer.firstElement();
						
					} else if (resPfarrer.size()==0){
						System.out.println("Pfarrer not found! Create new one.");
						bo_pfarrer=(BOObject)trx.getObject(mobjpfarrer,0,false);
						bo_pfarrer.setAttribute("Vorname","eingef�gt f�r "+datum, context, null);
						bo_pfarrer.setAttribute("Name","Unbekannt "+person, context, null);
						bo_pfarrer.setAttribute("Kurz",""+person, context, null);
						bo_pfarrer.setValue("Pfarrer",new Boolean(true),context);
						bo_pfarrer.setValue("aktiv",new Boolean(true),context);
						bo_pfarrer.setValue("anzeigen",new Boolean(true),context);
						bo_pfarrer.setReferencedObject("OIDKG",bo_kg);
						
						
					} else {
						bo_pfarrer =(BOObject)resPfarrer.firstElement();
					}
				} 
				if (disposition.equalsIgnoreCase("G") || (disposition.equalsIgnoreCase("P") && person.equalsIgnoreCase("1"))) {
					// gesperrt
					StringQuery pfarrerQuery = new StringQuery(trx,0);
					pfarrerQuery.init("SABAPersonQuery","SABAPersonQuery",mobjpfarrer);
					pfarrerQuery.setSqlStatement("select * from bx_sabaperson where bx_gesperrt='j'");
					Vector resPfarrer = pfarrerQuery.getResult(trx,context);
					if (resPfarrer.size()==0) {
						System.out.println("There is no Pseudo-Pfarrer gesperrt");
						System.out.println("Import aborted.");
						return;
					} else {
						bo_pfarrer =(BOObject)resPfarrer.firstElement();
					}
				}
				if (bo_pfarrer==null) nullPfarrer++;
				BOObject bo_kalenderDatum = null;
				if (disposition.equalsIgnoreCase("F")) {
					// Feiertag, gibts den schon?
					String queryHalbtag, queryKonf;
					if (halbtag.equalsIgnoreCase("V")) queryHalbtag = "(kaldat.bx_vormittag='j')"; else queryHalbtag="(kaldat.bx_nachmittag='j')";
					if (bo_kg.getReferencedObject("OIDKonfession",context).getAttribute("code",context).equalsIgnoreCase("RK")) queryKonf="(kal.bx_kath='j')"; else queryKonf="(kal.bx_ref='j')";
					StringQuery feiertagQuery = new StringQuery(trx,0);
					feiertagQuery.init("SABAFeiertagQuery","SABAFeiertagQuery",mobjkaldatum);
					feiertagQuery.setSqlStatement("select kaldat.* from bx_kalenderdatum as kaldat, bx_kalender as kal where kaldat.bx_tag="+actDay+" and kaldat.bx_monat="+actMonth+" and (isnull(kaldat.bx_jahr) or kaldat.bx_jahr="+really_actYear+") and "+queryHalbtag+" and kal.oid=kaldat.bx_kalenderoidlistkdatumoid and "+queryKonf);
					Vector resFeiertag = feiertagQuery.getResult(trx,context);
					if (resFeiertag.size()!=0) {
						bo_kalenderDatum=(BOObject)resFeiertag.firstElement();
					} else {
						System.out.println("Create Feiertag "+d_datum+" "+halbtag);
						bo_kalenderDatum=(BOObject)trx.getObject(mobjkaldatum,0,false);
						if (halbtag.equalsIgnoreCase("V")) {
							bo_kalenderDatum.setValue("Vormittag",new Boolean(true),context);
							bo_kalenderDatum.setValue("Nachmittag",new Boolean(false),context);
						} else {
							bo_kalenderDatum.setValue("Vormittag",new Boolean(false),context);
							bo_kalenderDatum.setValue("Nachmittag",new Boolean(true),context);
						}
						bo_kalenderDatum.setAttribute("Jahr",""+really_actYear,context,null);
						bo_kalenderDatum.setAttribute("Monat",""+actMonth,context,null);
						bo_kalenderDatum.setAttribute("Tag",""+actDay,context,null);
						
						
						BOObject kalender = (BOObject)trx.getObject(mobjkalender, 0, false);
						kalender.setAttribute("Name","unbekannter Feiertag "+laufnr+" (generiert von Import)",context,null);
						kalender.setAttribute("Kurzname","unbekannter Feiertag "+laufnr+" (import)",context,null);
						laufnr++;
						kalender.setAttribute("aktiv","true",context,null);
						kalender.setValue("Fix",new Boolean(true),context);
						if (bo_kg.getReferencedObject("OIDKonfession",context).getAttribute("code",context).equalsIgnoreCase("RK")) {
							kalender.setValue("Kath",new Boolean(true),context);
							kalender.setValue("Ref",new Boolean(false),context);
						}
						if (bo_kg.getReferencedObject("OIDKonfession",context).getAttribute("code",context).equalsIgnoreCase("EV-REF")) {
							kalender.setValue("Ref",new Boolean(true),context);
							kalender.setValue("Kath",new Boolean(false),context);
						}
											
						kalender.addListReferencedObject("OIDListKDatum",bo_kalenderDatum);
					}
				}
				
				BOObject bo_bemerkung=null;
				StringQuery bemerkungQuery = new StringQuery(trx,0);
				bemerkungQuery.init("SABABemerkungQuery","SABABemerkungQuery",mobjkaldatum);
				bemerkungQuery.setSqlStatement("select * from bx_bemerkung where bx_oidkg="+kg_oid+" and bx_kurzname="+bemerkung);
				Vector resBemerkung = bemerkungQuery.getResult(trx,context);
				if (resBemerkung.size()>0) {
					bo_bemerkung=(BOObject)resBemerkung.firstElement();
				}
				
				BOObject bo_ibem=null;
				StringQuery ibemQuery = new StringQuery(trx,0);
				ibemQuery.init("SABAIBemerkung","SABAIBemerkung", mobjibem);
				ibemQuery.setSqlStatement("select * from bx_sabaibemerkungentemp where bx_oidkg="+kg_oid+" and bx_datum='"+datum+"' and bx_halbtag='"+halbtag+"'");
				Vector ibemResult = ibemQuery.getResult(trx, context);
				if (ibemResult.size()>0) {
					zuordnung.setAttribute("Bemerkung",((BOObject)ibemResult.firstElement()).getAttribute("Bemerkung", context),context,null);
				}
				
				if (bo_pfarrer!=null) zuordnung.setReferencedObject("OIDSabaPers",bo_pfarrer);
				if (bo_bemerkung!=null) zuordnung.setReferencedObject("OIDBemerkung",bo_bemerkung);
				if (bo_kalenderDatum!=null) zuordnung.setReferencedObject("OIDKalenderDatum",bo_kalenderDatum);
				imported++;
				
				System.out.flush();
				
				
	        	
				trx.commit();
	        }
	        
	        System.out.println("Import terminated. "+imported+" of "+counter+" records inserted into database.");
	        System.out.println(nullPfarrer+" halfdays were null");
	        
        } catch(Exception e) {
        	System.out.println("Exception: "+e.getMessage());
        }
		
	}
}
