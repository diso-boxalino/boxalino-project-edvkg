package ch.objcons.ecom.proj.edvkg.mzv;

import java.util.Date;
import java.util.EnumMap;

import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.meta.MetaAttribute;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.proj.edvkg.mzv.imp.BOFamilienKirchgemeinde;
import ch.objcons.log.Log;

public final class Consts {

	/* Person */
	public static final MetaObject PersonMeta = getMetaObject("Person");
	public static final int ZusOIDStrasseIndex = getIndex("ZusOIDStrasse", PersonMeta);
	public static final int ZusStrassenNameIndex = getIndex("ZusStrassenName", PersonMeta);
	public static final int ZusHausNrIndex = getIndex("ZusHausNr", PersonMeta);
	public static final int ZusSortHausNrIndex = getIndex("ZusSortHausNr", PersonMeta);
	public static final int ZusAdrZusatzIndex = getIndex("ZusAdrZusatz", PersonMeta);
	public static final int ZusPLZOrtIndex = getIndex("ZusPLZOrt", PersonMeta);
	public static final int ZusPLZOrtEindIndex = getIndex("ZusPLZOrtEind", PersonMeta);
	public static final int ZusOrtIndex = getIndex("ZusOrt", PersonMeta);
	public static final int ZusOIDCodeOrtLandIndex = getIndex("ZusOIDCodeOrtLand", PersonMeta);
	public static final int ZusOrtLandNameIndex = getIndex("ZusOrtLandName", PersonMeta);
	public static final int ZusPostfachIndex = getIndex("ZusPostfach", PersonMeta);
	public static final int ZusPLZPostfachIndex = getIndex("ZusPLZPostfach", PersonMeta);
	public static final int ZusPLZPostfachEindIndex = getIndex("ZusPLZPostfachEind", PersonMeta);
	public static final int ZusArtIndex = getIndex("ZusArt", PersonMeta);
	public static final int ZusPostlagerndIndex = getIndex("ZusPostlagernd", PersonMeta);

	public static final int OIDCodeStrasseIndex = getIndex("OIDCodeStrasse", PersonMeta);
	public static final int StrasseKGIndex = getIndex("StrasseKG", PersonMeta);
	public static final int AdrZusatzIndex = getIndex("AdrZusatz", PersonMeta);
	public static final int HausNrIndex = getIndex("HausNr", PersonMeta);
	public static final int SortHausNrIndex = getIndex("SortHausNr", PersonMeta);
	
	public static final int PLZOrtIndex = getIndex("PLZOrt", PersonMeta);
	public static final int PLZOrtEindIndex = getIndex("PLZOrtEind", PersonMeta);
	public static final int OrtIndex = getIndex("Ort", PersonMeta);
	public static final int PostfachIndex = getIndex("Postfach", PersonMeta);
	public static final int PLZPostfachIndex = getIndex("PLZPostfach", PersonMeta);
	public static final int PLZPostfachEindIndex = getIndex("PLZPostfachEind", PersonMeta);
	public static final int PostlagerndIndex = getIndex("Postlagernd", PersonMeta);
	public static final int AdressSperreSicherheitIndex = getIndex("AdressSperreSicherheit", PersonMeta);
	public static final int BezirkIndex = getIndex("Bezirk", PersonMeta);
	public static final int EWIDIndex = getIndex("EWID", PersonMeta);
	
	public static final int ZusNameIndex = getIndex("ZusName", PersonMeta);
	public static final int ZusVornameIndex = getIndex("ZusVorname", PersonMeta);
	public static final int ZusAnredeIndex = getIndex("ZusAnrede", PersonMeta);

	public static final int ZeitungAnredeIndex = getIndex("ZeitungAnrede", PersonMeta);
	public static final int ZeitungNachnameIndex = getIndex("ZeitungNachname", PersonMeta);
	public static final int ZeitungVornameIndex = getIndex("ZeitungVorname", PersonMeta);

	public static final int CodeGeschlechtIndex = getIndex("CodeGeschlecht", PersonMeta);

	public static final int NameIndex = getIndex("Name", PersonMeta);
	public static final int VornameIndex = getIndex("Vorname", PersonMeta);

	public static final int OIDKGIndex = getIndex("OIDKG", PersonMeta);
	public static final int OIDVaterIndex = getIndex("OIDVater", PersonMeta);
	public static final int OIDMutterIndex = getIndex("OIDMutter", PersonMeta);
	public static final int OIDEhepartnerIndex = getIndex("OIDEhepartner", PersonMeta);
	public static final int OIDOrigPartnerIndex = getIndex("OIDOrigPartner", PersonMeta);
	public static final int VorstandIndex = getIndex("Vorstand", PersonMeta);
	public static final int OIDVorstandIndex = getIndex("OIDVorstand", PersonMeta);
	public static final int OIDFamilieIndex = getIndex("OIDFamilie", PersonMeta);
	public static final int OIDKonfessionIndex = getIndex("OIDKonfession", PersonMeta);
	public static final int KonfessionIndex = getIndex("Konfession", PersonMeta);
	public static final int FamInfoIndex = getIndex("famInfo", PersonMeta);
	public static final int PartnerBezFamIndex = getIndex("partnerBezFam", PersonMeta);
	public static final int OIDVormundIndex = getIndex("OIDVormund", PersonMeta);
	public static final int SchuelerInfoIndex = getIndex("schuelerInfo", PersonMeta);
	public static final int CodeHausKGIndex = getIndex("codeHausKG", PersonMeta);
	
	public static final int ZeitungVerzichtIndex = getIndex("ZeitungVerzicht", PersonMeta);
	
	public static final int KonfessionsDatumIndex = getIndex("KonfessionsDatum", PersonMeta);
	public static final int AufenthaltsartDatumIndex = getIndex("AufenthaltsartDatum", PersonMeta);
	public static final int MeldeadresseDatumIndex = getIndex("MeldeadresseDatum", PersonMeta);
	public static final int SterbedatumIndex = getIndex("Sterbedatum", PersonMeta);
	public static final int WegDatumIndex = getIndex("WegDatum", PersonMeta);
	public static final int PassivDatumIndex = getIndex("PassivDatum", PersonMeta);

	public static final int ZivilstandsDatumIndex = getIndex("ZivilstandsDatum", PersonMeta);

	public static final int autoZeitungIndex = getIndex("autoZeitung", PersonMeta);
	public static final int OIDIndex = getIndex("OID", PersonMeta);
	public static final int ArtOIDIndex = getIndex("ArtOID", PersonMeta);
	public static final int PersonensperreStadtIndex = getIndex("PersonensperreStadt", PersonMeta);
	
//	public static final int AdresseStadtIndex = getIndex("AdresseStadt", PersonMeta); //Wird momentan nicht gebraucht	
	
	/* PersonHistory */
	public static final MetaObject PersonHistoryMeta = getMetaObject("PersonHistory");
	public static final int Hist_zusOIDStrasseIndex = getIndex("ZusOIDStrasse", PersonHistoryMeta);
	public static final int Hist_zusStrassenNameIndex = getIndex("ZusStrassenName", PersonHistoryMeta);
	public static final int Hist_zusHausNrIndex = getIndex("ZusHausNr", PersonHistoryMeta);
	public static final int Hist_zusAdrZusatzIndex = getIndex("ZusAdrZusatz", PersonHistoryMeta);
	public static final int Hist_zusPLZOrtIndex = getIndex("ZusPLZOrt", PersonHistoryMeta);
	public static final int Hist_zusPLZOrtEindIndex = getIndex("ZusPLZOrtEind", PersonHistoryMeta);
	public static final int Hist_zusOrtIndex = getIndex("ZusOrt", PersonHistoryMeta);
	public static final int Hist_zusOIDCodeOrtLandIndex = getIndex("ZusOIDCodeOrtLand", PersonHistoryMeta);
	public static final int Hist_zusOrtLandNameIndex = getIndex("ZusOrtLandName", PersonHistoryMeta);
	public static final int Hist_zusPostfachIndex = getIndex("ZusPostfach", PersonHistoryMeta);
	public static final int Hist_zusPLZPostfachIndex = getIndex("ZusPLZPostfach", PersonHistoryMeta);
	public static final int Hist_zusPLZPostfachEindIndex = getIndex("ZusPLZPostfachEind", PersonHistoryMeta);
	public static final int Hist_zusPostlagerndIndex = getIndex("ZusPostlagernd", PersonHistoryMeta);
	public static final int Hist_zusArtIndex = getIndex("ZusArt", PersonHistoryMeta);
	
	public static final int Hist_oidCodeStrasseIndex = getIndex("OIDCodeStrasse", PersonHistoryMeta);
	public static final int Hist_StrasseKGIndex = getIndex("StrasseKG", PersonHistoryMeta);
	
	public static final int Hist_AdressSperreSicherheitIndex = getIndex("AdressSperreSicherheit", PersonHistoryMeta);
//	public static final int Hist_AdresseStadtIndex = getIndex("AdresseStadt", PersonHistoryMeta);
	public static final int Hist_VorstandIndex = getIndex("Vorstand", PersonHistoryMeta);
	public static final int Hist_OIDFamilieIndex = getIndex("OIDFamilie", PersonHistoryMeta);
	public static final int Hist_OIDVormundIndex = getIndex("OIDVormund", PersonHistoryMeta);
	public static final int Hist_OIDEhepartnerIndex = getIndex("OIDEhepartner", PersonHistoryMeta);
	public static final int Hist_OIDPersonIndex = getIndex("OIDPerson", PersonHistoryMeta);
	
	public static final int Hist_StatusPersonIndex = getIndex("StatusPerson", PersonHistoryMeta);
	public static final int Hist_OIDKGIndex = getIndex("OIDKG", PersonHistoryMeta);
	public static final int Hist_OIDKonfessionIndex = getIndex("OIDKonfession", PersonHistoryMeta);
	public static final int Hist_KonfessionIndex = getIndex("Konfession", PersonHistoryMeta);

	public static final int Hist_DatVerarbeitungIndex = getIndex("DatVerarbeitung", PersonHistoryMeta);
	public static final int Hist_GeburtsdatumIndex = getIndex("Geburtsdatum", PersonHistoryMeta);
	public static final int Hist_ZivilstandsDatumIndex = getIndex("ZivilstandsDatum", PersonHistoryMeta);
	public static final int Hist_SterbedatumIndex = getIndex("Sterbedatum", PersonHistoryMeta);
	public static final int Hist_PassivDatumIndex = getIndex("PassivDatum", PersonHistoryMeta);
	public static final int Hist_ZuzDatumIndex = getIndex("ZuzDatum", PersonHistoryMeta);
	public static final int Hist_WegDatumIndex = getIndex("WegDatum", PersonHistoryMeta);
	public static final int Hist_WegKGDatumIndex = getIndex("WegKGDatum", PersonHistoryMeta);
	public static final int Hist_KonfessionsDatumIndex = getIndex("KonfessionsDatum", PersonHistoryMeta);
	public static final int Hist_AufenthaltsartDatumIndex = getIndex("AufenthaltsartDatum", PersonHistoryMeta);
	public static final int Hist_MeldeadresseDatumIndex = getIndex("MeldeadresseDatum", PersonHistoryMeta);
	public static final int Hist_DateIndex = getIndex("Date", PersonHistoryMeta);
	public static final int Hist_ErstesUpdateDatumIndex = getIndex("ErstesUpdateDatum", PersonHistoryMeta);
	public static final int Hist_VorfallBitsIndex = getIndex("VorfallBits", PersonHistoryMeta);
	
	public static final int Hist_isLastIndex = getIndex("isLast", PersonHistoryMeta);
	public static final int Hist_lastEntryIndex = getIndex("lastEntry", PersonHistoryMeta);
	public static final int Hist_UserIndex = getIndex("User", PersonHistoryMeta);
	public static final int Hist_OIDKGOrigIndex = getIndex("OIDKGOrig", PersonHistoryMeta);;
	public static final int Hist_RecordStatusIndex = getIndex("RecordStatus", PersonHistoryMeta);;
	
	public static final int Hist_meldungenIndex = getIndex("meldungen", PersonHistoryMeta);
	public static final int Hist_ZeitungVerzichtIndex = getIndex("ZeitungVerzicht", PersonHistoryMeta);
	
	public static final MetaObject MeldungMeta = getMetaObject("Meldung");
	public static final int Meldung_mld = getIndex("mld", MeldungMeta);
	public static final int Meldung_arguments = getIndex("arguments", MeldungMeta);
	public static final int Meldung_PersonHistorymeldungenOID = getIndex("PersonHistorymeldungenOID", MeldungMeta);
	public static final int Meldung_MeldungsvorlagemeldungenOID = getIndex("MeldungsvorlagemeldungenOID", MeldungMeta);
	public static final int Meldung_abgehakt = getIndex("abgehakt", MeldungMeta);
	public static final int Meldung_AnforderungmeldungenOID = getIndex("AnforderungmeldungenOID", MeldungMeta);
		
	public static final MetaObject MeldungsvorlageMeta = getMetaObject("Meldungsvorlage");
	public static final int Meldungsvorlage_text = getIndex("text", MeldungsvorlageMeta);
	public static final int Meldungsvorlage_type = getIndex("type", MeldungsvorlageMeta);
	public static final int Meldungsvorlage_code = getIndex("code", MeldungsvorlageMeta);
	public static final int Meldungsvorlage_meldungen = getIndex("meldungen", MeldungsvorlageMeta);

	public static final MetaObject AnforderungMeta = getMetaObject("Anforderung");
	public static final int Anforderung_zip = getIndex("zip", AnforderungMeta);
	public static final int Anforderung_datum = getIndex("datum", AnforderungMeta);
	public static final int Anforderung_laufnummer = getIndex("laufnummer", AnforderungMeta);
	public static final int Anforderung_meldungen = getIndex("meldungen", AnforderungMeta);
	public static final int Anforderung_kommentar = getIndex("kommentar", AnforderungMeta);
	public static final int Anforderung_fbLaufnummer = getIndex("fbLaufnummer", AnforderungMeta);
	public static final int Anforderung_fbMitAnrecht = getIndex("fbMitAnrecht", AnforderungMeta);
	public static final int Anforderung_fbOhneAnrecht = getIndex("fbOhneAnrecht", AnforderungMeta);

	public static final MetaObject PersonOAMeta = getMetaObject("PersonOA");
	public static final int PersonOA_grundID = getIndex("grundID", PersonOAMeta);
	public static final int PersonOA_datVerarbeit = getIndex("datVerarbeit", PersonOAMeta);
	public static final int PersonOA_lwMutiert = getIndex("lwMutiert", PersonOAMeta);
	
	/* CodeStrasse */
	public static final MetaObject CodeStrasseMeta = getMetaObject("CodeStrasse");
	public static final int CodeStrasse_Name = getIndex("Name", CodeStrasseMeta);
	
	/* CodeHaus */
	public static final MetaObject CodeHausMeta = getMetaObject("CodeHaus");
	public static final int CodeHaus_HausNr = getIndex("HausNr", CodeHausMeta);
	public static final int CodeHaus_Postzustellbezirk = getIndex("Postzustellbezirk", CodeHausMeta);
	public static final int CodeHaus_Postleizahl = getIndex("Postleizahl", CodeHausMeta);
	
	/* CodeHausKG */
	public static final MetaObject CodeHausKGMeta = getMetaObject("CodeHausKG");
	
	public static final MetaObject FamilienKirchgemeinden = getMetaObject(BOFamilienKirchgemeinde.CLASS_NAME);
	
	/* Konfession */
	public static final MetaObject KonfessionMeta = getMetaObject("Konfession");
	public static final int Konfession_code = getIndex("code", KonfessionMeta);
	
	/* Map Person Attribute Indeces to PersonHistory Attribute Indeces */
	public static final int[] Person2History = createPerson2HistoryMap();
	public static final int[] Person2Person = createPerson2PersonMap();
	
	/*
	 * Bitfelder Familieninfo (bx_famInfo)
	 * Achtung: Das stellenwertig h�chste Bit darf wahrscheinlich nicht gebraucht werden bzw. es muss mit int <-> short
	 * umwandlungen aufgepasst werden! 
	 */
	public static final short FI_KonkubinatinfoBitPosition = 0;
	public static final short FI_KonkubinatinfoBitCount = 4;
	public static final short FI_FamilienrollenBitPosition = 4;
	public static final short FI_FamilienrollenBitCount = 3;
	public static final short FI_PassivesAusgewachsenesKindBitPosition = 7; 
	public static final short FI_PassivesAusgewachsenesKindBitCount = 1; 
	
	public static final short FI_KonkubinatinfoMask = ((1 << FI_KonkubinatinfoBitCount) - 1) << FI_KonkubinatinfoBitPosition;
	public static final short FI_FamilienrollenMask = ((1 << FI_FamilienrollenBitCount) - 1) << FI_FamilienrollenBitPosition;
	public static final short FI_PassivesAusgewachsenesKindMask = ((1 << FI_PassivesAusgewachsenesKindBitCount) - 1) << FI_PassivesAusgewachsenesKindBitPosition;
	
	public static final short FI_PredefinedMask = FI_PassivesAusgewachsenesKindMask;
	
	/**
	 * Achtung Positionen der Enum-Konstanten m�ssen erhalten bleiben
	 */
	public static enum FI_Familienrollen { 
		Undefined /* 0 */, Kind /* 1 */, KindMitVater /* 2 */, KindMitMutter /* 3 */, 
		Ehepartner /* 4 */, Partner /* 5 */, PartnerImKonkubinat /* 6 */, Alleine /* 7 */ 
	};
	
	
	public static EnumMap<FI_Familienrollen, String> FI_Familienrollen2String;
	
	static {
		FI_Familienrollen2String = new EnumMap<FI_Familienrollen, String>(FI_Familienrollen.class);
		FI_Familienrollen2String.put(FI_Familienrollen.Undefined, "Unklar");
		FI_Familienrollen2String.put(FI_Familienrollen.Kind, "Kind");
		FI_Familienrollen2String.put(FI_Familienrollen.KindMitVater, "Kind mit Vater");
		FI_Familienrollen2String.put(FI_Familienrollen.KindMitMutter, "Kind mit Mutter");
		FI_Familienrollen2String.put(FI_Familienrollen.Ehepartner, "Ehepartner");
		FI_Familienrollen2String.put(FI_Familienrollen.Partner, "Partner (eing.)");
		FI_Familienrollen2String.put(FI_Familienrollen.PartnerImKonkubinat, "Partner (Konk.)");
		FI_Familienrollen2String.put(FI_Familienrollen.Alleine, "Alleine");
	}
	
	public static enum Vorfall {
		Zuzug (0, true, "Zuz"),
		Wiederzuzug (1, true, "WZuz"),
		Wegzug (2, true, "Wegz"),
		Umzug (3, true, "Umz"),
		Zivilstandsaenderung (4, true, "Ziv"),
		Aufenthaltsartaenderung (5, true, "Auf"),
		Konfessionsaenderung (6, true, "Konf"),
		Tod (7, true, "Tod"),
		Geburt (8, true, "Geb"),
		Mutationslieferung (9, false, "Mut"),
		Familienlieferung (10, false, "Fam"),
		Anforderungslieferung (11, false, "Anf"),
		Erstlieferung(12, false, "Erst"), 
		OhneAnrecht (13, false, "OA");
		
		private int _bit;
		private boolean _istVorfall;
		private String _abkuerzung;
		
		private Vorfall (int bit, boolean vorfall, String abk) {
			_bit = bit;
			_istVorfall = vorfall;
			_abkuerzung = abk;
		}
		
		public int getBitPos() {
			return _bit;
		}
		
		public int getBitMask() {
			return 1 << _bit; 
		}
		
		public boolean istVorfall() {
			return _istVorfall;
		}
		
		public String getAbkuerzung() {
			return _abkuerzung;
		}
	}
	
	/**
	 * OID-Grenze f�r H�ndische
	 */
	public static final long AlleHaendischeGrenze = 500000000L;
	public static final Date GeburtsTagForNull = new Date(1900 - 1900, 0, 1);

	
	private static final MetaObject getMetaObject(String name) {
		MetaObject meta = null;
		try {
			meta = MetaObjectLookup.getInstance().getMetaObject(name);
		} catch (XMetaModelNotFoundException e) {
			Log.logSystemAlarm("Could not initialize EDVKG's Meta Object: " + name, e);
		}
		return meta;
	}
	
	private static final int getIndex(String attributeName, MetaObject metaObj) {
		MetaAttribute ma;
		try {
			if (metaObj != null) { // Kann w�hrend Ausf�hrung vom BoxUpgrader noch null sein
				ma = metaObj.getAllAttributeForName(attributeName, null);
			} else {
				return -1;
			}
		} catch (XMetaModelNotFoundException e) {
			Log.logSystemAlarm("Could not initialize EDVKG's Consts Class: " + attributeName, e);
			return -1;
		}
		return ma.getIndex();
	}
	
	private static final int[] createPerson2HistoryMap () {
		try {
			MetaAttribute[] personAttrs = PersonMeta.getAllAttributes();
			int[] res = new int[PersonMeta.getLastAttrIndex() + 1];
			for (int i = 0; i < personAttrs.length; i++) {
				MetaAttribute attr = personAttrs[i];
				if ("OID".equals(attr.getExternName()) 
					|| "ArtOID".equals(attr.getExternName())) continue;
				if (attr.getLifecycleType() == MetaAttribute.LIFECYCLE_PERSISTENT) {
					MetaAttribute targetAttr = null;
					boolean found = true;
					try {
						targetAttr = PersonHistoryMeta.getAllAttributeForName(attr.getExternName(), null);
					} catch (XMetaModelNotFoundException e) {
						found = false;
					}
					if (found) {
						res[attr.getIndex()] = targetAttr.getIndex();
					}
				}
			}
			return res;
		} catch (Exception e) {
			Log.logSystemAlarm("Could not initialize EDVKG's Consts Class", e);
		}
		return null;
	}
	
	private static final int[] createPerson2PersonMap () {
		try {
			int[] temp = new int[Person2History.length];
			for (int i = 0; i < temp.length; i++) {
				temp[i] = i;
			}
			return temp;
		} catch (Exception e) {
			Log.logSystemAlarm("Could not initialize EDVKG's Consts Class", e);
		}
		return null;
	}
}
