package ch.objcons.ecom.proj.edvkg.mzv.imp;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOObject;

public class BOFamilienKirchgemeinde extends BOObject {

	public static final String CLASS_NAME = "FamilienKirchgemeinde";
	
	public BOFamilienKirchgemeinde(BLTransaction arg0, long arg1) {
		super(arg0, arg1);
	}

}
