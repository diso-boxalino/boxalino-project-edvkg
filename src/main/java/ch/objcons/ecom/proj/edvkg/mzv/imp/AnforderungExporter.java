package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.sql.Connection;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.proj.edvkg.mzv.BoxMZV;

public class AnforderungExporter implements Runnable {

	public static final String JOB_KEY = "mzvAnforderungCreator";
	
	@Override
	public void run() {
		IRequest request = BoxalinoUtilities.getDummyRequest();
		BOContext context = (BOContext) request.getContext();
		Connection conn = null;
		try {
			conn = DBConnectionPool.instance().getConnection();
			Anforderungen.createExportFile(BoxMZV._anforderungsExportFile, context, conn, Anforderungen.LOG_FILE_WRITER);
		} catch (Exception e) {
			Anforderungen.LOGGER.error("failed to create Anforderungen!", e);
		} finally {
			DBConnectionPool.instance().ungetConnection(conn);
		}
	}

}
