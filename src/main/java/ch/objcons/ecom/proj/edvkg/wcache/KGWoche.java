/*
 * Created on 24.01.2005
 *
 * (c) by Boxalino
 */
package ch.objcons.ecom.proj.edvkg.wcache;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.proj.edvkg.BoxEdit;


/**
 * @author Dominik Raymann
 */
public class KGWoche extends AgendaWoche implements IKirchgemeinde{
	
		
	private String _kgOID = null;
	private String _kgName = null;
	 
	
	private BestatterWoche[][] _buchung = new BestatterWoche[5][4];
	private String[][] _standardBemerkungen = new String[5][4];
	private String[][] _bemerkungen = new String[5][4];
	
	private Vector[][] _verfuegbareBestatter = new Vector[5][4];
	
	private Vector _verfuegbareBestatterGanzeWoche = null;
			
	public KGWoche(String woche, String jahr, String kgOID, BOContext boContext) throws CacheException{
		super(woche, jahr, boContext);
		
		_kgOID = kgOID;
		
		try {
			MetaObject moKG = MetaObjectLookup.getInstance().getMetaObject("Kirchgemeinde");
			BLTransaction trans = BLTransaction.startTransaction(_boContext);
			BOObject boKG = trans.getObject(moKG,Long.parseLong(_kgOID),false);
			_kgName = boKG.getAttribute("Name",_boContext);
		} catch (Exception e) {
			throw new CacheException("ERROR while fetching KG data: "+e.getMessage());
		}
		
		init();
		
	}
	
	
	
	private void init() throws CacheException {
		CacheSABAWochen cacheClass = CacheSABAWochen.getInstance(_boContext);
		try {
			BLTransaction trans = BLTransaction.startTransaction(_boContext);
			
			
			MetaObject moSABAWoche = MetaObjectLookup.getInstance().getMetaObject("SABAWoche");
			BOObject boSABAWoche = null;
			
			long oid = BoxEdit.getSABAWeek(_jahr,_woche,_kgOID,_boContext);
			boSABAWoche = trans.getObject(moSABAWoche,oid,false);
			
			
			
			for (int tag = 0;tag<5;tag++) {
				for (int vierteltag = 0; vierteltag<4;vierteltag++) {
					BOObject boSABAZuordnung = boSABAWoche.getReferencedObject("OID"+TAGE[tag]+""+VIERTELTAGE[vierteltag],_boContext);
					if (boSABAZuordnung!=null) {
						BOObject boSABAPerson = boSABAZuordnung.getReferencedObject("OIDSabaPers", _boContext);
						if (boSABAPerson !=null) {
							BestatterWoche bestatterWoche = cacheClass.getBestatterWoche(_woche,_jahr,""+boSABAPerson.getOID());
							bestatterWoche.setBuchung(tag, vierteltag,this);
							_buchung[tag][vierteltag] = bestatterWoche;
							
						}
						
						BOObject boStandardBemerkung  = boSABAZuordnung.getReferencedObject("OIDBemerkung", _boContext);
						if (boStandardBemerkung!=null) {
							_standardBemerkungen[tag][vierteltag] = ""+boStandardBemerkung.getOID();
						}
						
						_bemerkungen[tag][vierteltag] = boSABAZuordnung.getAttribute("Bemerkung",_boContext);
						
						
						
					}
					
				}
			}
			
			
			
		} catch (Exception e) {
			throw new CacheException("ERROR - konnte Woche der Kirchgemeinde (OID = "+_kgOID+")nicht initialisieren : "+e.getMessage());
					
		}
		
		
	}
	
	public void initPhase2() throws CacheException{
		try {
			Hashtable alleVerfuegbare = new Hashtable();
			BLTransaction trans = BLTransaction.startTransaction(_boContext);
			Vector meinePfarrer = BoxalinoUtilities.executeQuery("SABAPerson","((SABAPerson_OIDKG == "+_kgOID+") or (SABAPerson_gesperrt == true)) and (SABAPerson_Pfarrer == true) and (SABAPerson_aktiv == true)",trans,_boContext);
			
			CacheSABAWochen cacheClass = CacheSABAWochen.getInstance(_boContext);
			for (int tag = 0;tag<5;tag++) {
				Calendar cal = new GregorianCalendar();
				cal.set(Calendar.WEEK_OF_YEAR, Integer.parseInt(_woche));
				cal.set(Calendar.YEAR, Integer.parseInt(_jahr));
				cal.set(Calendar.DAY_OF_WEEK, CALENDARDAYS[tag]);
				DateFormat dateFormat = _boContext.getDateFormat();
				
				Vector freigaben = BoxalinoUtilities.executeQuery("Freigabe","(Freigabe_Kirchgemeinden == "+_kgOID+") and (Freigabe_von <= "+dateFormat.format(cal.getTime())+") and (Freigabe_bis >="+dateFormat.format(cal.getTime())+")",trans,_boContext);	
				for (int vierteltag = 0; vierteltag<4;vierteltag++) {
					Vector liste = new Vector(meinePfarrer);
					for (Iterator it = freigaben.iterator();it.hasNext();) {
						BOObject freigabe = (BOObject)it.next();
						liste.add(freigabe.getReferencedObject("SABAPersonFreigabenOID", _boContext));
						
					}
					
					
					_verfuegbareBestatter[tag][vierteltag] = new Vector();
					
					for (Iterator it = liste.iterator();it.hasNext();) {
						
						BOObject bestatter = (BOObject)it.next();
						long oid = bestatter.getOID();
						BestatterWoche bestatterWoche = cacheClass.getBestatterWoche(_woche,_jahr,""+oid);
						
						
						
						if ((bestatterWoche.getVerfuegbar(tag,vierteltag))) { // einkommentieren, wenn ein Bestatter nur in einer KG gleichzeitig gebucht werden darf: && ((bestatterWoche.getBuchung(tag,vierteltag)==null) || (bestatterWoche.getBuchung(tag,vierteltag)==this))) {
							_verfuegbareBestatter[tag][vierteltag].add(bestatterWoche);
							
							alleVerfuegbare.put(bestatterWoche.getOID(),bestatterWoche);
						}
						
					}
					Collections.sort(_verfuegbareBestatter[tag][vierteltag]);
					
				}
			}
			_verfuegbareBestatterGanzeWoche = new Vector(alleVerfuegbare.values());
			
			Collections.sort(_verfuegbareBestatterGanzeWoche);
		
			
			
			
		} catch (Exception e) {
			throw new CacheException("ERROR - konnte Woche der Kirchgemeinde (OID = "+_kgOID+")nicht initialisieren (initPhase2): "+e.getMessage());
		}	
	}
	public Vector getVerfuegbareBestatter(int tag, int vierteltag) {
		return _verfuegbareBestatter[tag][vierteltag];
	}
	
	public Vector getVerfuegbareBestatterGanzeWoche() {
		
		return _verfuegbareBestatterGanzeWoche;
	}
	
	public IBestatter getGebuchterBestatter(int tag, int vierteltag) {
		return (IBestatter)_buchung[tag][vierteltag];
		
	}
	
	public String getStandardBemerkung(int tag, int vierteltag) {
		return _standardBemerkungen[tag][vierteltag];
		
	}
	
	public String getBemerkung(int tag, int vierteltag) {
		return _bemerkungen[tag][vierteltag];
		
	}



	
	public String getName() {
	
		return _kgName;
	}

	public String getOID() {
		
		return _kgOID;
	}
}