/*
 * Created on 28.01.2005
 *
 * (c) by Boxalino
 */
package ch.objcons.ecom.proj.edvkg;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.proj.edvkg.wcache.CacheSABAWochen;
import ch.objcons.ecom.system.security.XSecurityException;

/**
 * @author Dominik Raymann
 */
public class BOBemerkung extends BOObject {
	
		
	public void writeToDB(boolean isInsert, Connection con)
			throws SQLException, XMetaException, XSecurityException, XBOConcurrencyConflict {
		
		BOContext boContext = new BOContext(Locale.getDefault());
		CacheSABAWochen.getInstance(boContext).clearCache();
		
		
		super.writeToDB(isInsert, con);
	}
	
	
	public BOBemerkung(BLTransaction trans, long oid) {
		super(trans, oid);
	
	}
}
