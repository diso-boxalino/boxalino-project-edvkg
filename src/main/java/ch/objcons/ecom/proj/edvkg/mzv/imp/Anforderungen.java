package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.db.DBUtils;
import ch.objcons.db.dbimport.LogFileWriter;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.expr.XExpression;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.BOPersonHistory;
import ch.objcons.ecom.proj.edvkg.mzv.BoxMZV;
import ch.objcons.ecom.proj.edvkg.mzv.Consts;
import ch.objcons.ecom.proj.edvkg.mzv.OhneAnrechtWeil;
import ch.objcons.ecom.query.XMaxRowException;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

public class Anforderungen {
	
	static ILogger LOGGER = Logger.getLogger("boxalino.edvkg.anforderungen");
	static LogFileWriter LOG_FILE_WRITER = new LogFileWriter(LOGGER);

//	public static void createAnforderungen(Hashtable<Long, List<Meldung>> meldungen, LogFileWriter logWriter, BOContext context, Connection c) throws XSecurityException, XMetaException, XDataTypeException, SQLException, XBOConcurrencyConflict {
//		String table = "bx_anforderung";
//		logWriter.writeNewline();
//		logWriter.writeText("---------------------------------------------------------------------------------");
//		logWriter.writeText("Anforderungen werden in Datenbank geschrieben (" + table + ")");
//		logWriter.writeText("---------------------------------------------------------------------------------");
//
//		try {
//			BLTransaction trans = BLTransaction.startTransaction(context);
//			for (Entry<Long, List<Meldung>> entry : meldungen.entrySet()) {
//				List<Meldung> mListe = entry.getValue();
//				for (Meldung m : mListe) {
//					if (m._mv.istAnforderungNoetig() && m.istMeldungNeu()) {
//						BOAnforderung boAnf = BOAnforderung.createAnforderung(trans);
//						boAnf.setAttribute(Consts.Anforderung_zip, String.valueOf(entry.getKey()), context);
//						boAnf.setAttribute(Consts.Anforderung_meldung, String.valueOf(m._oid), context);
//					}
//				}
//			}
//			trans.commit(c, true);
//		} finally {
//		}
//	}

	private static final Lock EXPORT_LOCK = new ReentrantLock();
	
	private final Connection _con;
	
	private final boolean _checkLaufnummer;
	
	private final boolean _isAnforderungenSlave;
	
	public Anforderungen(Connection c) {
		this(c, BoxMZV.getInstance().getAnforderungCheckLaufnummer(), BoxMZV.getInstance().isAnforderungSlave());
	}
	
	/**
	 * @param c
	 * @param checkLaufnummer falls true, so werden nur Anforderungen akzeptiert deren import Laufnummer >= 
	 * der export Laufnummer ist.
	 */
	public Anforderungen(Connection c, boolean checkLaufnummer, boolean isAnforderungenSlave) {
		_con = c;
		_checkLaufnummer = checkLaufnummer;
		_isAnforderungenSlave = isAnforderungenSlave;
	}
	
	@Override
	protected void finalize() throws Throwable {
		if (_psCreateAnf != null) {
			_psCreateAnf.close();
		}
		super.finalize();
	}

	/*
	 * Variablen f�r createAnforderung
	 */
	private boolean _initialized = false;
	private PreparedStatement _psCreateAnf = null;
	long _currentAnfOID = -1;
	java.sql.Date _currentDate = null;
	int _numOfCreatedAnforderungen;
	
	/**
	 * Erstellt Anforderungen per direktem SQL.
	 * Warum werden nicht Boxalino Transactions verwendet? Weil die OID pro Anforderung klar sein muss und
	 * BOObject.setOID() eine ziemlich teure Operation ist. Alternativ k�nnte man per OIDGenerator einen
	 * OID-Bereich reservieren lassen und dann die OID mit BOObject.setOID(long) setzen. Aber warum der
	 * Aufwand - die MZV ist eh auf direkte SQL-Queries angewiesen (Query Hook, Post Import, ...) nicht
	 * zuletzt um die Leistungsf�hrigkeit zu gew�hrleisten.
	 * 
	 * Die Anforderungen werden durch Meldungen des Post Imports ausgel�st. Falls die Meldungen auf einem
	 * Datensatz nicht �ndern wird auch keine Anforderung ausgel�st, obwohl die Meldungen an und f�r sich
	 * dies erforderten. Dieses Verhalten scheint vorderhand in Ordnung zu sein, da sonst immer wieder die-
	 * selben Anforderungen gestellt w�rden, welche doch keine Verbesserung br�chten.
	 *  
	 * @param zip
	 * @param logWriter
	 * @return
	 * @throws SQLException
	 */
	public long createAnforderung(long zip, LogFileWriter logWriter) throws SQLException {
		if (_isAnforderungenSlave) return -1;
		
		final String table = "bx_anforderung";
		
		if (!_initialized) {
			logWriter.writeNewline();
			logWriter.writeText("---------------------------------------------------------------------------------");
			logWriter.writeText("Anforderungen werden in Datenbank geschrieben (" + table + ")");
			logWriter.writeText("---------------------------------------------------------------------------------");
			
			long artoid = Consts.AnforderungMeta.getOID();
			String sql = "insert into " + table + " (oid, artoid, bx_zip, bx_datum, bx_manuell) values (?, " + artoid + ", ?, ?, 'n')";
			_psCreateAnf = _con.prepareStatement(sql);
			_currentDate = new java.sql.Date(System.currentTimeMillis());
			_numOfCreatedAnforderungen = 0;
			// oid herausfinden
			Statement stmt = null;
			try {
				stmt = _con.createStatement();
				ResultSet rs = stmt.executeQuery("select max(oid) from " + table);
				rs.next();
				_currentAnfOID = rs.getLong(1);
				
			} finally {
				_initialized = true;
				if (stmt != null) {
					stmt.close();
				}
			}
		}
		_psCreateAnf.setLong(1, ++_currentAnfOID);
		_psCreateAnf.setLong(2, zip);
		_psCreateAnf.setDate(3, _currentDate);
		int num = _psCreateAnf.executeUpdate();
		_numOfCreatedAnforderungen += num;
		return _currentAnfOID;
	}
	
	public void logCreatedAnforderungen(LogFileWriter logWriter) {
		logWriter.writeText(_numOfCreatedAnforderungen + " Anforderungen erstellt");
	}

	public void importiert(boolean anrecht, long oidPerson, BOPersonHistory boHistory, Date verarbDatum,
			int fbLaufnummer, OhneAnrechtWeil oaw, boolean geliefertWeilMutiert, BLTransaction trx) throws XSecurityException, XMetaException, XDataTypeException {

		/* Schreiben auf DB mit BO-Transaktion l�sen, 
		 * da nicht alle History-Objekte schon eine OID haben */
		BOPersonOA boPersonOA = null;

		List<BOAnforderung> boAnfListe = getAnforderungen(oidPerson, trx);
		for (BOAnforderung boAnf : boAnfListe) {
			int laufnummer = boAnf.getLaufnummer();
			// eine Anforderung gilt nur dann als erf�llt, falls deren
			// import laufnummer >= der export laufnummer ist
			if (!_isAnforderungenSlave && _checkLaufnummer && fbLaufnummer < laufnummer) continue;
			
			boAnf.setValue(Consts.Anforderung_fbLaufnummer, fbLaufnummer);
			if (anrecht) {
				boAnf.setReferencedObject(Consts.Anforderung_fbMitAnrecht, boHistory);
			} else {
				if (boPersonOA == null) {
					boPersonOA = BOPersonOA.createOrSetPersonOA(oidPerson, verarbDatum, oaw, geliefertWeilMutiert, trx);
//					BOMeldung.createMeldung(trans, Meldungsvorlage.Anf_ManOhneAnrecht, oaw.toString());
				}
				boAnf.setReferencedObject(Consts.Anforderung_fbOhneAnrecht, boPersonOA);
			}
			
		}
	}
	
	private List<BOAnforderung> getAnforderungen(long zip, BLTransaction trans) throws XMetaModelQueryException {
		String query = "select * from bx_anforderung where bx_zip = " + zip;
		query += " and bx_fbLaufnummer is null order by oid desc";
		
		List<BOAnforderung> result = BLManager.getResult(trans, query, true /* isAlreadyFiltered */, trans.getContext(), _con);
		return result;
	}

	public static void doPostImportHandling(Connection con, LogFileWriter logWriter) throws SQLException {
		// Platzhalter f�r Post Import Implementierungen
	}
	
	
	/**
	 * Erstellt eine neue Anforderungsdatei mit allen Anforderungen ohne Laufnummer und setzt die Laufnummer dessen.
	 * Sind keine Anforderungen ohne Laufnummer vorhanden, so wird keine Operation ausgef�hrt und -1 zur�ck gegeben. 
	 * @param exportFile Das File ohne die Laufnummer
	 * @param context
	 * @param curCon kann auch <code>null</code> sein
	 * @return Laufnummer vom File, sonst -1
	 */
	public static int createExportFile(String exportFile, BOContext context, Connection curCon, LogFileWriter logWriter) throws SQLException, XMetaModelNotFoundException, XMetaModelQueryException, XExpression, XMetaException, XDataTypeException, XSecurityException, XMaxRowException, XBOConcurrencyConflict, IOException {
		if (BoxMZV.getInstance().isAnforderungSlave()) return -1;
		
		final Lock lock = EXPORT_LOCK;
		lock.lock();
		try {
			BLTransaction trans = BLTransaction.startTransaction(context);
			@SuppressWarnings("unchecked")
			List<BOAnforderung> anfListe = BoxalinoUtilities.executeQuery("Anforderung", "Anforderung_laufnummer == null", trans, context);
			if (anfListe.isEmpty()) {
				logWriter.writeText("Keine zu exportierenden Anforderungen verf�gbar");
				return -1;
			}
			
			int naechsteLaufnummer = -1;
			Connection con = null;
			Statement stmt = null;
			Writer w = null;
			File file = null;
			FileOutputStream fos = null;
			boolean exportFinished = false;
			try {
				con = curCon == null ? DBConnectionPool.instance().getConnection() : curCon;
				stmt = con.createStatement();
				String sql = "select max(bx_laufnummer) from bx_anforderung";
				ResultSet rs = DBUtils.executeQuery(stmt, sql);
				
				rs.next();
				int letzteLaufnummer = rs.getInt(1);
				naechsteLaufnummer = letzteLaufnummer + 1;
				
			    DecimalFormat decForm = new DecimalFormat("000000");
			    String numberFormatted = decForm.format(naechsteLaufnummer);
				file = new File(exportFile + numberFormatted);
				if (file.exists() && file.length() > 0) {
					String msg = "Das Exportfile '" + file.getAbsolutePath() + "' existiert schon. Abbruch.";
					logWriter.writeError(msg);
					throw new IllegalStateException(msg);
				}
				fos = new FileOutputStream(file);
				w = new BufferedWriter(new OutputStreamWriter(fos, "iso-8859-1"));
	
				Set<Long> exported = new HashSet<Long>(anfListe.size()); 
				for (BOAnforderung boAnf : anfListe) {
					long zip = boAnf.getReferencedOID(Consts.Anforderung_zip);
					if (zip > 0 && exported.add(zip)) {
						w.append(String.valueOf(zip));
						w.append(numberFormatted);
						w.append('\n');
					}
					boAnf.setValue(Consts.Anforderung_laufnummer, naechsteLaufnummer);
				}
				trans.commit(con, true);
				exportFinished = true;
			} finally {
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {
					}
				}
				if (con != null && curCon == null) {
					DBConnectionPool.instance().ungetConnection(con);
				}
				if (w != null) {
					if (!exportFinished) {
						naechsteLaufnummer = -1;
						w.flush();
						fos.getChannel().truncate(0); // Gr�sse auf 0 setzen, damit das File, wenn es nicht gel�scht werden kann, wenigstens die Gr�sse 0 hat
						w.close();
						if (!file.delete()) {
							logWriter.writeError("Konnte Export File '" + file.getName() + "' nicht l�schen nach misslungenem Export");
						}
					} else {
						w.close();
					}
				}
			}
			return naechsteLaufnummer;
		} finally {
			lock.unlock();
		}
	}
}
