package ch.objcons.ecom.proj.edvkg.analyse;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.db.dbimport.ImportStructText;
import ch.objcons.ecom.api.IRequest;

public class Schni2Analyse {


	
	public Schni2Analyse(IRequest request) {
		_request = request;
	}

	public static Schni2Analyse getInstance(IRequest request) {
		return new Schni2Analyse(request);
	}
	
	
	/*
	 * 
FieldWidth_0=9
#	('FAMILIEN-NR')         
FieldWidth_1=9
#	TEXT('ZIP')                 
FieldWidth_2=3
#	TEXT('TYP PARTNERBEZIEHUNG' 
FieldWidth_3=3
#	TEXT('Drittmeldepflichtiger’)    
FieldWidth_4=6
#	TEXT('KONFESSION')          
FieldWidth_5=8
#	TEXT('GEBURTSDATUM')        
FieldWidth_6=2
#	TEXT('KIRCHGEMEINDE')       Datei
FieldWidth_7=2
#	TEXT('PFARRKREIS')          Datei
FieldWidth_8=1
#	TEXT('AKTIV/PASSIV-STATUS   EF001
FieldWidth_9=8
#	TEXT('PASSIV-DATUM')        
FieldWidth_10=1
#	TEXT('ADRESSSPERRE')        EF005
FieldWidth_11=9
#	TEXT('ZIP HAUSHALTSVORSTAND 

select k.bx_name, count(k.oid) from tmpoid t inner join bx_person p on t.oid = p.oid left join bx_kirchgemeinde k on p.bx_oidkg = k.oid group by k.oid;
*
*/
	private IRequest _request = null;
	
	private PreparedStatement _ps1 = null;
	private PreparedStatement _ps2 = null;
	
	int _newPersons = 0; 

	public void makePersonListForImportFiles () {
		Connection con = null;
		try {
			/*
			 * Personen-Zips aus kbu-Importdateien auslesen und ZIPs vom Familienverbund hinzufügen
			 */
			con = DBConnectionPool.instance().getConnection();
			ImportStructText importer = new ImportStructText(12, new int[] {9,9,3,3,6,8,2,2,1,8,1,9});
			importer.setCharacterSet("iso-8859-1");
			importer.setSkipFirstLine(false);
			
			String getPersonSQL1 = "select oid from bx_person where oid = ?";
			_ps1 = con.prepareStatement(getPersonSQL1);
			String getPersonSQL2 = "select oid, bx_oidfamilie, bx_oidvater, bx_oidmutter, bx_oidvorstand, bx_oidehepartner from bx_person where oid = ? or bx_oidfamilie = ? or bx_oidhaushalt = ? or bx_oidvater = ? or bx_oidmutter = ? or bx_oidvorstand = ? or bx_oidehepartner = ?";
			_ps2 = con.prepareStatement(getPersonSQL2);
	
			Set<Integer> zips = new HashSet<Integer>(10000);
			int laufNrStart = 1770;
			int laufNrEnd = 1776;
			String dir = "c:/imports/Mai_10/";
			int recordCounter = 0;
			for (int laufNr = laufNrStart; laufNr <= laufNrEnd; laufNr++) {
				String fileNameSTRIMP = "zmpkbk00" + laufNr;
				File fileSTRIMP = new File(dir + fileNameSTRIMP);
				if (fileSTRIMP.exists()) {
					String fileNamePers = "zmpkbu00" + laufNr;
					File filePers = new File(dir + fileNamePers);
					if (filePers.exists()) {
						importer.parse(filePers.getAbsolutePath());
						while (importer.hasMoreRecords()) {
							List<String> rec = (List<String>) importer.nextRecord();
							handleRecord(rec, zips);
							recordCounter++;
						}
					}
				}
			}
			Statement stmt = con.createStatement();
			stmt.execute("delete from tmpoid");
			stmt.close();

			System.out.println("Import-ZIPs: " + recordCounter);
			System.out.println("Import-ZIPs Relatives: " + zips.size());
			
			/*
			 * Die ZIPs aus zwei Kirchgemeinden hinzufügen inlusive der ZIPs vom Familienverbund
			 */
			addKGZips(21, con, zips); // 21 Unterstrass EV-REF
			addKGZips(71, con, zips); // 71 Liebfrauen RK
			
			System.out.println("Total Zips: " + zips.size());
			System.out.println("Totale neue: " + _newPersons);
			writeZips(zips, con);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				DBConnectionPool.instance().ungetConnection(con);
			}
			if (_ps1 != null) {
				try {
					_ps1.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (_ps2 != null) {
				try {
					_ps2.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private void handleRecord(List<String> rec, Set<Integer> zips) throws Exception {
		int vorstand = Integer.valueOf(rec.get(0));
		int zip = Integer.valueOf(rec.get(1));
		int hh = Integer.valueOf(rec.get(11));
		
		addRelatives(zip, zips);
		
	}

	private void addRelatives(int zip, Set<Integer> zips) throws Exception {
		_ps1.setInt(1, zip);
		ResultSet rs1 = _ps1.executeQuery();
		if (rs1.next()) {
			if (zips.add(zip)) {
				for (int i = 1; i <= 7; i++) {
					_ps2.setInt(i, zip);
				}
				ResultSet rs2 = _ps2.executeQuery();
				List<Integer> zips2 = new ArrayList<Integer>();
				while (rs2.next()) { // separate loop because of reuse of result set
					for (int i = 1; i <= 6; i++) {
						int zip2 = rs2.getInt(i);
						if (zip2 != 0) {
							zips2.add(zip2);
						}
					}
				}
				for (Integer zip2 : zips2) {
					addRelatives(zip2, zips);
				}
			} else {
				// schon behandelt
			}
		} else {
			// neue Person
			_newPersons++;
		}
		
		
	}

	private void writeZips(Set<Integer> zips, Connection con) throws Exception {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("insert into tmpoid values (?)");
			for (Integer zip : zips) {
				ps.setInt(1, zip);
				ps.execute();
			}
		} finally {
			if (ps != null) {
				ps.close();
			}
		}
		
	}

	private void addKGZips(int oidkg, Connection con, Set<Integer> zips) throws Exception {
		PreparedStatement ps = null;
		try {
			int zipCountRel = zips.size();
			int zipCount = 0;
			ps = con.prepareStatement("select oid from bx_person where bx_oidkg = ?");
			ps.setInt(1, oidkg);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int zip = rs.getInt(1);
				addRelatives(zip, zips); // Verwandtschaftliche ZIP zum Set hinzufügen
				zipCount++;
			}
			zipCountRel = zips.size() - zipCountRel;
			System.out.println("KG Zips: " + zipCount);
			System.out.println("KG Zip Relatives (ungefähr): " + zipCountRel);
		} finally {
			ps.close();
		}
	}

}
