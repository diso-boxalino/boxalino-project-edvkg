package ch.objcons.ecom.proj.edvkg.tests;

import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ImportDataIterator<E> implements Iterator<E> {

//	private final SynchronousQueue<E> _testDataQueue = new SynchronousQueue<E>(); 
	private final BlockingQueue<E> _testDataQueue = new ArrayBlockingQueue<E>(10); 
	private boolean _stopped = false; 
	
	public boolean hasNext() {
		while (true) {
			if (_stopped) {
				return false;
			} else if (_testDataQueue.peek() == null) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					if (!_stopped) {
						e.printStackTrace();
					} else {
						Thread.interrupted(); // clears interrupted status
					}
					return false;
				}
			} else {
				return true;
			}
			
		}
	}
	
	public E next() {
		try {
			return _testDataQueue.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void remove() {
		// do nothing
	}
	
	// may be solved with thread interruption
	public void stopWaiting() {
		_stopped = true;
		
	}
	
	public void put(E testData) {
		try {
			_testDataQueue.put(testData);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
