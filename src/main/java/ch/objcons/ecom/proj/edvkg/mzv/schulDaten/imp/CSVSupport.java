package ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;

import au.com.bytecode.opencsv.CSVReader;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

/**
 * Utility class to read CSV-Files, both name and index-based access of columns is supported.
 */
public class CSVSupport {
	
	private static final ILogger LOGGER = Logger.getLogger(CSVSupport.class);
	
	/**
	 * <p>
	 * Provides a per-row iterator for a CSV-File, the first row is interpreted
	 * as header.
	 * </p>
	 * <p>
	 * The EntryRows the iterator is returning support either index based or 
	 * name based access to the columns. Which name maps to which index is determined
	 * by the specified fieldMap, or if fieldMap is null, each column's header.
	 * </p>
	 * @param path path to the CSV-File
	 * @param fieldMap mapping of field-names to indices
	 * @return
	 */
	public static CSVRows read(String path, Map<String, Integer> fieldMap) {
		try {
			return read(new CSVReader(new FileReader(path), ';'), fieldMap);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * <p>
	 * Provides a per-row iterator for a CSV source, the first row is interpreted
	 * as header.
	 * </p>
	 * <p>
	 * The EntryRows the iterator is returning support either index based or 
	 * name based access to the columns. Which name maps to which index is determined
	 * by the specified fieldMap, or if fieldMap is null, each column's header.
	 * </p>
	 * @param reader reader to an underlying CSV source 
	 * @param fieldMap mapping of field-names to indices
	 * @return
	 */
	public static CSVRows read(CSVReader csvReader, Map<String, Integer> fieldMap) {
		try {
			String[] headers = csvReader.readNext();
			Map<String, Integer> fileFieldMap = getFieldMap(headers);
			Map<String, Integer> fm;
			if (fieldMap == null) {
				fm = fileFieldMap;
			} else {
				fm = fieldMap;
				// equality of iteration order of mappings is irrelevant, don't check
				if (!fieldMap.equals(fileFieldMap)) {
					String mismatch = valueOrderMap(fileFieldMap).keySet() + " != " + valueOrderMap(fieldMap).keySet();
					LOGGER.warn("file-mapping does not match expected mapping: " + mismatch);
				}
			}
			return new CSVRows(csvReader, fm);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static <K, V extends Comparable<?>> Map<K, V> valueOrderMap(Map<K, V> m) {
		List<Map.Entry<K, V>> valueOrder = new ArrayList<Map.Entry<K,V>>(m.entrySet());
		Collections.sort(valueOrder, new Comparator<Map.Entry<K, V>>() {
			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public int compare(Entry<K, V> o1, Entry<K, V> o2) {
				Comparable c1 = o1.getValue();
				Comparable c2 = o2.getValue();
				return c1.compareTo(c2);
			}
		});
		Map<K, V> valueOrderMap = new LinkedHashMap<K, V>();
		for (Entry<K, V> entry : valueOrder) {
			valueOrderMap.put(entry.getKey(), entry.getValue());
		}
		return valueOrderMap;
	}
	
	private static Map<String, Integer> getFieldMap(String[] headers) {
		Map<String, Integer> mapping = new LinkedHashMap<String, Integer>();
		for (int i = 0; i < headers.length; i++) {
			mapping.put(headers[i], i);
		}
		return mapping;
	}

	/**
	 * <p>
	 * Represents a row in a CSV-File.
	 * </p>
	 * Columns may be accessed trough index or name.
	 * Which name maps to which index is determined by the specified mapping.
	 */
	public static class EntryRow implements Iterable<String> {
		
		private final List<String> _data;
		
		private final Map<String, Integer> _fieldMap;
		
		private final long _lineNumber;
		
		public EntryRow(String[] data, Map<String, Integer> fieldMap, long lineNumber) {
			_data = Arrays.asList(data);
			_fieldMap = fieldMap;
			_lineNumber = lineNumber;
		}
		
		/**
		 * Gets the corresponding value for the the specified name.
		 * @param name the name of a column
		 * @return
		 */
		public String get(String name) {
			Integer index = _fieldMap.get(name);
			if (index == null) {
				LOGGER.warn("Unknown field with name " + name, new Exception());
				
				return null;
			}
			return get(index);
		}
		
		/**
		 * Gets the corresponding value for the specified index.
		 * @param index
		 * @return
		 */
		public String get(int index) {
			if (index >= _data.size()) {
				LOGGER.warn("index out of bounds " + index +  " > " + _data.size(), new Exception());
				
				return null;
			}
			return _data.get(index);
		}

		/**
		 * Returns true, if the specified name corresponds to a column and the column is contained within this row. 
		 * @param name the name of a column
		 * @return true, if the specified name corresponds to a column and the column is contained within this row.
		 */
		public boolean contains(String name) {
			Integer index = _fieldMap.get(name);
			return index != null && index < size();
		}
		
		/**
		 * Returns how many columns this row contains
		 * @return the number of columns this row contains
		 */
		public int size() {
			return _data.size();
		}
		
		@Override
		public Iterator<String> iterator() {
			return _data.iterator();
		}
		
		/**
		 * Note that the line number does not necessarily represent the effective line number
		 * of the read file as multiline records are counted as one line. 
		 * @return the line number of this entry
		 */
		public long getLineNumber() {
			return _lineNumber;
		}
		
		@Override
		public String toString() {
			Map<String, String> m = new LinkedHashMap<String, String>();
			for (String field : _fieldMap.keySet()) {
				m.put(field, get(field));
			}
			return "line " + getLineNumber() + " " + m.toString();
		}
		
	}
	
	public static class CSVRows implements Iterable<EntryRow>, Closeable {
		
		private final CSVReader _r;
		
		private final Map<String, Integer> _fieldMap;
		
		private boolean _hasHadItr = false;
		
		private CSVRows(CSVReader r, Map<String, Integer> fieldMap) {
			_r = r;
			_fieldMap = fieldMap;
		}

		@Override
		public Iterator<EntryRow> iterator() {
			if (_hasHadItr) throw new IllegalStateException("can only request one iterator per CSVRows instance");
			
			_hasHadItr = true;
			return new Itr();
		}
		
		private class Itr implements Iterator<EntryRow> {

			private String[] _nextData;
			
			private long _lineNumber = 1; // first row is header row
			
			private Itr() {
				advance();
			}
			
			private void advance() {
				try {
					_nextData = _r.readNext();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
			
			@Override
			public boolean hasNext() {
				return _nextData != null;
			}

			@Override
			public EntryRow next() {
				String[] nextData = _nextData;
				if (nextData == null) throw new NoSuchElementException();
				
				advance();
				return new EntryRow(nextData, _fieldMap, ++_lineNumber);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
			
		}

		@Override
		public void close() {
			try {
				_r.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		
	}
	
}
