package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.Consts;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

public class BOAnforderung extends BOObject {

	public static final String CN = "Anforderung";
	
	private static final Pattern OID_PATTERN = Pattern.compile("\\d{9}");
	
	static ILogger LOGGER = Logger.getLogger("boxalino.edvkg.anforderungen");
	

	public BOAnforderung(BLTransaction trans, long oid) {
		super(trans, oid);
	}
	
	@Override
	public String getAttribute(String attrName, BOContext context) throws XMetaModelQueryException, XSecurityException {
		if ("mldWithUrsprung".equals(attrName)) {
			List<BOMeldung> meldungen = getMeldungen(context);
			if (!meldungen.isEmpty()) {
				BOMeldung meldung = meldungen.get(0);
				String mld = meldung.getMld(context);
				BOObject ph = meldung.getReferencedObject("ursprung", context);
				if (ph == null) return mld;
				
				long ursprung = ph.getReferencedOID("OIDPerson", context);
				return OID_PATTERN.matcher(mld).replaceFirst("" + ursprung);
			}
		}
		return super.getAttribute(attrName, context);
	}
	
	@SuppressWarnings("unchecked")
	private List<BOMeldung> getMeldungen(BOContext context) {
		try {
			return (List<BOMeldung>) getListReferencedObjects("meldungen", context);
		} catch (XMetaModelQueryException e) {
			throw new RuntimeException(e);
		} catch (XSecurityException e) {
			throw new RuntimeException(e);
		}
	}
	
	public final static BOAnforderung createAnforderung(BLTransaction trans) throws XMetaModelQueryException, XSecurityException {
		BOAnforderung boAnf = (BOAnforderung) trans.createNewObject("Anforderung");
		boAnf.setValue(Consts.Anforderung_datum, new Date());
		return boAnf;
	}
	
	public int getLaufnummer() throws XMetaModelNotFoundException, XSecurityException {
		Object laufnummer = getValue("laufnummer");
		return laufnummer == null ? -1 : ((Number) laufnummer).intValue();
	}

}
