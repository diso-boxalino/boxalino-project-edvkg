package ch.objcons.ecom.proj.edvkg.mzv;


public class OhneAnrechtWeil {
	
	private final int _id;
	
	private final String _code;
	
	public OhneAnrechtWeil (int id, String code) {
		_id = id;
		_code = code;
	}
	
	public int getID() {
		return _id;
	}
	
	public String getCode() {
		return _code;
	}
	
	public static OhneAnrechtWeil getGemaessID(int id) {
		for (OhneAnrechtCode oac : OhneAnrechtCode.values()) {
			OhneAnrechtWeil oaw = oac.getOAW();
			if (oaw.getID() == id) {
				return oaw; 
			}
		}
		return new OhneAnrechtWeil(id, "" + id);
	}
	
}
