package ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo;

import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOObject;

public class BOSchulHaus extends BOObject {

	public static final String CLASS_NAME = "SchulHaus";
	
	public BOSchulHaus(BLTransaction arg0, long arg1) {
		super(arg0, arg1);
	}
	
}
