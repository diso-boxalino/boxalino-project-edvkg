package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.sql.Connection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import ch.objcons.db.dbimport.XMappingException;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.datatypes.EDataTypeValidation;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.FeatureAttribute;
import ch.objcons.ecom.meta.MetaAttribute;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.MetaService;
import ch.objcons.ecom.meta.SimpleAttribute;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.BOPersonHistory;
import ch.objcons.ecom.proj.edvkg.mzv.UtilsEDVKG;
import ch.objcons.ecom.query.StandardQuery;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.log.Log;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;
import com.boxalino.log.Stacktrace;


/**
 * @deprecated nicht mehr in Gebrauch - nur noch zum Vergleich mit dem alten Code
 * @author Matthias Humbert
 *
 */
public class ImportRoulesEDVKG {
	static ILogger LOGGER = Logger.getLogger("ch.objcons.ecom.proj.edvkg.import");

	/** max number of possible columns */
	public static final int MAX_COLUMS = 1000;

	/** definition how the rows are in order */
	private MapIndexEDVKG _mapIndex;
	private Hashtable _defaultValues;
	private BLTransaction _trans;
	private BOContext _context;
	private MetaObject _metaObject;
	private MetaService _metaService;
	// private MetaObject _metaObjectZeitung;
	private TableMappingEDVKG _tableMapping;

	/** Mapping of the base datas */
	private Mapping _mapBase;
	private Mapping _mapBasePLZ;

	/** Mapping table for the erstanlieferung */
	private Mapping _mapErstanlieferung;
	private Mapping _mapErstanlieferungPLZ;

	/** Mapping for tze zustelladresse */
	private Mapping _mapZustell;
	private Mapping _mapZustellPLZ;
	private Mapping _mapZustellAAMU;
	private Mapping _mapZustellPLZAAMU;

	/** Mapping for the wegzug */
	private Mapping _mapWegzug;
	private Mapping _mapWegzugPLZ;

	/** spez codes tables */
	private HashMap codeKG;

	/** variables to set the OIDs */
	private static final int MAX_NUMBER_OF_OID = 10;
	private long _nextOID = 0;
	private long _maxOID = 0;

	private int _zusOIDStrasseIndex;
	private int _zusStrassenNameIndex;
	private int _zusHausNrIndex;
	private int _zusAdrZusatzIndex;
	private int _zusPLZOrtIndex;
	private int _zusPLZOrtEindIndex;
	private int _zusOrtIndex;
	private int _zusOIDCodeOrtLandIndex;
	private int _zusOrtLandNameIndex;
	private int _oidStrasseIndex;
	private int _zusPostfachIndex;
	private int _zusPLZPostfachIndex;
	private int _zusPLZPostfachEindIndex;
	private int _zusArtIndex;

	/**
	 * Class for the Roules how to import the EDVKG rows into the PersonenHistory.
	 */
	public ImportRoulesEDVKG (MapIndexEDVKG mapIndex,
			Hashtable defaultValues,
			BLTransaction trans,
			BOContext context,
			MetaService metaService,
			MetaObject metaObject,
			TableMappingEDVKG tableMapping) throws XMetaModelNotFoundException, XMetaModelQueryException, XSecurityException {
		_mapIndex = mapIndex;
		_defaultValues=defaultValues;
		_trans = trans;
		_context = context;
		_metaService = metaService;
		_metaObject = metaObject;
		_tableMapping = tableMapping;
		// _metaObjectZeitung = metaService.getClassForName("Zeitung");

		this.setMapBase();
		this.setMapErstanlieferung();
		this.setMapZustell();
		this.setMapZustellAAMU();
		this.setMapWegzug();
		this.setCodeKG();
		
		// Zus�tzliche Initialisierungen
		MetaAttribute ma = _metaObject.getAllAttributeForName("ZusOIDStrasse", _context);
		_zusOIDStrasseIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusStrassenName", _context);
		_zusStrassenNameIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusHausNr", _context);
		_zusHausNrIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusAdrZusatz", _context);
		_zusAdrZusatzIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusPLZOrt", _context);
		_zusPLZOrtIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusPLZOrtEind", _context);
		_zusPLZOrtEindIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusOrt", _context);
		_zusOrtIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusOIDCodeOrtLand", _context);
		_zusOIDCodeOrtLandIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusOrtLandName", _context);
		_zusOrtLandNameIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("OIDCodeStrasse", _context);
		_oidStrasseIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusPostfach", _context);
		_zusPostfachIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusPLZPostfach", _context);
		_zusPLZPostfachIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusPLZPostfachEind", _context);
		_zusPLZPostfachEindIndex = ma.getIndex();
		ma = _metaObject.getAllAttributeForName("ZusArt", _context);
		_zusArtIndex = ma.getIndex();
	}

	/**
	 * Sets the Zustelladresse.
	 * @param record
	 * @param boObj
	 * @throws XMetaException
	 * @throws XDataTypeException
	 * @throws XSecurityException
	 */
	public void setZustellValues(Vector record, BOObject boObj) throws XMetaException, XDataTypeException, XSecurityException {
		boolean zustellIsExtern = isZustellAAMU(record);
		boolean zustellIsStadt = isZustellStadt(record);
		if (zustellIsExtern && zustellIsStadt) LOGGER.info("Importierte Adressen (OID=" + boObj.getAttribute("OIDPerson", false, _context) + ") hat stadt-externe und stadt-interne Zustelladresse");
		if (zustellIsExtern) {
			/* The suggestion is that every field of the zustelladresse is
			 * given
			 */
			this.setMappedValues(_mapZustellAAMU, record, boObj);
			this.setPLZValues(_mapZustellPLZAAMU, record, boObj);
			boObj.setValue(_zusOIDStrasseIndex, null, _context);
			boObj.setValue(_zusOrtLandNameIndex, null, _context);
			boObj.setValue(_zusArtIndex, "e", _context);
			/* correct CodeOrtLand */
			Number oidCodeOrtLand = (Number)boObj.getValue(_zusOIDCodeOrtLandIndex, false, _context);
			if (oidCodeOrtLand.longValue() < UtilsEDVKG.COUNTRY_MIN_VALUE) { 
				boObj.setValue(_zusOIDCodeOrtLandIndex, null, _context);
			}
		} else {
			/* Zustelladresse ist stadt-intern oder gar nicht gesetzt */
			this.setMappedValues(_mapZustell, record, boObj); 
			this.setPLZValues(_mapZustellPLZ, record, boObj); 
			/* Set fields to null because they may contain (invalid) 
			 * values of an external address */
			boObj.setValue(_zusStrassenNameIndex, null, _context);
			boObj.setValue(_zusPLZOrtIndex, null, _context);
			boObj.setValue(_zusPLZOrtEindIndex, null, _context);
			boObj.setValue(_zusOrtIndex, null, _context);
			boObj.setValue(_zusOIDCodeOrtLandIndex, null, _context);
			boObj.setValue(_zusOrtLandNameIndex, null, _context);
			if (zustellIsStadt) {
				/* ZusPLZOrt, ZusPLZOrtEind and ZusOrt are set in the Consistency Check */
				boObj.setValue(_zusArtIndex, "s", _context);
			} else {
				boObj.setValue(_zusArtIndex, null, _context);
			}
		}
	}
	
	static void setRecordError(char errorTyp, String errorText, BOObject bo, BOContext context) throws XMetaException, XDataTypeException, XSecurityException {
		String recordErrorTyp = bo.getAttribute("RecordError", false, context);
		if (EDataTypeValidation.isNullOrEmptyString(recordErrorTyp)) {
			bo.setAttribute("RecordError", "" + errorTyp, context, null);
			bo.setAttribute("RecordErrorText", errorText, context, null);
		}
	}
	
	/**
	 * Is the Zustelladresse stadt-extern ?
	 */
	public boolean isZustellAAMU(Vector record) {
		// Beurteilungskriterium: Ist der ausw�rtige Zustellort gesetzt?
		String zOrt = (String)record.elementAt(_mapIndex._AAD_Ortsbezeichnung);
		return !EDataTypeValidation.isNullOrEmptyString(zOrt);
	}
	
	/**
	 * Is the imported Zustelladresse stadt-intern ?
	 */
	public boolean isZustellStadt(Vector record) {
		// Beurteilungskriterium: Ist die Orts-PLZ oder die Postfach-PLZ gesetzt?
		String zPLZPostfach = (String)record.elementAt(_mapIndex._ZUS_PLZPostfach);
		String zOIDStrasse = (String)record.elementAt(_mapIndex._ZUS_Strassennummer);
		return !EDataTypeValidation.isNullOrEmptyString(zPLZPostfach) ||
			!EDataTypeValidation.isNullOrEmptyString(zOIDStrasse);
	}

	/**
	 * Provides information about changing Zustelladresse for the user
	 */
	public void doZustellAlert(BOObject oldBo, BOObject newBo, boolean umzug) {
		try {
			String oldZustell = oldBo.getAttribute(_zusArtIndex, false, _context);
			boolean oldZustellIsEnteredManually = "m".equals(oldZustell); 
			if (oldZustellIsEnteredManually) {
				String newZustell = newBo.getAttribute(_zusArtIndex, false, _context);
				boolean newZustellIsExtern = "e".equals(newZustell);
				boolean newZustellIsStadt = "s".equals(newZustell);
				String alertText;
				if (newZustellIsExtern) {
					alertText = "Ausw�rtige Zustelladr. �berschreibt manuelle";
				} else if (newZustellIsStadt) {
					alertText = "St�dtische Zustelladr. �berschreibt manuelle";
				} else /* No new Zustell */ {
					if (umzug) return;
					UtilsEDVKG.copyExternalHistoryZustell(oldBo, newBo, _context);
					alertText = "L�schen manueller Zustelladr. verhindert";
				}
				setRecordError('Z', alertText, newBo, _context);
			}
		} catch (Exception e) {
			Log.logAlarm(LOGGER, "Zustell-Alert: " + e.getMessage(), e);
		}
	}
	
	/**
	 * we just compute the stuff for an Erstanlieferung
	 */
	public BOObject erstanlieferung(Vector record) throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		// set the default Values
		BOObject boObject = this.createPerson();

		/* set attribute values */
		this.setMappedValues(_mapBase, record, boObject);
		this.setMappedValues(_mapErstanlieferung, record, boObject);
		
		this.setCodeKGValues (record, boObject);

		// set the short values of the PLZ
		this.setPLZValues(_mapBasePLZ, record, boObject);
		this.setPLZValues(_mapErstanlieferungPLZ, record, boObject);
		// PLZOrt, PLZOrtEind and Ort are set in the consistency check

		this.setZustellValues(record, boObject);
		// this.setOID(boObject);
		// this.setZeitung(record, boObject);
		boObject.setValue("isLast", new Boolean(true), _context);

		this.setZuzKGDatum(boObject);
		return boObject;
	}

	/**
	 * compute the Geburt
	 */
	public BOObject geburt(Vector record)  throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		// the Geburt is equal to the erstanlieferung
		BOObject newBoObject = this.erstanlieferung(record);
		return newBoObject;
	}

	/**
	 * compute the Konfessionswechsel, Zivistandswechsel oder Aufenthaltsort
	 */
	public BOObject konfZiviAuf(Vector record, Connection con)  throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		BOObject oldBoObject;
		BOObject newBoObject;
		// get the old Object
		oldBoObject = this.getBaseObject(record, con);

		if (oldBoObject != null) {
			// dublicate the Object
			newBoObject = clonePerson(oldBoObject);
			oldBoObject.setValue("lastEntry", new Boolean(false), _context);
			oldBoObject.setValue("StatusPerson", new Boolean(false), _context);
			oldBoObject.setValue("isLast", new Boolean(false), _context);
			newBoObject.setValue("isLast", new Boolean(true), _context);
			newBoObject.setValue("RecordError", null, _context);
			newBoObject.setValue("RecordErrorText", null, _context);

			/* set attribute values */
			this.setMappedValues(_mapBase, record, newBoObject);
			// set the short values of the PLZ
			this.setPLZValues(_mapBasePLZ, record, newBoObject);
			// PLZOrt, PLZOrtEind and Ort are set in the consistency check
			this.setZustellValues(record, newBoObject);
			// this.setOID(newBoObject);
			boolean isUmzug = this.setUmzugAdr(oldBoObject, newBoObject, con);
			doZustellAlert(oldBoObject, newBoObject, isUmzug);
			
			this.setCodeKGValues (record, newBoObject);

			// set origKG -> konf-Wechsel!!!!
			if (EDataTypeValidation.isNullString(newBoObject.getAttribute("OIDKG", _context))) {
				String origKG = oldBoObject.getAttribute("OIDKG", _context);
				if (!EDataTypeValidation.isNullString(origKG)) {
					newBoObject.setAttribute("OIDKGOrig", origKG, _context, null);
				}
			}
		}
		else {
			// if we had find no record, just do the erstanlieferung
			newBoObject = this.erstanlieferung(record);
			// Mit der Familienzusammensetzung kann eine neue Person erfasst werden.
			// this.setError(record, "The OID was not nown in the table");
		}
		return newBoObject;
	}

	/**
	 * compute the standard Vorfall
	 */
	public BOObject stdVorfall(Vector record, Connection con)  throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		BOObject oldBoObject;
		BOObject newBoObject;
		// get the old Object
		oldBoObject = this.getBaseObject(record, con);

		if (oldBoObject != null) {
			// dublicate the Object
			newBoObject = clonePerson(oldBoObject);
			oldBoObject.setValue("lastEntry", new Boolean(false), _context);
			oldBoObject.setValue("StatusPerson", new Boolean(false), _context);
			oldBoObject.setValue("isLast", new Boolean(false), _context);
			newBoObject.setValue("isLast", new Boolean(true), _context);
			newBoObject.setValue("RecordError", null, _context);
			newBoObject.setValue("RecordErrorText", null, _context);

			/* set attribute values */
			this.setMappedValues(_mapBase, record, newBoObject);
			// set the short values of the PLZ
			this.setPLZValues(_mapBasePLZ, record, newBoObject);
			this.setZustellValues(record, newBoObject);
			// PLZOrt, PLZOrtEind and Ort are set in the consistency check
			
			// this.setOID(newBoObject);
			boolean isUmzug = this.setUmzugAdr(oldBoObject, newBoObject, con);
			
			// set recorderror if Zustelladresse creates conflict with ZusStrassenName
			this.doZustellAlert(oldBoObject, newBoObject, isUmzug);
			
			this.setCodeKGValues (record, newBoObject);

			// set origKG -> konf-Wechsel!!!!
			if (EDataTypeValidation.isNullString(newBoObject.getAttribute("OIDKG", _context))) {
				String origKG = oldBoObject.getAttribute("OIDKG", _context);
				if (!EDataTypeValidation.isNullString(origKG)) {
					newBoObject.setAttribute("OIDKGOrig", origKG, _context, null);
				}
			}
		}
		else {
			// if we had find no record, just do the erstanlieferung
			newBoObject = this.erstanlieferung(record);
			newBoObject.setAttribute("RecordError", "F", _context, null);
			newBoObject.setAttribute("RecordErrorText", "Diese Person war noch nicht erfasst", _context, null);
//			this.setError(record, "The OID was not nown in the table");
		}
		return newBoObject;
	}

	/**
	 * compute the  Wegzug, Tod
	 */
	public BOObject wegTodRueck(Vector record, Connection con)  throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		BOObject oldBoObject;
		BOObject newBoObject;
		String meldeDatum;
		// get the old Object
		oldBoObject = this.getBaseObject(record, con);

		if (oldBoObject != null) {
			// dublicate the Object
			newBoObject = clonePerson(oldBoObject);
			oldBoObject.setValue("lastEntry", new Boolean(false), _context);
			oldBoObject.setValue("StatusPerson", new Boolean(false), _context);
			oldBoObject.setValue("isLast", new Boolean(false), _context);
			newBoObject.setValue("isLast", new Boolean(true), _context);
			newBoObject.setValue("RecordError", null, _context);
			newBoObject.setValue("RecordErrorText", null, _context);

			/* set attribute values */
			this.setMappedValues(_mapBase, record, newBoObject);
	    	this.setMappedValues(_mapWegzug, record, newBoObject);
			// set the short values of the PLZ
			this.setPLZValues(_mapBasePLZ, record, newBoObject);
			// PLZOrt, PLZOrtEind and Ort are set in the consistency check
	    	this.setPLZValues(_mapWegzugPLZ, record, newBoObject);
			// this.setOID(newBoObject);

			this.setCodeKGValues (record, newBoObject);

			// set the Wegzugsdatum of the KG
			meldeDatum = newBoObject.getAttribute("DatVorfall", _context);
			newBoObject.setAttribute("WegKGDatum", meldeDatum, _context, null);
			newBoObject.setAttribute("WegOIDKG", "", _context, null);
			
			// set origKG -> konf-Wechsel!!!!
			if (EDataTypeValidation.isNullString(newBoObject.getAttribute("OIDKG", _context))) {
				String origKG = oldBoObject.getAttribute("OIDKG", _context);
				if (!EDataTypeValidation.isNullString(origKG)) {
					newBoObject.setAttribute("OIDKGOrig", origKG, _context, null);
				}
			}
		}
		else {
			// if we had find no record, just do the erstanlieferung
			newBoObject = this.erstanlieferung(record);
			newBoObject.setAttribute("RecordError", "F", _context, null);
			newBoObject.setAttribute("RecordErrorText", "Diese Person war noch nicht erfasst", _context, null);
//			this.setError(record, "The OID was not nown in the table");
		}
		return newBoObject;
	}

	/**
	 * compute the Zuzug
	 */
	public BOObject zuzug(Vector record)  throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		// wie eine erstanlieferung
		BOObject newBoObject = this.erstanlieferung(record);
		this.setZuzKGDatum(newBoObject);
		return newBoObject;
	}


	/**
	 * compute the Umzug in the Town
	 */
	public BOObject umzug(Vector record, Connection con)  throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		BOObject oldBoObject;
		BOObject newBoObject = null;
		String meldeDatum;
		// set the old Values
		oldBoObject = this.getBaseObject(record, con);

		if (oldBoObject != null) {
			// dublicate the Object
			newBoObject = clonePerson(oldBoObject);
			oldBoObject.setValue("lastEntry", new Boolean(false), _context);
			oldBoObject.setValue("StatusPerson", new Boolean(false), _context);
			newBoObject.setValue("RecordError", null, _context);
			newBoObject.setValue("RecordErrorText", null, _context);

			/* set attribute values, same like erstanlieferung, just that we keep the values*/
			this.setMappedValues(_mapBase, record, newBoObject);
			this.setMappedValues(_mapErstanlieferung, record, newBoObject);
			// set the short values of the PLZ
			this.setPLZValues(_mapBasePLZ, record, newBoObject);
			this.setPLZValues(_mapErstanlieferungPLZ, record, newBoObject);
			// PLZOrt, PLZOrtEind and Ort are set in the consistency check
			this.setZustellValues(record, newBoObject);
			// set recorderror if Zustelladresse creates conflict with ZusStrassenName
			this.doZustellAlert(oldBoObject, newBoObject, true);
			
			// set the umzugAdr in the old and new Obj
			this.setUmzugAdr(oldBoObject, newBoObject, con); //also the isLast will be set if needet

			this.setCodeKGValues (record, newBoObject);

			// set origKG -> konf-Wechsel!!!!
			if (EDataTypeValidation.isNullString(newBoObject.getAttribute("OIDKG", _context))) {
				String origKG = oldBoObject.getAttribute("OIDKG", _context);
				if (!EDataTypeValidation.isNullString(origKG)) {
					newBoObject.setAttribute("OIDKGOrig", origKG, _context, null);
				}
			}
		} else {
			// if we had find no record, just do the erstanlieferung
			newBoObject = this.erstanlieferung(record);
			this.setZuzKGDatum(newBoObject);
		}

		return newBoObject;
		/* // wie der Standard
		return this.stdVorfall(record); */
	}


	/**
	 * compute the Wiederzuzug
	 */
	public BOObject wiederZuzug(Vector record, Connection con)  throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		BOObject oldBoObject;
		BOObject newBoObject = null;
		// set the old Values
		oldBoObject = this.getBaseObject(record, con);

		if (oldBoObject != null) {
			// dublicate the Object
			newBoObject = clonePerson(oldBoObject);
			oldBoObject.setValue("lastEntry", new Boolean(false), _context);
			oldBoObject.setValue("StatusPerson", new Boolean(false), _context);
			oldBoObject.setValue("isLast", new Boolean(false), _context);
			newBoObject.setValue("isLast", new Boolean(true), _context);
			newBoObject.setValue("RecordError", null, _context);
			newBoObject.setValue("RecordErrorText", null, _context);

			/* set attribute values, same like erstanlieferung, just that we keep the values*/
			this.setMappedValues(_mapBase, record, newBoObject);
			this.setMappedValues(_mapErstanlieferung, record, newBoObject);
			// set the short values of the PLZ
			this.setPLZValues(_mapBasePLZ, record, newBoObject);
			this.setPLZValues(_mapErstanlieferungPLZ, record, newBoObject);
			// PLZOrt, PLZOrtEind and Ort are set in the consistency check
			
			this.setZustellValues(record, newBoObject);
			// this.setOID(newBoObject);
			this.setZuzKGDatum(newBoObject);
			
			this.setCodeKGValues (record, newBoObject);

			// set origKG -> konf-Wechsel!!!!
			if (EDataTypeValidation.isNullString(newBoObject.getAttribute("OIDKG", _context))) {
				String origKG = oldBoObject.getAttribute("OIDKG", _context);
				if (!EDataTypeValidation.isNullString(origKG)) {
					newBoObject.setAttribute("OIDKGOrig", origKG, _context, null);
				}
			}
		} else {
			// if we had find no record, just do the erstanlieferung
			newBoObject = this.erstanlieferung(record);
			this.setZuzKGDatum(newBoObject);
		}
		return newBoObject;
	}

	/**
	 * just set ZuzugsdatumKG
	 */
	private void setZuzKGDatum(BOObject newBoObject) throws XMetaException, XSecurityException, XDataTypeException {
		String meldeDatum;
		meldeDatum = newBoObject.getAttribute("DatVorfall", _context);
		newBoObject.setAttribute("ZuzKGDatum", meldeDatum, _context, null);
	}

	/**
	 * set sthe zuzug / Wegzug KG and Datum
	 */
	private boolean setUmzugAdr(BOObject oldBoObject, BOObject newBoObject, Connection con) throws XMetaException, XSecurityException, XDataTypeException  {
		String meldeDatum;
		BOObject tmpObj;
		boolean isUmzug = false;

		String oldKG = oldBoObject.getAttribute("OIDKG", _context);
		String oldStr = oldBoObject.getAttribute("OIDCodeStrasse", _context);
		String oldStrNr = oldBoObject.getAttribute("HausNr", _context);
		String newKG = newBoObject.getAttribute("OIDKG", _context);
		String newStr = newBoObject.getAttribute("OIDCodeStrasse", _context);
		String newStrNr = newBoObject.getAttribute("HausNr", _context);

		if (oldKG == null) oldKG = "";
		if (oldStr == null) oldStr = "";
		if (oldStrNr == null) oldStrNr = "";
		if (newKG == null) newKG = "";
		if (newStr == null) newStr = "";
		if (newStrNr == null) newStrNr = "";

		if ( !(oldStr.equals(newStr) && (oldStrNr.equals(newStrNr)) ) ) {
			// we have to set the zuzug and Wegzug str
			// Set the zuzugAdresse
			isUmzug = true;
			newBoObject.setAttribute("ZuzAdrZusatz", oldBoObject.getAttribute("AdrZusatz", _context), _context, null);
			newBoObject.setAttribute("ZuzPLZOrtEind", oldBoObject.getAttribute("PLZOrtEind", _context), _context, null);
			newBoObject.setAttribute("ZuzPLZOrt", oldBoObject.getAttribute("PLZOrt", _context), _context, null);
			newBoObject.setAttribute("ZuzOrt", oldBoObject.getAttribute("Ort", _context), _context, null);
			newBoObject.setAttribute("ZuzHausNr", oldBoObject.getAttribute("HausNr", _context), _context, null);
			tmpObj = oldBoObject.getReferencedObject("OIDCodeStrasse", _context);
			newBoObject.setAttribute("ZuzStrassenName", tmpObj != null ? tmpObj.getAttribute("Name", _context) : null, _context, null);
			newBoObject.setAttribute("ZuzPLZPostfachEind", oldBoObject.getAttribute("PLZPostfachEind", _context), _context, null);
			newBoObject.setAttribute("ZuzPLZPostfach", oldBoObject.getAttribute("PLZPostfach", _context), _context, null);
			newBoObject.setAttribute("ZuzPostfach", oldBoObject.getAttribute("Postfach", _context), _context, null);
			newBoObject.setAttribute("ZuzPostlagernd", oldBoObject.getAttribute("Postlagernd", _context), _context, null);
			newBoObject.setAttribute("ZuzPostfach", oldBoObject.getAttribute("Postfach", _context), _context, null);
			// set the Wegzugsadress
			oldBoObject.setAttribute("WegAdrZusatz", newBoObject.getAttribute("AdrZusatz", _context), _context, null);
			/* Ort und PLZ werden erst im Konsistenz-Check ge-updatet */
			String hausNr = newBoObject.getAttribute("HausNr", _context);
			oldBoObject.setAttribute("WegHausNr", hausNr, _context, null);
			tmpObj = newBoObject.getReferencedObject(_oidStrasseIndex, false, _context);
			setPLZAndOrt(oldBoObject, "Weg", tmpObj != null ? tmpObj.getOID() : -1, hausNr, con);
			oldBoObject.setAttribute("WegStrassenName", tmpObj.getAttribute("Name", _context), _context, null);
			oldBoObject.setAttribute("WegPLZPostfachEind", newBoObject.getAttribute("PLZPostfachEind", _context), _context, null);
			oldBoObject.setAttribute("WegPLZPostfach", newBoObject.getAttribute("PLZPostfach", _context), _context, null);
			oldBoObject.setAttribute("WegPostfach", newBoObject.getAttribute("Postfach", _context), _context, null);
			oldBoObject.setAttribute("WegPostlagernd", newBoObject.getAttribute("Postlagernd", _context), _context, null);
			oldBoObject.setAttribute("WegPostfach", newBoObject.getAttribute("Postfach", _context), _context, null);
			oldBoObject.setAttribute("Vorfallart", "UMZ", _context, null);

		}
		
		if (oldKG.equals(newKG)) {
			// that was not a change in the kg, so we do nothing exept set the islast
			oldBoObject.setValue("isLast", new Boolean(false), _context);
			newBoObject.setValue("isLast", new Boolean(true), _context);
		}
		else {
			// the old an the new will be active
			newBoObject.setValue("isLast", new Boolean(true), _context);

			// check if the are some old one (KG1 -> KG2 -> KG1)
			if (oldKG.equals("") || newKG.equals("")) {
				oldBoObject.setValue("isLast", new Boolean(false), _context);
			}
			else {
				String query = "select * from bx_personhistory where bx_islast='j' and bx_oidkg="+newKG+" and bx_oidperson="+newBoObject.getAttribute("OIDPerson", _trans.getContext())+" order by OID desc";
				Vector result = BLManager.getResult(_trans, query, true /* isAlreadyFiltered */, _context);
				if (result.size() > 0) {
					// it is the first Element
					BOObject boObject = (BOObject)result.elementAt(0);
					boObject.setValue("isLast", new Boolean(false), _context);
				}
			}
			
			// Set the Zuzug and Wegzug KG
			meldeDatum = newBoObject.getAttribute("DatVorfall", _context);
			newBoObject.setAttribute("ZuzKGDatum", meldeDatum, _context, null);
			newBoObject.setAttribute("ZuzOIDKG", oldKG, _context, null);
			oldBoObject.setAttribute("WegKGDatum", meldeDatum, _context, null);
			oldBoObject.setAttribute("WegOIDKG", newKG, _context, null);
		}
		return isUmzug;
	}

	/**
	 * get the corresponding object for the current record from the database
	 */
	private BOObject getBaseObject(Vector record, Connection con) throws XMetaModelQueryException{
		return getBaseObject(record, con, true);
	}
	
	private BOObject getBaseObject(Vector record, Connection con, boolean islast) throws XMetaModelQueryException{
		String persoid = (String)record.elementAt(_mapIndex._OIDPerson);
		String query = "select * from bx_personhistory where " +
				"bx_OIDPerson = " + persoid;
		if (islast) query += " and bx_islast='j'";
		query += " order by oid desc";
		// System.out.println("Query getBaseObject = <"+query+">");
		Vector result = BLManager.getResult(_trans, query, true /* isAlreadyFiltered */, _trans.getContext(), con);
		if (result.size() > 0) {
			// it is the first Element
			return (BOObject)result.elementAt(0);
		} else if (islast) {
			BOObject histObj = getBaseObject(record, con, false);
			if (histObj != null) Log.logAlarm(LOGGER, "Import: Person (ZIP: " + persoid + ") hat keinen letzten History-Eintrag", new Stacktrace());
			return histObj;
		}
		//Log.logSystemAlarm("Konnte keinen History-Eintrag der Person mit ZIP=" + persoid + " zur�ckgeben");
		return null;
	}

	private void setPLZAndOrt(BOObject bo, String prefix, long oidStrasse, String hausNr, Connection con) throws XSecurityException, XMetaException, XDataTypeException{
		if (hausNr == null || oidStrasse <= 0) return;
		String query = "select * from bx_codehaus where " +
				"bx_OIDStrasse = " + oidStrasse + " and bx_HausNr = '" + hausNr + "' order by oid desc";
		Vector result = BLManager.getResult(_trans, query, true /* isAlreadyFiltered */, _trans.getContext(), con);
		if (result.size() > 0) {
			// it is the first Element
			BOObject addressObj = (BOObject)result.elementAt(0);
			String plzEind = addressObj.getAttribute("Postleizahl", false, _context);
			bo.setAttribute(prefix + "PLZOrtEind", plzEind, _context, null);
			bo.setAttribute(prefix + "PLZOrt", plzEind.substring(0, 4), _context, null);
			bo.setAttribute(prefix + "Ort", "Z�rich", _context, null);
		}
	}
	/* private void deactivate(BOObject oldObj) {
		BOObject boObject = null;

		String query = "select * from bx_person where " +
				 "bx_OIDPerson = "+record.elementAt(oldObj.getAttribute("OIDPerson", _context)+" "+
				"order by oid desc";

		Vector result = BLManager.getResult(_trans, query, true , _context);
		if (result.size() > 0) {
			// it is the first Element
			boObject = (BOObject)result.elementAt(0);
		}
		oldObj.setValue("StatusPerson", new Boolean(false));

	} */

	private void setDefaultValues(BOObject pers) throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		// set default values
		if (_defaultValues != null) {
			Enumeration values = _defaultValues.keys();
			while (values.hasMoreElements()) {
				String key = (String) values.nextElement();
				pers.setAttribute(key, (String)_defaultValues.get(key), _context, null);
			}
		}
	}
	
	private BOObject clonePerson(BOObject pers) throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		BOObject newBoObject = new BOPersonHistory(pers.getTransaction(), 0);
		newBoObject.setClonedMembers(pers);
		// set the default Values
		setDefaultValues(newBoObject);
		return newBoObject;
	}

	/**
	 * get a default Object with the default values to put into the database
	 */
	private BOObject createPerson() throws XMappingException, XMetaException, XDataTypeException, XSecurityException {
		BOObject boObject = _trans.getObject(_metaObject, 0, false /* isPartOf */);
		setDefaultValues(boObject);
		return boObject;
	}


	/**
	 * set the error in erro table
	 */
	private void setError(Vector record, String error) {
		Log.logAlarm(LOGGER, "Fehler im Import in Record : "+record.elementAt(_mapIndex._OIDPerson)+", Fehler: "+error);
	}

	/**
	 * set the given Value at the defined position in the boObject
	 */
	/*private void setValue(String value, int to, BOObject boObject) {
		if (_mapIndex._metaType[to] instanceof FeatureAttribute) {
			boObject.setAttribute(to, value, _context, null);
		} else if (_mapIndex._metaType[to] instanceof SimpleAttribute) {
			value = _tableMapping.preprocessValue((SimpleAttribute)(_mapIndex._metaType[to]), value);
			boObject.setAttribute(to, value, _context, null);
		} else {
			// XXX error Message
		}
	} */

	/**
	 * set the values in the object acording to the mapping
	 */
	private void setMappedValues(Mapping mapping, Vector record, BOObject boObject) throws XMetaException, XDataTypeException, XSecurityException {
		int counter = 0;
		String value;
		Long codeValue;
		CodeTableEDVKG codeTable;
		int from;
		int to;
		MetaAttribute metaAttr;

		/* get the default values */
		Hashtable defaultValues = _tableMapping.getDefaultValues();

		for (counter = 0; counter < mapping.size(); counter++) {
			from = mapping.getFrom(counter);
			to = mapping.getTo(counter);
			metaAttr = mapping.getMeta(counter);
			value = ((String)(record.elementAt(from))).trim();

			// do not overwrite default value with empty one
			if ((!EDataTypeValidation.isNullString(value)) || (defaultValues == null) || (defaultValues.get(metaAttr.getExternName()) == null)) {
				if (metaAttr instanceof FeatureAttribute) {
					boObject.setAttribute(to, value, _context, null);
				} else if (metaAttr instanceof SimpleAttribute) {
					value = _tableMapping.preprocessValue((SimpleAttribute)(metaAttr), value);
					boObject.setAttribute(to, value, _context, null);
				} else {
					Log.logAlarm(LOGGER, "Error beim Import, falscher Typ in Funktion ImportRoulesEDVKG.setMappedValues, "+
						"counter="+counter+", from="+from+", to="+to+", name="+_mapIndex._nameList[to]);
				}
			}
		}
		
		// set the code values
		String defaultValue = null;
		for (counter = 0; counter < mapping.sizeCode(); counter++) {
			from = mapping.getCodeFrom(counter);
			to = mapping.getCodeTo(counter);
			codeTable = mapping.getCodeTable(counter);
			value = ((String)(record.elementAt(from))).trim();
			
			if (value.length() > 0) {
				codeValue = new Long(codeTable.getOID(value));
				// OIDs are allways SimpleAttribute's
				boObject.setValue(to, codeValue, _context);
			}
			else {
				// do not overwrite default value with empty one
				if (defaultValues != null) {
					metaAttr = mapping.getCodeMeta(counter);
					defaultValue = (String) defaultValues.get(metaAttr.getExternName());
				}
				if (defaultValue == null) {
					boObject.setValue(to, null, _context);
				}	
			}
		}
	}

	/**
	 * set the values of the PLZ
	 */
	private void setPLZValues(Mapping mapping, Vector record, BOObject boObject) throws XMetaException, XDataTypeException, XSecurityException {
		int counter = 0;
		String value;
		int from;
		int to;
		MetaAttribute metaAttr;
		for (counter = 0; counter < mapping.size(); counter++) {
			from = mapping.getFrom(counter);
			to = mapping.getTo(counter);
			metaAttr = mapping.getMeta(counter);
			value = ((String)(record.elementAt(from))).trim();
			//	Zwei letzten Ziffern (meistens 00) abschneiden
			// Achtung: value kann auch null oder leerer String sein
			if (value != null && value.length() > 4) value = value.substring(0,4); 
		    if (metaAttr instanceof FeatureAttribute) {
					boObject.setAttribute(to, value, _context, null);
		    } else if (metaAttr instanceof SimpleAttribute) {
		    	value = _tableMapping.preprocessValue((SimpleAttribute)(metaAttr), value);
					boObject.setAttribute(to, value, _context, null);
		    } else {
			    Log.logAlarm(LOGGER, "Error beim Import, falscher Typ in Funktion ImportRoulesEDVKG.setPLZValues");
		    }
		}
	}

	/**
	 * set the values of the KG's in the records
	 * @throws XDataTypeException 
	 * @throws XMetaException 
	 */
	private void setCodeKGValues (Vector record, BOObject boObject)
		    throws XSecurityException, XMetaException, XDataTypeException {
	    String name, kg;
		Long oid;
		name = (String)record.elementAt(_mapIndex._OIDPfarrkreis); // Das ist nicht die OID sondern '01', '02', '03', ...
		kg = (String)record.elementAt(_mapIndex._OIDKG);
		oid = (Long)codeKG.get(kg+name);
		if (oid == null && kg != null && !kg.equals("00")) 
			setRecordError('F', "Pfarrkreis konnte nicht ermittelt werden", boObject, _context);
		boObject.setValue("OIDPfarrkreis", oid, _context);
	}

	/**
	 * set the codes for the KG
	 */
	 private void setCodeKG()
			throws XMetaModelNotFoundException, XMetaModelQueryException, XSecurityException {
		codeKG = new HashMap();
		MetaObject metaObject = _metaService.getClassForName("Pfarrkreis");
		StandardQuery query = BLManager.instance().getExtent(metaObject);
		Vector result = query.getResult(_trans,false, true, _context);
		BOObject myObj;
		Object oid;
		String name, kg;
		for (int counter = 0; counter < result.size(); counter++) {
			myObj = (BOObject)result.elementAt(counter);
			oid = new Long(myObj.getOID());
			name = myObj.getAttribute("Name", _context); 
			/* mh@20060519 Achtung: "kg" muss ein zweistelliger String sein!!! */
			kg = myObj.getAttribute("OIDKG", _context);
			if (kg.length() == 1) kg = "0" + kg;
			codeKG.put(kg+name, oid);
		}
	}

	 /**
	  * create the Mapping for the base types
	  */
	private void setMapBase() throws XMetaModelQueryException, XMetaModelNotFoundException, XSecurityException {
		_mapBase = new Mapping(_context, _metaObject, _metaService);
		_mapBasePLZ = new Mapping(_context, _metaObject, _metaService);

	// allgemeinde Daten
		// OIDPerson wird verwendet _mapWegzug.add(_mapIndex._OID, _mapIndex._nameList[_mapIndex._OID]);
		_mapBase.add(_mapIndex._Heimatort, _mapIndex._nameList[_mapIndex._Heimatort]);
		_mapBase.add(_mapIndex._Geburtsdatum, _mapIndex._nameList[_mapIndex._Geburtsdatum]);
		_mapBase.add(_mapIndex._Beruf, _mapIndex._nameList[_mapIndex._Beruf]);
		_mapBase.add(_mapIndex._AktivPassivStatus, _mapIndex._nameList[_mapIndex._AktivPassivStatus]);
		_mapBase.add(_mapIndex._Passivdatum, _mapIndex._nameList[_mapIndex._Passivdatum]);
		_mapBase.add(_mapIndex._Sterbedatum, _mapIndex._nameList[_mapIndex._Sterbedatum]);
//		_mapBase.add(_mapIndex._AdressSperreSicherheit, _mapIndex._nameList[_mapIndex._AdressSperreSicherheit]);
		_mapBase.add(_mapIndex._CodeAdressSperre, _mapIndex._nameList[_mapIndex._CodeAdressSperre]);
		_mapBase.addCode(_mapIndex._CodeAufenthaltsart, _mapIndex._nameList[_mapIndex._CodeAufenthaltsart], "CodeAufenthalt");
		_mapBase.add(_mapIndex._CodeGeschlecht, _mapIndex._nameList[_mapIndex._CodeGeschlecht]);

	// Adresse
		_mapBase.add(_mapIndex._Familienname, _mapIndex._nameList[_mapIndex._Familienname]);
		_mapBase.add(_mapIndex._Vorname, _mapIndex._nameList[_mapIndex._Vorname]);
		_mapBase.add(_mapIndex._Rufname, _mapIndex._nameList[_mapIndex._Rufname]);
		_mapBase.add(_mapIndex._Allianzname, _mapIndex._nameList[_mapIndex._Allianzname]);
		_mapBase.addCode(_mapIndex._CodeVoranstellung, _mapIndex._nameList[_mapIndex._CodeVoranstellung], "CodeVoranstellung");
		_mapBase.add(_mapIndex._CodeAnrede, _mapIndex._nameList[_mapIndex._CodeAnrede]);
		_mapBase.addCode(_mapIndex._CodeTitel, _mapIndex._nameList[_mapIndex._CodeTitel], "CodeTitel");
		_mapBase.add(_mapIndex._Postlagernd, _mapIndex._nameList[_mapIndex._Postlagernd]);
		_mapBase.add(_mapIndex._Postfach, _mapIndex._nameList[_mapIndex._Postfach]);
		_mapBasePLZ.add(_mapIndex._PLZPostfach, "PLZPostfach");
		_mapBase.add(_mapIndex._PLZPostfach, _mapIndex._nameList[_mapIndex._PLZPostfach]);
		_mapBase.add(_mapIndex._Strassennummer, _mapIndex._nameList[_mapIndex._Strassennummer]);
		_mapBase.add(_mapIndex._Hausnummer, _mapIndex._nameList[_mapIndex._Hausnummer]);
		// no _mapBase.add(_mapIndex._Ort, _mapIndex._nameList[_mapIndex._Ort]);
		// no _mapBase.add(_mapIndex._PLZOrt, _mapIndex._nameList[_mapIndex._PLZOrt]);
		// no _mapBase.add(_mapIndex._PLZOrtEind, _mapIndex._nameList[_mapIndex._PLZOrtEind]);
		_mapBase.add(_mapIndex._Zusatzzeile, _mapIndex._nameList[_mapIndex._Zusatzzeile]);

	// Verwandschaft
		_mapBase.addCode(_mapIndex._OIDCodeZivilstand, _mapIndex._nameList[_mapIndex._OIDCodeZivilstand], "CodeZivilstand");
		_mapBase.add(_mapIndex._ZivilstandsDatum, _mapIndex._nameList[_mapIndex._ZivilstandsDatum]);
		_mapBase.add(_mapIndex._OIDHaushalt, _mapIndex._nameList[_mapIndex._OIDHaushalt]);
		// _mapBase.add(_mapIndex._OIDFamilie, _mapIndex._nameList[_mapIndex._OIDFamilie]);
		_mapBase.add(_mapIndex._OIDVorstand, _mapIndex._nameList[_mapIndex._OIDVorstand]);
		_mapBase.addCode(_mapIndex._OIDCodePartner, _mapIndex._nameList[_mapIndex._OIDCodePartner], "CodePartner");
		_mapBase.add(_mapIndex._OIDEhepartner, _mapIndex._nameList[_mapIndex._OIDEhepartner]);
		_mapBase.add(_mapIndex._OIDVater, _mapIndex._nameList[_mapIndex._OIDVater]);
		_mapBase.add(_mapIndex._OIDMutter, _mapIndex._nameList[_mapIndex._OIDMutter]);

	// KGSpez
		_mapBase.add(_mapIndex._OIDKG, _mapIndex._nameList[_mapIndex._OIDKG]);
		// no _mapBase.add(_mapIndex._OIDMission, _mapIndex._nameList[_mapIndex._OIDMission]);
		// no _mapBase.add(_mapIndex._OIDKGVorstand, _mapIndex._nameList[_mapIndex._OIDKGVorstand]);
		// no _mapBase.add(_mapIndex._OIDKGPartner, _mapIndex._nameList[_mapIndex._OIDKGPartner]);
		// no _mapBase.add(_mapIndex._OIDKGKind, _mapIndex._nameList[_mapIndex._OIDKGKind]);
		// no _mapBase.add(_mapIndex._OIDKGVorstandMission, _mapIndex._nameList[_mapIndex._OIDKGVorstandMission]);
		// no _mapBase.add(_mapIndex._OIDKGPartnerMission, _mapIndex._nameList[_mapIndex._OIDKGPartnerMission]);
		// no _mapBase.add(_mapIndex._OIDKGKindMission, _mapIndex._nameList[_mapIndex._OIDKGKindMission]);
		_mapBase.addCode(_mapIndex._OIDKonfession, _mapIndex._nameList[_mapIndex._OIDKonfession], "Konfession");
		_mapBase.add(_mapIndex._OIDKonfession, "Konfession");
		_mapBase.add(_mapIndex._OIDPfarrkreis, _mapIndex._nameList[_mapIndex._OIDPfarrkreis]);
		// no _mapBase.add(_mapIndex._TelPrivat, _mapIndex._nameList[_mapIndex._TelPrivat]);
		// no _mapBase.add(_mapIndex._TelGesch, _mapIndex._nameList[_mapIndex._TelGesch]);
		// no _mapBase.add(_mapIndex._TelMobile, _mapIndex._nameList[_mapIndex._TelMobile]);
		// no _mapBase.add(_mapIndex._TelFax, _mapIndex._nameList[_mapIndex._TelFax]);
		// no _mapBase.add(_mapIndex._Email, _mapIndex._nameList[_mapIndex._Email]);
		// no _mapBase.add(_mapIndex._ListeZeitung, _mapIndex._nameList[_mapIndex._ListeZeitung]);
		// no _mapBase.add(_mapIndex._OIDListOrgP, _mapIndex._nameList[_mapIndex._OIDListOrgP]);
		_mapBase.add(_mapIndex._CodeStimmrecht, _mapIndex._nameList[_mapIndex._CodeStimmrecht]);
		// no _mapBase.add(_mapIndex._AdresseAmt, _mapIndex._nameList[_mapIndex._AdresseAmt]);

	// History
		_mapBase.add(_mapIndex._OIDPerson, _mapIndex._nameList[_mapIndex._OIDPerson]);
		// no _mapBase.add(_mapIndex._Date, _mapIndex._nameList[_mapIndex._Date]);
		_mapBase.add(_mapIndex._Vorfallart, _mapIndex._nameList[_mapIndex._Vorfallart]);
		_mapBase.add(_mapIndex._Vorfalltyp, _mapIndex._nameList[_mapIndex._Vorfalltyp]);
		_mapBase.add(_mapIndex._DatVorfall, _mapIndex._nameList[_mapIndex._DatVorfall]);
		_mapBase.add(_mapIndex._DatVerarbeitung, _mapIndex._nameList[_mapIndex._DatVerarbeitung]);
		// _mapBase.add(_mapIndex._OIDBenutzer, _mapIndex._nameList[_mapIndex._OIDBenutzer]);
		// _mapBase.add(_mapIndex._RecordStatus, _mapIndex._nameList[_mapIndex._RecordStatus]);

	}


	/**
	 * create the Mapping table for the Stadt-Zustelladresse
	 */
	private void setMapZustell() throws XMetaModelQueryException, XMetaModelNotFoundException, XSecurityException {
		_mapZustell = new Mapping(_context, _metaObject, _metaService);
		_mapZustellPLZ = new Mapping(_context, _metaObject, _metaService);

		// Adresse Zustell Stadt
		_mapZustell.add(_mapIndex._ZUS_Postlagernd, _mapIndex._nameList[_mapIndex._ZUS_Postlagernd]);
		_mapZustell.add(_mapIndex._ZUS_Postfach, _mapIndex._nameList[_mapIndex._ZUS_Postfach]);
		_mapZustellPLZ.add(_mapIndex._ZUS_PLZPostfach, "ZusPLZPostfach");
		_mapZustell.add(_mapIndex._ZUS_PLZPostfach, _mapIndex._nameList[_mapIndex._ZUS_PLZPostfach]);
		_mapZustell.add(_mapIndex._ZUS_Strassennummer, _mapIndex._nameList[_mapIndex._ZUS_Strassennummer]);
		// no _mapBase.add(_mapIndex._ZusStrassenName, _mapIndex._nameList[_mapIndex._ZusStrassenName]);
		_mapZustell.add(_mapIndex._ZUS_Hausnummer, _mapIndex._nameList[_mapIndex._ZUS_Hausnummer]);
		// _mapBase.add(_mapIndex._ZusOrt, _mapIndex._nameList[_mapIndex._ZusOrt]);
		// _mapBase.add(_mapIndex._ZusPLZOrt, _mapIndex._nameList[_mapIndex._ZusPLZOrt]);
		// _mapBase.add(_mapIndex._ZusPLZOrtEind, _mapIndex._nameList[_mapIndex._ZusPLZOrtEind]);
		// _mapBase.add(_mapIndex._ZusOIDCodeOrtLand, _mapIndex._nameList[_mapIndex._ZusOIDCodeOrtLand]);
		_mapZustell.add(_mapIndex._ZUS_Zusatzzeile, _mapIndex._nameList[_mapIndex._ZUS_Zusatzzeile]);
	}

	/**
	 * create the Mapping table for the Zustelladresse ausserhalb der Stadt
	 */
	private void setMapZustellAAMU() throws XMetaModelQueryException, XMetaModelNotFoundException, XSecurityException {
		_mapZustellAAMU = new Mapping(_context, _metaObject, _metaService);
		_mapZustellPLZAAMU = new Mapping(_context, _metaObject, _metaService);

		// Adresse Zustell ausserhalb Stadt, bestimmt zum Ueberschreiben der Adresse innerhalb der Stadt
		_mapZustellAAMU.add(_mapIndex._AAD_Postlagernd,  _mapIndex._nameList[_mapIndex._ZUS_Postlagernd]);
		_mapZustellAAMU.add(_mapIndex._AAD_Postfach, _mapIndex._nameList[_mapIndex._ZUS_Postfach]);
		_mapZustellPLZAAMU.add(_mapIndex._AAD_PLZPostfach, "ZusPLZPostfach");
		_mapZustellAAMU.add(_mapIndex._AAD_PLZPostfach, _mapIndex._nameList[_mapIndex._ZUS_PLZPostfach]);
		_mapZustellPLZAAMU.add(_mapIndex._AAD_PLZOrt, "ZusPLZOrt"); // nur AAMU-Zustell
		_mapZustellAAMU.add(_mapIndex._AAD_PLZOrt, "ZusPLZOrtEind"); // nur AAMU-Zustell
		_mapZustellAAMU.add(_mapIndex._AAD_Ortsbezeichnung, "ZusOrt"); // nur AAMU-Zustell
		_mapZustellAAMU.add(_mapIndex._AAD_Strassenname, "ZusStrassenName");
		_mapZustellAAMU.add(_mapIndex._AAD_Hausnummer, "ZusHausNr");
		_mapZustellAAMU.add(_mapIndex._AAD_CodeOrtLand, "ZusOIDCodeOrtLand"); // nur AAMU-Zustell
		_mapZustellAAMU.add(_mapIndex._AAD_Zusatzzeile, _mapIndex._nameList[_mapIndex._ZUS_Zusatzzeile]);
		// _mapWegzug.add(_mapIndex._ZuzOIDKG, "WegOIDKG");
	}
	/**
	 * create the Mapping table for the wegzug
	 */
	private void setMapWegzug() throws XMetaModelQueryException, XMetaModelNotFoundException, XSecurityException {
		_mapWegzug = new Mapping(_context, _metaObject, _metaService);
		_mapWegzugPLZ = new Mapping(_context, _metaObject, _metaService);

		_mapWegzug.add(_mapIndex._WegDatum, _mapIndex._nameList[_mapIndex._WegDatum]);
		_mapWegzug.add(_mapIndex._AAD_Postlagernd, "WegPostlagernd");
		_mapWegzug.add(_mapIndex._AAD_Postfach, "WegPostfach");
		_mapWegzugPLZ.add(_mapIndex._AAD_PLZPostfach, "WegPLZPostfach");
		_mapWegzug.add(_mapIndex._AAD_PLZPostfach, "WegPLZPostfachEind");
		_mapWegzug.add(_mapIndex._AAD_Strassenname, "WegStrassenName");
		_mapWegzug.add(_mapIndex._AAD_Hausnummer, "WegHausNr");
		_mapWegzug.add(_mapIndex._AAD_Ortsbezeichnung, "WegOrt");
		_mapWegzugPLZ.add(_mapIndex._AAD_PLZOrt, "WegPLZOrt");
		_mapWegzug.add(_mapIndex._AAD_PLZOrt, "WegPLZOrtEind");
		_mapWegzug.add(_mapIndex._AAD_CodeOrtLand, "WegCodeOrtLand");
		_mapWegzug.add(_mapIndex._AAD_Zusatzzeile, "WegAdrZusatz");
		// _mapWegzug.add(_mapIndex._ZuzOIDKG, "WegOIDKG");
	}


	/**
	 * create the Mapping table for the erstanlieferung
	 */
	private void setMapErstanlieferung() throws XMetaModelQueryException, XMetaModelNotFoundException, XSecurityException {
		_mapErstanlieferung = new Mapping(_context, _metaObject, _metaService);
		_mapErstanlieferungPLZ = new Mapping(_context, _metaObject, _metaService);

		_mapErstanlieferung.add(_mapIndex._ZuzDatum, _mapIndex._nameList[_mapIndex._ZuzDatum]);
		/* DIe Zuzugsadresse wird nicht mehr verwendet, dav�r die Zustelladresse
		_mapErstanlieferung.add(_mapIndex._ZuzPostlagernd, _mapIndex._nameList[_mapIndex._ZuzPostlagernd]);
		_mapErstanlieferung.add(_mapIndex._ZuzPostfach, _mapIndex._nameList[_mapIndex._ZuzPostfach]);
		_mapErstanlieferungPLZ.add(_mapIndex._ZuzPLZPostfachEind, "ZuzPLZPostfach");
		_mapErstanlieferung.add(_mapIndex._ZuzPLZPostfachEind, _mapIndex._nameList[_mapIndex._ZuzPLZPostfachEind]);
		_mapErstanlieferung.add(_mapIndex._ZuzStrassenName, _mapIndex._nameList[_mapIndex._ZuzStrassenName]);
		_mapErstanlieferung.add(_mapIndex._ZuzHausNr, _mapIndex._nameList[_mapIndex._ZuzHausNr]);
		_mapErstanlieferung.add(_mapIndex._ZuzOrt, _mapIndex._nameList[_mapIndex._ZuzOrt]);
		_mapErstanlieferungPLZ.add(_mapIndex._ZuzPLZOrtEind, "ZuzPLZOrt");
		_mapErstanlieferung.add(_mapIndex._ZuzPLZOrtEind, _mapIndex._nameList[_mapIndex._ZuzPLZOrtEind]);
		_mapErstanlieferung.add(_mapIndex._ZuzCodeOrtLand, _mapIndex._nameList[_mapIndex._ZuzCodeOrtLand]);
		_mapErstanlieferung.add(_mapIndex._ZuzAdrZusatz, _mapIndex._nameList[_mapIndex._ZuzAdrZusatz]); */
		// no _mapErstanlieferung.add(_mapIndex._ZuzOIDKG, _mapIndex._nameList[_mapIndex._ZuzOIDKG]);

	}
	private class Mapping {
		private Vector fromList = new Vector();
		private Vector toList = new Vector();
		private Vector toMetaList = new Vector();
		private Vector fromCodeList = new Vector();
		private Vector toCodeList = new Vector();
		private Vector codeList = new Vector();
		private Vector toCodeMetaList = new Vector();

		private BOContext _context;
		private MetaObject _metaObject;
		private MetaService _metaService;

		public Mapping (BOContext context, MetaObject metaObject, MetaService metaService) {
			_context = context;
			_metaObject = metaObject;
			_metaService = metaService;
		}

		public void add (int from, String to) throws XMetaModelNotFoundException, XMetaModelQueryException {
			if (from >= ImportRoulesEDVKG.MAX_COLUMS/2) {

			}
			fromList.add(new Integer(from));
			int toNr;
			MetaAttribute metaAttr;

			metaAttr = _metaObject.getAllAttributeForName(to, _context);
			toNr = metaAttr.getIndex();
			toList.add(new Integer(toNr));
			toMetaList.add(metaAttr);
		}


		public int getFrom(int index) {
			return ((Integer)(fromList.elementAt(index))).intValue();
		}

		public int getTo(int index) {
			return ((Integer)(toList.elementAt(index))).intValue();
		}

		public MetaAttribute getMeta(int index) {
			return (MetaAttribute)(toMetaList.elementAt(index));
		}

		public int size() {
			return toList.size();
		}

		public void addCode (int from, String to, String codeName) throws XMetaModelNotFoundException, XMetaModelQueryException, XSecurityException {
			fromCodeList.add(new Integer(from));
			int toNr;
			MetaAttribute metaAttr;

			metaAttr = _metaObject.getAllAttributeForName(to, _context);
			toNr = metaAttr.getIndex();
			toCodeList.add(new Integer(toNr));
			toCodeMetaList.add(metaAttr);
			codeList.add(new CodeTableEDVKG(_trans, _context, _metaService, codeName));
		}

		public int getCodeFrom(int index) {
			return ((Integer)(fromCodeList.elementAt(index))).intValue();
		}

		public int getCodeTo(int index) {
			return ((Integer)(toCodeList.elementAt(index))).intValue();
		}
		
		public MetaAttribute getCodeMeta(int index) {
			return (MetaAttribute)(toCodeMetaList.elementAt(index));
		}		

		public CodeTableEDVKG getCodeTable(int index) {
			return (CodeTableEDVKG)(codeList.elementAt(index));
		}

		public int sizeCode() {
			return toCodeList.size();
		}

	}
}
