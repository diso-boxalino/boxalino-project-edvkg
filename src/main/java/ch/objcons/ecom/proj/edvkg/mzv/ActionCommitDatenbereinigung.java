package ch.objcons.ecom.proj.edvkg.mzv;

import ch.objcons.ecom.api.IAction;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.adapters.EActionAdapter2;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.engine.EEngine;
import ch.objcons.ecom.meta.MetaService;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.log.Log;

/*
 * stores all object in a transaction to the database
 *
 */
public class ActionCommitDatenbereinigung extends EActionAdapter2 {
	private IAction _editCmdCommit = null;
	public ActionCommitDatenbereinigung(BoxMZV editBox, IRequest request, String slotID, MetaService service, String nextPage, BLTransaction trx, boolean isCommit) {
		super (editBox, request, slotID, nextPage);
	}
	public void performX () throws XMetaException, XDataTypeException, XSecurityException {
		BOContext boContext = (BOContext)getRequest().getContext();

		boolean isGUIClient = boContext.isGUIClient();
		boolean isSupp = boContext.isReferenceCheckSuppressed();
		try {
			boContext.setGUIClient(true); // Referenzen werden nicht �berpr�ft ?
			boContext.setReferenceCheckSuppressed(true);
			try {
                _editCmdCommit.doPerform(getRequest());
            }
            catch (XPerformException x) {
                Log.logDesignerError("doPerform failed", x);
            }
		}
		finally {
			boContext.setGUIClient(isGUIClient);
			boContext.setReferenceCheckSuppressed(isSupp);
		}
	}
	public boolean validate() {
		BOContext boContext = (BOContext)getRequest().getContext();
		try {
			_editCmdCommit =
				EEngine.getInstance().handleParameter(getRequest(), "edit", "cmd_commit", new String[] { "true" });
			boolean isGUIClient = boContext.isGUIClient();
			boolean isSupp = boContext.isReferenceCheckSuppressed();
	
			try {
				boContext.setGUIClient(true);
				boContext.setReferenceCheckSuppressed(true);
				return _editCmdCommit.doValidate(getRequest());
			}
			finally {
				boContext.setGUIClient(isGUIClient);
				boContext.setReferenceCheckSuppressed(isSupp);
			}
		}
		catch (Exception x) {
			ch.objcons.log.Log.logSystemAlarm("Datenbereinigungs-validate", x);
		}
		return false;
	}
}
