package ch.objcons.ecom.proj.edvkg.systeminfo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.ecom.api.ISession;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.engine.ESessionAdapter;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.system.security.BOSecurityManager;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;
import com.boxalino.server.Request;

public class SystemInfo {

	private static final ILogger LOGGER = Logger.getLogger(SystemInfo.class);
	
	public static final String DISABLE_USER_LOGIN = "DISABLE_USER_LOGIN";
	
	public static final String READONLY = "READONLY";
	
	public static final String DISABLE_IMPORT = "DISABLE_IMPORT";
	
	public enum ReadOnlyMode { USER, ALL, DISABLED };
	
	private static final String RIGHT_TBL = "bx_right", RIGHT_ATTR_TBL = "bx_list_rightattroid";
	
	private static final String RIGHT_BAK_TBL = RIGHT_TBL + "_backup", RIGHT_ATTR_BAK_TBL = RIGHT_ATTR_TBL + "_backup";
	
	private static final Set<String> EXCLUDED_METAOBJECTS = new HashSet<String>(Arrays.asList("LoginUser", "LoginLog", BOSystemInfo.CLASS_NAME));
	
	private static final Lock LOCK = new ReentrantLock();
	
	public static void setUserLoginDisabled(boolean disabled) {
		if (disabled) terminateUserSessions();
		
		LoginGuard.getInstance().setUserLoginEnabled(!disabled);
		setActive(disabled, DISABLE_USER_LOGIN);
	}
	
	public static boolean getUserLoginDisabled() {
		return getActive(DISABLE_USER_LOGIN);
	}
	
	/**
	 * @return the sessions that were terminated
	 */
	private static Collection<ISession> terminateUserSessions() {
		List<ISession> terminatedSessions = new ArrayList<ISession>();
		@SuppressWarnings("unchecked")
		List<ESessionAdapter> sessions = ESessionAdapter.getSessions();
		for (ESessionAdapter session : sessions) {
			if (!isUserSession(session)) continue;
			
			session.terminate();
			terminatedSessions.add(session);
		}
		return terminatedSessions;
	}
	
	private static void terminateSessions(Collection<ISession> excludedSessions) {
		@SuppressWarnings("unchecked")
		List<ESessionAdapter> sessions = ESessionAdapter.getSessions();
		sessions.removeAll(excludedSessions);
		for (ESessionAdapter session : sessions) {
			session.terminate();
		}
	}
	
	private static void terminateSessions(ISession... excludedSessions) { terminateSessions(Arrays.asList(excludedSessions)); }

	static boolean isUserSession(ESessionAdapter session) {
		BOContext context = (BOContext) Request.getCurrentOrDummy().getContext();
		BLTransaction trx = BLTransaction.startTransaction(context);
		String userName = session.getUser();
		try {
			BOObject boLoginUser = BoxalinoUtilities.getObjectByKey("LoginUser", "loginName", userName, trx);
			if (boLoginUser == null) return false;
			
			return Boolean.FALSE.equals(boLoginUser.getValue("Administrator", context));
		} catch (XSecurityException e) {
			LOGGER.error("XSecurityException occurred!", e);
		} catch (XMetaModelQueryException e) {
			LOGGER.error("XMetaModelQueryException occurred!", e);
		}
		return false;
	}
	
	/**
	 * @param allowAdminWrite if {@code true}, administrators are still allowed to write 
	 */
	public static void activateReadOnlyMode(boolean allowAdminWrite) {
		if (!resetReadOnlyModeNoReload()) return;
	
		Connection conn = null;
		final Lock lock = getAndLock();
		try {
			conn = DBConnectionPool.instance().getConnection();
			Statement stmt = conn.createStatement();
			String exclude = " CLASSRIGHT_CLASSOID not in (" + getExcludedMetaObjectOIDs() + ")";
			
			if (allowAdminWrite) {
				long userGroupOID = getAdminUserGroupOID();
				ResultSet rsOff = stmt.executeQuery("select count(1) from bx_right");
				long off = rsOff.first() ? rsOff.getLong(1) : 0; 
				// update entries of right and set usergroup to admin to preserve admin write rights
				stmt.executeUpdate("insert into bx_right (oid, artoid, bx_user, bx_action, classright_classoid, classright_view, bx_name) "
				+ "select (oid + " + off + "), artoid, " + userGroupOID + ", bx_action, classright_classoid, classright_view, bx_name from bx_right "
				+ "where bx_user = 3 and " + exclude);
				exclude = "(" + exclude + " and bx_user != " + userGroupOID + ")";
				
				// copy attrs accordingly
				stmt.executeUpdate("insert into " + RIGHT_ATTR_TBL + " select ((select max(OID) from " +
				RIGHT_ATTR_TBL + ") + ra.OID), referenzOID + " + off + ", data from " + RIGHT_ATTR_TBL
				+ " ra join bx_right r on r.oid = referenzoid"); // only offset-copy for copied rights
			}
			// demote rights to read
			stmt.executeUpdate("delete from " + RIGHT_TBL + " where bx_action not like 'R' and " + exclude);
			stmt.executeUpdate("update " + RIGHT_TBL + " set bx_action = 'R' where " + exclude);
			reloadSecurity();
			setReadOnlyMode(allowAdminWrite ? ReadOnlyMode.USER : ReadOnlyMode.ALL);
		} catch (SQLException e) {
			LOGGER.error("Failed to activate read-only mode, resetting.", e);
			resetReadOnlyMode();
		} finally {
			if (conn != null) {
				DBConnectionPool.instance().ungetConnection(conn);
			}
			lock.unlock();
		}
	}

	static long getAdminUserGroupOID() {
		BOContext context = (BOContext) Request.getCurrentOrDummy().getContext(); 
		BLTransaction trx = BLTransaction.startTransaction(context);
		return BoxalinoUtilities.getObjectByKey("UserGroup", "name", "bx_administratorGroup", trx).getOID();
	}

	public static boolean resetReadOnlyMode() {
		boolean success = resetReadOnlyModeNoReload();
		if (success) {
			reloadSecurity();
			setReadOnlyMode(ReadOnlyMode.DISABLED);
		}
		return success;
	}
	
	private static void reloadSecurity() {
		try {
			BLManager.getInstance().emptyBuffersWithReferrers(MetaObjectLookup.getInstance().getMetaObject("Right"));
			BOSecurityManager.getManagerInstance().reload(Request.getCurrentOrDummy());
			// Logged in users other than the user performing this method call 
			// remain unaffected by BOSecurityManager.reload
			// terminating their session resolves this issue
			// and causes the user to see the system message (if available) 
			terminateSessions(Request.getCurrentOrDummy().getSession());
		} catch (XMetaModelNotFoundException e) {
			LOGGER.error("XMetaModelNotFoundException occurred!", e);
		}
	}
	
	private static boolean resetReadOnlyModeNoReload() {
		final Lock lock = getAndLock();
		try {
			restoreRights();
			backupRights();
			return true;
		} catch (SQLException e) {
			LOGGER.fatal("Failed to properly reset read-only mode!", e);
		} finally {
			lock.unlock();
		}
		return false;
	}

	public static void processReadOnlyMode(ReadOnlyMode mode) {
		if (mode == getReadOnlyMode()) return;
		
		switch(mode) {
		case ALL:
			activateReadOnlyMode(false);
			break;
		case USER:
			activateReadOnlyMode(true);
			break;
		case DISABLED:
			resetReadOnlyMode();
			break;
		}
	}
	
	public static ReadOnlyMode getReadOnlyMode() {
		BOContext context = (BOContext) Request.getCurrentOrDummy().getContext();
		try {
			return ReadOnlyMode.valueOf(getBOSystemInfo(SystemInfo.READONLY, null).getAttribute("mode", context));
		} catch (Exception e) {
			LOGGER.error("Exception occurred!", e);
		}
		return null;
	}
	
	private static void setReadOnlyMode(ReadOnlyMode mode) {
		BOContext context = (BOContext) Request.getCurrentOrDummy().getContext();
		BLTransaction trx = BLTransaction.startTransaction(context);
		try {
			BOObject boReadonly = getBOSystemInfo(SystemInfo.READONLY, trx);
			boReadonly.setValue("bxactive", mode != ReadOnlyMode.DISABLED, context);
			boReadonly.setAttribute("mode", mode.toString(), context);
			trx.commit();
		} catch (Exception e) {
			LOGGER.error("Exception occurred!", e);
		}
	}
	
	private static String getExcludedMetaObjectOIDs() {
		MetaObjectLookup lookup = MetaObjectLookup.getInstance();
		String oids = "";
		String prefix = "";
		try {
			for (String name : EXCLUDED_METAOBJECTS) {
				oids += prefix + lookup.getMetaObject(name).getOID();
				prefix = ",";
			}
		} catch (XMetaModelNotFoundException e) {
			throw new RuntimeException("Unexpected XMetaModelNotFoundException occurred!", e);
		}
		return oids;
	}
	
	private static void restoreRights() throws SQLException {
		final Lock lock = getAndLock();
		try {
			Connection conn = DBConnectionPool.instance().getConnection();
			try {
				Statement stmt = conn.createStatement();
				if (stmt.executeQuery("show tables like '" + RIGHT_BAK_TBL + "'").first()) {
					stmt.executeUpdate("drop table if exists " + RIGHT_TBL);
					stmt.executeUpdate("create table " + RIGHT_TBL + " as select * from " + RIGHT_BAK_TBL);
				}
				if (stmt.executeQuery("show tables like '" + RIGHT_ATTR_BAK_TBL + "'").first()) {
					stmt.executeUpdate("drop table if exists " + RIGHT_ATTR_TBL);
					stmt.executeUpdate("create table " + RIGHT_ATTR_TBL + " as select * from " + RIGHT_ATTR_BAK_TBL);
				}
			} finally {
				if (conn != null) {
					DBConnectionPool.instance().ungetConnection(conn);
				}
			}
		} finally {
			lock.unlock();
		}
	}
	
	private static void backupRights() throws SQLException {
		final Lock lock = getAndLock();
		try {
			Connection conn = DBConnectionPool.instance().getConnection();
			try {
				Statement stmt = conn.createStatement();
				stmt.executeUpdate("drop table if exists " + RIGHT_BAK_TBL);
				stmt.executeUpdate("create table " + RIGHT_BAK_TBL + " as select * from " + RIGHT_TBL);
				stmt.executeUpdate("drop table if exists " + RIGHT_ATTR_BAK_TBL);
				stmt.executeUpdate("create table " + RIGHT_ATTR_BAK_TBL + " as select * from " + RIGHT_ATTR_TBL);
			} finally {
				if (conn != null) {
					DBConnectionPool.instance().ungetConnection(conn);
				}
			} 
		} finally {
			lock.unlock();
		}
	}

	public static void setImportsDisabled(boolean disable) {
		setActive(disable, DISABLE_IMPORT);
	}
	
	public static boolean getImportsDisabled() {
		return getActive(DISABLE_IMPORT);
	}
	
	/**
	 * notifies SystemInfo that there is likely to be a major inconsistency in the db.
	 * <ul>
	 * <li>Triggers read-only mode for all users.</li>
	 * <li>Disables imports.</li>
	 * </ul>
	 */
	public static void handleInconsitency() {
		try {
			SystemInfo.activateReadOnlyMode(true);
			SystemInfo.setImportsDisabled(true);
		} catch (Throwable t) {
			LOGGER.fatal("throwable occured while trying to handle inconsistency", t);
		}
	}
	
	private static BOObject getBOSystemInfo(String code, BLTransaction trx) {
		if (trx == null) {
			trx = BLTransaction.startTransaction((BOContext) Request.getCurrentOrDummy().getContext());
		}
		return BoxalinoUtilities.getObjectByKey(BOSystemInfo.CLASS_NAME, "code", code, trx);
	}
	
	private static void setActive(boolean active, String code) {
		BOContext context = (BOContext) Request.getCurrentOrDummy().getContext();
		BLTransaction trx = BLTransaction.startTransaction(context);
		try {
			getBOSystemInfo(code, trx).setValue("bxactive", active, context);
			trx.commit();
		} catch (XMetaModelNotFoundException e) {
			LOGGER.error("XMetaModelNotFoundException occurred!", e);
		} catch (XSecurityException e) {
			LOGGER.error("XSecurityException occurred!", e);
		} catch (XMetaException e) {
			LOGGER.error("XMetaException occurred!", e);
		} catch (XBOConcurrencyConflict e) {
			LOGGER.error("XBOConcurrencyConflict occurred!", e);
		}
	}
	
	private static boolean getActive(String code) {
		try {
			return Boolean.TRUE.equals(getBOSystemInfo(code, null).getValue("bxactive"));
		} catch (XMetaModelNotFoundException e) {
			LOGGER.error("XMetaModelNotFoundException occurred!", e);
		} catch (XSecurityException e) {
			LOGGER.error("XSecurityException occurred!", e);
		}
		return false;
	}
	
	private static Lock getAndLock() {
		Lock lock = LOCK;
		lock.lock();
		return lock;
	}
	
	private SystemInfo() {}
	
}
