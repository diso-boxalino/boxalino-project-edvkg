package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.lang.management.ManagementFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.db.dbimport.LogFileWriter;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.utils.BOMQuery;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.engine.EEngine;
import ch.objcons.ecom.meta.DatabaseMetaManager;
import ch.objcons.ecom.meta.MySql;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.proj.edvkg.mzv.BOPerson;
import ch.objcons.ecom.proj.edvkg.mzv.BOPersonHistory;
import ch.objcons.ecom.proj.edvkg.mzv.BoxMZV;
import ch.objcons.ecom.proj.edvkg.mzv.Consts;
import ch.objcons.ecom.proj.edvkg.mzv.UtilsEDVKG;
import ch.objcons.ecom.proj.edvkg.mzv.imp.BOMeldung.Meldung;
import ch.objcons.ecom.proj.edvkg.systeminfo.SystemInfo;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.log.Log;
import ch.objcons.util.properties.ShopProperties;

import com.boxalino.util.NotImplementedException;

/**
 * @author Matthias Humbert
 * 
 */

public final class PostImport extends Thread {

	private static final boolean VM_DEBUG = ManagementFactory.getRuntimeMXBean().getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;
	
	private static final Lock LOCK = new ReentrantLock();
	
	/** consts for the querys */
	public static final int MISSION_ITALIA = 97;
	public static final int MISSION_ESPANOL = 98;
	public static final int STADTVERBAND_KAT = 1010;
	public static final int STADTVERBAND_REF = 1020;
	public static final int KATHOLISCH = 1;
	public static final int REFORMIERT = 2;
	public static final int ITALIEN = 8218;
	public static final int SPANIEN = 8236;
	public static final int PERSON_ARTOID = 72;

	private static final int oidCodeFraumuensterstrasse = 1480;
	private static final int oidCodeSelnaustrasse = 2546;
	
	/** Konstanten f�r die Methode gruppenDurchgang */
	private static final int FamilieChanged = 3;
	private static final int UpdateFamilienChanged = 5;
	private static final int SortiereFamilie = 9;
	private static final int UpdateZeitungsEmpfaenger = 10;
	
	/** the Connection */
	private final Connection _dbConn;

	/** log f�r Consistency Check-Vorg�nge */
	private LogFileWriter _logWriter = null;
	private boolean _logWriterClosureNeeded = false;
	/** log f�r gefundene Inkonsistenzen */
	private LogFileWriter _logInkonsistenzen = null;
	private boolean _logInkonsistenzenClosureNeeded = false; 
	
	/** Wir nur f�r activateMutationen() ben�tigt */
	private BOContext _boContext = null;
	
	/** the strings for the querys */
	final static String _shadowTable = "bx_personhistory";
	final static String _mainTable = "bx_person";
	final static String _zeitungsTable = "bx_zeitung";
	
	/**
	 * The hash table with all the persons in it 
	 */
	private Map<Long, Person> _per = null;
	/**
	 * All families
	 * Hashtable key: bx_oidfamilie (falls null dann kein Eintrag in Hashtable machen)
	 * Hashtable value: 
	 *    - falls Familie ein einziges Mitglied hat: Long-Objekt mit oidperson drin
	 *    - falls Familie mehrere Mitglieder hat: Vector mit Long-Objekten mit Personen-OIDs
	 */
	private Map<Long, ?> _alleFamilien = null;
	/**
	 * Familien, die mindestens ein Mitglied haben, das durch den Import ver�ndert wurde
	 */
	private final HashSet<Long> modifiedFamilienByImport = null;
	/**
	 * Alle Haushalte der Familien vom Table oidFamilieChanged 
	 */
	private final HashSet oidHaushalteChanged = null;
	/**
	 * Menge der Familien-OIDs, deren Familien gewisse Updates erfahren sollten, 
	 * um die Konsistenz von Beziehung zu sichern
	 */
	private final Set<Long> _modifiedFamilienForKGs = new HashSet<Long>();
	/**
	 * Meldungen je Person
	 */
	private Map<Long, List<Meldung>> _meldungen = null;

	/**
	 * Stichtag f�r �ber-16-J�hrige
	 */
	private java.util.Date alter16 = null;	
	/**
	 * Stichtag f�r Vollj�hrige (Familienbildung)
	 */
	private Date alter18 = null;
	/**
	 * Stichtag f�r Kleinkinder (Familienbildung-Adresszusatzindex)
	 */
	private Date alter2 = null;

	/** Sp�ter initialisierte Listen und Werte von speziellen OIDs */
	private HashSet _oidCodeAufenthaltZeitungsErhaltSet = null;
	private long _oidCodeZivilstandLedig;
	private long _oidCodeZivilstandVerheiratet;
	private long _oidCodeZivilstandEingetrPartner;
	
	/**
	 * An SQL statement object, useful for sending queries to the db
	 */
	private Statement _sql = null;
	
	/**
	 * Options for the execution of the CC
	 */
	private int _runCode = 0;
	
	// Constants for _runCode
	
	public static final int RUN_WITHOUT_POSTIMPORTQUERIES = 1 << 0;
	public static final int RUN_WITHOUT_ACTIVATION = 1 << 1;
	public static final int RUN_WITHOUT_ANFORDERUNGEN = 1 << 2;
	
	private boolean _personSubsetOnly = false;
	
//	// Stimmrecht Consts 
//	private static final int SPAN_AMOUNT_DAYS = 14;
//	
//	// Stimmrecht Members
//	private int _stimmrechtsAlterRef = 0;
//	private int _stimmrechtsAlterKath = 0;
//	
//	private Long[] _aartenOIDsRef = null;
//	private Long[] _aartenOIDsKath = null;
//	
//	private HashSet<Long> _oidCodeAufenthaltStimmrechtSetRef = null;
//	private HashSet<Long> _oidCodeAufenthaltStimmrechtSetKath = null;
//	
//	private java.util.Date _ageSpanFromRef = null;
//	private java.util.Date _ageSpanToRef = null;
//	private java.util.Date _ageSpanFromKath = null;
//	private java.util.Date _ageSpanToKath = null;
	
	
	private void writeErrorWithStack(String msg, Throwable t) {
		_logWriter.writeError(msg, t);
	}

	/**
	 * Class for the Roules how to check the EDVKG import consistency
	 */
	public PostImport(Connection dbConn, LogFileWriter importProcess, LogFileWriter inkonsistenzen, BOContext boContext, int runCode) {
		this(dbConn, importProcess, inkonsistenzen, boContext);
		_runCode = runCode;
	}

	/**
	 * @param runCodeConstant
	 * @return true, if the runCode includes the specified runCode constant.
	 */
	private boolean checkRunCode(int runCodeConstant) {
		return (_runCode & runCodeConstant) != 0;
	}
	
	/**
	 * Class for the Roules how to check the EDVKG import consistency
	 */
	public PostImport(Connection dbConn, LogFileWriter importLogWriter, LogFileWriter inkonsistenzenLog, BOContext boContext) {
		_dbConn = dbConn;
		_boContext = boContext;
		try {
			if (importLogWriter == null) {
				String logFileName = ShopProperties.getInstance().getPath("ecom.log.dir") + "importCC.txt";
				_logWriter = new LogFileWriter(logFileName);
				_logWriterClosureNeeded = true;
			} else {
				_logWriter = importLogWriter;
			}
			if (inkonsistenzenLog == null) {
				String logFileName2 = ShopProperties.getInstance().getPath("ecom.log.dir") + "inconsistencies.txt";
				_logInkonsistenzen = new LogFileWriter(logFileName2);
				_logInkonsistenzenClosureNeeded = true;
			} else {
				_logInkonsistenzen = inkonsistenzenLog;
			}
			_sql = _dbConn.createStatement();
		} catch (Exception e) {
			String msg = "Initialisierung vom Konsistenz-Check fehlgeschlagen:\n" + e.getMessage();
			Log.logSystemError(msg, e);
			if (_logWriter != null) {
				writeErrorWithStack(msg, e);
			}
		}
	}

	@Override
	public void run() {
		final Lock lock = LOCK;
		lock.lock();
		try {
			setName("Consistency Check");
			synchronized(PostImport.class) {
				try {
					setName("Consistency Check Running");
					this.startPostImport(null);
					setName("Consistency Check Finished");
				} finally {
					if (_dbConn != null) {
						DBConnectionPool.instance().ungetConnection(_dbConn);
					}
				}
			}
		} finally {
			lock.unlock();
		}
	}

	@Override
	public String toString() {
		return super.toString();
	}

	private long getHeap () {
		long heap = 0;
		try {
			// Memory Check
			Sizeof.runGC();
			heap = Sizeof.usedMemory();
		} catch (Exception e) {
			_logWriter.writeText("Memory Check: Es ist ein Fehler aufgetreten: " + e.getMessage());
		}
		return heap;
	}

	/**
	 * Return list of OID's specified by the query
	 * @param dbConn
	 * @param query
	 * @return null falls Problem, "-1" falls keine Resultate
	 * @throws XMetaModelQueryException
	 */
	private String getSet(Connection dbConn, String query) throws XMetaModelQueryException {
		// hack: return list of OID's instead of query!!!
		if (!(DatabaseMetaManager.getInstance() instanceof MySql)) {
			return null;
		}
		try {
			Statement stmt = dbConn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			String res = "";
			while (rs.next()) {
				if (res.length() > 0) res += ",";
				res += rs.getString(1);
			}
			if (res.length() == 0) return "-1";
			return res;
		}
		catch (SQLException e) {
			_logWriter.writeError(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * F�hre Foreign Key Checks durch und setze Familien-OID vom Vorstand bei
	 * Personen, die durch den Import ver�ndert wurden
	 * @param persons
	 */
	private void doImportChecksAndSets(Map<Long, Person> persons) {
		_logWriter.writeText("Fraum\u00fcnster Flag, ZIP Referenzen und Missionen Check-Up");
		for (Person p : persons.values()) {
			/*** F�r Personen die potenziell das "Fraum�nster-Flag" besitzen ***/
			/* Nur setzen falls true! -> Zur�cksetzen erfolgt manuell (nicht via Konsistenz-Check) */
			if (p._potenziellesFraumuensterFlag && !(p._oidKG == 1001 && p._zeitungverzicht) && p.hatRelevanteKonfession() ) {
				p.setFraumuensterFlag(true); // damit wird oidKG = 1001 und zeitungVerzicht = 'j' in die DB geschrieben
				// Die oidKG wird eventuell von Kindern usw. im weiteren Verlauf des CC gebraucht
				p._oidKG = 1001; //p.setOIDKG(1001); -> Es soll nicht auf diese Weise in die DB geschrieben werden, weil andere DB-Zeilen betroffen sind als bet setOID()
			}
			/*** Vater-OID-Check ***/
			if (p._oidVater != 0 && !persons.containsKey(new Long(p._oidVater))) {
				p.addMeldung(Meldungsvorlage.RefVaterError, p._oidVater);
			}
			/*** Mutter-OID-Check ***/
			if (p._oidMutter != 0 && !persons.containsKey(new Long(p._oidMutter))) {
				p.addMeldung(Meldungsvorlage.RefMutterError, p._oidMutter);
			}
			/*** Ehepartner-OID-Check ***/
			if (p._oidOrigPartner != 0 && !persons.containsKey(new Long(p._oidOrigPartner))) {
				p.addMeldung(Meldungsvorlage.RefPartnerError, p._oidOrigPartner);
			}
			/*** OIZ-Familie-OID-Check ***/
			if (p._oidVorstand != 0 && !persons.containsKey(new Long(p._oidVorstand))) {
				p.addMeldung(Meldungsvorlage.RefOIZFamilieError, p._oidVorstand);
			}
			/*** F�r Italiener und Spanier Mission setzen ***/
			/* Nur setzen falls true: -> Zur�cksetzen erfolgt manuell (nicht via Konsistenz-Check) */
			if (p._potenzielleOidMission > 0) p.setImportedOIDMission(p._potenzielleOidMission);
				
		}
	}
	
	/**
	 * F�gt zu einer Gruppe (z.B. Familie, Haushalt, ...) eine Person hinzu
	 * @param table Gruppen-Table
	 * @param oidgroup Gruppen-OID (z.B. oidfamilie)
	 * @param person Gruppen-Mitglied (z.B. person)
	 */
	private void updateGroup (Map table, long oidgroup, Person person) {
		if (oidgroup != 0) {
			Long objOidGroup = new Long(oidgroup);
			Object obj;
			if ((obj = table.get(objOidGroup)) != null ) {
				if (obj instanceof Person) {
					Vector groupVect = new Vector(2);
					groupVect.add(obj);
					groupVect.add(person);
					table.put(objOidGroup, groupVect);
				} else {
					((Vector)obj).add(person);
				}
			} else {
				table.put(objOidGroup, person);
			}
		}
	}

	/**
	 * Lies alle Personen ein (nur Felder, die wichtig sind f�r den Konsistenz-Check).
	 * Setze eventuell schon gewisse vorberechnete Werte, die im sp�teren Verlauf der Checks
	 * noch gebraucht werden. 
	 * @param c 
	 * @param ht Personentabelle
	 * @throws SQLException
	 */
	private void readAllPersons (Connection c, Map ht, Set<Integer> personOidSubset) throws SQLException {
		readAllPersons(c, ht, personOidSubset, null);
	}

	/**
	 * Lies alle Personen ein (nur Felder, die wichtig sind f�r den Konsistenz-Check).
	 * Setze eventuell schon gewisse vorberechnete Werte, die im sp�teren Verlauf der Checks
	 * noch gebraucht werden.
	 * @param c
	 * @param ht Personentabelle
	 * @throws SQLException
	 */
	private void readAllPersons (Connection c, Map ht, Set<Integer> personOidSubset, List<BOAuthorityAddress> authorityAddresses) throws SQLException {
		_logWriter.writeText("Personen werden eingelesen");
		ResultSet rs = null;
		try {
			String query = "select h.bx_oidperson, h.bx_oidkg, h.bx_oidvorstand, h.bx_oidehepartner, " +
				"h.bx_oidvater, h.bx_oidmutter, h.bx_oidfamilie, h.bx_oidhaushalt, " +
				"h.bx_hatkind, h.bx_statusperson, h.bx_rpgAktiv, h.bx_geburtsdatum, " +
				"h.bx_codestimmrecht, h.bx_stimmrecht, h.bx_konfession, h.bx_oidcodeaufenthalt, " +
				"h.bx_oidkonfession, h.bx_oidcodeortheimat, h.bx_oidmission, h.bx_oidkgorig, " +
				"h.bx_recordstatus, h.bx_oidcodezivilstand, h.bx_zeitungverzicht, " +
				"h.bx_oidcodestrasse, h.bx_hausnr, h.bx_zeitungverzicht, h.bx_codegeschlecht, h.bx_ewid, " + 
				"p.bx_autoZeitung, p.bx_oidehepartner as nfbPartner, p.bx_partnerbezfam, p.bx_faminfo, " +
				"z.oid as oidPersonZeitung, z.bx_oidkirchgemeinde, " +
				"p.bx_oidfamilie != h.bx_oidfamilie 'famChanged' " +
				"from bx_personhistory h " +
				"left join bx_person p on p.oid = h.bx_oidperson " +
				"left join bx_zeitung z on h.bx_oidperson = z.oid " +
				"where h.bx_lastentry = 'j' and h.bx_oidperson is not null ";
			
			if (personOidSubset != null && personOidSubset.size() > 0) {
				StringBuffer sb = new StringBuffer(personOidSubset.size() * 10 + 100);
				for (int oid : personOidSubset) {
					sb.append(String.valueOf(oid));
					sb.append(',');
				}
				sb.setCharAt(sb.length() - 1, ')');
				sb.insert(0, "and p.oid in (");
				query += sb.toString();
			}
			Statement s = c.createStatement();
			rs =  s.executeQuery(query);
			long cnt = 0;
			long last_oidPerson = 0; // Die oid der letzten Person merken, damit Duplikate gefiltert werden k�nnen

			BOContext boContext = new BOContext(Locale.getDefault());

			while (rs.next()) {
				Person p = new Person();
				long oidPerson = rs.getLong("bx_oidperson");
				if (oidPerson == 0) {
					//_logInkonsistenzen.writeText("ReadAllPersons: Person mit OID == 0 entdeckt!"); // TODO TEST
					continue;
				}
				// Sollte nicht vorkommen, da schon max(oid) from personhistory Dupletten filtert
				if (oidPerson == last_oidPerson) {
					continue;
				}
				last_oidPerson = oidPerson;
				p._oidPerson = oidPerson;
				String recordStatus = rs.getString("bx_recordstatus");
				if ("A".equals(recordStatus)) p._isImported = true;
				String tmp;
				p._oidKG = rs.getLong("bx_oidkg");
				p._oidOrigKG = rs.getLong("bx_oidkgorig");
				p._oidVorstand = rs.getLong("bx_oidvorstand");
				p._oidOrigPartner = rs.getLong("bx_oidehepartner");
				p._oidNfbPartner = rs.getLong("nfbPartner");
				p._oidVater = rs.getLong("bx_oidvater");
				p._oidMutter = rs.getLong("bx_oidmutter");
				p._oidFamilie = rs.getLong("bx_oidfamilie");
				if (!p._isImported) {
					boolean famChanged = rs.getBoolean("famChanged");
					if (famChanged) {
						_logWriter.writeText("oidFamilie inconsistent with person entry (oidPerson: " + oidPerson + ")");
						p._oidFamilieChanged = famChanged;
					}
				}
				p._oidHaushalt = rs.getLong("bx_oidhaushalt");
				p._geschlecht = (tmp = rs.getString("bx_codegeschlecht")) != null && tmp.length() > 0  ? Character.toUpperCase(tmp.charAt(0)) : ' ';
				p._partnerBez = (tmp = rs.getString("bx_partnerbezfam")) != null && tmp.length() > 0 ? tmp.charAt(0) : ' ';
				p._famInfo = rs.getShort("bx_faminfo"); if (rs.wasNull()) p._famInfo = -1;
				p._oidMission = rs.getLong("bx_oidMission");
				{
					String rawAutoZeitung = rs.getString("bx_autoZeitung");
					if (rawAutoZeitung == null) {
						p._autoZeitungChanged = true;
						p._autoZeitung = false;
					} else {
						p._autoZeitung = "j".equals(rawAutoZeitung);
					}
					
				}
				String statusPerson = rs.getString("bx_statusperson");
				//StatusPerson evaluieren 
				if (statusPerson == null) {
					p._istPassiv = true;
				} else if ("n".equals(statusPerson)) {
					p._istPassiv = true;
				} else if ("j".equals(statusPerson)) {
					p._istPassiv = false;
				} else {
					_logInkonsistenzen.writeText("ReadAllPersons: invalid status person = " + statusPerson + ", oidperson = " + p._oidPerson);
				}
				p._istRPGPassiv = !"j".equals(rs.getString("bx_rpgAktiv"));
				String hatKind = rs.getString("bx_hatkind");
				//HatKind evaluieren
				if (hatKind == null) {
					p._hatKind = false;
				} else if ("n".equals(hatKind)) {
					p._hatKind = false;
				} else if ("j".equals(hatKind)) {
					p._hatKind = true;
				} else {
					_logInkonsistenzen.writeText("ReadAllPersons: invalid bx_hatkind value = " + hatKind + ", oidperson = " + p._oidPerson);
				}
				java.util.Date bd = rs.getDate("bx_geburtsdatum");
				p._geburtsdatum = bd != null ? bd : Consts.GeburtsTagForNull;
				Long oidCodeAufenthalt = new Long(rs.getLong("bx_oidcodeaufenthalt"));

				// Zeitungsverzicht evaluieren
				String zeitungverzicht = rs.getString("bx_zeitungverzicht");
				if (zeitungverzicht == null) {
					p._zeitungverzicht = false;
				} else if ("j".equals(zeitungverzicht)) {
					p._zeitungverzicht = true;
				} else if ("n".equals(zeitungverzicht)) {
					p._zeitungverzicht = false;
				} else {
					_logInkonsistenzen.writeText("ReadAllPersons: invalid bx_zeitungverzicht value = " + zeitungverzicht + ", oidperson = " + p._oidPerson);
				}
				// potenzielle 'F�higkeit' bestimmen, ein Stimmrecht zu haben
				// w�rde dies nicht gemacht, m�ssten viel mehr Felder f�r sp�rere Auswertungen gespeichert werden
				/*if (codeStimmrecht == null && 
						("RK".equals(konfession) || "EV-REF".equals(konfession)) &&
						p._geburtsdatum != null && alter18end.after(p._geburtsdatum) && 
						_oidCodeAufenthaltStimmrechtSet.contains(oidCodeAufenthalt)) {
						*/
				p._oidZivilstand = rs.getByte("bx_oidcodezivilstand");
				// left join returns null for oidPersonZeitung if person has no Zeitung
				p._kriegtZeitung = (rs.getLong("oidPersonZeitung") != 0); 
				p._kriegtZeitungTemp = false; // Ist zuerst false, wird evt. w�hrend mehrerer Analyse-Runden zu true
				p._oidKGZeitung = rs.getLong("bx_oidkirchgemeinde");
				// Soweit es geht, schon vorberechnen, ob eine Person eine Zeitung erhalten kann
				// w�rde dies nicht gemacht, m�ssten viel mehr Felder f�r sp�rere Auswertungen gespeichert werden
				p._potenziellerZeitungsErhalt = (!p._istPassiv && 
						_oidCodeAufenthaltZeitungsErhaltSet.contains(oidCodeAufenthalt) &&
						p._geburtsdatum != null && p._geburtsdatum.before(alter16) && 
						p._oidKG != 0);
				
				/* Amtsadressen-Check */
				long oidCodeStrasse = rs.getLong("bx_oidcodestrasse");
				String hausNummer = rs.getString("bx_hausnr");
				if (hausNummer == null) hausNummer = "";

				//authorityAddresses - check ob codestrasse + hausnr in authorityAddresses enthalten
				Long tmpCodeStrasse = 0L;
				String tmpStreetNo = "";
				Boolean isAuthorityAddressActive;
				for(BOAuthorityAddress address : authorityAddresses){
					if(address != null)
					{
						try{
							tmpCodeStrasse = Long.parseLong(address.getAttribute("OIDCODESTRASSE", boContext));
							tmpStreetNo = address.getAttribute("StreetNo", boContext);
							isAuthorityAddressActive = Boolean.parseBoolean(address.getAttribute("Status", boContext));

							if (oidCodeStrasse == tmpCodeStrasse && tmpStreetNo.equals(hausNummer)){
								p._potenziellesFraumuensterFlag = true; // set the Person of Fraumuensterstr. 18 and Selnaustr. 9 EDVKG as Kirchgemeinde and set the Zeitungsverzicht.
								if(!isAuthorityAddressActive){
									p._potenziellesFraumuensterFlag = false;
									p._replaceEdvkgWithLastPersonKg = true;
								}
								break;
							}
						}catch (Exception ex){
							writeErrorWithStack("Exception aufgetreten beim Abfragen der Attribute von  BOAuthorityAddress:\n" + ex.getMessage(), ex);
						}
					}
				}

				p._egid = UtilsEDVKG.makeEGID(oidCodeStrasse, hausNummer);
				p._ewid = rs.getShort("bx_ewid");
				/* Vorbereitung zum sp�teren Setzen der Mission */
				p.setKonfession((int) rs.getLong("bx_oidkonfession"));
				long oidCodeOrtHeimat = rs.getLong("bx_oidcodeortheimat");
				if (p._isImported && p.getKonfession() == KATHOLISCH) {
					if (oidCodeOrtHeimat == ITALIEN) {
						p._potenzielleOidMission = MISSION_ITALIA;
					} else if (oidCodeOrtHeimat == SPANIEN) {
						p._potenzielleOidMission = MISSION_ESPANOL;
					}
				}
				
				// Person in die Hashtabelle einf�gen
				ht.put(new Long(oidPerson), p);

				cnt++;
				if ((cnt % 10000) == 0) {
					_logWriter.writeText("Personen einlesen, Nummer = " + cnt + "   Used Memory: " + (Sizeof.usedMemory() / (1024 * 1024)) + " MB");
				}
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}
			catch (Exception e) {
				writeErrorWithStack("Exception aufgetreten w\u00e4hrend ReadAllPerson:\n" + e.getMessage(), e);
			}
		}		
	}

	/**
	 * Sortiert ein Vector aus Person-Eintr�gen nach der Personen-OID.
	 * Dient zur Stabilit�t-Bildung im Endresultat, da verschiedene Programmabhandlungen
	 * andere Resultate ergeben f�r eine andere OID-Reihenfolge in den Personen.
	 * @param mitglieder
	 */
	private void sortFamilie(Vector mitglieder) {
		Object[] arr = mitglieder.toArray();
		Arrays.sort(arr);
		for (int i = 0; i < arr.length; i++) {
			mitglieder.setElementAt(arr[i], i);
		}
	}

	void passiviereFamilienOhneEineEinzigeKGMehr (Map<Long, Person> persons, Map<Long, List<Long>> familien) {
		_logWriter.writeText("Setze ehemalige Kirchgemeinden f\u00fcr passive Familien");
		for (List<Long> mitglieder : familien.values()) {
			boolean flag = true;
			for (Long oid : mitglieder) {
				Person p = persons.get(oid);
				// muss hier auf aktiv pr�fen, da die nfb auch passive personen inkludiert
				if (p._oidKG != 0 && p.istAktiv()) {
					flag = false;
				}
			}
			// die aktiven Mitglieder der Familie haben alle oidKG == null, passivieren.
			if (flag) {
				boolean origKGExists = false;
				for (Long oid : mitglieder) {
					Person p = persons.get(oid); 
					if (!p.istAktiv()) continue;
					
					p.setPassiv(true);
					if (p._oidOrigKG != 0) {
						p.setOIDKG(p._oidOrigKG);
						origKGExists = true;
					}
				}
				if (!origKGExists) {
					_logInkonsistenzen.writeText("Inkonsistenz: Die Familie mit OIDFamilie = " + persons.get(mitglieder.get(0))._oidFamilie + " wurde auf passiv gesetzt, aber sie hatte kein Mitglied mit ehemaliger oidkg (oidkgorig).");
				}
			}
		}
	}

	// Erzeuge ein einziges Mal ein Vektor anstatt 300000 Mal von neuem
	private boolean isInitialized = false;

	/**
	 * Diese Methode erstellt ein Hashtable, der die Familien-OIDs als Schl�ssel und
	 * die Vektoren von Person-Objekten als Werte enth�lt
	 * @param persons
	 * @param familien
	 */
	private void createFamilienTable (Map<Long, Person> persons, Map familien) {
		_logWriter.writeText("Erstelle Familientabelle");
		for (Person p : persons.values()) {
			updateGroup(familien, p._oidFamilie, p);
		}
	}
	
	/**
	 * Geht alle Gruppen einer Gruppen-Hashtable durch (z.B. Familien oder Haushalte)
	 * und ruft pro Gruppe (Familie, ...) eine bestimmte Methode auf
	 * @param alleFamilien
	 * @param auswahl falls nicht null dann die Auswahl der Familien, die durchzugehen sind
	 * @param methode Die aufzurufende Methode
	 * @throws Exception
	 */
	private void gruppenDurchgang (Map<Long, ?> alleFamilien, HashSet auswahl, int methode) throws Exception {
		_logWriter.writeText("Gruppendurchgang mit ID = " + methode);

		Vector helperVector = new Vector(1);
		helperVector.add(new Object());
		Vector mitglieder;
		for (Long oidObj : alleFamilien.keySet()) {
			if (auswahl == null || auswahl.contains(oidObj)) { 
				Object tableItem = alleFamilien.get(oidObj);
				if (tableItem != null) {
					// Gruppen-Vektor bestimmen
					if (tableItem instanceof Person) { //tableItem is a oidperson
						Person p = (Person)tableItem;
						helperVector.set(0, p);
						mitglieder = helperVector;
					} else { // tableItem is a vector with oidperson values
						mitglieder = (Vector)tableItem;
					}
					// Gruppen-Vektor bestimmen END
					
					switch (methode) {
					case FamilieChanged: buildSetForFamilienChangedThroughImport (mitglieder, oidObj); break;
					//case HaushalteChanged: updateHaushalteChanged (mitglieder); break;
					case UpdateFamilienChanged: updateFamilienChanged (mitglieder); break;
					case UpdateZeitungsEmpfaenger: updateZeitungsErhalt_Familie(mitglieder); break; 
					case SortiereFamilie: sortFamilie(mitglieder); break;
					default: throw new Exception("gruppenDurchgang() mit ung\u00fcltigem Methoden-Parameter aufgerufen!");
					}
				} else {
					_logInkonsistenzen.writeText("Inkonsistenz: Familie mit OID = " + oidObj + " nicht gefunden.");
				}
			}
		}
	}
	
	private long getOIDperSQL (Connection _dbConn, String sql, String platzhalter) throws XMetaModelQueryException, Exception {
		try {
			sql = sql.substring(0, sql.indexOf("???")) + platzhalter + sql.substring(sql.indexOf("???") + 3); 
			long res = Long.parseLong(getSet(_dbConn, sql));
			if (res == 0) throw new NumberFormatException();
			if (res == -1) throw new Exception();
			return res;
		} catch (Exception e) {
			throw new Exception("Error in getOIDperSQL(): Die Query gab" +
					"ein ung\u00fcltiges Resultat zur\u00fcck:\n" + sql);
		}
	}
	
	private void _initialize () throws XMetaModelQueryException, Exception {

		// Wichtige OIDs ermitteln
		String query1 = "select OID from bx_codeaufenthalt where bx_code = '???'";
		/*
		_oidCodeAufenthaltStimmrechtSet = new HashSet(2);
		
		_oidCodeAufenthaltStimmrechtSet.add(new Long(getOIDperSQL(_dbConn, query1, "STADTB")));
		_oidCodeAufenthaltStimmrechtSet.add(new Long(getOIDperSQL(_dbConn, query1, "CH NL")));
		*/
		

		_oidCodeAufenthaltZeitungsErhaltSet = new HashSet(20);
		_oidCodeAufenthaltZeitungsErhaltSet.add(new Long(getOIDperSQL(_dbConn, query1, "B EG")));
		_oidCodeAufenthaltZeitungsErhaltSet.add(new Long(getOIDperSQL(_dbConn, query1, "C AN")));
		_oidCodeAufenthaltZeitungsErhaltSet.add(new Long(getOIDperSQL(_dbConn, query1, "C EG")));
		_oidCodeAufenthaltZeitungsErhaltSet.add(new Long(getOIDperSQL(_dbConn, query1, "C FL")));
		_oidCodeAufenthaltZeitungsErhaltSet.add(new Long(getOIDperSQL(_dbConn, query1, "C NN")));
		_oidCodeAufenthaltZeitungsErhaltSet.add(new Long(getOIDperSQL(_dbConn, query1, "CH NL")));
		_oidCodeAufenthaltZeitungsErhaltSet.add(new Long(getOIDperSQL(_dbConn, query1, "CH NN")));
		_oidCodeAufenthaltZeitungsErhaltSet.add(new Long(getOIDperSQL(_dbConn, query1, "EXEM")));
		_oidCodeAufenthaltZeitungsErhaltSet.add(new Long(getOIDperSQL(_dbConn, query1, "L PR2")));
		_oidCodeAufenthaltZeitungsErhaltSet.add(new Long(getOIDperSQL(_dbConn, query1, "STADTB")));

		_oidCodeZivilstandLedig = getOIDperSQL(_dbConn, "select oid from bx_codezivilstand where bx_code = '???'", "LEDIG");
		_oidCodeZivilstandVerheiratet = getOIDperSQL(_dbConn, "select oid from bx_codezivilstand where bx_code = '???'", "VERH");
		_oidCodeZivilstandEingetrPartner = getOIDperSQL(_dbConn, "select oid from bx_codezivilstand where bx_code = '???'", "EIP");

		/* Stichdaten initialisieren */
		/*
		Calendar cal = Calendar.getInstance();
		
		// Bestimme Zeitraum, um Personen zu bestimmen, die 18 Jahre alt geworden sind
		cal.setTime(new java.util.Date());
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.YEAR , -18); // Heute vor 18 Jahren
		alter18end = cal.getTime();
		
		cal.add(Calendar.DAY_OF_YEAR , -14); // Und nochmals zwei Wochen fr�her
		alter18start = cal.getTime();
		// End Bestimme Zeitraum
		*/
		
		Calendar cal = Calendar.getInstance();
		
		// Bestimme Stichdatum f�r �ber 16-J�hrige
		cal.setTime(new java.util.Date());
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.YEAR, -2);
		alter2 = cal.getTime();
		cal.add(Calendar.YEAR, -14);
		alter16 = cal.getTime();
		cal.add(Calendar.YEAR, -2);
		alter18 = cal.getTime();
		isInitialized = true;
		
		//System.out.println("Stimmrechts-config: \nRef-Agespan: " + _ageSpanFromRef + " - " + _ageSpanToRef + "\nKath-Agespan: " + _ageSpanFromKath + " - " + _ageSpanToKath + "\nAufenthaltsCodes (Ref/Kath): " + _oidCodeAufenthaltStimmrechtSetRef + " / " + _oidCodeAufenthaltStimmrechtSetKath);
	}
	
	/**
	 * F�hrt die Familienbildung und die weiteren Checks des PostImports f�r eine Person und ihre Verwandten
	 * durch ohne die Post Import Queries und ohne eine Aktivation der PersonenHistory Objekte
	 * @param persOID
	 */
	public void callPostImportForSubset(int persOID) {
		List<Long> oidList = new ArrayList<Long>(1);
		oidList.add((long)persOID);
		try {
			startPostImport(oidList);
		} catch (Exception e) {
			_logWriter.writeError("Exception beim automatischen Berechnen des Zeitungserhalts", e);
		} 
	}
	
	/**
	 * 
	 * @param personenSubset wenn <code>null</code> wird �ber alle Personen der Postimport durchgef�hrt, ansonsten nur �ber 
	 * die gegebenen Personen und ihre Verwandten
	 */
	public void startPostImport (List<Long> personenSubset) {
		try {
			if (personenSubset != null) {
				_runCode = RUN_WITHOUT_POSTIMPORTQUERIES | RUN_WITHOUT_ACTIVATION | RUN_WITHOUT_ANFORDERUNGEN;
				_personSubsetOnly = true;
			}
			//boolean saveAutoCommitState;
			
			_logWriter.writeText("Der Konsistenz-Check wurde gestartet");
			_logWriter.writeText("------------------------------------");
	
			long heap1 = getHeap();
			_logWriter.writeText("Memory Check: Heap zu Beginn vom Consistency Check: " + (heap1 / (1024*1024)) + " MB");
			
			_initialize();
			
			/*** KONSISTENZ-CHECK ***/

			/*** Auf SQL-Basis ***/
			/* BEGIN TRANSACTION */
			//saveAutoCommitState = _dbConn.getAutoCommit();
			//_dbConn.setAutoCommit(false); // Die n�chste Transaktion beginnt beim ersten SQL-Statement

			if ((_runCode & RUN_WITHOUT_POSTIMPORTQUERIES) == 0) {
				PostImportHelper.doPostImportQueries(_dbConn, _logWriter);
			}
			if ((_runCode & RUN_WITHOUT_ANFORDERUNGEN) == 0) {
				Anforderungen.doPostImportHandling(_dbConn, _logWriter);
			}

			/*** Auf Java-Basis ***/
			Set<Integer> personenOIDs;
			Map<Long, List<Long>> nfbFamilien;
			if (_personSubsetOnly) {
				_per = new LinkedHashMap<Long, Person>(personenSubset.size(), 0.9f);
				_alleFamilien = new LinkedHashMap<Long, Object>(personenSubset.size(), 0.9f);
				personenOIDs = PostImportHelper.getRelatedPersons(personenSubset, _dbConn);
				nfbFamilien = new LinkedHashMap<Long, List<Long>>(personenSubset.size(), 0.9f);
				_meldungen = new LinkedHashMap<Long, List<Meldung>>(personenSubset.size(), 0.9f);
			} else {
				// Einlesen aller Personen - nur die f�r die Beziehungsfrage wichtigen Attribute der Klasse einlesen
				_per = new LinkedHashMap<Long, Person>(300000, 0.9f);
				_alleFamilien = new LinkedHashMap<Long, Object>(50000, 0.9f);
				_meldungen = new LinkedHashMap<Long, List<Meldung>>(10000, 0.9f);
				//modifiedFamilienByImport = new HashSet(2000);
				nfbFamilien = new LinkedHashMap<Long, List<Long>>(300000, 0.9f);
				personenOIDs = null;
			}

			//Alle Amtsadressen abfragen
			List<BOAuthorityAddress>  authorityAddressList = BoxMZV.getAuthorityAddresses(_boContext);

			// Alle Personen einlesen
			readAllPersons(_dbConn, _per, personenOIDs, authorityAddressList);

			if (!_personSubsetOnly) {
				/* Checks und Sets von Personen durchf�hren, die mit dem Import zusammenh�ngen */
				doImportChecksAndSets (_per);
			}
			
			/* Neue Familienbildung */
			createNFBFamilien(_per, nfbFamilien);
			passiviereFamilienOhneEineEinzigeKGMehr(_per, nfbFamilien);
			configNFBFamilien(_per, nfbFamilien);
			PostImportHelper.expandMeldungen(_meldungen, nfbFamilien, _logWriter);
			configHaendische(_per);
			/* Neue Familienbildung Ende */
			
			/* Hashtable mit Familien erzeugen */
			createFamilienTable(_per, _alleFamilien);
			/* Jede Familie sortieren nach der OID -> bringt mehr Stabilit�t ins Endresultat des CC */
			gruppenDurchgang(_alleFamilien, null, SortiereFamilie);
			/* Hashtable erzeugen mit allen durch den Import ge�nderten Familien drin */
//			gruppenDurchgang(_alleFamilien, null, FamilieChanged); // �ndert modifiedFamilien
			
			/*** Erste Runde: Alle Personen durcharbeiten ***/
			_logWriter.writeText("Erste Personenrunde");
			for (Person p : _per.values()) {
				// hatKind auf jeden Fall updaten. Damit entf�llt die �berpr�fung von hatKind im
				// Check f�r inkonsistente Familien am Schluss
				updateHatKind_Runde1(p);
				updateFamInfo(p);
			}

			/*** Zweite Runde: Alle Personen durcharbeiten ***/
			_logWriter.writeText("Zweite Personenrunde");
			for (Person p : _per.values()) {
				updateHatKind_LetzteRunde(p);
			}			
			
			/*** Dritte Runde: Alle Personen durcharbeiten f�r Zeitungsempfang ***/
			_logWriter.writeText("Dritte Personenrunde");
			updateZeitungsErhalt();
			
			/*********************************************************/
			/*** Alle n�tigen �nderungen in die DB zur�ckschreiben ***/
			/*********************************************************/
			/* BEGIN TRANSACTION WENN NICHT SCHON BEGONNEN */
			//if (_dbConn.getAutoCommit()) _dbConn.setAutoCommit(false); // Die n�chste Transaktion beginnt beim ersten SQL-Statement

			/* SQL: Personen aus Organisationen l�schen, wenn sie gestorben sind */
			PostImportHelper.updateTableOrganisationen(_dbConn, _sql, _logWriter);     // table bx_organisationen
			/* Ver�nderte Personen in den person history table zur�ckschreiben */
			PostImportHelper.updateTablePersons(_dbConn, _per, true, _logWriter, _boContext); // table bx_personHistory
			Anforderungen anforderungen = new Anforderungen(_dbConn);
			PostImportHelper.updateTableMeldungenUndAnforderungen(_dbConn, _meldungen, anforderungen, nfbFamilien, _logWriter);
			// don't need nfbFamilien anymore, not sure if this helps much
			nfbFamilien = null;
			anforderungen.logCreatedAnforderungen(_logWriter);
			/* Ver�nderungen im Erhalt einer Zeitung zur�ckschreiben */
			PostImportHelper.updateTableZeitungsErhalt(_dbConn, _per, _logWriter); // table bx_zeitung
			
			_logWriter.flush();
			
			if ((_runCode & RUN_WITHOUT_ACTIVATION) == 0) {
				// Postleitzahlen-Setzen ist unabh�ngig vom restlichen Konsistenz-Check: Kein Commit n�tig
				PostImportHelper.activateMutationen(_dbConn, _boContext, _logWriter); //Alle markierten Personen von personhistory nach person kopieren
				/* Nach Ausf�hrung von activateMutationen ist autoCommit bei Verwendung von MySQL auf true */
			}
			/* Ver�nderte Personen in den person table zur�ckschreiben: Erst nach activateMutationen, wegen
			 * der Erstellung von neu importierten Personen */
			PostImportHelper.updateTablePersons(_dbConn, _per, false, _logWriter, _boContext); // table bx_person
			
			if (_personSubsetOnly) {
				PostImportHelper.updateTableFamilienKirchgemeinden(_dbConn, _alleFamilien.keySet());
			} else {
				updateCodeHausKG(_dbConn);
				// Allen vom Import her markierten Personen die Markierung wegnehmen
				clearRecordStatusA();

				_logWriter.writeText("Update Familien Kirchgemeinden");
				PostImportHelper.reinitializeTableFamilienKirchgemeinden(_dbConn);

				_logWriter.writeText("Update Stimmrecht");
				BoxMZV edvkg = (BoxMZV) EEngine.getInstance().getBox("edvkg");
				edvkg.reloadStimmrechtProperties();

				/* COMMIT */
				/*
				if (_dbConn.getAutoCommit() == false) _dbConn.commit();
				_dbConn.setAutoCommit(saveAutoCommitState);
				*/
				
				/* Die zwei Zeitungsfunktionen haben ihr eigenes Commit */
				_logWriter.writeText("Zeitungsorganisationen werden geupdated (1)");
				BoxMZV.deleteExistingZeitungsOrganisation(_boContext.getRequest());
				_logWriter.writeText("Zeitungsorganisationen werden geupdated (2)");
				BoxMZV.createZeitungsOrganisation(_boContext.getRequest());
				
				_logWriter.writeText("Anforderungen werden exportiert");
				if ((_runCode & RUN_WITHOUT_ANFORDERUNGEN) == 0) {
					try {
						int ln = Anforderungen.createExportFile(BoxMZV._anforderungsExportFile, _boContext, _dbConn, _logWriter);
						if (ln != -1) {
							_logWriter.writeText("Anforderungen exportiert mit Laufnummer " + ln);
						}
					} catch (Exception x) {
						String msg = "Beim Exportieren der Anforderungen ist ein Fehler aufgetreten.\nKonsistenz-Check NICHT abgebrochen";
						_logWriter.writeError(msg, x);
						Log.logSystemAlarm(msg, x);
					}
				}
			}
			
			long heap2 = getHeap();
			_logWriter.writeText("Memory Check: Heap am Ende vom Consistency Check: " + (heap2 / (1024*1024)) + " MB");
			_logWriter.writeText("------------------------");
			_logWriter.writeText("Konsistenz-Check beendet");
		} catch (SQLException sqle) {
			writeErrorWithStack("SQL-Exception erhalten w\u00e4hrend des Consistency Checks: " + sqle.getMessage(), sqle);
			/* Sollte eigentlich funktionieren wenn alle autocommit statements in dieser Klasse selbst verwaltet werden
			 * und nicht durch den BLManager beeinflusst werden
			try {
				if (!_dbConn.getAutoCommit()) {
					_logWriter.writeText("Performing Transaction Rollback (falls Transaktionen von der DB unterst�tzt werden).");
					_dbConn.rollback();
				}
			} catch (SQLException se) {
				writeErrorWithStack("SQL-Exception during Rollback (Consistency Check): " + se.getMessage(), se);
			}
			*/
			_logWriter.writeText("Konsistenz-Check ABGEBROCHEN\n");
			_logWriter.writeText("Memory: " + getHeap());
			Log.logSystemAlarm("Konsistenz-Check abgebrochen", sqle);
			SystemInfo.handleInconsitency();
			return;
		} catch (Exception e) {
			writeErrorWithStack("Exception aufgetreten w\u00e4hrend der Ausf\u00fchrung vom Consistency Checks: " + e.getMessage(), e);
			_logWriter.writeText("Memory: " + getHeap());
			_logWriter.writeText("Konsistenz-Check ABGEBROCHEN\n");
			Log.logSystemAlarm("Konsistenz-Check abgebrochen", e);
			SystemInfo.handleInconsitency();
			return;
		} finally {
			if (_dbConn != null) {
				try {
					// Weil BLManager.executeUpdate autoCommit auf false setzt
					_dbConn.setAutoCommit(true);
				} catch (SQLException e) {
					writeErrorWithStack("SQLException occured!", e);
				}
			}
			if (_sql != null) {
				try {
					_sql.close();
				} catch (SQLException e) {
					// do nothing
				}
			}
			if (_logInkonsistenzenClosureNeeded && _logInkonsistenzen != null) {
				_logInkonsistenzen.closeLogFile();
			}
			if (_logWriterClosureNeeded && _logWriter != null) {
				_logWriter.closeLogFile();
			}
		}
		
	}

	private void updateCodeHausKG(Connection c) {
		Statement stmt = null;
		try {
			stmt = c.createStatement();
			String sql = "update bx_person p " +
						 "join bx_codehauskg h on p.bx_oidcodestrasse = h.bx_oidstrasse && h.bx_hausNr = p.bx_hausNr " +
						 "set p.bx_codehauskg = h.oid";
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (stmt != null) stmt.close();
			} catch (SQLException e) {}
		}
	}

	@SuppressWarnings("serial")
	private final class ArrayListSet extends ArrayList<Long> {

		public ArrayListSet(int i) {
			super(i);
		}

		public ArrayListSet(List<Long> l) {
			addAll(l);
		}

		@Override
		public void add(int index, Long element) {
			if (!contains(element)) {
				super.add(index, element);
			}
		}

		@Override
		public boolean add(Long e) {
			if (!contains(e)) {
				return super.add(e);
			} else {
				return false;
			}
		}

		@Override
		public boolean addAll(Collection<? extends Long> c) {
			boolean changed = false;
			for (Long e : c) {
				changed = add(e) | changed;
			}
			return changed;
		}

		@Override
		public boolean addAll(int index, Collection<? extends Long> c) {
			throw new NotImplementedException("");
		}

		@Override
		public Long set(int index, Long element) {
			throw new NotImplementedException("");
		}
		
	};
	
	/**
	 * Erstellt die Familien der Neuen FamilienBildung.
	 * 
	 * Passive Personen sind auch in den Familien.
	 * 
	 * �ber 18j�hrige, ledige und aktive mit relevanter Konfession verbleiben in Familie.
	 * �ber 18j�hrige, ledige und passive erhalten ein Flag falls mind. ein Elternteil in Familie aktiv ist (passive Kinder).
	 * 
	 * 
	 * @param persons Alle Personen
	 * @param fam Tabelle f�r alle Familien, jede Person zeigt auf dieselbe Familienliste, falls sie zur selben Familie
	 * geh�rt
	 */
	private void createNFBFamilien(Map<Long, Person> persons, Map<Long, List<Long>> fam) {
		_logWriter.writeText("Erstelle Familien");
		Long[] famBezOIDs = new Long[4];
		List<Long> famListToMerge;
		/* Bilde Familienliste durch Anschauung von
		 * OIDVater, OIDMutter und der Kriterien, die ein Kind einer MZV-Familie ausmachen
		 * bzw. OIDEhepartner (auch eingetr. Partnerschaft)
		 */
		for (Entry<Long, Person> pEntry : persons.entrySet()) {
			Person k = pEntry.getValue();
			famBezOIDs[3] = pEntry.getKey();
			if (famBezOIDs[3] > Consts.AlleHaendischeGrenze) {
				continue;
			}
			/* Phase 1: Sammeln der unmittelbaren famili�ren Beziehungen */
			
			// would be better to check, whether k is NOT within a konkubinat
			// as well but since this cannot be done efficiently we'll just split 
			// as necessarry later on (see splitFamily)
			
			// commented out second part of the following condition to fulfill, that 
			// children (within a family which is no Konkubinat) with no relevant konfession
			// MUST be flagged with Consts.FI_PassivesAusgewachsenesKindMask
			if (k.istLedig() /* && (!k.istVolljaehrig() || k.hatRelevanteKonfession()) */) {
				famBezOIDs[0] = k._oidMutter != 0 ? k._oidMutter : null;
				famBezOIDs[1] = k._oidVater != 0 ? k._oidVater : null;
				if (k._isImported && k.istKleinkind()) {
					Person mutter = persons.get(k._oidMutter);
					if (mutter != null && !mutter._isImported && 
						k.hatGleicheMeldeAdresse(mutter) && k._ewid == 0 && mutter._ewid != 0) {
						// Adresszusatzindex vom Kind gleich der Mutter setzen, weil wahrscheinlich Geburt
						k.setAdresszusatzindex(mutter._ewid);
						k.addMeldung(Meldungsvorlage.Fam_AziKindGemaessMutter, String.valueOf(k._oidPerson));
					}
				}
			} else {
				famBezOIDs[0] = null;
				famBezOIDs[1] = null;
			}
			famBezOIDs[2] = k._oidOrigPartner != 0 ? k._oidOrigPartner : null;
			// Invariante: alle Eintr�ge von famBezOIDs stimmen
			/* Ende Phase 1 */
			/* Phase 2: Erstellen der Menge der Familienmitglieder oder Erweitern, 
			 * falls schon vorhanden */
			famListToMerge = null;
			for (Long pOID : famBezOIDs) {
				if (pOID == null) continue;
				
				if (k._oidPerson != pOID) {
					Person p = persons.get(pOID);
					if (p == null || !k.hatGleicheMeldeAdresseUndAZI(p)) { 
						continue;
					}
				}
				List<Long> famList = fam.get(pOID);
				if (famList != null) {
					// f�r aktuellen Elternteil/Partner/Person gibt es schon eine Liste 
					if (famListToMerge != null) {
						if (famList == famListToMerge) {
							continue;
						}
						// Merge der Arbeitsliste dieses Loops mit der Liste der Hashtable
						famList.addAll(famListToMerge);
						// F�r alle gemergten Personen Zeiger auf Familienliste aktualisieren
						for (Long tmpOID : famListToMerge) {
							fam.put(tmpOID, famList);
						}
					} 
					famListToMerge = famList;
				} else {
					// f�r aktuellen Elternteil/Partner/Person gibt es noch keine Liste 
					if (famListToMerge == null) {
						famListToMerge = new ArrayListSet(4);
					}
					famListToMerge.add(pOID);
					// Familienliste f�r aktuelle Person setzen:
					fam.put(pOID, famListToMerge);
				}
			}
			/* Ende Phase 2 */
		}
		/*
		 * Phase 3:
		 * 1. �ber 18j�hrige, welche noch bei Vater und/oder Mutter in Familie sind, und Kinder haben, in eigene Familie stecken
		 * 2. �ber 18j�hrige, welche noch bei Vater und/oder Mutter in Familie sind, und passiv (geworden) sind, mit Flag behaften
		 * + Familien splitten, falls n�tig (ein ausgewachsenes Kind ist in einem Konkubinat) 
		 */
		List<Long> eigeneFam = new ArrayList<Long>(2);
		for (Entry<Long, Person> pEntry : persons.entrySet()) {
			Person k = pEntry.getValue();
			Long pOID = pEntry.getKey();
			
			if (pOID > Consts.AlleHaendischeGrenze) {
				continue;
			}
			
			boolean famInfoSet = false;
			if (k.istLedig() && k.istVolljaehrig()) {
				// testen, ob Mutter oder Vater in Familie
				List<Long> famListe = fam.get(pOID);
				if (famListe.contains(k._oidMutter) && k.getMutter().istAktivAny() || famListe.contains(k._oidVater) && k.getVater().istAktivAny()) {
					// Person k hat relevante Konfession, sonst w�re sie nicht in der Familie
					// wir haben ein �ber 18-j�hriges Kind k, das noch ledig in der Familie lebt
					// hat es Kinder?
					boolean kinderVorhanden = false;
					boolean gemeinsamesKind = false;
					eigeneFam.clear();
					for (Long mitgliedOID : famListe) {
						Person k2 = persons.get(mitgliedOID);
						if (k2.istAktivAny()) {
							boolean istMutter = pOID == k2._oidMutter;
							boolean istVater = !istMutter && pOID == k2._oidVater;
							gemeinsamesKind = (istMutter && famListe.contains(k2._oidVater)) || (istVater && famListe.contains(k2._oidMutter));
							if (gemeinsamesKind) {
								// Kind k2 hat Mutter und Vater in der Familie
								// d.h. Ausgewachsenes Kind k ist teil eines Konkubinates
								splitFamily(k2, persons, fam);
								break;
							} else if (istMutter || istVater /* Es k�nnte ja auch ein minderj�hriger Vater sein */) {
								kinderVorhanden = true;
								eigeneFam.add(mitgliedOID);
							}
						}
					}
					
					if (!gemeinsamesKind) {
						if (kinderVorhanden) {
							// Eigene Familie erstellen
							eigeneFam.add(pOID);
							List<Long> neueFam = new ArrayListSet(eigeneFam);
							for (Long mitgliedOID : eigeneFam) {
								famListe.remove(mitgliedOID);
								fam.put(mitgliedOID, neueFam);
							}
						} else if (!k.istAktivAny()) {
							// Flag setzen f�r passive Kinder f�r Familiendarstellung
							k.setFamilienInfo((short) (k._famInfo | Consts.FI_PassivesAusgewachsenesKindMask));
							famInfoSet = true;
						}
					}
				}
			}
			if (!famInfoSet) {
				k.setFamilienInfo((short) (k._famInfo & ~Consts.FI_PassivesAusgewachsenesKindMask));
			}
		}
		/* Ende Phase 3 */
	}
	
	/**
	 * For the specified child, collects its parents and the children (and children of children [...]) of both parents 
	 * and splits the members off of the original family.
	 */
	private void splitFamily(Person child, Map<Long, Person> persons, Map<Long, List<Long>> families) {
		long oidMutter = child._oidMutter, oidVater = child._oidVater;
		List<Long> family = families.get(child.getOID());
		if (!family.contains(oidMutter) || !family.contains(oidVater))  {
			throw new RuntimeException("mustn't split family for child: " + child);
		}
		Collection<Person> parents = child.getParents(persons);
		Map<Long, Person> famMap = new HashMap<Long, Person>();
		for (Long oid : family) {
			famMap.put(oid, persons.get(oid));
		}
		List<Long> ownSplit = new ArrayListSet(4);
		List<Long> otherSplit = new ArrayListSet(4);
		for (Long oid : family) {
			// It's okay that we include possibly "wrong" children or subChildren (i.e. children that have their 
			// own family within this family ("ownSplit")) because they'll get split later on (by this method).
			// Note that isChildOf checks only within famMap, thus we won't violate the family composition.
			// however this might partition 4 generations incorrectly, because the top two levels
			// will not get merged again
			if (oid == oidMutter || oid == oidVater || persons.get(oid).isChildOfDeep(parents, famMap)) {
				ownSplit.add(oid);
			} else {
				otherSplit.add(oid);
			}
		}
		for (Long oid : ownSplit) {
			families.put(oid, ownSplit);
		}
		for (Long oid : otherSplit) {
			families.put(oid, otherSplit);
		}
	}

	/**
	 * Setzt u.a. OIDFamilie (= Vorstand), OIDEhepartner, partnerBez, istVorstand.
	 * 
	 * Der Vorstand und seine Partnerin (falls vorhanden) sind nie passiv, ausser in einer komplett passiven Familie.
	 * 
	 * Es kann theoretisch mehrere Ehen, eing. Partnerschaften oder konkubinatsm�ssige Partnerschaften innerhalb derselben
	 * NFB-Familie geben, es wird jedoch immer eine ausgew�hlt und davon ein Vorstand bestimmt.
	 * 
	 * partnerBez:
	 * <ul>
	 * <li>p: Familie ist komplett passiv</li>
	 * <li>e: Familie mit Ehe</li>
	 * <li>k: Familie mit Konkubinat</li>
	 * <li>(v): Familie mit Ehe oder Konkubinat (reserviert f�r RPG-aktive Erziehungsberechtigte, �ber deren eheliche Beziehung keine Informationen vorliegen)</li>  
	 * <li>g: Familie mit eingetr. Partnerschaft</li>
	 * <li>a: Familie mit nur einem aktiven Elternteil bzw. alleinstehende Person</li>
	 * </ul>
	 */
	private void configNFBFamilien(Map<Long, Person> persons, Map<Long, List<Long>> fam) {
		_logWriter.writeText("Konfiguriere Familien");
		List<String> meldungen = new ArrayList<String>();
		Date today = new Date();
		// Familien kommen in der fam Hashtable als value mehrmals vor: Set draus machen 
		Set<List<Long>> familien = new HashSet<List<Long>>(fam.values());
		// note: result is dependent on iteration order IF data is invalid (e.g. multiple mothers or fathers per family)
		for (List<Long> familie : familien) {
			meldungen.clear();
			Person[] famPers = new Person[familie.size()];
			int[] kinderZahl = new int[familie.size()];
			// Erste Phase: Ermitteln von Ehepartner, Ermitteln von Elternteilen von Kinder innerhalb Familie und
			// Ermitteln des Partnertyps
			long partner1OID = 0;
			long partner2OID = 0;
			boolean vorstandEindeutig = true;
			boolean vaterGefunden, mutterGefunden, potenziellesKonkubinatGefunden = false;
			char partnerBez = ' ';
			long vaterOID = 0;
			long mutterOID = 0;
			long vorstandOID = 0;
			boolean famPassiv = true;
			int i = 0;
			for (Long pOID : familie) {
				Person p = persons.get(pOID);
				famPassiv = famPassiv && !p.istAktivAny();
				famPers[i++] = p;
				if (!p.istAktivAny()) continue;
				
				if (partner1OID == 0 || partner2OID == 0) {
					// nicht hineingehen, falls schon ein Paar gefunden wurde
					if (p._oidOrigPartner != 0 && familie.contains(p._oidOrigPartner) && p.getOrigPartner().istAktivAny()) {
						Person partner = p.getOrigPartner();
						if (p.equals(partner.getOrigPartner())) {
							// gegenseitig referenziertes Ehepaar gefunden
							partner1OID = pOID;
							partner2OID = p._oidOrigPartner;
						} else {
							// Es wird nur der eine Partner vermerkt, sonst gibt es zu viele Meldungen
							if (partner.getOrigPartner() == null) {
								partner.addMeldung(Meldungsvorlage.Fam_UnidirektPartnerVerbindung, partner._oidPerson, pOID);
							} else {
								// Inkonsistenz
								partner.addMeldung(Meldungsvorlage.Fam_UngleichePartner, pOID, partner._oidPerson);
							}
						}
					}
				}
				mutterGefunden = vaterGefunden = false;
				if (p._oidMutter != 0 && familie.contains(p._oidMutter) && p.getMutter().istAktivAny()) {
					if (!potenziellesKonkubinatGefunden) mutterOID = p._oidMutter;
					kinderZahl[familie.indexOf(mutterOID)]++;
					mutterGefunden = true;
				}
				if (p._oidVater != 0 && familie.contains(p._oidVater) && p.getVater().istAktivAny()) {
					if (!potenziellesKonkubinatGefunden) vaterOID = p._oidVater;
					kinderZahl[familie.indexOf(vaterOID)]++;
					vaterGefunden = true;
				}
				potenziellesKonkubinatGefunden = potenziellesKonkubinatGefunden || mutterGefunden && vaterGefunden;
				/* Vorstand, wie er von der OIZ als st�dtisches Feld geliefert wird, nehmen. Evt. ist (war) es aber 
				 * ein von der Stadt berechnetes Feld, darum nehmens wir dann trotzdem nicht bzw. setzen es in der
				 * DB auf null.  
				 */
				if (p._oidVorstand != 0 && familie.contains(vorstandOID) && p.getOIZVorstand().istAktivAny()) {
					if (vorstandOID != 0 && vorstandOID != p._oidVorstand) {
						vorstandEindeutig = false;
					} else {
						vorstandOID = p._oidVorstand;
					}
				}
			}
			vorstandEindeutig = vorstandEindeutig && vorstandOID != 0;
			
			// Unter der Vorbedingung, dass die Basis-Familiensetbildung grunds�tzlich l�uft
			// m�ssen, wenn die folgende Bedingung wahr ist, die Partner klar sein.
			if (partner1OID != 0 && partner2OID != 0) {
				Person p1 = persons.get(partner1OID);
				Person p2 = persons.get(partner2OID);
				p1.setOIDNfbPartner(partner2OID);
				p2.setOIDNfbPartner(partner1OID);
				partnerBez = p1._geschlecht == p2._geschlecht ? 'g' : 'e';
				if (!vorstandEindeutig) {
					int cmp = ((Boolean) p1.hatRelevanteKonfession()).compareTo(p2.hatRelevanteKonfession());
					if (cmp != 0) {
						vorstandOID = cmp > 0 ? partner1OID : partner2OID;
						vorstandEindeutig = true;
					}
				}
				if (!vorstandEindeutig) {
					int ixP1 = familie.indexOf(partner1OID);
					int ixP2 = familie.indexOf(partner2OID);
					int k1 = kinderZahl[ixP1];
					int k2 = kinderZahl[ixP2];
					if (k1 == k2) {
						// wenn beide Elternteile gleiche Anzahl Kinder haben, dann Mann nehmen
						vorstandOID = famPers[ixP1]._geschlecht == 'M' ? partner1OID : partner2OID;
						if (partnerBez == 'g') {
							vorstandOID = famPers[ixP1]._geburtsdatum.before(famPers[ixP2]._geburtsdatum) ? partner1OID : partner2OID;
						}
					} else {
						vorstandOID = k1 > k2 ? partner1OID : partner2OID;
					}
					vorstandEindeutig = true;
				}
			} else if (potenziellesKonkubinatGefunden) {
				// Konkubinat
				Person mutter = persons.get(mutterOID);
				Person vater = persons.get(vaterOID);
				mutter.setOIDNfbPartner(vaterOID);
				vater.setOIDNfbPartner(mutterOID);
				partner1OID = vaterOID; // f�r Phase 2
				partner2OID = mutterOID; // f�r Phase 2
				partnerBez = 'k';
				if (!vorstandEindeutig) {
					vorstandOID = kinderZahl[familie.indexOf(mutterOID)] > kinderZahl[familie.indexOf(vaterOID)] ? mutterOID : vaterOID;
					vorstandEindeutig = true;
				}
			} else if (famPassiv) {
				// Ganze Familie passiv: Vorstand ist �lteste Person
				Date aeltestesGeburtsdatum = today;
				vorstandOID = 0;
				for (Long pOID : familie) {
					Person p = persons.get(pOID);
					if (p._geburtsdatum != null && p._geburtsdatum.before(aeltestesGeburtsdatum)) {
						vorstandOID = pOID;
						aeltestesGeburtsdatum = p._geburtsdatum;
					}
				}
				if (vorstandOID == 0) {
					vorstandOID = familie.get(0);
				}
				vorstandEindeutig = true;
				partnerBez = 'p';
			} else {
				// Einsames (Eltern-) Teil
				partnerBez = 'a'; 
				vorstandEindeutig = true;
				if (mutterOID == 0 && vaterOID == 0) {
					vorstandOID = 0;
					Set<Long> requested = new HashSet<Long>(4);
					for (Long vOID : familie) {
						Person p = persons.get(vOID); 
						if (p.istAktivAny()) {
							if (vorstandOID != 0) {
								p.addMeldung(Meldungsvorlage.Fam_AlleinstehendMitFamilie, vorstandOID);
							}
							vorstandOID = vOID;
							// lonely children could have mismatching parents
							// ensure, that all parents are requested
							if (!p.istVolljaehrig()) {
								long mutter = p._oidMutter, vater = p._oidVater;
								if (mutter > 0 && requested.add(mutter)) {
									p.addMeldung(Meldungsvorlage.PsvMutterError, mutter);
								}
								if (vater > 0 && requested.add(vater)) {
									p.addMeldung(Meldungsvorlage.PsvVaterError, vater);
								}
							}
						}
					}
					// Auf alleinstehende Minderj�hrige achten
					Person v = persons.get(vorstandOID);
					if (!v.istVolljaehrig()) {
						v.addMeldung(Meldungsvorlage.Fam_AlleinstehendeKinder, vorstandOID);
					}
				} else {
					vorstandOID = mutterOID != 0 ? mutterOID : vaterOID;
				}
			}
			
			// Zweite Phase: Setzen der Familie und Vorst�nde und des Beziehungsbezeichners
			// Sicherstellen, dass Partner bei aktiven nach NFB gesetzt ist 
			for (Person p : famPers) {
				p.setOIDFamilie(vorstandOID);
				p.setPartnerBeziehung(partnerBez);

				// F�r alle passiven Originalehepartner setzen
				if (p._istPassiv) {
					p.setOIDNfbPartner(p._oidOrigPartner);
				} else if (partner1OID != p._oidPerson && partner2OID != p._oidPerson) {
					p.setOIDNfbPartner(0);
				}
			}
			
			if (meldungen.size() > 0) {
				for (String meldung : meldungen) {
					_logInkonsistenzen.writeError(meldung + " (NFB " + vorstandOID + ").");
				}	
			}
		}
	}
	
	private void configHaendische(Map<Long, Person> persons) {
		_logWriter.writeText("Konfiguriere H\u00e4ndische");
		for (Long oid : persons.keySet()) {
			if (oid > Consts.AlleHaendischeGrenze) {
				Person p = persons.get(oid);
				p.setOIDFamilie(oid);
				p.setPartnerBeziehung('a');
			}
		}
	}
	
	void updateFamInfo(Person p) {
		short bitfield = UtilsEDVKG.getFamilienInfoBitField(p, p._famInfo);
		p.setFamilienInfo(bitfield);
	}

	/**
	 * Bilde die Menge der Familien, die mindestens ein Mitglied haben, das durch den Import
	 * ver�ndert wurde
	 * @param mitglieder
	 * @param oidFamilie
	 */
	void buildSetForFamilienChangedThroughImport (Vector mitglieder, Long oidFamilie) {
		for (int i = 0; i < mitglieder.size(); i++) {
			Person p = (Person)mitglieder.elementAt(i);
			if (p._isImported) {
				modifiedFamilienByImport.add(oidFamilie);
				return;
			}
		}
	}

	int counteruFC = 0;
	void updateFamilienChanged (Vector mitglieder) {
		counteruFC++;
		if (counteruFC % 200 == 0) {
			_logWriter.writeText("updateFamilien SQL not yet implemented");
		}
	}
	
	/**
	 * Sage dem Vater oder der Mutter, dass sie ein Kind hat falls die aktuelle Person
	 * einen Vater oder eine Mutter hat, die im gleichen Haushalt lebt.
	 * Die Methode arbeitet nach dem Prinzip "Wenn einmal auf true dann nie wieder false".
	 * @param p aktuelle Person
	 */
	void updateHatKind_Runde1 (Person p) {
		// with the SchulDaten Update (version after S2) this check will cause minor errors
		// for parents with children that are RPG-Aktiv only
		// however should recheck hatKind anyway within setupFamily Anrede depending on isRPG
		if (!p.istAktivAny()) return;
		
		Person mutter = p.getMutter();
		if (mutter != null && p._oidFamilie == mutter._oidFamilie) {
			mutter._hatKindTemp = true;
		}
		Person vater = p.getVater();
		if (vater != null && p._oidFamilie == vater._oidFamilie) {
			vater._hatKindTemp = true;
		}
	}
	
	/**
	 * Setzt hatKind definitiv.
	 * Personen mit OID �ber 500000000 (sind nicht durch Import ber�hrt) nicht anschauen
	 * @param p
	 */
	void updateHatKind_LetzteRunde (Person p) {
		if (p._oidPerson < Consts.AlleHaendischeGrenze) p.setHatKind(p._hatKindTemp);
	}

	/**
	 * Ermittelt, ob eine Person eine Zeitung erhalten soll
	 * (wenn die Person potenziell eine Zeitung erhalten kann und wenn sie der Vorstand ist)
	 * @param p aktuelle Person
	 */
	private void updateZeitungsErhalt_Runde1 (Person p) {
		if (p._potenziellerZeitungsErhalt && p._oidFamilie == p._oidPerson) {
			p._kriegtZeitungTemp = true;
		}
	}

	/**
	 * Ermittelt, ob eine Person eine Zeitung erhalten soll
	 * (wenn eine Person potenziell eine Zeitung erhalten kann, wenn sie nicht der Vorstand ist,
	 * nicht ledig ist und die Familie f�r die eigene KG nicht schon eine Zeitung erh�lt) 
	 * @param p aktuelle Person
	 */
	private void updateZeitungsErhalt_Runde2 (Person p) {
		if (p._potenziellerZeitungsErhalt && !p._kriegtZeitungTemp && p._oidFamilie != p._oidPerson && !p.istLedig()) {
			// �berpr�fen, ob nicht schon f�r die eigene KG eine Zeitung in der Familie abonniert wurde
			if (!p.kriegtFamilieSchonZeitung()) {
				p._kriegtZeitungTemp = true;
				return;
			}
		} 
	}
	
	/**
	 * Ermittelt, ob eine Person eine Zeitung erhalten soll
	 * (wenn eine Person potenziell eine Zeitung erhalten kann, wenn sie nicht der Vorstand ist,
	 * LEDIG ist und die Familie f�r die eigene KG nicht schon eine Zeitung erh�lt) 
	 * 
	 * Die Routine setzt auch den definitiven Wert, ob die Person eine Zeitung erh�lt.
	 * Falls die Person einen Zeitungsverzicht hat, dann keine Zeitung vergeben.
	 * @param p aktuelle Person
	 */
	private void updateZeitungsErhalt_LetzteRunde (Person p) {
		if (p._potenziellerZeitungsErhalt && !p._kriegtZeitungTemp && p._oidFamilie != p._oidPerson && p.istLedig()) {
			// �berpr�fen, ob nicht schon f�r die eigene KG eine Zeitung in der Familie abonniert wurde
			if (!p.kriegtFamilieSchonZeitung()) {
				p._kriegtZeitungTemp = true;
			}
		} 
		p.setAutoZeitung(p._kriegtZeitungTemp);
		p.setKriegtZeitung(p._kriegtZeitungTemp && !p._zeitungverzicht);
	}

	/**
	 * Setzt Zeitungsempf�nger innerhalb der Familie. Es braucht drei unabh�ngige Runden, da
	 * sp�tere Runden von den vorhergehenden abh�ngig sind. 
	 * @param mitglieder
	 */
	private void updateZeitungsErhalt_Familie (Vector mitglieder) {
		/* Runde 1 */
		for (int i = 0; i < mitglieder.size(); i++) {
			Person p = (Person)mitglieder.elementAt(i);
			if (p != null) updateZeitungsErhalt_Runde1(p);
		}
		/* Runde 2 */
		for (int i = 0; i < mitglieder.size(); i++) {
			Person p = (Person)mitglieder.elementAt(i);
			if (p != null) updateZeitungsErhalt_Runde2(p);
		}
		/* Runde 3 */
		for (int i = 0; i < mitglieder.size(); i++) {
			Person p = (Person)mitglieder.elementAt(i);
			if (p != null) updateZeitungsErhalt_LetzteRunde(p);
		}
	}
	
	/**
	 * Ermittle alle Zeitungsempf�nger. Die Methode greift darum auf die Familien zur�ck,
	 * da deren Mitglieder nach OID sortiert sind. Das macht den �bergang vom alten auf den
	 * neuen Konsistenz-Check leichter.
	 * @throws Exception
	 */
	private void updateZeitungsErhalt () throws Exception {
		/* Alle Personen, die einer Familie angeh�ren, durchgehen */
		gruppenDurchgang(_alleFamilien, null, UpdateZeitungsEmpfaenger);
		/* Restliche Personen */
		_logWriter.writeText("Setze Zeitungsempf\u00e4nger unter den Personen ohne Familien-OID");
		for (Person p : _per.values()) {
			if (p != null && !_alleFamilien.containsKey(p._oidFamilie)) { // Nur in diesem Fall soll Zeitung nicht via Gruppendurchgang gesetzt werden
				/* Da die Person keine Familien-OID hat, d.h. vor der Initialisierung hatte, 
				 * k�nnen alle drei Methode direkt nacheinander aufgerufen werden */ 
				updateZeitungsErhalt_Runde1(p);
				updateZeitungsErhalt_Runde2(p);
				updateZeitungsErhalt_LetzteRunde(p);
			}
		}
	}
	
	/**
	 * Hebe den Import-Status der Zeilen im Table 'personhistory' auf 
	 * @throws SQLException
	 */
	void clearRecordStatusA () throws SQLException {
		String query = "update "+_shadowTable+" as person "+
	    " set person.bx_RecordStatus = 'O' "+
	    " where person.bx_RecordStatus = 'A' ";
		_logWriter.writeText("clearRecordStatusA = '" + query + "'");
		_sql.execute(query);
	}

	/**
	 * Die set-Methoden dieser Klasse setzen Werte, die zuvor von der Datenbank gelesen wurden.
	 * Falls die Werte �ndern, wird von der set-Methode ein Changed-Flag gesetzt, damit sp�ter
	 * entschieden werden kann, ob das entsprechende Feld in der Datenbank geupdated werden muss.
	 * 
	 * Die 'setImported'-Methoden aktualisieren in der DB im Table History nur Personen, die vom
	 * Import ver�ndert wurden.
	 */
	public final class Person implements java.lang.Comparable, IPerson {
		
		boolean _autoZeitungChanged;
		boolean _istPassiv;// Falls die Person gar nicht aktiv ist (StatusPerson = 'n' oder null)
		boolean _istRPGPassiv;
		boolean _hatKind; // Wenn von diesem Elternteil mindestens ein Kind im gleichen Haushalt lebt
		boolean _kriegtZeitung; // Person erh�lt mindestens eine Zeitung
		boolean _zeitungverzicht; // Will keine Zeitung
		boolean _ewidChanged; // Adresszusatzindex hat ge�ndert
		
		/* Diese Variablen werden nur gelesen */ 
		boolean _isImported; // Person wurde durch Import ver�ndert (Recordstatus = 'A')
		boolean _potenziellesStimmrecht = false; // Hilfsvariable, um das Stimmrecht zu ermitteln
		boolean _potenziellerZeitungsErhalt = false; // Hilfsvariable, um den Zeitungserhalt zu ermitteln
		boolean _kriegtZeitungTemp = false; // Hilfsvariable, wird zuerst w�hrend mehrerer 
											// Runden ermittelt bevor _kriegtZeitung ver�ndert wird
		boolean _hatKindTemp = false;       // Hilfsvariable, wird zuerst ermittelt bevor _hatKind gesetzt wird
		short   _potenzielleOidMission = 0; // Speichervariable, um zu einem bestimmten Zeitpunkt Mission zu setzen
		boolean _potenziellesFraumuensterFlag = false; // Hilfsvariable, Zwischenspeicher beim Einlesen der Personendaten
		boolean _replaceEdvkgWithLastPersonKg = false; // Hilfsvariable, steuert ob die statische EDV-KG (1001) durch die letzte KG aus dem PersonHistory ersetzt wird
		byte    _oidZivilstand; // 1-10
		boolean _fraumuensterFlag = false; // wird erst beim Write Back mit SQL ber�cksichtigt
		
		// Die folgenden boolean-Felder speichern, ob das zugeh�rige Feld ver�ndert wurde im Vergleich zur DB
		boolean _oidMissionChanged = false;
		boolean _oidKGChanged = false;
		boolean _passivChanged = false;
		boolean _hatKindChanged = false;
		boolean _stimmrechtChanged = false;
		boolean _kriegtZeitungChanged = false;
		boolean _oidFamilieChanged = false;
		boolean _fraumuensterFlagChanged = false;
		boolean _oidVorstandChanged = false;
		boolean _autoZeitung = false;
		boolean _famInfoChanged = false;
		boolean _partnerBezChanged = false;
		boolean _oidNfbPartnerChanged = false;
		boolean _oidHaushaltChanged = false;
		
		long _oidPerson = 0;
		long _oidKG = 0;
		long _oidOrigKG = 0;
		
		long _oidVorstand = 0;
		/**
		 * Importierter Partner von der OIZ
		 */
		long _oidOrigPartner = 0;
		/**
		 * Partner der neuen Familienbildung
		 */
		long _oidNfbPartner = 0;

		long _oidVater = 0;
		long _oidMutter = 0;

		/**
		 * @see PostImport#createNFBFamilien(Map, Map)
		 */
		long _oidFamilie = 0;
		long _oidHaushalt = 0;
		
		char _geschlecht;
		char _partnerBez;
		short _famInfo;
		/**
		 * besteht im Moment noch aus SSSSNNNNZZ, wobei S Strassen-ID, N Hausnr und Z Hausnummerzusatz
		 */
		long _egid;
		/**
		 * besteht im Moment noch aus Adresszusatzindex (3 Stellen)
		 */
		short _ewid = 0;
		
		long _oidMission = 0;
		
		long _oidKGZeitung = 0; // Die Kirchgemeinde f�r die Zeitung

		byte _konfession = 0;
		
		//long __bd = 0; // Geburtsdatum in Millisekunden
		java.util.Date _geburtsdatum = null;

		public boolean istVolljaehrig() {
			return !_geburtsdatum.after(alter18);
		}

		public boolean istKleinkind() {
			return _geburtsdatum.after(alter2);
		}

		@Override
		public boolean istAktiv() {
			return !_istPassiv;
		}
		
		/**
		 * @returns true, if this person is either aktiv or rpg aktiv
		 */
		@Override
		public boolean istAktivAny() {
			return istAktiv() || !_istRPGPassiv;
		}

		public boolean istVerheiratetOderPartnerschaft() {
			return _oidZivilstand == _oidCodeZivilstandVerheiratet || _oidZivilstand == _oidCodeZivilstandEingetrPartner;
		}

		public boolean istLedig() {
			return _oidZivilstand == _oidCodeZivilstandLedig;
		}

		public boolean hatGleicheMeldeAdresseUndAZI(Person persOIDs) {
			return _egid == persOIDs._egid && _ewid == persOIDs._ewid;
		}

		public boolean hatGleicheMeldeAdresse(Person persOIDs) {
			return _egid == persOIDs._egid;
		}

		public boolean hatRelevanteKonfession() {
			// Das Testen der Konf. darf nicht auf OIDKG basieren, da
			// OIDKG wieder gesetzt wird, sobald die ganze Fam. passiv ist
			return (_konfession == KATHOLISCH || _konfession == REFORMIERT);
		}

		@Override
		public Person getMutter () {
			return _per.get(_oidMutter);
		}
		
		@Override
		public Person getVater () {
			return _per.get(_oidVater);
		}

		public Person getNfbPartner () {
			return _per.get(_oidNfbPartner);
		}

		/**
		 * Private weil diese Funktion nur am Anfang der Familienbildung gebraucht wird
		 * @return
		 */
		private Person getOrigPartner () {
			return _per.get(_oidOrigPartner);
		}

		@Override
		public Person getVorstand () {
			return _per.get(_oidFamilie);
		}
		
		public Person getOIZVorstand () {
			return _per.get(_oidVorstand);
		}
		
		int getKonfession() {
			return _konfession;
		}
		
		void setKonfession(int konfession) {
			_konfession = (byte) konfession;
		}
		
		void setOIDKG (long oid) {
			if (oid == _oidKG) return;
			_oidKGChanged = true;
			_oidKG = oid;
		}
		
		/**
		 * Brauchts nur solange, wie die EWID noch nicht importiert wird
		 * @param adresszusatzindex
		 */
		void setAdresszusatzindex (short adresszusatzindex) {
			if (adresszusatzindex == _ewid) return;
			_ewidChanged = true;
			_ewid = adresszusatzindex;
		}

		void setPassiv (boolean passiv) {
			if (passiv == _istPassiv) return;
			_passivChanged = true;
			_istPassiv = passiv;
		}

		void setOIDNfbPartner (long oidPartner) {
			if (oidPartner == _oidNfbPartner) return;
			_oidNfbPartnerChanged = true;
			_oidNfbPartner = oidPartner;
		}

		void setHatKind (boolean hatKind) {
			if (hatKind == _hatKind) return;
			_hatKindChanged = true;
			_hatKind = hatKind;
		}

		void setAutoZeitung(boolean flag) {
			if (_autoZeitung == flag) return;
			_autoZeitungChanged = true;
			_autoZeitung = flag;
		}
		
		boolean getAutoZeitung() {
			return _autoZeitung;
		}

		void setKriegtZeitung (boolean kriegtZeitung) {
			if (kriegtZeitung == _kriegtZeitung) return;
			_kriegtZeitungChanged = true;
			_kriegtZeitung = kriegtZeitung;
		}
		
		void setFraumuensterFlag (boolean fraumuensterFlag) {
			if (fraumuensterFlag == _fraumuensterFlag) return;
			_fraumuensterFlagChanged = true;
			_fraumuensterFlag = fraumuensterFlag;
		}
		
		void setPartnerBeziehung(char bez) {
			if (bez == _partnerBez) return;
			_partnerBezChanged = true;
			_partnerBez = bez;
		}
		
		void setFamilienInfo(short bitfield) {
			if (bitfield == _famInfo) return;
			_famInfoChanged = true;
			_famInfo = bitfield;
		}
		
		void setOIDFamilie (long oidFamilie) {
			if (oidFamilie == _oidFamilie) return;
			_oidFamilieChanged = true;
			//Collections.addAll(_modifiedFamilienForKGs, _oidFamilie, oidFamilie);
			_oidFamilie = oidFamilie;
		}
		
		void setImportedOIDMission (long oid) {
			if (oid == _oidMission) return;
			_oidMissionChanged = true;
			_oidMission = oid;
		}
		
		/**
		 * 
		 * @param mv
		 * @param args Falls Eintrag vom Typ {@link Long}, dann wird das Argument 
		 * als Personen-Zip interpretiert u.U. eine Anfrage erstellt
		 */
		void addMeldung(Meldungsvorlage mv, Object... args) {
			PostImport.addMeldung(_oidPerson, mv, _meldungen, args);
		}
		
		/** Erh�lt mindestens ein Mitglied der Familie, das dieselbe oidKG hat, schon eine Zeitung ?
		 * 
		 * @return
		 */ 
		boolean kriegtFamilieSchonZeitung () {
			Object tableItem = _alleFamilien.get(new Long(_oidFamilie));
			if (tableItem != null) {
				// Gruppen-Vektor bestimmen
				if (tableItem instanceof Person) { //tableItem is a oidperson
					Person p = (Person)tableItem;
					if ((p._kriegtZeitungTemp) && p._oidKG == _oidKG) return true;
				} else { // tableItem is a vector with oidperson values
					Vector mitglieder = (Vector)tableItem;
					for (int i = 0; i < mitglieder.size(); i++) {
						Person p = (Person)mitglieder.elementAt(i);
						if ((p._kriegtZeitungTemp) && p._oidKG == _oidKG) return true;
					}
				}
				// Gruppen-Vektor bestimmen END
			} else if (_oidFamilie != 0) {
				_logInkonsistenzen.writeText("Inkonsistenz: Die Familie mit OID = " + _oidFamilie + " wurde nicht gefunden.");
			}
			return false;
		}

		@Override
		public int compareTo(Object p) {
			if (p == null || !(p instanceof Person) || _oidPerson < ((Person)p)._oidPerson) return -1;
			if (_oidPerson > ((Person)p)._oidPerson) return 1;
			return 0;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = (int) (prime * result + _oidPerson);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof Person))
				return false;
			Person other = (Person) obj;
			if (_oidPerson != other._oidPerson)
				return false;
			return true;
		}

//		@Override
//		public List<IPerson> getFamilie() {
//			Object tableItem = _alleFamilien.get(_oidFamilie);
//			if (tableItem == null || tableItem instanceof Person) { //tableItem is a oidperson
//				List<IPerson> list = new ArrayList(1); 
//				list.add(this);
//				return list;
//			} else { // tableItem is a vector with oidperson values
//				Vector mitglieder = (Vector)tableItem;
//				return mitglieder;
//			}
//		}
		
		@Override
		public boolean hatFamilienMitglied(IPerson fmg) {
			Object tableItem = _alleFamilien.get(_oidFamilie);
			if (tableItem == null) { //tableItem is a oidperson
				return false;
			} else if (tableItem instanceof Person) {
				return tableItem.equals(fmg);
			} else { // tableItem is a vector with oidperson values
				Vector mitglieder = (Vector)tableItem;
				return mitglieder.contains(fmg);
			}
		}
		

		@Override
		public char getGeschlecht() {
			return _geschlecht;
		}

		@Override
		public long getOID() {
			return _oidPerson;
		}

		@Override
		public IPerson getPartner() {
			return getNfbPartner();
		}

		@Override
		public char getPartnerBeziehung() {
			return _partnerBez;
		}
		
		public boolean isChildOfDeep(Iterable<Person> parents, Map<Long, Person> persons) {
			for (Person p : parents) {
				if (isChildOfDeep(p.getOID(), persons)) return true;
			}
			return false;
		}
		
		public boolean isChildOfDeep(long oidParent, Map<Long, Person> persons) {
			if (oidParent <= 0) return false;
			
			for (Person p : getParents(persons)) {
				if (isChildOf(oidParent, p, persons, 100)) return true;
			}
			return false;
		}
		
		private boolean isChildOf(long oidParent, Person nextParent, Map<Long, Person> persons, int depth) {
			if (--depth <= 0) return false;
			
			if (oidParent == nextParent.getOID()) return true;
			
			for (Person p : nextParent.getParents(persons)) {
				if (isChildOf(oidParent, p, persons, depth)) return true;
			}
			return false;
		}
		
		public Collection<Person> getParents(Map<Long, Person> persons) {
			Collection<Person> parents = new ArrayList<Person>(2);
			if (_oidMutter > 0) {
				Person p = persons.get(_oidMutter);
				if (p != null) parents.add(p);
			}
			if (_oidVater > 0) {
				Person p = persons.get(_oidVater);
				if (p != null) parents.add(p);
			}
			return parents;
		}
		
		@Override
		public String toString() {
			// hack because debug freezes on this toString (because it's somewhat slow) for huge collections or maps
			if (VM_DEBUG && (int) (Math.random() * 50) == 0) throw new RuntimeException("debug-hack"); 
			
			String s = personInfo(getOID());
			if (_oidMutter > 0) {
				s += " Mutter: " + personInfo(_oidMutter);
			}
			if (_oidVater > 0) {
				s += " Vater: " + personInfo(_oidVater);
			}
			if (_oidOrigPartner > 0) {
				s += " Partner: " + personInfo(_oidOrigPartner);
			}
			return s;
		}
		
		private final String personInfo(long oid) {
			if (oid <= 0) return null;
			
			Person p = _per.get(oid);
			BOPersonHistory bo = BOMQuery.of(BOPersonHistory.CLASS_NAME).match("OIDPerson == " + oid).order("isLast, OID").first();
			if (bo == null) return "" + oid; 
			
			BOContext context = BOContext.getCurrentOrDummy();
			try {
				String firstname = bo.getAttribute("Vorname", context);
				if (firstname == null) firstname = bo.getAttribute("Rufname", context);
				
				String st = "";
				if (p == null) {
					st += "[BO] " + (bo.isActiveSP(context) ? "A" : "P");
					BOPerson boPerson = BOMQuery.get(BOPerson.CLASS_NAME, oid, null);
					if (boPerson.isActiveRPG(context)) {
						st += " rpg: A";
					}
				} else {
					st += (p.istAktiv() ? "A" : "P");
					if (!p._istRPGPassiv) {
						st += " rpg: A";
					}
				}
				return firstname + " " + bo.getAttribute("Name", context) + " (" + st + " " + oid + ")";
			} catch (XMetaModelQueryException e) {
				throw new RuntimeException(e);
			} catch (XSecurityException e) {
				throw new RuntimeException(e);
			}
		}

	};
	
	/**
	 * @param oid Ursprung
	 * @param mv
	 * @param args Falls Eintrag vom Typ {@link Long}, dann wird das Argument 
	 * als Personen-Zip interpretiert u.U. eine Anfrage erstellt
	 */
	public static void addMeldung(long oid, Meldungsvorlage mv, Map<Long, List<Meldung>> meldungen, Object... args) {
		List<Meldung> mlds = meldungen.get(oid);
		if (mlds == null) {
			mlds = new ArrayList<Meldung>(1);
			meldungen.put(oid, mlds);
		}
		mlds.add(new Meldung(mv, args));
	}
	
}

