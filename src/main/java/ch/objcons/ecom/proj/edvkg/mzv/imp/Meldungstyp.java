/**
 * 
 */
package ch.objcons.ecom.proj.edvkg.mzv.imp;

public enum Meldungstyp {
	Fehler("F"), Warnung("W"), Info("I");

	private String _typeIdent;
	
	private Meldungstyp(String typeIdent) {
		_typeIdent = typeIdent;
	}
	
	public String getTypeIdentifier() {
		return _typeIdent;
	}
}