package ch.objcons.ecom.proj.edvkg.mzv;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.ecom.api.EContent;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.adapters.EActionAdapter2;
import ch.objcons.ecom.bom.BOFileUpload;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.XMetaException;

/*
 * makes the export for the Zeitung
 *
 */
public class ActionExportCSV extends EActionAdapter2 {

	private long _aid;
	private long _fileOID;
	// private IFileStorageEntry _file;
	private FileInputStream outFile;
	boolean _iskath = false;
	private Hashtable _duplicateFinder = new Hashtable();
	private int _numOfDuplicates = 0;
	private char _sep = ';';
	private String _line = null;
	private String _kg = "";
	private static final boolean _showPostsperre = true;
	
	public ActionExportCSV(ch.objcons.ecom.proj.edvkg.mzv.BoxMZV box, IRequest request,
		String slotID, String nextPage, boolean kath) {
		super (box, request, slotID, nextPage);
	    _iskath = kath;
	    setKG(request.getParameters().getParameter("client_request_kg"));
	}
	private void add (String val) {
		if (_line == null) _line = "";
		else _line = _line + _sep;
		if (val == null) return;
		_line = _line + quote(val);
	}
	private void fillKath(OutputStream outStream)
	{
		Connection conn = null ;
		ResultSet queryResult = null;
		// my Strings
		String query;
		StringBuffer oneLine;
		String zip, geschlecht, anrede, titel, kgnr, vorname, name, strasse, hausnr;
		String plz, ort, postzustell, adrZusatz, postfach, anz, codeZeitung, postsperre;
		String plzPostfach;
		Object myObj;

		try {
			conn = DBConnectionPool.instance().getConnection();

			// mysql query :
			query = " select "+
					"pers.oid, pers.bx_CodeGeschlecht, pers.bx_ZeitungAnrede, pers.bx_zusAnrede, titel.bx_name, zei.bx_OIDKirchgemeinde, "+
					"pers.bx_ZeitungVorname, pers.bx_ZusVorname, pers.bx_rufname, "+
					"pers.bx_ZeitungNachname, pers.bx_ZusName, pers.bx_name, "+
					"pers.bx_ZusStrassenName, zusstr.bx_name, pers.bx_Strassekg, str.bx_name, "+
					"pers.bx_ZusHausNr, pers.bx_HausNr, "+
					"pers.bx_ZusPLZOrt, pers.bx_PLZOrt, "+
					"pers.bx_ZusOrt,  pers.bx_Ort, "+
					"zushaus.bx_Postzustellbezirk, haus.bx_Postzustellbezirk, "+
					"pers.bx_zusAdrZusatz, bx_AdrZusatz, "+
					"pers.bx_zusPostfach, bx_Postfach, "+
		            "pers.bx_zusPLZPostfach, bx_PLZPostfach, "+
					"zei.bx_Anzahl, pers.bx_adresssperresicherheit, " +
					"pers.bx_Postsperre "+
				"FROM bx_person as pers "+
				"INNER JOIN bx_zeitung as zei on " +
				"zei.bx_PersonListZeitungOID = pers.oid AND "+
				"(coalesce(zei.bx_zeitungsnr, '') != 99999 OR coalesce(pers.bx_zeitungverzicht, 'n') = 'n') " +
				"LEFT JOIN bx_codehaus as haus on "+
					 "pers.bx_OIDCodeStrasse = haus.bx_OIDStrasse AND "+
				  "pers.bx_HausNr = haus.bx_HausNr "+
				"LEFT JOIN bx_codehaus as zushaus on "+
					"pers.bx_ZusOIDStrasse = zushaus.bx_OIDStrasse AND "+
					"pers.bx_ZusHausNr = zushaus.bx_HausNr "+
				"LEFT JOIN bx_codestrasse as str on "+
					"pers.bx_OIDCodeStrasse = str.oid "+
				"LEFT JOIN bx_codestrasse as zusstr on "+
					"pers.bx_ZusOIDStrasse = zusstr.oid "+
		        "LEFT JOIN bx_codetitel as titel ON "+
			        "pers.bx_OIDCodeTitel = titel.OID " +
				"WHERE "+
					"zei.bx_oidkirchgemeinde = " + _kg + " AND "+
		            "pers.bx_statusperson = 'j'";

		    long logID = 0;
			if (ch.objcons.ecom.query.Config.getInstance().showQueryString()) {
				logID = ch.objcons.log.Log.logDesignerInfo("Query: ActionExportCSV.fillKath: " + query);
				ch.objcons.log.Log.flushDesigner();
			}
			Statement querySTM = conn.createStatement();
			queryResult =  querySTM.executeQuery(query);

			if (ch.objcons.ecom.query.Config.getInstance().getSQLTraceLevel() > 0) {
				ch.objcons.log.Log.logDesignerInfoPS(logID, "");
			}
			newLine();

			add("ZIP");
			add("Titel");
			add("Vorname");
			add("Name");
			add("AdrZusatz");
			add("Strasse");
			add("HausNr");
			add("Postfache");
			add("PLZ");
			add("Ort");
			add("PostZustell");
//			add("CodeZeitung1");
//			add("CodeZeitung2");
			add("Anzahl");
//			add("CodeZeitung");
			
			if(_showPostsperre){
				add("Postsperre");
			}
			_line = _line + "\n";
			outStream.write(_line.getBytes());
		
			while (queryResult.next()) {
				myObj = queryResult.getObject(1);
				zip = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(2);
				geschlecht = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(3);
				if (myObj == null) myObj = queryResult.getObject(4);
				if (myObj == null) myObj = queryResult.getObject(5);
				if (myObj == null) {
					if (geschlecht.trim().equals("M")) {
						myObj = "Herr";
					}
					else if (geschlecht.trim().equals("W")) {
						myObj = "Frau";
					}
					else {
					  myObj = "An";
					}
				}
				titel = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(6);
				kgnr = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(7);
				if (myObj == null) myObj = queryResult.getObject(8);
				if (myObj == null) myObj = queryResult.getObject(9);
				vorname = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(10);
				if (myObj == null) myObj = queryResult.getObject(11);
				if (myObj == null) myObj = queryResult.getObject(12);
				name = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(13);
				if (myObj == null) myObj = queryResult.getObject(14);
				if (myObj == null) myObj = queryResult.getObject(15);
				if (myObj == null) myObj = queryResult.getObject(16);
				strasse = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(17);
				if (myObj == null) myObj = queryResult.getObject(18);
				hausnr = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(19);
				if (myObj == null) myObj = queryResult.getObject(20);
				plz = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(21);
				if (myObj == null) myObj = queryResult.getObject(22);
				ort = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(23);
				if (myObj == null) myObj = queryResult.getObject(24);
				postzustell = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(25);
				if (myObj == null) myObj = queryResult.getObject(26);
				adrZusatz = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(27);
				if (myObj == null) myObj = queryResult.getObject(28);
				postfach = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(29);
				if (myObj == null) myObj = queryResult.getObject(30);
				plzPostfach = (myObj == null)?"":myObj.toString();
				myObj = queryResult.getObject(31);
				anz = (myObj == null)?"":myObj.toString();
				if (myObj == null) myObj = queryResult.getObject(30);
				

				myObj = queryResult.getObject(33);
				postsperre = (myObj == null)?"":myObj.toString();
				postsperre = postsperre.equals("j") ? "ja" : "nein";
				
				myObj = queryResult.getObject(32);
				String sperre = (myObj == null)?"":myObj.toString();
				if (sperre.equalsIgnoreCase("j")) {
					System.out.println("zeitungsfile: zip " + zip + "ist gesperrt");
				}

/*
				myObj = queryResult.getObject(32);
				codeZeitung = (myObj == null)?" ":myObj.toString();
*/
				newLine();

				add(zip);
				add(titel);
				add(vorname);
				add(name);
				add(adrZusatz);
				add(strasse);
				add(hausnr);
				add(postfach);
				if ( (postfach.trim().length() > 0) && (plzPostfach.trim().length() > 0) ) {
					plz=plzPostfach;
				}
				add(plz);
				add(ort);
				add(postzustell);
//				add(codeZeitung.substring(1, 3));
//				add(codeZeitung.substring(3));
				add(anz);
//				add(codeZeitung);
				if(_showPostsperre){
					add(postsperre);
				}
				if (_duplicateFinder.get(_line) == null) {
					_duplicateFinder.put(_line, new Long(0));
					_line = _line + "\n";
					outStream.write(_line.getBytes());
				}
				else {
					_numOfDuplicates++;
				}
			}
			outStream.close();

		}
		catch (SQLException e) {
				ch.objcons.log.Log.logSystemAlarm("SQL Exception beim generieren der Zeitung", e);
		}
		catch (IOException e) {
				ch.objcons.log.Log.logSystemAlarm("IO Exception beim generieren der Zeitung", e);
		}
		finally {
			try {
				if (queryResult != null) {
					queryResult.close();
				}
				if (conn != null) {
					DBConnectionPool.instance().ungetConnection(conn);
				}
			}
			catch (Exception e) {
				ch.objcons.log.Log.logSystemAlarm("Unexpected Exception", e);
			}
		}
	}
/**
 * fills the output in the file for the ref
 * Das Resultat ist eine Ausgabe die folgende Info enth�llt :
*	Adresscode    	1
*	KIRCHGEMEINDE	2      0
*	PLZ Haus             4      0
*	PLZ-NAME             25
*	POSTZUSTELLBEZ HAUS  3      0
*	STRASSENNAME         30
*	HAUSNUMMER ZIS       5
*	IDENT-NUMMER Abo Nr. 6      0
*	Anrede               12
*	Titel/Fname/Rufname  30
*	Adresszeile          30
*	Strasse/Hausnummer   27
*	PLZ/Ort              30
*	ANZ. EXEMPLARE       2      0
*
*	F�r die Adresse sind folgende Felder als Resultat der Query gew�nscht :
*	ZIP, Geschlecht, Titel, Kirchgemeinde, Vorname, Name, Strasse, HausNr, PLZOrt, Ort, Postbez
*
 */
private void fillRef(OutputStream outStream) {
	Connection conn = null;
	ResultSet queryResult = null;
	// my Strings
	String query;
	StringBuffer oneLine;
	String zip, geschlecht, anrede, titel, kgnr, vorname, name, strasse, hausnr;
	String plz, ort, postzustell, postfach, plzPostfach, adrZusatz, anz;
	String adrStadt, adrCode, postsperre;
	Object myObj;

	try {
		conn = DBConnectionPool.instance().getConnection();

		final int	ZIP = 1;

		final int	CodeGeschlecht = 2;
		final int	ZeitungAnrede = 3;
		final int	ZusAnrede = 4;

		final int	Titel = 5;
		final int	KG = 6;
		
		final int	ZeitungVorname = 7;
		final int	ZusVorname = 8;
		final int	Vorname = 9;
		
		final int	ZeitungNachname = 10;
		final int	ZusNachname = 11;
		final int	Nachname = 12;

		final int	ZusStrassenName = 13;
		final int	ZusStrassenNameStadt = 14;
		final int	StrasseKG = 15;
		final int	StrasseNameStadt = 16;

		final int	ZusHausNr = 17;
		final int	HausNr = 18;

		final int	ZusPLZOrt = 19;
		final int	PLZOrt = 20;
		final int	ZusOrt = 21;
		final int	Ort = 22;
		
		final int	ZusHausPostZustellBezirk = 23;
		final int	HausPostZustellBezirk = 24;

		final int	ZusAdrZusatz = 25;
		final int	AdrZusatz = 26;
		
		final int	Anzahl = 27;

		final int	AdresseStadt = 28;
		
		final int	ZusPostfach = 29;
		final int	Postfach = 30;

		final int	ZusPLZPostfach = 31;
		final int	PLZPostfach = 32;
		
		final int	Sperre = 33;
		final int   Postsperre = 34;
		
		// the query for the Person that have the Strasse
		query =
			" select "
				+ "pers.oid, "							//  1
				+ "pers.bx_CodeGeschlecht, "			//  2
				+ "pers.bx_ZeitungAnrede, "				//  3
				+ "pers.bx_zusAnrede, "					//  4
				+ "titel.bx_name, "						//  5
				+ "zei.bx_OIDKirchgemeinde, "			//  6
				+ "pers.bx_ZeitungVorname, "			//  7
				+ "pers.bx_ZusVorname, "				//  8
				+ "pers.bx_rufname, "					//  9
				+ "pers.bx_ZeitungNachname, "			// 10
				+ "pers.bx_ZusName, "					// 11
				+ "pers.bx_name, "						// 12
				+ "pers.bx_ZusStrassenName, "			// 13
				+ "zusstr.bx_name, "					// 14
				+ "pers.bx_Strassekg, "					// 15
				+ "str.bx_name, "						// 16
				+ "pers.bx_ZusHausNr, "					// 17
				+ "pers.bx_HausNr, "					// 18
				+ "pers.bx_ZusPLZOrt, "					// 19
				+ "pers.bx_PLZOrt, "					// 20
				+ "pers.bx_ZusOrt, "					// 21
				+ "pers.bx_Ort, "						// 22
				+ "zushaus.bx_Postzustellbezirk, "		// 23
				+ "haus.bx_Postzustellbezirk, "			// 24
				+ "pers.bx_zusAdrZusatz, "				// 25
				+ "bx_AdrZusatz, "						// 26
				+ "zei.bx_Anzahl, "						// 27
				+ "pers.bx_AdresseStadt, "				// 28
				+ "pers.bx_zusPostfach, "				// 29
				+ "pers.bx_Postfach, "					// 30
				+ "pers.bx_zusPLZPostfach, "			// 31
				+ "pers.bx_PLZPostfach, "				// 32
				+ "pers.bx_adresssperresicherheit, "	// 33
				+ "pers.bx_postsperre "					// 34
				+ "FROM bx_person as pers "
				+ "INNER JOIN bx_zeitung as zei on "
				+ "zei.bx_PersonListZeitungOID = pers.oid AND "
				+ "(coalesce(zei.bx_zeitungsnr, '') != 99999 OR coalesce(pers.bx_zeitungverzicht, 'n') = 'n') "
				+ "LEFT JOIN bx_codehaus as haus on "
				+ "pers.bx_OIDCodeStrasse = haus.bx_OIDStrasse AND "
				+ "pers.bx_HausNr = haus.bx_HausNr "
				+ "LEFT JOIN bx_codehaus as zushaus on "
				+ "pers.bx_ZusOIDStrasse = zushaus.bx_OIDStrasse AND "
				+ "pers.bx_ZusHausNr = zushaus.bx_HausNr "
				+ "LEFT JOIN bx_codestrasse as str on "
				+ "pers.bx_OIDCodeStrasse = str.oid "
				+ "LEFT JOIN bx_codestrasse as zusstr on "
				+ "pers.bx_ZusOIDStrasse = zusstr.oid "
				+ "LEFT JOIN bx_codetitel as titel ON "
				+ "pers.bx_OIDCodeTitel = titel.OID "
				+ "WHERE "
				//+ "pers.bx_oidkg = " + _kg + " AND "
				+ "zei.bx_oidkirchgemeinde = " + _kg + " AND "
				+ "pers.bx_statusperson = 'j'";

		long logID = 0;
		if (ch.objcons.ecom.query.Config.getInstance().showQueryString()) {
			logID = ch.objcons.log.Log.logDesignerInfo("Query: ActionExportCSV.fillRef: " + query);
			ch.objcons.log.Log.flushDesigner();
		}
		Statement querySTM = conn.createStatement();
		queryResult = querySTM.executeQuery(query);

		if (ch.objcons.ecom.query.Config.getInstance().getSQLTraceLevel() > 0) {
			ch.objcons.log.Log.logDesignerInfoPS(logID, "");
		}

		newLine();
		add("PLZ");
		add("Ort");
		add("PostZustell");
		add("Strasse");
		add("HausNr");
		add("ZIP");
		add("Anrede");
		add("NamensZeile");
		add("AdrZusatz");
		add("StrassenZeile");		
		add("PLZOrtZeile");
		add("Anzahl");
		if(_showPostsperre){
			add("Postsperre");
		}
		
		_line = _line + "\n";
		outStream.write(_line.getBytes());
		
		while (queryResult.next()) {
			// ZIP
			myObj = queryResult.getObject(ZIP);
			zip = (myObj == null) ? "" : myObj.toString();
			zip = zip.trim();

			// Geschlecht
			myObj = queryResult.getObject(CodeGeschlecht);
			geschlecht = (myObj == null) ? "" : myObj.toString();
			geschlecht = geschlecht.trim();

			// Anrede
			boolean omitTitle = false;
			anrede = "";
			myObj = queryResult.getObject(ZeitungAnrede);
			if ((myObj != null) && (((String)myObj).trim().length() > 0)) {
				anrede = (myObj == null) ? "" : myObj.toString();
				omitTitle = true;
			}
			else {
				myObj = queryResult.getObject(ZusAnrede);
				if ((myObj != null) && (((String)myObj).trim().length() > 0)) {
					anrede = (myObj == null) ? "" : myObj.toString();
					omitTitle = true;
				}
				else {
					if (geschlecht.trim().equals("M")) {
						anrede = "Herr";
					}
					else if (geschlecht.trim().equals("W")) {
						anrede = "Frau";
					}
					else {
					  anrede = "An";
					}
				}
			}
			anrede = anrede.trim();
			
			// Titel
			myObj = queryResult.getObject(Titel);
			titel = (myObj == null) ? "" : myObj.toString();
			if (omitTitle) titel = "";
			titel = titel.trim();

			// Kirchgemeinde
			myObj = queryResult.getObject(KG);
			kgnr = (myObj == null) ? "" : myObj.toString();
			kgnr = kgnr.trim();
			
			// Vorname
			myObj = queryResult.getObject(ZeitungVorname);
			if (myObj == null) {
				myObj = queryResult.getObject(ZusVorname);
			}
			if (myObj == null) {
				myObj = queryResult.getObject(Vorname);
			}
			vorname = (myObj == null) ? "" : myObj.toString();
			vorname = vorname.trim();
			int ix = vorname.indexOf(' ');
			if (ix != -1) {
				vorname = vorname.substring(0,ix);
			}

			// Nachname
			myObj = queryResult.getObject(ZeitungNachname);
			if (myObj == null) {
				myObj = queryResult.getObject(ZusNachname);
			}
			if (myObj == null) {
				myObj = queryResult.getObject(Nachname);
			}
			name = (myObj == null) ? "" : myObj.toString();
			name = name.trim();
			
			// Strassen-Name
			myObj = queryResult.getObject(ZusStrassenName);
			if (myObj == null) {
				myObj = queryResult.getObject(ZusStrassenNameStadt);
			}
			if (myObj == null) {
				myObj = queryResult.getObject(StrasseKG);
			}
			if (myObj == null) {
				myObj = queryResult.getObject(StrasseNameStadt);
			}
			strasse = (myObj == null) ? "" : myObj.toString();
			strasse = strasse.trim();
			
			// Haus-Nummer
			myObj = queryResult.getObject(ZusHausNr);
			if (myObj == null) {
				myObj = queryResult.getObject(HausNr);
			}
			hausnr = (myObj == null) ? "" : myObj.toString();
			hausnr = hausnr.trim();
			
			// PLZ
			myObj = queryResult.getObject(ZusPLZOrt);
			if (myObj == null) {
				myObj = queryResult.getObject(PLZOrt);
			}
			plz = (myObj == null) ? "" : myObj.toString();
			plz = plz.trim();

			// Ort
			myObj = queryResult.getObject(ZusOrt);
			if (myObj == null) {
				myObj = queryResult.getObject(Ort);
			}
			ort = (myObj == null) ? "" : myObj.toString();
			ort = ort.trim();

			// Postzustell
			myObj = queryResult.getObject(ZusHausPostZustellBezirk);
			if (myObj == null) {
				myObj = queryResult.getObject(HausPostZustellBezirk);
			}
			postzustell = (myObj == null) ? "" : myObj.toString();
			postzustell = postzustell.trim();
			
			// AdrZusatz
			myObj = queryResult.getObject(ZusAdrZusatz);
			if (myObj == null) {
				myObj = queryResult.getObject(AdrZusatz);
			}
			adrZusatz = (myObj == null) ? "" : myObj.toString();
			adrZusatz = adrZusatz.trim();

			// Anzahl
			myObj = queryResult.getObject(Anzahl);
			anz = (myObj == null) ? "" : myObj.toString();
			anz = anz.trim();
			
			// Adresse Stadt
			myObj = queryResult.getObject(AdresseStadt);
			adrStadt = (myObj == null) ? "" : myObj.toString();
			adrStadt = adrStadt.trim();
			
			// Postfach
			myObj = queryResult.getObject(ZusPostfach);
			if ((myObj == null) || (((String)myObj).trim().length() == 0)) {
				myObj = queryResult.getObject(Postfach);
			}
			postfach = (myObj == null) ? "" : myObj.toString();
			postfach = postfach.trim();
			
			// PLZPostfach
			myObj = queryResult.getObject(ZusPLZPostfach);
			if (myObj == null) {
				myObj = queryResult.getObject(PLZPostfach);
			}
			plzPostfach = (myObj == null) ? "" : myObj.toString();
			plzPostfach = plzPostfach.trim();
			
			// calculate the Adresscode
			if (postfach.trim().length() != 0) {
				adrCode = "4";
				if (plzPostfach.length() > 0) {
					plz = plzPostfach; // wir m�ssen das auch an die richtige PLZ schicken
				}
			}
			else if ("j".equals(adrStadt)) {
				adrCode = "3";
			}
			else if (plz.trim().length() == 0) {
				adrCode = "1";
			}
			else {
				adrCode = "2";
			}

			myObj = queryResult.getObject(Sperre);
			String sperre = (myObj == null) ? "" : myObj.toString();
			if (sperre.equalsIgnoreCase("j")) {
				System.out.println("zeitungsfile: zip " + zip + "ist gesperrt");
			}
			if(_showPostsperre){
				myObj = queryResult.getObject(Postsperre);
				postsperre = (myObj == null) ? "" : myObj.toString();
				postsperre = postsperre.equals("j") ? "ja" : "nein";
			}
			
			newLine();
			
			add(plz);
			add(ort);
			add(postzustell);
			add(strasse);
			add(hausnr);
			add(zip);
			add(anrede);

			if (titel.length() == 0) {
				add(vorname + " " + name);
			}
			else {
				add(titel + " " + vorname + " " + name);
			}
			
			if (postfach.length() == 0) {
				add(adrZusatz);
			}
			else {
				add("");
			}
			
			if (postfach.length() == 0) {
				add(strasse + " " + hausnr);
			}
			else {
				add("Postfach " + postfach);
			}
			
			add(plz + " " + ort);
			add(anz);
			add(postsperre);
			if (_duplicateFinder.get(_line) == null) {
				_duplicateFinder.put(_line, new Long(0));
				_line = _line + "\n";
				outStream.write(_line.getBytes());
			}
			else {
				_numOfDuplicates++;
			}
		}
		outStream.close();
	}
	catch (SQLException e) {
		ch.objcons.log.Log.logSystemAlarm("SQL Exception beim generieren der Zeitung", e);
	}
	catch (IOException e) {
		ch.objcons.log.Log.logSystemAlarm("IO Exception beim generieren der Zeitung", e);
	}
	finally {
		try {
			if (queryResult != null) {
				queryResult.close();
			}
			if (conn != null) {
				DBConnectionPool.instance().ungetConnection(conn);
			}
		}
		catch (Exception e) {
			ch.objcons.log.Log.logSystemAlarm("Unexpected Exception", e);
		}
	}

}
	private void newLine () {
		_line = null;
	}
	public void performX () throws XMetaException, XDataTypeException {
		EContent result;
		File outFile = null;
		FileInputStream inStream;
		OutputStream outStream;

		try {
			if (_iskath) {
				// we create the stream
				outFile = new File(BOFileUpload.Config.getInstance().getTempBaseDir(), "zeikath.csv");
				if (outFile.exists()) {
					outFile.delete();
				}
				outFile.createNewFile();
				outStream = new BufferedOutputStream(new FileOutputStream(outFile));

				this.fillKath(outStream);
			}
			else {
				// we create the stream
				outFile = new File(BOFileUpload.Config.getInstance().getTempBaseDir(), "zeiref.csv");
				if (outFile.exists()) {
					outFile.delete();
				}
				outFile.createNewFile();
				outStream = new BufferedOutputStream(new FileOutputStream(outFile));

				this.fillRef(outStream);
			}

			// open the stream for reading
	    	inStream = new FileInputStream(outFile);

			// content type zur�ckgeben

			if (_iskath) {
				result = new EContent("kathZeitungen.txt", "txt" , inStream );
			}
			else {
				result = new EContent("refZeitungen.txt", "txt" , inStream );
			}

			this.setContent(result);
		}
		catch (FileNotFoundException e) {
			    ch.objcons.log.Log.logSystemAlarm("File not Found Exception beim generieren der Zeitung", e);
		}
		catch (IOException e) {
			    ch.objcons.log.Log.logSystemAlarm("IO Exception beim generieren der Zeitung", e);
		}
	}
	private String quote (String val) {
		int six = val.indexOf('"');
		if (six == -1) {
			return "\"" + val + "\"";
		}
		String result = "\"";
		int eix = 0;
		while (six != -1) {
			result += val.substring(eix, six) + "\"";
			eix = six;
			six = val.indexOf('"', eix + 1);
		}
		result += val.substring(eix, val.length()) + "\"";
		return result;
	}
	public void setKG (String kg) {
		_kg = kg;
	}
}
