package ch.objcons.ecom.proj.edvkg;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.BOValidationException;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.ReferenceAttribute;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.meta.XMetaModelNotFoundException;
import ch.objcons.ecom.meta.XMetaModelQueryException;
import ch.objcons.ecom.query.StringQuery;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

/*
 * just setz the history of the Person
 */
public class BOSABAWoche extends BOObject {
	StringQuery _kalDatQuery = null;
	MetaObject _mobjzu = null;

	private static final ILogger LOGGER = Logger.getLogger(BOSABAWoche.class);
	
	public BOSABAWoche(BLTransaction trans, long oid) {
		super(trans, oid);
		try {
			MetaObject kalm = MetaObjectLookup.getInstance().getMetaObject("KalenderDatum");
			_kalDatQuery = new StringQuery(_trans, 0);
			_kalDatQuery.init("KalenderDatumQuery", "KalenderDatumQuery", kalm);
			_mobjzu = MetaObjectLookup.getInstance().getMetaObject("SABAZuordnung");
		}
		catch (XMetaModelNotFoundException e) {
			ch.objcons.log.Log.logSystemAlarm("Unable to initialize the BOSABAWoche", e);
		}
	}

	/** returns the value, also form the not avaliable elements */
	/* public Object getValue(int id, boolean useAccessControl, BOContext context) throws XMetaModelNotFoundException, XSecurityException {
		return super.getValue(id, useAccessControl, context);
	} */
	public BOObject getReferencedObject(int index, boolean useAccessControl, BOContext context)
			throws XMetaModelQueryException, XSecurityException {
		// String attributeName = this.getMetaObject().getAllAttributeForIndex(index).getExternName();
		BOObject refObj = super.getReferencedObject(index, false, context);
		ReferenceAttribute attr = (ReferenceAttribute)(this.getMetaObject().getAllAttributeForIndex(index));
		String className = attr.getReferencedObject().getName();

		try {
			if ("SABAZuordnung".equals(className)) {
//				this.setCalenderDatum(refObj, null, context, null);
			}
		}
		catch (Exception e) {
			//XXX Better Error
			ch.objcons.log.Log.logSystemAlarm("Unexpected Exception setting the KalenderDatum", e);
		}
		return refObj;
	}

	public void validate(BOContext context) throws BOValidationException, XMetaModelQueryException, XSecurityException {
		super.validate(context);
		if (!isDeleted()) {
			try {
				this.updateObject(context);
			}
			catch (Exception e) {
				e.printStackTrace(); // XXX make that better
			}
		}
	}

	/**
	 * checks ob der Halbtag korrekt gesetzt ist. Wenn nicht wird '1' zur�ckgegeben
	 */
/*	private  int checkHalvDay(String sabaZuordnung, BOContext context) throws XSecurityException, XMetaModelQueryException{
		
		//		System.out.println("OID = " + getOID());
		BOObject refObj = this.getReferencedObject(sabaZuordnung, context);
		if (refObj.getAttribute("OIDKalenderDatum", context) != null) {
			// there is the Day off !!!
			return 0;
		}
		String sabaPersOID = refObj.getAttribute("OIDSabaPers", context);
		if (sabaPersOID == null) return 1;
		if (sabaPersOID.equals("0")) return 1;
		BOObject sabaPers = refObj.getReferencedObject("OIDSabaPers", context);
		if (sabaPers != null) {
			String gesperrt = sabaPers.getAttribute("gesperrt", context);
			
			if ((gesperrt != null) && (gesperrt.equals("j"))) return 1;
			else return 0;
		}
		return 1;
	}
*/

	public static void main (String[] inp) {
		GregorianCalendar cal = new GregorianCalendar();
		// just for test
		System.out.println("Time start to set is "+cal.getTime().toString());

		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY); // we start with Monday and Java starts with Sunday
		cal.set(Calendar.WEEK_OF_YEAR, 48);
		cal.set(Calendar.YEAR, 2002);

		// just for test
		System.out.println("Time start after  set is "+cal.getTime().toString());
	}
	/**
	 * set the Kalender it there is one
	 */
	private void setCalenderDatum(BOObject refObj, GregorianCalendar cal, BOContext context, String vierteltag)
			throws XMetaModelQueryException, XSecurityException, XMetaException{
		String query = null;
		if (refObj != null) {
			if (cal == null) {
				cal = new GregorianCalendar();
				cal.setTime((java.sql.Date)(refObj.getValue("Datum", context)) );
			}
			int day = cal.get(Calendar.DAY_OF_MONTH);
			int month = cal.get(Calendar.MONTH)+1;
			int year = cal.get(Calendar.YEAR);

			if (vierteltag == null) {
				vierteltag = refObj.getAttribute("Vierteltag", context);
			}
			if (vierteltag.equals("1")) {
				query = "select kald.* from bx_kalenderdatum as kald, bx_kalender as kal "+
			" where kal.oid = kald.bx_KalenderOIDListKDatumOID and "+
			" kald.bx_Tag = "+day+" and kald.bx_monat = "+month+" and kald.bx_vormittag = 'j' and "+
			" ( (kal.bx_fix = 'n' and kald.bx_jahr = "+year+" ) OR "+
			"   (kal.bx_fix = 'j') ) ";
			} else if (vierteltag.equals("2")) {
				query = "select kald.* from bx_kalenderdatum as kald, bx_kalender as kal "+
				" where kal.oid = kald.bx_KalenderOIDListKDatumOID and "+
				" kald.bx_Tag = "+day+" and kald.bx_monat = "+month+" and kald.bx_vormittag2 = 'j' and "+
				" ( (kal.bx_fix = 'n' and kald.bx_jahr = "+year+" ) OR "+
				"   (kal.bx_fix = 'j') ) ";
			} else if (vierteltag.equals("3")) {
				query = "select kald.* from bx_kalenderdatum as kald, bx_kalender as kal "+
			" where kal.oid = kald.bx_KalenderOIDListKDatumOID and "+
			" kald.bx_Tag = "+day+" and kald.bx_monat = "+month+" and kald.bx_nachmittag = 'j' and "+
			" ( (kal.bx_fix = 'n' and kald.bx_jahr = "+year+" ) OR "+
			"   (kal.bx_fix = 'j') ) ";
				
			} else if (vierteltag.equals("4")) {
				query = "select kald.* from bx_kalenderdatum as kald, bx_kalender as kal "+
			" where kal.oid = kald.bx_KalenderOIDListKDatumOID and "+
			" kald.bx_Tag = "+day+" and kald.bx_monat = "+month+" and kald.bx_nachmittag2 = 'j' and "+
			" ( (kal.bx_fix = 'n' and kald.bx_jahr = "+year+" ) OR "+
			"   (kal.bx_fix = 'j') ) ";
			}
			_kalDatQuery.setSqlStatement(query);
			Vector toDoList = _kalDatQuery.getResult(_trans, context);
			for (int count = 0; count < toDoList.size(); count++) {
				refObj.setReferencedObject("OIDKalenderDatum", (BOObject)toDoList.elementAt(count));
			}
		}
	}
	
	public static void setAllZuordParam(BOContext context, BOObject woche)
			throws XMetaModelNotFoundException, XSecurityException, XMetaModelQueryException, XMetaException {
		((BOSABAWoche)woche).setAllZuordParam(context);
	}

	/**
	 * set all Parameter in the Zuordnungen
	 */
	void setAllZuordParam(BOContext context)
			throws XMetaModelNotFoundException, XSecurityException, XMetaModelQueryException, XMetaException {
		GregorianCalendar cal = new GregorianCalendar();
		int jahr = Integer.parseInt(getAttribute("Jahr", false, context));
		int woche = Integer.parseInt(getAttribute("Woche", false, context));
		int actyear = cal.get(Calendar.YEAR);
		int actweek = cal.get(Calendar.DAY_OF_WEEK);

		// DON'T CHANGE the ORDER OF THE NEXT 3 STATEMENTS
		// The calendar will not wird of you do this !!!!!!!!!!
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		cal.set(Calendar.WEEK_OF_YEAR, woche);
		cal.set(Calendar.YEAR, jahr);
		
		
		BOObject kg =  this.getReferencedObject("OIDKG", context);
		
		
		Calendar cal2 = new GregorianCalendar();
		cal2.setTime(cal.getTime());
		cal2.add(Calendar.DATE, 4);
		java.sql.Date dateStart = new java.sql.Date(cal.getTime().getTime());
		java.sql.Date dateEnd = new java.sql.Date(cal2.getTime().getTime());
		
		if (willCauseConflict(kg.getOID(),dateStart, dateEnd)) {
			LOGGER.fatal("Es existiert bereits Zuordnungseintr�ge f�r diese Woche! ( Zuordning: " + this.getOID() + " KG: " + kg.getOID() + " Datumsbereich: " + dateStart + " - " + dateEnd);
		} else {
			// and now we set it
			this.setMissingParam("OIDMoVor" ,cal, "Mo", "1", kg, context, _mobjzu);
			this.setMissingParam("OIDMoVor2" ,cal, "Mo", "2", kg, context, _mobjzu);
			this.setMissingParam("OIDMoNach",cal, "Mo", "3", kg, context, _mobjzu);
			this.setMissingParam("OIDMoNach2",cal, "Mo", "4", kg, context, _mobjzu);
			
			cal.add(Calendar.DATE, 1);
			this.setMissingParam("OIDDiVor",cal, "Di", "1", kg, context, _mobjzu);
			this.setMissingParam("OIDDiVor2",cal, "Di", "2", kg, context, _mobjzu);
			this.setMissingParam("OIDDiNach",cal, "Di", "3", kg, context, _mobjzu);
			this.setMissingParam("OIDDiNach2",cal, "Di", "4", kg, context, _mobjzu);
			
			cal.add(Calendar.DATE, 1);
			this.setMissingParam("OIDMiVor",cal, "Mi", "1", kg, context, _mobjzu);
			this.setMissingParam("OIDMiVor2",cal, "Mi", "2", kg, context, _mobjzu);
			this.setMissingParam("OIDMiNach",cal, "Mi", "3", kg, context, _mobjzu);
			this.setMissingParam("OIDMiNach2",cal, "Mi", "4", kg, context, _mobjzu);
			
			cal.add(Calendar.DATE, 1);
			this.setMissingParam("OIDDoVor",cal, "Do", "1", kg, context, _mobjzu);
			this.setMissingParam("OIDDoVor2",cal, "Do", "2", kg, context, _mobjzu);
			this.setMissingParam("OIDDoNach",cal, "Do", "3", kg, context, _mobjzu);
			this.setMissingParam("OIDDoNach2",cal, "Do", "4", kg, context, _mobjzu);
			
			cal.add(Calendar.DATE, 1);
			this.setMissingParam("OIDFrVor",cal, "Fr", "1", kg, context, _mobjzu);
			this.setMissingParam("OIDFrVor2",cal, "Fr", "2", kg, context, _mobjzu);
			this.setMissingParam("OIDFrNach",cal, "Fr", "3", kg, context, _mobjzu);
			this.setMissingParam("OIDFrNach2",cal, "Fr", "4", kg, context, _mobjzu);
		}
		
		
	}

	private boolean willCauseConflict(long kgOID, java.sql.Date dateStart, java.sql.Date dateEnd) {
		Connection conn = null;
		try {
			conn = DBConnectionPool.instance().getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select * from bx_sabazuordnung where bx_OIDKG = " + kgOID + " && bx_datum >= '" + dateStart + "' && bx_datum <= '" + dateEnd + "'");
			return rs.first();
			
		} catch (SQLException e) {
			LOGGER.error("SQLException occured!", e);
		} finally {
			if (conn != null) {
				DBConnectionPool.instance().ungetConnection(conn);
			}
		}
		return false;
	}
	
	/**
	 * set the missing Parameters in the SABA Zuordnung
	*/
	private void setMissingParam(String sabaZuordnung, GregorianCalendar cal, String tagBez, String vierteltag, BOObject kg, BOContext context, MetaObject mobj)
			throws XMetaModelNotFoundException, XSecurityException, XMetaModelQueryException, XMetaException {
		BOObject refObj = super.getReferencedObject(getIndex(sabaZuordnung), true, context);
		java.sql.Date date = new java.sql.Date(cal.getTime().getTime());
		if (refObj == null) {
			refObj = _trans.getObject(mobj, 0, false);
			this.setReferencedObject(sabaZuordnung, refObj);
		}
		refObj.setValue("Datum", date, context);
		refObj.setValue("TagBez", tagBez, context);
		refObj.setValue("Vierteltag", vierteltag, context);
		if (vierteltag.equalsIgnoreCase("1") || vierteltag.equalsIgnoreCase("2")) {
			refObj.setValue("Halbtag", "V", context);
		} else { 
			refObj.setValue("Halbtag", "N", context);
		}
		//System.out.println("*** Zuordnung "+refObj.getOID()+" --> KG = "+kg);
		refObj.setReferencedObject("OIDKG",kg);

		// set the CalenderDatum if there is one
		this.setCalenderDatum(refObj, cal, context, vierteltag);
	}

	/**
	 * updateas all internal stuff of the Object
	 */
	public void updateObject (BOContext context) throws XMetaException, XMetaModelQueryException, XSecurityException, SQLException {

		//set all missing params in Zuordnungen
		try {
			
			if (this.getValue("komplett", context) == null) {
				// do it only the first time
				this.setValue("komplett",new Boolean(false),context);
				this.setAllZuordParam(context);
			}
		}
		catch ( XMetaException e) {
			e.printStackTrace(); // XXX
		}

		// and now we check if week is finished
		/*try {
//			EBoxWithMetaDataAdapter constsBox = (EBoxWithMetaDataAdapter)ch.objcons.ecom.engine.EEngine.getInstance().getBox("global");
			String anzFrei = BoxMZV._SABAAnzahlFreiTage;
			int anzFreitage = Integer.parseInt(anzFrei);

			anzFreitage -= this.checkHalvDay("OIDMoVor", context);
			anzFreitage -= this.checkHalvDay("OIDMoNach", context);
			anzFreitage -= this.checkHalvDay("OIDDiVor", context);
			anzFreitage -= this.checkHalvDay("OIDDiNach", context);
			anzFreitage -= this.checkHalvDay("OIDMiVor", context);
			anzFreitage -= this.checkHalvDay("OIDMiNach", context);
			anzFreitage -= this.checkHalvDay("OIDDoVor", context);
			anzFreitage -= this.checkHalvDay("OIDDoNach", context);
			anzFreitage -= this.checkHalvDay("OIDFrVor", context);
			anzFreitage -= this.checkHalvDay("OIDFrNach", context);
//			System.out.println("OID = " + getOID() + ", anzFreitage = " + anzFreitage);
			Object origValue = this.getValue("komplett", context);
			if (anzFreitage >= 0) {
				if ((origValue != null) && (origValue instanceof Boolean)) {
					if (origValue.equals(Boolean.TRUE)) return;
				}
				this.setValue("komplett", new Boolean(true), context);
			}
			else {
				if ((origValue != null) && (origValue instanceof Boolean)) {
					if (origValue.equals(Boolean.FALSE)) return;
				}
				this.setValue("komplett", new Boolean(false), context);
			}
		}
		finally {
		}
		// _trans.commit();*/
	}
	
	
}
