package ch.objcons.ecom.proj.edvkg.mzv;

import static ch.objcons.ecom.bom.utils.UpgradeUtil.MANDATORY;
import static ch.objcons.ecom.bom.utils.UpgradeUtil.NOT_INTERNAL;
import static ch.objcons.ecom.bom.utils.UpgradeUtil.NOT_MANDATORY;
import static ch.objcons.ecom.bom.utils.UpgradeUtil.NOT_UNIQUE;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.ecom.api.Boxalino;
import ch.objcons.ecom.api.IBox;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.adapters.EBoxUpgraderAdapter;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.bom.utils.BOMQuery;
import ch.objcons.ecom.bom.utils.BoxalinoUtilities;
import ch.objcons.ecom.bom.utils.UpgradeUtil;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.expr.XExpression;
import ch.objcons.ecom.meta.MetaAttribute;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.MetaService;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.proj.edvkg.mzv.bva.BOBVA;
import ch.objcons.ecom.proj.edvkg.mzv.bva.BOBVAEntry;
import ch.objcons.ecom.proj.edvkg.mzv.imp.*;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchuelerInfo;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchulHaus;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchulJahrBeginn;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchulKlasse;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchulSprache;
import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.bo.BOSchulStufe;
import ch.objcons.ecom.proj.edvkg.systeminfo.BOSystemInfo;
import ch.objcons.ecom.proj.edvkg.systeminfo.SystemInfo;
import ch.objcons.ecom.query.XMaxRowException;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.box.pseudo.BoxPseudo;
import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

public class BoxUpgrader extends EBoxUpgraderAdapter {
		private static ILogger LOGGER = Logger.getLogger("ch.ikg.upgrader");
		
		BoxUpgrader (IBox box) {
	        super(box);
	    }

		public void initialSetup (IRequest request, IBox box) throws Exception {
		}
	    
		public void upgradeToVersion_01_01(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = null;
			MetaObject mobjPerson = null;
			MetaObject mobjPersonHist = null;
			trx = BLTransaction.startTransaction(context);

			mobjPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			uu.epoShortAttribute(trx, context, mobjPerson, "SortHausNr", 0, "Zus�tzliches Feld f�r die Hausnummer zwecks Sortierung", false, false, false);

			mobjPersonHist = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			uu.epoShortAttribute(trx, context, mobjPersonHist, "SortHausNr", 0, "Zus�tzliches Feld f�r die Hausnummer zwecks Sortierung", false, false, false);
			uu.epoStringAttribute(trx, context, mobjPersonHist, "User", 50, "Benutzer, der die Hand�nderungen bewirkt hat", false, false /* mandantory */, false);
			
			box.getMetaService().setVersion("01.01");
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_01_20(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = null;
			MetaObject mobjPerson = null;
			MetaObject mobjPersonHist = null;
			trx = BLTransaction.startTransaction(context);

			mobjPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			uu.epoShortAttribute(trx, context, mobjPerson, "ZusSortHausNr", 0, "Zus�tzliches Feld f�r die Wahl-Hausnummer zwecks Sortierung", false, false, false);
			uu.epoStringAttribute(trx, context, mobjPerson, "Bezirk", 3, "Zustellbezirk f�r Postversand", false, false /* mandantory */, false);

			mobjPersonHist = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			uu.epoShortAttribute(trx, context, mobjPersonHist, "ZusSortHausNr", 0, "Zus�tzliches Feld f�r die Wahl-Hausnummer zwecks Sortierung", false, false, false);
			
			box.getMetaService().setVersion("01.20");
			trx.putObject(box.getMetaService());
			trx.commit();

			// SQL Updates
			/*
			Connection conn = null;
			try {
				conn = DBConnectionPool.instance().getConnection();
			} catch (SQLException e) {
				
			}
			finally {
				if (conn != null) DBConnectionPool.instance().ungetConnection(conn);
			}
			*/
		}

		public void upgradeToVersion_01_21(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = null;
			MetaObject mobjCodeHaus = null;
			MetaObject mobjCodeHausKG = null;
			trx = BLTransaction.startTransaction(context);

			mobjCodeHaus = MetaObjectLookup.getInstance().getMetaObject("CodeHaus");
			uu.epoStringAttribute(trx, context, mobjCodeHaus, "CodeHinweis", 1, "Zus�tzliches Feld zur Hausnummer", false, false, false);

			mobjCodeHausKG = MetaObjectLookup.getInstance().getMetaObject("CodeHausKG");
			uu.epoStringAttribute(trx, context, mobjCodeHausKG, "CodeHinweis", 1, "Zus�tzliches Feld zur Hausnummer", false, false, false);
			
			box.getMetaService().setVersion("01.21");
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_01_30(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = null;
			MetaObject mobjPerson = null;
			MetaObject mobjPersonHistory = null;
			trx = BLTransaction.startTransaction(context);

			mobjPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			uu.epoStringAttribute(trx, context, mobjPerson, "ZusArt", 1, "Art der Zustelladresse ('' == keine, m == manuell, s == stadt, e == extern)", false, false, false);

			mobjPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			uu.epoStringAttribute(trx, context, mobjPersonHistory, "ZusArt", 1, "Art der Zustelladresse ('' == keine, m == manuell, s == stadt, e == extern)", false, false, false);
			
			box.getMetaService().setVersion("01.30");
			trx.putObject(box.getMetaService());
			trx.commit();
		}
	    
		
		public void upgradeToVersion_01_40(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = null;
			MetaObject mobjPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			MetaObject mobjPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			trx = BLTransaction.startTransaction(context);
			
			uu.epoBooleanAttribute(trx, context, mobjPerson, "PersonensperreStadt", "Stadtsperre", false, false, false);
			
			uu.epoBooleanAttribute(trx, context, mobjPerson, "Postsperre", "Postsperre", false, false, false);
			uu.epoBooleanAttribute(trx, context, mobjPersonHistory, "Postsperre", "Postsperre", false, false, false);
			
			box.getMetaService().setVersion("01.40");
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		
		public void upgradeToVersion_01_41(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext)request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			UpgradeUtil uu = UpgradeUtil.getInstance();
			Connection conn = null;
			Statement stmtPostsperre = null;
			try {
				trx.begin();
				conn = trx.getConnection();
				stmtPostsperre = conn.createStatement();
				LOGGER.info("Doing Postsperre Update: postsperre = adresssperre");
				stmtPostsperre.executeUpdate("update bx_person set bx_PostSperre = bx_adresssperresicherheit");
				LOGGER.info("Doing Postsperre Update: stadtsperre = adresssperre");
				stmtPostsperre.executeUpdate("update bx_person set bx_PersonensperreStadt = bx_adresssperresicherheit");
				LOGGER.info("Doing Postsperre Update: zeitungsverzicht = adresssperre");
				stmtPostsperre.executeUpdate("update bx_person set bx_zeitungverzicht = 'j' where bx_adresssperresicherheit = 'j'");
				LOGGER.info("Doing Postsperre Update: adresssperre = 'n'");
				stmtPostsperre.executeUpdate("update bx_person set bx_adresssperresicherheit = 'n'");
				LOGGER.info("Creating index on personhistory's name attr");
				MetaObject mo = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
				uu.epoIndex(trx, context, false, mo, mo.getAllAttributeForName("Name", context).getIndex());
				
				stmtPostsperre.close();
				stmtPostsperre = null;
				
				box.getMetaService().setVersion("01.41");
				trx.putObject(box.getMetaService());
				trx.commit();
			} catch (Exception e) {
				trx.rollback();
				LOGGER.fatal("Updating to Version 01.41 failed.", e);
			} finally {
				if(stmtPostsperre != null) {
					try {
						stmtPostsperre.close();
					} catch (SQLException e) {
						LOGGER.fatal("Closing statement stmtPostsperre failed.", e);
					}
				}
			}
		}
		
		public void upgradeToVersion_01_42(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext)request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			MetaService service = box.getMetaService();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			
			MetaObject moStimmrechtConfigutartion   = uu.epoMetaObject(trx, service, BOStimmrechtConfiguration.CLASS_NAME, BOStimmrechtConfiguration.class.getName(), "Enth�lt Einstellungen zum Stimmrecht", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.SHOW_EXTENT);
			MetaObject moStimmrechtAufenthaltsarten = uu.epoMetaObject(trx, service, BOStimmrechtAufenthaltsarten.CLASS_NAME, BOStimmrechtAufenthaltsarten.class.getName(), "Enth�lt Aufenthaltsarten zum Stimmrecht", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.SHOW_EXTENT);
			
			{
				MetaObject mobj = moStimmrechtConfigutartion;
				uu.epoLongAttribute(trx, context, mobj, "konfession", "Konfession", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
				uu.epoStringAttribute(trx, context, mobj, "propertyType", 255, "Einstellungstyp", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
				uu.epoStringAttribute(trx, context, mobj, "propertyValue", 255, "Einstellungswert", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
				
			}
			
			{
				MetaObject mobj = moStimmrechtAufenthaltsarten;
				uu.epoLongAttribute(trx, context, mobj, "konfession", "Konfession", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
				uu.epoStringAttribute(trx, context, mobj, "aufenthaltsart", 255, "Aufenthaltsart", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			}
			
			
			
			box.getMetaService().setVersion("01.42");
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		
		public void upgradeToVersion_01_43(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext)request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			UpgradeUtil uu = UpgradeUtil.getInstance();
			
			MetaObject moStimmrechtConfigutartion   = uu.getClassForName(BOStimmrechtConfiguration.CLASS_NAME);
			MetaObject moStimmrechtAufenthaltsarten = uu.getClassForName(BOStimmrechtAufenthaltsarten.CLASS_NAME);
			boolean hasEntries = false;
			try {
				List<?> rsa = BoxalinoUtilities.executeQuery(BOStimmrechtAufenthaltsarten.CLASS_NAME, "", trx, context);
				List<?> rsc = BoxalinoUtilities.executeQuery(BOStimmrechtConfiguration.CLASS_NAME, "", trx, context);
				hasEntries = !rsa.isEmpty() || !rsc.isEmpty();
			} catch (XExpression e1) {
				LOGGER.error("XExpression occured!", e1);
			} catch (XDataTypeException e1) {
				LOGGER.error("XDataTypeException occured!", e1);
			} catch (XMaxRowException e1) {
				LOGGER.error("XMaxRowException occured!", e1);
			}
			
			if (!hasEntries) {
				String[] codes = {"CH NL","STADTB","B EG","B FK","B FL","B OV","C AN","C EG","C FL","L EG","L PR2"};
				for (String konfession : new String[]{"1","2"}) {
					for (String code : codes) {
						BOObject bo = trx.createNewObject(moStimmrechtAufenthaltsarten);
						try {
							bo.setAttribute("konfession", konfession, context);
							bo.setAttribute("aufenthaltsart", code, context);
						} catch (XDataTypeException e) {
							LOGGER.error("XDataTypeException occured!", e);
						}

					}
					
					try {
						BOObject  bo = trx.createNewObject(moStimmrechtConfigutartion);
						bo.setAttribute("konfession", konfession, context);
						bo.setAttribute("propertyType","alter");
						bo.setAttribute("propertyValue","18");
					} catch (XDataTypeException e) {
						LOGGER.error("XDataTypeException occured!", e);
					}
				}	
			}
			
			box.getMetaService().setVersion("01.43");
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		
		public void upgradeToVersion_01_44(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext)request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			UpgradeUtil uu = UpgradeUtil.getInstance();
			
			MetaObject mobjPerson = uu.getClassForName("Person");
			
			uu.epoBooleanAttribute(trx, context, mobjPerson, "autoZeitung", "Person hat Anrecht auf eine Automatische Zeitung", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			
			box.getMetaService().setVersion("01.44");
			trx.putObject(box.getMetaService());
			trx.commit();
			
		}
		
		public void upgradeToVersion_01_45(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext)request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			Connection conn = null;
			Statement stmt = null;
			try {
				trx.begin();
				conn = trx.getConnection();
				stmt = conn.createStatement();
				LOGGER.info("adding [IGNORE] MUL-KEY to TABLE bx_orgpersonen");
				//Deletes all duplicates
				stmt.executeUpdate("ALTER IGNORE TABLE bx_orgpersonen ADD UNIQUE (BX_PERSONOIDLISTORGPOID, BX_ORGANISATIONOIDLISTORGPOID);");
				LOGGER.info("deleting entries for table bx_orgpersonen where BX_ORGANISATIONOIDLISTORGPOID is null or BX_PERSONOIDLISTORGPOID is null");
				stmt.executeUpdate("delete from bx_orgpersonen where BX_ORGANISATIONOIDLISTORGPOID is null or BX_PERSONOIDLISTORGPOID is null;");
				
				stmt.close();
				stmt = null;
				
				box.getMetaService().setVersion(getActualVersion());
				trx.putObject(box.getMetaService());
				trx.commit();
			} catch (Exception e) {
				trx.rollback();
				LOGGER.fatal("Updating to Version " + getActualVersion() + " failed.", e);
			} finally {
				if(stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {
						LOGGER.fatal("Closing statement stmt failed.", e);
					}
				}
			}
		}
		
		public void upgradeToVersion_01_50(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext)request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			UpgradeUtil uu = UpgradeUtil.getInstance();

			LOGGER.info("adding Index to TABLE bx_personhistory");
			MetaObject moPersonHistory = uu.getClassForName("PersonHistory");
			Integer[] maList = {moPersonHistory.getAllAttributeForName("RecordStatus", context).getIndex()};
			uu.epoIndex(trx, context, false, moPersonHistory, maList);
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_01_51(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			UpgradeUtil uu = UpgradeUtil.getInstance();
			
			MetaObject moLoginUser = uu.getClassForName("LoginUser");
			uu.epoTimestampAttribute(trx, context, moLoginUser, "modificationTS", "Zeit der letzten effektiven Bearbeitung", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			
			trx.commit();

			
			List<BOObject> loginUserList = BoxalinoUtilities.executeQuery("LoginUser", "", "", trx, request);
			for (BOObject boLoginUser : loginUserList) {
				if (boLoginUser.getValue("modificationTS") == null) {
					boLoginUser.setValue("modificationTS", boLoginUser.getValue("modDate"), context);
				}
			}
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public boolean cancelProcessWhenError(int majorVersion, int minorVersion) {
			return true;
		}

		public void upgradeToVersion_01_52(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			
			for (int menuOID : new int[] {53, 41, 40, 39, 38}) {
				BOObject bo = trx.loadObject("MenuItem", menuOID);
				if (bo != null) bo.setDeleted(true, context);
			}
			
			// Admin allg. Adressen
			trx.loadObject("MenuItem", 26).setAttribute("href", "/member/adr_pfarrmain.html");
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_01_53(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			Connection conn = null;
			Statement stmt = null;
			BLTransaction trx = BLTransaction.startTransaction(context);
			try {
				trx.begin();
				conn = trx.getConnection();
				stmt = conn.createStatement();
				
				String[] services = new String[] { "order", "catalog", "product", "cart" };
				for (String service : services) {
					LOGGER.info("update old metaservices: " + service);
					stmt.executeUpdate("update metaservices set classname='"+ BoxPseudo.class.getCanonicalName() + "', type=3 where name = '" + service + "';");
				}
				
				
				/*
				String[] cols = { "bx_salutation", "bx_title", "bx_suffix", "bx_firstname2", "bx_nickname", "bx_birthday", "bx_spouse", "bx_children", "bx_anniversary1", "bx_anniversary2", "bx_anniversary3", "bx_anniversariescomment", "bx_panel", "bx_employer", "bx_jobtitle", "bx_managername", "bx_assistantName", "bx_officelocation", "bx_assistantmobile", "bx_employeenumber", "bx_sex" };
				for (String col : cols) {
					LOGGER.info("drop col: " + col);
					stmt.executeUpdate("alter table bx_person drop column " + col + ";");
				}
				*/
				
				stmt.close();
				stmt = null;

				box.getMetaService().setVersion(getActualVersion());
				trx.putObject(box.getMetaService());
				trx.commit();
			} catch (Exception e) {
				trx.rollback();
				LOGGER.fatal("Updating to Version " + getActualVersion() + " failed.", e);
			} finally {
				if(stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {
						LOGGER.fatal("Closing statement stmt failed.", e);
					}
				}
			}
			try {
				conn = DBConnectionPool.instance().getConnection();
				stmt = conn.createStatement();
				// Erh�he service type auf 5 damit edvkg box nach der meta Box geladen wird
				LOGGER.info("set ikg box level");
				stmt.executeUpdate("update metaservices set type=5 where name = 'edvkg';");
				conn.commit();
			} catch (Exception e) {
				LOGGER.fatal("Updating to box type 5 failed", e);
			} finally {
				if(stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {
						LOGGER.fatal("Closing statement stmt failed.", e);
					}
				}
			}
			
			throw new Exception("Please restart system once again");
		}

		public void upgradeToVersion_01_54(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);

			String[] services = new String[] { "news", "order", "catalog", "product", "cart" };
			Boxalino boxalino = new Boxalino(request);
			for (String service : services) {
				boxalino.handleParameter("meta", "service_Name", service);
				boxalino.handleParameter("meta", "cmd_deleteBox", "true");
			}
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		
		@SuppressWarnings("serial")
		public void upgradeToVersion_01_55(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			MetaService service = box.getMetaService();
			BLTransaction trx = BLTransaction.startTransaction(context);
			UpgradeUtil uu = UpgradeUtil.getInstance();
			
			MetaObject moSystemInfo = uu.epoMetaObject(trx, service, BOSystemInfo.CLASS_NAME, BOSystemInfo.class.getName(), BOSystemInfo.CLASS_NAME, UpgradeUtil.NOT_INTERNAL, UpgradeUtil.SHOW_EXTENT);
			uu.epoStringAttribute(trx, context, moSystemInfo, "code", 255, "Code", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.MANDATORY, UpgradeUtil.UNIQUE);
			uu.epoStringAttribute(trx, context, moSystemInfo, "mode", 255, "Mode", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoBxActive(trx, context, moSystemInfo);
			uu.epoBxSorter(trx, context, moSystemInfo);
			uu.epoTextAttribute(trx, context, moSystemInfo, "text", 0, "Text", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);

			trx.commit();
			
			Connection conn = DBConnectionPool.instance().getConnection();
			try {
				Statement stmt = conn.createStatement();
				if (!stmt.executeQuery("select OID from bx_right where CLASSRIGHT_CLASSOID = " + moSystemInfo.getOID()).first()) {
					stmt.executeUpdate("insert into bx_right values((select max(OID) from bx_right r) + 1, (select oid  from metaobjects where name = 'ClassRight'),"
							+ " null, 3, \"CRUD\", " + moSystemInfo.getOID() + ", null, null, null)");
					// anonymous read access is required by the init of box edit
					// and anonymous read / write access is required by the consistency check
					stmt.executeUpdate("insert into bx_right values((select max(OID) from bx_right r) + 1, (select oid  from metaobjects where name = 'ClassRight'),"
							+ " null, 1, \"CRUD\", " + moSystemInfo.getOID() + ", null, null, null)");
				}
				stmt.execute("create or replace view histMaxOIDs as select max(oid) as maxoid from bx_personhistory group by bx_oidfamilie");
			} finally {
				if (conn != null) {
					DBConnectionPool.instance().ungetConnection(conn);
				}
			}
			
			for (final String code : new String[] {"DISPLAY_INFO", SystemInfo.DISABLE_USER_LOGIN}) {
				uu.epoData(trx, context, BOSystemInfo.CLASS_NAME, "code", code, new Hashtable<String, String>(){{
					put("code", code);
				}});
			}
			
			uu.epoData(trx, context, BOSystemInfo.CLASS_NAME, "code", SystemInfo.READONLY, new Hashtable<String, String>(){{
				put("code", SystemInfo.READONLY);
				put("mode", "" + SystemInfo.ReadOnlyMode.DISABLED);
			}});
			
			uu.epoData(trx, context, BOSystemInfo.CLASS_NAME, "code", SystemInfo.DISABLE_IMPORT, new Hashtable<String, String>(){{
				put("code", SystemInfo.DISABLE_IMPORT);
			}});
			
			BOObject boAdminUserItem = uu.epoData(trx, context, "IndividualUser", "name", "bx_administrator", new Hashtable<String, String>(){{
				put("name", "bx_administratorUser");
			}});
			
			BOObject boAdminUserGroup = uu.epoData(trx, context, "UserGroup", "name", "bx_administratorGroup", new Hashtable<String, String>(){{
				put("name", "bx_administratorGroup");
			}});
			
			@SuppressWarnings("unchecked")
			List<BOObject> administrators = BoxalinoUtilities.executeQuery("LoginUser", "LoginUser_Administrator == 'true'", null, trx, request);

			boAdminUserItem.addListReferencedObject("containedIn", boAdminUserGroup);
			boAdminUserItem.addListReferencedObject("containedIn", BoxalinoUtilities.getObjectByKey("UserGroup", "name", "edvkgUser", trx));
			
			for (BOObject boLoginUser : administrators) {
				boLoginUser.setReferencedObject("userItem", boAdminUserItem);
			}
			
			uu.epoData(trx, context, "MenuItem", "labelID", "SystemAvailability", new Hashtable<String, String>(){{
				put("label", "Systemverf�gbarkeit");
				put("parentid", "48");
				put("level", "1");
				put("position", "2");
				put("href", "../member/adm_systemAvailability.html?client_session_labelA=SystemAvailability");
				put("labelID", "SystemAvailability");
			}});
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		
		public void upgradeToVersion_01_56(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			
			/*
			 * Bemerkung: Es m�ssen nur diejenigen Felder kopiert werden,
			 * welche der CC als Datenbasis einliest. Die Felder der
			 * Personenaktivierungsroutine m�ssen nicht beachtet werden,
			 * da die Routine nur vom Import kommende Felder kopiert.
			 */
			Connection conn = DBConnectionPool.instance().getConnection();
			try {
				Statement stmt = conn.createStatement();
				String query = "update bx_personhistory ph"
					+ " inner join bx_person p on p.oid = ph.bx_oidperson"
					+ " inner join (select max(oid) as maxoid from bx_personhistory group by bx_oidperson) phmax on ph.oid = phmax.maxoid"
					+ " set ph.  bx_oidkg   = p.  bx_oidkg"
					+ " , ph.	bx_oidvorstand	 = p.	bx_oidvorstand"
					+ " , ph.	bx_oidehepartner	 = p.	bx_oidehepartner"
					+ " , ph.	bx_oidvater	 = p.	bx_oidvater"
					+ " , ph.	bx_oidmutter	 = p.	bx_oidmutter"
					+ " , ph.	bx_oidfamilie	 = p.	bx_oidfamilie"
					+ " , ph.	bx_oidhaushalt	 = p.	bx_oidhaushalt"
					+ " , ph.	bx_WegAdrZusatz	 = p.	bx_WegAdrZusatz"
					+ " , ph.	bx_AdrZusatz	 = p.	bx_AdrZusatz"
					+ " , ph.	bx_ZuzAdrZusatz	 = p.	bx_ZuzAdrZusatz"
					+ " , ph.	bx_WegAdrZusatz	 = p.	bx_WegAdrZusatz"
					+ " , ph.	bx_ZusAdrZusatz	 = p.	bx_ZusAdrZusatz"
					+ " , ph.	bx_hatkind	 = p.	bx_hatkind"
					+ " , ph.	bx_statusperson	 = p.	bx_statusperson"
					+ " , ph.	bx_geburtsdatum	 = p.	bx_geburtsdatum"
					+ " , ph.	bx_codestimmrecht	 = p.	bx_codestimmrecht"
					+ " , ph.	bx_stimmrecht	 = p.	bx_stimmrecht"
					+ " , ph.	bx_konfession	 = p.	bx_konfession"
					+ " , ph.	bx_oidcodeaufenthalt	 = p.	bx_oidcodeaufenthalt"
					+ " , ph.	bx_oidkonfession	 = p.	bx_oidkonfession"
					+ " , ph.	bx_oidcodeortheimat	 = p.	bx_oidcodeortheimat"
					+ " , ph.	bx_oidmission	 = p.	bx_oidmission"
					+ " , ph.	bx_oidcodezivilstand	 = p.	bx_oidcodezivilstand"
					+ " , ph.	bx_oidcodestrasse	 = p.	bx_oidcodestrasse"
					+ " , ph.	bx_hausnr	 = p.	bx_hausnr"
					+ " , ph.	bx_zeitungverzicht	 = p.	bx_zeitungverzicht" 
					+ " , ph.	bx_codegeschlecht	 = p.	bx_codegeschlecht"
					;
				stmt.executeUpdate(query);
			} finally {
				if (conn != null) {
					DBConnectionPool.instance().ungetConnection(conn);
				}
			}
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		public void upgradeToVersion_01_57(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			MetaService service = box.getMetaService();
			BLTransaction trx = BLTransaction.startTransaction(context);
			UpgradeUtil uu = UpgradeUtil.getInstance();
			
			MetaObject moPerson = uu.getClassForName("Person");
			MetaObject moPersonHistory = uu.getClassForName("PersonHistory");
			MetaObject moFamilyKG = uu.epoMetaObject(trx, service, BOFamilienKirchgemeinde.CLASS_NAME, BOFamilienKirchgemeinde.class.getName(), "Familien KGs", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.SHOW_EXTENT);
			MetaObject moKirchgemeinde = uu.getClassForName("Kirchgemeinde");
			
			{
				uu.epoReferenceAttribute(trx, context, moFamilyKG, "OIDKG", moKirchgemeinde, UpgradeUtil.NOT_PARTOF, "OIDKG", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.MANDATORY, UpgradeUtil.NOT_UNIQUE);
				uu.epoBooleanAttribute(trx, context, moFamilyKG, "isMission", "Ist Mission", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.MANDATORY, UpgradeUtil.NOT_UNIQUE);
			}
			
			for (String name : new String[] { "OIDKGVorstand", "OIDKGPartner", "OIDKGKind", 
				"OIDKGVorstandMission", "OIDKGPartnerMission", "OIDKGKindMission" }) {
				uu.eaMetaAttribute(trx, context, moPerson, name);
				uu.eaMetaAttribute(trx, context, moPersonHistory, name);
			}
			uu.epoParentChildrenAttribute(trx, context, moPerson, moFamilyKG, "familienKGListe", "Familien KG Liste", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			trx.commit();
			
			Connection c = DBConnectionPool.instance().getConnection();
			Statement stmt = null; 
			try { 
				stmt = c.createStatement();
				String userKGMatchExpression = "( ({=login_Administrator} != true) OR (Person_familienKGListe_OIDKG == {=login_OIDKirchgemeinde}) ) AND (Person_AdresseStadt == true)";
				String userKGMismatchExpression = "({=login_Administrator} != true) AND (Person_familienKGListe_OIDKG != {=login_OIDKirchgemeinde})";
				stmt.executeUpdate("update bx_view set bx_expression = \"" + userKGMatchExpression + "\" where bx_name = 'kirchgemeindePerson'");
				stmt.executeUpdate("update bx_view set bx_expression = \"" + userKGMismatchExpression + "\" where bx_name = 'ausserKirchGemeinde'");
				PostImportHelper.reinitializeTableFamilienKirchgemeinden(c);
			} finally {
				if (stmt != null) {
					stmt.close();
				}
				DBConnectionPool.instance().ungetConnection(c);
			}
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_01_70(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			/* Person */
			MetaObject moPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			uu.epoStringAttribute(trx, context, moPerson, "partnerBezFam", 1, "Beziehung der Eltern/Partner untereinander in der tech. Familie, e=Ehe, k=Konkubinat, g=gleichgeschlechtlich", false, false, false);
			uu.epoShortAttribute(trx, context, moPerson, "famInfo", "Bitfelder f�r Familienhilfeinfos", false, false, false);
			uu.eaMetaAttribute(trx, context, moPerson, "OIDVaterFamilie");
			uu.eaMetaAttribute(trx, context, moPerson, "OIDMutterFamilie");
			
			/* Person History */
			MetaObject moPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_02_00(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			/* Person */
			MetaObject moPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
//			uu.epoLongAttribute(trx, context, moPerson, "OIDnfb", "Neue Familienbildung Familien OID (temp.)", false, false, false);

			MetaObject moPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			uu.epoBooleanAttribute(trx, context, moPersonHistory, "lastEntry", "Letzter Eintrag", false, false, false);
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_02_01(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			MetaObject moPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			MetaObject moCodeHaus = MetaObjectLookup.getInstance().getMetaObject("CodeHaus");
			MetaObject moCodeHausKG = MetaObjectLookup.getInstance().getMetaObject("CodeHausKG");
			
			uu.epoIndex(trx, context, false, moPersonHistory, moPersonHistory.getAllAttributeForName("lastEntry", context).getIndex());
			uu.epoIndex(trx, context, false, moCodeHaus, moCodeHaus.getAllAttributeForName("OIDStrasse", context).getIndex(), moCodeHaus.getAllAttributeForName("HausNr", context).getIndex());
			uu.epoIndex(trx, context, false, moCodeHausKG, moCodeHausKG.getAllAttributeForName("OIDStrasse", context).getIndex(), moCodeHausKG.getAllAttributeForName("HausNr", context).getIndex());
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_02_02(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			MetaObject moPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			MetaObject moPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			
			uu.eaMetaAttribute(trx, context, moPerson, "Vorstand");
			uu.eaMetaAttribute(trx, context, moPersonHistory, "Vorstand");
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_02_03(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			MetaObject moPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			MetaObject moPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			
			uu.epoBooleanAttribute(trx, context, moPerson, "Vorstand", "Vorstand (berechnet)", false, false, false, MetaAttribute.LIFECYCLE_CALCULATED);
			uu.epoBooleanAttribute(trx, context, moPersonHistory, "Vorstand", "Vorstand (berechnet)", false, false, false, MetaAttribute.LIFECYCLE_CALCULATED);
			
			MetaAttribute ma = uu.epoReferenceAttribute(trx, context, moPerson, "OIDOrigPartner", moPerson, false, "Der Partner nach OIZ Import", false, false, false);
			uu.epoIndex(trx, context, false, moPerson, ma.getIndex());
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_02_04(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			MetaObject moPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			MetaObject moPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			
			moPerson.setClassName(BOPerson.class.getCanonicalName());
			moPersonHistory.setClassName(BOPersonHistory.class.getCanonicalName());
			
			trx.putObject(moPerson);
			trx.putObject(moPersonHistory);
			
			// Attributrechte setzen
//			trx.begin();
//			Connection c = trx.getConnection();
//			MetaObject moAttrRight = MetaObjectLookup.getInstance().getMetaObject("AttributeRight");
//			BLManager.getInstance().setLastOID((ListAttribute)moAttrRight.getAllAttributeForName("attrOID", context), c);
			MetaAttribute maFamInfo = moPerson.getAllAttributeForName("famInfo", context);
			MetaAttribute maPartnerBezFam = moPerson.getAllAttributeForName("partnerBezFam", context);
			MetaAttribute[] attrs = { maFamInfo, maPartnerBezFam };
			List<BOObject> boAttrRights = BoxalinoUtilities.executeQuery("AttributeRight", "AttributeRight_OID in (7)", trx, context);   
			for (BOObject attrRight : boAttrRights) {
				for (MetaAttribute ma : attrs) {
					attrRight.addListValue("attrOID", "" + ma.getOID(), context, null);
				}
			}
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_02_05(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			MetaObject moPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			MetaObject moPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			uu.epoShortAttribute(trx, context, moPerson, "EWID", "Adresszusatzindex bzw. Eidg. Wohnungsid.", false, false, false);
			uu.epoShortAttribute(trx, context, moPersonHistory, "EWID", "Adresszusatzindex bzw. Eidg. Wohnungsid.", false, false, false);
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}			

		public void upgradeToVersion_03_00(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			MetaObject moPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			MetaObject moPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");

			/* Meldungen */
			MetaObject moMeldung = uu.epoMetaObject(trx, box.getMetaService(), BOMeldung.CLASS_NAME, BOMeldung.class.getName(), "Meldungen des Imports und Postimports", false, false);
			uu.eaMetaAttribute(trx, context, moMeldung, "text");
			uu.eaMetaAttribute(trx, context, moMeldung, "type");
			uu.eaMetaAttribute(trx, context, moMeldung, "code");
			uu.epoStringAttribute(trx, context, moMeldung, "arguments", 255, "Mit Trennzeichen separierte Argumente f�r Meldung", false, false, false);
			uu.epoStringAttribute(trx, context, moMeldung, "mld", 3000, "Zusammengesetzte Meldung", false, false, false, MetaAttribute.LIFECYCLE_CALCULATED);
			uu.epoBooleanAttribute(trx, context, moMeldung, "abgehakt", "Meldung wurde zur Kenntnis genommen.", false, false, false);
			

			/* Vordefinierte Meldungen */
			MetaObject moMldVorlage = uu.epoMetaObject(trx, box.getMetaService(), "Meldungsvorlage", BOObject.class.getName(), "Meldungsvorlage", false, false);
			uu.epoStringAttribute(trx, context, moMldVorlage, "text", 255, "Meldungstext mit Platzhaltern", false, false, false);
			uu.epoStringAttribute(trx, context, moMldVorlage, "type", 1, "Schwere der Meldung, e=error, w=warning, i=info", false, false, false);
			uu.epoStringAttribute(trx, context, moMldVorlage, "code", 5, "Code der Meldung; Subdomain", false, false, false);
			uu.epoStringAttribute(trx, context, moMldVorlage, "domain", 1, "Import oder PostImport", false, false, false);
			uu.epoParentChildrenAttribute(trx, context, moMldVorlage, moMeldung, "meldungen", "Meldungen, welche auf diese Vorlage verweisen", false, false, false);
			
			/* Allgemeine Felder */
			uu.epoDateAttribute(trx, context, moPerson, "KonfessionsDatum", "Aenderungsdatum der Konfession", false, false, false);
			uu.epoDateAttribute(trx, context, moPersonHistory, "KonfessionsDatum", "Aenderungsdatum der Konfession", false, false, false);
			uu.epoDateAttribute(trx, context, moPerson, "AufenthaltsartDatum", "Aenderungsdatum der Aufenthaltsart", false, false, false);
			uu.epoDateAttribute(trx, context, moPersonHistory, "AufenthaltsartDatum", "Aenderungsdatum der Aufenthaltsart", false, false, false);
			uu.epoDateAttribute(trx, context, moPerson, "MeldeadresseDatum", "Aenderungsdatum der Meldeadresse", false, false, false);
			uu.epoDateAttribute(trx, context, moPersonHistory, "MeldeadresseDatum", "Aenderungsdatum der Meldeadresse", false, false, false);
			uu.epoReferenceAttribute(trx, context, moPerson, "OIDVormund", moPerson, false, "", false, false, false);
			uu.epoReferenceAttribute(trx, context, moPersonHistory, "OIDVormund", moPerson, false, "", false, false, false);
			
			/* Felder nur auf Person History */
			uu.epoDateAttribute(trx, context, moPersonHistory, "ErstesUpdateDatum", "Wann die Personendaten dieses Records zum ersten Mal eingelesen wurden", false, false, false);
			uu.epoParentChildrenAttribute(trx, context, moPersonHistory, moMeldung, "meldungen", "Meldungen vom Import bzw. PostImport her", false, false, false);
			uu.epoShortAttribute(trx, context, moPersonHistory, "VorfallBits", "Interne Verwendung", false, false, false);
			
			uu.eaMetaAttribute(trx, context, moPersonHistory, "CodeAdressSperre");
			uu.eaMetaAttribute(trx, context, moPerson, "CodeAdressSperre");
			uu.eaMetaAttribute(trx, context, moPerson, "OIDnfb");
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}			

		public void upgradeToVersion_03_01(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			UpgradeUtil uu = UpgradeUtil.getInstance();

			MetaObject moMV = MetaObjectLookup.getInstance().getMetaObject("Meldungsvorlage");
			uu.epoIndex(trx, context, false, moMV, moMV.getAllAttributeForName("code", context).getIndex());
			uu.epoIndex(trx, context, false, moMV, moMV.getAllAttributeForName("type", context).getIndex());
			MetaObject moMld = MetaObjectLookup.getInstance().getMetaObject("Meldung");
			uu.epoIndex(trx, context, false, moMld, moMld.getAllAttributeForName("abgehakt", context).getIndex());
			MetaObject moPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			uu.epoIndex(trx, context, false, moPersonHistory, moPersonHistory.getAllAttributeForName("ErstesUpdateDatum", context).getIndex());
			
			Meldungsvorlage.writeVorlagenToDB((BOContext)request.getContext());
			

			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		
		public void upgradeToVersion_04_00(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();

			BLTransaction trx = BLTransaction.startTransaction(context);
		
			MetaObject moPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			MetaObject moPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			MetaObject moMeldung = MetaObjectLookup.getInstance().getMetaObject("Meldung");

			MetaObject moOhneAnrecht = uu.epoMetaObject(trx, box.getMetaService(), BOPersonOA.CLASS_NAME, BOPersonOA.class.getName(), "Personen ohne Anrecht (wird im Moment nur f�r angeforderte gef�hrt, mh@20110628)", false, false);
			// oid ist ZIP, d.h. es gibt pro ZIP nur ein Eintrag
			uu.epoByteAttribute(trx, context, moOhneAnrecht, "grundID", "Grund f�r kein Anrecht als ID (autom. erzeugt von OIZ) von Feedback", false, false, false);
			uu.epoDateAttribute(trx, context, moOhneAnrecht, "datVerarbeit", "Verarbeitungsdatum auf Seite OIZ", false, false, false);
			uu.epoBooleanAttribute(trx, context, moOhneAnrecht, "lwMutiert", "Geliefert weil mutiert", false, false, false);
			
			MetaObject moAnforderung = uu.epoMetaObject(trx, box.getMetaService(), BOAnforderung.CN, BOAnforderung.class.getName(), "Personenanforderungen an die OIZ", false, false);
			uu.epoReferenceAttribute(trx, context, moAnforderung, "zip", moPerson, false, "ZIP der Person", false, true, false);
			uu.epoIntegerAttribute(trx, context, moAnforderung, "laufnummer", "zugeh�rige Laufnummer des Exportfiles", false, false, false);
			uu.epoDateAttribute(trx, context, moAnforderung, "datum", "Erstellungsdatum der Anforderung", false, false, false);
//			uu.epoReferenceAttribute(trx, context, moAnforderung, "meldung", moMeldung, false, "Referenz auf Meldung als Grund falls autom. Anforderung", false, false, false);
			uu.epoStringAttribute(trx, context, moAnforderung, "kommentar", 255, "Manuell eingegebener Kommentar", false, false, false);
			uu.epoIntegerAttribute(trx, context, moAnforderung, "fbLaufnummer", "Laufnummer des Feedbacks", false, false, false);
			uu.epoReferenceAttribute(trx, context, moAnforderung, "fbMitAnrecht", moPersonHistory, false, "Referenz auf importierten Record", false, false, false);
			uu.epoReferenceAttribute(trx, context, moAnforderung, "fbOhneAnrecht", moOhneAnrecht, false, "Referenz auf importierten Record ohne Anrecht", false, false, false);
			uu.epoParentChildrenAttribute(trx, context, moAnforderung, moMeldung, "meldungen", "Meldungen, welche zu dieser Anforderung gef�hrt haben", false, false, false);
			uu.eaMetaAttribute(trx, context, moAnforderung, "meldung");
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_04_01(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);
			
			MetaObject moAnforderung = MetaObjectLookup.getInstance().getMetaObject("Anforderung");
			uu.epoIndex(trx, context, false, moAnforderung, moAnforderung.getAllAttributeForName("zip", context).getIndex());
			uu.epoIndex(trx, context, false, moAnforderung, moAnforderung.getAllAttributeForName("laufnummer", context).getIndex());
			uu.epoIndex(trx, context, false, moAnforderung, moAnforderung.getAllAttributeForName("fbMitAnrecht", context).getIndex());
			uu.epoIndex(trx, context, false, moAnforderung, moAnforderung.getAllAttributeForName("fbOhneAnrecht", context).getIndex());
			uu.epoIndex(trx, context, false, moAnforderung, moAnforderung.getAllAttributeForName("fbLaufnummer", context).getIndex());
			
			MetaObject moPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			uu.epoIndex(trx, context, false, moPerson, moPerson.getAllAttributeForName("OIDVormund", context).getIndex());
			MetaObject moMeldung = MetaObjectLookup.getInstance().getMetaObject("Meldung");
			uu.epoIndex(trx, context, false, moMeldung, moMeldung.getAllAttributeForName("PersonHistorymeldungenOID", context).getIndex());
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_04_02(IRequest request, IBox box) throws Exception {
			// Kontrolliertes Werfen einer Exception zum Unterbruch des Updates:
			throw new Exception("Kontrollierter Unterbruch des Update-Prozedere Schni2 zum zwischenzeitigen Ausf�hren von Scripts");
		}
		
		public void upgradeToVersion_04_03(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);
			
			MetaObject moPersonHistory = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			uu.eaMetaAttribute(trx, context, moPersonHistory, "DatVorfall");
			uu.eaMetaAttribute(trx, context, moPersonHistory, "Vorfallart");
			uu.eaMetaAttribute(trx, context, moPersonHistory, "Vorfalltyp");
			uu.eaMetaAttribute(trx, context, moPersonHistory, "RecordErrorText");
			uu.eaMetaAttribute(trx, context, moPersonHistory, "RecordError");

			MetaObject moPerson = MetaObjectLookup.getInstance().getMetaObject("Person");
			uu.eaMetaAttribute(trx, context, moPerson, "Anrecht");
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_04_04(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			MetaObject moPH = MetaObjectLookup.getInstance().getMetaObject("PersonHistory");
			MetaObject moMeldung = MetaObjectLookup.getInstance().getMetaObject("Meldung");
			uu.epoReferenceAttribute(trx, context, moMeldung, "ursprung", moPH, false, "L�ngerfristige Verbindung zum ph-Eintrag", false, false, false);
			MetaObject moAnrecht = MetaObjectLookup.getInstance().getMetaObject("Anforderung");
			MetaAttribute ma = uu.epoBooleanAttribute(trx, context, moAnrecht, "manuell", "Anf. wurde manuell erfasst", false, false, false);
			uu.epoIndex(trx, context, false, moAnrecht, ma.getIndex());
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		
		public void upgradeToVersion_04_05(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			String [] indexedAttrs = new String [] { "OIDKG", "PersonfamilienKGListeOID", "isMission"};
			for (String attr : indexedAttrs) {
				int attrIndex = Consts.FamilienKirchgemeinden.getAllAttributeForName(attr, context).getIndex();
				uu.epoIndex(trx, context, false, Consts.FamilienKirchgemeinden, attrIndex);
			}
	
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_04_06(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			Meldungsvorlage.writeVorlagenToDB((BOContext)request.getContext());

			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		

		public void upgradeToVersion_04_07(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext) request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			UpgradeUtil uu = UpgradeUtil.getInstance();
			
			MetaObject moOptionenKG = uu.getClassForName("OptionenKG");
			uu.epoBooleanAttribute(trx, context, moOptionenKG, "internalTemplate", "Interne Vorlage", NOT_INTERNAL, NOT_MANDATORY, NOT_UNIQUE);
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		
		public void upgradeToVersion_04_08(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext) request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			for (BOObject bo : BOMQuery.of("OptionenKG").trx(trx).get()) {
				if (bo.getValue("internalTemplate", context) == null) {
					boolean isInternal = !Integer.valueOf(1001).equals(bo.getValue("OIDKirchgemeinde", context));
					bo.setValue("internalTemplate", isInternal, context);
				}
			}
			// delete all templates that were not modified by either of the administrators, i.e. the standard templates located within the kgs
			String sql = "select * from bx_optionenkg o inner join bx_optionenkg oc on o.bx_titel = oc.bx_titel" +
						 " where o.bx_modoidlogin in(4, 244) && o.BX_OIDKIRCHGEMEINDE < 1001 && oc.BX_OIDKIRCHGEMEINDE = 1001;";
			for (BOObject bo : BOMQuery.sql(sql, trx, context)) {
				bo.setDeleted(true, context);
			}
			Meldungsvorlage.writeVorlagenToDB((BOContext) request.getContext());
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		
		public void upgradeToVersion_04_09(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);
			MetaService service = box.getMetaService();
			
			MetaObject moBVA = uu.epoMetaObject(BOBVA.class, service, trx);
			MetaObject moBVAEntry = uu.epoMetaObject(BOBVAEntry.class, service, trx);
			
			uu.epoBxCreationTimestamp(trx, context, moBVA);
			uu.epoBxModificationTimestamp(trx, context, moBVA);
			uu.epoTextAttribute(trx, context, moBVA, "cause", 0, "Grund", NOT_INTERNAL, NOT_MANDATORY, NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moBVA, "status", 255, "Status", NOT_INTERNAL, NOT_MANDATORY, NOT_UNIQUE);
			uu.epoLongAttribute(trx, context, moBVA, "creationImportId", "Import Id Erstellung", NOT_INTERNAL, NOT_MANDATORY, NOT_UNIQUE);
			uu.epoLongAttribute(trx, context, moBVA, "modificationImportId", "Import Id Bearbeitung", NOT_INTERNAL, NOT_MANDATORY, NOT_UNIQUE);
			uu.epoLongAttribute(trx, context, moBVA, "creationRequestId", "Anforderungs Id Erstellung", NOT_INTERNAL, NOT_MANDATORY, NOT_UNIQUE);
			uu.epoLongAttribute(trx, context, moBVA, "modificationRequestId", "Anforderungs Id Bearbeitung", NOT_INTERNAL, NOT_MANDATORY, NOT_UNIQUE);
			uu.epoParentChildrenAttribute(trx, context, moBVA, moBVAEntry, "entries", "Eintraege", NOT_INTERNAL, NOT_MANDATORY, NOT_UNIQUE);
			
			uu.epoBxCreationTimestamp(trx, context, moBVAEntry);
			uu.epoBxModificationTimestamp(trx, context, moBVAEntry);
			uu.epoLongAttribute(trx, context, moBVAEntry, "OIDPerson", "Person", NOT_INTERNAL, MANDATORY, NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moBVAEntry, "status", 255, "Status", NOT_INTERNAL, NOT_MANDATORY, NOT_UNIQUE);
			
			MetaObject moPerson = uu.getClassForName(BOPerson.CLASS_NAME);
			MetaObject moCodeHausKG = uu.getClassForName("CodeHausKG");
			uu.epoReferenceAttribute(trx, context, moPerson, "codeHausKG", moCodeHausKG, UpgradeUtil.NOT_PARTOF, "CodeHausKG", NOT_INTERNAL, NOT_MANDATORY, NOT_UNIQUE);
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_04_10(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);
			MetaService service = box.getMetaService();
			
			MetaObject moPersonHistory = uu.getClassForName(BOPersonHistory.CLASS_NAME);
			MetaObject moPerson = uu.getClassForName(BOPerson.CLASS_NAME);
			MetaObject moCodeStrasse = uu.getClassForName("CodeStrasse");
			MetaObject moKG = uu.getClassForName("Kirchgemeinde");
			
			MetaObject moSchuelerInfo = uu.epoMetaObject(trx, service, BOSchuelerInfo.CLASS_NAME, BOSchuelerInfo.class.getName(), "SchuelerInfo", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.SHOW_EXTENT);
			MetaObject moSchulHaus = uu.epoMetaObject(trx, service, BOSchulHaus.CLASS_NAME, BOSchulHaus.class.getName(), "SchulHaus", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.SHOW_EXTENT);
			MetaObject moSchulKlasse = uu.epoMetaObject(trx, service, BOSchulKlasse.CLASS_NAME, BOSchulKlasse.class.getName(), "SchulKlasse", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.SHOW_EXTENT);
			MetaObject moSchulSprache = uu.epoMetaObject(trx, service, BOSchulSprache.CLASS_NAME, BOSchulSprache.class.getName(), "SchulSprache", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.SHOW_EXTENT);
			MetaObject moSchulStufe = uu.epoMetaObject(trx, service, BOSchulStufe.CLASS_NAME, BOSchulStufe.class.getName(), "SchulStufe", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.SHOW_EXTENT);
			MetaObject moSchulJahrBeginn = uu.epoMetaObject(trx, service, BOSchulJahrBeginn.CLASS_NAME, BOSchulJahrBeginn.class.getName(), "SchulJahrBeginn", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.SHOW_EXTENT);
			
			
			uu.epoReferenceAttribute(trx, context, moPerson, "schuelerInfo", moSchuelerInfo, UpgradeUtil.NOT_PARTOF, "SchulerInfo", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoBooleanAttribute(trx, context, moPerson, "rpgAktiv", "RPG Aktiv", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoParentChildrenAttribute(trx, context, moPerson, moSchuelerInfo, "schuelerInfos", "Schueler Infos", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			
			
			uu.epoBooleanAttribute(trx, context, moPersonHistory, "rpgAktiv", "RPG Aktiv", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			
			
			uu.epoBooleanAttribute(trx, context, moSchuelerInfo, "schulAktiv", "Schul Aktiv", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoReferenceAttribute(trx, context, moSchuelerInfo, "erstSprache", moSchulSprache, UpgradeUtil.NOT_PARTOF, "ErstSprache", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoReferenceAttribute(trx, context, moSchuelerInfo, "aktSchulklasse", moSchulKlasse, UpgradeUtil.NOT_PARTOF, "Aktuelle Schulklasse", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoN2MAttribute(trx, context, moSchuelerInfo, moSchulKlasse, "schulKlassen", "Schulklassen", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoBooleanAttribute(trx, context, moSchuelerInfo, "rpgSperre", "RPG-Sperre", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoReferenceAttribute(trx, context, moSchuelerInfo, "rpgKG", moKG, UpgradeUtil.NOT_PARTOF, "RPG-KG", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoIntegerAttribute(trx, context, moSchuelerInfo, "rpgSchuleOffset", "RPG-Schule-Offset", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoIntegerAttribute(trx, context, moSchuelerInfo, "rpgStufe", "RPG-Stufe (berechnet)", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoBooleanAttribute(trx, context, moSchuelerInfo, "lastEntry", "Last Entry", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoTimestampAttribute(trx, context, moSchuelerInfo, "importDate", "Import Date", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			
			
			uu.epoStringAttribute(trx, context, moSchulHaus, "adressTyp", 255, "Adresstyp", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moSchulHaus, "name", 255, "Name", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoReferenceAttribute(trx, context, moSchulHaus, "OIDStrasse", moCodeStrasse, UpgradeUtil.NOT_PARTOF, "Strasse", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moSchulHaus, "strassenName", 255, "Strassen Name", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoIntegerAttribute(trx, context, moSchulHaus, "hausNr", "Haus-Nr.", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moSchulHaus, "hausNrZusatz", 2, "Haus-Nr.-Zusatz", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moSchulHaus, "plz", 10, "PLZ", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moSchulHaus, "ort", 255, "Ort", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moSchulHaus, "externalId", 255, "externalId", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moSchulHaus, "schulKreis", 255, "schulKreis", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoParentChildrenAttribute(trx, context, moSchulHaus, moSchulKlasse, "schulKlassen", "Schulklassen", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			
			
			uu.epoIntegerAttribute(trx, context, moSchulKlasse, "schuljahr", "Schuljahr", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoIntegerAttribute(trx, context, moSchulKlasse, "id", "ID", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moSchulKlasse, "lpAnrede", 255, "LPAnrede", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moSchulKlasse, "lpVorname", 255, "LPVorname", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moSchulKlasse, "lpName", 255, "LPName", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			// ??
			uu.epoStringAttribute(trx, context, moSchulKlasse, "name", 255, "Name", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoN2MAttribute(trx, context, moSchulKlasse, moSchuelerInfo, "aktuelleSchulklassen", "Aktuelle Schulklassen", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			
			
			uu.epoStringAttribute(trx, context, moSchulSprache, "bezeichnung", 255, "Bezeichnung", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			
			
			uu.epoStringAttribute(trx, context, moSchulStufe, "name", 255, "Name", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoIntegerAttribute(trx, context, moSchulStufe, "stufe", "Stufe", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moSchulStufe, "bezeichnung", 255, "Bezeichnung", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE, MetaAttribute.LIFECYCLE_CALCULATED);
			uu.eaMetaAttribute(trx, context, moSchulStufe, "schulKlassen");
			uu.epoParentChildrenAttribute(trx, context, moSchulStufe, moSchulKlasse, "schulKlassen", "Schul-Klassen", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			
			uu.epoDateAttribute(trx, context, moSchulJahrBeginn, "per", "per", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		
		public void upgradeToVersion_04_11(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext) request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			
			String[] updates = {
				"insert into bx_menuitem values((select max(OID) + 1 from bx_menuitem x), 63, 'RPG Adressbuch', '../public/org_adressmain.html?client_session_menu=KGMenuOrganisationen&client_session_label=rpgAdressbuch&client_request_resetFields=true&view_search_clearCriterias_suchePersonen=true&view_search_clear_suchePersonen=true&param_isRPG=true', null, 15, 1, 7, 'n', 'n', null, 'rpgAdressbuch');",
				"insert into bx_menuitem values((select max(OID) + 1 from bx_menuitem x), 63, 'RPG Suche', '../public/rpg_suchemain.html?client_session_menu=KGMenuMitglieder&client_session_label=Suche&client_request_resetFields=true&view_search_clear_suchePersonen=true&view_search_clearCriterias_suchePersonen=true', null, 2, 1, 2, 'n', 'n', null, 'RPG Suche');",
				"insert into bx_right (oid, artoid, bx_user, bx_action, classright_classoid, classright_view, bx_name)" +
				"values ((select max(oid) + 1 from bx_right x), 125, 3, 'CRUD', (select oid from metaobjects where name like 'SchuelerInfo'), null, 'SchuelerInfo');",
				/* obsolete (?)
				"insert into bx_list_rightattroid values ((select max(oid) + 1 from bx_list_rightattroid x), 26, (select oid from metaattributes where internname like 'rpgSperre' and objectoid = (select oid from metaobjects where name = 'SchuelerInfo')));",
				"insert into bx_list_rightattroid values ((select max(oid) + 1 from bx_list_rightattroid x), 6, (select oid from metaattributes where internname like 'rpgSperre' and objectoid = (select oid from metaobjects where name = 'SchuelerInfo')));",
				"insert into bx_list_rightattroid values ((select max(oid) + 1 from bx_list_rightattroid x), 7, (select oid from metaattributes where internname like 'rpgSperre' and objectoid = (select oid from metaobjects where name = 'SchuelerInfo')));",
				*/
				/*
					those updates are required because AdresseStadt does not necessarily mean OID < 500M anymore this is the case for
				 	RPG Personen which are not already in the mzv
				 */
				"update bx_view set bx_expression = '( ({=login_Administrator} != true) OR (Person_familienKGListe_OIDKG == {=login_OIDKirchgemeinde}) ) AND (Person_OID < 500000000)' where bx_name = 'kirchgemeindePerson';",
				"update bx_view set bx_expression = '(Person_OID >= 500000000)' where bx_name = 'kgEigenePerson';",
				"update bx_view set bx_expression = '({=login_Administrator} == true) AND (Person_OID < 500000000)' where bx_name = 'AdminPerson';"
			};
			// check if necessary at all
			if (null == BoxalinoUtilities.getObjectByKey("MenuItem", "label", "RPG Adressbuch", trx)) {
				for (String update : updates) {
					execUpdate(update);
				}
			} else {
				LOGGER.warn("skipping updates (" + Arrays.toString(updates) + ")");
			}
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_04_12(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			MetaObject moSchuelerInfo = uu.getClassForName(BOSchuelerInfo.CLASS_NAME);
			uu.epoIndex(trx, context, false, moSchuelerInfo, moSchuelerInfo.getAllAttributeForName("PersonschuelerInfosOID", context).getIndex());
			uu.epoIndex(trx, context, false, moSchuelerInfo, moSchuelerInfo.getAllAttributeForName("lastEntry", context).getIndex());

			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_04_13(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			MetaObject moSchuelerInfo = uu.getClassForName(BOSchuelerInfo.CLASS_NAME);
			uu.epoDateAttribute(trx, context, moSchuelerInfo, "taufe", "Taufe", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoDateAttribute(trx, context, moSchuelerInfo, "konfirmation", "Konfirmation", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);

			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		
		public void upgradeToVersion_04_14(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext) request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			
			String[] updates = {
				"insert into bx_list_rightattroid values ((select max(oid) + 1 from bx_list_rightattroid x), 26, (select oid from metaattributes where internname like 'rpgAktiv' and objectoid = (select oid from metaobjects where name = 'Person')));",
				"insert into bx_list_rightattroid values ((select max(oid) + 1 from bx_list_rightattroid x), 6, (select oid from metaattributes where internname like 'rpgAktiv' and objectoid = (select oid from metaobjects where name = 'Person')));",
				"insert into bx_list_rightattroid values ((select max(oid) + 1 from bx_list_rightattroid x), 7, (select oid from metaattributes where internname like 'rpgAktiv' and objectoid = (select oid from metaobjects where name = 'Person')));"
			};
			if (0 == countQuery("select count(1) from bx_list_rightattroid where referenzoid = 26 and data = (select oid from metaattributes where internname like 'rpgAktiv' and objectoid = (select oid from metaobjects where name = 'Person'))")) {
				for (String update : updates) {
					execUpdate(update);
				}
			} else {
				LOGGER.warn("skipping updates (" + Arrays.toString(updates) + ")");
			}
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}
		
		public void upgradeToVersion_04_15(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext) request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);
			
			String[] updates = {
				"insert into bx_menuitem values((select max(OID) + 1 from bx_menuitem x), 63, 'RPG Menu', '../public/rpg_suchemain.html?client_session_menu=KGMenuMitglieder&client_session_label=Suche&client_request_resetFields=true&view_search_clear_suchePersonen=true&view_search_clearCriterias_suchePersonen=true', null, null, 0, -1, 'n', 'n', null, 'RPG Menu');",
				"insert into bx_menuitem values((select max(OID) + 1 from bx_menuitem x), 63, 'Detail Person', '../public/rpg_suchemain.html?client_session_menu=KGMenuMitglieder&client_session_label=Suche&client_request_resetFields=true&view_search_clear_suchePersonen=true&view_search_clearCriterias_suchePersonen=true', null, (select oid from bx_menuitem x where bx_labelID = 'RPG Menu'), 1, 2, 'n', 'n', null, 'Detail Person');",
				"insert into bx_menuitem values((select max(OID) + 1 from bx_menuitem x), 63, 'Suche RPG Personen', '../public/rpg_suchemain.html?client_session_menu=KGMenuMitglieder&client_session_label=Suche&client_request_resetFields=true&view_search_clear_suchePersonen=true&view_search_clearCriterias_suchePersonen=true', null, (select oid from bx_menuitem x where bx_labelID = 'RPG Menu'), 1, 2, 'n', 'n', null, 'Suche RPG Personen');",
				"update bx_menuitem set bx_href = '../public/rpg_suchemain.html?client_session_menu=RPG Menu&client_session_label=Suche RPG Personen&client_request_resetFields=true&view_search_clear_suchePersonen=true&view_search_clearCriterias_suchePersonen=true' where bx_label like 'Rpg Suche';",
				"update bx_menuitem set bx_href = '../public/rpg_detailperson.html?client_session_menu=RPG Menu&client_session_label=Suche RPG Personen&client_request_resetFields=true&view_search_clear_suchePersonen=true&view_search_clearCriterias_suchePersonen=true' where bx_labelid like 'Detail Person';"
			};
			if (null == BOMQuery.byKey("MenuItem", "labelID", "RPG Menu", null)) {
				for (String update : updates) {
					execUpdate(update);
				}
			} else {
				LOGGER.warn("skipping updates (" + Arrays.toString(updates) + ")");
			}
			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}


		public void upgradeToVersion_04_16(IRequest request, IBox box) throws XDataTypeException, XMetaException, XSecurityException, XBOConcurrencyConflict {

			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			MetaObject moCodeStrasse = uu.getClassForName("CodeHaus");
			BLTransaction trx = BLTransaction.startTransaction(context);

			LOGGER.info("Create Table:" + BOAuthorityAddress.CLASS_NAME);
			MetaObject moAuthorityAddress = uu.epoMetaObject(trx, box.getMetaService(), BOAuthorityAddress.CLASS_NAME, BOAuthorityAddress.class.getName(), "Amtsadressen, die mit den importierten Adressen verglichen werden, (LB@20150701)", false, false);
			uu.epoIntegerAttribute(trx, context, moAuthorityAddress, "OIDCODESTRASSE", "Strassen-Code der Amtsadresse", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoIntegerAttribute(trx, context, moAuthorityAddress, "OIDCODEHAUS", "Hausnummer-Code der Amtsadresse", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoIntegerAttribute(trx, context, moAuthorityAddress, "StreetNo", "Hausnummer der Amtsadresse", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoDateAttribute(trx, context, moAuthorityAddress, "ValidFrom", "Amtsadresse gueltig ab", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoStringAttribute(trx, context, moAuthorityAddress, "RoomNumber", 50, "Wohnungsnummer", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.NOT_MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoBooleanAttribute(trx, context, moAuthorityAddress, "Status", "Aktiv", UpgradeUtil.NOT_INTERNAL, UpgradeUtil.MANDATORY, UpgradeUtil.NOT_UNIQUE);
			uu.epoIndex(trx, context, false, moAuthorityAddress, moAuthorityAddress.getAllAttributeForName("OIDCODESTRASSE", context).getIndex());
			uu.epoIndex(trx, context, false, moAuthorityAddress, moAuthorityAddress.getAllAttributeForName("OIDCODEHAUS", context).getIndex());

			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}


		public void upgradeToVersion_04_17(IRequest request, IBox box) throws XDataTypeException, XMetaException, XSecurityException, XBOConcurrencyConflict {

			BOContext context = (BOContext)request.getContext();
			UpgradeUtil uu = UpgradeUtil.getInstance();
			BLTransaction trx = BLTransaction.startTransaction(context);

			MetaObject moAuthorityAddress = uu.epoMetaObject(trx, box.getMetaService(), BOAuthorityAddress.CLASS_NAME, BOAuthorityAddress.class.getName(), "Amtsadressen, die mit den importierten Adressen verglichen werden, (LB@20150701)", false, false);

			Long artOID = moAuthorityAddress.getOID();
			String tableName = "bx_" + BOAuthorityAddress.CLASS_NAME;
			String[] updates = {
					"insert into " + tableName + " values (1, "+artOID+", 1023, 978, 330, null, null, 'j');",
					"insert into " + tableName + " values ((select max(OID) + 1 from "+ tableName +" x), "+artOID+", 1101, 3530, 88, null, null, 'j');",
					"insert into " + tableName + " values ((select max(OID) + 1 from "+ tableName +" x), "+artOID+", 1480, 14427, 28, null, null, 'j');",
					"insert into " + tableName + " values ((select max(OID) + 1 from "+ tableName +" x), "+artOID+", 1752, 21553, 24, null, null, 'j');",
					"insert into " + tableName + " values ((select max(OID) + 1 from "+ tableName +" x), "+artOID+", 2513, 41338, 41, null, null, 'j');",
					"insert into " + tableName + " values ((select max(OID) + 1 from "+ tableName +" x), "+artOID+", 2546, 42805, 17, null, null, 'j');",
			};
			LOGGER.info("Insert values in Table:" + BOAuthorityAddress.CLASS_NAME);
			//if (null == BOMQuery.byKey(BOAuthorityAddress.CLASS_NAME, "OIDCODEHAUS", "978", null)) {
				for (String update : updates) {
					execUpdate(update);
				}
			//} else {
			//	LOGGER.warn("skipping updates (" + Arrays.toString(updates) + ")");
			//}

			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}


		//Disable the RPG-Suche for the moment - Mr. Parziani doesn't want it to be published atm, KUS 19.11.2015
		public void upgradeToVersion_04_18(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext) request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);

			String update = "update bx_menuitem set bx_parentid = NULL, bx_position = -1 where bx_label like 'RPG Suche';";
			execUpdate(update);

			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		//Famanschrift Property
		public void upgradeToVersion_04_19(IRequest request, IBox box) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
			BOContext context = (BOContext) request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);

			String update = "insert into metaserviceproperty values ((select max(OID) + 1 from metaserviceproperty x), '119', 'FamilienanschriftGleichgeschlechtlichM', 'An die Herren');";
			execUpdate(update);

			update = "insert into metaserviceproperty values ((select max(OID) + 1 from metaserviceproperty x), '119', 'FamilienanschriftGleichgeschlechtlichW', 'An die Damen');";
			execUpdate(update);

			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		public void upgradeToVersion_04_20(IRequest request, IBox box) throws Exception {
			BOContext context = (BOContext) request.getContext();
			BLTransaction trx = BLTransaction.startTransaction(context);

			String[] updates = {
					"update bx_menuitem set bx_parentid = NULL, bx_position = -1 where bx_label like 'RPG Suche';",
					"update bx_menuitem set bx_parentid = NULL, bx_position = -1 where bx_label like 'RPG Adressbuch';"
			};

			for (String update : updates) {
				execUpdate(update);
			}

			box.getMetaService().setVersion(getActualVersion());
			trx.putObject(box.getMetaService());
			trx.commit();
		}

		private void execUpdate(String sql) {
			Connection c = null;
			Statement stmt = null;
			try {
				c = DBConnectionPool.instance().getConnection();
				stmt = c.createStatement();
				stmt.executeUpdate(sql);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				DBConnectionPool.instance().ungetConnection(c);
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {
						LOGGER.error("SQLException occurred!", e);
					}
				}
			}
		}

		private int countQuery(String sql) {
			Connection c = null;
			Statement stmt = null;
			try {
				c = DBConnectionPool.instance().getConnection();
				stmt = c.createStatement();
				ResultSet rs = stmt.executeQuery(sql);
				rs.next();
				return rs.getInt(1);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				DBConnectionPool.instance().ungetConnection(c);
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {
						LOGGER.error("SQLException occurred!", e);
					}
				}
			}
		}
		
}