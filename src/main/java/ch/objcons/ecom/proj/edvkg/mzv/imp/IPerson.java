package ch.objcons.ecom.proj.edvkg.mzv.imp;


public interface IPerson {
	
	public long getOID(); 
	
	public IPerson getVater();
	public IPerson getMutter();
	public IPerson getPartner();
	/**
	 * w = weiblich, m = m�nnlich, s = keine Person (muss nicht beachtet werden) 
	 * @return
	 */
	public char getGeschlecht();
	/**
	 * Holt den technischen Vorstand.
	 * @return nie <code>null</code>
	 */
	public IPerson getVorstand();
	/**
	 * e = Ehe, k = Konkubinat, g = gleichgeschlechtlich
	 * @return
	 */
	public char getPartnerBeziehung();

	/**
	 * Ist die angegebene Person ein Mitglied der Familie von der aktuellen Person?
	 * @param fmg
	 * @return
	 */
	public boolean hatFamilienMitglied(IPerson fmg);
	
	/**
	 * @return <code>true</code>, if and only if statusPerson is <code>true</code>
	 */
	public boolean istAktiv();
	
	/**
	 * @return <code>true</code>, if either statusPerson or rpgAktiv is <code>true</code>
	 */
	public boolean istAktivAny();
	
}
