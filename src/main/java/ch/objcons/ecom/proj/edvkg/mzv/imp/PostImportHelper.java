package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import ch.objcons.db.DBUtils;
import ch.objcons.db.dbimport.LogFileWriter;
import ch.objcons.ecom.api.ISession;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.bom.utils.BOMQuery;
import ch.objcons.ecom.bom.utils.BOMSupport;
import ch.objcons.ecom.box.util.MetaObjectLookup;
import ch.objcons.ecom.datatypes.EDataTypeValidation;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.*;
import ch.objcons.ecom.proj.edvkg.mzv.BOPersonHistory;
import ch.objcons.ecom.proj.edvkg.mzv.Consts;
import ch.objcons.ecom.proj.edvkg.mzv.bva.BVAStatus;
import ch.objcons.ecom.proj.edvkg.mzv.bva.BVASupport;
import ch.objcons.ecom.proj.edvkg.mzv.imp.BOMeldung.Meldung;
import ch.objcons.ecom.proj.edvkg.mzv.imp.PostImport.Person;

import ch.objcons.ecom.system.security.XSecurityException;
import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

public final class PostImportHelper {

	private static final ILogger LOGGER = Logger.getLogger(PostImportHelper.FamilyKGInfo.class);
	
	private static class FamilyKGInfo {
		final Set<KGWithInfo> _kgs = new HashSet<KGWithInfo>();
		final Set<Long> _affectedPersons = new HashSet<Long>();
		
		void addKG(long OIDKG, boolean isMission) {
			_kgs.add(new KGWithInfo(OIDKG, isMission)); 
		}
		
		void addAffectedPerson(long OID) {
			_affectedPersons.add(OID);
		}
		
		class KGWithInfo {
			final long _OIDKG;
			final boolean _isMission;
			
			KGWithInfo(long OIDKG, boolean isMission) {
				_OIDKG = OIDKG;
				_isMission = isMission;
			}
			
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + (int) (_OIDKG ^ (_OIDKG >>> 32));
				result = prime * result + (_isMission ? 1231 : 1237);
				return result;
			}
			
			@Override
			public boolean equals(Object obj) {
				KGWithInfo other = (KGWithInfo) obj;
				return _OIDKG == other._OIDKG && _isMission == other._isMission;
			}
		}
		
	}

	public static void updateTableFamilienKirchgemeinden (Connection c, Collection<Long> affectedFamilien) throws SQLException {
		Statement stmt = null;
		PreparedStatement stAddEntry = null;
		try {
			final long artOID;
			try {
				artOID = MetaObjectLookup.getInstance().getMetaObject(BOFamilienKirchgemeinde.CLASS_NAME).getOID();
			} catch (XMetaModelNotFoundException e) {
				throw new RuntimeException("Unexpected XMetaModelNotFoundException occurred!", e);
			}
			
			Collection<String> affectedFamilienBundles = new ArrayList<String>();
			{
				StringBuilder sb = new StringBuilder();
				String seperator = "";
				int i = 0;
				for (Long oid : affectedFamilien) {
					sb.append(seperator).append(oid);
					if (i++ % 500 == 0) {
						affectedFamilienBundles.add(sb.toString());
						sb.setLength(0);
						seperator = "";
						continue;
					}
					seperator = ",";
				}
			}
	
			stmt = c.createStatement();
			String queryClearEntries = "delete fkg from bx_familienkirchgemeinde fkg inner join bx_person p on p.OID = fkg.bx_PersonfamilienKGListeOID"
				+ " where p.bx_OIDFamilie in (%s)";
			String querySelectPersons = "select OID, bx_OIDKG, bx_OIDMission, bx_OIDFamilie from bx_person where bx_OIDFamilie in (%s)";
			stAddEntry = c.prepareStatement("insert into bx_familienkirchgemeinde (OID, ArtOID, bx_OIDKG, bx_PersonfamilienKGListeOID, bx_isMission)" 
				+ "values (?, " + artOID + ", ?, ?, ?)");
			for (String entries : affectedFamilienBundles) {
				stmt.executeUpdate(String.format(queryClearEntries, entries));
			}
			
			Map<Long, FamilyKGInfo> familyToKGList = new HashMap<Long, FamilyKGInfo>(affectedFamilien.size());
			for (String familienOIDs : affectedFamilienBundles) {
				stmt.execute(String.format(querySelectPersons, familienOIDs));
				ResultSet rsAffectedPersons = stmt.getResultSet();
				while (rsAffectedPersons.next()) {
					long OID = rsAffectedPersons.getLong(1);
					long OIDKG = rsAffectedPersons.getLong(2);
					long OIDMission = rsAffectedPersons.getLong(3);
					long OIDFamilie = rsAffectedPersons.getLong(4);
					
					FamilyKGInfo familyInfo = familyToKGList.get(OIDFamilie);
					if (familyInfo == null) {
						familyInfo = new FamilyKGInfo();
						familyToKGList.put(OIDFamilie, familyInfo);
					}
					
					if (OIDKG > 0) {
						familyInfo.addKG(OIDKG, false);
					}
					if (OIDMission > 0) {
						familyInfo.addKG(OIDMission, true);
					}
					familyInfo.addAffectedPerson(OID);
				}
			}
			
			ResultSet rsMaxFKGOID = stmt.executeQuery("select max(OID) from bx_familienkirchgemeinde");
			long oidFamilienKirchgemeinden = rsMaxFKGOID.first() ? rsMaxFKGOID.getLong(1) : 0;
			for (FamilyKGInfo familyInfo : familyToKGList.values()) {
				for (FamilyKGInfo.KGWithInfo kgInfo : familyInfo._kgs) {
					for (long OIDPerson : familyInfo._affectedPersons) {
						stAddEntry.setLong(1, ++oidFamilienKirchgemeinden);
						stAddEntry.setLong(2, kgInfo._OIDKG);
						stAddEntry.setLong(3, OIDPerson);
						stAddEntry.setString(4, kgInfo._isMission ? "j" : "n");
						stAddEntry.execute();
					}
				}
			}
		} finally {
			closeAll(stmt, stAddEntry);
		}
	}

	public static void reinitializeTableFamilienKirchgemeinden(Connection c) throws SQLException {
		Statement stmt = null;
		PreparedStatement stAddEntry = null;
		try {
			final long artOID;
			try {
				artOID = MetaObjectLookup.getInstance().getMetaObject(BOFamilienKirchgemeinde.CLASS_NAME).getOID();
			} catch (XMetaModelNotFoundException e) {
				throw new RuntimeException("Unexpected XMetaModelNotFoundException occurred!", e);
			}
			
			stmt = c.createStatement();
			String queryClearEntries = "truncate table bx_familienkirchgemeinde";
			String querySelectPersons = "select OID, bx_OIDKG, bx_OIDMission, bx_OIDFamilie from bx_person";
			stAddEntry = c.prepareStatement("insert into bx_familienkirchgemeinde (OID, ArtOID, bx_OIDKG, bx_PersonfamilienKGListeOID, bx_isMission)" 
				+ "values (?, " + artOID + ", ?, ?, ?)");
			stmt.executeUpdate(queryClearEntries);
			
			stmt.executeQuery(querySelectPersons);
			ResultSet rsAffectedPersons = stmt.getResultSet();
			Map<Long, FamilyKGInfo> familyToKGList = new HashMap<Long, FamilyKGInfo>(500 * 1000);
			while (rsAffectedPersons.next()) {
				long OID = rsAffectedPersons.getLong(1);
				long OIDKG = rsAffectedPersons.getLong(2);
				long OIDMission = rsAffectedPersons.getLong(3);
				long OIDFamilie = rsAffectedPersons.getLong(4);
				
				FamilyKGInfo familyInfo = familyToKGList.get(OIDFamilie);
				if (familyInfo == null) {
					familyInfo = new FamilyKGInfo();
					familyToKGList.put(OIDFamilie, familyInfo);
				}
				
				if (OIDKG > 0) {
					familyInfo.addKG(OIDKG, false);
				}
				if (OIDMission > 0) {
					familyInfo.addKG(OIDMission, true);
				}
				familyInfo.addAffectedPerson(OID);
			}
			
			ResultSet rsMaxFKGOID = stmt.executeQuery("select max(OID) from bx_familienkirchgemeinde");
			long oidFamilienKirchgemeinden = rsMaxFKGOID.first() ? rsMaxFKGOID.getLong(1) : 0;
			for (FamilyKGInfo familyInfo : familyToKGList.values()) {
				for (FamilyKGInfo.KGWithInfo kgInfo : familyInfo._kgs) {
					for (long OIDPerson : familyInfo._affectedPersons) {
						stAddEntry.setLong(1, ++oidFamilienKirchgemeinden);
						stAddEntry.setLong(2, kgInfo._OIDKG);
						stAddEntry.setLong(3, OIDPerson);
						stAddEntry.setString(4, kgInfo._isMission ? "j" : "n");
						stAddEntry.execute();
					}
				}
			}
		} finally {
			closeAll(stmt, stAddEntry);
			BLManager.getInstance().emptyBuffersWithReferrers(Consts.FamilienKirchgemeinden);
		}
	}

	private static void closeAll(Statement... statements) {
		for (Statement statement : statements) {
			try {
				if (statement != null) statement.close();
			} catch (SQLException ignore) {}
		}
	}

	/**
	 * Aktualisiert Datenbankfelder, welche aufgrund des Imports einer Aktualisierung bed�rfen. 
	 * Im folgenden eine Auflistung dieser Felder:
	 * 
	 * bx_islast (PersonHistory): Mindestens der chronologisch letzte Eintrag einer Person
	 *                            in der History muss ein 'islast = true' haben
	 * bx_Konfession: Wichtig f�r join mit codehauskg (Pfarrkreis und KG-Aktualisierung)
	 * bx_OIDKonfession: Muss mit bx_konfession �bereinstimmen
	 * bx_PLZOrt und bx_PLZOrtEind: Wird nach der CodeHaus-Tabelle aktualisiert
	 * bx_ZusPLZOrt und bx_ZusPLZOrtEind und bx_ZusOrt: Werden nach der CodeHaus-Tabelle aktualisiert
	 * bx_StrasseKG: Updaten mit derjenigen Strasse, welche f�r Versandadresse verwendet wird
	 * bx_SortHausNr und bx_ZusSortHausNr: Zur Sortierung notwendig. Updaten nach bx_HausNr und bx_ZusHausNr
	 * bx_Bezirk: Update nach st�dtischen Strassen und Hausnummern (CodeHaus-Tabelle)
	 * bx_AdresseStadt: Muss auf true sein f�r st�dtische Personen und sonst auf false
	 * bx_OIDKG und bx_OIDPfarrkreis: Update nach CodeHausKG-Tabelle
	 * bx_StatusPerson: Dieses besonders wichtige Feld muss in bx_person nach bx_personhistory gef�hrt werden
	 *   
	 * @throws XMetaModelQueryException
	 */
	static void doPostImportQueries (Connection dbConn, LogFileWriter logWriter) throws XMetaModelQueryException {
		/* Jede Person muss mindestens ein History-Eintrag mit islast = true und lastEntry = true haben */
		String query = "delete from tmpoid";
		logWriter.writeText("set islast/lastEntry (0a) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
		query = "insert into tmpoid" +
				" select max(oid) from bx_personhistory group by bx_oidperson having bit_or(bx_islast='j') = 0 or bit_or(bx_lastEntry='j') = 0";
		logWriter.writeText("set islast/lastEntry (0b) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
		query = "update bx_personhistory p inner join tmpoid t on p.oid = t.oid set bx_islast = 'j', bx_lastEntry = 'j'"; 
		logWriter.writeText("set islast/lastEntry (0c) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
	
		/*
		 * Setze die Postleitzahlen der Personen, die �nderungen beim Import erfahren haben.
		 * Die PLZs werden im Import nicht gesetzt. 
		 */
		/* Import: Normale PLZ f�r Importierte setzen */ 
		query = "update " + PostImport._shadowTable + " pers set pers.bx_PLZOrtEind = "+
			" (select distinct haus.bx_postleizahl from bx_codeHaus haus where "+
			"    haus.bx_oidstrasse = pers.bx_OIDCodeStrasse AND "+
			"    haus.bx_HausNr = pers.bx_HausNr ) " +
			" where pers.bx_RecordStatus = 'A'" ;
		logWriter.writeText("set plz (1a) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
		/* Import: Eindeutige Zustell-PLZ f�r st�dtische Zustelladressen ermitteln */ 
		query = "update " + PostImport._shadowTable + " pers " +
			" set pers.bx_ZusPLZOrtEind = " +
			" (select distinct haus.bx_postleizahl from bx_codeHaus haus where "+
			"    haus.bx_oidstrasse = pers.bx_ZusOIDStrasse AND "+
			"    haus.bx_HausNr = pers.bx_ZusHausNr ) " +
			" where pers.bx_RecordStatus = 'A' and bx_ZusArt = 's'";
		logWriter.writeText("set plz (1b) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
	
		/* Import: Normaler Ort und PLZ nach eindeutiger PLZ setzen */
		query = "update " + PostImport._shadowTable + " pers set pers.bx_PLZOrt = SUBSTRING(pers.bx_PLZOrtEind, 1, 4), "+
			" pers.bx_Ort='Z\u00fcrich' " +
			" where pers.bx_RecordStatus = 'A'";
		logWriter.writeText("set plz (2a) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
		/* Import: Zustell -Ort und -PLZ f�r st�dtische Zustelladressen setzen */
		query = "update " + PostImport._shadowTable + " pers set "+
			" pers.bx_ZusPLZOrt = SUBSTRING(pers.bx_ZusPLZOrtEind, 1, 4), " +
			" pers.bx_ZusOrt= 'Z\u00fcrich' " +
			" where pers.bx_RecordStatus = 'A' and pers.bx_ZusArt = 's'";
		logWriter.writeText("set plz (2b) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
	
		/* Alle PLZ-Eintr�ge der Stadtadressen updaten (Vielleicht wurden
		 * diese durch den Import in der CodeHaus-Tabelle ge�ndert) */
		query = "update " + PostImport._mainTable + " p inner join bx_codehaus s on p.bx_oidcodestrasse = s.bx_oidstrasse and p.bx_hausnr = s.bx_hausnr " +
			    "set p.bx_PLZOrtEind = s.bx_postleizahl, p.bx_PLZOrt = substring(s.bx_postleizahl, 1, 4)";
		logWriter.writeText("set plz (3) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
		query = "update " + PostImport._mainTable + " p inner join bx_codehaus s on p.bx_zusoidstrasse = s.bx_oidstrasse and p.bx_zushausnr = s.bx_hausnr " +
	    		"set p.bx_ZusPLZOrtEind = s.bx_postleizahl, p.bx_ZusPLZOrt = substring(s.bx_postleizahl, 1, 4) " +
	    		"where p.bx_ZusArt = 's'";
		logWriter.writeText("set plz (4) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
		
		/* Update des Strassenfelds (Beschleunigung von selects, da
		 * joins mit Strassentabellen nicht mehr notwendig)
		 * M�sste eigentlich nur f�r importierte gemacht werden, da die Business-Logik
		 * von BOPerson dieses Feld schon updatet.
		 */
		query = "update " + PostImport._shadowTable + " p inner join bx_codestrasse s on coalesce(p.bx_zusoidstrasse, p.bx_oidcodestrasse) = s.oid " +
			    "set p.bx_strassekg = s.bx_name";
		logWriter.writeText("set StrasseKG (1) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
		query = "update " + PostImport._mainTable + " p inner join bx_codestrasse s on coalesce(p.bx_zusoidstrasse, p.bx_oidcodestrasse) = s.oid " +
	    	"set p.bx_strassekg = s.bx_name";
		logWriter.writeText("set StrasseKG (2) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
	
		/* Hilfsfeld f�r Hausnummer-Sortierungen setzen */
		/*  bx_SortHausNr = convert(p.bx_hausnr, UNSIGNED) speichert nur die anf�ngliche Zahl ohne nachfolgende Buchstaben (mysql-spezifisch) */
		/* M�sste eigentlich nur f�r importierte Personen erfolgen, da die Business-Logik von
		 * BOPerson dieses Feld schon aktualisiert
		 */
		query = "update " + PostImport._shadowTable + " p set bx_SortHausNr = startingNumber(substring(p.bx_hausnr,1,4)) ";
		logWriter.writeText("set SortHausNr (1) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
		query = "update " + PostImport._shadowTable + " p set bx_ZusSortHausNr = startingNumber(substring(p.bx_zushausnr,1,4)) ";
		logWriter.writeText("set ZusSortHausNr (1) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
	
		/* Update vom Bezirksfeld der Person, da vielleicht die Tabelle der 
		 * Bezirks-Strassen-Zuordnungen durch Import ver�ndert worden ist.
		 * �nderungen an Postfach, Strassennummer, PLZPostfach usw. und deren Konsequenzen
		 * auf den Bezirk werden durch BOPerson gesteuert. 
		 */
		query = "update " + PostImport._mainTable + " p left join bx_codehaus s on case when p.bx_zusart = 's' then p.bx_zusoidstrasse else p.bx_oidcodestrasse end = s.bx_oidstrasse and " +
		  "  case when p.bx_zusart = 's' then p.bx_zushausnr else p.bx_hausnr end = s.bx_hausnr " +
		  "set p.bx_bezirk = case when (" +
		  "  p.bx_zusart = 's' and " +
		  "  p.bx_zusoidstrasse is not null and " +
		  "  p.bx_zushausnr is not null and " +
		  "  p.bx_zuspostfach is null and p.bx_zusplzpostfach is null " + 
		  "  ) or (" +
		  "  p.bx_zusart is null and " +
		  "  p.bx_oidcodestrasse is not null and " +
		  "  p.bx_hausnr is not null and " +
		  "  p.bx_postfach is null and p.bx_plzpostfach is null) " +
		  "then s.bx_postzustellbezirk else null end";				
		logWriter.writeText("set Bezirk (1) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
	
		/* Sicherstellen, dass AdresseStadt nicht-null ist */
		query = "update " + PostImport._shadowTable + " p set bx_AdresseStadt = 'n' where bx_AdresseStadt is null ";
		logWriter.writeText("set AdresseStadt (1) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
		
		/* Kirchgemeinde und Pfarrkreis nach CodeHausKG-Tabelle updaten */
		/* !!!! ACHTUNG !!!! Dieses Update kommt bei Importierten gar nicht durch, da activateMutationen den gesetzten Wert
		 * wieder �berschreibt!
		 */
		query = "update " + PostImport._mainTable + " p " +
				"left join bx_codehauskg ch on p.bx_oidcodestrasse = ch.bx_oidstrasse and p.bx_hausnr = ch.bx_hausnr and p.bx_konfession = ch.bx_konfession " +
				"left join bx_pfarrkreis pk on ch.bx_oidkg = pk.bx_oidkg and ch.bx_pfarrkreis = pk.bx_name " +
				"set p.bx_oidkg = ch.bx_oidkg, p.bx_oidpfarrkreis = pk.oid " +
				"where (p.bx_konfession = 'RK' or p.bx_konfession = 'EV-REF') and p.bx_oidkg < 999 and p.bx_statusperson = 'j' " +
				"and bx_adressestadt = 'j' ";
		logWriter.writeText("set OIDKG, Pfarrkreis (1) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
		
		/* Daf�r sorgen, dass Personen im Maintable sicher passiv sind, wenn sie in der
		 * History passiv sind.
		 */
		query = "update " + PostImport._shadowTable + " ph inner join " + PostImport._mainTable + " p on ph.bx_oidperson = p.oid " +
			"set p.bx_statusperson = 'n' where ph.bx_lastentry = 'j' and ph.bx_statusperson = 'n' " + 
			"and p.bx_statusperson = 'j'";
		logWriter.writeText("set StatusPerson (1) = '" + query + "'");
		BLManager.executeUpdate(dbConn, query, false);
	}

	static void updateTableOrganisationen (Connection c, Statement stmt, LogFileWriter logWriter) throws SQLException {
		String subquery = "select distinct person.bx_OIDPerson from "+PostImport._shadowTable+ " as person " +
			"where person.bx_RecordStatus='A' and (person.bx_SterbeDatum is not null)";
		// Automatisches L�schene aus Organisationen nicht gut nach Michael (20081201)	
		//"where person.bx_RecordStatus='A' and (person.bx_VorfallArt = 'TOD' or person.bx_statusperson='n')";
	
		String query = "delete from bx_orgpersonen where bx_personOIDlistOrgPOID in (" + subquery + ")";
		logWriter.writeText("updateTableOrganisationen = '" + query + "'");
		//BLManager.executeUpdate(_dbConn, query, true);
		stmt.executeUpdate(query);
	}

	static void updateTableMeldungenUndAnforderungen (Connection c, Map<Long, List<BOMeldung.Meldung>> meldungen, Anforderungen anforderungen, Map<Long, List<Long>> fam, LogFileWriter logWriter) throws SQLException, XMetaModelNotFoundException, XMetaModelChangeFailedException {
		final String table = "bx_meldung";
		int numOfMeldungenUpdates = 0;
		int numOfPersonenPerMeldungenUpdates = 0;
		logWriter.writeNewline();
		logWriter.writeText("---------------------------------------------------------------------------------");
		logWriter.writeText("Ge\u00e4nderte Meldungen werden in Datenbank zur\u00fcckgeschrieben (" + table + ")");
		logWriter.writeText("---------------------------------------------------------------------------------");
		
		Statement s = null;
		List<Statement> stmts = new ArrayList<Statement>();
		try {
			s = c.createStatement();
			ResultSet rs = s.executeQuery("select max(oid) from bx_meldung");
			rs.next();
			long maxoid = rs.getLong(1);
			long artoid = MetaObjectLookup.getInstance().getMetaObject("Meldung").getOID();
			String sqlGetAllMldPersons = "select distinct bx_oidperson from bx_meldung m inner join bx_personhistory ph on m.bx_PersonHistorymeldungenOID = ph.oid inner join bx_meldungsvorlage mv on m.bx_MeldungsvorlagemeldungenOID = mv.oid where mv.bx_domain = '" + Meldungsdomain.PostImport.getIdentifier() + "' and ph.bx_lastentry = 'j'";
			String sqlSetMeldung = "insert into " + table + " (oid, artoid, bx_PersonHistorymeldungenOID, bx_MeldungsvorlagemeldungenOID, bx_arguments, bx_AnforderungmeldungenOID) " +
				"values (?," + artoid + ",(select oid from bx_personhistory where bx_oidperson = ? and bx_lastEntry = 'j' limit 1),?,?,?)";
			String sqlGetMeldungen = "select bx_MeldungsvorlagemeldungenOID, bx_arguments, m.OID from bx_meldung m inner join bx_meldungsvorlage mv on m.bx_MeldungsvorlagemeldungenOID = mv.oid inner join bx_personhistory ph on m.bx_PersonHistorymeldungenOID = ph.oid where bx_oidperson = ? and ph.bx_lastentry = 'j' and mv.bx_domain = '" + Meldungsdomain.PostImport.getIdentifier() + "'";
			String sqlGetMeldungenPrev = "select bx_MeldungsvorlagemeldungenOID, bx_arguments, m.OID from bx_meldung m inner join bx_meldungsvorlage mv on m.bx_MeldungsvorlagemeldungenOID = mv.oid inner join bx_personhistory ph on m.bx_PersonHistorymeldungenOID = ph.oid where ph.oid = ? and mv.bx_domain = '" + Meldungsdomain.PostImport.getIdentifier() + "'";
			String sqlDelMeldungen = "delete m from bx_meldung m inner join bx_meldungsvorlage mv on m.bx_MeldungsvorlagemeldungenOID = mv.oid inner join bx_personhistory ph on m.bx_PersonHistorymeldungenOID = ph.oid where bx_oidperson = ? and ph.bx_lastentry = 'j' and mv.bx_domain = '" + Meldungsdomain.PostImport.getIdentifier() + "' and m.bx_AnforderungmeldungenOID is null";
			String sqlUpdMeldungen = "update bx_meldung m inner join bx_meldungsvorlage mv on m.bx_MeldungsvorlagemeldungenOID = mv.oid inner join bx_personhistory ph on m.bx_PersonHistorymeldungenOID = ph.oid set m.bx_PersonHistorymeldungenOID = null where bx_oidperson = ? and ph.bx_lastentry = 'j' and mv.bx_domain = '" + Meldungsdomain.PostImport.getIdentifier() + "' and m.bx_AnforderungmeldungenOID is not null";
			PreparedStatement psInsertMeldung = c.prepareStatement(sqlSetMeldung);
			PreparedStatement psGetMeldungen = c.prepareStatement(sqlGetMeldungen);
			PreparedStatement psGetMeldungenPrev = c.prepareStatement(sqlGetMeldungenPrev);
			PreparedStatement psDelMeldungen = c.prepareStatement(sqlDelMeldungen);
			PreparedStatement psUpdMeldungen = c.prepareStatement(sqlUpdMeldungen);
			PreparedStatement psPrevPH = c.prepareStatement(PostImportHelperAnf.SQL_PREV_PH);
			// checks that statusperson is the only relevant difference
			PreparedStatement psRecheck = c.prepareStatement(PostImportHelperAnf.SQL_RECHECK);
			PreparedStatement psGetPHMaxOID = c.prepareStatement(PostImportHelperAnf.SQL_GET_PHMAXOID);
			Collections.addAll(stmts, s, psInsertMeldung, psGetMeldungen, psDelMeldungen, psUpdMeldungen, psGetMeldungenPrev, psPrevPH, psGetPHMaxOID);
			rs = s.executeQuery(sqlGetAllMldPersons);
			final PSCompound psc = new PSCompound(psGetMeldungenPrev, psPrevPH, psRecheck, psGetPHMaxOID, psGetMeldungen);
			final List<Meldung> emptyMldList = new ArrayList<Meldung>(0);
			Map<Long, Long> generierteAnforderungen = new Hashtable<Long, Long>();
			// stores which person (oidPerson) has explicit or implicit (within familiy) changes
			Map<Long, Boolean> hasChanges = new HashMap<Long, Boolean>(1024);
			/*
			 * Das ZIP-Set der Meldungen um diejenigen vergr�ssern, welche im einen aktuellen
			 * History-Eintrag mit mind. einer Meldung haben. Damit ist auch gew�hrleistet, dass
			 * Meldungen gel�scht werden, wenn diese im PostImport nicht mehr produziert werden. 
			 */
			while (rs.next()) {
				long zip = rs.getLong(1);
				if (!meldungen.containsKey(zip)) {
					meldungen.put(zip, emptyMldList);
				}
			}
			
			int totalMeldungen = 0;
			for (Entry<Long, List<Meldung>> entry : meldungen.entrySet()) {
				long oidPerson = entry.getKey();
				
				List<Meldung> newMeldungen = entry.getValue();
				totalMeldungen += newMeldungen.size();
				
				boolean empty = newMeldungen.isEmpty();
				// rewrite this person's meldungen if newMeldungen is empty, or if changed
				if (!empty && meldungenEqual(oidPerson, newMeldungen, fam, hasChanges, psc, meldungen, true)) continue;
				
				// delete prev meldungen from PersonHistory which DON'T have an anforderung
				numOfPersonenPerMeldungenUpdates++;
				psDelMeldungen.setLong(1, entry.getKey());
				psDelMeldungen.execute();
				psDelMeldungen.getUpdateCount();
				
				// unlink prev meldungen from PersonHistory which have an anforderung
				psUpdMeldungen.setLong(1, entry.getKey());
				psUpdMeldungen.execute();
				psUpdMeldungen.getUpdateCount();
				
				for (int i = 0; i < newMeldungen.size(); i++) {
					Meldung m = newMeldungen.get(i);
					++maxoid;
					if (m._mv.istAnforderungNoetig()) {
						Long zipForAnforderung = m.getZIPForAnforderung();
						if (zipForAnforderung == null) {
							zipForAnforderung = entry.getKey(); // ursprung entspricht angeforderter person
						}
						Long anforderungsOID = generierteAnforderungen.get(zipForAnforderung); 
						if (anforderungsOID == null) {
							anforderungsOID = anforderungen.createAnforderung(zipForAnforderung, logWriter);
							generierteAnforderungen.put(zipForAnforderung, anforderungsOID);
						}
						psInsertMeldung.setLong(5, anforderungsOID);
					} else {
						psInsertMeldung.setNull(5, Types.BIGINT);
					}
					psInsertMeldung.setLong(1, maxoid);
					psInsertMeldung.setLong(2, entry.getKey());
					psInsertMeldung.setLong(3, m._mv.getOID());
					psInsertMeldung.setString(4, m.getArgString());
					int nofUpdates = psInsertMeldung.executeUpdate();
					if (nofUpdates > 0) {
						numOfMeldungenUpdates++;
					}
				}
				if ((numOfPersonenPerMeldungenUpdates % 1000) == 0) {
					logWriter.writeWarning(numOfPersonenPerMeldungenUpdates + " Personen mit Meldungen geupdatet");
				}
			}
			logWriter.writeText("Personen mit Meldungen geupdatet: " + numOfPersonenPerMeldungenUpdates);
			logWriter.writeText("Meldungen insgesamt geschrieben: " + numOfMeldungenUpdates);
			logWriter.writeText("Meldungen insgesamt erzeugt vom PostImport: " + totalMeldungen);
		} finally {
			if (s != null) {
				try {
					String queryString = "update bx_meldung set bx_ursprung = bx_PersonHistorymeldungenOID where bx_ursprung is null and bx_PersonHistorymeldungenOID is not null";
					DBUtils.executeUpdate(s, queryString);
				} catch (Exception e) {
					logWriter.writeError("Update von Ursprung fehlgeschlagen", e);
				}
			}
			BLManager.getInstance().setLastOID(Consts.MeldungMeta, c);
			BLManager.getInstance().setLastOID(Consts.AnforderungMeta, c);
			for (Statement stmt : stmts) {
				if (stmt != null) stmt.close();
			}
		}
	}

	/**
	 * Checks, wether this person's meldungen are diffrent from the current meldungen in the db as well as,
	 * the meldungen of the previous PersonHistory entry (to prevent cycles).
	 * Furthermore, if the meldungen are equal (i.e. the issue has not been resolved by the anforderung)
	 * the corresponding person is and the associated meldung (first) is written to the BVA.
	 * @param meldungen 
	 * @param implicit if true, returns false, if any of the person's familymembers have changes
	 * @return true, if any of this person's family members has changes 
	 */
	private static boolean meldungenEqual(long oidPerson, List<Meldung> newMeldungen, Map<Long, List<Long>> fam, Map<Long, Boolean> hasChanges, PSCompound psc, Map<Long, List<Meldung>> p2m, boolean implicit) throws SQLException {
		if (newMeldungen.isEmpty()) throw new IllegalArgumentException("newMeldungen empty");
		
		if (implicit) {
			Boolean changed = hasChanges.get(oidPerson);
			// return cached value only if it's been initialized for this person
			if (changed != null) return !changed; 
		}
		Collection<Long> requestIds = new ArrayList<Long>();
		boolean[] okTable = new boolean[newMeldungen.size()];
		PreparedStatement psGetMeldungen = psc.getPsGetMeldungen();
		psGetMeldungen.setLong(1, oidPerson);
		ResultSet mlds = psGetMeldungen.executeQuery();
		boolean equal = updateOKTable(newMeldungen, okTable, mlds, requestIds);
		if (!equal) {
			// try rechecking - this should prevent certain anforderung cycles
			PreparedStatement psPrevPH = psc.getPsPrevPH();
			psPrevPH.setLong(1, oidPerson);
			ResultSet rsPH = psPrevPH.executeQuery();
			if (rsPH.next()) {
				long prevPHOID = rsPH.getLong(1);
				PreparedStatement psGetPHMaxOID = psc.getPsGetPHMaxOID();
				psGetPHMaxOID.setLong(1, oidPerson);
				ResultSet rsMaxOID = psGetPHMaxOID.executeQuery();
				long phOID = rsMaxOID.next() ? rsMaxOID.getLong(1) : -1;
				if (canRecheck(phOID, prevPHOID, psc.getPsRecheck())) {
					PreparedStatement psGetMeldungenPrev = psc.getPsGetMeldungenPrev();
					psGetMeldungenPrev.setLong(1, prevPHOID);
					mlds = psGetMeldungenPrev.executeQuery();
					requestIds.clear();
					equal = updateOKTable(newMeldungen, okTable, mlds, requestIds);
				}
			}
		}
		if (!implicit) return equal;
		
		final List<Long> persons = fam.get(oidPerson);
		if (equal) {
			boolean implicitChanges = false;
			for (Long oid : persons) {
				List<Meldung> nm = p2m.get(oid);
				if (nm == null || nm.isEmpty()) continue; // undecided
				
				if (!meldungenEqual(oid, nm, fam, hasChanges, psc, p2m, false)) {
					implicitChanges = true;
					break;
				}
			}
			equal = !implicitChanges;
			if (equal) {
				Iterator<Long> it = requestIds.iterator();
				long requestId = it.hasNext() ? it.next() : -1;
				boolean hasPermission = false;
				for (Meldung m : newMeldungen) {
					// check the persons which are going to be requested instead
					// of the persons which the requests are created for (i.e. the family)
					// because if we were to check based on the family we might wrongfully
					// write to the BVA. This is the case if, for instance a person is missing
					// from a family, then we want to check whether that person is permitted
					// to be requested instead of checking whether the family members are permitted.
					if (psc.hasPermission(m.getZIPForAnforderung())) {
						hasPermission = true;
						break;
					}
				}
				Meldung m = newMeldungen.isEmpty() ? null : newMeldungen.get(0);
				String cause = m != null ? BOMeldung.makeMeldung(m.getVorlage().getText(), m.getArgString()) : "?";
				BVAStatus status = hasPermission ? BVAStatus.INCONSISTENT : BVAStatus.SUPPRESSED;
				BVASupport.addEntries(persons, status, BVASupport.currentImportId(), requestId, cause, null);
			}
		}
		// it's probably not faster to write a entry per person but we use the same structure as fam for consitency
		for (Long oid : persons) {
			hasChanges.put(oid, !equal);
		}
		return equal;
	}

	private static boolean canRecheck(long phOID, long prevPHOID, PreparedStatement recheck) throws SQLException {
		if (phOID <= 0 || prevPHOID <= 0) return false;
		
		// recheck query used to test that prev statusperson != cur statusperson, 
		// however this won't work, as the PersonHistory has already been written
		recheck.setLong(1, phOID);
		recheck.setLong(2, prevPHOID);
		return recheck.executeQuery().next();
	}

	private static boolean updateOKTable(List<Meldung> newMeldungen, boolean[] okTable, ResultSet mlds, Collection<Long> requestIds) throws SQLException {
		boolean allMatch = true;
		Set<String> dbTable = new HashSet<String>();
		while (mlds.next()) {
			dbTable.add(mlds.getLong(1) + "|" + mlds.getString(2));
			requestIds.add(mlds.getLong(3));
		}
		if (okTable.length != newMeldungen.size()) {
			LOGGER.warn("okTable, newMeldungen size mismatch. hint: " + requestIds);
		}
		int len = Math.min(okTable.length, newMeldungen.size());
		for (int i = 0; i < len; i++) {
			if (okTable[i]) continue; // already checked
			
			Meldung m = newMeldungen.get(i);
			String key = m._mv.getOID() + "|" + m.getArgString();
			if (dbTable.contains(key)) {
				okTable[i] = true;
			} else {
				// we expect the okTable to be initialized with false as default and we don't override
				// previous checks, if this method is called multiple times
				allMatch = false;
			}
		}
		return allMatch && dbTable.size() == okTable.length;
	}
	
	/**
	 * Utility class for meldungen checks which holds many prepared statements
	 */
	private static final class PSCompound {

		private final PreparedStatement _psGetMeldungenPrev, _psPrevPH, _psRecheck, _psGetPHMaxOID, _psGetMeldungen;
		
		private final Set<Long> _rejectedPersons;
		
		public PSCompound(PreparedStatement psGetMeldungenPrev, PreparedStatement psPrevPH, PreparedStatement psRecheck,
				PreparedStatement psGetPHMaxOID, PreparedStatement psGetMeldungen) {
			_psGetMeldungenPrev = psGetMeldungenPrev;
			_psPrevPH = psPrevPH;
			_psRecheck = psRecheck;
			_psGetPHMaxOID = psGetPHMaxOID;
			_psGetMeldungen = psGetMeldungen;
			_rejectedPersons = BOMSupport.oidSet(BOMQuery.of("PersonOA").get());
		}

		public PreparedStatement getPsGetMeldungenPrev() { return _psGetMeldungenPrev; }

		public PreparedStatement getPsPrevPH() { return _psPrevPH; }

		public PreparedStatement getPsRecheck() { return _psRecheck; }

		public PreparedStatement getPsGetPHMaxOID() { return _psGetPHMaxOID; }

		public PreparedStatement getPsGetMeldungen() { return _psGetMeldungen; }
		
		public boolean hasPermission(long person) {
			return !_rejectedPersons.contains(person);
		}
		
	}

	/**
		 * Schreibt �nderungen der Personendaten in die DB zur�ck falls sie ge�ndert worden sind.
		 * Pro Person und Eigenschaft gibt es ungef�hr eine Update-Query. 
		 */
		static void updateTablePersons (Connection c, Map<Long, Person> ht, final boolean history, LogFileWriter logWriter) throws SQLException {
			updateTablePersons(c, ht, history, logWriter, null);
		}

		static void updateTablePersons (Connection c, Map<Long, Person> ht, final boolean history, LogFileWriter logWriter, BOContext context) throws SQLException {

			int numOfRecA = 0;
			int numOfUpdates = 0;
			int numOfPassivUpdates = 0;
			int numOfKGUpdates = 0;
			int numOfHatKindUpdates = 0;
			int numOfFraumuensterUpdates = 0;
			int numOfMissionUpdates = 0;
			int numOfFamilieUpdates = 0;
			int numOfStimmrechtUpdates = 0;
			int numOfAutoZeitungUpdates = 0;
			int numOfFamInfoUpdates = 0;
			int numOfPartnerBezFamUpdates = 0;
			int numOfPartnerUpdates = 0;
			int numOfAZIUpdates = 0;
			int numOfResetImportedEntries = 0;
			int numReplaceKGwithPrevKG = 0;
	
			// Initialize SQL Statements
			String updateTable;
			String updateWhere;     // F�r Konsistenz-Checks, die nicht nur vom Import ver�nderten Personen gelten
			String updateWhereRecA; // Es geht hier um ein Update, das zwar eine Konsistenz-Berichtigung ist,
									// aber nur f�r Personen gilt, die vom Import ver�ndert wurden.
			String table;
			if (history) {
				table = PostImport._shadowTable;
				updateTable = "update " + PostImport._shadowTable + " ph ";
				updateWhere = "where bx_oidperson = ? and bx_lastEntry = 'j'";
				updateWhereRecA = "where bx_oidperson = ? and bx_recordstatus = 'A'";
			} else {
				table = PostImport._mainTable;
				updateTable = "update " + PostImport._mainTable;
				updateWhere = "where oid = ?";
				updateWhereRecA = updateWhere;
			}
	
			logWriter.writeNewline();
			logWriter.writeText("---------------------------------------------------------------------------------");
			logWriter.writeText("Ge\u00e4nderte Personendaten werden in Datenbank zur\u00fcckgeschrieben (" + table + ")");
			logWriter.writeText("---------------------------------------------------------------------------------");
	
			PreparedStatement stPassivOrKGChanged = null;
			PreparedStatement stHatKindChanged = null;
			PreparedStatement stFraumuensterFlagSet = null;
			PreparedStatement stMissionChanged = null;
			PreparedStatement stFamilieChanged = null;
			PreparedStatement stAutoZeitungChanged = null;
			PreparedStatement stPartnerBezFamChanged = null;
			PreparedStatement stFamInfoChanged = null;
			PreparedStatement stNFBPartnerChanged = null;
			PreparedStatement stAZIChanged = null;
			PreparedStatement stResetImportedEntrySet = null;
			PreparedStatement stReplaceKgWithPrevKgSet = null;
			
			/* Queries mit RecordStatus A */
			String sqlFraumuensterFlagSet = updateTable + " set bx_zeitungverzicht = 'j', bx_oidkg = 1001 " + updateWhereRecA;
			stFraumuensterFlagSet = c.prepareStatement(sqlFraumuensterFlagSet);

			String sqlResetImportedEntrySet = updateTable + " set bx_recordstatus = 'O', BX_ISLAST = 'n' " + updateWhere;
			stResetImportedEntrySet = c.prepareStatement(sqlResetImportedEntrySet);

			String sqlReplaceKgWithPrevKgSet = updateTable + " set bx_oidkg = ?, bx_zeitungverzicht = ? " + updateWhere;
			stReplaceKgWithPrevKgSet = c.prepareStatement(sqlReplaceKgWithPrevKgSet);

			// Wegen Spanischer und Italienischer Mission, die f�r Importierte zugewiesen wird:
			String sqlMissionChanged = updateTable + " set bx_oidmission = ? " + updateWhereRecA;
			stMissionChanged = c.prepareStatement(sqlMissionChanged);
	
			/* Allgemeine Queries */
			String sqlFamilieChanged = updateTable + " set bx_oidfamilie = ? " + updateWhere;
			stFamilieChanged = c.prepareStatement(sqlFamilieChanged);
			
			String sqlPassivOrKGChanged = updateTable + " set bx_statusperson = ?, bx_oidkg = ? " + updateWhere;
			stPassivOrKGChanged = c.prepareStatement(sqlPassivOrKGChanged);
			
			String sqlHatKindChanged = updateTable + " set bx_hatkind = ? " + updateWhere; 
			stHatKindChanged = c.prepareStatement(sqlHatKindChanged);
			
			String sqlAZIChanged = updateTable + " set bx_ewid = ? " + updateWhere;
			stAZIChanged = c.prepareStatement(sqlAZIChanged);
	
			String sqlPartnerChanged = null; 
			String sqlAutoZeitung = null;
			String sqlPartnerBezFamChanged = null;
			String sqlFamInfoChanged = null;
			if (!history) {
				sqlPartnerChanged = updateTable + " set bx_oidehepartner = ? " + updateWhere; 
				stNFBPartnerChanged = c.prepareStatement(sqlPartnerChanged);

				sqlAutoZeitung = updateTable + " set bx_autoZeitung = ? " + updateWhere;
				stAutoZeitungChanged = c.prepareStatement(sqlAutoZeitung);
				
				sqlPartnerBezFamChanged = updateTable + " set bx_partnerBezFam = ? " + updateWhere;
				stPartnerBezFamChanged = c.prepareStatement(sqlPartnerBezFamChanged);
				
				sqlFamInfoChanged = updateTable + " set bx_famInfo = ? " + updateWhere;
				stFamInfoChanged = c.prepareStatement(sqlFamInfoChanged);
			}
			if (history) {
				Statement stmt = c.createStatement();
				ResultSet rs = stmt.executeQuery("select count(*) from " + PostImport._shadowTable + " where bx_recordstatus = 'A'");
				rs.next();
				numOfRecA = rs.getInt(1);
				stmt.close();
			}
			
			int lastNumOfUpdates = 0;
			int num = 0;
			for (Person p : ht.values()) {
	//update StatusPerson and oidKG (most times they are updated together)
				if (p._passivChanged || p._oidKGChanged) {
					String s;
					if (p._istPassiv) s = "n"; else s = "j";
					stPassivOrKGChanged.setString(1, s);
					if (p._oidKG != 0) {
						stPassivOrKGChanged.setLong(2, p._oidKG);
					} else {
						stPassivOrKGChanged.setNull(2, Types.BIGINT);
					}
					stPassivOrKGChanged.setLong(3, p._oidPerson);
					num = stPassivOrKGChanged.executeUpdate();
					if (!history && num != 1) {
						logWriter.writeWarning("sql result count != 1 (4): " + sqlPassivOrKGChanged + ", num of returned rows = " + num + ", oid = " + p._oidPerson);
					} else {
						if (p._passivChanged) numOfPassivUpdates += num;
						if (p._oidKGChanged) numOfKGUpdates += num;
						numOfUpdates += num;
					}
					if ((numOfPassivUpdates % 1000) == 0 && p._passivChanged) {
						logWriter.writeText(numOfPassivUpdates + " times '" + sqlPassivOrKGChanged + "'");
					}
					if ((numOfKGUpdates % 1000) == 0 && p._oidKGChanged) {
						logWriter.writeText(numOfKGUpdates + " times '" + sqlPassivOrKGChanged + "'");
					}
				}
	//update hatKind
				if (p._hatKindChanged) {
					String s;
					if (p._hatKind) {
						s = "j";
						stHatKindChanged.setString(1, s);
					} else {
						// Wenn kein Kind dann null (und nicht 'n')
						stHatKindChanged.setNull(1, Types.CHAR); // Der sql-Typ ist CHAR(1)
					}
					stHatKindChanged.setLong(2, p._oidPerson);
					num = stHatKindChanged.executeUpdate();
					if (!history && num != 1) {
						logWriter.writeWarning("sql result count != 1 (5): " + sqlHatKindChanged + ", num of returned rows = " + num + ", oid = " + p._oidPerson);
					} else {
						numOfHatKindUpdates += num;
						numOfUpdates += num;
					}
					if ((numOfHatKindUpdates % 1000) == 0) {
						logWriter.writeText(numOfHatKindUpdates + " times '" + sqlHatKindChanged + "'");
					}
				}
				//	do update (overwrite) if fraumuenster flag is set
				if (p._fraumuensterFlagChanged && p._fraumuensterFlag == true) {
					if (history || !history) { // Falls table bx_person, dann immer setzen

						if(history){
							//aktuelle lastObj zurücksetzen
							stResetImportedEntrySet.setLong(1, p._oidPerson);
							num = stResetImportedEntrySet.executeUpdate();
							if (num != 1) {
								logWriter.writeWarning("sql result count != 1 (6a): " + sqlResetImportedEntrySet + ", num of returned rows = " + num + ", oid = " + p._oidPerson);
							}else{
								numOfResetImportedEntries += num;
								numOfUpdates += num;
							}

							BLTransaction trx = BLTransaction.startTransaction(context);
							//neues Obj lastObj als Sicherung erstellen
							BOPersonHistory personHistory  = BOMQuery.of(BOPersonHistory.CLASS_NAME).match("OIDPerson == " + p._oidPerson).order("OID_desc").first();
							try {
								personHistory.setTransaction(trx);
								personHistory.setOID(0);
								personHistory.setNew(true);
								personHistory.setValue(Consts.Hist_RecordStatusIndex, "A");
								personHistory.setValue(Consts.Hist_isLastIndex, new Boolean(true));
								trx.commit();
							} catch (XMetaException e) {
								e.printStackTrace();
							} catch (XSecurityException e) {
								e.printStackTrace();
							} catch (XBOConcurrencyConflict xboConcurrencyConflict) {
								xboConcurrencyConflict.printStackTrace();
							}
						}

						//KG=1001 bei Amtsadresse setzen
						stFraumuensterFlagSet.setLong(1, p._oidPerson);
						num = stFraumuensterFlagSet.executeUpdate();

						if (!history && num != 1) {
							logWriter.writeWarning("sql result count != 1 (6): " + sqlFraumuensterFlagSet + ", num of returned rows = " + num + ", oid = " + p._oidPerson);
						} else {
							numOfFraumuensterUpdates += num;
							numOfUpdates += num;
						}
						if ((numOfFraumuensterUpdates % 1000) == 0) {
							logWriter.writeText(numOfFraumuensterUpdates + " times '" + sqlFraumuensterFlagSet + "'");
						}
					}
				}
				//reset KG with prev-KG from History
				if(p._replaceEdvkgWithLastPersonKg && p._oidKG==1001){
					if (history || !history) {
						//1. letzte kg aus history holen
						BLTransaction trx = BLTransaction.startTransaction(context);

						//neues Obj lastObj erstellen
						BOPersonHistory personHistory  = BOMQuery.of(BOPersonHistory.CLASS_NAME).match("OIDPerson == " + p._oidPerson,"OIDKG != 1001").order("OID_desc").first();

						//2. letzte kg bei neuem p obj aktualisieren und Rec-Status: A setzen
						if(personHistory != null){
							//aktuelle lastObj zurücksetzen
							try {
								stReplaceKgWithPrevKgSet.setLong(1, Long.parseLong(personHistory.getAttribute("OIDKG", context)));
								stReplaceKgWithPrevKgSet.setObject(2, personHistory.getValue("ZeitungVerzicht", context));
								stReplaceKgWithPrevKgSet.setLong(3, p._oidPerson);
							} catch (XMetaModelQueryException e) {
								e.printStackTrace();
							} catch (XSecurityException e) {
								e.printStackTrace();
							}
							num = stReplaceKgWithPrevKgSet.executeUpdate();
							if (num != 1) {
								logWriter.writeWarning("sql result count != 1 (6b): " + stReplaceKgWithPrevKgSet + ", num of returned rows = " + num + ", oid = " + p._oidPerson);
							}else{
								numReplaceKGwithPrevKG += num;
								numOfUpdates += num;
							}
						}

					}
				}

				// update AZI
				if (p._ewidChanged) {
					if (p._ewid != 0) {
						stAZIChanged.setShort(1, p._ewid);
					} else {
						stAZIChanged.setNull(1, Types.BIGINT);
					}
					stAZIChanged.setLong(2, p._oidPerson);
					num = stAZIChanged.executeUpdate();
					if (!history && num != 1) {
						logWriter.writeWarning("sql result count != 1 (8): " + sqlAZIChanged + ", num of returned rows = " + num + ", oid = " + p._oidPerson);
					} else {
						numOfAZIUpdates += num;
						numOfUpdates += num;
					}
					if ((numOfAZIUpdates % 1000) == 0) {
						logWriter.writeText(numOfAZIUpdates + " times '" + sqlAZIChanged + "'");
					}
				}
	//  update oidmission
				if (p._oidMissionChanged) {
					if (p._oidMission != 0) {
						stMissionChanged.setLong(1, p._oidMission);
					} else {
						stMissionChanged.setNull(1, Types.BIGINT);
					}
					stMissionChanged.setLong(2, p._oidPerson);
					num = stMissionChanged.executeUpdate();
					if (!history && num != 1) {
						logWriter.writeWarning("sql result count != 1 (8): " + sqlMissionChanged + ", num of returned rows = " + num + ", oid = " + p._oidPerson);
					} else {
						numOfMissionUpdates += num;
						numOfUpdates += num;
					}
					if ((numOfMissionUpdates % 1000) == 0) {
						logWriter.writeText(numOfMissionUpdates + " times '" + sqlMissionChanged + "'");
					}
				}
	// update oidfamilie
				if (p._oidFamilieChanged) {
					if (p._oidFamilie != 0) {
						stFamilieChanged.setLong(1, p._oidFamilie);
					} else {
						stFamilieChanged.setNull(1, Types.BIGINT);
					}
					stFamilieChanged.setLong(2, p._oidPerson);
					num = stFamilieChanged.executeUpdate();
					if (!history && num != 1) {
						logWriter.writeWarning("sql result count != 1 (10): " + sqlFamilieChanged + ", num of returned rows = " + num + ", oid = " + p._oidPerson);
					} else {
						numOfFamilieUpdates += num;
						numOfUpdates += num;
					}
					if ((numOfFamilieUpdates % 1000) == 0) {
						logWriter.writeText(numOfFamilieUpdates + " times '" + sqlFamilieChanged + "'");
					}
				}
				if (!history) {
					// update oidehepartner (nicht in History Table, da originaler Ehepartner erhalten bleiben soll
					if (p._oidNfbPartnerChanged) {
						if (p._oidNfbPartner != 0) {
							stNFBPartnerChanged.setLong(1, p._oidNfbPartner);
						} else {
							stNFBPartnerChanged.setNull(1, Types.BIGINT);
						}
						stNFBPartnerChanged.setLong(2, p._oidPerson);
						num = stNFBPartnerChanged.executeUpdate();
						if (!history && num != 1) {
							logWriter.writeWarning("sql result count != 1 (14): " + sqlPartnerChanged + ", num of returned rows = " + num + ", oid = " + p._oidPerson);
						} else {
							numOfPartnerUpdates += num;
							numOfUpdates += num;
						}
//						if (num > 0) {
//							logWriter.writeText("OID Changed (NFB): " + p._oidPerson + "(" + p._oidNfbPartner + ")");
//						}
						if ((numOfPartnerUpdates % 1000) == 0) {
							logWriter.writeText(numOfPartnerUpdates + " times '" + sqlPartnerChanged + "'");
						}
					}
		//			update famInfo
					if (p._famInfoChanged) {
						if (p._famInfo != -1) {
							stFamInfoChanged.setShort(1, p._famInfo);
						} else {
							stFamInfoChanged.setNull(1, Types.SMALLINT);
						}
						stFamInfoChanged.setLong(2, p._oidPerson);
						num = stFamInfoChanged.executeUpdate();
						if (!history && num != 1) {
							logWriter.writeWarning("sql result count != 1 (9b): " + sqlFamInfoChanged + ", num of returned rows = " + num + ", oid = " + p._oidPerson);
						} else {
							numOfFamInfoUpdates += num;
							numOfUpdates += num;
						}
						if ((numOfFamInfoUpdates % 1000) == 0) {
							logWriter.writeText(numOfFamInfoUpdates + " times '" + sqlFamInfoChanged + "'");
						}
					}
		//			update partnerBezFam
					if (p._partnerBezChanged) {
						if (p._partnerBez != ' ') {
							stPartnerBezFamChanged.setString(1, String.valueOf(p._partnerBez));
						} else {
							stPartnerBezFamChanged.setNull(1, Types.VARCHAR);
						}
						stPartnerBezFamChanged.setLong(2, p._oidPerson);
						num = stPartnerBezFamChanged.executeUpdate();
						if (!history && num != 1) {
							logWriter.writeWarning("sql result count != 1 (9c): " + sqlPartnerBezFamChanged + ", num of returned rows = " + num + ", oid = " + p._oidPerson);
						} else {
							numOfPartnerBezFamUpdates += num;
							numOfUpdates += num;
						}
						if ((numOfPartnerBezFamUpdates % 1000) == 0) {
							logWriter.writeText(numOfPartnerBezFamUpdates + " times '" + sqlPartnerBezFamChanged + "'");
						}
					}
					if (p._autoZeitungChanged) {
						stAutoZeitungChanged.setString(1, (p.getAutoZeitung() ? "j" : "n"));
						stAutoZeitungChanged.setLong(2, p._oidPerson);
						int updates = stAutoZeitungChanged.executeUpdate();
						if (updates != 1) {
							logWriter.writeWarning("sql result count != 1 (13)" + sqlAutoZeitung + ", num of returned rows = " + num + ", oid = " + p._oidPerson);
						} else {
							numOfAutoZeitungUpdates += updates;
							numOfUpdates += updates;
						}
						if ((numOfAutoZeitungUpdates % 1000) == 0) {
							logWriter.writeText(numOfAutoZeitungUpdates + " times '" + sqlAutoZeitung + "'");
						}
					}
				}
				
				if ((numOfUpdates % 1000) == 0 && numOfUpdates != lastNumOfUpdates) {
					logWriter.writeText(numOfUpdates + " Updates ausgef\u00fchrt auf Table " + table);
					lastNumOfUpdates = numOfUpdates;
				}
			}
			
			logWriter.writeText("Statistik f\u00fcr Tabelle " + table + ":");
			if (history) {
				logWriter.writeText("# of imported persons:  " + numOfRecA);
			} else {
				logWriter.writeText("# of updated rows (oidehepartner [NFB-Partner]):  " + numOfPartnerUpdates);
				logWriter.writeText("# of updated rows (partnerBezFam):  " + numOfPartnerBezFamUpdates);
				logWriter.writeText("# of updated rows (famInfo):  " + numOfFamInfoUpdates);
				logWriter.writeText("# of updated rows (autoZeitung): " + numOfAutoZeitungUpdates);
			}
			logWriter.writeText("# of updated rows (ewid):  " + numOfAZIUpdates);
			logWriter.writeText("# of updated rows (oidfamilie):  " + numOfFamilieUpdates);
			logWriter.writeText("# of updated rows (statusPerson):  " + numOfPassivUpdates);
			logWriter.writeText("# of updated rows (oidKG):  " + numOfKGUpdates);
			logWriter.writeText("# of updated rows (oidmission):  " + numOfMissionUpdates);
			logWriter.writeText("# of updated rows (hatKind):  " + numOfHatKindUpdates);
			logWriter.writeText("# of updated rows (stimmrecht):  " + numOfStimmrechtUpdates);
			logWriter.writeText("# of updated rows (Fraumuenster Flag):  " + numOfFraumuensterUpdates);
			logWriter.writeText("# of updated rows (Reset Imported Entry):  " + numOfResetImportedEntries);
			logWriter.writeText("# of updated rows (Replace KG with Prev KG):  " + numReplaceKGwithPrevKG);
			
			/* CLOSE SQL Statements */
			stHatKindChanged.close();
			stAZIChanged.close();
			stFraumuensterFlagSet.close();
			stMissionChanged.close();
			stFamilieChanged.close();
			stResetImportedEntrySet.close();
			stReplaceKgWithPrevKgSet.close();
			if (!history) {
				stNFBPartnerChanged.close();
				stAutoZeitungChanged.close();
				stFamInfoChanged.close();
				stPartnerBezFamChanged.close();
			}
			
		}

	/*
		 * ACHTUNG! ACHTUNG! Scheint nur darum zu funktionieren, weil neue OIDs f�r die Tabelle
		 * ab 600000000 vergeben werden (wegen dem lastoid-eintrag nehme ich an)!!! mh@20091020
		 * Die Tabelle enth�lt als OID die oid der entprechenden Person, falls es eine 
		 * 100'000'000-Person ist (automatisch verarbeitete Person, d.h. in der Stadt
		 * wohnendes Kirchenmitglied). 500- und 600-Millionen-OID-Personen sind manuell
		 * hinzugef�gte Personen, welche nicht der automatischen Bestimmung des Zeitungs-
		 * erhalts unterliegen.
		 */
		static void updateTableZeitungsErhalt (Connection c, Map<Long, Person> ht, LogFileWriter logWriter) throws SQLException {
			int numOfKGAbonnementUpdates = 0;
			int numOfNewAbonnements = 0;
			int numOfAbonnementDeletions = 0;
	
			// Initialize SQL Statements
			String table;
			table = PostImport._zeitungsTable;
			
			logWriter.writeNewline();
			logWriter.writeText("---------------------------------------------------------------------------------");
			logWriter.writeText("Ge\u00e4nderte Personendaten werden in Datenbank zur\u00fcckgeschrieben (" + table + ")");
			logWriter.writeText("---------------------------------------------------------------------------------");
	
			PreparedStatement stNewAbonnements = null;
			PreparedStatement stAbonnementDeletions = null;
			PreparedStatement stKGAbonnementUpdates = null;
			
			String sqlNewAbonnements = "insert into " + table + 
			" (OID, ArtOID, bx_OIDKirchgemeinde, bx_ZeitungsNr, bx_Anzahl, bx_PersonListZeitungOID, bx_Betrag) " +
			"values (?, 74, ?, '99999', 1, ?, 0)"; 
			stNewAbonnements = c.prepareStatement(sqlNewAbonnements);
			
			String sqlAbonnementDeletions = "delete from " + table + " where oid = ?"; 
			stAbonnementDeletions = c.prepareStatement(sqlAbonnementDeletions);
			
			String sqlKGAbonnementUpdates = "update " + table + " set bx_oidkirchgemeinde = ? where oid = ?"; 
			stKGAbonnementUpdates = c.prepareStatement(sqlKGAbonnementUpdates);
	
			int num = 0;
			for (Person p : ht.values()) {
				if (p._kriegtZeitung && !p._kriegtZeitungChanged && p._oidKGZeitung != p._oidKG && p._oidPerson < 500000000L) {
					numOfKGAbonnementUpdates++;
	// update oidKirchgemeinde, weil Kirchgemeinde der Person in der Zeitungstabelle von derjenigen in
	// der Personentabelle abweicht
					if (p._oidKG > 0) {
						stKGAbonnementUpdates.setLong(1, p._oidKG);
					}
					else {
						stKGAbonnementUpdates.setNull(1, java.sql.Types.BIGINT); // TODO mh:sql-Typ dynamisch bestimmen
					}
					stKGAbonnementUpdates.setLong(2, p._oidPerson);
					num = stKGAbonnementUpdates.executeUpdate();
					if (num != 1) {
						logWriter.writeError("sql failed (1), number of returned rows not equal 1: " + sqlKGAbonnementUpdates + ", num of returned rows = " + num);
					}
					if ((numOfKGAbonnementUpdates % 1000) == 0) {
						logWriter.writeText("Executed " + numOfKGAbonnementUpdates + " times: '" + sqlKGAbonnementUpdates + "'");
					}
				}
	// insert row
				if (p._kriegtZeitung && p._kriegtZeitungChanged && p._oidPerson < 500000000L) {
					numOfNewAbonnements++;
	
					stNewAbonnements.setLong(1, p._oidPerson);
					if (p._oidPerson > 0) {
						stNewAbonnements.setLong(2, p._oidKG);
					} else {
						stNewAbonnements.setNull(2, Types.BIGINT); 
					}
					stNewAbonnements.setLong(3, p._oidPerson);
					num = stNewAbonnements.executeUpdate();
					if (num != 1) {
						logWriter.writeError("sql failed (2), number of returned rows not equal 1: " + sqlNewAbonnements + ", num of returned rows = " + num);
					}
					if ((numOfNewAbonnements % 1000) == 0) {
						logWriter.writeText("Executed " + numOfNewAbonnements + " times: '" + sqlNewAbonnements + "'");
					}
				}
	// delete row
				if (!p._kriegtZeitung && p._kriegtZeitungChanged && p._oidPerson < 500000000L) {
					numOfAbonnementDeletions++;
					stAbonnementDeletions.setLong(1, p._oidPerson);
					num = stAbonnementDeletions.executeUpdate();
					if (num != 1) {
						logWriter.writeError("sql failed (3), number of returned rows not equal 1: " + sqlAbonnementDeletions + ", num of returned rows = " + num);
					}
					if ((numOfAbonnementDeletions % 1000) == 0) {
						logWriter.writeText("Executed " + numOfAbonnementDeletions + " times: '" + sqlAbonnementDeletions + "'");
					}
				}
			}
			
			logWriter.writeText("Statistik f\u00fcr Tabelle " + PostImport._zeitungsTable + ":");
			logWriter.writeText("# of updated rows (Kirchgemeinde ge\u00e4ndert):     " + numOfKGAbonnementUpdates);
			logWriter.writeText("# of deleted rows (Person erh\u00e4lt keine Zeitung mehr): " + numOfAbonnementDeletions);
			logWriter.writeText("# of created rows (Person erh\u00e4lt neuerdings eine Zeitung):  " + numOfNewAbonnements);
	
			/* CLOSE SQL Statements */
			stKGAbonnementUpdates.close();
			stNewAbonnements.close();
			stAbonnementDeletions.close();
		}

	static void activateMutationen(Connection dbConn, BOContext boContext, LogFileWriter logWriter) throws Exception {
		logWriter.writeText("Personenmutationen werden aktiviert");
	
		boolean saveReferenceCheckSuppressedFlag = boContext.isReferenceCheckSuppressed();
		boContext.setReferenceCheckSuppressed(true); //mh@20060403 evt. existieren nicht alle OIDs
		
		BOObject persObj = null;
		try {
			String histAttr;
			MetaObject persMeta;
			BLTransaction trans;
			
			String[][] mutNameList = new String[][] {
				{"OIDCodeAufenthalt", null},
				{"OIDCodeZivilstand", null},
				{"ZivilstandsDatum", null},
				{"OIDFamilie", null},
				{"OIDCodePartner", null},
				{"OIDEhepartner", "OIDOrigPartner"},
				{"OIDVater", null},
				{"OIDMutter", null},
				{"OIDKG", null},
				{"Name", null},
				{"VornameLang", null},
				{"Vorname", null},
				{"Allianzname", null},
				{"OIDCodeAnrede", null},
				{"OIDCodeTitel", null},
				{"OIDKonfession", null},
				{"Geburtsdatum", null},
				{"Beruf", null},
				{"StatusPerson", null},
				{"PassivDatum", null},
				{"Sterbedatum", null},
				{"TelPrivat", null},
				{"TelGesch", null},
				{"TelFax", null},
				{"Email", null},
				{"TelMobile", null},
				{"AdresseStadt", null},
				{"OIDCodeVoranstellung", null},
				{"Postlagernd", null},
				{"Postfach", null},
				{"OIDCodeOrtPostfach", null},
				{"OIDCodeStrasse", null},
				{"EWID", null},
				{"HausNr", null},
				{"Ort", null},
				{"PLZOrt", null},
				{"PLZOrtEind", null},
				{"AdrZusatz", null},
				{"ZusPostlagernd", null},
				{"ZusPostfach", null},
				{"PLZPostfach", null},
				{"PLZPostfachEind", null},
				{"ZusPLZPostfach", null},
				{"ZusPLZPostfachEind", null},
				{"ZusOIDStrasse", null},
				{"ZusStrassenName", null},
				{"ZusHausNr", null},
				{"ZusPLZOrtEind", null},
				{"ZusPLZOrt", null},
				{"ZusArt", null},
				{"ZuzDatum", null},
				{"ZuzKGDatum", null},
				{"ZuzPostlagernd", null},
				{"ZuzPostfach", null},
				{"ZuzPLZPostfachEind", null},
				{"ZuzPLZPostfach", null},
				{"ZuzStrassenName", null},
				{"ZuzHausNr", null},
				{"ZuzOrt", null},
				{"ZuzPLZOrt", null},
				{"ZuzPLZOrtEind", null},
				{"ZuzCodeOrtLand", null},
				{"ZuzAdrZusatz", null},
				{"ZuzOIDKG", null},
				{"WegDatum", null},
				{"WegKGDatum", null},
				{"WegOIDKG", null},
				{"WegPostlagernd", null},
				{"WegPostfach", null},
				{"WegPLZPostfach", null},
				{"WegPLZPostfachEind", null},
				{"WegStrassenName", null},
				{"WegHausNr", null},
				{"WegOrt", null},
				{"WegPLZOrt", null},
				{"WegPLZOrtEind", null},
				{"WegCodeOrtLand", null},
				{"WegAdrZusatz", null},
				{"OIDMission", null},
				{"OIDPfarrkreis", null},
				{"ZusOrt", null},
				{"OIDCodeOrtHeimat", null},
				{"ZusOIDCodeOrtLand", null},
				{"ZusAdrZusatz", null},
				{"CodeZivilstand", null},
				{"CodeGeschlecht", null},
				{"CodeAufenthalt", null},
				{"OIDVorstand", null},
				{"Konfession", null},
				{"CodeVoranstellung", null},
				{"OIDHaushalt", null},
				{"CodeStimmrecht", null},
				{"Stimmrecht", null},
				{"MeldeadresseDatum", null},
				{"OIDVormund", null},
				{"AufenthaltsartDatum", null},
				{"KonfessionsDatum", null},
				{"AdressSperreSicherheit", "PersonensperreStadt"},
				{"rpgAktiv", null}
			};
			//int indStatusPerson = mutNameList.indexOf("StatusPerson");
			
			/* set the sesstion to switch the history off */
			ISession mySession = boContext.getRequest().getSession();
			mySession.setValue("import_noHistory", "true");
			
			persMeta = MetaObjectLookup.getInstance().getMetaObject("Person");
			// Buffers leeren, da sonst eventuell noch dirty Daten in einem Cache sind
			BLManager.getInstance().emptyBuffersWithReferrers(persMeta);
			
			trans = BLTransaction.startTransaction(boContext);
			
			int count = 0;
			for (BOObject histObj : BOMQuery.of("PersonHistory").filter("PersonHistory_RecordStatus == 'A'").itr()) {
				count++;
				try {
					persObj = trans.getObject(persMeta, Long.parseLong(histObj.getAttribute("OIDPerson", boContext)), false /* isPartOf */);
				} catch (Exception e) {
					logWriter.writeError("Unexpected Error in getting the person object of OID "+
							histObj.getAttribute("OIDPerson", boContext) + "\n" + e.getMessage(), e);
					persObj = null;
				}
			
					if (persObj != null) {
						//mh: Objekt existiert schon
						// we have a object
					}
					else {
						//mh: Objekt existiert noch nicht
						persObj = trans.getObject(persMeta, 0 /* OID */, false /* isPartOf */);
						try {
							long newOID=Long.parseLong(histObj.getAttribute("OIDPerson", boContext));
							persObj.setOID(newOID);
						}
						catch (Exception e) {
							logWriter.writeError("Unexpected Error in getting the ZIP of the History Object "+
									histObj.getAttribute("OIDPerson", boContext) + "\n" + e.getMessage(), e);
						}
					}
			
					// set all that parameters ....
					for (String[] attrPair : mutNameList) {
						histAttr = histObj.getAttribute(attrPair[0], false /*useAccessControl*/, boContext);
						if ("Email".equals(attrPair[0]) && EDataTypeValidation.validate_email(histAttr, false, "Email") != null) {
							logWriter.writeText("E-Mail Adresse ung\u00fcltig " + histAttr + " (Person " + histObj.getAttribute("OIDPerson", boContext) + ")");
							histAttr = null;
						}
						String setAttrName = attrPair[1] == null ? attrPair[0] : attrPair[1];
						persObj.setAttribute(setAttrName, histAttr, boContext, null);
					}
				
					if ( (count != 0) && ((count % 10000) == 0) ) {
						logWriter.writeText(count + " Personen aktiviert");
						trans.commit();
						trans = BLTransaction.startTransaction(boContext);
					}
			}
			/* Commiting */
			logWriter.writeText("Commiting (aktivierte Personen)");
			trans.commit();
			// jetzt ist der BO Read Cache von ArtOID=Person leer
		} catch (Exception e) {
			// XMetaModelChangeException, XMetaModelQueryException, XSecurityException, XMetaException, XDataTypeException
			throw new Exception("Error during activateMutationen: " + e.getMessage(), e);
		} finally {
			boContext.setReferenceCheckSuppressed(saveReferenceCheckSuppressedFlag);
		}
	}
	
	/**
	 * Ermittelt die durch famili�re Relationen verbundene Personen (OIDMutter, OIDVater, 
	 * OIDPartner, OIDFamilie [f�r Einschluss von Personen vergangener Familienbildung])
	 * 
	 * Wichtig ist, dass diese Funktion auf den gleichen Feldern basierend wie die neue Familienbildung 
	 * den/die potentiellen Familienverbunde zusammenstellt, sonst kann eine Familienbildung, die nur auf
	 * dem Set der hier ermittelten Personen arbeitet andere Resultate liefern, wie wenn dieselbe Familienbildung
	 * auf der gesamten Personenmenge l�uft. 
	 * @param personOIDs
	 * @param con
	 * @return
	 * @throws Exception
	 */
	public static Set<Integer> getRelatedPersons(Collection<? extends Number> personOIDs, Connection con) throws Exception {
		String getPersonSQL1 = "select oid from bx_person where oid = ?";
		PreparedStatement pExistsStmt = null;
		String getPersonSQL2 = "select oid, bx_oidfamilie, bx_oidvater, bx_oidmutter, bx_oidorigpartner from bx_person where oid = ? or bx_oidfamilie = ? or bx_oidvater = ? or bx_oidmutter = ? or bx_oidorigpartner = ?";
		PreparedStatement pRelativesStmt = null;
		pExistsStmt = con.prepareStatement(getPersonSQL1);
		pRelativesStmt = con.prepareStatement(getPersonSQL2);
		Iterator<? extends Number> it = personOIDs.iterator();
		Set<Integer> zips = new HashSet<Integer>(personOIDs.size());
		while (it.hasNext()) {
			addRelatives(it.next().intValue(), zips, pExistsStmt, pRelativesStmt);
		}
		return zips;
	}

	private static void addRelatives(int zip, Set<Integer> zips, 
			PreparedStatement pExistsStmt, PreparedStatement pRelativesStmt) throws Exception {
		pExistsStmt.setInt(1, zip);
		if (!zips.contains(zip)) {
			ResultSet rs1 = pExistsStmt.executeQuery();
			if (rs1.next()) {
				zips.add(zip);
				for (int i = 1; i <= 5; i++) {
					pRelativesStmt.setInt(i, zip);
				}
				ResultSet rs2 = pRelativesStmt.executeQuery();
				List<Integer> zips2 = new ArrayList<Integer>();
				while (rs2.next()) { // separate loop because of reuse of result set
					for (int i = 1; i <= 5; i++) {
						int zip2 = rs2.getInt(i);
						if (zip2 != 0) {
							zips2.add(zip2);
						}
					}
				}
				for (Integer zip2 : zips2) {
					addRelatives(zip2, zips, pExistsStmt, pRelativesStmt);
				}
			} else {
				// nicht vorhandene Person
			}
		} else {
			// schon hinzugef�gt
		}
		
		
	}

	/**
	 * For each person, expands its first meldung which would cause an anforderung 
	 * to the whole immediate family, for each member which does not already have a
	 * meldung which causes an anforderung.
	 */
	public static void expandMeldungen(Map<Long, List<Meldung>> meldungen, Map<Long, List<Long>> fam, LogFileWriter logWriter) {
		long expandCount = 0;
		for (Entry<Long, List<Meldung>> e : new HashSet<Entry<Long, List<Meldung>>>(meldungen.entrySet())) {
			Meldung m = firstWithAnf(e.getValue());
			if (m == null) continue;
			
			long self = e.getKey();
			for (long oid : fam.get(self)) {
				if (oid == self || firstWithAnf(meldungen.get(oid)) != null) continue;
				
				Object[] args = m.argsWithOID(self, oid, "(implizit durch " + self + ")");
				PostImport.addMeldung(self, m._mv, meldungen, args);
				expandCount++;
			}
		}
		LOGGER.info("PostImportHelper.expandMeldungen expandCount: " + expandCount);
	}
	
	private static Meldung firstWithAnf(Collection<Meldung> mList) {
		if (mList == null) return null;
		
		for (Meldung meldung : mList) {
			if (meldung.istAnforderungNoetig()) return meldung;
		}
		return null;
	}
	
}
