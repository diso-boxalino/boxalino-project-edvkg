package ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp;

import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.ADRESSZUS;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.GEBDAT;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.GESCHLECHT;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.HAUSNR;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.HAUSNRZUS;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.NAME;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.ORT;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.PLZ;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.REL;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.STRASSE;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.STRNR;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.VORNAME;
import static ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.PersonValues.ValueType.ZIP;

import java.util.Collections;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.Map;

import ch.objcons.ecom.proj.edvkg.mzv.schulDaten.imp.CSVSupport.EntryRow;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

/**
 * Thin wrapper for a person's values of the SchulDatenImport.
 */
class PersonValues {
	
	private static final ILogger LOGGER = Logger.getLogger(PersonValues.class);
	
	public enum ValueType {
		ZIP, NAME, VORNAME, GESCHLECHT, GEBDAT, STRNR, STRASSE, HAUSNR, HAUSNRZUS, ADRESSZUS, PLZ, ORT, REL
	};
	
	public enum PersonType {
		PARENT, CHILD
	};
	
	private final EntryRow _e;
	
	private final Map<ValueType, String> _m;
	
	private final PersonType _t;
	
	public PersonValues(EntryRow e, Map<ValueType, String> m, PersonType t) {
		if (e == null || m == null || t == null) throw new NullPointerException();
		
		_e = e;
		_m = m;
		_t = t;
	}
	
	public static MappingBuilder mappingBuilder() {
		return new MappingBuilder();
	}
	
	private String get(ValueType vt) {
		String name = _m.get(vt);
		if (name == null) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("unmapped ValueType " + vt + " for Map " + _m);
			}
			return null;
		}
		return _e.get(name);
	}
	
	public String zip() { return get(ZIP); }
	
	public String name() { return get(NAME); }
	
	public String vorname() { return get(VORNAME); }
	
	public String geschlecht() { return get(GESCHLECHT); }
	
	public String gebdat() { return get(GEBDAT); }
	
	public String strnr() { return get(STRNR); }
	
	public String strasse() { return get(STRASSE); }
	
	public String hausnr() { return get(HAUSNR); }
	
	public String hausnrzus() { return get(HAUSNRZUS); }
	
	public String adresszus() { return get(ADRESSZUS); }
	
	public String plz() { return get(PLZ); }
	
	public String ort() { return get(ORT); }
	
	public String rel() { return get(REL); }
	
	/**
	 * @return the backing EntryRow
	 */
	public EntryRow getEntryRow() {
		return _e;
	}
	
	public Map<ValueType, String> getMapping() {
		return _m;
	}
	
	public PersonType getType() {
		return _t;
	}
	
	@Override
	public String toString() {
		Map<ValueType, String> m = new LinkedHashMap<ValueType, String>();
		for (ValueType vt : ValueType.values()) {
			String name = _m.get(vt);
			if (name != null) {
				m.put(vt, _e.get(name));
			}
		}
		return m.toString();
	}
	
	public static class MappingBuilder {
		
		private final Map<ValueType, String> _m = new EnumMap<ValueType, String>(ValueType.class);
		
		private MappingBuilder() {}
		
		private MappingBuilder map(ValueType key, String mappedName) {
			_m.put(key, mappedName);
			return this;
		}
		
		public MappingBuilder zip(String s) { return map(ZIP, s); }
		
		public MappingBuilder name(String s) { return map(NAME, s); }
		
		public MappingBuilder vorname(String s) { return map(VORNAME, s); }
		
		public MappingBuilder geschlecht(String s) { return map(GESCHLECHT, s); }
		
		public MappingBuilder gebdat(String s) { return map(GEBDAT, s); }
		
		public MappingBuilder strnr(String s) { return map(STRNR, s); }
		
		public MappingBuilder strasse(String s) { return map(STRASSE, s); }
		
		public MappingBuilder hausnr(String s) { return map(HAUSNR, s); }
		
		public MappingBuilder hausnrzus(String s) { return map(HAUSNRZUS, s); }
		
		public MappingBuilder adresszus(String s) { return map(ADRESSZUS, s); }
		
		public MappingBuilder plz(String s) { return map(PLZ, s); }
		
		public MappingBuilder ort(String s) { return map(ORT, s); }
		
		public MappingBuilder rel(String s) { return map(REL, s); }
		
		public Map<ValueType, String> build() {
			return Collections.unmodifiableMap(new EnumMap<ValueType, String>(_m));
		}
	}
	
}