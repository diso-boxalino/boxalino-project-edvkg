package ch.objcons.ecom.proj.edvkg;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Hashtable;

/**
 * Insert the type's description here.
 * Creation date: (06.01.2003 20:47:51)
 * @author: 
 */
public class ZeitungDuplicateFinder {
/**
 * ZeitungDuplicateFinder constructor comment.
 */
public ZeitungDuplicateFinder() {
	super();
}
/**
 * Starts the application.
 * @param args an array of command-line arguments
 */
public static void main(java.lang.String[] args) {
	try {
		FileReader fr = new FileReader("c:\\edvkg\\kathZeitungen.txt");
		BufferedReader br = new BufferedReader(fr);
		String line = null;
		Hashtable ht = new Hashtable();
		int duplicateCount = 0;
		while ((line = br.readLine()) != null) {
//			System.out.println(line);
			if (ht.containsKey(line)) {
				duplicateCount++;
				String zip = line.substring(0, 9);
				String zcode1 = line.substring(248, 251);
				String zcode2 = line.substring(252, 255);
				System.out.println(zip /*+ " code = '" + zcode1 + "/" + zcode2 + "'*/ + " (Originalzeile: " + line + ")");
			}
			ht.put(line, line);
		}
	}
	catch (Exception x) {
		x.printStackTrace();
	}
}
}
