package ch.objcons.ecom.proj.edvkg.mzv.bva;

import static ch.objcons.ecom.proj.edvkg.mzv.bva.BVAStatus.DELIVERED;
import static ch.objcons.ecom.proj.edvkg.mzv.bva.BVAStatus.INCONSISTENT;
import static ch.objcons.ecom.proj.edvkg.mzv.bva.BVAStatus.SUPPRESSED;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

import ch.objcons.db.SQLFormat;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.bom.utils.BOMFilter;
import ch.objcons.ecom.bom.utils.BOMQuery;
import ch.objcons.ecom.bom.utils.BOMSupport;
import ch.objcons.ecom.datatypes.XDataTypeException;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.system.security.XSecurityException;

import com.boxalino.log.ILogger;
import com.boxalino.log.Logger;

/**
 * Support class for writing / updating and marking BVA entries.<br>
 * BOBVA and BOBVAEntry represent which persons have been inconsistent / caused a Request (Anforderung)
 * which has NOT been resolved. More formally, if a family has the exact same Requests during two separate,
 * successive PostImport's {@link #addEntries} is invoked. Whenever a Person is delivered trough the import,
 * its corresponding BVAEntry, if available is marked accordingly {@link #markEntriesDelivered}.
 */
public final class BVASupport {

	private static final ILogger LOGGER = Logger.getLogger(BVASupport.class);
	
	/**
	 * Creates a BOBVA object with a BOBVAEntry per member. If and only if such an entry with the exact
	 * same members already exists, it is reused, otherwise it is created anew. If trx is null, it a new
	 * transaction is created internally and immediately committed.
	 * @return the created or reused BOBVA object
	 */
	public static BOBVA addEntries(List<Long> members, BVAStatus status, long importId, long requestId, String cause, BLTransaction trx) {
		BOContext ctx = BOContext.get();
		boolean commit = false;
		if (trx == null) {
			trx = BLTransaction.startTransaction(ctx);
			commit = true;
		}
		enterPrivileged();
		try {
			BOBVA bva = tryUpdateEntries(members, status, importId, requestId, cause, trx);
			if (bva != null) {
				commitIf(trx, commit);
				return bva;
			}
			bva = (BOBVA) trx.createNewObject(BOBVA.CN);
			bva.setAttribute("cause", cause, ctx);
			bva.setStatus(status);
			bva.setValue("creationImportId", importId, ctx);
			bva.setValue("modificationImportId", importId, ctx);
			bva.setValue("creationRequestId", requestId, ctx);
			bva.setValue("modificationRequestId", requestId, ctx);
			for (Long oidPerson : members) {
				bva.addListReferencedObject("entries", newEntry(oidPerson, status, trx));
			}
			commitIf(trx, commit);
			return bva;
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			exitPrivileged();
		}
	}
	
	/**
	 * Updates the BOBVA and its entries to which the specified members correspond to if, and only if
	 * if such BOBVA with the exact specified entries exists and returns the matching BOBVA, else null is returned.
	 */
	private static BOBVA tryUpdateEntries(List<Long> members, BVAStatus status, long importId, long requestId, String cause, BLTransaction trx) {
		BOBVAEntry probe = getBVAEntry(members.get(0), trx);
		if (probe == null) return null;
		
		BOBVA bva = probe.getBVA();
		if (!bva.sameMembers(members)) return null;
		
		enterPrivileged();
		try {
			BOContext ctx = BOContext.get();
			bva.setValue("modificationImportId", importId, ctx);
			bva.setValue("modificationRequestId", requestId, ctx);
			bva.setAttribute("cause", cause, ctx);
			bva.setStatus(status);
			for (BOBVAEntry e : bva.getEntries()) {
				e.setStatus(status);
			}
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			exitPrivileged();
		}
		return bva;
	}
	
	private static BOBVAEntry newEntry(Long oidPerson, BVAStatus status, BLTransaction trx) {
		enterPrivileged();
		try {
			BOContext ctx = BOContext.get();
			BOBVAEntry bvaEntry = (BOBVAEntry) trx.createNewObject(BOBVAEntry.CN);
			bvaEntry.setValue("OIDPerson", oidPerson, ctx);
			bvaEntry.setStatus(status);
			return bvaEntry;
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			exitPrivileged();
		}
	}

	/**
	 * Marks all entries whose status is {@link BVAStatus#INCONSISTENT} OR {@link BVAStatus#SUPPRESSED}. The importId is updated and the status is set to DELIVERED. 
	 * If trx is null, a new transaction is created internally and immediately committed.
	 * @return the affected BOBVA's. Usually the list has one entry or is empty.
	 */
	public static List<BOBVA> markEntriesDelivered(long oidPerson, long importId, BLTransaction trx) {
		return markEntries(oidPerson, importId, EnumSet.of(INCONSISTENT, SUPPRESSED), DELIVERED, trx);
	}
	
	/**
	 * Marks all entries whose status match the specified searchStatus. The importId is updated and the status is set to newStatus.
	 * If trx is null, a new transaction is created internally and immediately committed.
	 * @return the affected BOBVA's. Usually the list has one entry or is empty.
	 */
	public static List<BOBVA> markEntries(long oidPerson, long importId, Collection<BVAStatus> searchStates, BVAStatus newStatus, BLTransaction trx) {
		BOContext ctx = BOContext.get();
		boolean commit = false;
		if (trx == null) {
			trx = BLTransaction.startTransaction(ctx);
			commit = true;
		}
		enterPrivileged();
		try {
			List<BOBVA> bvas = getBVAs(oidPerson, searchStates, trx);
			if (bvas.isEmpty()) return Collections.emptyList();
			
			if (bvas.size() > 1) {
				LOGGER.warn("invariant: bvas size exceeds 1 for Person " + oidPerson + " (" + BOMSupport.oidList(bvas) +")");
			}
			for (BOBVA bva : bvas) {
				// not setting requestId (anforderungId) here because the idea is to preserve the original requestId
				bva.setValue("modificationImportId", importId, ctx);
				bva.setStatus(newStatus);
				for (BOBVAEntry bvaEntry : bva.getEntries()) {
					bvaEntry.setStatus(newStatus);
				}
			}
			commitIf(trx, commit);
			return bvas;
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			exitPrivileged();
		}
	}

	private static List<BOBVA> getBVAs(long oidPerson, Collection<BVAStatus> searchStates, BLTransaction trx) {
		return BOMFilter.of(BOBVA.CN)
			.eq("entries_OIDPerson", oidPerson)
			.in("status", searchStates)
			.qry().trx(trx).get();
	}

	static BVAStatus setStatus(BVAStatus newState, BOObject bo) {
		BVAStatus prev = getStatus(bo);
		try {
			bo.setAttribute("status", newState.toString(), BOContext.get());
			return prev;
		} catch (XMetaException e) {
			throw new RuntimeException(e);
		} catch (XDataTypeException e) {
			throw new RuntimeException(e);
		} catch (XSecurityException e) {
			throw new RuntimeException(e);
		}
	}

	static BVAStatus getStatus(BOObject bo) {
		String status = bo.attr("status");
		return status != null ? BVAStatus.valueOf(status) : null;
	}
	
	private static BOBVAEntry getBVAEntry(long oidPerson, BLTransaction trx) {
		return byKeyHi(BOBVAEntry.CN, "OIDPerson", oidPerson, trx);
	}
	
	/**
	 * like BOMQuery's byKey, except it is guaranteed to return the highest entry (max oid) 
	 */
	private static <T extends BOObject> T byKeyHi(String className, String key, Object value, BLTransaction trx) {
		value = SQLFormat.stringToSQLvalue(value == null ? null : "" + value);
		@SuppressWarnings("unchecked")
		T bo = (T) BOMQuery.of(className).match(key + " == " + value).order("OID_desc").trx(trx).first();
		return bo;
	}
	
	public static long currentImportId() {
		// TODO MOVEME
		BOObject boImportLog = BOMQuery.of("ImportLog", "", "OID_desc", null).first();
		return boImportLog != null ? boImportLog.getOID() : -1;
	}
	
	private static void commitIf(BLTransaction trx, boolean commit) {
		try {
			if (commit) trx.commit();
		} catch (XMetaException e) {
			throw new RuntimeException(e);
		} catch (XSecurityException e) {
			throw new RuntimeException(e);
		} catch (XBOConcurrencyConflict e) {
			throw new RuntimeException(e);
		}
	}
	
	private static final ThreadLocal<Long> PRIV_STATE = new ThreadLocal<Long>() {
		
		protected Long initialValue() { return 0L; }
		
	};
	
	private static final long WAS_PRIV = 1 << 61;
	
	private static void enterPrivileged() {
		long state = PRIV_STATE.get() + 1;
		if (state == WAS_PRIV) throw new IllegalStateException();
		
		if (state == 1) {
			BOContext context = BOContext.get();
			if (context.isGUIClient()) {
				state |= WAS_PRIV;
			} else { 
				// assume, that between reentrant enterPrivileged calls isGuiClient never changes
				context.setGUIClient(true);
			}
		}
		PRIV_STATE.set(state);
	}
	
	private static void exitPrivileged() {
		long state = PRIV_STATE.get() - 1;
		if (state == -1) throw new IllegalStateException("must enter first");
		
		if (state == WAS_PRIV) {
			state = 0;
		} else if (state == 0) {
			BOContext.get().setGUIClient(false);
		}
		PRIV_STATE.set(state);
	}
	
	private BVASupport() {}
	
}
