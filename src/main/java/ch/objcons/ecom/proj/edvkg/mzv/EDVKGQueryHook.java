package ch.objcons.ecom.proj.edvkg.mzv;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import ch.objcons.db.DBConnectionPool;
import ch.objcons.db.DBUtils;
import ch.objcons.ecom.api.Boxalino;
import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.api.XBoxValueManagementException;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.box.client.BoxClient;
import ch.objcons.ecom.datatypes.EDataTypeValidation;
import ch.objcons.ecom.query.IQueryHook;
import ch.objcons.log.Log;

import com.boxalino.server.Request;

public class EDVKGQueryHook implements IQueryHook {

	public static final int QH_BRIEF_AN_FAMILIE             = 1;
	public static final int QH_BRIEF_AN_PERSON              = 2;
	public static final int QH_BRIEF_AN_ELTERN              = 3;
	public static final int QH_BRIEF_AN_ELTERN_NO_DUPLS     = 7;
	public static final int QH_BRIEF_AN_ZEITUNGSADRESSE     = 4;
	public static final int QH_ZEITUNGSEMPFAENGER           = 5;
	public static final int QH_ORGPERSONEN                  = 6;
	
	public static final int QH_BRIEF_AN_FAMILIE_ORG         = 8;
	public static final int QH_BRIEF_AN_PERSON_ORG          = 9;
	public static final int QH_BRIEF_AN_ELTERN_ORG          = 10;
	public static final int QH_BRIEF_AN_ELTERN_NO_DUPLS_ORG = 11;
	
	private String _selectClause = null;
	private String _fromClause = null;
	private Map<Boolean, String> _where;
	
	protected EDVKGQueryHook() {
		
	}
	
	protected EDVKGQueryHook(int queryType) throws Exception {
		String select = null, from = null, where = "";
		switch (queryType) {
		case QH_BRIEF_AN_PERSON:
			select = "select count(a.oid) from ";
			where = " and (coalesce(a.bx_Postsperre, 'n') = 'n'";
			break;
		case QH_BRIEF_AN_FAMILIE:
			select = "select count(distinct a.bx_oidfamilie) from ";
			where = " and (coalesce(a.bx_Postsperre, 'n') = 'n'";
			// from = "left join bx_person v on a.bx_oidfamilie = v.oid left join bx_person ehe on v.bx_oidehepartner = ehe.oid "; // braucht es nicht
			break;
		case QH_BRIEF_AN_ELTERN:
			select = "select count(a.oid) from ";
			where = " and (coalesce(a.bx_Postsperre, 'n') = 'n'";
			break;
		case QH_BRIEF_AN_ELTERN_NO_DUPLS:
			select = "select count(distinct a.bx_oidfamilie) from ";
			where = " and (coalesce(a.bx_Postsperre, 'n') = 'n'";
			break;
		case QH_BRIEF_AN_ZEITUNGSADRESSE:
			select = "select count(distinct a.oid) from ";
			break;
		case QH_ZEITUNGSEMPFAENGER:
			select = "select count(distinct a.oid) from ";
			break;
			
			/* ***
			 * distinct n�tig, da m�glicherweise eine Person in mehreren Org.
			 * Die Org-Queries werden auch f�r das Total der Resultate der normalen
			 * Mitgliedersuche verwendet.
			 * *** */
		case QH_BRIEF_AN_PERSON_ORG:
			select = "select count(distinct a.oid) from "; 
			break;
		case QH_BRIEF_AN_FAMILIE_ORG:
			select = "select count(distinct a.bx_oidfamilie) from ";
			break;
		case QH_BRIEF_AN_ELTERN_ORG:
			select = "select count(distinct a.oid) from ";
			break;
		case QH_BRIEF_AN_ELTERN_NO_DUPLS_ORG:
			select = "select count(distinct a.bx_oidfamilie) from ";
			break;
		default:
			throw new RuntimeException("Invalid Query Hook Type");
		} 
		_selectClause = select;
		_fromClause = from;
		String wherePf = EDataTypeValidation.isNullOrEmptyString(where) ? "" : ")";
		Map<Boolean, String> whereMap = new HashMap<Boolean, String>(2);
		whereMap.put(true, where + " and a.bx_rpgAktiv = 'j'" + wherePf);
		whereMap.put(false, where + " and a.bx_statusperson = 'j'" + wherePf);
		_where = Collections.unmodifiableMap(whereMap);
	}
	
	private String getWhere(boolean isRPG) {
		return _where.get(isRPG);
	}
	
	/**
	 * Returns an implementation of the IQueryHook interface 
	 * @param queryType One of the QH constants of this class
	 * @return the query hook instance
	 * @exception Exception If the query type is unkwown
	 */
	public static EDVKGQueryHook getInstance(int queryType) throws Exception {
		switch (queryType) {
		case QH_BRIEF_AN_PERSON:
		case QH_BRIEF_AN_ELTERN:
		case QH_BRIEF_AN_ELTERN_NO_DUPLS:
		case QH_BRIEF_AN_FAMILIE:
		//case QH_BRIEF_AN_ZEITUNGSADRESSE:
			// mail merge hooks
			return new MailMergeQueryHook(queryType);
		default:
			return new EDVKGQueryHook(queryType);
		}
	}
	
	public String hookMaxRowCount(String queryString, QueryInfo queryInfo, BOContext context) {
		try {
			return getQuery(queryString, queryInfo);
		} catch (Exception e) {
			return queryString + " limit 0";
		}
	}
	
	@Override
	public String hookGetResults(String queryString, QueryInfo queryInfo, BOContext context) {
		Connection con = null;
		IRequest request = null;
		try {
			request = context.getRequest();
			con = DBConnectionPool.instance().getConnection();
			String derivedQuery = getQuery(queryString, queryInfo);
			Statement statement = con.createStatement();
			ResultSet rs = DBUtils.executeQuery(statement, derivedQuery);
			rs.next();
			setResult(request, rs.getInt(1));
		} catch (Exception e) {
			Log.logSystemError("EDVKG Query Hook SQL failed: " + e.getMessage());
			if (request != null) setResult(request, 0);
		} finally {
			if (con != null) DBConnectionPool.instance().ungetConnection(con);
		}
		return queryString;
	}
	
	protected void setResult(IRequest request, int rowCount) {
		String[] numberOfRows = { String.valueOf(rowCount) };
		try {
			BoxClient.getInstance().handleParameter(request, "session_hookNumberOfRows", numberOfRows);
		} catch (XBoxValueManagementException e) {
		} 
	}

	protected String getQuery(String queryString, QueryInfo queryInfo) throws Exception {
		/* prepared query example: select ... where ((((bx_OIDKG = 23) AND (bx_AdresseStadt = 'j')) AND (bx_StatusPerson = 'j'))) */
		/*
		int pos = queryString.toLowerCase().indexOf(" from ");
		if (pos != -1) queryString = "select count(distinct bx_oidfamilie) " + queryString.substring(pos);
		return queryString;
		*/
		/* Annahme: Es gibt keine table aliases */
		queryString = queryString.toLowerCase();
		queryString = normalize(queryString);
		int posFrom = queryString.indexOf(" from ");
		if (posFrom == -1) throw new Exception("QueryHook konnte From Clause in query nicht finden");
		int posWhere = queryString.indexOf(" where ");
		if (posWhere == -1) {
			queryString += " where 1=1";
			posWhere = queryString.indexOf(" where ");
		}
		String fromClause = queryString.substring(posFrom + 5, posWhere + 1);
		int posPerson = fromClause.indexOf(" bx_person ");
		if (posPerson == -1) throw new Exception("QueryHook konnte table in query nicht finden");
		fromClause = "(" + fromClause.substring(0, posPerson + " bx_person".length()) + " a" + 
			fromClause.substring(posPerson + " bx_person".length()) + ") ";
		if (_fromClause != null) fromClause += _fromClause;
			 
		int posOrderBy = queryString.indexOf(" order by ", posWhere);
		if (posOrderBy == -1) posOrderBy = queryString.length();
		String whereClause = queryString.substring(posWhere, posOrderBy);
		
		/* we don't have to consider the aliases as they do not contain the bx_person table
		 * being the root table 
		 */
		if (!queryInfo.useTableNames) {
			whereClause = replace(whereClause, " bx_", " a.bx_");
			whereClause = replace(whereClause, " oid", " a.oid");
		} else {
			whereClause = replace(whereClause, " bx_person.", " a.");
		}
		boolean isRPG = Boolean.parseBoolean(new Boxalino(Request.getCurrent()).getValue("param", "isRPG"));
		queryString = _selectClause + fromClause + whereClause + getWhere(isRPG);
		
		return queryString;
	}
	
	public static String normalize(String query) {
		StringBuffer buf = new StringBuffer(2*query.length());
		for (int i = 0; i < query.length(); i++) {
			if (query.charAt(i) == ',' || query.charAt(i) == ')') buf.append(' ');
			buf.append(query.charAt(i));
			if (query.charAt(i) == ',' ||
				query.charAt(i) == '(' 
				) buf.append(' ');
		}
		return buf.toString();
	}
	
	/**
	 * 
	 * 
	 * see http://forum.java.sun.com/thread.jspa?threadID=502816&start=45&tstart=0 and
	 * http://www.sun.com/termsofuse.jsp paragraph 4
	 * @param s
	 * @param oldText
	 * @param newText
	 * @return
	 */
	public static String replace(String s, String oldText, String newText) {
		final int sLength = s.length();
		final int oldLength = oldText.length();
		final int newLength = newText.length();
 
		if (oldLength == 0)
			throw new IllegalArgumentException("cannot replace the empty string");
 
		if (oldText.equals(newText))
			return s;
 
		StringBuffer sb = null;
 
		if (newLength <= oldLength) {
			sb = new StringBuffer(sLength);
		}
		else {
			int c = 0; // count the number of replacements to optimize memory usage
 
			int i = 0;
 
			while ((i = s.indexOf(oldText, i)) > -1) {
				i += oldLength;
				c++;
			}
 
			if (c == 0)
				return s;
 
			sb = new StringBuffer(sLength + c * (newLength - oldLength));
		}
 
		int i = 0, x = 0;
 
		while ((x = s.indexOf(oldText, i)) > -1) {
			sb.append(s.substring(i, x));
			sb.append(newText);
			i = x + oldLength;
		}
 
		sb.append(s.substring(i));
 
		return sb.toString();
	}
	
	
	public static void main(String[] args) {
		// TESTS
		String str1 = "abcdefg abcdefg";
		String str2 = "xxxxxx";
		String str3 = "xxxxxxx";
		String str4 = "";
		try {
			System.out.println(replace(str2, "xx", "x"));
			System.out.println(str3.replace("", "yy"));
			if (!replace(str1, "fg", "").equals("abcde abcde")) throw new Exception("1");
			if (!replace(str1, "fg", "----").equals("abcde---- abcde----")) throw new Exception("2");
			if (!replace(str1, "fg", "x").equals("abcdex abcdex")) throw new Exception("3");
			if (!replace(str2, "xx", "x").equals("xxx")) throw new Exception("4");
			if (!replace(str2, "xx", "").equals("")) throw new Exception("5");
			if (!replace(str2, "xxxxxx", "").equals("")) throw new Exception("6");
			if (!replace(str2, "xxxxxx", "zzzzzzzz").equals("zzzzzzzz")) throw new Exception("7");
			if (!replace(str2, "xxx", "").equals("")) throw new Exception("8");
			if (!replace(str3, "x", "").equals("")) throw new Exception("9");
			if (!replace(str3, "xx", "").equals("x")) throw new Exception("10");
			if (!replace(str3, "xx", "yyy").equals("yyyyyyyyyx")) throw new Exception("11");
			//if (!replace(str3, "", "y").equals("xxxxxxx")) throw new Exception("12");
			//if (!replace(str4, "", "").equals("")) throw new Exception("13");
			//if (!replace(str4, "", "a").equals("")) throw new Exception("14");
			if (!replace(str4, "a", "").equals("")) throw new Exception("15");

			
			
			EDVKGQueryHook hook = EDVKGQueryHook.getInstance(QH_BRIEF_AN_FAMILIE);
			QueryInfo queryInfo = new QueryInfo();
			queryInfo.useTableNames = false;
			System.out.println("1: " + hook.getQuery("select * from bx_person where ((((OID < 300000000) AND (bx_OIDKG = 23) AND (bx_AdresseStadt = 'j')) AND (bx_StatusPerson = 'j')))", queryInfo));
			System.out.println("2: " + hook.getQuery("select * from bx_person", queryInfo));
			queryInfo.useTableNames = true;
			System.out.println("3: " + hook.getQuery("select distinct bx_person.* from bx_person,bx_orgpersonen where ((((bx_person.OID < 300000000) AND (bx_person.bx_Name LIKE 'a%') AND (bx_orgpersonen.bx_OrganisationOIDListOrgPOID = 86)) AND (bx_person.bx_StatusPerson = 'j'))) and (bx_person.OID = bx_orgpersonen.bx_PersonOIDListOrgPOID) order by bx_person.bx_AdresseStadt, coalesce(bx_person.bx_ZusVorname,bx_person.bx_Rufname) DESC, coalesce(bx_person.bx_ZusName,bx_person.bx_Name), coalesce(bx_person.bx_ZusStrassenName,bx_person.bx_StrasseKG), coalesce(bx_person.bx_ZusSortHausNr,bx_person.bx_SortHausNr), coalesce(bx_person.bx_ZusHausNr,bx_person.bx_HausNr)", queryInfo));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
