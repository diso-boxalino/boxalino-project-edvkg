package ch.objcons.ecom.proj.edvkg.mzv.imp;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.util.Vector;

import ch.objcons.ecom.api.IRequest;
import ch.objcons.ecom.bl.BLManager;
import ch.objcons.ecom.bl.BLTransaction;
import ch.objcons.ecom.bom.BOContext;
import ch.objcons.ecom.bom.BOFeatureValues;
import ch.objcons.ecom.bom.BOObject;
import ch.objcons.ecom.bom.XBOConcurrencyConflict;
import ch.objcons.ecom.box.dbimport.BoxImportPlus;
import ch.objcons.ecom.box.dbimport.Consts;
import ch.objcons.ecom.meta.DataType;
import ch.objcons.ecom.meta.Feature;
import ch.objcons.ecom.meta.FeatureManager;
import ch.objcons.ecom.meta.MetaAttribute;
import ch.objcons.ecom.meta.MetaAttributeGroup;
import ch.objcons.ecom.meta.MetaObject;
import ch.objcons.ecom.meta.MetaService;
import ch.objcons.ecom.meta.SimpleAttribute;
import ch.objcons.ecom.meta.XMetaException;
import ch.objcons.ecom.system.security.XSecurityException;
import ch.objcons.log.Log;

/*
 * box for customized data import from text files
 */
public class BoxImportEDVKG extends BoxImportPlus {

    private static final String VERSION_STRING = "01.07";

	private static int _transactionSize = 0;
	private static boolean _logNumberPlusOne = true;

    public BoxImportEDVKG (String slotID) {
        super(slotID);
		}

	public static int getTransactionSize() {
			return _transactionSize;
		}
	
	public static boolean mustIncrementLogNumberByOne() {
		return _logNumberPlusOne;
	}

	private void initialSetup(IRequest request) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
		BOContext context = (BOContext) request.getContext();
		BLTransaction trans = BLTransaction.startTransaction(context);

		// create value list: ImportState
		try {
			Feature feature = FeatureManager.getInstance().addFeature("ImportState" , null, null /* superType */, context);
            BOFeatureValues values = new BOFeatureValues(feature, null);
            feature.getData().addFeatureValues(values);
						values.addValue("inactive", context);
						values.addValue("active", context);
						values.addValue("started", context);
						values.addValue("aborted", context);
						values.addValue("done", context);
        }
        catch (XMetaException e) {
            // nothing to do, is upgrade from BoxImport
        }

		// create value list: ImportUpdatePolicy
		try {
			Feature feature = FeatureManager.getInstance().addFeature("ImportUpdatePolicy" , null, null /* superType */, context);
		    BOFeatureValues values = new BOFeatureValues(feature, null);
		    feature.getData().addFeatureValues(values);
			values.addValue(Consts.UPDATE_POLICY_DELETE, context);
			values.addValue(Consts.UPDATE_POLICY_IGNORE, context);
			values.addValue(Consts.UPDATE_POLICY_QUERY, context);
		}
		catch (XMetaException e) {
			// nothing to do, is upgrade from BoxImportPlus
		}

		// create import class and attributes
		MetaObject importClass = getMetaService().addObject("ImportEDVKG", "ImportEDVKG", "ch.objcons.ecom.proj.edvkg.BOImportEDVKG", "ImportEDVKG", true /* isIntern */, true /* showExtent */, trans);
		importClass.addAttribute("loginUserID", "loginUserID", DataType.INT, 0, false /* isMandatory */, false /* isUnique */, "loginUserID", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);
		MetaAttribute attr1 = importClass.addAttribute("title", "title", DataType.STRING, 0, 255, false /* isMandatory */, false /* isUnique */, "Titel", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);
		MetaAttribute attr2 = importClass.addAttribute("importFile", "importFile", DataType.FILE, 0, false /* isMandatory */, false /* isUnique */, "Importdatei", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);
		MetaAttribute attr3 = importClass.addAttribute("mappingFile", "mappingFile", DataType.STRING, 0, 255, true /* isMandatory */, false /* isUnique */, "Mapping-Datei", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);
		MetaAttribute attr4 = importClass.addAttribute("startDate", "startDate", DataType.DATETIME, 0, false /* isMandatory */, false /* isUnique */, "Startzeitpunkt", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);
		MetaAttribute attr5 = importClass.addAttribute("state", "state", false /* isMandatory */, "Status", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, "ImportState", trans);
		MetaAttribute attr6 = importClass.addAttribute("update", "update", DataType.BOOLEAN, 0, false /* isMandatory */, false /* isUnique */, "Update", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);
		MetaAttribute attr7 = importClass.addAttribute("updatePolicy", "updatePolicy", false /* isMandatory */, "Update-Politik", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, "ImportUpdatePolicy", trans);
		MetaAttribute attr8 = importClass.addAttribute("updateDelete", "updateDelete", DataType.STRING, 0, 255, false /* isMandatory */, false /* isUnique */, "Zu loeschende Datensaetze", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);
		MetaAttribute attr9 = importClass.addAttribute("autoImport", "autoImport", DataType.BOOLEAN, 0, false /* isMandatory */, false /* isUnique */, "automatischer Import", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);
		MetaAttribute attr10 = importClass.addAttribute("autoRemove", "autoRemove", DataType.BOOLEAN, 0, false /* isMandatory */, false /* isUnique */, "Importdatei automatisch loeschen", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);
		MetaAttribute attr11 = importClass.addAttribute("autoImportFile", "autoImportFile", DataType.STRING, 0, 255, false /* isMandatory */, false /* isUnique */, "Importdatei (automatischer Import)", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);
		MetaAttribute attr12 = importClass.addAttribute("autoRepeatTime", "autoRepeatTime", DataType.INT, 0, false /* isMandatory */, false /* isUnique */, "Zeitintervall (in Minuten)", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);


		MetaAttribute attr13 = importClass.addAttribute("suppressReferenceCheck", "suppressReferenceCheck", DataType.BOOLEAN, 0, false /* isMandatory */, false /* isUnique */, "suppressReferenceCheck", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);
		MetaAttribute attr14 = importClass.addAttribute("consistenceCheck", "consistenceCheck", DataType.BOOLEAN, 0, false /* isMandatory */, false /* isUnique */, "suppressReferenceCheck", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);

		// define groups
		MetaAttributeGroup group1 = importClass.addAttributeGroup("import", true /* isIntern */, null /* superGroup */, 2, trans);
		MetaAttributeGroup group2 = importClass.addAttributeGroup("update", true /* isIntern */, null /* superGroup */, 2, trans);
		MetaAttributeGroup group3 = importClass.addAttributeGroup("auto", true /* isIntern */, null /* superGroup */, 2, trans);

		// move attributes to new attribute groups
		/*
		MetaAttributeGroupPosHandler oldHandler = importClass.getAttributeGroupPosHandler(importClass.getRootClass().getDefaultAttributeGroup().getName());
		MetaAttributeGroupPosHandler newHandler = importClass.getAttributeGroupPosHandler("import");
		oldHandler.removeAttribute(attr1, trans);
		newHandler.addAttribute(attr1, 1, trans);
		oldHandler.removeAttribute(attr2, trans);
		newHandler.addAttribute(attr2, 2, trans);
		oldHandler.removeAttribute(attr3, trans);
		newHandler.addAttribute(attr3, 3, trans);
		oldHandler.removeAttribute(attr4, trans);
		newHandler.addAttribute(attr4, 4, trans);
		oldHandler.removeAttribute(attr5, trans);
		newHandler.addAttribute(attr5, 5, trans);

		newHandler = importClass.getAttributeGroupPosHandler("update");
		oldHandler.removeAttribute(attr6, trans);
		newHandler.addAttribute(attr6, 6, trans);
		oldHandler.removeAttribute(attr7, trans);
		newHandler.addAttribute(attr7, 7, trans);
		oldHandler.removeAttribute(attr8, trans);
		newHandler.addAttribute(attr8, 8, trans);

		newHandler = importClass.getAttributeGroupPosHandler("auto");
		oldHandler.removeAttribute(attr9, trans);
		newHandler.addAttribute(attr9, 9, trans);
		oldHandler.removeAttribute(attr10, trans);
		newHandler.addAttribute(attr10, 10, trans);
		oldHandler.removeAttribute(attr11, trans);
		newHandler.addAttribute(attr11, 11, trans);
		oldHandler.removeAttribute(attr12, trans);
		newHandler.addAttribute(attr12, 12, trans);
		oldHandler.removeAttribute(attr13, trans);
		newHandler.addAttribute(attr13, 13, trans);
		oldHandler.removeAttribute(attr14, trans);
		newHandler.addAttribute(attr14, 14, trans);
		*/
		// set actual version number
		getMetaService().setVersion("01.00");
		trans.putObject(getMetaService());

		// commit transaction
		trans.commit();
	}

	public void upgrade(IRequest request) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
		String version = getMetaService().getVersion();
		try{
			if (version == null) {
				// initial setup
				initialSetup(request);
				version = "01.00";
			}
		}catch(Exception e) {
			 e.printStackTrace();
			//throw new Exception(e);
		}
		try{
			if (!version.equals(VERSION_STRING)) {
				if (version.compareTo("01.01") < 0) {
					updateToVersion_01_01(request);
				}
			    if (version.compareTo("01.02") < 0) {
					updateToVersion_01_02(request);
				}
			    if (version.compareTo("01.03") < 0) {
					updateToVersion_01_03(request);
				}
			    if (version.compareTo("01.04") < 0) {
					updateToVersion_01_04(request);
				}
			    if (version.compareTo("01.05") < 0) {
					updateToVersion_01_05(request);
				}
			    if (version.compareTo("01.06") < 0) {
					updateToVersion_01_06(request);
				}
			    if (version.compareTo("01.07") < 0) {
					updateToVersion_01_07(request);
				}
				// insert next updates here
			}
		}catch(Exception e) {
			 e.printStackTrace();
			//throw new Exception(e);
		}
	}

	private void updateToVersion_01_01(IRequest request) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
		BLTransaction trans = BLTransaction.startTransaction((BOContext) request.getContext());
		MetaService service = this.getMetaService();

		MetaObject importEDVKG = this.getMetaService().getClassForName("ImportEDVKG");
		importEDVKG.addAttribute("DependsOn", "DependsOn", "DependsOn",
				false /* isMandatory */, false /* isUnique */, importEDVKG, false /* isPartOf */,
				true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT,
				false /* isReverseLink */, trans);

		// set actual version number
		service.setVersion("01.01");
		trans.putObject(service);

		// commit transaction
		trans.commit();

	}

	private void updateToVersion_01_02(IRequest request) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
		BLTransaction trans = BLTransaction.startTransaction((BOContext) request.getContext());
		MetaService service = this.getMetaService();

		MetaObject importEDVKG = this.getMetaService().getClassForName("ImportEDVKG");
		importEDVKG.addAttribute("IsErstanlieferung", "IsErstanlieferung", DataType.BOOLEAN, 0,
			false /* isMandatory */, false /* isUnique */, "Update", true /* isIntern */,
			SimpleAttribute.LIFECYCLE_PERSISTENT, trans);

		// set actual version number
		service.setVersion("01.02");
		trans.putObject(service);

		// commit transaction
		trans.commit();

	}

	private void updateToVersion_01_03(IRequest request) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
		BLTransaction trans = BLTransaction.startTransaction((BOContext) request.getContext());
		MetaService service = this.getMetaService();

		MetaObject importEDVKG = this.getMetaService().getClassForName("ImportEDVKG");
		importEDVKG.addAttribute("WaitForImportfile", "WaitForImportfile", DataType.BOOLEAN, 0,
			false /* isMandatory */, false /* isUnique */, "WaitForImportfile", true /* isIntern */,
			SimpleAttribute.LIFECYCLE_PERSISTENT, trans);
		importEDVKG.addAttribute("LastImportDate", "LastImportDate", DataType.DATETIME, 0,
			false /* isMandatory */, false /* isUnique */, "LastImportDate", true /* isIntern */,
			SimpleAttribute.LIFECYCLE_PERSISTENT, trans);

		// set actual version number
		service.setVersion("01.03");
		trans.putObject(service);

		// commit transaction
		trans.commit();

	}

	private void updateToVersion_01_04(IRequest request) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
		BLTransaction trans = BLTransaction.startTransaction((BOContext) request.getContext());

		MetaObject importClass = getMetaService().getClassForName("ImportEDVKG");
		MetaAttribute simpleAttr = importClass.getAttributeForName("loginUserID", (BOContext) request.getContext());
		simpleAttr.setIsIntern(false);
		importClass.removeAttribute("loginUserID", trans);

		// set actual version number
		getMetaService().setVersion("01.04");
		trans.putObject(getMetaService());

		// commit transaction
		trans.commit();
	}

	private void updateToVersion_01_05(IRequest request) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
		BLTransaction trans = BLTransaction.startTransaction((BOContext) request.getContext());

		MetaObject importClass = getMetaService().getClassForName("ImportEDVKG");
		MetaAttribute ma = importClass.addAttribute("transactionSize", "transactionSize", DataType.INT, 0, false /* isMandatory */, false /* isUnique */, "transactionSize", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);

		/*
		MetaAttributeGroupPosHandler oldHandler = importClass.getAttributeGroupPosHandler(importClass.getRootClass().getDefaultAttributeGroup().getName());
		MetaAttributeGroupPosHandler newHandler = importClass.getAttributeGroupPosHandler("import");
		oldHandler.removeAttribute(ma, trans);
		newHandler.addAttribute(ma, 7, trans);
		 */
		// set actual version number
		getMetaService().setVersion("01.05");
		trans.putObject(getMetaService());

		// commit transaction
		trans.commit();
	}

	private void updateToVersion_01_06(IRequest request) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
		BLTransaction trans = BLTransaction.startTransaction((BOContext) request.getContext());

		MetaObject importClass = getMetaService().getClassForName("ImportEDVKG");
		MetaAttribute ma = importClass.addAttribute("ignoreWithUnresolvedReferences", "ignoreWithUnresolvedReferences", DataType.BOOLEAN, 0, false /* isMandatory */, false /* isUnique */, "ignoreWithUnresolvedReferences", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);

		/*
		MetaAttributeGroupPosHandler oldHandler = importClass.getAttributeGroupPosHandler(importClass.getRootClass().getDefaultAttributeGroup().getName());
		MetaAttributeGroupPosHandler newHandler = importClass.getAttributeGroupPosHandler("import");
		if (newHandler == null) {
			newHandler = importClass.getAttributeGroupPosHandler("Import");
		}
		oldHandler.removeAttribute(ma, trans);
		newHandler.addAttribute(ma, 8, trans);
		 */
		
		// set actual version number
		getMetaService().setVersion("01.06");
		trans.putObject(getMetaService());

		// commit transaction
		trans.commit();
	}

	private void updateToVersion_01_07(IRequest request) throws XMetaException, XSecurityException, XBOConcurrencyConflict {
		// Add Fields of BoxImportPlus
		BLTransaction trans = BLTransaction.startTransaction((BOContext) request.getContext());

		MetaObject importPlus = getMetaService().getClassForName("ImportEDVKG");
		MetaAttribute ma = importPlus.addAttribute("omitReferences", "omitReferences", DataType.BOOLEAN, 0, false /* isMandatory */, false /* isUnique */, "omitReferences", true /* isIntern */, SimpleAttribute.LIFECYCLE_PERSISTENT, trans);

		// set actual version number
		getMetaService().setVersion("01.07");
		trans.putObject(getMetaService());

		// commit transaction
		trans.commit();
		
	}

	protected void handlePendingJobs(IRequest request) {
		// put all import objects not yet processed to the JobScheduler
		try {
			MetaObject mo = getMetaService().getClassForName("ImportEDVKG");
			BOContext boContext = (BOContext) request.getContext();
			BLTransaction trans = BLTransaction.startTransaction(boContext);

			Vector res = BLManager.instance().getExtent(mo).getResult(trans, boContext);
			for (int i=0; i<res.size(); i++) {
				BOObject obj = (BOObject) res.elementAt(i);

				if (obj instanceof BOImportEDVKG) {
					BOImportEDVKG boImport = (BOImportEDVKG) obj;
					
					// put to JobScheduler if it is an active import object
					String state = boImport.getAttribute("state", boContext);

					Log.logDesignerInfo("box-import-edvkg, check pending imports, import " + boImport.getAttribute("title", boContext) + ", state = " + state);

					if (state.equals("active") || state.equals("started")) {
                        boImport.putToJobScheduler(boContext);
                    }
                }
            }
        }
        catch (XMetaException e) {
            // ???
			e.printStackTrace();
        }
        catch (XSecurityException e) {
            // ???
			e.printStackTrace();
        }
	}

    protected void initProperties () throws Exception {
        super.initProperties();

		try {
			String transSize = getProperty("transaction size", "1000");
			_transactionSize = Integer.parseInt(transSize);
			String logNumberPlusOne = getProperty("no strict log number increment", "false");
			_logNumberPlusOne = !Boolean.parseBoolean(logNumberPlusOne);
		}
		catch (NumberFormatException e) {
			Log.logSystemError("Initialisierungsfehler initProperties", e);
		}
    }

    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        super.vetoableChange(evt);

        String key = evt.getPropertyName();

		if (key.equals("transaction size")) {
            String transactionSize = (String) evt.getNewValue();
            try {
				Integer.decode(transactionSize);
			}
			catch (NumberFormatException e) {
					// that was NOT an number
				throw new PropertyVetoException("Bitte eine korrekte Zahl eingeben", evt);
			}
        }
    }

}
